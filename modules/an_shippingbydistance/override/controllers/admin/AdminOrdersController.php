<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author     Anvanto (anvantoco@gmail.com)
 *  @copyright 2007-2018  http://anvanto.com
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AdminOrdersController extends AdminOrdersControllerCore
{
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);

        $key = Configuration::get('AN_GMAPS_KEY') ? '&key=' . Configuration::get('AN_GMAPS_KEY'):'';
        $this->addJqueryUI('ui.datepicker');
        $this->addJS(_PS_JS_DIR_.'vendor/d3.v3.min.js');
        $this->addJS('https://maps.googleapis.com/maps/api/js?v=3.exp' . $key);

        if (method_exists($this, 'access') && $this->access('edit') && $this->display == 'view') {
            $this->addJS(_PS_JS_DIR_.'admin/orders.js');
            $this->addJS(_PS_JS_DIR_.'tools.js');
            $this->addJqueryPlugin('autocomplete');
        }
    }
}
