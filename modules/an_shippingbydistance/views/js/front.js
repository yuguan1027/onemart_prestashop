/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */



var mapTpl = $('<div id="an-map-canvas" style="width:100%;height:300px"></div>')[0];

var map;
var directionsDisplay;
var directionsService;
var placesService;
var marker;
var markers = [];

var adressFields = {
    id_country:'id_country',
    city:'city',
    postcode:'postcode',
    address1:'address1',
    address2:'address2'
};

var placeSearch, autocomplete;
var componentForm = {
    locality:'city',
    postal_code: 'postcode'
};
var componentFormAdditional = {
    route: 'route',
    street_number: 'street_number'
};

function initializeAnMap()
{
    document.getElementsByName('id_country')[0].after(mapTpl);
    map = new google.maps.Map(document.getElementById('an-map-canvas'), {
        center: {lat: -33.867, lng: 151.206},
        zoom: 15,
        styles: [{
            stylers: [{ visibility: 'simplified' }]
        }, {
            elementType: 'labels',
            stylers: [{ visibility: 'off' }]
        }]
    });
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsService = new google.maps.DirectionsService();
    placesService = new google.maps.places.PlacesService(map);

    //directionsDisplay.setMap(map);
    //directionsDisplay.setOptions( { suppressMarkers: true, suppressInfoWindows: true } );

    for (var component in adressFields) {
        document.getElementsByName(component)[0].oninput = function () {
            deleteMarkers();
            if (!!getAdressFields()) {
                placesService.textSearch({query:getAdressFields()},callbackPlaces);
            }
        }
    }
    document.getElementsByName('id_country')[0].onchange = function () {
        initializeAnMap();
    }
}

$(document).ready(function () {
    //google.maps.event.addDomListener(window, 'load', initializeAnMap);
    google.maps.event.addDomListener(window, 'load', initAutocomplete);
});

function getAdressFields()
{
    var ret = "";
    for (var component in adressFields) {
        if (
            component != 'address2'
            && document.getElementsByName(component)[0].value == ""
        ) {
            return false;
        }
        if (component != 'id_country') {
            ret += ' ' + document.getElementsByName(component)[0].value;
        } else {
            ret += ' ' + getCountryNameFromSelect(component);
        }
    }
    return ret;
}

function getCountryNameFromSelect(component)
{
    select = document.getElementsByName(component)[0];
    return $('select option[value="' + select.value + '"]').html();
}

function callbackPlaces(results, status)
{
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var place = results[0];

        marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map
        });
        markers.push(marker);
        map.setCenter(place.geometry.location);


        /*
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, this);
        });
        infowindow.open(map, marker);
        */
    }
}

function setMapOnAll(map)
{
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers()
{
    setMapOnAll(null);
}

function deleteMarkers()
{
    clearMarkers();
    markers = [];
}

function initAutocomplete()
{
    setTimeout(timeoutedInitAutocomplete, 3000)
}

function timeoutedInitAutocomplete()
{
    if (document.getElementsByName('address1')[0] instanceof HTMLInputElement) {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementsByName('address1')[0]),
            {
                types: ['geocode']
                /*,componentRestrictions: {country: "dk"}*/
            }
        );

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
}

function fillInAddress()
{
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    //console.log(place);

    for (var component in componentForm) {
        document.getElementsByName(componentForm[component])[0].value = '';
        document.getElementsByName(componentForm[component])[0].disabled = false;
    }

    var valadress = '';
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i].long_name;
            document.getElementsByName(componentForm[addressType])[0].value = val;
        }

        if (componentFormAdditional[addressType]) {
            valadress = place.address_components[i].long_name + ' ' + valadress;
        }
    }
    document.getElementsByName('address1')[0].value = valadress;
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate()
{
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

