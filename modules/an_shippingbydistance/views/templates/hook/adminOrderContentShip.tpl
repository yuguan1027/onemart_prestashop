{*
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
*}

{literal}
<script>
$(document).ready(function(){
	$('a[href="#an_shipping_map"]').click();
});
var map;
function initializeAnMap() {
	map = new google.maps.Map(document.getElementById('an-map-canvas'), {});
	var directionsDisplay = new google.maps.DirectionsRenderer();
	var directionsService = new google.maps.DirectionsService();
	directionsDisplay.setMap(map);
	directionsDisplay.setOptions( { suppressMarkers: true, suppressInfoWindows: true } );

	{/literal}
	var start_point = new google.maps.LatLng({$an_map_data.start->lat|escape:false}, {$an_map_data.start->lng|escape:false});
	var end_point = new google.maps.LatLng({$an_map_data.end->lat|escape:false}, {$an_map_data.end->lng|escape:false});
	{literal}

	var marker = new google.maps.Marker({
		position: start_point,
		map: map
	});

	var marker = new google.maps.Marker({
		position: end_point,
		map: map
	});		

	google.maps.event.addListener(marker, 'click', function () {
		infowindow.open(map, this);
	});			

	var request = {
	 origin: start_point,
	 destination: end_point,
	 travelMode: google.maps.TravelMode.DRIVING,
	 provideRouteAlternatives: true,
	};
	directionsService.route(request, function(result, status) {
	 if (status == google.maps.DirectionsStatus.OK) {
	  directionsDisplay.setDirections(result);
	  {/literal}
	  infowindow = new google.maps.InfoWindow({ content: '{l s='Distance:' mod='an_shippingbydistance' js=1} {$an_map_data.distance->text|escape:htmlall}<br>{l s='Duration:' mod='an_shippingbydistance' js=1} {$an_map_data.duration->text|escape:htmlall}' });
	  {literal}
	  infowindow.open(map, marker);
	 }
	});
}

google.maps.event.addDomListener(window, 'load', initializeAnMap);

</script>
{/literal}
<div class="tab-pane" id="an_shipping_map">
	<b>{l s='From:' mod='an_shippingbydistance'}</b> {$an_map_data.from|urldecode}<br/>
	<b>{l s='To:' mod='an_shippingbydistance'}</b> {$an_map_data.to|urldecode}<br/>
	<b>{l s='Distance:' mod='an_shippingbydistance'}</b> {$an_map_data.distance->text|urldecode}<br/>
	<b>{l s='Duration:' mod='an_shippingbydistance'}</b> {$an_map_data.duration->text|urldecode}<br/><br/>
	<div id="an-map-canvas" style="width:100%;height:300px"></div>
</div>