{*
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
*}

<div class="panel">
	<div class="panel-heading">
			{l s='An Shipping By Distance	Import' mod='an_shippingbydistance'}
	</div>
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row">
            <label class="control-label col-lg-5" for="an_delimiter" style="text-align: right;margin-bottom: 0;padding-top: 7px;">
                     {l s='Delimiter:' mod='an_shippingbydistance'}
            </label>
            <div class="col-lg-5">
                <input type="text" id="an_delimiter" name="an_delimiter" value=";">
            </div>
        </div><br/>
        <div class="row">
            <label class="control-label col-lg-5" for="an_enclosure" style="text-align: right;margin-bottom: 0;padding-top: 7px;">
                     {l s='Enclosure:' mod='an_shippingbydistance'}
            </label>
            <div class="col-lg-5">
                <input type="text" id="an_enclosure" name="an_enclosure" value='"'>
            </div>
        </div><br/>
        <div class="row">
            <label class="control-label col-lg-5" for="an_import" style="text-align:right">
                <span data-toggle="tooltip" title="">
                     {l s='Import CSV file' mod='an_shippingbydistance'} (<a href="../modules/an_shippingbydistance/lib/example.csv">{l s='Example' mod='an_shippingbydistance'}</a>):
                </span>
            </label>
            <div class="col-lg-5">
                <input type="file" id="an_import" name="an_import" value="">
            </div>
        </div><br/>
        <div class="row">
            <label class="control-label col-lg-5">
            </label>
            <div class="col-lg-5">
                <input type="submit" class="btn" value="{l s='Upload' mod='an_shippingbydistance'}">
            </div>
        </div>
	</form>
</div>