{*
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
*}

{extends file="helpers/form/form.tpl"}
    {block name="input"}

    {if $input.type == 'text_info'}
        {if isset($input.text)}
            <div class="an_text_info">
                    {$input.text}
            </div>
        {/if}
	{elseif $input.name == 'AN_GMAPS_KEY'}
	{$smarty.block.parent}
	<ul style="padding-top: 10px;">
		<li>Getting API key <br /><a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="blank">https://developers.google.com/maps/documentation/javascript/get-api-key</a></li>
	
		<li>Open and enable «Directions API», «Distance Matrix API», «Geocoding API», «Maps JavaScript API», «Places API for Web» <br />
		<a href="https://developers.google.com/maps/gmp-get-started#enable-api-sdk" target="blank">https://developers.google.com/maps/gmp-get-started#enable-api-sdk</a></li>
	
	</ul>
	
	{if isset($info)}
	<div style="padding-top: 10px;">{$info}</div>
    {/if}
	{else}
        {$smarty.block.parent}
        {if isset($input.gmap) && $input.gmap}
        {literal}  
            <style>
                #map {
                    height: 300px;
                }

                .controls {
                    margin-top: 10px;
                    border: 1px solid transparent;
                    border-radius: 2px 0 0 2px;
                    box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    height: 32px;
                    outline: none;
                    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                }

                #pac-input {
                    background-color: #fff;
                    font-family: Roboto;
                    font-size: 15px;
                    font-weight: 300;
                    margin-left: 12px;
                    padding: 0 11px 0 13px;
                    text-overflow: ellipsis;
                    width: 300px;
                }

                #pac-input:focus {
                    border-color: #4d90fe;
                }

                .pac-container {
                    font-family: Roboto;
                }

                #type-selector {
                    color: #fff;
                    background-color: #4d90fe;
                    padding: 5px 11px 0px 11px;
                }

                #type-selector label {
                    font-family: Roboto;
                    font-size: 13px;
                    font-weight: 300;
                }
                #target {
                    width: 345px;
                }
            </style>

            <div id="map"></div>
            <script>
                // This example adds a search box to a map, using the Google Place Autocomplete
                // feature. People can enter geographical searches. The search box will return a
                // pick list containing a mix of places and predicted search terms.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                function initAutocomplete() {
                    {/literal}
                    {if isset($input.start) && !!$input.start}
                    var start_point = {literal}{{/literal}lat: {$input.start->lat|escape:false}, lng: {$input.start->lng|escape:false}{literal}}{/literal}
                    {else}
                    var start_point = {literal}{lat: 48.864716, lng: 2.349014}{/literal};
                    {/if}
                    {literal}
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: start_point,
                        zoom: 13,
                        mapTypeId: "roadmap"
                    });
                    {/literal}
                    var markers = [];
                    {if isset($input.start) && !!$input.start}
                    {literal}
                    markers.push(new google.maps.Marker({position: start_point, map: map}));
                    {/literal}
                    {/if}
                    {literal}


                    // Create the search box and link it to the UI element.
                    var input = document.getElementById('ADDRESS_FROM');
                    var searchBox = new google.maps.places.SearchBox(input);
                    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener('bounds_changed', function() {
                        searchBox.setBounds(map.getBounds());
                    });

                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener('places_changed', function() {
                        var places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }

                        // Clear out the old markers.
                        markers.forEach(function(marker) {
                            marker.setMap(null);
                        });
                        markers = [];

                        // For each place, get the icon, name and location.
                        var bounds = new google.maps.LatLngBounds();
                        places.forEach(function(place) {
                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }
                            var icon = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };

                            // Create a marker for each place.
                            markers.push(new google.maps.Marker({
                                map: map,
                                icon: icon,
                                title: place.name,
                                position: place.geometry.location
                            }));

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        map.fitBounds(bounds);
                        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('ADDRESS_FROM'));
                        autocomplete.bindTo('bounds', map);
                        return map;
                    });
                }
                var map = initAutocomplete();



                function initAutocomplete2() {
                    {/literal}
                    {if isset($input.start) && !!$input.start}
                    var start_point = {literal}{{/literal}lat: {$input.start->lat|escape:false}, lng: {$input.start->lng|escape:false}{literal}}{/literal}
                    {else}
                    var start_point = {literal}{lat: 48.864716, lng: 2.349014}{/literal};
                    {/if}
                    {literal}
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: start_point,
                        zoom: 13,
                        mapTypeId: "roadmap"
                    });
                    {/literal}
                    var markers = [];
                    {if isset($input.start) && !!$input.start}
                    {literal}
                    markers.push(new google.maps.Marker({position: start_point, map: map}));
                    {/literal}
                    {/if}
                    {literal}


                    // Create the search box and link it to the UI element.
                    var input = document.getElementById('ADDRESS_FROM2');
                    var searchBox = new google.maps.places.SearchBox(input);
                    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener('bounds_changed', function() {
                        searchBox.setBounds(map.getBounds());
                    });

                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener('places_changed', function() {
                        var places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }

                        // Clear out the old markers.
                        markers.forEach(function(marker) {
                            marker.setMap(null);
                        });
                        markers = [];

                        // For each place, get the icon, name and location.
                        var bounds = new google.maps.LatLngBounds();
                        places.forEach(function(place) {
                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }
                            var icon = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };

                            // Create a marker for each place.
                            markers.push(new google.maps.Marker({
                                map: map,
                                icon: icon,
                                title: place.name,
                                position: place.geometry.location
                            }));

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        map.fitBounds(bounds);
                        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('ADDRESS_FROM2'));
                        autocomplete.bindTo('bounds', map);
                        return map;
                    });
                }
                var map = initAutocomplete2();





                function initAutocomplete3() {
                    {/literal}
                    {if isset($input.start) && !!$input.start}
                    var start_point = {literal}{{/literal}lat: {$input.start->lat|escape:false}, lng: {$input.start->lng|escape:false}{literal}}{/literal}
                    {else}
                    var start_point = {literal}{lat: 48.864716, lng: 2.349014}{/literal};
                    {/if}
                    {literal}
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: start_point,
                        zoom: 13,
                        mapTypeId: "roadmap"
                    });
                    {/literal}
                    var markers = [];
                    {if isset($input.start) && !!$input.start}
                    {literal}
                    markers.push(new google.maps.Marker({position: start_point, map: map}));
                    {/literal}
                    {/if}
                    {literal}


                    // Create the search box and link it to the UI element.
                    var input = document.getElementById('ADDRESS_FROM3');
                    var searchBox = new google.maps.places.SearchBox(input);
                    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener('bounds_changed', function() {
                        searchBox.setBounds(map.getBounds());
                    });

                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener('places_changed', function() {
                        var places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }

                        // Clear out the old markers.
                        markers.forEach(function(marker) {
                            marker.setMap(null);
                        });
                        markers = [];

                        // For each place, get the icon, name and location.
                        var bounds = new google.maps.LatLngBounds();
                        places.forEach(function(place) {
                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }
                            var icon = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };

                            // Create a marker for each place.
                            markers.push(new google.maps.Marker({
                                map: map,
                                icon: icon,
                                title: place.name,
                                position: place.geometry.location
                            }));

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        map.fitBounds(bounds);
                        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('ADDRESS_FROM3'));
                        autocomplete.bindTo('bounds', map);
                        return map;
                    });
                }
                var map = initAutocomplete3();
            </script>
        {/literal}
        {/if}
    {/if}
{/block}
