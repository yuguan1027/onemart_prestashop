{*
* 2021 Anvanto
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
*  @author Anvanto <anvantoco@gmail.com>
*  @copyright  2020 Anvanto
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of Anvanto
*}

{include file='./suggestions.tpl'}


<ul class="nav nav-pills nav-fill" style="margin-bottom: 15px;">
  <li class="nav-item">
    <a class="nav-link btn btn-default" href="{$costsUrl}">{l s='Costs (Rules)' mod='an_shippingbydistance'}</a>
  </li>
  <li class="nav-item">
	<a class="nav-link btn btn-default" href="{$settingUrl}">{l s='Setting' mod='an_shippingbydistance'}</a>
  </li>
  <li class="nav-item">
	<a class="nav-link btn btn-default" href="{$AddLogUrl}">{l s='Logs' mod='an_shippingbydistance'}</a>
  </li>  
   <li class="nav-item">
	<a class="nav-link btn btn-default" href="{$AddCarrierUrl}">{l s='Add Carrier' mod='an_shippingbydistance'}</a>
  </li>    
</ul>
