<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(_PS_MODULE_DIR_ . 'an_shippingbydistance/classes/AnShippingByDistanceLog.php');

class an_shippingbydistance extends CarrierModule
{
    const PREFIX = 'AN_SBD';

    public $id_carrier;
    public $done = false;

    protected $hooks = array(
        'displayAdminOrderTabShip',
        'displayAdminOrderContentShip',
        'BackOfficeHeader',
        'displayHeader'
    );

    protected static $configs = array(
        'ADDRESS_FROM',
        'ADDRESS_FROM2',
        'ADDRESS_FROM3',
        'AN_GMAPS_KEY',
        'DISTANCE_UNIT',
		'CACHE',
		'ANSHIPDIS_LOG',
    );

    protected $tabs = array(
        array(
            'class_name' => 'AdminAnShippingByDistance',
            'parent' => 'AdminParentShipping',
            'name' => 'AN Shipping By Distance',
			'active' => 1
        ),
        array(
            'class_name' => 'AdminAnshippingbydistanceCarrier',
            'parent' => 'AdminParentShipping',
            'name' => 'AN Shipping By Distance: Add Carrier',
			'active' => 0
        ),	
        array(
            'class_name' => 'AdminAnShippingByLog',
            'parent' => 'AdminParentShipping',
            'name' => 'AN Shipping By Distance: Logs',
			'active' => 0
        ),		
    );

    public $own_carriers_basic_names = array(
        'an_sd_base_1' => 'Shipping by distance 1',
        'an_sd_base_2' => 'Shipping by distance 2',
        'an_sd_base_3' => 'Shipping by distance 3',
    );

    /**
     * an_shippingbydistance constructor.
     */
    public function __construct()
    {
        $this->name = 'an_shippingbydistance';
        $this->tab = 'shipping_logistics';
        $this->version = '2.3.0';
        $this->author = 'Anvanto';
        $this->module_key = 'c6970a365be5114ad3b32ee42a593818';
		$this->addons_product_id = '30439';
		$this->url_rate = 'https://bit.ly/3jhfk9H';
		$this->url_contact_us = 'http://bit.ly/39M532h';		

        parent::__construct();

        $this->displayName = $this->l('Shipping Costs Based on Distance');
        $this->description = $this->l('The distance will calculate by Google Maps API');
		
		$this->textDemoMode = $this->l('Demo mode is on. This functionality has been disabled.');
		
        $this->new = version_compare(_PS_VERSION_, '1.6.9.9', '>') ? true : false;
    }

    /**
     * @return bool
     */
    public function install()
    {
        if (!function_exists('curl_init')) {
            $this->_errors[] = $this->l('The CURL is not installed on the server!');
            return false;
        }

        if (parent::install()) {

            foreach ($this->hooks as $hook) {
                if (!$this->registerHook($hook)) {
                    return false;
                }
            }

            $this->getParam('ADDRESS_FROM', '');
            $this->getParam('ADDRESS_FROM2', '');
            $this->getParam('ADDRESS_FROM3', '');

            Configuration::updateValue('AN_GMAPS_KEY', '');
            $this->getParam('DISTANCE_UNIT', 'metric');
            $this->getParam('ANSHIPDIS_LOG', '');

            $sql = array();
            $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'an_shippingbydistance` (
		            `id_an_shippingbydistance` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `carrier_id_reference` int(11) DEFAULT "0",
		            `distance_from` int(11) DEFAULT "0",
		            `distance_to` int(11) DEFAULT "0",
		            `price_from` decimal(20,6) DEFAULT "0",
		            `price_to` decimal(20,6) DEFAULT "0",
		            `weight_from` decimal(20,6) DEFAULT "0",
		            `weight_to` decimal(20,6) DEFAULT "0",
		            `shipping_price` decimal(20,6) NOT NULL,
		            `price_per_km` int(1) DEFAULT "0",
		            `formula`  varchar(255) NOT NULL,
		            PRIMARY KEY (`id_an_shippingbydistance`)
		        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';
				
            $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'an_shippingbydistance_logs` (
		            `id_log` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
					
					`from` varchar(255),
					`to` varchar(255),
					`units` varchar(255),
					
					`distance_meter` int(11) DEFAULT "0",
					`distance_yard` int(11) DEFAULT "0",
					`price` decimal(20,6) NOT NULL,
					`weight` int(11) DEFAULT "0",
					`cost_id` int(11) DEFAULT "0",
					`cost_price` decimal(20,6) NOT NULL,
					
					`date_add` datetime NOT NULL,
					
		            PRIMARY KEY (`id_log`)
		        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';
				
            foreach ($sql as $_sql) {
                Db::getInstance()->Execute($_sql);
            }

            $languages = Language::getLanguages();  
            foreach ($this->tabs as $tab) {
                $_tab = new Tab();
                $_tab->class_name = $tab['class_name'];
				$_tab->active = $tab['active'];
                $_tab->id_parent = Tab::getIdFromClassName($tab['parent']);
                if (empty($_tab->id_parent)) {
                    $_tab->id_parent = 0;
                }

                $_tab->module = $this->name;
                foreach ($languages as $language) {
                    $_tab->name[$language['id_lang']] = $this->l($tab['name']);
                }

                $_tab->add();
            }

            foreach ($this->own_carriers_basic_names as $carrier_name) {
                $this->addCarrier($carrier_name);
            }

            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function uninstall()
    {
        $langDefault = (int)Configuration::get('PS_LANG_DEFAULT');
        $_carriers = new Collection('Carrier');
        $_carriers->where('external_module_name', '=', $this->name);
        $_carriers->where('deleted', '=', 0);

        foreach ($_carriers as $carrier) {
            if (Configuration::get('PS_CARRIER_DEFAULT') == $carrier->id) {
                $carriers = Carrier::getCarriers(
                    $langDefault,
                    true,
                    false,
                    false,
                    null,
                    PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE
                );
                foreach ($carriers as $c) {
                    if ($c['active'] && !$c['deleted'] && ($c['external_module_name'] != $this->name)) {
                        Configuration::updateValue('PS_CARRIER_DEFAULT', $c['id_carrier']);
                    }
                }
            }

            $carrier->delete();
        }

        foreach ($this->tabs as $tab) {
            $_tab_id = Tab::getIdFromClassName($tab['class_name']);
            $_tab = new Tab($_tab_id);
            $_tab->delete();
        }

        $uninstall = parent::uninstall();
        foreach ($this->hooks as $hook) {
            if (!$this->unregisterHook($hook)) {
                return false;
            }
        }

        $sql = array();
        $sql[] = 'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'an_shippingbydistance`';

        foreach ($sql as $_sql) {
            Db::getInstance()->Execute($_sql);
        }

        return $uninstall;
    }

    /**
     * @param $name
     * @param null $value
     * @return bool|string
     */
    public static function getParam($name, $value = null)
    {
        if (in_array($name, self::$configs)) {
            $param = self::PREFIX . $name;
            if (!is_null($value)) {
                Configuration::updateValue($param, $value);
            }

            return Configuration::get($param);
        }

        return false;
    }

    public function hookDisplayHeader($params)
    {
        $key = Configuration::get('AN_GMAPS_KEY') ? '&key=' . Configuration::get('AN_GMAPS_KEY'):'';


        if ($this->new) {

            $this->context->controller->registerJavascript(
                'an-gmaps-front-js',
                'https://maps.googleapis.com/maps/api/js?libraries=places' . $key,
                array('position' => 'top', 'priority' => 100, 'server' => 'remote')
            );
            $this->context->controller->registerJavascript(
                'an-gmaps-front2-js',
                'modules/'.$this->name.'/views/js/front.js',
                array('position' => 'top', 'priority' => 100)
            );
        } else {
            $this->context->controller->addJS($this->_path .'views/js/front.js');
            $this->context->controller->addJS('https://maps.googleapis.com/maps/api/js?libraries=places' . $key);
        }
    }

    public function getImportForm()
    {
        return $this->display(__FILE__, 'import_form.tpl');
    }

    public function hookBackOfficeHeader($params)
    {
        if (Tools::getValue('configure') === 'an_shippingbydistance') {
            $key = Configuration::get('AN_GMAPS_KEY') ? '&key=' . Configuration::get('AN_GMAPS_KEY'):'';
            $this->context->controller->addJS('https://maps.googleapis.com/maps/api/js?libraries=places' . $key);
        }
    }

    /**
     * @param $params
     * @param $shipping_cost
     * @return bool|float
     */
    public function getOrderShippingCost($params, $shipping_cost)  
    {

		$log = [];
		
        $addr = new Address($params->id_address_delivery);
        if (!Validate::isLoadedObject($addr)) {
            $shipping_rates = Db::getInstance()->ExecuteS('
                SELECT * FROM `' . _DB_PREFIX_ . 'an_shippingbydistance`
                WHERE `price_per_km` = 0
                ORDER BY `shipping_price` ASC
            ');
            if (count($shipping_rates)) {
                $shipping_cost = (float)$shipping_rates[0]['shipping_price'];
                $shipping_cost = Tools::convertPrice(
                    $shipping_cost,
                    Currency::getCurrencyInstance($params->id_currency)
                );
				//	$shipping_cost = ceil(ceil($shipping_cost)* 0.001);
                return $shipping_cost;
            }
            return false;
        }
		
        $from = urlencode($this->getParam('ADDRESS_FROM'));  
        $key = urlencode(Configuration::get('AN_GMAPS_KEY'));
        $to = urlencode($addr->address1 . ', ' . $addr->city . ', ' . $addr->postcode . ', ' . $addr->country);
        $units = $this->getParam('DISTANCE_UNIT');
		
 		if ($from == '' | $key == ''){
			return false;
		} 
		
		if (self::getParam('CACHE')){
			$cachedir = _PS_MODULE_DIR_ . $this->name . '/cache/';
			$cachefile = $cachedir . md5($to);
			$interval = strtotime('-24 hours');

			foreach (glob($cachedir."*") as $file) { 
				if (filemtime($file) <= $interval ) { 
					unlink($file); 
				}
			}

			if (file_exists($cachefile)) {
				$str_data = file_get_contents($cachefile);
				$request = json_decode($str_data,false);  

                $from2 = urlencode($this->getParam('ADDRESS_FROM2')); 
                $replaced_space2 = str_replace('+', ' ', $from2);
                $replaced_comma2 = str_replace('%2C', ',', $replaced_space2);


                if($request->destination_addresses[0] == $replaced_comma2)
                {
                    
                    $request->origin_addresses[0] = $replaced_comma2;

                    $request->rows[0]->elements[0]->distance->text = "1 m";

                    $request->rows[0]->elements[0]->distance->value = 0;

                    $request->rows[0]->elements[0]->duration->text = "1 min";

                    $request->rows[0]->elements[0]->duration->value = 0;

                    
                }


                


                $from3 = urlencode($this->getParam('ADDRESS_FROM3')); 
                $replaced_space3 = str_replace('+', ' ', $from3);
                $replaced_comma3 = str_replace('%2C', ',', $replaced_space3);

    

                if($request->destination_addresses[0] == $replaced_comma3)
                {
                    
                    $request->origin_addresses[0] = $replaced_comma3;

                    $request->rows[0]->elements[0]->distance->text = "1 m";

                    $request->rows[0]->elements[0]->distance->value = 0;

                    $request->rows[0]->elements[0]->duration->text = "1 min";

                    $request->rows[0]->elements[0]->duration->value = 0;

                    
                }


			} else {
					$request = self::sendRequest(
						'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
						. $from . '&destinations=' . $to . '&output=json&units=' . $units . '&sensor=false&key='. $key
					);
				if ($request->status == 'OK') {
					$myfile = fopen($cachefile, "w");
					fwrite($myfile, json_encode($request,JSON_UNESCAPED_UNICODE));
					fclose($myfile);
				}
			}
		} else { 
			$request = self::sendRequest(
				'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
				. $from . '&destinations=' . $to . '&output=json&units=' . $units . '&sensor=false&key='. $key
			);
		}

        $distance = false;
        if ($request->status == 'OK') {
            foreach ($request->rows as $row) {
                foreach ($row->elements as $element) {
                    if ($element->status == 'OK') {
                        $distance = (int)$element->distance->value;	//	Always in meters
                        break;
                    }
                }

                if ($distance || $distance === 0) {
                    break;
                }
            }
        }

		$log['from'] = $from;
		$log['to'] = $to;
		$log['units'] = $units;
		$log['distance_meter'] = $distance;

        if ($distance || $distance === 0) {
				
			//	Meter to Yars
			if ($units == 'imperial'){
				$distance = round($distance * 1.0936);
				$log['distance_yard'] = $distance;
			}
			
			
            $price = (float)$params->getOrderTotal(false, Cart::BOTH_WITHOUT_SHIPPING);
            $weight = (float)$params->getTotalWeight();

            $default_lang = Configuration::get('PS_LANG_DEFAULT');
            $_carrier = new Carrier($this->id_carrier, $default_lang);

            $shipping_rates = Db::getInstance()->ExecuteS('
				SELECT * FROM `' . _DB_PREFIX_ . 'an_shippingbydistance`
				WHERE `distance_from` <= ' . (int)$distance . '
				AND (`carrier_id_reference` = ' . (int)$_carrier->id_reference . ' OR `carrier_id_reference` = 0)
				AND (`distance_to` >= ' . (int)$distance . ' OR `distance_to` = 0)
				AND (`price_from` <= ' . (float)$price . ' OR `price_from` = 0)
				AND (`price_to` >= ' . (float)$price . ' OR `price_to` = 0)
				AND (`weight_from` <= ' . (float)$weight . ' OR `weight_from` = 0)
				AND (`weight_to` >= ' . (float)$weight . ' OR `weight_to` = 0)
				ORDER BY `shipping_price` ASC
			');
			
			$log['price'] = $price;			
			$log['weight'] = $weight;	

            if (count($shipping_rates)) {
                if($shipping_rates[0]['price_per_km']) {
					
					$inKm = 1000;
					if ($units == 'imperial'){
						$inKm = 1093.61;
					}
					
                    $shipping_cost = (float)$shipping_rates[0]['shipping_price'] * $distance / $inKm;
                } else {
                    $shipping_cost = (float)$shipping_rates[0]['shipping_price'];
                }
				
				$log['shipping_cost_id'] = $shipping_rates[0]['id_an_shippingbydistance'];
				$log['shipping_cost_price'] = $shipping_cost;
				
				$this->logAdd($log);

                $shipping_cost = Tools::convertPrice(
                    $shipping_cost,
                    Currency::getCurrencyInstance($params->id_currency)
                );
				
			//	$shipping_cost = ceil(ceil($shipping_cost)* 0.001);
                return $shipping_cost;
            }
        }
		
		$this->logAdd($log);
        return false;
    }
	
	public function logAdd($log = [])
	{
		if (!$this->done && (bool)$this->getParam('ANSHIPDIS_LOG')){
			
			$anLog = new AnShippingByDistanceLog();
			foreach ($log as $key => $item){				
				$anLog->{$key} = $item;
			}
			if (count($log)){
				$anLog->save();
			}
		}
		
		$this->done = true;
	}
	
	public function logClear()
	{
		return Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."an_shippingbydistance_logs  ");
	}
    /**
     * @param $params
     * @return bool|float
     */
    public function getOrderShippingCostExternal($params)
    {
        return $this->getOrderShippingCost($params, 0);
    }

    /**
     * @param $url
     * @return array
     */
    public static function sendRequest($url)
    {
        $cache_key = 'an_shippingbydistance_' . $url;
        if (!Cache::isStored($cache_key)) {
            $out = file_get_contents($url);
            /*$curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);*/
            if (!$out) {
                $out = Tools::jsonEncode((object)array('status' => 'UNKNOWN_ERROR'));
            }

            Cache::store($cache_key, $out);
        }

        $out = Cache::retrieve($cache_key);
        return Tools::jsonDecode($out);
    }

    /**
     * @param $order
     * @return bool
     */
    protected function checkOrderCarrier($order)
    {
        $carrier = new Carrier($order->id_carrier);
        if ($carrier->external_module_name == $this->name) {
            return true;
        }

        return false;
    }

    /**
     * @param $params
     * @return bool|string
     */
    public function hookDisplayAdminOrderTabShip($params)
    {
        /*if (!$this->checkOrderCarrier($params['order'])) {
            return false;
        }*/

        return $this->display(__FILE__, 'adminOrderTabShip.tpl');
    }

    /**
     * @param $params
     * @return bool|string
     */
    public function hookDisplayAdminOrderContentShip($params)
    {
        if (!$this->checkOrderCarrier($params['order'])) {
            return false;
        }

        $addr = new Address($params['order']->id_address_delivery);
        /*if (!Validate::isLoadedObject($addr)) {
            return false;
        }*/
        $key = urlencode(Configuration::get('AN_GMAPS_KEY'));
        if (!$key) {
            return $this->l('Google API Error... Please check the Google maps API key in module configuration');
        }

        $from = urlencode($this->getParam('ADDRESS_FROM'));

        $to = urlencode($addr->address1 . ', ' . $addr->city . ', ' . $addr->postcode . ', ' . $addr->country);
        $units = $this->getParam('DISTANCE_UNIT');

        $request = self::sendRequest(
            'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
            . $from . '&destinations=' . $to . '&output=json&units=' . $units . '&sensor=false&key=' . $key
        );
        $start = self::sendRequest(
            'https://maps.googleapis.com/maps/api/geocode/json?address=' . $from
            . '&key=' . $key
        );
        $end = self::sendRequest(
            'https://maps.googleapis.com/maps/api/geocode/json?address=' . $to
            . '&key=' . $key
        );

	
/* 		var_dump($start);
		die; */
		
		

        if ($request->status == 'OK' && isset($start->results)  && is_array($start->results) && array_key_exists(0, $start->results) ) {
            $data = array(
                'from' => $from,
                'to' => $to,
                'start' => $start->results[0]->geometry->location,
				'end' => $end->results[0]->geometry->location,
                'distance' => $request->rows[0]->elements[0]->distance,
                'duration' => $request->rows[0]->elements[0]->duration,
            );

            $this->context->smarty->assign('an_map_data', $data);
            return $this->display(__FILE__, 'adminOrderContentShip.tpl');
        } else {
            if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_) {
/*                 echo('<pre>');
                var_dump($request);
                echo('</pre>'); */
            }
            return print_r($request, true);
        }

        return $this->l('Google API Error... Please check the Store address or delivery address');
    }

    /**
     * Load the Configuration form
     */
    public function getContent()
    {
   
		$this->bootstrap = true;
		
        $errors = '';
        $info = '';		
		
		$apis = array(
			'key' =>  array('name'=> 'key', 'status' => false),
			'DistanceMatrixAPI' =>  array('name'=> 'Distance Matrix API', 'status' => false),
			'GeocodingAPI' =>  array('name'=> 'Geocoding API', 'status' => false),
			'MapsJavaScriptAPI' =>  array('name'=> 'Maps Java Script API', 'status' => false),
			'PlacesAPI' =>  array('name'=> 'Places API', 'status' => false),	// Нужно для подсказок адресов
			'DirectionsAPI' =>  array('name'=> 'Directions API', 'status' => false), //	Нужно для отображения маршрута в заказа бэкофиса
		);
		
		/**
         * If values have been submitted in the form, process.
         */

        if (Tools::getIsset($this->name)) {
			if (_PS_MODE_DEMO_) {
				$errors .= $this->displayError($this->textDemoMode);
			}  else {
				$this->postProcess();
			}  			
        }

        $key = urlencode(Configuration::get('AN_GMAPS_KEY'));
		
        if (!$key) {
            $errors .= $this->displayError(
                $this->l('Google API Error... Please check the Google maps API key in module configuration')
            );
        } else {
            $from = urlencode('Praha');
            $to = urlencode('Berlin');
            $units = $this->getParam('DISTANCE_UNIT');

            $request = self::sendRequest(
                'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
                . $from . '&destinations=' . $to . '&output=json&units=' . $units . '&key=' . $key . '&sensor=false'
            );
		
			$msgOk = array();
			$msgError = array();

            if ($request->status == 'OK') {
                
				$info .= $this->displayConfirmation('Google API key is correct');
				$msgOk[] = $this->l('Distance Matrix API: Enabled');
				$msgOk[] = $this->l('Maps Java Script API: Enabled');
				
				$apis['key']['status'] = true;
				$apis['DistanceMatrixAPI']['status'] = true;
				$apis['MapsJavaScriptAPI']['status'] = true;
            } elseif (isset($request->error_message) && ($request->error_message == 'The provided API key is invalid.' | $request->error_message != 'This API project is not authorized to use this API.')) {
				$info .= $this->displayError($request->error_message);
			} else {
                if(isset($request->error_message)) {
                    $info .= $this->displayError(
                        $this->l('Google distancematrix API Error... Please check api key permissions. ') . $request->error_message
                    );
                } else {
                    $info .= $this->displayError(
                        $this->l('Google distancematrix API Error... Please check api key permissions. ')
                    );
                    $info .= $this->displayError(
                        print_r($request, true)
                    );
                }
            }
			
			
			// GeocodingAPI
			if ($apis['key']['status']){
				
				$start = self::sendRequest(
					'https://maps.googleapis.com/maps/api/geocode/json?address=' . $from
					. '&key=' . $key
				);
				
				if(isset($start->error_message)) {
					$info .= $this->displayError(
                        $this->l($apis['GeocodingAPI']['name'].': Disabled. ') . $start->error_message
                    );
				} else {
					$apis['GeocodingAPI']['status'] = true;
					$msgOk[] = $this->l($apis['GeocodingAPI']['name']. ': Enabled');
				}
						
				//	'DirectionsAPI' =>  array('name'=> 'Directions API', 'status' => false),
				$test = self::sendRequest(
					'https://maps.googleapis.com/maps/api/directions/json?origin=Praha&destination=Berlin&key=' . $key
				);
				if(isset($test->error_message)) {
					$info .= $this->displayError(
                        $this->l($apis['DirectionsAPI']['name'].': Disabled. ') . $test->error_message
                    );
				} else {
					$apis['DirectionsAPI']['status'] = true;
					$msgOk[] = $apis['DirectionsAPI']['name']. ': '.$this->l('Enabled');
				}

				if (count($msgOk) > 0){
					foreach ($msgOk as $itemMsg) {
						$info .= $this->displayConfirmation($itemMsg);
					}
				}					
				
				// PlacesAPI
				$text = $apis['PlacesAPI']['name']. ': An automated check is not possible. Please check manually. Enabling this option is necessary for auto-prompts when entering an address on the order page';
				if(method_exists($this, 'displayInformation')) {
					$info .= $this->displayInformation($text);
				} else {
					$info .= $this->displayWarning($text);
				}		

				
				
			}	

			//	Store address
			$ADDRESS_FROM = $this->getParam('ADDRESS_FROM');

			if (!$ADDRESS_FROM | $ADDRESS_FROM == ''){
				$info .= $this->displayError('Enter the store address. This is necessary for the module to work correctly.');
			}			
        }

		$this->context->smarty->assign('info', $info);
		
        return  $errors . $this->topNav() . $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the Configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->module = $this;
        $helper->submit_action = $this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (self::$configs as $key) {
            $helper->tpl_vars['fields_value'][$key] = self::getParam($key);
        }
        $helper->tpl_vars['fields_value']['AN_GMAPS_KEY'] = Configuration::get('AN_GMAPS_KEY');
        $helper->tpl_vars['fields_value']['add_new_carrier'] = '';
        $info = '';
        if (Configuration::get('AN_GMAPS_KEY') == '') {
            $info = $this->displayWarning($this->l('To specify the store address enter Google maps API key first.'));
        }	

        return $info . $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        if (Configuration::get('AN_GMAPS_KEY') == '') {
            $gmap = false;
            $start = false;
        } else {
            $gmap = true;
            if ($this->getParam('ADDRESS_FROM') != '') {
                $from = urlencode($this->getParam('ADDRESS_FROM'));
                $answer = self::sendRequest(
                    'https://maps.googleapis.com/maps/api/geocode/json?address=' . $from
                    . '&key=' . Configuration::get('AN_GMAPS_KEY')
                );
                if ($answer->status == 'OK') {
                    $start = $answer->results[0]->geometry->location;
                } else {
                    $start = false;
                }
            } else {
                $start = false;
            }
        }

        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('An Shipping By Distance Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
				
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google maps API key:'),
                        'name' => 'AN_GMAPS_KEY',
                        'require' => true,
                    ),				
				
                    array(
                        'type' => 'text',
                        'label' => $this->l('Store address:'),
                        'name' => 'ADDRESS_FROM',
                        'desc' => $this->l(
                            'This address will sending with the Google API queries (Start position of distance).'
                        ),
                        'require' => true,
                        'gmap' => $gmap,
                        'readonly' => !$gmap,
                        'start' => $start,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Store address2:'),
                        'name' => 'ADDRESS_FROM2',
                        'desc' => $this->l(
                            'This address will sending with the Google API queries (Start position of distance).'
                        ),
                        'require' => true,
                        'gmap' => $gmap,
                        'readonly' => !$gmap,
                        'start' => $start,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Store address3:'),
                        'name' => 'ADDRESS_FROM3',
                        'desc' => $this->l(
                            'This address will sending with the Google API queries (Start position of distance).'
                        ),
                        'require' => true,
                        'gmap' => $gmap,
                        'readonly' => !$gmap,    
                        'start' => $start,
                    ),  

                    array(
                        'type' => 'select',
                        'label' => $this->l('Unit of distance:'),
                        'name' => 'DISTANCE_UNIT',
                        'options' => array(
                            'query' => array(
                                array('id' => 'metric', 'name' => $this->l('Metric(meters)')),
                                array('id' => 'imperial', 'name' => $this->l('Imperial(yards)'))
                            ),
                            'id' => 'id',
                            'name' => 'name',
                        )
                    ),
					
                array(
                    'type' => 'switch',
                    'label' => $this->l('Use cache'),
                    'name' => 'CACHE',
                    'is_bool' => true,
					'desc' => $this->l('24h'),
                    'values' => array(
                        array(
                            'id' => 'is_enabled_on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'is_enabled_off',
                            'value' => 0
                        )
                    ),
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Log'),
                    'name' => 'ANSHIPDIS_LOG',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'is_enabled_on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'is_enabled_off',
                            'value' => 0
                        )
                    ),
                ),
					
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        return $form;
    }

    /**
     * @param bool $conf
     * @return string
     */
    public function getConfigureUrl($conf = false, $carrier_added = false)
    {
        return $this->context->link->getAdminLink('AdminModules')
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name
            .($conf ? '&conf=4' : '').($carrier_added ? '&carrier_added=1' : '');
    }

    public function addCarrier($name)
    {
        $carrier = new Carrier();
        $carrier->name = $name;
        $carrier->active = true;
        $carrier->deleted = 0;
        $carrier->shipping_handling = false;
        $carrier->range_behavior = 0;
        $carrier->delay[Configuration::get('PS_LANG_DEFAULT')] = $this->l('The shipping price calculates by shippingmatrix module');
        $carrier->shipping_external = true;
        $carrier->is_module = true;
        $carrier->external_module_name = $this->name;
        $carrier->need_range = true;

        $carrier->shipping_method = Carrier::SHIPPING_METHOD_WEIGHT;

        if ($carrier->add()) {
            $groups = Group::getGroups(true);
            foreach ($groups as $group) {
                Db::getInstance()->insert(
                    'carrier_group',
                    array(
                        'id_carrier' => (int) $carrier->id,
                        'id_group' => (int) $group['id_group'])
                );
            }

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '100000000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $z) {
                Db::getInstance()->insert(
                    'carrier_zone',
                    array('id_carrier' => (int) $carrier->id, 'id_zone' => (int) $z['id_zone'])
                );
                Db::getInstance()->insert(
                    'delivery',
                    array(
                        'id_carrier' => (int) $carrier->id,
                        'id_range_price' => null,
                        'id_range_weight' => (int) $rangeWeight->id,
                        'id_zone' => (int) $z['id_zone'],
                        'price' => '10'
                    ),
                    true
                );
            }

            @copy(dirname(__FILE__) . '/views/img/carrier.jpg', _PS_SHIP_IMG_DIR_ . '/' . (int) $carrier->id . '.jpg');
        }
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        foreach (self::$configs as $key) {
			if (Tools::getIsset($key)) {
				self::getParam($key, Tools::getValue($key));
			}
        }
		if (Tools::getIsset('AN_GMAPS_KEY')) {
			Configuration::updateValue('AN_GMAPS_KEY', Tools::getValue('AN_GMAPS_KEY'));
		}
        Tools::redirectAdmin($this->getConfigureUrl(true));
    }
	
	public function getIdCarrierByName($name, $carriers = array()){
		
		if (!is_array($carriers)){
			return false;
		}
		
		foreach ($carriers as $carrier) {
			if ($carrier['name'] == $name){
				return $carrier['id_reference'];
			}
		}
		return false;
	}	
	
    /**
     * Get all carriers in a given language.
     *
     * @param int  $id_lang         Language id
     * @param int  $modules_filters Possible values:
     *                              - PS_CARRIERS_ONLY
     *                              - CARRIERS_MODULE
     *                              - CARRIERS_MODULE_NEED_RANGE
     *                              - PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE
     *                              - ALL_CARRIERS
     * @param bool $active          Returns only active carriers when true
     *
     * @return array Carriers
     */
    public static function getOwnCarriers($id_lang, $active = false, $delete = false, $id_zone = false, $ids_group = null)
    {
        // Filter by groups and no groups => return empty array
        if ($ids_group && (!is_array($ids_group) || !count($ids_group))) {
            return array();
        }

        $sql = '
		SELECT c.*, cl.delay
		FROM `'._DB_PREFIX_.'carrier` c
		LEFT JOIN `'._DB_PREFIX_.'carrier_lang` cl ON (c.`id_carrier` = cl.`id_carrier` AND cl.`id_lang` = '.(int) $id_lang.Shop::addSqlRestrictionOnLang('cl').')
		LEFT JOIN `'._DB_PREFIX_.'carrier_zone` cz ON (cz.`id_carrier` = c.`id_carrier`)'.
            ($id_zone ? 'LEFT JOIN `'._DB_PREFIX_.'zone` z ON (z.`id_zone` = '.(int) $id_zone.')' : '').'
		'.Shop::addSqlAssociation('carrier', 'c').'
		WHERE c.`deleted` = '.($delete ? '1' : '0');
        if ($active) {
            $sql .= ' AND c.`active` = 1 ';
        }
        if ($id_zone) {
            $sql .= ' AND cz.`id_zone` = '.(int) $id_zone.' AND z.`active` = 1 ';
        }
        if ($ids_group) {
            $sql .= ' AND EXISTS (SELECT 1 FROM '._DB_PREFIX_.'carrier_group
									WHERE '._DB_PREFIX_.'carrier_group.id_carrier = c.id_carrier
									AND id_group IN ('.implode(',', array_map('intval', $ids_group)).')) ';
        }

        $sql .= ' AND c.is_module = 1 AND c.external_module_name = \'an_shippingbydistance\' ';

        $sql .= ' GROUP BY c.`id_carrier` ORDER BY c.`position` ASC';

        $cache_id = 'Carrier::getCarriers_'.md5($sql);
        if (!Cache::isStored($cache_id)) {
            $carriers = Db::getInstance()->executeS($sql);
            Cache::store($cache_id, $carriers);
        } else {
            $carriers = Cache::retrieve($cache_id);
        }
	//	echo '<pre>'; var_dump($carriers); die;
        foreach ($carriers as $key => $carrier) {
            if ($carrier['name'] == '0') {
                $carriers[$key]['name'] = Carrier::getCarrierNameFromShopName();
            }
/* 			if ($carrier['active']){
				$carriers[$key]['name'] .= ' (Enabled)';
			} else {
				$carriers[$key]['name'] .= ' (Disabled)';
			} */
        }

        return $carriers;
    }	
	
	public function topNav()
	{
	
	$this->context->smarty->assign('theme', $this->getUrlsSuggestions());
		
		$this->context->smarty->assign(array(
			'settingUrl' => $this->context->link->getAdminLink('AdminModules').'&configure=an_shippingbydistance',
			'costsUrl' => $this->context->link->getAdminLink('AdminAnShippingByDistance'),
			'AddCarrierUrl' => $this->context->link->getAdminLink('AdminAnshippingbydistanceCarrier'),
			'AddLogUrl' => $this->context->link->getAdminLink('AdminAnShippingByLog'),
			'addons_product_id' => $this->addons_product_id,
			'modulePath' => $this->_path,
		));
		
		return $this->display(__FILE__, 'views/templates/admin/top.tpl');
	}	
	
	public function getUrlsSuggestions()
	{
		$urlContactUs = 'https://addons.prestashop.com/contact-form.php';

		if (isset($this->url_contact_us) && $this->url_contact_us != ''){
			$urlContactUs = $this->url_contact_us;
		} elseif (isset($this->addons_product_id) && $this->addons_product_id != ''){
			$urlContactUs .= '?id_product=' .$this->addons_product_id;
		}
		
		$urls['url_contact_us'] = $urlContactUs;
					
		$urlRate = 'https://addons.prestashop.com/ratings.php';

		if (isset($this->url_rate) && $this->url_rate != ''){
			$urlRate = $this->url_rate;
		} elseif (isset($this->addons_product_id) && $this->addons_product_id != ''){
			$urlRate .= '?id_product=' .$this->addons_product_id;
		}
		
		$urls['url_rate'] = $urlRate;
				
		return $urls;
	}		
	
}
