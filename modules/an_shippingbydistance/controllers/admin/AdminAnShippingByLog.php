<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

require_once(_PS_MODULE_DIR_ . 'an_shippingbydistance/classes/AnShippingByDistanceLog.php');

class AdminAnShippingByLogController extends ModuleAdminController
{
    protected $position_identifier = 'id_log';
    
    protected $allow_export = true;
	
    protected $_defaultOrderBy = 'date_add';
    protected $_defaultOrderWay = 'DESC';	

    public function __construct()
    {
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->table = 'an_shippingbydistance_logs';
        $this->identifier = 'id_log';
        $this->className = 'AnShippingByDistanceLog';
        $this->lang = false;

		$this->addRowAction('delete');

        parent::__construct();
		$units = '';
/*         if ($this->module->getParam('DISTANCE_UNIT') == 'metric') {
            $units = ', '.$this->module->l('meters');
        } elseif ($this->module->getParam('DISTANCE_UNIT') == 'imperial') {
            $units = ', '.$this->module->l('yards');
        } */
		

        $this->fields_list = array(
            'id_log' => array('title' => $this->l('ID')),
            
			'date_add' => array('title' => $this->l('Date'), 'width' => 120),

            'from' => array('title' => $this->module->l('From'), 'width' => 220),
            'to' => array('title' => $this->module->l('To'), 'width' => 220),
            'distance_meter' => array('title' => $this->module->l('Distance_meter')),
            'distance_yard' => array('title' => $this->module->l('Distance_yard')),
            'price' => array('title' => $this->module->l('price')),
            'weight' => array('title' => $this->module->l('weight')),
            'cost_id' => array('title' => $this->module->l('Rule')),
            'cost_price' => array('title' => $this->module->l('Rule price')),

        );

    }
	
    public function renderList()
    {
        $info = $this->displayInformation($this->l('You can enable / disable logs in the settings'));			
		
		$this->context->smarty->assign(array(
			'AddLogUrl' => $this->context->link->getAdminLink('AdminAnShippingByLog'),
		));		
		
		$clearLogsBut = $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->module->name . '/views/templates/admin/top-log.tpl');
		return $info . $this->module->topNav() . $clearLogsBut . parent::renderList();
    }

    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        	
        foreach ($this->_list as &$list) {
			

			$list['from'] = str_replace('+', ' ', $list['from']);
			$list['to'] = str_replace('+', ' ', $list['to']);
			
			$list['price'] = Tools::displayPrice($list['price']);
			$list['cost_price'] = Tools::displayPrice($list['cost_price']);
				
				
        }
    }	

    public function initToolbar()
    {
        parent::initToolbar();
        if (array_key_exists('new', $this->toolbar_btn)) {
            unset($this->toolbar_btn['new']);
        }
    }
	
	public function postProcess(){
		
		if (Tools::getIsset('clearlogs')) {
			$this->module->logClear();
			$this->confirmations[] = $this->module->l('Done!');
		}
		
		return parent::postProcess();
	}	
}
