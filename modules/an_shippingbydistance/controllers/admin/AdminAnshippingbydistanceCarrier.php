<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

class AdminAnshippingbydistanceCarrierController extends ModuleAdminController
{
    protected $_module = null;
	
    protected static $configs = array(
        'ADDRESS_FROM',
        'ADDRESS_FROM2',   
        'ADDRESS_FROM3',
        'AN_GMAPS_KEY',
        'DISTANCE_UNIT',
		'CACHE',
    );


    public function __construct()
    {
        $this->bootstrap = true;
        $this->display = 'view';
 
		$this->name = 'AdminAnshippingbydistanceCarrierController';
		
        parent::__construct();
    }
	


    /**
     * Create the structure of your form.
     */
    protected function getConfigFormCarrier()
    {
        $form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('AN Shipping By Distance: Add Carrier'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Name'),
                        'name' => 'add_new_carrier',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Add'),
                ),
            ),
        );

        return $form;
    }






    public function renderView()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->name_controller = $this->name;
        $helper->submit_action = $this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminAnshippingbydistanceCarrier', false);
        $helper->token = Tools::getAdminTokenLite('AdminAnshippingbydistanceCarrier');
		$helper->tpl_vars['fields_value']['add_new_carrier'] = '';
		
		$info = '';
		
        $carrierInfo = $this->l('If you want to edit or delete carriers, please open: Shipping / Carriers');
        if(method_exists($this, 'displayInformation')) {
           $this->displayInformation($carrierInfo);
        } else {
           $this->displayWarning($carrierInfo);
        }			
		
        if (Tools::getIsset($this->name)) {
            if ($this->postProcess()){
				$info .= $this->module->displayConfirmation($this->l('Carrier added.'));
			}
			
        }		
		
        return $info . $this->module->topNav() . $helper->generateForm(array($this->getConfigFormCarrier()));		
	}

    /**
     * Save form data.
     */
    public function postProcess()
    {		
        if(Tools::getIsset('add_new_carrier') && Tools::getValue('add_new_carrier') != '') {
			
			if (_PS_MODE_DEMO_) {
				$this->errors[] = $this->module->textDemoMode;
				return false;
			}
			
            $this->module->addCarrier(Tools::getValue('add_new_carrier'));
			
			$currentIndex = $this->context->link->getAdminLink('AdminAnshippingbydistanceCarrier', false);
			$token = Tools::getAdminTokenLite('AdminAnshippingbydistanceCarrier');		
			
			Tools::redirectAdmin($currentIndex.'&token='.$token.'&conf=4'); 
	
			return true;
        }		
		
		return  false;
    }
	
	
	
}
