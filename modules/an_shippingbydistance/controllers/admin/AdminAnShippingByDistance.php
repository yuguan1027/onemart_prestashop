<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

require_once(_PS_MODULE_DIR_ . 'an_shippingbydistance/classes/AnShippingByDistance.php');
require_once(_PS_MODULE_DIR_ . 'an_shippingbydistance/lib/Csv.php');

class AdminAnShippingByDistanceController extends ModuleAdminController
{
    protected $position_identifier = 'id_an_shippingbydistance';
    
    protected $allow_export = true;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->table = 'an_shippingbydistance';
        $this->identifier = 'id_an_shippingbydistance';
        $this->className = 'AnShippingByDistance';
        $this->lang = false;

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        parent::__construct();
		$units = '';
/*         if ($this->module->getParam('DISTANCE_UNIT') == 'metric') {
            $units = ', '.$this->module->l('meters');
        } elseif ($this->module->getParam('DISTANCE_UNIT') == 'imperial') {
            $units = ', '.$this->module->l('yards');
        } */
		

        $this->fields_list = array(
            'id_an_shippingbydistance' => array('title' => $this->l('ID'), 'width' => 25),
            'carrier_id_reference' => array(
                'title' => $this->module->l('Carrier'),
                'desc' => $this->module->l('Select a carrier from the list. Open Shipping -> Carriers to manage carriers')
            ),
            'distance_from' => array(
                'title' => $this->l('Distance From') . ' (' . $this->module->getParam('DISTANCE_UNIT') . ')'.$units
            ),
            'distance_to' => array(
                'title' => $this->l('Distance To') . ' (' . $this->module->getParam('DISTANCE_UNIT') . ')'.$units
            ),
            'price_from' => array('title' => $this->module->l(
                'Price From'
            ) . ', ' . $this->context->currency->sign,
                'type' => 'price'
            ),
            'price_to' => array('title' => $this->module->l(
                'Price To'
            ) . ', ' . $this->context->currency->sign,
                'type' => 'price'
            ),
            'weight_from' => array('title' => $this->module->l('Weight From, kg.')),
            'weight_to' => array('title' => $this->module->l('Weight To, kg.')),
            'shipping_price' => array('title' => $this->module->l(
                'Shipping Price'
            ) . ', ' . $this->context->currency->sign,
                'type' => 'price'
            ),
        //    'price_per_km' => array('title' => $this->module->l('Shipping price per km')),
        );

    }
	
    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        
		$carriers = an_shippingbydistance::getOwnCarriers($this->context->language->id);
	
        foreach ($this->_list as &$list) {
			
			if ($list['carrier_id_reference'] != 0){
				foreach ($carriers as $carrier) {
					if ($carrier['id_reference'] == $list['carrier_id_reference']){
						$list['carrier_id_reference'] = $carrier['name'];
					}
				}
			} else {
				$list['carrier_id_reference'] = '';
			}
			
			if ($list['price_per_km']){
			   //BY THOR 11/3/2021, Prestashop unable to handle this line
				//$list['shipping_price'] = Tools::displayPrice($list['shipping_price']) .' per km.';
  
			}
 			
			if ($list['shipping_price'] == '0'){
         //BY THOR 11/3/2021, Prestashop unable to handle this line
				//$list['shipping_price'] = 'free';
			}
			
			
        }
    }		

    public function renderForm()
    {
        $this->display = 'edit';
        $this->initToolbar();
        if (!$this->loadObject(true)) {
            return;
        }
		
        if ($this->module->getParam('DISTANCE_UNIT') == 'metric') {
            $units = ', '.$this->module->l('meters');
        } elseif ($this->module->getParam('DISTANCE_UNIT') == 'imperial') {
            $units = ', '.$this->module->l('yards');
        } else {
            $units = '';
        }		

        $this->fields_form = array(
            'tinymce' => false,
            'legend' => array('title' => $this->module->l('Shipping Rule')),
            'input' => array(),
            'submit' => array('title' => $this->module->l('Save'))
        );
		
		$this->fields_form['input'][] = array(
			'type' => 'select',
			'label' => $this->module->l('Carrier'),
			'required' => true,
			'lang' => false,
			'name' => 'carrier_id_reference',
			'options' => array(
				'query' => array_merge(
					array(array('id_reference' => '0', 'name' => '*')),
					$this->module->getOwnCarriers($this->context->language->id)
				),
				'id' => 'id_reference',
				'name' => 'name',
			)
		);
		
		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->l('Distance From') . ' (' . $this->module->getParam('DISTANCE_UNIT') . ')'.$units,
			'lang' => false,
			'name' => 'distance_from',
			'col' => 3,
		);		
		
		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->l('Distance To') . ' (' . $this->module->getParam('DISTANCE_UNIT') . ')'.$units,
			'lang' => false,
			'name' => 'distance_to',
			'col' => 3,
		);

		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->module->l('Price From') . ', ' . $this->context->currency->iso_code,
			'lang' => false,
			'name' => 'price_from',
			'col' => 3,
		);

		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->module->l('Price To') . ', ' . $this->context->currency->iso_code,
			'lang' => false,
			'name' => 'price_to',
			'col' => 3,
		);

		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->module->l('Weight From, kilograms'),
			'lang' => false,
			'name' => 'weight_from',
			'col' => 3,
		);	

		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->module->l('Weight To, kilograms'),
			'lang' => false,
			'name' => 'weight_to',
			'col' => 3,
		);	

		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->module->l('Shipping Price') . ', ' . $this->context->currency->iso_code,
			'lang' => false,
			'name' => 'shipping_price',
			'col' => 3,
		);

        $this->fields_form['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Shipping price per km'),
            'name' => 'price_per_km',
            'values' => array(
                array(
                    'id' => 'price_per_km_1',
                    'value' => 1,
                    'label' => $this->l('Yes')),
                array(
                    'id' => 'price_per_km_0',
                    'value' => 0,
                    'label' => $this->l('No')),
            ),
        );
/* 		
		$this->fields_form['input'][] = array(
			'type' => 'text',
			'label' => $this->module->l('Formula'),
			'lang' => false,
			'name' => 'formula',
		);		
 */
 
        return $this->module->topNav() . parent::renderForm();
    }

    public function processExport($text_delimiter = '"')
    {
        unset($this->fields_list['id_an_shippingbydistance']);
        parent::processExport($text_delimiter);
    }

    public function renderList()
    {
        return $this->module->topNav() . parent::renderList() . $this->module->getImportForm();
    }

    public function postProcess()
    {
        if (isset($_FILES['an_import']) && $_FILES['an_import']['size'] > 0) {
			
			if (_PS_MODE_DEMO_) {
				$this->errors[] = $this->module->textDemoMode;
				return false;
			}
			
            $csv = new Varien_File_Csv();
            $csv->setDelimiter(Tools::getValue('an_delimiter', ';'));
            $csv->setEnclosure(Tools::getValue('an_enclosure', '"'));

            $data = $csv->getData($_FILES['an_import']['tmp_name']);
            if (Tools::substr($_FILES['an_import']['name'], -4) != '.csv') {
                $this->errors[] = $this->module->l(
                    'Please check the file you are trying to upload. The format is required to be csv.'
                );
            } elseif (count($data) > 1) {
                unset($data[0]);
                Db::getInstance()->Execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'an_shippingbydistance`');

                foreach ($data as $key => $line) {
                    if (count($line) >= 7) {
                        $rate = new AnShippingByDistance();
                        $rate->carrier_name = $line[0];
                        $rate->distance_from = (float)$line[1];
                        $rate->distance_to = (float)$line[2];
                        $rate->price_from = (float)$line[3];
                        $rate->price_to = (float)$line[4];
                        $rate->weight_from = (float)$line[5];
                        $rate->weight_to = (float)$line[6];
                        $rate->shipping_price = (float)$line[7];
                        $rate->price_per_km = (int)$line[8];
                        $rate->save();
                    } else {
                        $this->errors[] = $this->module->l('Bad line #' . ($key + 1) . ' in the CSV file!');
                    }
                }
            } else {
                $this->errors[] = $this->module->l('Empty CSV file!');
            }
        }

        if (Tools::getIsset('carrier_id_reference')) {
            $carrier_id_reference = Tools::getValue('carrier_id_reference');
		if ($carrier_id_reference == '*' | $carrier_id_reference == '0' ) {
                $this->errors[] = $this->module->l('Please select a Carrier.');
            }
        }

        return parent::postProcess();
    }

}
