<?php
/**
 * 2020 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2020 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
function upgrade_module_2_1_0($object)
{
    $sql = array();

    $sql[] = '
      ALTER TABLE `' . _DB_PREFIX_ . 'an_shippingbydistance`
        ADD `carrier_id_reference` INT DEFAULT "0" AFTER `id_an_shippingbydistance`
    ';

    $return = true;
    foreach ($sql as $_sql) {
        $return = Db::getInstance()->Execute($_sql);
		if (!$return){
			return false;
		}
    }
		


 	$sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'an_shippingbydistance`';
	$rates = Db::getInstance()->ExecuteS($sql);
	
	$carriers = an_shippingbydistance::getOwnCarriers(Context::getContext()->language->id);
	foreach ($rates as $rate){
 		$carrierId = $object->getIdCarrierByName($rate['carrier_name'], $carriers);
		if ($carrierId){
			$sql = 'UPDATE `' . _DB_PREFIX_ . 'an_shippingbydistance` SET `carrier_id_reference` = '.(int)$carrierId.' WHERE `id_an_shippingbydistance` = '.(int)$rate['id_an_shippingbydistance'];
			$return = Db::getInstance()->Execute($sql);
			if (!$return){
				return false;
			}			
		}  
	
	}

    return true;
}
