<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */
 
if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_2_0_0($object, $install = false)
{
    $return = true;

    $field = Db::getInstance()->executeS('DESCRIBE `'._DB_PREFIX_.'an_shippingbydistance` `price_per_km`');

    if(!is_array($field) || !count($field)) {
        $return &= Db::getInstance()->Execute(
            'ALTER TABLE `'._DB_PREFIX_.'an_shippingbydistance` ADD `carrier_name` varchar(255) NOT NULL, DEFAULT "Shipping by distance 1"'
        );
    }

    $base_name_iterator = 1;
    foreach ($object->own_carriers_basic_names as $carrier_name) {
        $carrier = new Carrier();
        $carrier->name = $carrier_name;
        $carrier->active = false;
        $carrier->deleted = 0;
        $carrier->shipping_handling = false;
        $carrier->range_behavior = 0;
        $carrier->delay[Configuration::get('PS_LANG_DEFAULT')] = $object->l('The shipping price calculates by shippingmatrix module');
        $carrier->shipping_external = true;
        $carrier->is_module = true;
        $carrier->external_module_name = $object->name;
        $carrier->need_range = true;

        $carrier->shipping_method = Carrier::SHIPPING_METHOD_WEIGHT;

        $return &= $carrier->add();
        if ($return) {
            $base_name_iterator++;
            $groups = Group::getGroups(true);
            foreach ($groups as $group) {
                Db::getInstance()->insert(
                    'carrier_group',
                    array(
                        'id_carrier' => (int) $carrier->id,
                        'id_group' => (int) $group['id_group'])
                );
            }

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '100000000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $z) {
                Db::getInstance()->insert(
                    'carrier_zone',
                    array('id_carrier' => (int) $carrier->id, 'id_zone' => (int) $z['id_zone'])
                );
                Db::getInstance()->insert(
                    'delivery',
                    array(
                        'id_carrier' => (int) $carrier->id,
                        'id_range_price' => null,
                        'id_range_weight' => (int) $rangeWeight->id,
                        'id_zone' => (int) $z['id_zone'],
                        'price' => '10'
                    ),
                    true
                );
            }

            @copy(dirname(__FILE__) . '/views/img/carrier.jpg', _PS_SHIP_IMG_DIR_ . '/' . (int) $carrier->id . '.jpg');
        }
    }

    $return &= $object->uninstall(true);
    $return &= $object->install(true);
    return $return;
}
?>