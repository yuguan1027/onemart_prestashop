<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */
 
if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_2_3_0($object, $install = false)
{
    $sql = array();
	
            $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'an_shippingbydistance` (
		            `id_an_shippingbydistance` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `carrier_id_reference` int(11) DEFAULT "0",
		            `distance_from` int(11) DEFAULT "0",
		            `distance_to` int(11) DEFAULT "0",
		            `price_from` decimal(20,6) DEFAULT "0",
		            `price_to` decimal(20,6) DEFAULT "0",
		            `weight_from` decimal(20,6) DEFAULT "0",
		            `weight_to` decimal(20,6) DEFAULT "0",
		            `shipping_price` decimal(20,6) NOT NULL,
		            `price_per_km` int(1) DEFAULT "0",
		            `formula`  varchar(255) NOT NULL,
		            PRIMARY KEY (`id_an_shippingbydistance`)
		        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

    $return = true;
    foreach ($sql as $_sql) {
        $return = Db::getInstance()->Execute($_sql);
		if (!$return){
			return false;
		}
    }

    return true;
}
?>