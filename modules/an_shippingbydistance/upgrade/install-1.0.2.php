<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */
 
if (!defined('_PS_VERSION_'))
    exit;

function upgrade_module_1_0_2($object, $install = false)
{
    $result = true;

    $field = Db::getInstance()->executeS('DESCRIBE `'._DB_PREFIX_.'an_shippingbydistance` `price_per_km`');

    if(!is_array($field) || !count($field)) {
        $result &= Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'an_shippingbydistance` ADD `price_per_km` int(1) DEFAULT "0"');
    }

    return $result;
}
?>