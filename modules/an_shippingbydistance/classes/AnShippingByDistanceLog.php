<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

class AnShippingByDistanceLog extends ObjectModel
{
                       
    public $id;
    public $id_log;

	public $from;
    public $to;
    public $units;
    public $distance_meter = 0;
    public $distance_yard = 0;
    public $price = 0;
    public $weight = 0;
    public $cost_id = 0;
    public $cost_price;
	
    /** @var string Object creation date */
    public $date_add;

    public static $definition = array(
        'table' => 'an_shippingbydistance_logs',
        'primary' => 'id_log',
        'multilang' => false,
        'fields' => array(
			'from' =>    array('type' => self::TYPE_STRING),
            'to' =>  array('type' => self::TYPE_STRING),
            'units' =>    array('type' => self::TYPE_STRING),
			
            'distance_meter' =>     array('type' => self::TYPE_INT),
            'distance_yard' =>       array('type' => self::TYPE_INT),
			
            'price' =>    array('type' => self::TYPE_FLOAT),
            'weight' =>      array('type' => self::TYPE_INT),
            'cost_id' =>      array('type' => self::TYPE_INT),
			
            'cost_price' => array('type' => self::TYPE_FLOAT),
			
			'date_add' => array('type' => self::TYPE_DATE),

        ),
    );
}
