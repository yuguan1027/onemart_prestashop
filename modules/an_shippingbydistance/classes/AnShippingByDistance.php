<?php
/**
 * 2021 Anvanto
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 *  @author Anvanto <anvantoco@gmail.com>
 *  @copyright  2021 Anvanto
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of Anvanto
 */

class AnShippingByDistance extends ObjectModel
{
                       
    public $id;
    public $id_an_shippingbydistance;
//    public $carrier_name;
	public $carrier_id_reference;
    public $distance_from;
    public $distance_to;
    public $price_from = 0;
    public $price_to = 0;
    public $weight_from = 0;
    public $weight_to = 0;
    public $shipping_price;
    public $price_per_km;
    public $formula;

    public static $definition = array(
        'table' => 'an_shippingbydistance',
        'primary' => 'id_an_shippingbydistance',
        'multilang' => false,
        'fields' => array(
        //    'carrier_name' =>   array('type' => self::TYPE_STRING, 'validate' => 'isCarrierName'),
			'carrier_id_reference' =>    array('type' => self::TYPE_INT),
            'distance_from' =>  array('type' => self::TYPE_INT),
            'distance_to' =>    array('type' => self::TYPE_INT),
            'price_from' =>     array('type' => self::TYPE_FLOAT),
            'price_to' =>       array('type' => self::TYPE_FLOAT),
            'weight_from' =>    array('type' => self::TYPE_FLOAT),
            'weight_to' =>      array('type' => self::TYPE_FLOAT),
            'shipping_price' => array('type' => self::TYPE_FLOAT),
            'price_per_km' => array('type' => self::TYPE_INT),
			'formula' => array('type' => self::TYPE_STRING),
        ),
    );
}
