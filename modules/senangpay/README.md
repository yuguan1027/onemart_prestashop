SENANGPAY [NEW PS 1.7+ VERSION]
=========

Prestashop Addons Development by Mimpro Enterprise (Malaysia Prestashop Certified Agency)
This module is for payment module, and for the purpose of Prestashop User 
Distribution of this plugin is via Prestashop Addons

Contact MIMPRO ENTERPRISE for any Prestashop Help, Development and Addons Module / Theme Integration

*DUDY | mimpro.my@gmail.com



SHORT DESCRIPTION
-----------------
This module is for senangPay autopayment, which will enable securepayment directly to your Prestashop and verify the upon successful payment to update order status. Therefore payment is automated and you don't need to publish your bank account number for manual payment. Payment is secured using senangPay and minimal fee scheme is applied to senangPay merchant account.

You will benefit with the facility of Credit/Debit card and FPX (local online payment) method with this senangPay Prestashop module.



THE BENEFITS FOR MERCHANTS
--------------------------
Merchants will have autopayment facility with this senangPay Prestashop Module. It has integrated with API for high security and payment is executed in Safe Secured Layer (SSL / HTTPS://) mode to ensure no fraud in the execution.

Your seller will have less process in purchasing and finish transaction in just few clicks. Successful payment will update order status to Payment Accepted, it will automatically verify the payment into your Prestashop backend. 

No manual transfer is required in this module.

Payment will be available to your account at very minimal time, and low transaction fee, lowest annual fee is applied. Boost your marketing and selling with automated senangpay Prestashop module for easier and more joy on user experience through your Prestashop.

This module is built by MIMPRO, a dedicated and highly skilled Prestashop Agency partner and will able to support any other integration you required.



FEATURES 
--------
Module completed with Bahasa Melayu and English translation set, and able to add more languages upon request.

Easier experience with senangPay gateway registration. No hassle with unnecessary rules, pay less and benefit more.

Very fast pay up to your account, with guarantee less than 1 week and fixed, low transaction fee of RM1.50 per FPX transaction only. No limits on how big the transaction to enjoy this low rate.

FPX transaction is the ability to receive payment from any local banks, and you don't need to have account registered on all banks, the FPX will arrange the fund from any banks to your bank by senangPay.

Autopayment execution means, will do smart verification upon successful payment and update the order status, so both customers and merchant will shorten the order process, and no fraud payment guaranteed.



THE BENEFITS FOR CUSTOMERS
--------------------------
Customers are able to use Creditcard / Debitcard / Visa / Mastercard, and also FPX payment to pay from any local bank online transaction interface regardless you have that particular account registered on such banks. 

Customers will get verified automatically on each successful payment and orders updated automatically. If the purchase is for virtual product / online bookings / ticketing system, the product / booking confirmation is executed and transfered to buyer in only seconds after the payment.

Safe and secure payment, within Safe Secured Layer sessions. Customers data on creditcard / login is safe.

Payment execution just take few clicks to finish. Easier and faster ever for customer to purchase. No need to copy account number or key in product price manually. No need to print or provide screenshot /snapshot on receipts.



INSTALLATION
------------
Just install and put your data of senangPay accounts. You will be provided with links to API payment page to update (just copy and paste) into your senangPay merchant settings page. 

Your shop need to use Friendly URL for this module to respond with confirmation from senangPay.

Not registered yet with senangPay? Sure you can contact senangPay or register online. Registration will be very easy.

go to https:/senangpay.my for online registration



RECOMMENDATIONS
---------------
Your shop need to be either Prestashop 1.5+ or 1.6+ (up to latest version).

Registered with senangPay. You may register later but to complete the installation setup you will need the Merchand ID and secret code, which you only get upon senangPay account registration.


CALLBACK
--------

You need to configure your senangPay with the displayed value of Return URL, Return URL Parameters, and Callback URL
Return URL is for redirection after payment, so the verification of order and updating to your site will take place immediately. Meanwhile for unexpected events like internet connection is down or weak, or browser freezes or maybe also accidentally closed the current browser, which there will be no chance for redirection event updating the order immediately.

CALLBACK is a function where it will reissue payment verification to your site, by server-to-server protocol within 30 min to 1 hour so if the order is not yet updated, this CALLBACK will make sure this without having to interrupt manually in the Prestashop Admin section.


OTHER
-----
The default currency used via senangPay is Ringgit Malaysia. If you offer multicurrency shop, other currency will be asked to be converted to Ringgit Malaysia by dialog box for customer to confirm agree to use Ringgit Malaysia with your currency exchange rate settings in your Prestashop.


