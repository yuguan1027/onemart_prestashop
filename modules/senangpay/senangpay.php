<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

class SenangPay extends PaymentModule
{
    public function __construct()
    {
        $this->name      = 'senangpay';
        $this->tab       = 'payments_gateways';
        $this->version   = '1.0.4';
        $this->author    = 'Mimpro';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('senangPay');
        $this->description = $this->l('Malaysia Online Payment Gateway');
        $this->module_key = 'c5d62cd5e95ea08744d4d73b83a266e8';
        $this->ps_versions_compliancy = array(
            'min' => '1.7',
            'max' => _PS_VERSION_
        );
    }
    public function install()
    {
        $sqlval     = 'SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page= \'module-senangpay-validationapi\'';
        $metaidvapi = Db::getInstance()->getValue($sqlval);
        if (!$metaidvapi) {
            Db::getInstance()->insert('meta', array(
                'page' => 'module-senangpay-validationapi'
            ), true);
            foreach (Language::getLanguages() as $language) {
                Db::getInstance()->insert('meta_lang', array(
                    'id_meta' => Db::getInstance()->getValue($sqlval),
                    'id_lang' => (int) $language['id_lang'],
                    'title' => 'Validation API',
                    'keywords' => '',
                    'url_rewrite' => 'validation'
                ));
            } //Language::getLanguages() as $language
        } //!$metaidvapi
        $sqlerror      = 'SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page= \'module-senangpay-paymenterror\'';
        $metaidpayerro = Db::getInstance()->getValue($sqlerror);
        if (!$metaidpayerro) {
            Db::getInstance()->insert('meta', array(
                'page' => 'module-senangpay-paymenterror'
            ), true);
            foreach (Language::getLanguages() as $language) {
                Db::getInstance()->insert('meta_lang', array(
                    'id_meta' => Db::getInstance()->getValue($sqlerror),
                    'id_lang' => (int) $language['id_lang'],
                    'title' => 'Payment Error',
                    'keywords' => '',
                    'url_rewrite' => 'payment-error'
                ));
            } //Language::getLanguages() as $language
        } //!$metaidpayerro
        $sqlpayx    = 'SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page= \'module-senangpay-paymentnotmatched\'';
        $metaidpayx = Db::getInstance()->getValue($sqlpayx);
        if (!$metaidpayx) {
            Db::getInstance()->insert('meta', array(
                'page' => 'module-senangpay-paymentnotmatched'
            ), true);
            foreach (Language::getLanguages() as $language) {
                Db::getInstance()->insert('meta_lang', array(
                    'id_meta' => Db::getInstance()->getValue($sqlpayx),
                    'id_lang' => (int) $language['id_lang'],
                    'title' => 'Payment Not Matched',
                    'keywords' => '',
                    'url_rewrite' => 'payment-not-matched'
                ));
            } //Language::getLanguages() as $language
        } //!$metaidpayx
        $sqlsucc    = 'SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page= \'module-senangpay-successfulpayment\'';
        $metaidsucc = Db::getInstance()->getValue($sqlsucc);
        if (!$metaidsucc) {
            Db::getInstance()->insert('meta', array(
                'page' => 'module-senangpay-successfulpayment'
            ), true);
            foreach (Language::getLanguages() as $language) {
                Db::getInstance()->insert('meta_lang', array(
                    'id_meta' => Db::getInstance()->getValue($sqlsucc),
                    'id_lang' => (int) $language['id_lang'],
                    'title' => 'Successful Payment',
                    'keywords' => '',
                    'url_rewrite' => 'successful-payment'
                ));
            } //Language::getLanguages() as $language
        } //!$metaidsucc
        $sqlcallb    = 'SELECT `id_meta` FROM `'._DB_PREFIX_.'meta` WHERE page= \'module-senangpay-callback\'';
        $metaidcallb = Db::getInstance()->getValue($sqlcallb);
        if (!$metaidcallb) {
            Db::getInstance()->insert('meta', array(
                'page' => 'module-senangpay-callback'
            ), true);
            foreach (Language::getLanguages() as $language) {
                Db::getInstance()->insert('meta_lang', array(
                    'id_meta' => Db::getInstance()->getValue($sqlcallb),
                    'id_lang' => (int) $language['id_lang'],
                    'title' => 'Callback',
                    'keywords' => '',
                    'url_rewrite' => 'callback'
                ));
            } //Language::getLanguages() as $language
        } //!$metaidcallb

        if (!parent::install()
            || !$this->registerHook('paymentOptions')
            || !$this->registerHook('paymentReturn')
            || !$this->registerHook('header')) {
            return false;
        }
        if (!$this->installOrderState()) {
            return false;
        }
        return true;
    }


    public function installOrderState()
    {
        if (Configuration::get('PS_OS_SENANGPAY') < 1) {
            $order_state              = new OrderState();
            $order_state->send_email  = false;
            $order_state->module_name = $this->name;
            $order_state->invoice     = false;
            $order_state->color       = '#98c3ff';
            $order_state->logable     = true;
            $order_state->shipped     = false;
            $order_state->unremovable = false;
            $order_state->delivery    = false;
            $order_state->hidden      = false;
            $order_state->paid        = false;
            $order_state->deleted     = false;
            $order_state->name        = array(
                (int) Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('senangPay - Awaiting confirmation'))
            );
            $order_state->template    = array();
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_SENANGPAY', $order_state->id);
                copy(dirname(__FILE__).'/logo.gif', dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
                copy(
                    dirname(__FILE__).
                    '/logo.gif',
                    dirname(__FILE__)
                    .'/../../img/tmp/order_state_mini_'
                    .$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }
        return true;
    }

    public function hookHeader()
    {
        return $this->context->controller->addCSS($this->_path . 'views/css/senangpay.css');
    }


    public function getHookController($hook_name)
    {
        require_once(dirname(__FILE__).'/controllers/hook/'.$hook_name.'.php');
        $controller_name = $this->name.$hook_name.'Controller';
        $controller      = new $controller_name($this, __FILE__, $this->_path);
        return $controller;
    }

    public function hookPaymentOptions($params)
    {
        $controller = $this->getHookController('paymentOptions');
        return $controller->run($params);
    }

    public function hookPaymentReturn($params)
    {
        if ($this->active == false) {
            return;
        }

        $tx_msg = str_replace("_", " ", $_REQUEST['tx_msg']);

        $this->smarty->assign(array(
            'tx_id' => $_REQUEST['tx_id'],
            'tx_msg' => $tx_msg,
        ));


        $state = $params[ 'order' ]->getCurrentState();
        if (in_array($state, array(
            Configuration::get('PS_OS_SENANGPAY'),
            Configuration::get('PS_OS_OUTOFSTOCK'),
            Configuration::get('PS_OS_OUTOFSTOCK_UNPAID')
        ))) {
            $this->smarty->assign(array(
                'note' => $_REQUEST['module_id'],
                'total' => Tools::displayPrice(
                    $params[ 'order' ]->getOrdersTotalPaid(),
                    new Currency($params[ 'order' ]->id_currency),
                    false
                ),
                'reference' => $params[ 'order' ]->reference,
                'contact_url' => $this->context->link->getPageLink('contact', true)
            ));
        } else {
            $this->smarty->assign(array(
                'status' => 'failed',
                'contact_url' => $this->context->link->getPageLink('contact', true)
            ));
        }

        return $this->fetch('module:senangpay/views/templates/hook/paid.tpl');
    }

    public function getContent()
    {
        $controller = $this->getHookController('getContent');
        return $controller->run();
    }
}
