<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

class SenangPayPaymentOptionsController
{
    public function __construct($module, $file, $path)
    {
        $this->file    = $file;
        $this->module  = $module;
        $this->context = Context::getContext();
        $this->_path   = $path;
    }

    public function run()
    {

        $customer         = new Customer((int) $this->context->cart->id_customer);
        $custname         = $customer->firstname . ' ' . $customer->lastname;
        $custemail        = str_replace(" ", "&nbsp;", $customer->email);
        $address          = new Address((int) $this->context->cart->id_address_invoice);
        $custphone        = str_replace(" ", "&nbsp;", $address->phone);
        $senangpay_ID     = Configuration::get('SENANGPAY_API_ID');
        $senangpay_SECRET = Configuration::get('SENANGPAY_API_SECRET');
        $customer_ORDER   = $this->context->cart->id;
        $detail           = "CART_NO_" . $customer_ORDER;
        $total_to_PAY     = number_format((float) $this->context->cart->getOrderTotal(true, Cart::BOTH), 2, '.', '');
        $currency         = new Currency((int) $this->context->cart->id_currency);
        $iso_currency     = $currency->iso_code_num;
        $before_conv      = '';
        $changecartcurrency = '';
        $oldcurrency = '';
        $rmcurrency_id    = Currency::getIdByIsoCode('MYR', $this->context->cart->id_shop);
        $this->myr_id = $rmcurrency_id ;
        if (!$rmcurrency_id) {
            return false;
        }


        if ($iso_currency != 458) {
             return $this->alternative();
             /*
            $changecartcurrency = 'must';
            $oldcurrency     = Context::getContext()->currency;
            $rmcurrency_inst = Currency::getCurrencyInstance($rmcurrency_id);
            $before_conv     = $total_to_PAY;
            $before_conv     = $name_currency . ' ' . $before_conv;
            $total_to_PAY    = Tools::convertPriceFull($total_to_PAY, $oldcurrency, $rmcurrency_inst);
            $total_to_PAY    = number_format($total_to_PAY, 2, '.', '');
            */
        }
        $concat_prehash = $senangpay_SECRET . $detail . $total_to_PAY . $customer_ORDER;
        $senangpay_HASH = md5($concat_prehash);
        $post_args      = array(
            'detail' => $detail,
            'amount' => $total_to_PAY,
            'order_id' => $customer_ORDER,
            'hash' => $senangpay_HASH,
            'name' => $custname,
            'email' => $custemail,
            'phone' => $custphone
        );
        $senangpay_args = '';
        foreach ($post_args as $key => $value) {
            $senangpay_args .= "&";
            $senangpay_args .= $key . "=" . urlencode($value);
        }
        $senangpay_SEND_PARAM = $senangpay_args;
        $api_url_general      = 'https://sandbox.senangpay.my/payment/' . $senangpay_ID;
        $api_url              = $api_url_general . '?' . $senangpay_SEND_PARAM;

        $this->context->smarty->assign(array(
            'total_to_pay' => $total_to_PAY,
            'id_cart' => $customer_ORDER,
            'payment_token' => $senangpay_HASH,
            'detail' => $detail,
            'name' => $custname,
            'email' => $custemail,
            'phone' => $custphone,
            'foreign' => $before_conv,
            'change_cart_currency' => $changecartcurrency,
            'rmcurrency_id' => $rmcurrency_id,
            'oldcurrency' => $oldcurrency,
        ));

        $senangPay = new PaymentOption();
        $senangPay->setModuleName('senangpay')
                ->setCallToActionText('senangPay [ VISA / MASTERCARD / FPX ]')
                ->setAction($api_url)
                ->setAdditionalInformation($this->module->fetch('module:senangpay/views/templates/hook/intro.tpl'));

        $payment_options = array($senangPay,);

        return $payment_options;
    }

    public function alternative()
    {
        $senangPay = new PaymentOption();
        $senangPay->setModuleName('senangpay')
                ->setCallToActionText('senangPay [ VISA / MASTERCARD / FPX ]')
                ->setAction('index.php?controller=order&amp;SubmitCurrency=1&amp;id_currency=' . $this->myr_id)
                ->setAdditionalInformation(
                    $this->module->fetch('module:senangpay/views/templates/hook/alternative.tpl')
                );
        $payment_options = array($senangPay,);

        return $payment_options;
    }
}
