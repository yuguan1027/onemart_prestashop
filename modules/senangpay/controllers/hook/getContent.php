<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

class SenangpayGetContentController
{
    public function __construct($module, $file, $path)
    {
        $this->file    = $file;
        $this->module  = $module;
        $this->context = Context::getContext();
        $this->_path   = $path;
    }
    public function processConfiguration()
    {
        if (Tools::isSubmit('senangpay_form')) {
            Configuration::updateValue('SENANGPAY_API_ID', Tools::getValue('SENANGPAY_API_ID'));
            Configuration::updateValue('SENANGPAY_API_SECRET', Tools::getValue('SENANGPAY_API_SECRET'));
            $this->context->smarty->assign('confirmation', 'ok');
        }
    }
    public function renderForm()
    {
        $inputs = array(
            array(
                'name' => 'SENANGPAY_API_ID',
                'label' => $this->module->l('senangPay API ID'),
                'type' => 'text'
            ),
            array(
                'name' => 'SENANGPAY_API_SECRET',
                'label' => $this->module->l('senangpay API SECRET'),
                'type' => 'text'
            )
        );

        $param_url = '?status_id=[TXN_STATUS]&order_id=[ORDER_ID]&transaction_id=[TXN_REF]';
        $param_url .= '&msg=[MSG]&paid_amount=[AMOUNT]&hash=[HASH]';
        $description                      = '<div>
                        <p style="color:red">Please TURN ON Friendly URL for senangPay to update your payment status.
                        Else, you will get error 404 page when trying to make payment with senangPay</p>
						<p style="color:red">Attention: Please copy these info and paste it in your senangPay Configuration Settings</p>
						<p >Return URL </p>
						<span style="padding:5px;border:#808080 1px solid !important;margin: 1px;background: #FFF;">'
                        .  _PS_BASE_URL_ . __PS_BASE_URI__ . 'validation</span>
						<br><br>
						<p >Return URL Parameters </p>
                        <span style="padding:5px;border:#808080 1px solid !important;margin: 1px;background: #FFF;">'
                        . $param_url . '</span>
                        <br><br>
						<p>Callback URL </p>
						<span style="padding: 5px;border:#808080 1px solid !important;margin:1px;background:#FFF;">'
                        .  _PS_BASE_URL_ . __PS_BASE_URI__ . 'callback</span>
						</div><hr><div>
						<p style="color:grey; ">
                            This module only works for 1 live site for each senangPay account,
                            and not working on localhost environment.
                            <br/><br/>
                            If you need help, contact developer MIMPRO at mimpro.my@gmail.com</p>
						</div>';
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->module->l('senangPay configuration'),
                    'icon' => 'icon-wrench'
                ),
                'input' => $inputs,
                'description' => $description,
                'submit' => array(
                    'title' => $this->module->l('Save')
                )
            )
        );
        $helper                           = new HelperForm();
        $helper->table                    = 'senangpay';
        $helper->default_form_language    = (int) Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = (int) Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
        $helper->submit_action            = 'senangpay_form';
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false);
        $helper->currentIndex           .= '&configure=' . $this->module->name;
        $helper->currentIndex           .= '&tab_module=' . $this->module->tab;
        $helper->currentIndex           .= '&module_name=' . $this->module->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars                 = array(
            'fields_value' => array(
                    'SENANGPAY_API_ID' => Tools::getValue(
                        'SENANGPAY_API_ID',
                        Configuration::get('SENANGPAY_API_ID')
                    ),
                    'SENANGPAY_API_SECRET' => Tools::getValue(
                        'SENANGPAY_API_SECRET',
                        Configuration::get('SENANGPAY_API_SECRET')
                    ),
                    'languages' => $this->context->controller->getLanguages()
            )
        );
        return $helper->generateForm(array($fields_form));
    }
    public function run()
    {
        $this->processConfiguration();
        $html_confirmation_message = $this->module->display($this->file, 'getContent.tpl');
        $html_form                 = $this->renderForm();
        return $html_confirmation_message . $html_form;
    }
}
