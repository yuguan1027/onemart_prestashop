<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

class SenangPayCallbackModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        $senangpay_SECRET         = Configuration::get('SENANGPAY_API_SECRET');
        $senangpay_status_id      = $_REQUEST['status_id'];
        $senangpay_order_id       = $_REQUEST['order_id'];
        $senangpay_msg            = $_REQUEST['msg'];
        $senangpay_transaction_id = $_REQUEST['transaction_id'];
        $senangpay_hash           = $_REQUEST['hash'];

        $senangpay_paid           = number_format($_REQUEST['paid_amount'], 2, '.', '');
        $extra_vars               = array('transaction_id' => $senangpay_transaction_id);
        $cart                     = new Cart($senangpay_order_id);

        //for message, only if "Payment was successful"
        $msg_ok = "Payment was successful";

        if ($cart->id_customer == 0
            || $cart->id_address_delivery == 0
            || $cart->id_address_invoice == 0
            || !$this->module->active) {
            $this->returnError('Invalid cart');
        }

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            $this->returnError('Invalid customer');
        }

        $currency          = new Currency((int) $cart->id_currency);

        $pre_hash          = $senangpay_SECRET;
        $pre_hash          .='?status_id=' . $senangpay_status_id;
        $pre_hash          .='&order_id=' . $senangpay_order_id;
        $pre_hash          .='&transaction_id=' . $senangpay_transaction_id;
        $pre_hash          .='&msg=' . $senangpay_msg;
        $pre_hash          .='&paid_amount=' . $senangpay_paid;
        $pre_hash          .='&hash=[HASH]';

        $set_hash          = md5($pre_hash);

        if ($set_hash == $senangpay_hash) {
            if ($senangpay_status_id == 1 || $senangpay_status_id == '1' || $senangpay_msg == $msg_ok) {
                if (!Order::getOrderByCartId($senangpay_order_id)) {
                    $this->module->validateOrder(
                        $senangpay_order_id,
                        Configuration::get('PS_OS_PAYMENT'),
                        $senangpay_paid,
                        $this->module->displayName,
                        null,
                        $extra_vars,
                        (int) $currency->id,
                        false,
                        $customer->secure_key
                    );
                }
            }
        }

        echo ('OK');
        die();
    }
}
