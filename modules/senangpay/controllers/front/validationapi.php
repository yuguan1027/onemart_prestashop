<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

class SenangPayValidationApiModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        $senangpay_SECRET         = Configuration::get('SENANGPAY_API_SECRET');
        $senangpay_status_id      = $_REQUEST['status_id'];
        $senangpay_order_id       = $_REQUEST['order_id'];
        $senangpay_msg            = $_REQUEST['msg'];
        $senangpay_transaction_id = $_REQUEST['transaction_id'];
        $senangpay_hash           = $_REQUEST['hash'];

        //parameter paid_amount from merchant instead of amount from cart
        $senangpay_paid           = '0';
        $senangpay_paid           = number_format($_REQUEST['paid_amount'], 2, '.', '');
        $extra_vars               = array('transaction_id' => $senangpay_transaction_id);
        $cart                     = new Cart($senangpay_order_id);

        //for message, only if "Payment was successful"
        $msg_ok = "Payment was successful";

        if ($cart->id_customer == 0
            || $cart->id_address_delivery == 0
            || $cart->id_address_invoice == 0
            || !$this->module->active) {
            $this->returnError('Invalid cart');
        }

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            $this->returnError('Invalid customer');
        }

        $currency          = new Currency((int) $cart->id_currency);

        //preparing redirect url for result pages
        $payment_not_matched = 'payment-not-matched?';
        $payment_error = 'payment-error?';

        //total_catr_amount just to check with paid_amount
        $total_cart_amount = number_format((float) $cart->getOrderTotal(true, Cart::BOTH), 2, '.', '');

        $pre_hash          = $senangpay_SECRET;
        $pre_hash          .='?status_id=' . $senangpay_status_id;
        $pre_hash          .='&order_id=' . $senangpay_order_id;
        $pre_hash          .='&transaction_id=' . $senangpay_transaction_id;
        $pre_hash          .='&msg=' . $senangpay_msg;
        $pre_hash          .='&paid_amount=' . $senangpay_paid;
        $pre_hash          .='&hash=[HASH]';

        $set_hash          = md5($pre_hash);



        if ($set_hash == $senangpay_hash) {
            //for successful payment status
            //if updated by callback, no need to validate more, just skip and display successful page
            //if not yet updated by callback, proceed the validation


            if ($senangpay_status_id == 1 || $senangpay_status_id == '1' || $senangpay_msg == $msg_ok) {
                if (!Order::getOrderByCartId($senangpay_order_id)) {
                    $this->module->validateOrder(
                        $senangpay_order_id,
                        Configuration::get('PS_OS_PAYMENT'),
                        $senangpay_paid,
                        $this->module->displayName,
                        null,
                        $extra_vars,
                        (int) $currency->id,
                        false,
                        $customer->secure_key
                    );
                }

                //after validated by callback, or validation, proceed next
                $ps_id_order = Order::getOrderByCartId($senangpay_order_id);
                $ref         = Order::getUniqReferenceOf($ps_id_order);

                //for payment that successful but not matched with cart amount, return with a payment unmatch page
                if ($senangpay_paid != $total_cart_amount) {
                    Tools::redirect(
                        _PS_BASE_URL_ . __PS_BASE_URI__ . $payment_not_matched
                        . '&msg=' . $senangpay_msg
                        . '&transaction_id=' . $senangpay_transaction_id
                        . '&reference=' . $ref
                        . '&ps_order=' . $ps_id_order
                        . '&total=' . $senangpay_paid
                        . '&total_cart=' . $total_cart_amount
                    );
                } else {
                //payment successful, and paid amount matched with cart amount, return with success page
                    Tools::redirect(
                        'index.php?controller=order-confirmation&id_cart='.$cart->id
                        . '&id_module=' . $this->module->id
                        . '&id_order=' . $ps_id_order
                        . '&tx_id=' . $senangpay_transaction_id
                        . '&tx_msg=' . $senangpay_msg
                        . '&key=' . $customer->secure_key
                    );
                }

            //status id is not successful. Return error payment page
            } else {
                $senangpay_msg .= " and status id conflicted from senangPay";
                Tools::redirect(_PS_BASE_URL_ . __PS_BASE_URI__ . $payment_error . '&msg=' . $senangpay_msg);
            }

        //hash is not correct. Return error payment page
        } else {
            $senangpay_msg .= " and hash return not matched from senangPay";
            Tools::redirect(_PS_BASE_URL_ . __PS_BASE_URI__ . $payment_error . '&msg=' . $senangpay_msg);
        }
    }

    public function returnError($result)
    {
        echo Tools::jsonEncode(array('error' => $result));
        exit;
    }
}
