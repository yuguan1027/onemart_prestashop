<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

class SenangpayPaymentNotMatchedModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        //$senangpay_msg            = $_REQUEST['msg'];
        $senangpay_msg            = str_replace("_", " ", $senangpay_msg);
        $cart_total               = $_REQUEST['total_cart'];
        $total_paid               = $_REQUEST['total'];
        $senangpay_transaction_id = $_REQUEST['transaction_id'];
        $reference                = $_REQUEST['reference'];
        $id_order                 = $_REQUEST['ps_order'];

        $this->context->smarty->assign('senangpay_msg', $senangpay_msg);
        $this->context->smarty->assign('senangpay_transaction_id', $senangpay_transaction_id);
        $this->context->smarty->assign('reference', $reference);
        $this->context->smarty->assign('id_order', $id_order);
        $this->context->smarty->assign('total_cart', $cart_total);
        $this->context->smarty->assign('total_paid', $total_paid);
        $this->setTemplate('module:senangpay/views/templates/front/paymentnotmatched.tpl');
    }
}
