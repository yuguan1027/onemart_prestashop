<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Dudy | mimpro.my@gmail.com
 *  @copyright 2017 MIMPRO ENTERPRISE
 *  @license   Valid via official purchase from addons.prestashop.com Only
 */

class SenangPayPaymentErrorModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $senangpay_msg            = $_REQUEST['msg'];
        $senangpay_msg            = str_replace("_", " ", $senangpay_msg);
        $this->context->smarty->assign('senangpay_msg', $senangpay_msg);
        $this->setTemplate('module:senangpay/views/templates/front/paymenterror.tpl');
    }
}
