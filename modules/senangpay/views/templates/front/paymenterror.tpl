{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Dudy <mimpro.my@gmail.com>
*  @copyright  2017 MIMPRO ENTERPRISE
*  @license    Valid via official purchase from addons.prestashop.com Only
*}	

<head>
    {block name='head'}
        {include file='_partials/head.tpl'}
    {/block}
</head>

<body>
{hook h='displayAfterBodyOpeningTag'}
<main>
    <!-- Menu part-->
    <header id="header">
        {block name='header'}
            {include file='_partials/header.tpl'}
        {/block}
    </header>

    <!-- Header part ends -->

    <section id="wrapper">
        <div class="container">

            <section id="sp-err">
                <section id="content" class="page-content card card-block">
                    {include file='_partials/breadcrumb.tpl'}
                    <h2>{l s='Error in senangPay Transaction' mod='senangpay'}</h2>

                    <div class="table-responsive-row clearfix">
                    	<div class="sp-a"><img src="/modules/senangpay/views/img/senangpay.jpg"></div>
                        <div class="box" style="  background: #FCA6A6;">
                        	<p class="cheque-indent">
                        		<strong class="dark">
                        			{l s='Payment error' mod='senangpay'}.
                        			<br>
                        			<br>

                        		{l s='No purchase order is executed' mod='senangpay'}. 
                        		<br>
                        		{l s='Kindly check your bank status and contact us if you have problem with this transaction' mod='senangpay'}.</strong>
                        	</p>
                        	<hr>
                        	<p class="cheque-indent">
                        		<strong class="dark">{l s='Payment Status Message' mod='senangpay'} :<span style="color:red"> {$senangpay_msg|escape:'html':'UTF-8'}</span></strong>
                        	</p>	
                        </div>
                    </div>



                </section>
            </section>
        </div>
    </section>
    <!-- Footer starts -->

    <footer id="footer">
        {block name="footer"}
            {include file="_partials/footer.tpl"}
        {/block}
    </footer>
    <!-- Footer Ends -->
    {block name='javascript_bottom'}
        {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
    {/block}
    {hook h='displayBeforeBodyClosingTag'}
</main>

</body>		
