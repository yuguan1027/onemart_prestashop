{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Dudy <mimpro.my@gmail.com>
*  @copyright  2017 MIMPRO ENTERPRISE
*  @license    Valid via official purchase from addons.prestashop.com Only
*}



<head>
    {block name='head'}
        {include file='_partials/head.tpl'}
    {/block}
</head>

<body>
{hook h='displayAfterBodyOpeningTag'}
<main>
    <!-- Menu part-->
    <header id="header">
        {block name='header'}
            {include file='_partials/header.tpl'}
        {/block}
    </header>

    <!-- Header part ends -->

    <section id="wrapper">
        <div class="container">

            <section id="sp-pnm">
                <section id="content" class="page-content card card-block">
                    {include file='_partials/breadcrumb.tpl'}
                    <h2>{l s='Amount senangPay Transaction Not Matching with Order Amount ' mod='senangpay'}</h2>

                    <div class="table-responsive-row clearfix">
                    	<div class="sp-a"><img src="/modules/senangpay/views/img/senangpay.jpg"></div>
                        <div class="box" style="  background: #FAD767;">
	<p class="cheque-indent">
		<strong class="dark">{l s='Your payment senangPay is successful but not matched with your cart details' mod='senangpay'}.</strong>
	</p>
	
	<p>
		Total Cart Amount : RM {$total_cart|escape:'htmlall':'UTF-8'}
		<br>
		Total Paid Amount : RM {$total_paid|escape:'htmlall':'UTF-8'} <span style="color:red; font-weight:bold">[NOT MATCHED ]</span>
		<br>
		Payment Method : senangPay 
		<br>
		Status Message : {$senangpay_msg|escape:'htmlall':'UTF-8'}
		<br>
		Transaction ID : {$senangpay_transaction_id|escape:'htmlall':'UTF-8'}
		<br>
		<br>
		<strong>NOTE : ORDER IS CREATED BUT PAYMENT IS NOT MATCHED, PLEASE CONTACT US FOR ASSISTANT</strong>
		<br>
		<br>
	</p>

	<p>
    {if !isset($reference)}
	    {l s='Your order number is' mod='senangpay'}: <span style="color:red; font-weight:bold">{l s='#%d' sprintf=[$id_order] mod='senangpay'}</span> 
    {else}
	    {l s='Your order reference is' mod='senangpay'}: <span style="color:red; font-weight:bold">{l s='%s' sprintf=[$reference] mod='senangpay'}</span>. 
    {/if}
    {l s='Keep this as reference number for the order.' mod='senangpay'}
    <br>
    <br>
    {l s='We have emailed you the details of this order, and the invoice.' mod='senangpay'}
	</p>
	<hr>
	<p><strong>{l s='Contact us for resolving this status. We will update your payment with successful and proceed with delivery once it is resolved' mod='senangpay'}</strong></p>
	<hr>
	<br>
	<p>{l s='If you have questions, comments or concerns, please contact our' mod='senangpay'} <a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}">{l s='expert customer support team.' mod='senangpay'}</a>.</p>
</div>
                    </div>



                </section>
            </section>
        </div>
    </section>
    <!-- Footer starts -->

    <footer id="footer">
        {block name="footer"}
            {include file="_partials/footer.tpl"}
        {/block}
    </footer>
    <!-- Footer Ends -->
    {block name='javascript_bottom'}
        {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
    {/block}
    {hook h='displayBeforeBodyClosingTag'}
</main>

</body>		

		