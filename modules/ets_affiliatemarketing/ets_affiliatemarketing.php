<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrSearch by name, reference and idestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <contact@etssoft.net>
 * @copyright  2007-2021 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
if (!defined('_ETS_AM_MODULE_')) {
    define('_ETS_AM_MODULE_', 'ets_affiliatemarketing');
}
define('EAM_AFF_CUSTOMER_COOKIE', 'eam_aff_customer_cookie');
define('EAM_AFF_PRODUCT_COOKIE', 'eam_aff_product_cookie');
define('EAM_AFF_VISITED_PRODUCTS', 'eam_aff_visited_products');
define('EAM_CUSTOMER_VIEW_PRODUCT', 'eam_customer_view_product');
define('EAM_REFS', 'eam_refs');
define('EAM_AM_LOYALTY_REWARD', 'loy');
define('EAM_AM_AFFILIATE_REWARD', 'aff');
define('EAM_AM_REF_REWARD', 'ref');
define('URL_REGISTER_REWARD_PROGRAM', 'affiliate-marketing/register-programs');
define('URL_REF_PROGRAM', 'affiliate-marketing/referral-program');
define('URL_CUSTOMER_REWARD', 'affiliate-marketing/customer-reward');
define('URL_LOY_PROGRAM', 'affiliate-marketing/loyalty-program');
define('URL_AFF_PROGRAM', 'affiliate-marketing/affiliate');
define('URL_EAM_HISTORY', 'affiliate-marketing/reward-histories');
define('URL_EAM_WITHDRAW', 'affiliate-marketing/withdraw');
define('URL_EAM_VOUCHER', 'affiliate-marketing/voucher');
define('URL_EAM_AFF_PRODUCT', 'affiliate-marketing/affiliate-products');
define('URL_EAM_PRODUCT_VIEW', 'affiliate-marketing/product-view');
define('URL_EAM_MY_SALE', 'affiliate-marketing/my-sales');
define('ETS_AM_PROMO_PREFIX', 'EAM');
define('EAM_PATH_IMAGE_BANER', 'img/ets_affiliatemarketing/');
define('EAM_INVOICE_PATH', 'invoices');
define('LOG_IP_CONFIGURATION_KEY', 'ETS_AM_IP_LOG');

require_once(dirname(__FILE__) . '/classes/admin/EtsAmAdmin.php');
require_once(dirname(__FILE__) . '/classes/Ets_AM.php');
require_once(dirname(__FILE__) . '/classes/Ets_Loyalty.php');
require_once(dirname(__FILE__) . '/classes/Ets_Participation.php');
require_once(dirname(__FILE__) . '/classes/Ets_Affiliate.php');
require_once(dirname(__FILE__) . '/classes/Ets_Sponsor.php');
require_once(dirname(__FILE__) . '/classes/Ets_History.php');
require_once(dirname(__FILE__) . '/classes/Ets_Reward_Usage.php');
require_once(dirname(__FILE__) . '/classes/Ets_Invitation.php');
require_once(dirname(__FILE__) . '/classes/Ets_Banner.php');
require_once(dirname(__FILE__) . '/classes/Ets_Withdraw.php');
require_once(dirname(__FILE__) . '/classes/Ets_Withdraw_Field.php');
require_once(dirname(__FILE__) . '/classes/Ets_PaymentMethod.php');
require_once(dirname(__FILE__) . '/classes/Ets_Voucher.php');
require_once(dirname(__FILE__) . '/controllers/front/all.php');
require_once(dirname(__FILE__) . '/classes/Ets_User.php');
require_once(dirname(__FILE__) . '/classes/Ets_Reward_Product.php');
require_once(dirname(__FILE__) . '/classes/Ets_Access_Key.php');
require_once(dirname(__FILE__) . '/classes/Ets_Product_View.php');
require_once(dirname(__FILE__) . '/classes/Ets_ImportExport.php');
require_once(dirname(__FILE__) . '/defines.php');
require_once(dirname(__FILE__) . '/classes/Ets_aff_email.php');
require_once(dirname(__FILE__) . '/classes/Ets_aff_qr_code.php');

class Ets_affiliatemarketing extends PaymentModule
{
    const CACERT_LOCATION = 'https://curl.haxx.se/ca/cacert.pem';
    const SERVICE_LOCALE_REPOSITORY = 'prestashop.core.localization.locale.repository';
    public $_html;
    public $dashboard = array();
    public $reward_history = array();
    public $loyalty_conditions = array();
    public $loyalty_options = array();
    public $reward_settings = array();
    public $loyalty_messages = array();
    public $loyalty_email = array();
    public $rs_program_conditions = array();
    public $rs_program_reward_caculation = array();
    public $rs_program_voucher = array();
    public $rs_program_suab = array();
    public $rs_program_email = array();
    public $rs_program_messages = array();
    public $usage_settings = array();
    public $withdraw_list = array();
    public $affiliate_conditions = array();
    public $affiliate_reward_caculation = array();
    public $affiliate_voucher = array();
    public $affiliate_messages = array();
    public $affiliate_email = array();
    public $fields_list = array();
    public $import_export = array();
    public $general_settings = array();
    public $general_email = array();
    public $loyalty_bases = array();
    //public $cronjob_settings = array();
    public $cronjob_config = array();
    public $cronjob_history = array();

    public $reward_usage = array();
    public $applications = array();
    public $payment_settings = array();
    public $reward_users = array();
    public static $trans = array();
    public $_errors = array();
    public $is17;
    public $currencies = array();
    public $countries = array();
    public $_id_product = null;
    public $shortlink;
    public $list_id = null;
    public $toolbar_btn = array(); 
    protected $_filterHaving = "";
    protected $_filter = "";


    /**
     * Ets_affiliatemarketing constructor.
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function __construct()
    {
        $this->createRequiresTable();
        $this->name = 'ets_affiliatemarketing';
        $this->tab = 'front_office_features';
        $this->version = '1.4.2';
        $this->author = 'ETS-Soft';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = '54356e288958a33ac3a434d4f4d0d1eb';
        $this->bootstrap = true;
        $this->loyalty_groups = array();
        parent::__construct();

        self::$trans = array(
            'point' => $this->l('point'),
            'points' => $this->l('points'),
            'Edit' => $this->l('Edit'),
            'View' => $this->l('View'),
            'Expired' => $this->l('Expired'),
            'Delete' => $this->l('Delete'),
            'Approve' => $this->l('Approve'),
            'Active' => $this->l('Active'),
            'Approved' => $this->l('Approved'),
            'Pending' => $this->l('Pending'),
            'Decline' => $this->l('Decline'),
            'Declined' => $this->l('Declined'),
            'Suspended' => $this->l('Suspended'),
            'Refuse' => $this->l('Refuse'),
            'Cancel' => $this->l('Cancel'),
            'Canceled' => $this->l('Canceled'),
            'Stop' => $this->l('Stop'),
            'Validated' => $this->l('Validated'),
            'Validate' => $this->l('Validate'),
            'join_affiliate_btn' => $this->l('Join Affiliate Program'),
            'copy-to-clipboard' => $this->l('Click to copy to clipboard'),
            'copy-to-clipboard-success' => $this->l('Copied to clipboard'),
            'new-reward' => $this->l('New reward was created.'),
            'customer' => $this->l('Customer'),
            'amount' => $this->l('Amount'),
            'type' => $this->l('Type'),
            'expiry' => $this->l('Expired in'),
            'reward_amount' => $this->l('Reward amount'),
            'reward_validated' => $this->l('Your reward was approved'),
            'reward_canceled' => $this->l('Your reward was canceled'),
            'reward_going_to_be_expired' => $this->l('Your reward is going to be expired!'),
            'reward_expired' => $this->l('Reward expired'),
            'turnover' => $this->l('Turnover'),
            'reward' => $this->l('Reward'),
            'net_profit' => $this->l('Net Profit'),
            'orders' => $this->l('Orders'),
            'customers' => $this->l('Customers'),
            'conversion_rate' => $this->l('Conversion Rate'),
            'error_fee_payment' => $this->l('Fee of payment method must be a decimal.'),
            'error_payment_method_string' => $this->l('Title of payment method must be a string.'),
            'error_payment_field_string' => $this->l('Title of payment field must be a string.'),
            'confirm_msg' => $this->l('Are you sure to do this action?'),
            'times' => $this->l('Time'),
            'dates' => $this->l('Date'),
            'banner_uploaded' => $this->l('Successful update'),
            'level' => $this->l('Level'),
            'referral_program' => $this->l('Referral program'),
            'affiliate_program' => $this->l('Affiliate program'),
            'loyalty_program' => $this->l('Loyalty program'),
            'no_fee' =>  $this->l('No Fee'),
            'yes' =>  $this->l('Yes'),
            'no' =>  $this->l('No'),
            'user_deleted' => $this->l('User deleted'),
            'view_details' =>  $this->l('View details'),
            'views' =>  $this->l('Views'),
            'Deleted' =>  $this->l('Deleted'),
            'Decline_return' =>  $this->l('Decline - Return reward'),
            'Decline_deduct' =>  $this->l('Decline - Deduct reward'),
            'total_order' =>  $this->l('Total orders'),
            'total_view' =>  $this->l('Total views'),
            'earning_reward' =>  $this->l('Earning rewards'),
            'loyalty' =>  $this->l('Loyalty'),
            'affiliate' =>  $this->l('Affiliate'),
            'referral' =>  $this->l('Referral'),
            'estimated' =>  $this->l('estimated'),
            'view_user' =>  $this->l('View user'),
            'reward_unit_label_required' =>  $this->l('Reward unit label is required'),
            'coversion_rate_required' =>  $this->l('Conversion rate is required'),
            'email_receive_required' =>  $this->l('Email to receive is required'),
            'specific_time_required' =>  $this->l('Specific time is required'),
            'categories_required' =>  $this->l('Categories are required'),
            'discount_percent_required' =>  $this->l('Discount percentage is required'),
            'discount_availability_required' =>  $this->l('Discount availability is required'),
            'amount_required' =>  $this->l('Amount is required'),
            'percentage_required' =>  $this->l('Percentage is required'),
            'amount_fixed_required' =>  $this->l('Fixed amount is required'),
            'second_ago' => $this->l('second(s) ago'),
            'minute_ago' => $this->l('minute(s) ago'),
            'hour_ago' => $this->l('hour(s) ago'),
            'day_ago' => $this->l('day(s) ago'),
            'month_ago' => $this->l('month(s) ago'),
            'year_ago' => $this->l('year(s) ago'),
            'less_than_1s_ago' => $this->l('less than 1 second ago'),
            'reward_usage' =>$this->l('Reward usage'),
            'suspend' => $this->l('Suspend'),
            'reward_used_label' => $this->l('Used reward'),
            'reward_earned_label' => $this->l('Earned reward'),
            'reward_created_for_you' => $this->l('A new reward created for you'),
            'a_reward_validated' => $this->l('A reward was approved'),
            'a_reward_canceled' => $this->l('A reward was canceled'),
            'a_reward_created' => $this->l('A new reward was created'),
            'voucher_sell_quantity_require' => $this->l('Quantity is required'),
            'voucher_sell_quantity_vaild' => $this->l('Quantity is not valid'),
            'subject_approve_width'=> $this->l('Your withdrawal request was approved!'),
            'subject_admin_approve_width'=> $this->l('You have approved a withdrawal request'),
            'subject_decline_width'=> $this->l('Your withdrawal request was declined!'),
            'subject_admin_decline_width'=> $this->l('You have declined a withdrawal request'),
            'Deduct' => $this->l('Deduct'),
            'Refund' => $this->l('Refund'),
            'deduct_reward' => $this->l('Deducted reward'),
            'return_reward' => $this->l('Returned reward'),
            'referral_and_affiliate_program' => $this->l('Referral and Affiliate program'),
            'your_application_was_declined' => $this->l('Your application was declined'),
            'your_application_was_approved' => $this->l('Your application was approved'),
            'your_reward_is_going_be_expired'=> $this->l('Your reward is going to be expired'),
            'a_new_reward_was_created' => $this->l('A new reward was created'),
            'your_reward_was_expired' => $this->l('Your reward was expired'),
            'note_reward_ref_user' => $this->l('Refer new user (#%s)'),
            'note_reward_ref_order' => $this->l('Referral commission (Order: #%s, Level: %s)'),
            'categories_valid' => $this->l('Categories are not valid'),
        );

        $this->displayName = $this->l('Loyalty, referral and affiliate program (reward points)');
        $this->description = $this->l('Allows customers to earn rewards (points or cash) when they buy, sell or refer new customers to your website. Includes 3 marketing programs: Loyalty, Referral and Affiliate programs to boost your sales and customers.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        
        $this->shortlink = 'https://mf.short-link.org/';
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->is17 = true;
        }
    }

    /**
     * @param $params
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplaySnwProductList($params)
    {
        if (isset($params['ids']) && ($productIds = $params['ids'])) {
            $IDs = explode(',', $productIds);
            $products = array();
            foreach ($IDs as $id) {
                $image = Product::getCover($id);
                $product = new Product($id, false, (int)Configuration::get('PS_LANG_DEFAULT'));
                if ($product) {
                    $imagePath = $this->context->link->getImageLink($product->link_rewrite, $image['id_image']);
                    $product_url = $this->context->link->getProductLink((int)$id);
                    $product->image = $imagePath;
                    $product->product_url = $product_url;
                    $product->id_product = (int)$id;
                    $products[] = $product;
                }
            }

            $this->smarty->assign(array(
                'products' => $products,
                'default_lang' => (int)Configuration::get('PS_LANG_DEFAULT')
            ));
            //return $this->display(__FILE__, 'block_product_item.tpl');
            return $this->display(__FILE__, 'block_prd_items.tpl');
        }
    }

    /**
     * @param $excludeId
     * @return bool|string
     */
    public function isValidIds($excludeId)
    {
        if ($excludeId != '') {
            $ids = explode('-', $excludeId);
            if (!isset($ids[1]))
                $ids[1] = 0;
            if (Validate::isInt($ids[0]) && Validate::isInt($ids[1]))
                return (int)$ids[0] . '-' . (int)$ids[1];
            return false;
        }
        return false;
    }
    public function displayRecommendedModules()
    {
        $cacheDir = dirname(__file__) . '/../../cache/'.$this->name.'/';
        $cacheFile = $cacheDir.'module-list.xml';
        $cacheLifeTime = 24;
        $cacheTime = (int)Configuration::getGlobalValue('ETS_MOD_CACHE_'.$this->name);
        $profileLinks = array(
            'en' => 'https://addons.prestashop.com/en/207_ets-soft',
            'fr' => 'https://addons.prestashop.com/fr/207_ets-soft',
            'it' => 'https://addons.prestashop.com/it/207_ets-soft',
            'es' => 'https://addons.prestashop.com/es/207_ets-soft',
        );
        if(!is_dir($cacheDir))
        {
            @mkdir($cacheDir, 0755,true);
            if ( @file_exists(dirname(__file__).'/index.php')){
                @copy(dirname(__file__).'/index.php', $cacheDir.'index.php');
            }
        }
        if(!file_exists($cacheFile) || !$cacheTime || time()-$cacheTime > $cacheLifeTime * 60 * 60)
        {
            if(file_exists($cacheFile))
                @unlink($cacheFile);
            if($xml = self::file_get_contents($this->shortlink.'ml.xml'))
            {
                $xmlData = @simplexml_load_string($xml);
                if($xmlData && (!isset($xmlData->enable_cache) || (int)$xmlData->enable_cache))
                {
                    @file_put_contents($cacheFile,$xml);
                    Configuration::updateGlobalValue('ETS_MOD_CACHE_'.$this->name,time());
                }
            }
        }
        else
            $xml = Tools::file_get_contents($cacheFile);
        $modules = array();
        $categories = array();
        $categories[] = array('id'=>0,'title' => $this->l('All categories'));
        $enabled = true;
        $iso = Tools::strtolower($this->context->language->iso_code);
        $moduleName = $this->displayName;
        $contactUrl = '';
        if($xml && ($xmlData = @simplexml_load_string($xml)))
        {
            if(isset($xmlData->modules->item) && $xmlData->modules->item)
            {
                foreach($xmlData->modules->item as $arg)
                {
                    if($arg)
                    {
                        $index = 'title'.($iso=='en' ? '' : '_'.$iso);
                        if(isset($arg->module_id) && (string)$arg->module_id==$this->name && isset($arg->{$index}) && (string)$arg->{$index})
                        {
                            $moduleName = (string)$arg->{$index};
                        }
                        if(isset($arg->module_id) && (string)$arg->module_id==$this->name && isset($arg->contact_url) && (string)$arg->contact_url)
                            $contactUrl = $iso!='en' ? str_replace('/en/','/'.$iso.'/',(string)$arg->contact_url) : (string)$arg->contact_url;
                        $temp = array();
                        foreach($arg as $key=>$val)
                        {
                            if($key=='price' || $key=='download')
                                $temp[$key] = (int)$val;
                            elseif($key=='rating')
                            {
                                $rating = (float)$val;
                                if($rating > 0)
                                {
                                    $ratingInt = (int)$rating;
                                    $ratingDec = $rating-$ratingInt;
                                    $startClass = $ratingDec >= 0.5 ? ceil($rating) : ($ratingDec > 0 ? $ratingInt.'5' : $ratingInt);
                                    $temp['ratingClass'] = 'mod-start-'.$startClass;
                                }
                                else
                                    $temp['ratingClass'] = '';
                            }
                            elseif($key=='rating_count')
                                $temp[$key] = (int)$val;
                            else
                                $temp[$key] = (string)strip_tags($val);
                        }
                        if($iso)
                        {
                            if(isset($temp['link_'.$iso]) && isset($temp['link_'.$iso]))
                                $temp['link'] = $temp['link_'.$iso];
                            if(isset($temp['title_'.$iso]) && isset($temp['title_'.$iso]))
                                $temp['title'] = $temp['title_'.$iso];
                            if(isset($temp['desc_'.$iso]) && isset($temp['desc_'.$iso]))
                                $temp['desc'] = $temp['desc_'.$iso];
                        }
                        $modules[] = $temp;
                    }
                }
            }
            if(isset($xmlData->categories->item) && $xmlData->categories->item)
            {
                foreach($xmlData->categories->item as $arg)
                {
                    if($arg)
                    {
                        $temp = array();
                        foreach($arg as $key=>$val)
                        {
                            $temp[$key] = (string)strip_tags($val);
                        }
                        if(isset($temp['title_'.$iso]) && $temp['title_'.$iso])
                                $temp['title'] = $temp['title_'.$iso];
                        $categories[] = $temp;
                    }
                }
            }
        }
        $key_xml = 'intro_'.$iso;
        if(isset($xmlData->{$key_xml}))
        {
            $intro = $xmlData->{$key_xml};
        }
        else
            $intro = isset($xmlData->intro_en) ? $xmlData->intro_en : false;
        $this->smarty->assign(array(
            'modules' => $modules,
            'enabled' => $enabled,
            'module_name' => $moduleName,
            'categories' => $categories,
            'img_dir' => $this->_path . 'views/img/',
            'intro' => $intro,
            'shortlink' => $this->shortlink,
            'ets_profile_url' => isset($profileLinks[$iso]) ? $profileLinks[$iso] : $profileLinks['en'],
            'trans' => array(
                'txt_must_have' => $this->l('Must-Have'),
                'txt_downloads' => $this->l('Downloads!'),
                'txt_view_all' => $this->l('View all our modules'),
                'txt_fav' => $this->l('Prestashop\'s favourite'),
                'txt_elected' => $this->l('Elected by merchants'),
                'txt_superhero' => $this->l('Superhero Seller'),
                'txt_partner' => $this->l('Module Partner Creator'),
                'txt_contact' => $this->l('Contact us'),
                'txt_close' => $this->l('Close'),
            ),
            'contactUrl' => $contactUrl,
         ));
         echo $this->display(__FILE__, 'module-list.tpl');
         die;
    }
    public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 60)
    {
        if ($stream_context == null && preg_match('/^https?:\/\//', $url)) {
            $stream_context = stream_context_create(array(
                "http" => array(
                    "timeout" => $curl_timeout,
                    "max_redirects" => 101,
                    "header" => 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
                ),
                "ssl"=>array(
                    "allow_self_signed"=>true,
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            ));
        }
        if (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => html_entity_decode($url),
                CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => $curl_timeout,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_FOLLOWLOCATION => true,
            ));
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        } elseif (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url)) {
            return Tools::file_get_contents($url, $use_include_path, $stream_context);
        } else {
            return false;
        }
    }
    public function actionAjax()
    {
        if(($configure = Tools::getValue('configure')) && $configure==$this->name && ($tabActive = Tools::getValue('tabActive')) && Tools::strtolower($tabActive)=='othermodules')
        {
            $this->displayRecommendedModules();
        }
        if (($query = Tools::getValue('q', false)) && $query && Validate::isCleanHtml($query)) {
            EtsAmAdmin::searchPrdById($query);
        }
        if (($productType = Tools::getValue('product_type', false)) && Validate::isCleanHtml($productType) && ($IDs = Tools::getValue('ids', false)) && Validate::isCleanHtml($IDs) ) {
            die(Tools::jsonEncode(array(
                'html' => $this->hookDisplaySnwProductList(array('ids' => $IDs)),
            )));
        }
        if(Tools::isSubmit('clear_log'))
        {
            $cleared = false;
            if(file_exists(dirname(__FILE__).'/cronjob.log')){
                @unlink(dirname(__FILE__).'/cronjob.log');
                $cleared = true;
            }
            die(Tools::jsonEncode(array(
                'success' => $cleared ? $this->l('Log cleared') : false,
                'error' => !$cleared ? $this->l('Log is empty. Nothing to do!') : false,
            )));
        }
        if (($initSearch = Tools::getValue('initSearchProduct', false)) && Validate::isCleanHtml($initSearch) && ($ids = Tools::getValue('ids', false)) && Ets_affiliatemarketing::validateArray($ids)) {
            $this->getProductsAdded($ids);
        }
        if (($action = Tools::getValue('table_action')) && Validate::isCleanHtml($action) && ($id = Tools::getValue('id'))) {
            if (Validate::isInt($id)) {
                $id = (int)$id;
                if (in_array($action, array('APPROVE', 'DECLINE_RETURN', 'DECLINE_DEDUCT', 'DELETE'))) {
                    $response = Ets_Withdraw::updateWithdrawAndReward($id, $action);
                    $wStatus = $this->l('Approved');
                    if($action == 'DECLINE_RETURN' || $action == 'DECLINE_DEDUCT'){
                        $wStatus = $this->l('Declined');
                    }

                    if ($response['success']) {
                        die(Tools::jsonEncode(array(
                            'success' => true,
                            'message' => $action == 'DELETE' ? $this->l('Successful deleted') : $this->l('Updated successfully'),
                            'actions' => $response['actions'],
                            'status' => $wStatus
                        )));
                    }
                }
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->l('Error')
            )));
        }
        if (($updateProduct = Tools::getValue('updateProductSetting', false)) && Validate::isCleanHtml($updateProduct) && ($data_settings = Tools::getValue('data', false))) {
            $setting_error = false;
            if (isset($data_settings['loy_settings']) && $data_settings['loy_settings']) {
                foreach ($data_settings['loy_settings'] as $key => $data) {
                    if ($key !== 'base_on' && $key !== 'qty_min') {
                        if (!Validate::isUnsignedFloat($data)) {
                            $setting_error = true;
                            break;
                        }
                    }
                }
            }
            if (isset($data_settings['aff_settings']) && $data_settings['aff_settings'] && !$setting_error) {
                foreach ($data_settings['aff_settings'] as $key => $data) {
                    if ($key !== 'how_to_calculate') {
                        if ($key == 'single_min_product') {
                            if (!Validate::isUnsignedInt($data)) {
                                $setting_error = true;
                                break;
                            }
                        } else {
                            if (!Validate::isUnsignedFloat($data)) {
                                $setting_error = true;
                                break;
                            }
                        }
                    }
                }
            }

            if ($setting_error) {
                die(Tools::jsonEncode(
                    array(
                        'success' => false,
                        'message' => $this->l('Update failed, data is invalid.')
                    )
                ));
            } else {
                //Create or update setting
                if (isset($data_settings['loy_settings']) && $data_settings['loy_settings']) {

                    $data_loy_setting = $data_settings['loy_settings'];
                    $data_loy_setting['id_shop'] = $this->context->shop->id;
                    $loy_setting = EtsAmAdmin::createOrUpdateSetting('loyalty', $data_loy_setting);
                    $aff_setting = null;
                    if ($loy_setting && isset($data_settings['aff_settings']) && $data_settings['aff_settings']) {
                        $data_aff_setting = $data_settings['aff_settings'];
                        $data_aff_setting['id_shop'] = $this->context->shop->id;

                        $aff_setting = EtsAmAdmin::createOrUpdateSetting('affiliate', $data_aff_setting);
                    }
                    if ($loy_setting && $aff_setting) {
                        //Update success
                        die(Tools::jsonEncode(
                            array(
                                'success' => true,
                                'message' => $this->l('Settings updated.')
                            )
                        ));
                    }

                }

                //Update fail
                die(Tools::jsonEncode(
                    array(
                        'success' => false,
                        'message' => $this->l('Update failed')
                    )
                ));
            }

        }

        if (($getLevel = Tools::getValue('getLevelInput')) && Validate::isCleanHtml($getLevel)) {
            $count = 2;
            $quit = false;
            $level_fields = array();
            while ($quit == false) {
                $level_data = Configuration::get('ETS_AM_REF_SPONSOR_COST_LEVEL_' . $count, false);
                if ($level_data !== false) {
                    array_push($level_fields, array(
                        'level' => $count,
                        'value' => $level_data
                    ));
                    $count++;
                } else {
                    $quit = true;
                }
            }

            if (!empty($level_fields)) {
                die(Tools::jsonEncode(
                    array(
                        'success' => true,
                        'data' => $level_fields
                    )
                ));
            }

            die(Tools::jsonEncode(
                array(
                    'success' => false,
                    'data' => $level_fields
                )
            ));
        }

        if ((int)Tools::isSubmit('actionApplication', false)) {
            $id_approve = (int)Tools::getValue('id_approve', 0);
            $action_user = ($action_user = Tools::getValue('action_user', 0)) && Validate::isCleanHtml($action_user) ? $action_user : '';
            $reason = null;
            if ($action_user == 'decline' || $action_user == 'approve') {
                $reason = ($reason = Tools::getValue('reason', null)) && Validate::isCleanHtml($reason) ? $reason : '';
            }
            $response = EtsAmAdmin::actionCustomer($id_approve, $action_user, $reason);
            $app_status = $this->l('Approved');
            if($action_user == 'decline'){
                $app_status = $this->l('Declined');
            }
            if ($response['success']) {
                die(Tools::jsonEncode(array(
                    'success' => true,
                    'message' => $action_user == 'delete' ? $this->l('Deleted successfully.') : $this->l('Updated successfully.'),
                    'redirect' => $action_user == 'delete' ? $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name.'&tabActive=applications' : '',
                    'actions' => $response['actions'],
                    'status' => $app_status
                )));
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->l('Failed')
            )));
        }

        if ((bool)Tools::isSubmit('getLanguage')) {
            $langs = Language::getLanguages(false);
            $currency = Currency::getDefaultCurrency();
            die(Tools::jsonEncode(array(
                'success' => true,
                'languages' => $langs,
                'currency' => $currency
            )));
        }

        if ((bool)Tools::isSubmit('get_stat_reward')) {
            $this->statsReward();
        }
        if ((bool)Tools::isSubmit('get_pie_chart_reward')) {
            $this->getPercentReward(array('status'=>1));
        }
        if ((bool)Tools::isSubmit('getTotalUserAppPending', false)) {
            $total_pedning_app = EtsAmAdmin::getTotalPendingApplications();
            if ($total_pedning_app) {
                die(Tools::jsonEncode(array(
                    'success' => true,
                    'message' => $this->l('Successful'),
                    'total' => $total_pedning_app
                )));
            } else {
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->l('Failed'),
                    'total' => 0
                )));
            }
        }

        if ((bool)Tools::isSubmit('sortPaymentMethodField', false)) {
            if (($sort_data = Tools::getValue('sort_data')) && Ets_affiliatemarketing::validateArray($sort_data)) {
                if (EtsAmAdmin::updateSortPaymentMethodfield($sort_data)) {
                    die(Tools::jsonEncode(array(
                        'success' => true,
                        'message' => $this->l('Sorted successfully'),
                    )));
                }
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->l('Sort failed.'),
            )));
        }

        if ((bool)Tools::isSubmit('sortPaymentMethod', false)) {
            if (($sort_data = Tools::getValue('sort_data')) && Ets_affiliatemarketing::validateArray($sort_data)) {
                if (EtsAmAdmin::updateSortPaymentMethod($sort_data)) {
                    die(Tools::jsonEncode(array(
                        'success' => true,
                        'message' => $this->l('Sorted successfully'),
                    )));
                }
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->l('Sort failed.'),
            )));
        }
        if ((bool)Tools::isSubmit('getTabDataDasboard', false)) {
            $page = ($page = Tools::getValue('page', 1)) && Validate::isCleanHtml($page) ? $page : 1;
            $type = ($type = Tools::getValue('type', false)) && Validate::isCleanHtml($type) ? $type : '';
            $data_filter = ($data_filter =Tools::getValue('data_filter', array())) && is_array($data_filter) && Ets_affiliatemarketing::validateArray($data_filter) ? $data_filter : array();
            $params = array(
                'page' => $page,
                'type' => $type,
                'data_filter' => $data_filter
            );
            $results = Ets_Am::getStatsTopTrending($params);
            die(Tools::jsonEncode(array(
                'success' => true,
                'html' => $this->renderTableDashboard($results, $type)
            )));
        }

        if (($action_reward = Tools::getValue('doActionRewardItem', false)) && Validate::isCleanHtml($action_reward)) {
            $id_reward = (int)Tools::getValue('id_reward', false);
            $response = EtsAmAdmin::actionReward($id_reward, $action_reward);
            $reward_status = $this->l('Approved');
            if($action_reward == 'cancel'){
                $reward_status = $this->l('Canceled');
            }
            if ($response['success']) {
                die(Tools::jsonEncode(array(
                    'success' => true,
                    'message' => $this->l('The status has been successfully updated'),
                    'actions' => $response['actions'],
                    'status' => $reward_status,
                    'user' => $response['user'],
                )));
            } else {
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->l('Reward update failed'),
                )));
            }
        }
        if (($action_reward = Tools::getValue('doActionRewardUsageItem', false)) && Validate::isCleanHtml($action_reward)) {
            $id_reward = (int)Tools::getValue('id_reward', false);
            $response = EtsAmAdmin::actionRewardUsage($id_reward, $action_reward);
            if ($response['success']) {
                $status = $this->l('Deducted');
                if($action_reward == 'cancel'){
                     $status = $this->l('Refunded');
                }
                die(Tools::jsonEncode(array(
                    'success' => true,
                    'message' => $this->l('The status has been successfully updated'),
                    'actions' => $response['actions'],
                    'status' => $status,
                    'user' => $response['user'],
                )));
            } else {
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->l('Reward update failed'),
                )));
            }
        }

        if ((bool)Tools::isSubmit('loadMoreSponsorFriend', false)) {
            $id_customer = (int)Tools::getValue('id_customer', false);
            $page = (int)Tools::getValue('page', false);

            if ($id_customer && $page) {
                $sponsors = Ets_Sponsor::getDetailSponsors($id_customer, array(
                    'page' => $page
                ));

                die(Tools::jsonEncode(array(
                    'success' => true,
                    'html' => $this->loadmoreSponsors($sponsors)
                )));
            }
        }
        if ((bool)Tools::isSubmit('loadMoreHistoryReward', false)) {
            $id_customer = (int)Tools::getValue('id_customer', false);
            $page = (int)Tools::getValue('page', false);

            if ($id_customer && $page) {
                $histories = EtsAmAdmin::getRewardHistory($id_customer);

                die(Tools::jsonEncode(array(
                    'success' => true,
                    'html' => $this->loadmoreRewardHistory($histories)
                )));
            }
        }

        if (($actionUser = Tools::isSubmit('actionUserReward', false)) && Validate::isCleanHtml($actionUser)) {
            if (($id_customer = (int)Tools::getValue('id_user_reward', false)) && ($action = Tools::getValue('action_user_reward', false)) && Validate::isCleanHtml($action) ) {
                $response = Ets_User::processActionStatus($id_customer, $action);
                $uStatus = $this->l('Active');
                if($action == 'decline'){
                    $uStatus = $this->l('Suspended');
                }
                if ($response) {
                    die(Tools::jsonEncode(array(
                        'success' => true,
                        'message' => $this->l('Saved successfully'),
                        'actions' => $response['actions'],
                        'status' => $uStatus
                    )));
                }
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->l('Error')
            )));
        }

        if ((bool)Tools::isSubmit('searchSuggestion', false)) {
            $query = ($query = Tools::getValue('query', '')) && Validate::isCleanHtml($query) ? $query : '';
            $query_type = ($query_type = Tools::getValue('query_type', '')) && Validate::isCleanHtml($query_type) ? $query_type : '';
            if ($query) {

                die(Tools::jsonEncode(array(
                    'success' => true,
                    'html' => $this->getSearchSuggestions($query, $query_type)
                )));
            }
        }

        if ((bool)Tools::isSubmit('deletefileBackend', false)) {
            $name_config = Tools::getValue('name_config', false);
            if ($name_config && Validate::isCleanHtml($name_config)) {
                $file = Configuration::get($name_config);
                if ($file) {
                    $path = _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $file;
                    if (unlink($path)) {
                        Configuration::updateValue($name_config, false);
                        die(Tools::jsonEncode(array(
                            'success' => true,
                            'message' => 'Deleted successfully'
                        )));
                    }
                }
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => 'Can not delete this file'
            )));

        }

        if ((bool)Tools::isSubmit('actionProgramUser', false)) {
            $id_user = (int)Tools::getValue('id_user', false);
            $program = Tools::getValue('program', false);
            $action = ($action = Tools::getValue('action_user', false)) && Validate::isCleanHtml($action) ? $action : '';
            $reason = ($reason = Tools::getValue('reason', false)) && Validate::isCleanHtml($reason) ? $reason : '';
            if ($id_user && $program && Validate::isCleanHtml($program)  && $action) {
                if (Ets_Participation::actionProgramUser($id_user, $program, $action, $reason)) {
                    die(Tools::jsonEncode(array(
                        'success' => true,
                        'message' => $this->l('Updated successfully')
                    )));
                }
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->l('Failed.')
            )));
        }

        if((bool)Tools::isSubmit('updateCronjobSecureCode', false)){
            if(($secure_code = Tools::getValue('secure_code')) && Validate::isCleanHtml($secure_code) ){
                Configuration::updateGlobalValue('ETS_AM_CRONJOB_TOKEN', $secure_code);
                die(Tools::jsonEncode(array(
                    'success' => true,
                    'message' => $this->l('Cronjob token updated successfully'),
                    'secure' => $secure_code,
                )));
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => !$secure_code ? $this->l('Cronjob secure token is required') : $this->l('Cronjob secure token is not valid'),
            )));
        }

        if((bool)Tools::isSubmit('close_cronjob_alert', false)){
            
            $cronjob_cookie = new Cookie('eam_cronjob');
            $cronjob_cookie->setExpire(time() + 12*60 * 60);
            $cronjob_cookie->closed_alert = 1;
           
            die(Tools::jsonEncode(array(
                'success' => true,
                'message' => ''
            )));
        }

    }

    /**
     * @param $ids
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getProductsAdded($ids)
    {
        if ($ids && is_array($ids)) {
            $ids_str = implode(',', $ids);
            die(Tools::jsonEncode(array(
                'html' => $this->hookDisplaySnwProductList(array('ids' => $ids_str)),
            )));
        }
    }

    public function install()
    {
        return parent::install()
            && $this->createRequiresTable()
            && $this->addIndexTable()
            && $this->registerHook('displayFooterProduct')
            && $this->registerHook('displayShoppingCartFooter')
            && $this->registerHook('displayCustomerAccount')
            && $this->registerHook('displayProductAdditionalInfo')
            && $this->registerHook('actionValidateOrder')
            && $this->registerHook('actionCustomerAccountAdd')
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayBackOfficeHeader')
            && $this->registerHook('displaySnwProductList')
            && $this->registerHook('displayAdminProductsExtra')
            && $this->registerHook('displayHome')
            && $this->registerHook('payment')
            && $this->registerHook('paymentReturn')
            && $this->registerHook('paymentOptions')
            && $this->registerHook('actionOrderStatusPostUpdate')
            && $this->registerHook('displayEtsAmFrontOfficeWithdraw')
            && $this->registerHook('displayEtsAmFrontOfficeVoucher')
            && $this->registerHook('displayEtsAmFrontOfficeRewardHistory')
/*            && $this->registerHook('displayFooterBefore')*/
            && $this->registerHook('displayFooter')
            && $this->registerHook('displayCustomerAccountForm')
            && $this->registerHook('displayRightColumnProduct')
            && $this->registerHook('actionAuthentication')
            && $this->registerHook('actionCartSave')
            && $this->registerHook('actionCustomerLogoutAfter')
            && $this->registerHook('displayOrderConfirmation')
            && $this->registerHook('cartRuleCheckValidity')
            && $this->registerHook('actionObjectOrderDetailDeleteAfter')
            && $this->registerHook('actionObjectOrderDetailAddAfter')
            && $this->registerHook('actionObjectOrderDetailUpdateAfter')
            && $this->registerHook('actionObjectOrderUpdateAfter')
            && $this->registerHook('actionFrontControllerAfterInit')
            && $this->setDefaultValues()
            && $this->__installTabs()
            && $this->setDefaultImage() && $this->installLinkDefault();
    }
    public function installLinkDefault()
    {
        $metas= array(
            array(
                'controller' => 'my_sale',
                'title' => $this->l('My sales'),
                'url_rewrite' => 'my-sales'
            ),
            array(
                'controller' => 'aff_products',
                'title' => $this->l('Affiliate Products'),
                'url_rewrite' => 'affiliate-products'
            ),
            array(
                'controller' => 'sponsorship',
                'title' => $this->l('My friends'),
                'url_rewrite' => 'my-friends'
            ),
            array(
                'controller' => 'refer_friends',
                'title' => $this->l('How to refer friends'),
                'url_rewrite' => 'how-to-refer-friends'
            ),
            array(
                'controller' => 'loyalty',
                'title' => $this->l('Loyalty program'),
                'url_rewrite' => 'loyalty-program'
            ),
            array(
                'controller' => 'dashboard',
                'title' => $this->l('Dashboard'),
                'url_rewrite' => 'affiliate-dashboard'
            ),
            array(
                'controller' => 'history',
                'title' => $this->l('Reward history'),
                'url_rewrite' => 'reward-history'
            ),
            array(
                'controller' => 'withdraw',
                'title' => $this->l('Withdraw'),
                'url_rewrite' => 'affiliate-withdraw'
            ),
            array(
                'controller' => 'voucher',
                'title' => $this->l('Convert into vouchers'),
                'url_rewrite' => 'convert-into-vouchers'
            ),
            array(
                'controller' => 'register',
                'title' => $this->l('Register program'),
                'url_rewrite' => 'register-program'
            ),
        );
        $languages = Language::getLanguages(false);
        foreach($metas as $meta)
        {
            if(!Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'meta_lang` WHERE url_rewrite ="'.pSQL($meta['url_rewrite']).'"') && !Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'meta` WHERE page ="module-'.pSQL($this->name).'-'.pSQL($meta['controller']).'"'))
            {
                $meta_class = new Meta();
                $meta_class->page = 'module-'.$this->name.'-'.$meta['controller'];
                $meta_class->configurable=1;
                foreach($languages as $language)
                {
                    $meta_class->title[$language['id_lang']] = $meta['title'];
                    $meta_class->url_rewrite[$language['id_lang']] = $meta['url_rewrite'];
                }
                $meta_class->add();
            }
            
        }
        return true;
    }
    protected function createRequiresTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_reward` (
                  `id_ets_am_reward` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `amount` decimal(20,6) DEFAULT 0,
                  `program` varchar(3) DEFAULT NULL,
                  `sub_program` varchar(3) DEFAULT NULL,
                  `status` tinyint(2) NOT NULL DEFAULT 0,
                  `datetime_added` datetime DEFAULT NULL,
                  `datetime_validated` datetime DEFAULT NULL,
                  `expired_date` datetime DEFAULT NULL,
                  `datetime_canceled` datetime DEFAULT NULL,
                  `note` varchar(55) DEFAULT NULL,
                  `id_customer` int(11) NOT NULL,
                  `id_friend` int(11) DEFAULT NULL,
                  `id_order` int(11) DEFAULT NULL,
                  `id_shop` int(11) NOT NULL,
                  `id_currency` int(11) NOT NULL,
                  `await_validate` int(11) DEFAULT 0,
                  `send_expired_email` datetime DEFAULT NULL,
                  `send_going_expired_email` datetime DEFAULT NULL,
                  `last_modified` datetime DEFAULT NULL,
                  `deleted` TINYINT UNSIGNED DEFAULT 0,
                  `used` INT(11) NOT NULL,
                  PRIMARY KEY (`id_ets_am_reward`)
                  
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";

        $sql2 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_loy_reward` (
                    `id_ets_am_loy_reward` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_product` INT(10) UNSIGNED NOT NULL,
                    `id_shop` INT(10) UNSIGNED DEFAULT NULL,
                    `use_default` TINYINT UNSIGNED DEFAULT 1,
                    `base_on` VARCHAR(20) DEFAULT NULL,
                    `amount` DECIMAL(20,6) UNSIGNED DEFAULT 0,
                    `amount_per` DECIMAL(20,6) UNSIGNED DEFAULT 0,
                    `gen_percent` DECIMAL(20,6) UNSIGNED DEFAULT 0,
                    `qty_min` INT(10) UNSIGNED DEFAULT 0,
                    PRIMARY KEY (`id_ets_am_loy_reward`)
            ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";

        $sql3 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_aff_reward`
            (
                `id_ets_am_aff_reward` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_product` INT(10) UNSIGNED NOT NULL,
                `id_shop` INT(10) UNSIGNED DEFAULT NULL,
                `use_default` TINYINT UNSIGNED DEFAULT 1,
                `how_to_calculate` VARCHAR(20) DEFAULT NULL,
                `default_percentage` DECIMAL(20,6) UNSIGNED DEFAULT NULL,
                `default_fixed_amount` DECIMAL(20,6) UNSIGNED DEFAULT NULL,
                PRIMARY KEY (`id_ets_am_aff_reward`)
            ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";

        $sql4 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_sponsor`
            (
                `id_ets_am_sponsor` INT(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_customer` INT(10) UNSIGNED NOT NULL,
                `id_parent` INT(10) UNSIGNED NOT NULL,
                `level` TINYINT UNSIGNED DEFAULT 1,
                `id_shop` INT(10) UNSIGNED DEFAULT NULL,
                `datetime_added` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id_ets_am_sponsor`)
            ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";

        $sql5 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_participation` (
                  `id_ets_am_participation` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                  `id_customer` INT(10) UNSIGNED NOT NULL,
                  `datetime_added` datetime DEFAULT NULL,
                  `status` TINYINT DEFAULT 0,
                  `program` VARCHAR(3) DEFAULT NULL,
                  `id_shop` INT(10) NOT NULL,
                  `intro` TEXT DEFAULT NULL,
                  PRIMARY KEY (`id_ets_am_participation`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql6 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_reward_usage` (
                    `id_ets_am_reward_usage` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `type` varchar(50) DEFAULT 'loy',
                    `amount` decimal(20,6) DEFAULT 0.00,
                    `id_customer` int(10) NOT NULL,
                    `id_shop` int(10) NOT NULL,
                    `id_order` int(10) DEFAULT null,
                    `id_withdraw` int(10) DEFAULT null,
                    `id_voucher` int(10) DEFAULT NULL,
                    `id_currency` int(10) DEFAULT NULL,
                    `status` tinyint(2) default 0 not null,
                    `note` varchar(55) DEFAULT null,
                    `datetime_added` datetime DEFAULT null,
                    `deleted` tinyint(2) default 0 not null,
                    PRIMARY KEY (`id_ets_am_reward_usage`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql7 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_invitation`(
                    `id_ets_am_invitation` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `email` VARCHAR(255) NOT NULL,
                    `name` VARCHAR(255) DEFAULT NULL,
                    `datetime_sent` DATETIME NULL,
                    `id_friend` INT(10) UNSIGNED DEFAULT NULL,
                    `id_sponsor` INT(10) UNSIGNED NOT NULL,
                    PRIMARY KEY (`id_ets_am_invitation`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";

        $sql8 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_banner`(
                    `id_ets_am_banner` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_sponsor` INT(10) UNSIGNED NOT NULL,
                    `datetime_added` DATETIME NULL,
                    `img` VARCHAR(255) NULL,
                    PRIMARY KEY (`id_ets_am_banner`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql9 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method`(
                    `id_ets_am_payment_method` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_shop` INT(10) UNSIGNED NOT NULL,
                    `fee_type` VARCHAR(10) DEFAULT 'FIXED',
                    `fee_fixed` DECIMAL(20,6) UNSIGNED DEFAULT NULL,
                    `fee_percent` DECIMAL(20,6) UNSIGNED DEFAULT NULL,
                    `estimated_processing_time` INT(10) DEFAULT NULL,
                    `enable` TINYINT UNSIGNED DEFAULT 0,
                    `deleted` TINYINT UNSIGNED DEFAULT 0,
                    `sort` TINYINT UNSIGNED DEFAULT 0,
                    PRIMARY KEY (`id_ets_am_payment_method`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql10 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method_lang`(
                    `id_ets_am_payment_method_lang` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_payment_method` INT(10) UNSIGNED NOT NULL,
                    `id_lang` INT(10) UNSIGNED NOT NULL,
                    `title` VARCHAR(255) NULL,
                    `description` TEXT DEFAULT NULL,
                    `note` TEXT DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_payment_method_lang`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql11 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method_field`(
                    `id_ets_am_payment_method_field` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_payment_method` INT(10) UNSIGNED NOT NULL,
                    `type` VARCHAR(20) DEFAULT 'text',
                    `sort` TINYINT UNSIGNED DEFAULT 0,
                    `required` TINYINT UNSIGNED DEFAULT 0,
                    `enable` TINYINT UNSIGNED DEFAULT 0,
                    `deleted` TINYINT UNSIGNED DEFAULT 0,
                    PRIMARY KEY (`id_ets_am_payment_method_field`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql12 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method_field_lang`(
                    `id_ets_am_payment_method_field_lang` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_payment_method_field` INT(10) UNSIGNED NOT NULL,
                    `id_lang` INT(10) UNSIGNED NOT NULL,
                    `title` VARCHAR(255) NULL,
                    `description` TEXT NULL,
                    PRIMARY KEY (`id_ets_am_payment_method_field_lang`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql13 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_withdrawal`(
                    `id_ets_am_withdrawal` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_payment_method` INT(10) UNSIGNED NOT NULL,
                    `status` TINYINT DEFAULT 0,
                    `invoice` VARCHAR(255) DEFAULT NULL,
                    `fee` FLOAT(10,2) DEFAULT NULL,
                    `fee_type` VARCHAR(255) DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_withdrawal`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql14 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_withdrawal_field`(
                    `id_ets_am_withdrawal_field` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_withdrawal` INT(10) UNSIGNED NOT NULL,
                    `id_payment_method_field` INT(10) UNSIGNED NOT NULL,
                    `value` TEXT DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_withdrawal_field`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql15 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_voucher`(
                    `id_ets_am_voucher` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_cart_rule` INT(10) UNSIGNED NOT NULL,
                    `id_customer` INT(10) UNSIGNED NOT NULL,
                    `id_product` INT(10) UNSIGNED DEFAULT NULL,
                    `id_cart` INT(10) UNSIGNED DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_voucher`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql16 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_user`(
                    `id_ets_am_user` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_customer` INT(10) UNSIGNED NOT NULL,
                    `loy` TINYINT DEFAULT 0,
                    `ref` TINYINT DEFAULT 0,
                    `aff` TINYINT DEFAULT 0,
                    `status` TINYINT DEFAULT 0,
                    `id_shop` INT(10) UNSIGNED DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_user`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql17 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_reward_product` (
                    `id_ets_am_reward_product` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_product` INT(10) UNSIGNED NOT NULL,
                    `id_ets_am_reward` INT(10) UNSIGNED NOT NULL,
                    `program` VARCHAR(3) DEFAULT NULL,
                    `quantity` INT(10) UNSIGNED NOT NULL,
                    `amount` DECIMAL (20,6) DEFAULT 0.00,
                    `id_seller` INT(10) UNSIGNED NULL,
                    `id_order` INT (10) UNSIGNED NULL,
                    `status` INT(3) DEFAULT 0,
                    `datetime_added` DATETIME DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_reward_product`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql18 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_access_key` (
                    `id_ets_am_access_key` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `key` VARCHAR(32) NOT NULL,
                    `ip_address` VARCHAR(30) DEFAULT NULL,
                    `id_seller` INT(10) UNSIGNED NOT NULL,
                    `id_product` INT(10) UNSIGNED NOT NULL,
                    `datetime_added` DATETIME DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_access_key`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql19 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_product_view` (
                    `id_ets_am_product_view` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `count` INT(10) UNSIGNED NOT NULL,
                    `id_product` INT(10) UNSIGNED NOT NULL,
                    `id_seller` INT(10) UNSIGNED NOT NULL,
                    `date_added` DATE DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_product_view`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        $sql20 = "CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "ets_am_cart_rule_seller` (
                    `id_ets_am_cart_rule_seller` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `id_customer` INT(10) UNSIGNED NOT NULL,
                    `id_cart_rule` INT(10) UNSIGNED NOT NULL,
                    `code` VARCHAR(32),
                    `date_added` DATE DEFAULT NULL,
                    PRIMARY KEY (`id_ets_am_cart_rule_seller`)
                ) ENGINE = "._MYSQL_ENGINE_." DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
        return Db::getInstance()->execute($sql)
            && Db::getInstance()->execute($sql2)
            && Db::getInstance()->execute($sql3)
            && Db::getInstance()->execute($sql4)
            && Db::getInstance()->execute($sql5)
            && Db::getInstance()->execute($sql6)
            && Db::getInstance()->execute($sql7)
            && Db::getInstance()->execute($sql8)
            && Db::getInstance()->execute($sql9)
            && Db::getInstance()->execute($sql10)
            && Db::getInstance()->execute($sql11)
            && Db::getInstance()->execute($sql12)
            && Db::getInstance()->execute($sql13)
            && Db::getInstance()->execute($sql14)
            && Db::getInstance()->execute($sql15)
            && Db::getInstance()->execute($sql16)
            && Db::getInstance()->execute($sql17)
            && Db::getInstance()->execute($sql18)
            && Db::getInstance()->execute($sql19)
            && Db::getInstance()->execute($sql20);
    }

    public function addIndexTable()
    {
        Db::getInstance()->execute("CREATE INDEX `ets_am_reward_index_c` ON `"._DB_PREFIX_."ets_am_reward` (program,sub_program,status,id_customer,id_friend,id_order,id_shop)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_loy_reward_index_c` ON `"._DB_PREFIX_."ets_am_loy_reward` (id_product,id_shop,use_default)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_aff_reward_index_c` ON `"._DB_PREFIX_."ets_am_aff_reward` (id_product,id_shop,use_default)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_sponsor_index_c` ON `"._DB_PREFIX_."ets_am_sponsor` (id_customer,id_parent,id_shop)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_participation_index_c` ON `"._DB_PREFIX_."ets_am_participation` (id_customer,status,program,id_shop)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_reward_usage_index_c` ON `"._DB_PREFIX_."ets_am_reward_usage` (id_customer,id_shop,id_order,id_withdraw,id_voucher,`deleted`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_invitation_index_c` ON `"._DB_PREFIX_."ets_am_invitation` (id_friend,id_sponsor,email)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_banner_index_c` ON `"._DB_PREFIX_."ets_am_banner` (id_sponsor)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_payment_method_index_c` ON `"._DB_PREFIX_."ets_am_payment_method` (id_shop,enable,deleted)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_payment_method_lang_index_c` ON `"._DB_PREFIX_."ets_am_payment_method_lang` (id_payment_method,id_lang)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_payment_method_field_index_c` ON `"._DB_PREFIX_."ets_am_payment_method_field` (id_payment_method,`type`,`enable`,`deleted`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_payment_method_field_lang_index_c` ON `"._DB_PREFIX_."ets_am_payment_method_field_lang` (id_payment_method_field,`id_lang`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_withdrawal_index_c` ON `"._DB_PREFIX_."ets_am_withdrawal` (id_payment_method,`status`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_withdrawal_field_index_c` ON `"._DB_PREFIX_."ets_am_withdrawal_field` (id_withdrawal,`id_payment_method_field`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_voucher_index_c` ON `"._DB_PREFIX_."ets_am_voucher` (id_cart_rule,`id_customer`,`id_product`,`id_cart`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_user_index_c` ON `"._DB_PREFIX_."ets_am_user` (`id_customer`)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_reward_product_index_c` ON `"._DB_PREFIX_."ets_am_reward_product` (id_product,id_ets_am_reward,id_order,id_seller,program)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_access_key_index_c` ON `"._DB_PREFIX_."ets_am_access_key` (id_product,id_seller)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_product_view_index_c` ON `"._DB_PREFIX_."ets_am_product_view` (id_product,id_seller)");
        Db::getInstance()->execute("CREATE INDEX `ets_am_cart_rule_seller_index_c` ON `"._DB_PREFIX_."ets_am_cart_rule_seller` (id_customer,id_cart_rule)");
        return true;
    }

    /**
     * Drop module required table
     * @return bool
     */
    protected function removeModuleTable()
    {
        Configuration::deleteByName('ETS_AM_SAVE_LOG');
        $sql = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_reward`";
        $sql2 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_loy_reward`";
        $sql3 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_aff_reward`";
        $sql4 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_sponsor`";
        $sql5 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_participation`";
        $sql6 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_reward_usage`";
        $sql7 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_invitation`";
        $sql8 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_banner`";
        $sql9 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method`";
        $sql10 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method_lang`";
        $sql11 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method_field`";
        $sql12 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_payment_method_field_lang`";
        $sql13 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_withdrawal`";
        $sql14 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_withdrawal_field`";
        $sql15 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_voucher`";
        $sql16 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_user`";
        $sql17 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_reward_product`";
        $sql18 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_access_key`";
        $sql19 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_product_view`";
        $sql20 = "DROP TABLE IF EXISTS `" . _DB_PREFIX_ . "ets_am_cart_rule_seller`";
        return DB::getInstance()->execute($sql)
            && Db::getInstance()->execute($sql2)
            && Db::getInstance()->execute($sql3)
            && Db::getInstance()->execute($sql4)
            && Db::getInstance()->execute($sql5)
            && Db::getInstance()->execute($sql6)
            && Db::getInstance()->execute($sql7)
            && Db::getInstance()->execute($sql8)
            && Db::getInstance()->execute($sql9)
            && Db::getInstance()->execute($sql10)
            && Db::getInstance()->execute($sql11)
            && Db::getInstance()->execute($sql12)
            && Db::getInstance()->execute($sql13)
            && Db::getInstance()->execute($sql14)
            && Db::getInstance()->execute($sql15)
            && Db::getInstance()->execute($sql16)
            && Db::getInstance()->execute($sql17)
            && Db::getInstance()->execute($sql18)
            && Db::getInstance()->execute($sql19)
            && Db::getInstance()->execute($sql20);
    }


    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function uninstall()
    {
        return parent::uninstall()
            && $this->removeModuleTable()
            && $this->__uninstallTabs()
            && $this->removeImages();
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getContent()
    {
        if(!Configuration::get('ETS_AM_CRONJOB_TOKEN')){
            $this->generateTokenCronjob();
        }
        $this->actionAjax();
        $submit_success = '';
        $caption = array(
            'title' => '',
            'icon' => ''
        );
        if (Tools::isSubmit('save' . $this->name)) {
            $this->postProcess();
            $submit_success = $this->displayConfirmation($this->l('Configuration saved'));
        }
        $activetab = Tools::getValue('tabActive');
        if(!$activetab || !Validate::isCleanHtml($activetab))
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure=ets_affiliatemarketing&tabActive=dashboard');
        $defined = new EtsAffDefine();
        $def_config_tabs = $defined->def_config_tabs();
        foreach ($def_config_tabs as $key => $tab) {
            if ($key == $activetab) {
                $caption['title'] = $tab['title'];
                $caption['icon'] = $tab['icon'];
            } else {
                if (isset($tab['subtabs']) && $tab['subtabs']) {
                    foreach ($tab['subtabs'] as $subkey => $subtab) {
                        if($subtab){
                            //
                        }
                        if ($subkey == $activetab) {
                            $caption['title'] = $tab['title'];
                            $caption['icon'] = $tab['icon'];
                            break;
                        }
                    }
                }
            }

            if ($caption['title']) {
                break;
            }

        }

        if (true) {
            $func = 'def_'.$activetab;
            $config_data = $defined->{$func}();
            if (isset($config_data['form'])) {
                $params = array(
                    'config' => $activetab,
                );
                $this->renderForm($params);
            } elseif ($activetab == 'applications') {
                $this->getApplications();
            } elseif ($activetab == 'reward_users') {
                if(Tools::isSubmit('deletereward_users') && ($id_customer = (int)Tools::getValue('id_customer')))
                {
                    $sqls = array();
                    $sqls[] = 'DELETE FROM `'._DB_PREFIX_.'ets_am_reward` WHERE id_customer='.(int)$id_customer.' AND id_shop='.(int)$this->context->shop->id;
                    $sqls[] = 'DELETE FROM `'._DB_PREFIX_.'ets_am_sponsor` WHERE id_parent='.(int)$id_customer .' AND id_shop='.(int)$this->context->shop->id;
                    $sqls[] = 'DELETE FROM `'._DB_PREFIX_.'ets_am_participation` WHERE id_customer='.(int)$id_customer.' AND id_shop='.(int)$this->context->shop->id;
                    $sqls[] = 'DELETE FROM `'._DB_PREFIX_.'ets_am_reward_usage` WHERE id_customer='.(int)$id_customer.' AND id_shop='.(int)$this->context->shop->id;
                    $sqls[] = 'DELETE FROM `'._DB_PREFIX_.'ets_am_voucher` WHERE id_customer='.(int)$id_customer;
                    $sqls[] = 'DELETE FROM `'._DB_PREFIX_.'ets_am_user` WHERE id_customer='.(int)$id_customer.' AND id_shop='.(int)$this->context->shop->id;
                    foreach($sqls as $sql)
                        Db::getInstance()->execute($sql);
                    Tools::redirectLink($this->context->link->getAdminLink('AdminModules').'&configure=ets_affiliatemarketing&tabActive=reward_users&conf=1');
                }
                if (($id_user = (int)Tools::getValue('id_reward_users', false)) && Tools::isSubmit('viewreward_users')) {
                    if(Tools::isSubmit('aff_search_customer'))
                        $this->ajaxSearchFriends();
                    if(Tools::isSubmit('aff_add_search_customer'))
                        $this->ajaxAddFriend();
                    $this->getDetailUser($id_user);
                } else {
                    if(Tools::isSubmit('submitAddUserReward'))
                    {
                        $id_customer=(int)Tools::getValue('id_customer');
                        $errors = array();
                        if(!$id_customer)
                        {
                            $errors[] = $this->l('Customer is required');
                        }
                        $customer_loyalty = (int)Tools::getValue('aff_customer_loyalty');
                        $customer_referral = (int)Tools::getValue('aff_customer_referral');
                        $customer_affiliate = (int)Tools::getValue('aff_customer_affiliate');
                        $aff_customer_loyalty = !Configuration::get('ETS_AM_LOYALTY_REGISTER') ? 0 : $customer_loyalty;
                        $aff_customer_referral= !Configuration::get('ETS_AM_REF_REGISTER_REQUIRED') ? 0 : $customer_referral;
                        $aff_customer_affiliate = !Configuration::get('ETS_AM_AFF_REGISTER_REQUIRED') ? 0 : $customer_affiliate;
                        if(!$customer_loyalty && !$customer_referral && !$customer_affiliate)
                            $errors[] = $this->l('Join program is required');
                        else
                        {
                            $sql ='SELECT * FROM (
                                SELECT id_customer FROM `'._DB_PREFIX_.'ets_am_participation` WHERE id_shop = '.(int)$this->context->shop->id.'  AND (0 '.($aff_customer_affiliate ? ' OR program="aff"':'').($aff_customer_loyalty ? ' OR program="loy"':'').($aff_customer_referral ? ' OR program="ref"':'').')
                            UNION
                                SELECT id_customer FROM `'._DB_PREFIX_.'ets_am_reward` r WHERE id_shop = '.(int)$this->context->shop->id.'  AND (0 '.($aff_customer_affiliate ? ' OR program="aff"':'').($aff_customer_loyalty ? ' OR program="loy"':'').($aff_customer_referral ? ' OR program="ref"':'').')
                            '.($aff_customer_referral ? '
                                UNION
                                SELECT id_parent as id_customer FROM `'._DB_PREFIX_.'ets_am_sponsor` s WHERE s.`level` = 1 AND id_shop = '.(int)$this->context->shop->id:'').'
                            UNION
                                SELECT id_customer FROM `'._DB_PREFIX_.'ets_am_user` WHERE id_shop = '.(int)$this->context->shop->id.' AND ( 0 '.($aff_customer_affiliate ? ' OR aff=1':'').($aff_customer_loyalty ? ' OR loy=1':'').($aff_customer_referral ? ' OR ref=1':'').')
                            )  app WHERE app.id_customer='.(int)$id_customer;
                            if(Db::getInstance()->getRow($sql))
                            {
                                $errors[] = $this->l('This user is already joined marketing program');
                            }
                        }
                        if(!$errors)
                        {
                            if(!Db::getInstance()->getValue('SELECT id_customer FROM `'._DB_PREFIX_.'ets_am_user` WHERE id_customer="'.(int)$id_customer.'" AND id_shop = '.(int)$this->context->shop->id))
                                Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'ets_am_user(id_customer,loy,ref,aff,status,id_shop) VALUES("'.(int)$id_customer.'","'.(int)$customer_loyalty.'","'.(int)$customer_referral.'","'.(int)$customer_affiliate.'","1","'.(int)$this->context->shop->id.'")');
                            else
                            {
                                $set_value = '';
                                if($aff_customer_loyalty || !Configuration::get('ETS_AM_LOYALTY_REGISTER'))
                                    $set_value .= ' loy = 1,';
                                if($aff_customer_affiliate || !Configuration::get('ETS_AM_AFF_REGISTER_REQUIRED'))
                                    $set_value .= ' aff = 1,';
                                if($aff_customer_referral || !Configuration::get('ETS_AM_REF_REGISTER_REQUIRED'))
                                    $set_value .= ' ref = 1,';
                                Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ets_am_user` SET '.trim($set_value,',').' WHERE  id_customer="'.(int)$id_customer.'" AND id_shop="'.(int)$this->context->shop->id.'"');
                            }
                            $this->context->smarty->assign(
                                array(
                                    'id_customer'=>$id_customer,
                                    'link'=> $this->context->link,
                                    'aff_customer' => new Customer($id_customer),
                                    'price_program' => Ets_AM::displayRewardAdmin(0),
                                    'price_widthraw' => Ets_affiliatemarketing::displayPrice(0,(int)Configuration::get('PS_CURRENCY_DEFAULT')),
                                )
                            );
                            die(
                                Tools::jsonEncode(
                                    array(
                                        'tr_html' => $this->display(__FILE__,'row_user.tpl'),
                                        'success' => $this->displaySuccessMessage($this->l('Added successfully'))
                                    )
                                )
                            );
                        }
                        else
                        {
                            die(
                                Tools::jsonEncode(
                                    array(
                                        'errors' => $this->displayError($errors)
                                    )
                                )
                            );
                        }
                    }
                    $this->renderList($defined->def_reward_users());
                }
            } elseif ($activetab == 'import_export') {
                $this->renderImportExportForm();
            } elseif (isset($config_data['list'])) {
                $func = 'def_'.$activetab;
                $list_data = $defined->{$func}();
                $params = $list_data['list'] + array(
                        'fields_list' => $list_data['fields'],
                    );
                $this->renderDatatable($params);
            } elseif ($activetab == 'dashboard') {
                $this->renderStatisticReward();
            } elseif ($activetab == 'payment_settings') {
                $this->getPaymentMethods();
            }
            elseif($activetab == 'cronjob_config'){
                $this->cronjobSettings();
            }
            elseif($activetab == 'cronjob_history'){
                $this->cronjobHistory();
            }
        }
        $this->context->controller->addJquery();
        $this->context->controller->addJs($this->_path . 'views/js/tree.js');
        $this->context->controller->addJqueryUI('ui.sortable');
        $this->context->controller->addJqueryUI('ui.datepicker');
        $this->context->controller->addJS($this->_path . 'views/js/other.js');
        if ($activetab == 'dashboard') {
            $this->context->controller->addJS($this->_path . 'views/js/chart.js');
        }
        if ($activetab == 'dashboard' || $activetab == 'rs_program_reward_history' || $activetab == 'affiliate_reward_history' || $activetab == 'loyalty_reward_history' || $activetab == 'applications' || $activetab == 'withdraw_list' || $activetab == 'reward_history' || !$activetab) {

            $this->context->controller->addCss($this->_path . 'views/css/daterangepicker.css');
            $this->context->controller->addJs($this->_path . 'views/js/moment.min.js');
            $this->context->controller->addJs($this->_path . 'views/js/daterangepicker.js');
        }
        $cookie_filter = $this->context->cookie->getFamily('reward_usersFilter_');
        $setting_tabs = array();
        $breadcrumb_admin = array();
        $def_config_tabs = $defined->def_config_tabs();
        $menuActive = $activetab;

        foreach ($def_config_tabs as $key => $tab) {
            if($key == 'loyalty_program' || $key == 'affiliate_program' || $key == 'rs_program'){
                if($menuActive == $key){
                    $menuActive = 'marketing_program';
                }
                $setting_tabs['marketing_program']['sub'][$key] = $tab;
                $setting_tabs['marketing_program']['img'] = 'marketing_program.png';
                $setting_tabs['marketing_program']['title'] = $this->l('Marketing programs');

                $breadcrumb_admin['marketing_program']['title'] = $this->l('Marketing programs');
                $breadcrumb_admin['marketing_program']['subtabs'][$key] =  $tab;

                if(isset($tab['subtabs']) && is_array($tab['subtabs'])){
                    foreach ( $tab['subtabs'] as $ks=>$isub){
                        if($menuActive == $ks && $isub){
                            $menuActive = 'marketing_program';
                            break;
                        }
                    }
                }
            }
            else if ($key == 'usage_settings' || $key == 'reward_history' || $key == 'withdraw_list'){
                if($menuActive == $key){
                    $menuActive = 'rewards';
                }
                $setting_tabs['rewards']['sub'][$key] =$tab;
                $setting_tabs['rewards']['img'] = 'rewards.png';
                $setting_tabs['rewards']['title'] = $this->l('Rewards');

                $breadcrumb_admin['rewards']['title'] = $this->l('Rewards');
                $breadcrumb_admin['rewards']['subtabs'][$key] =  $tab;

                if(isset($tab['subtabs']) && is_array($tab['subtabs'])){
                    foreach ( $tab['subtabs'] as $ks=>$isub){
                        if($menuActive == $ks && $isub){
                            $menuActive = 'rewards';
                            break;
                        }
                    }
                }
            }
            else if ($key == 'applications' || $key == 'reward_users'){
                if($menuActive == $key){
                    $menuActive = 'customers';
                }
                $setting_tabs['customers']['sub'][$key] =$tab;
                $setting_tabs['customers']['img'] = 'customers.png';
                $setting_tabs['customers']['title'] = $this->l('Customers');
                
                $breadcrumb_admin['customers']['title'] = $this->l('Customers');
                $breadcrumb_admin['customers']['subtabs'][$key] =  $tab;
                if(isset($tab['subtabs']) && is_array($tab['subtabs'])){
                    foreach ( $tab['subtabs'] as $ks=>$isub){
                        if($menuActive == $ks && $isub){
                            $menuActive = 'customers';
                            break;
                        }
                    }
                }
            }
            else{
                $setting_tabs[$key] = $tab;
                if(isset($tab['subtabs']) && is_array($tab['subtabs'])){
                    foreach ( $tab['subtabs'] as $ks=>$isub){
                        if($menuActive == $ks && $isub){
                            $menuActive = $key;
                            break;
                        }
                    }
                }
            }
            if($key == 'othermodules' && isset($this->refs)){
                $setting_tabs[$key]['class'] = 'refs_othermodules';
                $setting_tabs[$key]['link'] = $this->refs.$this->context->language->iso_code;
                $setting_tabs[$key]['target'] = '_blank';
            }
        }

        $this->smarty->assign(array(
            'html' => $this->_html,
            'config_tabs' => $def_config_tabs,
            'activetab' => $activetab,
            'menuActive' => $menuActive,
            'link_tab' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name,
            'linkJs' => $this->_path . 'views/js/admin.js',
            'caption' => $caption,
            'currency' => Currency::getDefaultCurrency(),
            'cookie_filter' => $cookie_filter,
            'submit_errors' => $this->_errors ? 1 : 0,
            'setting_tabs' => $setting_tabs,
            'linkImg' =>  $this->_path.'views/img/',
            'breadcrumb_admin' => $breadcrumb_admin,
            'idRewardUser' => (int)Tools::getValue('id_reward_users')
        ));

        $output = $this->_errors ? $this->displayError($this->_errors) : $submit_success;
        return $output . $this->display(__FILE__, 'admin_form.tpl');
    }

    /**
     * @param $params
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function renderForm($params)
    {
        if (!$params || !isset($params['config'])) {
            return false;
        }
        $defined = new EtsAffDefine();
        $func_config = 'def_'.$params['config'];
        $configForm = $defined->{$func_config}();
        $fields_form = array();
        $fields_form['form'] = $configForm['form'];
        if (!empty($fields_form['form']['buttons'])) {
            $fields_form['form']['buttons']['back']['href'] = AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . (($tab = Tools::getValue('tabActive', false)) && Validate::isCleanHtml($tab) ? '&tabActive=' . $tab : '');
        }
        $configs = $configForm['config'];
        
        if ($configs) {
            foreach ($configs as $key => $config) {
                $config_fields = array(
                    'name' => $key,
                    'type' => $config['type'],
                    'label' => $config['label'],
                    'desc' => isset($config['desc']) ? $config['desc'] : false,
                    'required' => isset($config['required']) && $config['required'] ? true : false,
                    'autoload_rte' => isset($config['autoload_rte']) && $config['autoload_rte'] ? true : false,
                    'options' => isset($config['options']) && $config['options'] ? $config['options'] : array(),
                    'multiple' => isset($config['multiple']) && $config['multiple'],
                    'form_group_class' => isset($config['form_group_class']) ? $config['form_group_class'] :false,
                    'values' => $config['type'] == 'switch' ? array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ) : (isset($config['values']) && $config['values'] ? $config['values'] : false),
                    'lang' => isset($config['lang']) ? $config['lang'] : false,
                    'col' => isset($config['col']) ? $config['col'] : '9',
                    'rows' => isset($config['rows']) ? $config['rows'] : '3',
                    'group' => isset($config['group']) ? $config['group'] : false,
                    'class' => isset($config['class']) ? $config['class'] : false,
                    'is_image' => isset($config['is_image']) ? $config['is_image'] : false,
                    'size' => isset($config['size']) ? $config['size'] : false,
                    'caption_before' => isset($config['caption_before']) ? $config['caption_before'] : false,
                    'divider_before' => isset($config['divider_before']) ? $config['divider_before'] : false,
                    'default' => isset($config['default']) && $config['default'] ? $config['default'] : false,
                    'fill' => isset($config['fill']) && $config['fill'] ? $config['fill'] : false,
                    'id' => isset($config['id']) && $config['id'] ? $config['id'] : '',
                    'items' => isset($config['items']) && $config['items'] ? $config['items'] : array(),
                    '_currencies' => isset($config['_currencies']) && $config['_currencies'] ? $config['_currencies'] : array(),
                    'default_currency' => isset($config['default_currency']) && $config['default_currency'] ? $config['default_currency'] : array(),
                );
                if (isset($config['tree']))
                    $config_fields['tree'] = $config['tree'];
                if (!empty($config['suffix']))
                    $config_fields = $config_fields + array('suffix' => $config['suffix']);
                if (!empty($config['cols']))
                    $config_fields = $config_fields + array('cols' => $config['cols']);
                if (!empty($config['rows']))
                    $config_fields = $config_fields + array('rows' => $config['rows']);
                if (!empty($config['img_dir']))
                    $config_fields = $config_fields + array('img_dir' => $config['img_dir']);
                if (!empty($config['group']))
                    $config_fields = $config_fields + array('group' => $config['group']);

                if (!$config_fields['multiple']) {
                    unset($config_fields['multiple']);
                } elseif ($config['type'] == 'select' && stripos($config_fields['name'], '[]') === false) {
                    $config_fields['name'] .= '[]';
                }
                array_push($fields_form['form']['input'], $config_fields);
            }
        }

        $language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper = new HelperForm();
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->table = $this->table;
        $helper->default_form_language = $language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save' . $this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . $this->getUrlParams();
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->override_folder = '/';
        $helper->show_cancel_button = false;
        $fields = array();
        $languages = Language::getLanguages(false);
        if (Tools::isSubmit('save' . $this->name)) {
            if ($configs) {
                foreach ($configs as $key => $config) {

                    if (isset($config['lang']) && $config['lang']) {
                        foreach ($languages as $l) {
                            $fields[$key][$l['id_lang']] = Tools::getValue($key . '_' . $l['id_lang'], isset($config['default']) ? $config['default'] : '');
                        }
                    } elseif ($config['type'] == 'select' && isset($config['multiple']) && $config['multiple']) {
                        $fields[$key . ($config['type'] == 'select' ? '[]' : '')] = Tools::getValue($key, array());
                    } elseif ($config['type'] == 'ets_checkbox_group') {
                        $fields[$key] = implode(',', Tools::getValue($key, array()));
                    } elseif ($config['type'] == 'file') {
                        $fields[$key] = $this->getFields(false, $key, $config);
                    } else {
                        $fields[$key] = Tools::getValue($key, isset($config['default']) ? $config['default'] : '');
                    }
                }
            }
        } else {
            if ($configs) {
                $obj = !empty($params['obj']) ? $params['obj'] : false;
                foreach ($configs as $key => $config) {
                    if (isset($config['lang']) && $config['lang']) {
                        foreach ($languages as $l) {
                            $fields[$key][$l['id_lang']] = $this->getFields($obj, $key, $config, (int)$l['id_lang']);
                        }
                    } elseif ($config['type'] == 'select' && isset($config['multiple']) && $config['multiple']) {
                        $fields[$key . ($config['type'] == 'select' ? '[]' : '')] = ($result = $this->getFields($obj, $key, $config)) != '' ? explode(',', $result) : array();
                    } else {

                        $fields[$key] = $this->getFields($obj, $key, $config);
                    }
                }
            }
        }

        $helper->tpl_vars = array(
            'table' => isset($fields_form['form']['name']) && $fields_form['form']['name'] ? $fields_form['form']['name'] : null,
            'base_url' => $this->context->shop->getBaseURL(),
            'language' => array(
                'id_lang' => $language->id,
                'iso_code' => $language->iso_code
            ),
            'fields_value' => $fields,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'path_banner' => Ets_AM::getBaseURL(true) . EAM_PATH_IMAGE_BANER
        );
        $fields_form['form']['submit'] = array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        );
        $this->_html .= $helper->generateForm(array($fields_form));
    }

    /**
     * @return void
     */
    public function hookDisplayBackOfficeHeader()
    {
        //Ets_Sponsor::checkRewardRef($this->context->cart);
        $configure = Tools::getValue('configure');
        if($configure == $this->name){
            $this->context->controller->addJquery();
        }
        if(Tools::isSubmit('close_popup_demo'))
        {
            $this->context->cookie->ok_button_demo=1;
            $this->context->cookie->write();
            die('1');
        }
        if ($configure == $this->name && !Tools::isSubmit('tabActive') && !Tools::isSubmit('getTotalUserAppPending')) {
            $cronjob_cookie = new Cookie('eam_cronjob');
            $cronjob_cookie->setExpire(time() + 30 * 60);
            $cronjob_cookie->closed_alert = 0;
        }
        $this->context->controller->addCss($this->_path . 'views/css/admin_all.css');
        $controller = Tools::getValue('controller');
        if ($configure == $this->name && $controller=='AdminModules') {
            $this->context->controller->addCss($this->_path . 'views/css/admin.css');
            $this->context->controller->addCss($this->_path . 'views/css/other.css');
        }
        if (!$this->is17) {
            $this->context->controller->addCss($this->_path . 'views/css/admin16.css');
        }
        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=') && version_compare(_PS_VERSION_, '1.7.7.0', '<'))
            $this->context->controller->addJS(_PS_JS_DIR_ . 'jquery/jquery-'._PS_JQUERY_VERSION_.'.min.js');
        else
            $this->context->controller->addJquery();
        if ($configure == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/d3.v3.min.js');
            $this->context->controller->addJS($this->_path . 'views/js/nv.d3.min.js');
            $this->context->controller->addCss($this->_path . 'views/css/nv.d3.css');
            $this->context->controller->addCss($this->_path . 'views/css/header.css');
        } elseif($controller=='AdminProducts') {
            $this->context->controller->addCss($this->_path . 'views/css/admin_product.css');
            
            if($this->is17){
                $this->context->controller->addJs($this->_path . 'views/js/admin_product.js');
            }
        }
        $this->context->controller->addJs($this->_path . 'views/js/admin_all.js');
    }

    /*== Add tab to sidebar admin === */
    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function __installTabs()
    {
        $languages = Language::getLanguages(false);
        $tab = new Tab();
        $tab->class_name = 'AdminEtsAm';
        $tab->module = $this->name;
        if(!$this->is17){
          $tab->icon = 'trophy';  
        }
        $tab->id_parent = 0;
        foreach ($languages as $lang) {
            $tab->name[$lang['id_lang']] = $this->l('Marketing programs');
        }
        $tab->save();
        $eam_tab_id = Tab::getIdFromClassName('AdminEtsAm');
        if ($eam_tab_id) {
            $subTabs = array();
            $defined = new EtsAffDefine();
            $def_config_tabs = $defined->def_config_tabs();
            foreach ($def_config_tabs as $tb) {
                if (isset($tb['class']) && $tb['class'] && isset($tb['title']) && $tb['title'] && $tb['class']!='othermodules') {
                    $ct = array(
                        'class_name' => $tb['class'],
                        'tab_name' => $tb['title']
                    );
                    if ($this->is17) {
                        $ct['icon'] = isset($tb['icon17']) && $tb['icon17'] ? $tb['icon17'] : '';
                    } else {
                        $ct['icon'] = isset($tb['icon']) && $tb['icon'] ? $tb['icon'] : '';
                    }
                    array_push($subTabs, $ct);
                }
            }
            foreach ($subTabs as $tabArg) {
                $tab = new Tab();
                $tab->class_name = $tabArg['class_name'];
                $tab->module = $this->name;
                $tab->icon = $tabArg['icon'];
                $tab->id_parent = $eam_tab_id;
                foreach ($languages as $lang) {
                    $tab->name[$lang['id_lang']] = $tabArg['tab_name'];
                }
                $tab->save();
            }
        }
        return true;
    }

    /**
     * @return bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    private function __uninstallTabs()
    {
        $defined = new EtsAffDefine();
        $tabs = $defined->def_config_tabs();
        if (!empty($tabs)) {
            foreach ($tabs as $item) {
                if (isset($item['class']) && $item['class'] && $item['class']!='othermodules') {
                    if ($tabId = Tab::getIdFromClassName($item['class'])) {
                        $tab = new Tab($tabId);
                        if ($tab) {
                            $tab->delete();
                        }
                    }
                }
            }
        }
        if ($tabId = Tab::getIdFromClassName('AdminEtsAm')) {
            $tab = new Tab($tabId);
            if ($tab) {
                $tab->delete();
            }
        }
    return true;
    }


    /**
     * @return string
     */
    public function getUrlParams()
    {
        $params = '';
        if (($currentTabs = Tools::getValue('tabActive', false)) && Validate::isCleanHtml($currentTabs))
            $params .= '&tabActive=' . $currentTabs;
        if (($subTab = Tools::getValue('subTab', false)) && Validate::isCleanHtml($subTab) )
            $params .= '&subTab=' . $subTab;
        return $params;
    }

    /**
     * @param $obj
     * @param $key
     * @param $config
     * @param bool $id_lang
     * @return bool|int|null|string
     */
    public function getFields($obj, $key, $config, $id_lang = false)
    {
        $default_value = false;
        if (isset($config['default']) && $config['default']) {
            $default_value = $config['default'] === true ? 1 : $config['default'];
        }
        if ($obj) {
            if (!$obj->id)
                return (isset($config['default']) ? $config['default'] : null);
            elseif ($id_lang)
                return !empty($obj->{$key}) && !empty($obj->{$key}[$id_lang]) ? $obj->{$key}[$id_lang] : '';
            else
                return $obj->$key;
        } else {
            if ($id_lang)
                return ($value = Configuration::get($key, $id_lang)) || (Configuration::get($key, $id_lang) !== false && Configuration::get($key, $id_lang) == 0) ? $value : $default_value;
            else
                return ($value = Configuration::get($key)) || (Configuration::get($key, $id_lang) !== false && Configuration::get($key, $id_lang) == 0) ? $value : $default_value;
        }
    }

    /**
     * @return void
     * @throws PrestaShopException
     */
    private function postProcess()
    {
        if (Tools::isSubmit('save' . $this->name)) {
           
            $tabActive = Tools::getValue('tabActive', 'general_settings');
            if (!$this->_errors && Validate::isCleanHtml($tabActive)) {
                $defined = new EtsAffDefine();
                $func = 'def_'.$tabActive;
                if (method_exists($defined, $func)) {
                    $tab_fields = $defined->{$func}();
                    if (isset($tab_fields['config']) && !empty($tab_fields['config'])) {
                        $params = array(
                            'tab' => $tabActive,
                            'configs' => $tab_fields['config']
                        );
                        $this->processSave($params);
                    }
                }
            }
        }
    }

    /**
     * @param $params
     * @return bool
     * @throws PrestaShopException
     */
    private function processSave($params)
    {
        if (empty($params)) {
            return false;
        }
        $languages = Language::getLanguages(false);
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $configs = array();
        if (isset($params['configs']) && $params['configs']) {
            $configs = $params['configs'];
        }
        $obj = isset($params['obj']) ? $params['obj'] : false;
        if ($configs) {
            foreach ($configs as $key => $config) {
                if (isset($config['lang']) && $config['lang']) {
                    $val_lang_default = Tools::getValue($key . '_' . $id_lang_default);
                    if (isset($config['required']) && $config['required'] && $config['type'] != 'switch' && trim($val_lang_default) == '') {
                        $this->_errors[] = sprintf($this->l('%s is required'),$config['label']);
                    }elseif($val_lang_default && !Validate::isCleanHtml($val_lang_default))
                        $this->_errors[] = sprintf($this->l('%s is not valid'),$config['label']);
                    else
                    {
                        foreach($languages as $lang)
                        {
                            $val_lang = Tools::getValue($key . '_' . $lang['id_lang']);
                            if($val_lang && !Validate::isCleanHtml($val_lang))
                                $this->_errors[] = sprintf($this->l('%s is not valid in %s'),$config['label'],$lang['iso_code']);
                        }
                    }
                } else {
                    $val = Tools::getValue($key);
                    if (isset($config['required']) && $config['required'] && $config['type'] != 'switch' && !$val) {
                        $this->_errors[] = sprintf($this->l('%s is required'),$config['label']);
                    } elseif (!is_array($val) && isset($config['validate']) && method_exists('Validate', $config['validate'])) {
                        if ($key !== 'ETS_AM_LOYALTY_AMOUNT' && $key !== 'ETS_AM_LOYALTY_AMOUNT' && $key !== 'ETS_AM_LOYALTY_BASE_ON') {
                            $validate = $config['validate'];
                            if (trim($val) && !Validate::$validate(trim($val))) {
                                $this->_errors[] = sprintf($this->l('%s is invalid'),$config['label']);
                            }
                            unset($validate);
                        }
                    } elseif (!is_array($val) && !Validate::isCleanHtml(trim($val))) {
                        if ($key !== 'ETS_AM_LOYALTY_AMOUNT' && $key !== 'ETS_AM_LOYALTY_AMOUNT' && $key !== 'ETS_AM_LOYALTY_BASE_ON')
                            $this->_errors[] = sprintf($this->l('%s is invalid'),$config['label']);
                    }
                    elseif(is_array($val) && !Ets_affiliatemarketing::validateArray($val))
                        $this->_errors[] = sprintf($this->l('%s is invalid'),$config['label']);
                    if ($key == 'ETS_AM_LOYALTY_TIME') {
                        if ($val !== 'ALL') {
                            $from_date = Tools::getValue('ETS_AM_LOYALTY_TIME_FROM');
                            if($from_date && !Validate::isDate($from_date))
                                $this->_errors[] = $this->l('From date is not valid');
                            $to_date = Tools::getValue('ETS_AM_LOYALTY_TIME_TO');
                            if($to_date && !Validate::isCleanHtml($to_date))
                                $this->_errors[] = $this->l('To date is not valid');
                            if ($from_date && $to_date) {
                                if (date(strtotime($from_date)) > date(strtotime($to_date))) {
                                    $this->_errors[] = $this->l('From date must be smaller than To date');
                                }
                            } else {
                                $this->_errors[] = $this->l('From date and To date must be fill');
                            }
                        }
                    }
                    if ($key == 'ETS_AM_AFF_OFFER_VOUCHER') {
                        if ($val) {
                            $this->validatePromoCode(EAM_AM_AFFILIATE_REWARD);
                        }
                    }
                    if ($key == 'ETS_AM_REF_OFFER_VOUCHER') {
                        if ($val) {
                            $this->validatePromoCode(EAM_AM_REF_REWARD);
                        }
                    }
                    if ($key == 'ETS_AM_WAITING_STATUS'/* || $key == 'ETS_AM_VALIDATED_STATUS' || $key == 'ETS_AM_CANCELED_STATUS'*/) {
                        $waiting_states = Tools::getValue('ETS_AM_WAITING_STATUS', array());
                        $validated_states = Tools::getValue('ETS_AM_VALIDATED_STATUS', array());
                        $canceled_states = Tools::getValue('ETS_AM_CANCELED_STATUS', array());
                        if(!Ets_affiliatemarketing::validateArray($waiting_states) || !Ets_affiliatemarketing::validateArray($validated_states) || !Ets_affiliatemarketing::validateArray($canceled_states))
                            $this->_errors[] = $this->l('Status of reward should be unique in 3 cases: Awaiting, Approved and Canceled');
                        else
                        {
                            $dublicate_states1 = array_intersect($waiting_states, $validated_states);
                            $dublicate_states2 = array_intersect($waiting_states, $canceled_states);
                            $dublicate_states3 = array_intersect($canceled_states, $validated_states);
    
                            if ($dublicate_states1 || $dublicate_states2 || $dublicate_states3) {
                                $this->_errors[] = $this->l('Status of reward should be unique in 3 cases: Awaiting, Approved and Canceled');
                            }
                        }
                    }
                    if ($config['type'] == 'file') {
                        $file_types = isset($config['file_type']) && $config['file_type'] ? $config['file_type'] : null;
                        $file_size_allow = isset($config['file_size']) && $config['file_size'] ? $config['file_size'] : null;
                        if (isset($_FILES[$key]['tmp_name']) && $_FILES[$key]['error'] <= 0 && $file_types) {
                            if(!Validate::isFileName(str_replace(' ', '_', $_FILES[$key]['name']))){
                                $this->_errors[] = sprintf($this->l('The file name is invalid "%s"'), $_FILES[$key]['name']);
                            }
                            else{
                                $file_data = $_FILES[$key];
                                $file_ext = pathinfo($file_data['name'], PATHINFO_EXTENSION);
                                $file_types = explode(',', $file_types);
                                $file_size = (int)$file_data['size'] / 1024;
                                if (!in_array($file_ext, $file_types)) {
                                    $this->_errors[] = sprintf($this->l('The file name "%s" is not in the correct format, accepted formats: %s'),$_FILES[$key]['name'],'.'.trim(implode(', .',$file_types),', .'));
                                } else if ($file_size > $file_size_allow) {
                                    $this->_errors[] = sprintf($this->l('%s is too large. Please upload file less than or equal to %s'),$config['label'],Tools::ps_round($file_size_allow,2).'Mb');
                                }
                            }

                        }
                        if(isset($_FILES[$key]['name']) && $_FILES[$key]['name'] && !Validate::isFileName(str_replace(' ', '_',$_FILES[$key]['name'])))
                            $this->_errors[] = sprintf($this->l('The file name "%s" is invalid'),$_FILES[$key]['name']);
                    }
                    if (($key == 'ETS_AM_LOYALTY_BASE_ON') && ($BASE_ON = Tools::getValue($key)))
                    {
                        if ($BASE_ON == 'FIXED' || $BASE_ON == 'SPC_FIXED')
                        {
                            $amount = Tools::getValue('ETS_AM_LOYALTY_AMOUNT');
                            if (!$amount) {
                                $this->_errors[] = $this->l('Amount field is required');
                            } elseif (!Validate::isUnsignedFloat($amount)) {
                                $this->_errors[] = $this->l('Amount must be an unsigned number');
                            }
                        }
                        else
                        {
                            $percent = Tools::getValue('ETS_AM_LOYALTY_GEN_PERCENT');
                            if (!$percent) {
                                $this->_errors[] = $this->l('Percentage field is required');
                            } elseif (!Validate::isUnsignedFloat($percent)) {
                                $this->_errors[] = $this->l('Percentage must be an unsigned number');
                            }
                        }
                    }
                }
            }
        }

        $errors_validate = EtsAmAdmin::validateDataConfig($params['tab']);
        $this->_errors = array_merge($this->_errors, $errors_validate);
        if (!$this->_errors) {
            if ($configs) {
                foreach ($configs as $key => $config) {
                    /*$default_value = null;
                    if (isset($config['default']) && $config['default']) {
                        $default_value = $config['default'];
                    }*/
                    if (isset($config['lang']) && $config['lang']) {
                        $values = array();
                        $val_lang_default = trim(Tools::getValue($key . '_' . $id_lang_default));
                        foreach ($languages as $lang) {
                            if ($config['type'] == 'switch') {
                                $val = (int)trim(Tools::getValue($key . '_' . $lang['id_lang']));
                                $values[$lang['id_lang']] = $val ? 1 : 0;
                            } else {
                                $val_lang = trim(Tools::getValue($key . '_' . $lang['id_lang']));
                                $values[$lang['id_lang']] = $val_lang && Validate::isCleanHtml($val_lang) ? $val_lang : (Validate::isCleanHtml($val_lang_default) ? $val_lang_default:'');
                            }
                        }
                        $this->setFields($obj, $key, $values, true);
                    } else {
                        if ($config['type'] == 'switch') {
                            $val = (int)trim(Tools::getValue($key));
                            $this->setFields($obj, $key, $val ? 1 : 0, true);
                        } elseif ($config['type'] == 'select' && isset($config['multiple']) && $config['multiple']) {
                            $val = Tools::getValue($key);
                            if(Ets_affiliatemarketing::validateArray($val))
                                $this->setFields($obj, $key, implode(',', $val));
                        }
                        elseif($config['type']=='categories_tree' || $config['type']=='categories'){
                            $val = Tools::getValue($key);
                            if(Ets_affiliatemarketing::validateArray($val))
                                Configuration::updateValue($key,$val ? implode(',',$val):'');
                        } elseif ($config['type'] == 'ets_checkbox_group') {
                            if (Tools::getIsset($key)) {
                                $val = ($getVal = Tools::getValue($key, array())) && Ets_affiliatemarketing::validateArray($getVal) && !in_array('ALL', $getVal) && ($result = implode(',', $getVal)) != 'ALL' ? $result : 'ALL';

                                $this->setFields($obj, $key, $val);
                            } else {
                                $this->setFields($obj, $key, null);
                            }
                        } elseif ($config['type'] == 'ets_radio_group') {
                            if (Tools::getIsset($key)) {
                                $val = Tools::getValue($key, '');
                                if(Validate::isCleanHtml($val))
                                    $this->setFields($obj, $key, $val);
                            } else {
                                $this->setFields($obj, $key, null);
                            }
                        } elseif ($config['type'] == 'text_search_prd') {
                            if (Tools::getIsset($key)) {
                                $val = Tools::getValue($key, '');
                                if(Validate::isCleanHtml($val))
                                    $this->setFields($obj, $key, $val);
                            } else {
                                $this->setFields($obj, $key, null);
                            }
                        } elseif ($config['type'] == 'file' && isset($config['is_image']) && $config['is_image']) {
                            $path_img = $this->uploadImage($key);
                            if ($path_img) {
                                $this->setFields($obj, $key, $path_img);
                            }
                        } elseif ($key == 'ETS_AM_LOYALTY_CATEGORIES') {
                            $key_values= Tools::getValue($key);                            
                            if ($key_values == 'ALL') {
                                $this->setFields($obj, $key, 'ALL');
                            } else {
                                if(is_array($key_values) && !empty($key_values) ){
                                    if(Ets_affiliatemarketing::validateArray($key_values))
                                        $this->setFields($obj, $key, implode(',', $key_values));
                                }
                                else{
                                     $this->setFields($obj, $key, 'ALL');
                                }
                            }
                        } elseif ($key != 'position') {
                            $field_value = Tools::getValue($key);
                            if ($field_value!==false) {
                                if ($key == 'ETS_AM_REF_SPONSOR_COST_LEVEL_1') {
                                    EtsAmAdmin::saveSponsorLevel();
                                } 
                                if (!is_array($field_value)) {
                                    $field_value = Validate::isCleanHtml($field_value)  ?trim($field_value):'';
                                } else {
                                    $field_value = Ets_affiliatemarketing::validateArray($field_value) ? implode(',', $field_value):'';
                                }
                                $this->setFields($obj, $key, $field_value, true);
                            }
                        }
                    }
                }
                if($params['tab']=='rs_program_voucher')
                {
                    $cart_rules = Db::getInstance()->executeS('SELECT id_cart_rule FROM '._DB_PREFIX_.'ets_am_cart_rule_seller');
                    if($cart_rules)
                        foreach($cart_rules as $cart_rule)
                            $this->saveCartRule($cart_rule['id_cart_rule']);
                }
            }
        }
        //custom multishop.
        if (!count($this->_errors) && isset($obj->id_shop)) {
            $shops = Shop::getShops(false);
            $result = true;
            if (Shop::CONTEXT_ALL == Shop::getContext() && count($shops) > 1 && !$obj->id) {
                foreach ($shops as $shop) {
                    $obj->id_shop = (int)$shop['id_shop'];
                    $obj->position = (int)$obj->maxVal($shop['id_shop']) + 1;
                    $result &= $obj->add();
                }
            } else {
                if (!$obj->id)
                    $obj->position = (int)$obj->maxVal() + 1;
                $result &= ($obj->id && $obj->update() || !$obj->id && $obj->add());
            }
            if ($result) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', true) . '&conf=4&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name . (!empty($params['tab']) ? '&tabActive=' . $params['tab'] : '') . (!empty($params['list_id']) ? '&id_' . $params['list_id'] . '=' . $obj->id . '&update' . $params['list_id'] : ''));
            }
        }
    }

    /**
     * Validate promo code
     *
     * @param $program
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    protected function validatePromoCode($program)
    {
        if ($program == EAM_AM_AFFILIATE_REWARD) {
            $type = Tools::getValue('ETS_AM_AFF_VOUCHER_TYPE');
            $voucherCode = Tools::getValue('ETS_AM_AFF_VOUCHER_CODE');
        } elseif ($program == EAM_AM_REF_REWARD) {
            $type = Tools::getValue('ETS_AM_REF_VOUCHER_TYPE');
            $voucherCode = Tools::getValue('ETS_AM_REF_VOUCHER_CODE');
        } else {
            $type = false;
        }
        if (!$type) {
            $this->_errors[] = $this->l('Voucher type field is required.');
            return;
        }
        if ($type == 'FIXED') {
            if (!$voucherCode) {
                $this->_errors[] = $this->l('Voucher code is required.');
                return;
            }
            if (!Validate::isGenericName($voucherCode)) {
                $this->_errors[] = $this->l('Voucher code is invalid.');
                return;
            }
            $promoId = CartRule::cartRuleExists($voucherCode);
            if ($promoId) {
                if ($cartRule = new CartRule($promoId)) {
                    if (!$cartRule->active) {
                        $this->_errors[] = $this->l('Voucher is not active.');
                        return;
                    }
                }
            } else {
                $this->_errors[] = $this->l('Voucher does not exists.');
                return;
            }
        }
    }

    /**
     * @param $obj
     * @param $key
     * @param $values
     * @param bool $html
     */
    public function setFields($obj, $key, $values, $html = false)
    {
        if ($obj) {
            $obj->$key = $values;
        } else {
            Configuration::updateValue($key, $values, $html);
        }
    }

    /**
     * @param $type
     * @param null $label
     * @param null $value
     * @return bool|string
     */
    public function displaySmarty($type, $label = null, $value = null)
    {
        if (!$type)
            return false;
        $assign = array(
            'type' => $type,
            'label' => $label
        );
        if ($value)
            $assign = array_merge($assign, array(
                'value' => $value
            ));
        $this->smarty->assign($assign);
        return $this->display(__FILE__, 'admin-smarty.tpl');
    }

    /**
     * @param array $params
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function getRspRewardHistory($params = array())
    {
        return Ets_Sponsor::getRspRewardHistory($params);
    }

    /**
     * @param $key
     * @return bool|string
     */
    private function uploadImage($key)
    {
        if (isset($_FILES[$key])) {
            $allowExtentions = array('png', 'jpg', 'jpeg', 'gif');
            $imagesize = @getimagesize($_FILES[$key]['tmp_name']);
            $ext = Tools::strtolower(Tools::substr(strrchr($_FILES[$key]['name'], '.'), 1));
            $ext2 = Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1));
            if ($_FILES[$key]['error'] <= 0 && in_array($ext, $allowExtentions) && in_array($ext2, $allowExtentions)) {
                // $name = $_FILES[$key]["name"];
                $img_exists = Configuration::get($key);
                $tmp_name = microtime().rand(1111, 99999) . $img_exists;
                if ($img_exists) {
                    rename(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $img_exists, _PS_ROOT_DIR_ . '/modules/' . $this->name . '/cache/' . $tmp_name);
                    @unlink(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $img_exists);
                }
                $success = false;
                if (!ImageManager::validateUpload($_FILES[$key], 2097152)) {
                    $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                    if (move_uploaded_file($_FILES[$key]['tmp_name'], $temp_name)) {
                        Ets_AM::createPath(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER);

                        $img_name = Tools::strtolower($key) . '.' . $ext;
                        $resize_with = null;
                        $resize_height = null;
                        if ($key == 'ETS_AM_REF_DEFAULT_BANNER') {
                            $img_name = 'default_banner' . '.' . $ext;
                            if ((int)Configuration::get('ETS_AM_RESIZE_BANNER')) {
                                $resize_with = (int)Configuration::get('ETS_AM_RESIZE_BANNER_WITH');
                                $resize_height = (int)Configuration::get('ETS_AM_RESIZE_BANNER_HEIGHT');
                            }
                        }
                        $path_img = _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $img_name;
                        if (ImageManager::resize($temp_name, $path_img, $resize_with, $resize_height, $ext)) {
                            $success = true;
                            if (isset($temp_name)) {
                                @unlink($temp_name);
                            }
                            @unlink(_PS_ROOT_DIR_ . '/modules/' . $this->name . '/cache/' . $tmp_name);
                            return $img_name;
                        }
                        if (isset($temp_name)) {
                            @unlink($temp_name);
                        }
                    }
                }
                if (!$success && $img_exists) {
                    rename(_PS_ROOT_DIR_ . '/modules/' . $this->name . '/cache/' . $tmp_name, _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $img_exists);
                    @unlink(_PS_ROOT_DIR_ . '/modules/' . $this->name . '/cache/' . $tmp_name);
                    return $img_exists;
                }
                return false;
            }
        }
        return false;
    }

    /**
     * @param $params
     * @throws PrestaShopException
     */
    public function hookActionObjectOrderDetailDeleteAfter($params)
    {
        if(Configuration::get('ETS_AM_RECALCULATE_COMMISSION'))
        {
            $order_detail = $params['object'];
            $order = new Order($order_detail->id_order);
            $cart_loyalty = Ets_Loyalty::getOrderReward($order,null,true);
            if($cart_loyalty)
            {
                $amount = is_array($cart_loyalty)? (float)$cart_loyalty['reward'] : (float)$cart_loyalty;
                $data = array(
                    'id_friend' => $order->id_customer,
                    'amount' => $amount,
                    'program' => EAM_AM_LOYALTY_REWARD,
                    'id_currency' => (int)Configuration::get('PS_CURRENCY_DEFAULT'),
                    'datetime_added' => date('Y-m-d H:i:s'),
                );
                $data['id_customer'] = (int)$order->id_customer;
                $data['id_order'] = $order->id;
                $data['id_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
                $products = is_array($cart_loyalty) && isset($cart_loyalty['products'])? $cart_loyalty['products'] : array();
                if($products)
                    $data['note'] = sprintf($this->l('Purchased loyalty product (Order: #%s)'), $order->id);
                else
                    $data['note'] = sprintf($this->l('Purchased loyalty shopping cart (Order: #%s)'), $order->id);
                self::updateAmReward($data, $products);
            }
        }
    }
    public function hookActionObjectOrderDetailUpdateAfter($params)
    {
        if(Configuration::get('ETS_AM_RECALCULATE_COMMISSION'))
        {
            $order_detail = $params['object'];
            $order = new Order($order_detail->id_order);
            $cart_loyalty = Ets_Loyalty::getOrderReward($order,null,true);
            if($cart_loyalty)
            {
                $amount = is_array($cart_loyalty)? (float)$cart_loyalty['reward'] : (float)$cart_loyalty;
                $data = array(
                    'id_friend' => $order->id_customer,
                    'amount' => $amount,
                    'program' => EAM_AM_LOYALTY_REWARD,
                    'id_currency' => (int)Configuration::get('PS_CURRENCY_DEFAULT'),
                    'datetime_added' => date('Y-m-d H:i:s'),
                );
                $data['id_customer'] = (int)$order->id_customer;
                $data['id_order'] = $order->id;
                $data['id_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
                $products = is_array($cart_loyalty) && isset($cart_loyalty['products'])? $cart_loyalty['products'] : array();
                if($products)
                    $data['note'] = sprintf($this->l('Purchased loyalty product (Order: #%s)'), $order->id);
                else
                    $data['note'] = sprintf($this->l('Purchased loyalty shopping cart (Order: #%s)'), $order->id);
                self::updateAmReward($data, $products);
            }
        }
    }
    public function hookActionObjectOrderUpdateAfter($params)
    {
        if(Configuration::get('ETS_AM_RECALCULATE_COMMISSION'))
        {
            $loyalty_product = ($loyalty = Configuration::get('ETS_AM_LOYALTY_BASE_ON')) != 'SPC_FIXED' && $loyalty && $loyalty != 'SPC_PERCENT';
            if(!$loyalty_product)
            {
                $order = $params['object'];
                $cart_loyalty = Ets_Loyalty::getOrderReward($order,null,true);
                if($cart_loyalty)
                {
                    $amount = is_array($cart_loyalty)? (float)$cart_loyalty['reward'] : (float)$cart_loyalty;
                    $data = array(
                        'id_friend' => $order->id_customer,
                        'amount' => $amount,
                        'program' => EAM_AM_LOYALTY_REWARD,
                        'id_currency' => (int)Configuration::get('PS_CURRENCY_DEFAULT'),
                        'datetime_added' => date('Y-m-d H:i:s'),
                    );
                    $data['id_customer'] = (int)$order->id_customer;
                    $data['id_order'] = $order->id;
                    $data['id_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
                    $products = is_array($cart_loyalty) && isset($cart_loyalty['products'])? $cart_loyalty['products'] : array();
                    if($products)
                        $data['note'] = sprintf($this->l('Purchased loyalty product (Order: #%s)'), $order->id);
                    else
                        $data['note'] = sprintf($this->l('Purchased loyalty shopping cart (Order: #%s)'), $order->id);
                    self::updateAmReward($data, $products);
                }
            }
        }
        
    }
    public function hookActionObjectOrderDetailAddAfter($params)
    {
        if(Configuration::get('ETS_AM_RECALCULATE_COMMISSION'))
        {
            $order_detail = $params['object'];
            $order = new Order($order_detail->id_order);
            $cart_loyalty = Ets_Loyalty::getOrderReward($order,null,true);
            if($cart_loyalty)
            {
                $amount = is_array($cart_loyalty)? (float)$cart_loyalty['reward'] : (float)$cart_loyalty;
                $data = array(
                    'id_friend' => $order->id_customer,
                    'amount' => $amount,
                    'program' => EAM_AM_LOYALTY_REWARD,
                    'id_currency' => (int)Configuration::get('PS_CURRENCY_DEFAULT'),
                    'datetime_added' => date('Y-m-d H:i:s'),
                );
                $data['id_customer'] = (int)$order->id_customer;
                $data['id_order'] = $order->id;
                $data['id_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
                $products = is_array($cart_loyalty) && isset($cart_loyalty['products'])? $cart_loyalty['products'] : array();
                if($products)
                    $data['note'] = sprintf($this->l('Purchased loyalty product (Order: #%s)'), $order->id);
                else
                    $data['note'] = sprintf($this->l('Purchased loyalty shopping cart (Order: #%s)'), $order->id);
                self::updateAmReward($data, $products);
            }
        }
        
    }
    protected function updateAmReward($data,$products= array())
    {
        if($id_reward = Db::getInstance()->getValue('SELECT id_ets_am_reward FROM `'._DB_PREFIX_.'ets_am_reward` WHERE id_order="'.(int)$data['id_order'].'" AND program="'.pSQL($data['program']).'"'))
        {
            $reward = new Ets_AM($id_reward);
            $reward->amount = $data['amount'];
            if ($reward->update() && $products) {
                Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ets_am_reward_product` WHERE id_ets_am_reward="'.(int)$reward->id.'"');
                foreach ($products as $product) {
                    $product_reward = new Ets_Reward_Product();
                    $product_reward->id_product = $product['id_product'];
                    $product_reward->quantity = (int)$product['quantity'];
                    $product_reward->id_ets_am_reward = $reward->id;
                    $product_reward->amount = $product['reward_amount'];
                    $product_reward->id_order = $data['id_order'];
                    $product_reward->id_seller = $data['id_customer'];
                    $product_reward->program = $data['program'];
                    $product_reward->datetime_added = date('Y-m-d H:i:s');
                    $product_reward->add();
                }
            }
            return $reward ? $reward : false;
        }
    }
    public function hookActionValidateOrder($params)
    {
        if (!(isset($params['cart'])) || !(isset($params['order'])) || !($cart = $params['cart']) || !($order = $params['order']))
            return;
        if($order->module == $this->name)
        {
            $this->actionPaymentByReward($params['order']);
        }
        if (($cart_loyalty = Ets_Loyalty::calculateCartTotalReward($params, $this->context, true)) && ($amount = is_array($cart_loyalty)? (float)$cart_loyalty['reward'] : (float)$cart_loyalty)) {
            if((float)$amount){
                $data = array(
                    'id_friend' => $cart->id_customer,
                    'amount' => $amount,
                    'program' => EAM_AM_LOYALTY_REWARD,
                    'id_currency' => (int)Configuration::get('PS_CURRENCY_DEFAULT'),
                    'datetime_added' => date('Y-m-d H:i:s'),
                );
//            if (($day = Configuration::get('ETS_AM_LOYALTY_MAX_DAY'))) {
//                $data['expired_date'] = date('Y-m-d H:i:s', strtotime($day . ' days'));
//            }
                $data['id_customer'] = (int)$this->context->customer->id;
                $data['id_shop'] = $cart->id_shop;
                $data['id_order'] = $order->id;

                $data['id_currency'] = Configuration::get('PS_CURRENCY_DEFAULT');
                $products = is_array($cart_loyalty) && isset($cart_loyalty['products'])? $cart_loyalty['products'] : array();
                if($products)
                    $data['note'] = sprintf($this->l('Purchased loyalty product (Order: #%s)'), $order->id);
                else
                    $data['note'] = sprintf($this->l('Purchased loyalty shopping cart (Order: #%s)'), $order->id);
                $rewardLoy = self::createNewAmReward($data, $products);
                Ets_Loyalty::sendEmailToAdminWhenNewRewardCreated(self::$trans['new-reward'], $rewardLoy);
                Ets_Loyalty::sendEmailToCustomerWhenNewRewardCreated(self::$trans['reward_created_for_you'], $rewardLoy);
            }

        }
        if($aff_customer = $this->context->cookie->__get(EAM_AFF_CUSTOMER_COOKIE))
        {
            $aff_customer = explode('-',$aff_customer);
            $aff_product =  explode('-', $this->context->cookie->__get(EAM_AFF_PRODUCT_COOKIE));
            $customers = array();
            foreach($aff_customer as $key => $customer)
            {
                if(!isset($customers[$customer]))
                    $customers[$customer]= array($aff_product[$key]);
                else
                    $customers[$customer][]= $aff_product[$key];
            }
            if($customers)
            {
                $i = 0;
                foreach($customers as $id_customer=> $aff_pro)
                {
                    $i++;
                    $cartAffiliate = Ets_Affiliate::calculateAffiliateCartReward($cart, $this->context, true,$id_customer,$aff_pro, count($customers) == $i ? true :false);
                    if ($cartAffiliate && $cartAffiliate !== 0) {
                        if ($this->context->cookie->__get(EAM_AFF_CUSTOMER_COOKIE)) {
                            if ($this->context->cookie->__get(EAM_AFF_PRODUCT_COOKIE)) {
                                if (is_array($cartAffiliate)) {
                                    $products = $cartAffiliate['products'];
                                    $amount = $cartAffiliate['reward'];
                                } else {
                                    $products = array();
                                    $amount = $cartAffiliate;
                                }
                                if ($amount > 0) {
                                    $date = date('Y-m-d H:i:s');
                                    $data = array(
                                        'id_friend' => $cart->id_customer,
                                        'amount' => $amount,
                                        'program' => EAM_AM_AFFILIATE_REWARD,
                                        'datetime_added' => $date,
                                        'datetime_canceled' => null,
                                        'datetime_validated' => null,
                                        'id_customer' => $id_customer,
                                        'id_shop' => $params['cart']->id_shop,
                                        'id_order' => $params['order']->id,
                                        'note' => sprintf($this->l('Affiliate commission (Order: #%s)'), $params['order']->id),
                                        'id_currency' => Configuration::get('PS_CURRENCY_DEFAULT')
                                    );
                                    $data['expired_date'] = null;
                                    $reward = self::createNewAmReward($data, $products);
                                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RC')){
                                        Ets_Affiliate::sendEmailWhenAffiliateRewardCreated($this->l('A new reward was created'), $reward, true);
                                    }
                                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RC')){
                                        Ets_Affiliate::sendEmailWhenAffiliateRewardCreated($this->l('A new reward created for you'), $reward, false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if (!$this->context->cookie->__get(EAM_REFS)) {
            $ref = Ets_Sponsor::getIdRefByCart($cart->id, $this->context->customer->id);

        }
        if(isset($ref) && $ref) {
            $this->context->cookie->__unset(EAM_REFS);
            $this->context->cookie->__unset('ets_am_show_voucher_ref');
            if(Ets_Sponsor::addFriendSponsored($ref))
                Ets_Sponsor::getRewardWithoutOrder($ref);
        }
        Ets_Sponsor::getRewardWithFirstOrder($cart, $order);
        Ets_Sponsor::getRewardOnOrder($params);
    }

    /**
     * @param $data
     * @param array $products
     * @return bool|Ets_AM
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    protected function createNewAmReward($data, $products = array())
    {
        $reward = new Ets_AM();
        foreach ($data as $key => $value) {
            $reward->{$key} = $value;
        }
        $reward->add(true, true) ? $reward : false;
        if ($reward->id && $products) {
            foreach ($products as $product) {
                $product_reward = new Ets_Reward_Product();
                $product_reward->id_product = $product['id_product'];
                $product_reward->quantity = (int)$product['quantity'];
                $product_reward->id_ets_am_reward = $reward->id;
                $product_reward->amount = $product['reward_amount'];
                $product_reward->id_order = $data['id_order'];
                $product_reward->id_seller = $data['id_customer'];
                $product_reward->program = $data['program'];
                $product_reward->datetime_added = date('Y-m-d H:i:s');
                $product_reward->add();
            }
        }
        return $reward ? $reward : false;
    }

    /**
     * @param $params
     * @return null|string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */

    public function hookDisplayShoppingCartFooter($params)
    {
        $context = $this->context;
        if($context->customer->id && (Configuration::get('ETS_AM_AFF_ALLOW_VOUCHER_IN_CART')===false || Configuration::get('ETS_AM_AFF_ALLOW_VOUCHER_IN_CART')))
        {
            $total_earn = Ets_Reward_Usage::getTotalEarn(null, $context->customer->id, $context);
            $total_spent = Ets_Reward_Usage::getTotalSpent($context->customer->id, false, null, $context);
            $total_balance = $total_earn - $total_spent;
        }      
        if(!Ets_Loyalty::isCustomerSuspendedOrBannedLoyaltyProgram($this->context->customer->id)){
            $cart = $params['cart'];
            $message = Ets_Loyalty::getCartMessage($cart, $this->context);
        }
        $convert_message = Configuration::get('ETS_AM_AFF_CONVERT_VOUCHER_MSG',$this->context->language->id) ? Configuration::get('ETS_AM_AFF_CONVERT_VOUCHER_MSG',$this->context->language->id): $this->l('You have [available_reward_to_convert] in your balance. It can be converted into voucher code. [Convert_now]');
        $this->smarty->assign(array(
            'message' => isset($message) && $message ? $message:false,
            'link'=>$this->context->link,
            'convert_message' => $convert_message,
            'total_balance' => isset($total_balance) && $total_balance ? Ets_AM::displayRewardInMsg($total_balance, $this->context):false,
        ));
        $button = $this->display(__FILE__,'convert_now_button.tpl'); 
        $this->context->smarty->assign('convert_now_button',$button);
        return $this->display(__FILE__, 'cart-message.tpl');
    }

    /**
     * @return bool
     */
    private function setDefaultValues()
    {
        $this->generateTokenCronjob();
        $languages = Language::getLanguages(false);
        $order_state_exists = Db::getInstance()->getRow("SELECT * FROM `"._DB_PREFIX_."order_state` WHERE `module_name` = '".pSQL($this->name)."'");
        $orderState = new OrderState();
        if($order_state_exists){
            $orderState = new OrderState((int)$order_state_exists['id_order_state']);
        }
        foreach ($languages as $lang) {
            $orderState->name[(int)$lang['id_lang']] = $this->l('Reward payment accepted');
        }
        $orderState->invoice = 0;
        $orderState->send_email = false;
        $orderState->module_name = $this->name;
        $orderState->color = '#32CD32';
        $orderState->unremovable = 1;
        $orderState->paid = 1;
        if ($orderState->save()) {
            $source = _PS_MODULE_DIR_.$this->name.'/views/img/temp/os_payment.gif';
            $destination = _PS_ROOT_DIR_.'/img/os/'.(int) $orderState->id.'.gif';
            copy($source, $destination);
            Configuration::updateValue('PS_ETS_AM_REWARD_PAID', $orderState->id);
        }
        

        $obj = false;
        $defined = new EtsAffDefine();
        $def_config_tabs = $defined->def_config_tabs();
        if ($def_config_tabs) {
            foreach ($def_config_tabs as $keytab => $tab) {
                if (isset($tab['subtabs']) && $tab['subtabs']) {
                    foreach ($tab['subtabs'] as $k => $subtab) {
                        if($subtab){
                            //
                        }
                        $func = 'def_'.$k;
                        if(!method_exists($defined, $func)){
                            continue;
                        }
                        $cfg = $defined->{$func}();
                        if ($cfg && isset($cfg['form']) && isset($cfg['config']) && $cfg['config']) {
                            $configs = $cfg['config'];
                            $this->insertDefaultData($configs, $languages, $obj);
                        }
                    }
                } else {
                    $func = 'def_'.$keytab;    
                    if(!method_exists($defined, $func)){
                        continue;
                    }
                    $cfg = $defined->{$func}();
                    if ($cfg && isset($cfg['form']) && isset($cfg['config']) && $cfg['config']) {
                        $configs = $cfg['config'];
                        $this->insertDefaultData($configs, $languages, $obj);
                    }
                }
            }
        }

        //Create default payment method
        $pm_params = array();
        $pmf_params = array(
            array(
                'type' => 'text',
                'required' => 1,
                'enable' => 1,
                'sort' => 1,
            ),
            array(
                'type' => 'text',
                'required' => 1,
                'enable' => 1,
                'sort' => 2,
            ),
            array(
                'type' => 'text',
                'required' => 1,
                'enable' => 1,
                'sort' => 3,
            ),
            array(
                'type' => 'text',
                'required' => 1,
                'enable' => 1,
                'sort' => 4,
            ),
        );
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $pm_params['title'][$lang['id_lang']] = $this->l('PayPal');
            $pm_params['desc'][$lang['id_lang']] = $this->l('The fastest method to withdraw funds, directly to your local bank account!');
            $pm_params['note'][$lang['id_lang']] = null;

            foreach ($pmf_params as &$p) {
                if($p['sort'] == 1){
                    $p['title'][$lang['id_lang']] = $this->l('First name');
                    $p['desc'][$lang['id_lang']] = $this->l('Type your first name');
                }
                elseif($p['sort'] == 2){
                    $p['title'][$lang['id_lang']] = $this->l('Last name');
                    $p['desc'][$lang['id_lang']] = $this->l('Type your last name');
                }
                elseif($p['sort'] == 3){
                    $p['title'][$lang['id_lang']] = $this->l('PayPal email');
                    $p['desc'][$lang['id_lang']] = $this->l('Type your PayPal email to receive money');
                }
                elseif($p['sort'] == 4){
                    $p['title'][$lang['id_lang']] = $this->l('Phone');
                    $p['desc'][$lang['id_lang']] = $this->l('Type your phone');
                }
            }
        }
        $pm_params['fee_fixed'] = 1;
        $pm_params['fee_type'] = 'NO_FEE';
        $pm_params['fee_percent'] = null;
        $pm_params['enable'] = 1;
        $pm_params['estimate_processing_time'] = 7;
        $pm = new Ets_PaymentMethod();
        $id_pm = $pm->createPaymentMethod($pm_params);
        if($id_pm){
            foreach ($pmf_params as $pmf_param) {
                $pmf = new Ets_PaymentMethodField();
                $pmf_param['id_payment_method'] = $id_pm;
                $pmf->createPaymentMethodField($pmf_param);
            }
        }

        //Set cookie notification cronjob
        $cronjob_cookie = new Cookie('eam_cronjob');
        $cronjob_cookie->setExpire(time() + 12*60 * 60);
        $cronjob_cookie->closed_alert = 1;
        return true;
    }

    public function setDefaultImage()
    {
        if(!is_dir(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER))
            mkdir(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER);    
        @copy(_PS_ROOT_DIR_ . '/modules/ets_affiliatemarketing/views/img/temp/default_popup_banner.jpg', _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . 'ets_am_ref_intro_banner.jpg');
        @copy(_PS_ROOT_DIR_ . '/modules/ets_affiliatemarketing/views/img/temp/default_banner.jpg', _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . 'default_banner.jpg');
        // Configuration::updateValue('ETS_AM_REF_INTRO_BANNER', 'ets_am_ref_intro_banner.jpg');
        return true;
    }

    private function insertDefaultData($configs, $languages, $obj)
    {
        foreach ($configs as $key => $config) {

            $default_value = false;
            if (isset($config['default']) && $config['default']) {
                $default_value = $config['default'];
            }
            if (isset($config['lang']) && $config['lang']) {
                $values = array();
                foreach ($languages as $lang) {
                    if ($config['type'] == 'switch') {
                        $values[$lang['id_lang']] = (int)$default_value ? 1 : 0;
                    } else {
                        $values[$lang['id_lang']] = $default_value ? $default_value : null;
                    }
                }
                $this->setFields($obj, $key, $values, true);
            } else {
                if ($config['type'] == 'switch') {
                    $this->setFields($obj, $key, (int)$default_value);
                } elseif ($config['type'] == 'select' && isset($config['multiple']) && $config['multiple']) {
                    if ($default_value) {
                        $this->setFields($obj, $key, $default_value);
                    } else {
                        $this->setFields($obj, $key, null);
                    }
                } elseif ($config['type'] == 'ets_checkbox_group') {
                    $checkbox_value = array();
                    if (isset($config['values']) && $config['values']) {
                        foreach ($config['values'] as $option) {
                            if (isset($option['default']) && $option['default'] && isset($option['value']) && $option['value']) {
                                $checkbox_value[] = $option['value'];
                            }
                        }
                    }
                    $this->setFields($obj, $key, implode(',', $checkbox_value));

                } elseif ($config['type'] == 'ets_radio_group') {
                    $radio_value = null;
                    if (isset($config['values']) && $config['values']) {
                        foreach ($config['values'] as $option) {
                            if (isset($option['default']) && $option['default'] && isset($option['value']) && $option['value']) {
                                $radio_value = $option['value'];
                                break;
                            }
                        }
                    }
                    $this->setFields($obj, $key, $radio_value);
                } elseif ($config['type'] == 'ets_radio_group_tree') {
                    $tree_value = null;
                    if (isset($config['values']) && $config['values']) {
                        foreach ($config['values'] as $option) {
                            if (isset($option['default']) && $option['default'] && isset($option['value']) && $option['value']) {
                                $tree_value = $option['value'];
                                break;
                            }
                        }
                    }

                    $this->setFields($obj, $key, $tree_value);
                } elseif ($config['type'] == 'text_search_prd') {
                    $this->setFields($obj, $key, null);
                } elseif ($config['type'] == 'file' && isset($config['is_image']) && $config['is_image']) {
                    $this->setFields($obj, $key, $default_value);
                } elseif ($key != 'position') {

                    if ($default_value) {
                        $this->setFields($obj, $key, $default_value, true);
                    } else {
                        $this->setFields($obj, $key, null, true);
                    }
                }
            }
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hookDisplayHeader()
    {
        if(!$this->is17)
            $this->checkCartRuleValidity();
        if(Tools::isSubmit('affSubmitSharEmail'))
        {
            $errors = array();
            $to = trim(Tools::getValue('aff_mails'));
            $aff_product_share_link = Tools::getValue('aff_product_share_link');
            $aff_message = Tools::getValue('aff_message');
            $aff_name = Tools::getValue('aff_name');
            $aff_product_share_name = Tools::getValue('aff_product_share_name');
            if(!Configuration::get('ETS_AM_AFF_ENABLED'))
                $errors[] = $this->l('You do not have permission to send this email');
            else
            {
                if(!$to)
                    $errors[] = $this->l('Email is required');
                elseif(!Validate::isEmail($to))
                    $errors[]= $this->l('Email is not valid');
                if($aff_product_share_link && !Validate::isCleanHtml($aff_product_share_link))
                    $errors[] = $this->l('Product link is not valid');
                if($aff_message && !Validate::isCleanHtml($aff_message))
                    $errors[] = $this->l('Message is not valid');
                if($aff_name && !Validate::isCleanHtml($aff_name))
                    $errors[] = $this->l('Name is not valid');
                if($aff_product_share_name && !Validate::isCleanHtml($aff_product_share_name))
                    $errors[] = $this->l('Product name is not valid');
            }
            if(!$errors)
            {
                $data = array(
                    '{link_product}' => $aff_product_share_link,
                    '{aff_message}' => $aff_message,
                    '{aff_name}' => $aff_name,
                    '{name_product}' => $aff_product_share_name,
                    '{customer_name}' => $this->context->customer->firstname.' '.$this->context->customer->lastname,
                );
                if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
                    if(!$id_lang = Db::getInstance()->getValue('SELECT id_lang FROM `'._DB_PREFIX_.'customer` WHERE email="'.pSQL($to).'"'))
                        $id_lang = $this->context->language->id;
                    $subjects = array(
                        'translation' => $this->l('Your friend has shared a product with you'),
                        'origin'=> 'Your friend has shared a product with you',
                        'specific'=>false
                    );
                    Ets_aff_email::send($id_lang,'aff_share_product',$subjects,$data,$to);
                }
                die(
                    Tools::jsonEncode(
                        array(
                            'success' => $this->displaySuccessMessage($this->l('Email was sent successfully')),
                        )
                    )
                );
            }
            else
            {
                die(
                    Tools::jsonEncode(
                        array(
                            'errors' => $this->displayError($errors),
                        )
                    )
                );
            }            
        }
        $lang = $this->context->language->id;
        $aff_customer = (int)Tools::getValue('affp');
        $aff_product = (int)Tools::getValue('id_product');
        $aff = false;
        if ($aff_customer && ($customer = new Customer($aff_customer)) && Validate::isLoadedObject($customer) && Ets_Affiliate::isCustomerBelongToValidAffiliateGroup($customer)) {
            $aff = true;
        }
        if ($aff) {
            if ($aff_product) {
                if ($aff_customer) {
                    $aff_products_cookie = $this->getCookie(EAM_AFF_PRODUCT_COOKIE);
                    $aff_customers = $this->getCookie(EAM_AFF_CUSTOMER_COOKIE);
                    if($aff_customers)
                        $aff_customers_array = explode('-',$aff_customers);
                    else
                        $aff_customers_array = array();
                    if ($aff_products_cookie) {
                        $aff_products_cookie = explode('-', $aff_products_cookie);
                        if (!in_array($aff_product, $aff_products_cookie)) {
                            $aff_products_cookie[] = $aff_product;
                            $aff_customers_array[] = $aff_customer;
                        }
                        else
                        {
                            foreach($aff_products_cookie as $key=>$id_product)
                            {
                                if($id_product == $aff_product)
                                    $aff_customers_array[$key] = $aff_customer;
                            }
                        }
                    } else {
                        $aff_products_cookie = array($aff_product);
                        $aff_customers_array[] = $aff_customer; 
                    }
                    if($aff_customers_array)
                        $this->setCookie(EAM_AFF_CUSTOMER_COOKIE, implode('-',$aff_customers_array));
                    $aff_products_cookie = implode('-', $aff_products_cookie);
                    $this->setCookie(EAM_AFF_PRODUCT_COOKIE, $aff_products_cookie);
                    if($this->context->customer->logged)
                        $this->hookActionCartSave();
                }
                $visited_products = $this->getCookie(EAM_AFF_VISITED_PRODUCTS);
                if ($visited_products) {
                    $visited_products = explode('-', $visited_products);
                    if (!in_array($aff_product, $visited_products)) {
                        $visited_products[] = $aff_product;
                    }
                } else {
                    $visited_products = array($aff_product);
                }
                $visited_products = implode('-', $visited_products);
                $this->setCookie(EAM_AFF_VISITED_PRODUCTS, $visited_products);
            }
        }
        if (($ref = Tools::getValue('refs')) && Validate::isCleanHtml($ref)) {
            Ets_Sponsor::setCookieRef($ref);
            $this->smarty->assign(array(
                'og_url' => Ets_AM::getBaseUrl() . '?refs=' . $ref,
                'og_type' => 'article',
                'og_title' => Configuration::get('ETS_AM_REF_SOCIAL_TITLE', $lang) ? Configuration::get('ETS_AM_REF_SOCIAL_TITLE', $lang) : '',
                'og_description' => Configuration::get('ETS_AM_REF_SOCIAL_DESC', $lang) ? Configuration::get('ETS_AM_REF_SOCIAL_DESC', $lang) : '',
                'og_image' => Configuration::get('ETS_AM_REF_SOCIAL_IMG') ? Ets_AM::getBaseUrl() . EAM_PATH_IMAGE_BANER . Configuration::get('ETS_AM_REF_SOCIAL_IMG') : '',
                '_token' => md5($this->name.'-'.$this->version)
            ));
        }
        if(($module = Tools::getValue('module')) && $module =='ets_affiliatemarketing')
        {
            $this->context->controller->addJS($this->_path . 'views/js/front/ets_affiliatemarketing.js');
            $this->context->controller->addCss(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/css/nv.d3.css');
            $this->context->controller->addCss(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/css/daterangepicker.css');
            $this->context->controller->addJs(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/js/d3.v3.min.js');
            $this->context->controller->addJs(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/js/nv.d3.min.js');
            $this->context->controller->addJs(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/js/moment.min.js');
            $this->context->controller->addJs(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/js/daterangepicker.js');
        }
        $this->context->controller->addCss($this->_path . 'views/css/front.css');
        if(!$this->is17){
           $this->context->controller->addCss($this->_path . 'views/css/front16.css'); 
        }
        $this->context->controller->addJS($this->_path . 'views/js/front.js');
        if (($controller = Tools::getValue('controller')) && $controller == 'product' && $aff_customer) {
            $id_product = (int)Tools::getValue('id_product');
            $this->smarty->assign(array(
                'ets_am_product_view_link' => Ets_AM::getBaseUrlDefault('product_view',array('id_product'=>$id_product,'affp'=>$aff_customer)),
                'eam_id_seller' => $aff_customer ? (int)$aff_customer : 0
            ));
        }
        $this->smarty->assign(array(
            'link_cart' => $this->context->link->getPageLink('cart', Tools::usingSecureMode() ? true : false),
            'link_reward' => $this->context->link->getModuleLink($this->name, 'dashboard', array('ajax'=>1), Tools::usingSecureMode() ? true : false),
            'link_shopping_cart' => $this->context->link->getModuleLink('ps_shoppingcart', 'ajax', array(), Tools::usingSecureMode() ? true : false),
            '_token' => md5($this->name.'-'.$this->version)
        ));
        return $this->display(__FILE__, 'head.tpl');
    }

    /**
     * @param $key
     * @param $value
     * @return void
     * @throws Exception
     */

    public function setCookie($key, $value)
    {
        $this->context->cookie->__set($key, $value);
    }

    /**
     * @param $key
     * @return string
     */
    public function getCookie($key)
    {
        return $this->context->cookie->__get($key);
    }

    /**
     * @param $params
     * @return string
     * @throws PrestaShopException
     */
    public function hookDisplayAdminProductsExtra($params)
    {
        if (!($loyaltyBaseOn = Configuration::get('ETS_AM_LOYALTY_BASE_ON')))
            return;
        $id_product = (int)Tools::getValue('id_product');
        if ((isset($params['id_product']) && (int)$params['id_product']) || $id_product) {
            $id_product = isset($params['id_product']) && (int)$params['id_product'] ? (int)$params['id_product'] : $id_product;
            $this->_id_product = $id_product;
            $id_shop = $this->context->shop->id;
            $loyalty_reward = array();
            $loyalty_reward_fields = array(
                'ETS_AM_LOYALTY_BASE_ON', 'ETS_AM_LOYALTY_AMOUNT', 'ETS_AM_LOYALTY_AMOUNT_PER', 'ETS_AM_LOYALTY_GEN_PERCENT', 'ETS_AM_QTY_MIN'
            );
            $aff_reward = array();
            $aff_reward_fields = array(
                'ETS_AM_AFF_HOW_TO_CALCULATE',
                'ETS_AM_AFF_DEFAULT_PERCENTAGE',
                'ETS_AM_AFF_DEFAULT_FIXED_AMOUNT'
            );

            $loyalty_reward_data = EtsAmAdmin::getLoyaltySettings($id_product, $id_shop);
            $aff_reward_data = EtsAmAdmin::getAffiliateSettings($id_product, $id_shop);

            $defined = new EtsAffDefine();
            if ($loyaltyBaseOn && $loyaltyBaseOn !== 'DYNAMIC') {
                if (isset($defined->def_reward_settings()['config']) && $defined->def_reward_settings()['config']) {
                    foreach ($defined->def_reward_settings()['config'] as $key => $config) {
                        if (in_array($key, $loyalty_reward_fields)) {
                            $name = Tools::strtolower(str_replace('ETS_AM_LOYALTY_', '', $key));
                            $name = str_replace('ets_am_', '', $name);
                            $config['class'] = $key;
                            if (!empty($loyalty_reward_data)) {
                                $config['value'] = $loyalty_reward_data[$name];
                            }
                            if ($key == 'ETS_AM_LOYALTY_BASE_ON') {
                                $loyalty_bases = $config['values'];
                                unset($loyalty_bases['SPC_FIXED'], $loyalty_bases['SPC_PERCENT']);
                                $config['values'] = $loyalty_bases;
                            }
                            $loyalty_reward[$name] = $config;
                        }
                    }
                    $loyalty_reward['use_default'] = !empty($loyalty_reward_data) ? $loyalty_reward_data['use_default'] : 1;
                }
            }
            if (isset($defined->def_affiliate_reward_caculation()['config']) && $defined->def_affiliate_reward_caculation()['config']) {
                foreach ($defined->def_affiliate_reward_caculation()['config'] as $key => $config) {
                    if (in_array($key, $aff_reward_fields)) {
                        $name = Tools::strtolower(str_replace('ETS_AM_AFF_', '', $key));
                        $config['class'] = $key;
                        if (!empty($aff_reward_data)) {
                            $config['value'] = $aff_reward_data[$name];
                        }
                        $aff_reward[$name] = $config;
                    }
                }
                $aff_reward['use_default'] = !empty($aff_reward_data) ? $aff_reward_data['use_default'] : 1;
            }
            $aff_excluded = Configuration::get('ETS_AM_AFF_PRODUCTS_EXCLUDED') ? explode(',',Configuration::get('ETS_AM_AFF_PRODUCTS_EXCLUDED')):array();
            $discount_excluded = Configuration::get('ETS_AM_AFF_PRODUCTS_EXCLUDED_DISCOUNT') && $this->checkSpecificProudct($id_product);
            $this->smarty->assign(array(
                'settings' => array(
                    'aff_reward' => !in_array($id_product,$aff_excluded) && !$discount_excluded ? $aff_reward :array(),
                    'loyalty_reward' => Configuration::get('ETS_AM_LOYALTY_BASE_ON')=='SPC_FIXED' || Configuration::get('ETS_AM_LOYALTY_BASE_ON') =='SPC_PERCENT' ? false : $loyalty_reward
                ),
                'loyalty_base_on' => $loyaltyBaseOn,
                'id_product' => $id_product,
                'linkAjax' => $this->context->link->getAdminLink('AdminModules', true) . '&conf=4&configure=' . $this->name,
                'using_cart' => Configuration::get('ETS_AM_LOYALTY_BASE_ON') == 'SPC_FIXED' || Configuration::get('ETS_AM_LOYALTY_BASE_ON') == 'SPC_PERCENT' ? 1 : 0,
                'is17' => $this->is17,
                'linkJs' => $this->_path . 'views/js/admin_product.js'
            ));
            return $this->display(__FILE__, 'product_settings.tpl');
        }
    }
    public function checkSpecificProudct($id_product)
    {
        $sql = 'SELECT id_specific_price FROM `'._DB_PREFIX_.'specific_price` WHERE id_product ="'.(int)$id_product.'" AND (`from` = "0000-00-00 00:00:00" OR `from` <="'.pSQL(date('Y-m-d H:i:s')).'" ) AND (`to` = "0000-00-00 00:00:00" OR `to` >="'.pSQL(date('Y-m-d H:i:s')).'" )';
        return Db::getInstance()->getRow($sql);
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayCustomerAccount()
    {
        $customer = $this->context->customer;
        $output = '';
        $this->smarty->assign(array(
            'customer' => $customer,
            'is17' => $this->is17
        ));
        if(Ets_Sponsor::isRefferalProgramReady() && Ets_AM::isCustomerBelongToValidGroup($customer,'ETS_AM_REF_GROUPS')){
            if(Configuration::get('ETS_AM_REF_MSG_CONDITION', $this->context->language->id) != ''){
                $this->smarty->assign(array(
                    'refUrl' =>Ets_AM::getBaseUrlDefault('refer_friends')
                ));
                $output .= $this->display(__FILE__, 'referral_box.tpl');
            }
            else{
                if (Ets_Sponsor::canUseRefferalProgram() || Ets_Sponsor::registeredReferralProgram()) {
                    $this->smarty->assign(array(
                        'refUrl' => Ets_AM::getBaseUrlDefault('refer_friends')
                    ));
                    $output .= $this->display(__FILE__, 'referral_box.tpl');
                }
            }
        }
        if((int)Configuration::get('ETS_AM_LOYALTY_ENABLED') && Ets_AM::isCustomerBelongToValidGroup($customer,'ETS_AM_LOYALTY_GROUPS')){
            if(Configuration::get('ETS_AM_LOY_MSG_CONDITION', $this->context->language->id) != ''){
                $this->smarty->assign(array(
                    'refUrl' => Ets_AM::getBaseUrlDefault('loyalty')
                ));
                $output .= $this->display(__FILE__, 'loyalty_box.tpl');
            }
            else{
                if (Ets_Loyalty::isCustomerCanJoinLoyaltyProgram()) {
                    if ($min = Configuration::get('ETS_AM_LOYALTY_MIN_SPENT')) {
                        $minSpent = Ets_Loyalty::calculateCustomerSpent($this->context);
                        if ($minSpent >= (float)$min) {
                            $this->smarty->assign(array(
                                'refUrl' => Ets_AM::getBaseUrlDefault('loyalty')
                            ));
                            $output .= $this->display(__FILE__, 'loyalty_box.tpl');
                        }
                    } else {
                        $this->smarty->assign(array(
                            'refUrl' => Ets_AM::getBaseUrlDefault('loyalty')
                        ));
                        $output .= $this->display(__FILE__, 'loyalty_box.tpl');
                    }
                }
            }
        }
        if((int)Configuration::get('ETS_AM_AFF_ENABLED') && Ets_AM::isCustomerBelongToValidGroup($customer,'ETS_AM_AFF_GROUPS')){
            if(Configuration::get('ETS_AM_AFF_MSG_CONDITION', $this->context->language->id) != ''){
                $this->smarty->assign(array(
                    'refUrl' => Ets_AM::getBaseUrlDefault('aff_products')
                ));
                $output .= $this->display(__FILE__, 'affiliate_box.tpl');
            }
            else{
                if (Configuration::get('ETS_AM_AFF_ENABLED')) {
                    $valid = false;
                    if (Ets_Affiliate::isCustomerCanJoinAffiliateProgram()) {
                        $valid = true;
                    }
                    if ($valid) {
                        $this->smarty->assign(array(
                            'refUrl' => Ets_AM::getBaseUrlDefault('aff_products')
                        ));
                        $output .= $this->display(__FILE__, 'affiliate_box.tpl');
                    }
                }
            }
        }
        if(!Configuration::get('ETS_AM_LOY_MSG_CONDITION', $this->context->language->id) && !Configuration::get('ETS_AM_REF_MSG_CONDITION', $this->context->language->id) && !Configuration::get('ETS_AM_AFF_MSG_CONDITION', $this->context->language->id)){
            if ($output) {
                $this->smarty->assign(array(
                    'refUrl' => Ets_AM::getBaseUrlDefault('dashboard')
                ));
                $output .= $this->display(__FILE__, 'customer_reward.tpl');
            }
        }
        else{
            if ($output) {
                $this->smarty->assign(array(
                    'refUrl' => Ets_AM::getBaseUrlDefault('dashboard')
                ));
                $output .= $this->display(__FILE__, 'customer_reward.tpl');
            }
        }
        
        return $output;
    }

    /**
     * @return array
     */
    protected function getTemplateVarInfos()
    {
        $total_balance = Ets_Reward_Usage::getTotalBalance($this->context->customer->id);

        if (Ets_AM::needExchange($this->context)) {
            $total_balance = Tools::convertPrice($total_balance);
        }
        $show_point = Configuration::get('ETS_AM_REWARD_DISPLAY') == 'point' ? 1 : 0;
        return array(
            'eam_reward_total_balance' => Ets_affiliatemarketing::displayPrice($total_balance),
            'eam_reward_point' => $show_point ? Ets_AM::displayReward($total_balance) : 0,
            'show_point' => $show_point,

        );
    }



    /**
     *
     */
    public function getApplications()
    {
        if (($id_app = (int)Tools::getValue('id_application', false)) && Tools::isSubmit('viewapp') ) {
            $app = Ets_Participation::getApplicationById($id_app);
            $this->smarty->assign(array(
                'app' => $app,
                'id_data' => $id_app,
                'user_link' => $this->getLinkCustomerAdmin($app['id_customer']),
                'link_app' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=ets_affiliatemarketing&tabActive=applications'
            ));
            $this->_html .= $this->display(__FILE__, 'view_app.tpl');
        } else {
            $pagination = EtsAmAdmin::getDataApplications();
            if($pagination['results'])
            {
                foreach($pagination['results'] as &$result)
                {
                    $result['link'] = $this->getLinkCustomerAdmin($result['id_customer']);
                }
            }
            $this->smarty->assign(array(
                'results' => $pagination['results'],
                'current_page' => $pagination['current_page'],
                'total_page' => $pagination['total_page'],
                'total_data' => $pagination['total_data'],
                'per_page' => $pagination['per_page'],
                'search' => ($search = Tools::getValue('search', '')) && Validate::isCleanHtml($search) ? $search :'',
                'limit' => (int)Tools::getValue('limit', 10),
                'search_placeholder' => $this->l('Search for id, status, email...'),
                'params' => Tools::getAllValues(),
                'link_customer' => $this->context->link->getAdminLink('AdminCustomers', true),
                'enable_email_approve_app' => (int)Configuration::get('ETS_AM_ENABLED_EMAIL_RES_REG'),
                'enable_email_decline_app' => (int)Configuration::get('ETS_AM_ENABLED_EMAIL_DECLINE_APP')
            ));
            $this->_html .= $this->display(__FILE__, 'list_applications.tpl');
        }

    }
    public function getLinkCustomerAdmin($id_customer)
    {
        if(version_compare(_PS_VERSION_, '1.7.6', '>='))
        {
            $sfContainer = call_user_func(array('\PrestaShop\PrestaShop\Adapter\SymfonyContainer','getInstance'));
        	if (null !== $sfContainer) {
        		$sfRouter = $sfContainer->get('router');
        		$link_customer= $sfRouter->generate(
        			'admin_customers_view',
        			array('customerId' => $id_customer)
        		);
        	}
        }
        else
            $link_customer = $this->context->link->getAdminLink('AdminCustomers').'&id_customer='.(int)$id_customer.'&viewcustomer';
        return $link_customer;
    }
    /**
     * @param $params
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayProductAdditionalInfo($params)
    {
        if (!(isset($params['product'])) || !$params['product']) 
            return;
        $cart = $this->context->cart;
        $id_product = (int)Tools::getValue('id_product');
        $product = $params['product'];
        $count_current_product = 0;
        if ($products = $cart->getProducts()) {
           foreach ($products as $p) {
                if ((int)$p['id_product'] == (int)$product['id_product']) {
                    $count_current_product = (int)$p['cart_quantity'];
                    break;
                }
           }
        }
        $loyalty_suspended = Ets_Loyalty::isCustomerSuspendedOrBannedLoyaltyProgram($this->context->customer->id);
        $affiliate_suspended = Ets_Affiliate::isCustomerSuspendedOrBannedAffiliateProgram($this->context->customer->id);
        if (!$loyalty_suspended || !$affiliate_suspended) {
            $data_product = array();
            $p = new Product($product['id_product']);
            $data_product['price_with_reduction_without_tax'] = $p->getPrice(false);
            $data_product['price_with_reduction'] = $p->getPrice(true);
            $data_product['id_product'] = $p->id;
            $product['price_with_tax_with_reduction'] = $p->getPrice();
            $product['price_without_tax_with_reduction'] = $p->getPrice(false);
            $product['price_with_tax_without_reduction'] = $p->getPriceWithoutReduct(false);
            $product['price_without_tax_without_reduction'] = $p->getPriceWithoutReduct(true);
            $assignment = array(
                'loyalty_suspended' => $loyalty_suspended,
                'affiliate_suspended' => $affiliate_suspended,
            );
            $assignment['eam_product_addition_loy_message'] = 'ban';
            $assignment['eam_product_addition_aff_message'] = 'ban';
            if (! $loyalty_suspended) {
                $qty_min = Configuration::get('ETS_AM_QTY_MIN');
                $productRewardSetting = Db::getInstance()->getRow( "SELECT * FROM `" . _DB_PREFIX_ . "ets_am_loy_reward` WHERE `id_product` = " . (int)$product['id_product']);
                if ($productRewardSetting && count($productRewardSetting) && (int)$productRewardSetting['use_default'] != 1) {
                    $qty_min = $productRewardSetting['qty_min'];
                } 
                if(!$qty_min || (int)$qty_min > $count_current_product){
                    $eam_product_addition_loy_message = Ets_Loyalty::getLoyaltyMessageOnProductPage($product, $this->context, $cart);
                }
                else{
                    $eam_product_addition_loy_message = Ets_Loyalty::getLoyaltyMessageOnProductPage($product, $this->context);
                }
                $assignment['eam_product_addition_loy_message'] = $eam_product_addition_loy_message;
            }
            $productClass = new Product($product['id_product']);
            if (! $affiliate_suspended ) {
                $data_message = Ets_Affiliate::getAffiliateMessage($data_product, $this->context);
                $eam_product_addition_aff_message = null;
                if($data_message && is_array($data_message)){
                    $eam_product_addition_aff_message = $this->getAffiliateMessage($data_message);
                    if($data_message['is_aff'])
                        $assignment['link_share'] = $data_message['link'];
                }
                $assignment['eam_product_addition_aff_message'] = $eam_product_addition_aff_message;
            }
            $this->smarty->assign($assignment);
            if (Configuration::get('ETS_AM_AFF_ENABLED') && $this->getCookie(EAM_AFF_PRODUCT_COOKIE)) {
                $aff_products_cookie = explode('-',$this->getCookie(EAM_AFF_PRODUCT_COOKIE));
                $aff_customers = explode('-',$this->getCookie(EAM_AFF_CUSTOMER_COOKIE));
                if($aff_products_cookie)
                {
                    foreach($aff_products_cookie as $key=> $aff_product)
                        if($aff_product==$product['id_product'])
                            $aff_customer = isset($aff_customers[$key]) ? $aff_customers[$key] :0;
                }
                $display_aff_promo_code = false;
                $aff_promo_code_msg = null;
                if (in_array($product['id_product'],$aff_products_cookie) && Ets_Voucher::canAddAffiliatePromoCode($product['id_product'],$aff_customer,true)) {
                    
                    if (Ets_Affiliate::productValidAffiliateProgram($productClass) && $discount_value = Ets_AM::GetDiscountVoucher('aff')) {
                        $display_aff_promo_code = true;
                        $mesage_code =strip_tags(Configuration::get('ETS_AM_AFF_WELCOME_MSG', $this->context->language->id));
                        $aff_promo_code_msg = str_replace('[discount_value]',$discount_value,$mesage_code);
                    }
                }
                if ($display_aff_promo_code && $aff_promo_code_msg) {
                    $this->smarty->assign(array(
                        'eam_display_aff_promo_code' => true,
                        'eam_aff_promo_code_message' => $aff_promo_code_msg,
                    ));
                }
            }
            $product_classs= new Product($id_product,false,$this->context->language->id);
            $this->smarty->assign(
                array(
                    'product' => $product_classs,
                )
            );
            return  $this->display(__FILE__, 'product-additional.tpl');
        }
    }

    /**
     * @param $params
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayRightColumnProduct($params)
    {
        if (!$this->is17) {
            $loyalty_suspended = Ets_Loyalty::isCustomerSuspendedOrBannedLoyaltyProgram($this->context->customer->id);
            $affiliate_suspended = Ets_Affiliate::isCustomerSuspendedOrBannedAffiliateProgram($this->context->customer->id);
            if (! $loyalty_suspended || ! $affiliate_suspended) {
                $id_product = (int)Tools::getValue('id_product');
                $p = new Product($id_product);
                $product = (array)$p;
                $cart = $this->context->cart;
                $count_current_product = 0;
                if ($products = $cart->getProducts()) {
                   foreach ($products as $prd) {
                        if ((int)$prd['id_product'] == (int)$id_product) {
                            $count_current_product = (int)$prd['cart_quantity'];
                            break;
                        }
                   }
                }
                $priceWithTaxWithReduct = $p->getPrice();
                $priceWithoutTaxWithReduct = $p->getPrice(false);
                $priceWithTaxWithoutReduct = $p->getPriceWithoutReduct(false);
                $priceWithoutTaxWithoutReduct = $p->getPriceWithoutReduct(true);
                $product['price_with_tax_with_reduction'] = $priceWithTaxWithReduct;
                $product['price_without_tax_with_reduction'] = $priceWithoutTaxWithReduct;
                $product['price_with_tax_without_reduction'] = $priceWithTaxWithoutReduct;
                $product['price_without_tax_without_reduction'] = $priceWithoutTaxWithoutReduct;
                $product['id_product'] = $p->id;
                $assignment = array(
                    'loyalty_suspended' => $loyalty_suspended,
                    'affiliate_suspended' => $affiliate_suspended,
                );
                 $assignment['eam_product_addition_loy_message'] = '';
                 $assignment['eam_product_addition_aff_message'] = '';
                if (! $loyalty_suspended) {
                    if(!Configuration::get('ETS_AM_QTY_MIN') || (int)Configuration::get('ETS_AM_QTY_MIN') > $count_current_product){
                        $eam_product_addition_loy_message = Ets_Loyalty::getLoyaltyMessageOnProductPage($product, $this->context, $cart);
                    }
                    else{
                        $eam_product_addition_loy_message = Ets_Loyalty::getLoyaltyMessageOnProductPage($product, $this->context);
                    }
                    $assignment['eam_product_addition_loy_message'] = $eam_product_addition_loy_message;
                }
                if (! $affiliate_suspended) {
                    $data_message = Ets_Affiliate::getAffiliateMessage($product, $this->context);
                    $eam_product_addition_aff_message = null;
                    if($data_message && is_array($data_message)){
                        $eam_product_addition_aff_message = $this->getAffiliateMessage($data_message);
                        $assignment['link_share'] = $data_message['link'];
                    }
                    $assignment['eam_product_addition_aff_message'] = $eam_product_addition_aff_message;
                }
                $this->smarty->assign($assignment);
                if (Configuration::get('ETS_AM_AFF_ENABLED') && $this->getCookie(EAM_AFF_PRODUCT_COOKIE)) {
                    $aff_products_cookie = explode('-',$this->getCookie(EAM_AFF_PRODUCT_COOKIE));
                    //$aff_customers = explode('-',$this->getCookie(EAM_AFF_CUSTOMER_COOKIE));
                    $display_aff_promo_code = false;
                    $aff_promo_code_msg = null;
                    if (in_array($product['id_product'],$aff_products_cookie) && Ets_Voucher::canAddAffiliatePromoCode($product['id_product'],$this->context->customer->id,true) ) {
                        $productClass = new Product($product['id_product']);
                        if (Ets_Affiliate::productValidAffiliateProgram($productClass) && $discount_value = Ets_AM::GetDiscountVoucher('aff')) {
                            $display_aff_promo_code = true;
                            $mesage_code =strip_tags(Configuration::get('ETS_AM_AFF_WELCOME_MSG', $this->context->language->id));
                            $aff_promo_code_msg = str_replace('[discount_value]',$discount_value,$mesage_code);
                        }
                    }
                    
                    if ($display_aff_promo_code && $aff_promo_code_msg) {
                        $this->smarty->assign(array(
                            'eam_display_aff_promo_code' => true,
                            
                            'eam_aff_promo_code_message' => $aff_promo_code_msg,
                        ));
                    }
                }
                $product_classs= new Product($id_product,false,$this->context->language->id);
                $this->smarty->assign(
                    array(
                        'product' => $product_classs,
                    )
                );
                return $this->display(__FILE__, 'product-additional.tpl');
            }
        }
    }

    public function hookActionCustomerAccountAdd($params)
    {
        $ref = null;
        if ((int)$this->context->cookie->__get(EAM_REFS)) {
            $ref = (int)$this->context->cookie->__get(EAM_REFS);
        } else {
            if (($code = Tools::getValue('eam_code_ref', false)) && Validate::isCleanHtml($code) ) {
                $ref = Ets_Sponsor::checkSponsorCode($code);
                if (!$ref) {
                    $ref = null;
                }
                else{
                    if(Ets_Sponsor::isActive($ref)){
                        $this->context->cookie->__set('ets_am_show_voucher_ref', (int)$this->context->customer->id);
                    }
                }
            }
        }
        $id_customer = $params['newCustomer']->id;
        $id_sponsor = 0;
        if($this->context->cart->id)
        {
            $id_sponsor = Ets_Sponsor::getIdRefByCart($this->context->cart->id, $id_customer);
            if($id_sponsor){
                $this->context->cookie->__unset('ets_am_show_voucher_ref');
                $this->context->cookie->__unset(EAM_REFS);
            }
        }
        if(!$ref){
            $ref = $id_sponsor;
        }
        if (Ets_Sponsor::addFriendSponsored($ref)) {
            Ets_Sponsor::getRewardWithoutOrder($ref);
        }
        $email_customer = $params['newCustomer']->email;
        Ets_Invitation::updateIdFriend($id_customer, $email_customer);
    }

    /**
     * @param $params
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookActionOrderStatusPostUpdate($params)
    {
        Ets_AM::actionWhenOrderStatusChange($params);
    }

    public function renderDatatable($params)
    {
        $link_withdraw = $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tabActive=withdraw_list';
        if (Tools::isSubmit('submitSaveNoteWithdrawal', false)) {
            if (($note = Tools::getValue('note', false)) && Validate::isCleanHtml($note) && ($id_usage = (int)Tools::getValue('id_usage'))) {
                $usage = new Ets_Reward_Usage($id_usage);
                $usage->note = $note;
                $usage->update();
                $this->_html .= $this->displayConfirmation($this->l('Saved successfully'));
            }
        }
        if (Tools::isSubmit('submitApproveWithdrawItem', false)) {
            if ($id_usage = (int)Tools::getValue('id_usage', false)) {
                Ets_Withdraw::updateWithdrawAndReward($id_usage,'APPROVE');
                $this->_html .= $this->displayConfirmation($this->l('Saved successfully'));
            }
        }
        if (Tools::isSubmit('submitDeclineReturnWithdrawItem', false)) {
            if ($id_usage = (int)Tools::getValue('id_usage', false)) {
                Ets_Withdraw::updateWithdrawAndReward($id_usage,'DECLINE_RETURN');
                $this->_html .= $this->displayConfirmation($this->l('Saved successfully'));
            }
            
        }
        if (Tools::isSubmit('submitDeclineDeductWithdrawItem', false)) {
            if ($id_usage = (int)Tools::getValue('id_usage', false)) {
                Ets_Withdraw::updateWithdrawAndReward($id_usage,'DECLINE_DEDUCT');
                $this->_html .= $this->displayConfirmation($this->l('Saved successfully'));
            }
            
        }
        if (Tools::isSubmit('submitDeleteWithdrawItem', false)) {
            if ($id_usage = (int)Tools::getValue('id_usage', false)) {
                $usage = new Ets_Reward_Usage($id_usage);
                $usage->deleted = 1;
                $usage->update();
                $this->_html .= $this->displayConfirmation($this->l('Saved successfully'));
                Tools::redirectAdmin($link_withdraw);
            }
        }
        if (isset($params['withdrawal']) && $params['withdrawal'] && ($view = Tools::getValue('view', false)) && Validate::isCleanHtml($view)) {
            if ( ($id_withdrawal = (int)Tools::getValue('id_withdrawal', false))) {
                $this->smarty->assign(array(
                    'user' => Ets_Withdraw::getUserWithdrawal($id_withdrawal),
                    'user_link' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tabActive=reward_users',
                    'link_withdraw' => $link_withdraw,
                    'id_data' => $id_withdrawal
                ));
                $this->_html .= $this->display(__FILE__, 'withdrawal_view.tpl');
            }
        } else {
            if (isset($params['withdrawal']) && $params['withdrawal']) {
                $pagination = Ets_Withdraw::getCustomerWithdrawalRequests();
                $placeholder = $this->l('Customer\'s ID or name');
            } else {
                $pagination = EtsAmAdmin::getRewardHistory();
                $placeholder = $this->l('Customer\'s ID or name');
            }
            $this->smarty->assign(array(
                'fields' => $params['fields_list'],
                'results' => $pagination['results'],
                'current_page' => $pagination['current_page'],
                'total_page' => $pagination['total_page'],
                'total_data' => $pagination['total_data'],
                'per_page' => $pagination['per_page'],
                'search' => ($search =  Tools::getValue('search', '')) && Validate::isCleanHtml($search) ? $search :'',
                'limit' => (int)Tools::getValue('limit', 10),
                'search_placeholder' => $placeholder,
                'params' => Tools::getAllValues(),
                'link_customer' =>$this->context->link->getAdminLink('AdminModules', true)
            ));

            if (isset($params['withdrawal']) && $params['withdrawal']) {
                $this->_html .= $this->display(__FILE__, 'withdrawal.tpl');
            } else {
                $this->_html .= $this->display(__FILE__, 'datatable.tpl');
            }
        }

    }

    /**
     * @param $params
     * @return array
     * @throws Exception
     */
    public function hookPaymentOptions($params)
    {
        if (!Configuration::get('ETS_AM_AFF_ALLOW_BALANCE_TO_PAY')) {
            return;
        }
        $cart = $params['cart'];
        $cart_total = $cart->getOrderTotal(true, Cart::BOTH);
        if (Ets_AM::needExchange($this->context)) {
            $cart_total = Tools::convertPrice($cart_total, null, false);
        }
        if ($min = Configuration::get('ETS_AM_MIN_BALANCE_REQUIRED_FOR_ORDER')) {
            $min = (float)$min;
            if ($cart_total < $min) {
                return;
            }
        }
        if ($max = Configuration::get('ETS_AM_MAX_BALANCE_REQUIRED_FOR_ORDER')) {
            $max = (float)$max;
            if ($cart_total > $max) {
                return;
            }
        }
        $total_balance = Ets_Reward_Usage::getTotalBalance($this->context->customer->id);
        if ($total_balance < $cart_total) {
            return;
        }
        $this->smarty->assign(
            $this->getTemplateVarInfos()
        );
        $newOption = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $newOption->setModuleName($this->name)
            ->setCallToActionText($this->l('Pay by reward'))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true))
            ->setAdditionalInformation($this->fetch('module:ets_affiliatemarketing/views/templates/hook/payment_info.tpl'));
        $payment_options = array(
            $newOption,
        );
        return $payment_options;
    }
    public function hookPayment($params)
	{
		if (!Configuration::get('ETS_AM_AFF_ALLOW_BALANCE_TO_PAY')) {
            return;
        }
        $cart = $params['cart'];
        $cart_total = $cart->getOrderTotal(true, Cart::BOTH);
        if (Ets_AM::needExchange($this->context)) {
            $cart_total = Tools::convertPrice($cart_total, null, false);
        }
        if ($min = Configuration::get('ETS_AM_MIN_BALANCE_REQUIRED_FOR_ORDER')) {
            $min = (float)$min;
            if ($cart_total < $min) {
                return;
            }
        }
        if ($max = Configuration::get('ETS_AM_MAX_BALANCE_REQUIRED_FOR_ORDER')) {
            $max = (float)$max;
            if ($cart_total > $max) {
                return;
            }
        }
        $total_balance = Ets_Reward_Usage::getTotalBalance($this->context->customer->id);
        if ($total_balance < $cart_total) {
            return;
        }
        $this->smarty->assign(
            $this->getTemplateVarInfos()
        );
		return $this->display(__FILE__, 'payment.tpl');
		
	}
    /**
     * @param $params
     * @return bool|Ets_Reward_Usage
     */
    public function actionPaymentByReward($order)
    {
        $context = $this->context;
        if ($order) {
            $count_order = (int)Db::getInstance()->getValue("SELECT COUNT(*) FROM `"._DB_PREFIX_."ets_am_reward_usage` WHERE id_customer = ".(int)$this->context->customer->id." AND id_order = ".(int)$order->id);
            if($count_order){
                return false;
            }
            $amount = Tools::convertPrice($order->total_paid,$this->context->currency,false);
            $usageLOY = 0;
            $usageANR = 0;
            $totalLoy = Ets_Reward_Usage::getTotalEarn('loy', $context->customer->id, $context,$order->id);
            $totalSpentLoy = Ets_Reward_Usage::getTotalSpentLoy($context->customer->id, false, null, $context);
            $remainLoy = (float)$totalLoy - (float)$totalSpentLoy;
            if($remainLoy > (float)$amount){
                $usageLOY = $amount;
            }
            else{
                if($remainLoy > 0){
                    $usageLOY = $remainLoy;
                    $usageANR = (float)$amount - (float)$usageLOY;
                }
                else{
                    $usageANR = (float)$amount;
                }
            }
            $usage = false;
            if($usageLOY > 0){
                $usage = new Ets_Reward_Usage();
                $usage->type = 'loy';
                $usage->id_customer = $this->context->customer->id;
                $usage->id_shop = $this->context->shop->id;
                $usage->id_currency = $this->context->currency->id;
                $usage->id_order = $order->id;
                $usage->status = 1;
                $usage->amount = $usageLOY;
                $usage->datetime_added = date('Y-m-d H:i:s');
                $usage->note = sprintf($this->l('Paid for order #%s'), $order->id);
                $usage->add();
                Ets_affiliatemarketing::loyRewardUsed($usageLOY,$usage->id,$usage->id_customer);
            }
            if($usageANR > 0){
                $programs = array('mnu','aff','ref');
                foreach($programs as $program)
                {
                    $total = Ets_Reward_Usage::getTotalEarn($program, $context->customer->id, $context);
                    $totalSpent = Ets_Reward_Usage::getTotalSpent($context->customer->id, false, null, $context,$program);
                    $remain = (float)$total - (float)$totalSpent;
                    if($remain >0)
                    {
                        if($usageANR < $remain)
                        {
                            $usage = $usageANR;
                            $continue = false;
                        }
                        else
                        {
                            $usage = $remain;
                            $continue=true;
                            $usageANR = $usageANR-$remain;
                        }
                        $rewardUsage = new Ets_Reward_Usage();
                        $rewardUsage->id_customer = $this->context->customer->id;
                        $rewardUsage->id_shop = $this->context->shop->id;
                        $rewardUsage->id_currency = $this->context->currency->id;
                        $rewardUsage->id_order = $order->id;
                        $rewardUsage->status = 1;
                        $rewardUsage->amount = $usage;
                        $rewardUsage->type = $program;
                        $rewardUsage->datetime_added = date('Y-m-d H:i:s');
                        $rewardUsage->note = sprintf($this->l('Paid for order #%s'), $order->id);
                        $rewardUsage->add();
                        if(!$continue)
                            break;
                    }
                }
                
            }
            return isset($rewardUsage) ? true : false;
            
        }
        return false;
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayEtsAmFrontOfficeWithdraw()
    {
        $this->smarty->assign(array(
            'ets_am_withdraw_url' => Ets_AM::getBaseUrl() . 'ets-am-withdraw',
        ));
        return $this->display(__FILE__, 'views/templates/hook/front/customer_withdraw.tpl');
    }

    /**
     * @return string
     */
    public function hookDisplayEtsAmFrontOfficeVoucher()
    {
        $currencies = Currency::getCurrencies();
        $this->smarty->assign(array(
            'id_lang' => $this->context->language->id,
            'currencies' => $currencies
        ));
        return $this->display(__FILE__, 'views/templates/hook/front/voucher.tpl');
    }

    /**
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayEtsAmFrontOfficeRewardHistory()
    {
        return $this->display(__FILE__, 'views/templates/hook/front/history.tpl');
    }

    /**
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function displayFooterBefore()
    {
        $output = '';
        $show_popup_banner = 0;
        if(Tools::isSubmit('action')  || Tools::isSubmit('ajax'))
            return '';                
        if ($this->context->customer && Ets_Sponsor::isRefferalProgramReady()) {
            if ((int)Configuration::get('ETS_AM_REF_INTRO_ENABLED')) {
                $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
                $banner = Configuration::get('ETS_AM_REF_INTRO_BANNER');
                if (!$banner) {
                    $banner = Configuration::get('ETS_AM_REF_DEFAULT_BANNER');
                }
                $banner_img = '';
                if ($banner) {
                    $banner_img = Ets_AM::getBaseUrl(true) . EAM_PATH_IMAGE_BANER . $banner;
                }
                $title = Configuration::get('ETS_AM_REF_INTRO_TITLE', $default_lang);
                $content = Configuration::get('ETS_AM_REF_INTRO_CONTENT', $default_lang);
                $delay = (int)Configuration::get('ETS_AM_REF_INTRO_REDISPLAY');
                $link_ajax = $this->context->link->getModuleLink($this->name,'exec');
                $link_ref = Ets_AM::getBaseUrlDefault('sponsorship');
                $show_popup = (int)Ets_Banner::showPopupBanner($delay, $this->context);
                $show_popup_banner = $show_popup;
                $this->smarty->assign(array(
                    'banner' => $banner_img,
                    'title' => $title,
                    'content' => $content,
                    'delay' => $delay,
                    'link_ref' => $link_ref,
                    'show_popup' => $show_popup,
                    'link_ajax' => $link_ajax
                ));
                $output .= $this->display(__FILE__, 'popup_referral.tpl');
            }
        }
        if ($this->context->customer->id && (int)Configuration::get('ETS_AM_REF_OFFER_VOUCHER') && Configuration::get('ETS_AM_REF_ENABLED')) {
            if (Ets_Sponsor::allowGetVoucher()) {
                $voucher = Ets_AM::generateVoucher('ref');
                if ($voucher && !$show_popup_banner) {
                    $link_ajax = $this->context->link->getModuleLink($this->name,'exec');
                    $this->smarty->assign(array(
                        'voucher' => $voucher,
                        'show_popup_voucher' => 1,
                        'link_ajax' => $link_ajax
                    ));
                    $output .= $this->display(__FILE__, 'popup_voucher_ref.tpl');
                }
            }
        }
        if (Configuration::get('ETS_AM_AFF_ENABLED')) {
            
            $display_aff_promo_code = false;
            $aff_promo_code_msg = null;
            
            if (($aff_product = (int)Tools::getValue('affp')) && ($id_product= (int)Tools::getValue('id_product'))&& Ets_Voucher::canAddAffiliatePromoCode($id_product,$aff_product,true)) {
                $product = new Product($id_product);
                if (Ets_Affiliate::productValidAffiliateProgram($product) && $discount_value = Ets_AM::GetDiscountVoucher('aff')) {
                    $display_aff_promo_code = true;
                    $mesage_code =strip_tags(Configuration::get('ETS_AM_AFF_WELCOME_MSG', $this->context->language->id));
                    $aff_promo_code_msg = str_replace('[discount_value]',$discount_value,$mesage_code);
                }
            }
            if ($display_aff_promo_code && $aff_promo_code_msg) {
                $this->smarty->assign(array(
                    'eam_display_aff_promo_code' => true,
                    'eam_aff_promo_code_message' => $aff_promo_code_msg,
                ));
                $output .= $this->display(__FILE__, 'popup_voucher_aff.tpl');
            }
        }
        return $output;
    }
    public function hookActionCustomerLogoutAfter()
    {
        $this->setCookie(EAM_AFF_CUSTOMER_COOKIE,'');
        $this->setCookie(EAM_AFF_PRODUCT_COOKIE, '');
        $this->setCookie(EAM_REFS,'');
    }
    public function hookActionCartSave()
    {
        if(isset($this->context->customer->logged) && $this->context->customer->logged)
        {
            if (Configuration::get('ETS_AM_AFF_OFFER_VOUCHER') && Configuration::get('ETS_AM_AFF_ENABLED') && $this->getCookie(EAM_AFF_PRODUCT_COOKIE)) {
                $aff_products = explode('-', $this->getCookie(EAM_AFF_PRODUCT_COOKIE));
                $aff_customers = explode('-',$this->getCookie(EAM_AFF_CUSTOMER_COOKIE)) ;
                
                if($aff_products)
                {
                    if(Configuration::get('ETS_AM_AFF_VOUCHER_TYPE')=='FIXED')
                    {
                        foreach($aff_products as $key=> $aff_product)
                        {
                            $product = new Product($aff_product);
                            if(Ets_Affiliate::productValidAffiliateProgram($product) && Ets_Affiliate::isCustomerCanJoinAffiliateProgram($aff_customers[$key]))
                            {
                                $voucher_code = Configuration::get('ETS_AM_AFF_VOUCHER_CODE');
                                $cartRule = CartRule::getCartsRuleByCode($voucher_code, $this->context->language->id);
                                if($cartRule)
                                {
                                    $id_cart_rule = $cartRule[0]['id_cart_rule'];
                                    $cartRuleClas= new CartRule($id_cart_rule);
                                    if (!$cartRuleClas->checkValidity($this->context, false, true)) {
                                        $this->context->cart->addCartRule($cartRuleClas->id);
                                    }
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach($aff_products as $key=> $aff_product)
                        {
                            $product = new Product($aff_product);
                            if(Ets_Affiliate::productValidAffiliateProgram($product) && Ets_Affiliate::isCustomerCanJoinAffiliateProgram($aff_customers[$key]))
                            {
                                
                                
                                if (Ets_Voucher::canAddAffiliatePromoCode($aff_product,$aff_customers[$key])) {
                                    $promo_code = Ets_AM::generateVoucher('aff', $aff_product, 0, $this->context);
                                    if(isset($promo_code['id_cart_rule']) && $promo_code['id_cart_rule'])
                                    {
                                        $cartRuleClas= new CartRule($promo_code['id_cart_rule']);
                                        $this->context->cart->addCartRule($cartRuleClas->id);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }    
    public function hookDisplayFooter()
    {

        return $this->displayFooterBefore();
    }

    /**
     *
     */
    protected function statsReward()
    {
        $params = array();
        if (($filter_status = Tools::getValue('filter_status')) && Validate::isCleanHtml($filter_status)) {
            $params['status'] = $filter_status;
        }
        else
        {
            $params['stats_type']='reward';
            $params['status'] =1;
        }
        if (($filter_reward_status = Tools::getValue('filter_reward_status')) && Validate::isCleanHtml($filter_reward_status) ) {
            $params['reward_status'] = $filter_reward_status;
        }
        if (($filter_order_status = Tools::getValue('filter_order_status')) && Validate::isCleanHtml($filter_order_status)) {
            $params['order_status'] = $filter_order_status;
        }
        if (($filter_date_from = Tools::getValue('filter_date_from')) && Validate::isCleanHtml($filter_date_from)) {
            $params['date_from'] = $filter_date_from;
        }
        if (($filter_date_to = Tools::getValue('filter_date_to')) && Validate::isCleanHtml($filter_date_to) ) {
            $params['date_to'] = $filter_date_to;
        }
        if (($filter_date_type = Tools::getValue('filter_date_type')) && Validate::isCleanHtml($filter_date_type)) {
            $params['date_type'] = $filter_date_type;
        }
        if (($filter_type_stats = Tools::getValue('filter_type_stats')) && Validate::isCleanHtml($filter_type_stats) ) {
            $params['stats_type'] = $filter_type_stats;
        }
        if (($program = Tools::getValue('program')) && Validate::isCleanHtml($program) ) {
            $params['program'] = $program;
        }
        $results = Ets_AM::getStartChartReward($params);
        die(Tools::jsonEncode($results));
    }

    protected function renderStatisticReward()
    {
        $params = array();
        if (($tabActive = Tools::getValue('tabActive')) && Validate::isCleanHtml($tabActive)) {
            if (($filter_status = Tools::getValue('filter_status')) && Validate::isCleanHtml($filter_status)) {
                $params['status'] = $filter_status;
            }
            else
            {
                $params['stats_type']='reward';
                $params['status'] =1;
            }
                
            if (($filter_date_from = Tools::getValue('filter_date_from')) && Validate::isCleanHtml($filter_date_from)) {
                $params['date_from'] = $filter_date_from;
            }
            if (($filter_date_to = Tools::getValue('filter_date_to')) && Validate::isCleanHtml($filter_date_to) ) {
                $params['date_to'] = $filter_date_to;
            }
            if (($program = Tools::getValue('program')) && Validate::isCleanHtml($program)) {
                $params['program'] = $program;
            }
        }
        $results = Ets_AM::getStartChartReward($params);
        $score_counter = Ets_Am::getStatsCounter();

        $order_states = OrderState::getOrderStates((int)Configuration::get('PS_LANG_DEFAULT'));
        $default_currency = Currency::getDefaultCurrency();
        $recently_rewards = Ets_Am::getRecentReward();
        $pie_reward = Ets_Am::getPercentReward(array('status'=>1));

        $log_path = _PS_MODULE_DIR_.'ets_affiliatemarketing/cronjob_time.log';
        $last_cronjob = array();
        if(file_exists($log_path)){
            $logs = Tools::file_get_contents($log_path);
            if($logs){
                $logs = trim($logs);
                $cronjob_time = $logs;
                if($cronjob_time){
                    $last_cronjob['time'] = $cronjob_time;
                    $date1 = strtotime(date('Y-m-d H:i:s'));
                    $date2 = strtotime($cronjob_time);
                    $diff = $date1 - $date2;
                    $diff_hour = $diff/3600;
                    
                    $last_cronjob['warning'] = 0;
                    if($diff_hour > 12){
                        $last_cronjob['warning'] = 1;
                    }
                }
            }
        }
        $cronjob_cookie = new Cookie('eam_cronjob');
        $assignment = array(
            'data_stats' => $results,
            'pie_reward' => $pie_reward,
            'last_cronjob' => $last_cronjob,
            'recently_rewards' => $recently_rewards,
            'score_counter' => $score_counter,
            'order_states' => $order_states,
            'recent_orders' => Ets_AM::getStatsTopTrending(array('type' => 'recent_orders')),
            'customer_link' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tabActive=reward_users',
            'order_link' => $this->context->link->getAdminLink('AdminOrders', true),
            'default_currency' => $default_currency,
            'reward_history_link' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tabActive=reward_history',
            'module_link' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name,
            'cronjob_closed_alert' => $cronjob_cookie->closed_alert,
            'loyaltyPrograEnabled' =>Configuration::get('ETS_AM_LOYALTY_ENABLED'),
            'loyaltyRewardAvailability' =>Configuration::get('ETS_AM_LOYALTY_MAX_DAY'),
            'eam_currency_code' =>$this->context->currency->iso_code,// Configuration::get('ETS_AM_REWARD_DISPLAY_BO') == 'point' ? (Configuration::get('ETS_AM_REWARD_UNIT_LABEL', $this->context->language->id) ? Configuration::get('ETS_AM_REWARD_UNIT_LABEL', $this->context->language->id) : Configuration::get('ETS_AM_REWARD_UNIT_LABEL', (int)Configuration::get('PS_LANG_DEFAULT'))) : $this->context->currency->iso_code,
        );
        if ($this->is17) {
            $assignment['is17'] = true;
        } else {
            $assignment['is17'] = false;
        }
        $this->smarty->assign($assignment);
        $this->_html = $this->display(__FILE__, 'stats.tpl');
    }
    public function hookActionAuthentication(){
        $this->hookActionCartSave();
        if($back = Tools::getValue('back', false)){
            if($back == URL_REF_PROGRAM){
                Tools::redirect(Ets_AM::getBaseUrlDefault('sponsorship'));
            }
            elseif($back == URL_CUSTOMER_REWARD){
                Tools::redirect(Ets_AM::getBaseUrlDefault('dashboard'));
            }
            elseif($back == URL_AFF_PROGRAM){
                Tools::redirect(Ets_AM::getBaseUrlDefault('affiliate'));
            } 
            elseif($back == URL_LOY_PROGRAM){
                Tools::redirect(Ets_AM::getBaseUrlDefault('loyalty'));
            }
        }
    }
    protected function getPaymentMethods()
    {
        $link_pm = $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tabActive=payment_settings';
        if (Tools::isSubmit('update_payment_method', false)) {
            if (($pm_fee_type = Tools::getValue('payment_method_fee_type')) && $pm_fee_type != 'NO_FEE')
            {
                if ($pm_fee_type == 'FIXED') {
                    if (!($pm_fee_fixed = Tools::getValue('payment_method_fee_fixed'))) {
                        $this->_errors[] = $this->l('Fee (fixed amount) is required');
                    } elseif (!Validate::isFloat($pm_fee_fixed)) {
                        $this->_errors[] = $this->l('Fee (fixed amount) must be a decimal number.');
                    }
                } elseif ($pm_fee_type == 'PERCENT') {
                    if (!($pm_fee_percent = Tools::getValue('payment_method_fee_percent'))) {
                        $this->_errors[] = $this->l('Fee (percentage) is required');
                    } elseif (!Validate::isFloat($pm_fee_percent)) {
                        $this->_errors[] = $this->l('Fee (percentage) must be a decimal number.');
                    }
                }
            }
            if ($pm_estimated = Tools::getValue('payment_method_estimated', false)) {
                if (!Validate::isUnsignedInt($pm_estimated)) {
                    $this->_errors[] = $this->l('Estimated processing time must be a integer');
                }
            }
            if ($pmf = Tools::getValue('payment_method_field', array())) {
                foreach ($pmf as $item) {
                    if (isset($item['title']) && is_array($item['title']) && $item['title']) {
                        $title_fill = 0;
                        foreach ($item['title'] as $title) {
                            if($title){
                                if (!Validate::isString('$title')) {
                                    $this->_errors[] = $this->l('Title of payment method field must be a string');
                                }
                                else{
                                    $title_fill = 1;
                                }
                            }
                        }
                        if(!$title_fill){
                            $this->_errors[] = $this->l('Title of payment method field is required');
                        }
                    }
                }
            }
            if (!$this->_errors) {
                if ($id_pm = (int)Tools::getValue('payment_method')) {
                    EtsAmAdmin::updatePaymentMethod($id_pm);
                    $this->_html .= $this->displayConfirmation($this->l('Payment method updated successfully'));
                }
            }
        } elseif (Tools::isSubmit('create_payment_method', false)) {
            if ($pm_name = Tools::getValue('payment_method_name', array())) {
                $title_fill = 0;
                foreach ($pm_name as $item) {
                    if ($item) {
                        if (!Validate::isString($item)) {
                            $this->_errors[] = $this->l('Title of payment method must be a string.');
                        }
                        else{
                            $title_fill = 1;
                        }
                    }
                }
                if(!$title_fill){
                    $this->_errors[] = $this->l('Title of payment method is required.');
                }
            }
            if (($pm_fee_type = Tools::getValue('payment_method_fee_type')) && $pm_fee_type != 'NO_FEE')
            {
                if ($pm_fee_type == 'FIXED') {
                    if (!($pm_fee_fixed = Tools::getValue('payment_method_fee_fixed'))) {
                        $this->_errors[] = $this->l('Fee (fixed amount) is required');
                    } elseif (!Validate::isFloat($pm_fee_fixed)) {
                        $this->_errors[] = $this->l('Fee (fixed amount) must be a decimal number.');
                    }
                } elseif ($pm_fee_type == 'PERCENT') {
                    if (!($pm_fee_percent = Tools::getValue('payment_method_fee_percent'))) {
                        $this->_errors[] = $this->l('Fee (percentage) is required');
                    } elseif (!Validate::isFloat($pm_fee_percent)) {
                        $this->_errors[] = $this->l('Fee (percentage) must be a decimal number.');
                    }
                }
            }
            if ($pm_estimated = Tools::getValue('payment_method_estimated', false)) {
                if (!Validate::isUnsignedInt($pm_estimated)) {
                    $this->_errors[] = $this->l('Estimated processing time must be a integer');
                }
            }
            if (!$this->_errors) {
                if ($id_pm = EtsAmAdmin::createPaymentMethod()) {
                    $this->context->cookie->__set('flash_created_pm_success', $this->l('Payment method created successfully.'));
                    return Tools::redirectAdmin($link_pm . '&payment_method=' . $id_pm . '&edit_pm=1');
                }
            } else {
                $languages = Language::getLanguages('false');
                $currency = Currency::getDefaultCurrency();
                $this->smarty->assign(array(
                    'languages' => $languages,
                    'currency' => $currency,
                    'link_pm' => $link_pm,
                    'query' => Tools::getAllValues()
                ));
                return $this->_html .= $this->display(__FILE__, 'payment/create_payment_method.tpl');
            }
        } elseif (Tools::isSubmit('delete_payment_method') && ($id_pm = (int)Tools::getValue('payment_method'))) {
            EtsAmAdmin::deletePaymentMethod($id_pm);
        }
        $languages = Language::getLanguages('false');
        $currency = Currency::getDefaultCurrency();
        if (Tools::isSubmit('create_pm')) {
            $this->smarty->assign(array(
                'languages' => $languages,
                'currency' => $currency,
                'link_pm' => $link_pm,
            ));
            return $this->_html .= $this->display(__FILE__, 'payment/create_payment_method.tpl');
        } elseif (Tools::isSubmit('edit_pm') && ($id_pm = (int)Tools::getValue('payment_method', false))) {
            $payment_method = EtsAmAdmin::getPaymentMethod($id_pm);
            $pmf = EtsAmAdmin::getListPaymentMethodField($id_pm);
            $this->smarty->assign(array(
                'payment_method' => $payment_method,
                'payment_method_fields' => $pmf,
                'languages' => $languages,
                'default_lang' => (int)Configuration::get('PS_LANG_DEFAULT'),
                'currency' => $currency,
                'link_pm' => $link_pm
            ));
            if ($msg = $this->context->cookie->__get('flash_created_pm_success')) {
                $this->_html .= $this->displayConfirmation($msg);
                $this->context->cookie->__set('flash_created_pm_success', null);
            }
            return $this->_html .= $this->display(__FILE__, 'payment/edit_payment_method.tpl');
        }
        $payment_methods = EtsAmAdmin::getListPaymentMethods();
        $default_currency = Currency::getDefaultCurrency()->iso_code;
        $this->smarty->assign(array(
            'payment_methods' => $payment_methods,
            'default_currency' => $default_currency,
            'link_pm' => $link_pm
        ));
        return $this->_html .= $this->display(__FILE__, 'payment/payment_methods.tpl');
    }

    protected function renderTableDashboard($data, $type)
    {
        $default_currency = Currency::getDefaultCurrency();
        $temp = 'dashboard/recent_orders.tpl';
        switch ($type) {
            case 'recent_orders':
                $temp = 'dashboard/recent_orders.tpl';
                break;
            case 'best_seller':
                $temp = 'dashboard/best_seller.tpl';
                break;
            case 'top_sponsor':
                $temp = 'dashboard/top_sponsor.tpl';
                break;
            case 'top_affiliate':
                $temp = 'dashboard/top_affiliate.tpl';
                break;
            case 'top_customer':
                $temp = 'dashboard/top_customer.tpl';
                break;
            case 'top_reward_accounts':
                $temp = 'dashboard/top_reward_accounts.tpl';
                break;

        }
        $this->smarty->assign(array(
            'data' => $data,
            'default_currency' => $default_currency,
            'customer_link' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name . '&tabActive=reward_users',
            'order_link' => $this->context->link->getAdminLink('AdminOrders', true),
        ));
        return $this->display(__FILE__, $temp);
    }

    public function getDetailUser($id_customer)
    {
        if (Tools::isSubmit('deduct_reward_by_admin') || Tools::isSubmit('add_reward_by_admin')) {
            $amount = Tools::getValue('amount', false);
            $action = Tools::getValue('action', false);
            $reason = Tools::getValue('reason', false);
            $program = Tools::getValue('type_program', false);
            if(!in_array($program,array('loy','aff','ref','mnu')))
                $program='mnu';
            if (!$amount) {
                $this->_errors = $this->l('Amount is required');
            } elseif (!Validate::isPrice($amount)) {
                if($action=='deduct')
                    $this->_errors= $this->l('The reward is not enough to deduct');
                else
                    $this->_errors = $this->l('Amount must be a decimal');
            }
            if($reason && !Validate::isCleanHtml($reason))
                $this->_errors[] = $this->l('Reason is not valid');
            if($action!='add' && $action!='deduct')
                $this->_errors[] = $this->l('Action is not valid');
            if (!$this->_errors) {
                $customer = new Customer($id_customer);
                $type_program = Tools::getValue('type_program');
                if ($action == 'deduct') {
                    $remain = Ets_Reward_Usage::getTotalRemaining($id_customer,$program);
                    if ($remain < $amount) {
                        $this->_errors = $this->l('Reward remaining not enough to deduct.');
                    } else {
                        $usage = new Ets_Reward_Usage();
                        $usage->amount = $amount;
                        $usage->status = 1;
                        $usage->id_customer = $id_customer;
                        $usage->note = $reason ? $reason : null;
                        $usage->datetime_added = date('Y-m-d H:i:s');
                        $usage->type = $program;
                        $usage->id_shop = $this->context->shop->id;
                        $usage->save(true, true);
                        $this->_html .= $this->displayConfirmation($this->l('Deducted successfully'));
                        $data = array(
                            '{customer_name}' => $customer->firstname.' '.$customer->lastname,
                            '{id_reward}' => $usage->id,
                            '{amount}' => Tools::displayPrice($usage->amount),
                            '{program}' => ($type_program=='loy' ? $this->l('Loyalty program') : ($type_program=='aff' ? $this->l('Affiliate program') :( $type_program =='ref' ? $this->l('Referral program'):'---'))),
                            '{reason}' => $usage->note,
                        );
                        if(Configuration::get('ETS_EMAIL_ADMIN_DEDUCT_REWARD') || Configuration::get('ETS_EMAIL_ADMIN_DEDUCT_REWARD')===false)
                        { 
                            $subjects = array(
                                'translation' => $this->l('Admin has deducted a reward from you'),
                                'origin'=> 'Admin has deducted a reward from you',
                                'specific'=>false
                            );
                            Ets_aff_email::send($customer->id_lang,'admin_deduct_reward',$subjects,$data,$customer->email);
                        }                      
                    }
                } elseif ($action == 'add') {
                    $reward = new Ets_AM();
                    $reward->amount = $amount;
                    $reward->note = $reason ? $reason : null;
                    $reward->status = 1;
                    $reward->id_shop = $this->context->shop->id;
                    $reward->id_customer = $id_customer;
                    $reward->program = $program;
                    $reward->datetime_added = date('Y-m-d H:i:s');
                    $reward->datetime_validated = date('Y-m-d H:i:s');
                    $reward->save(true, true);
                    $this->_html .= $this->displayConfirmation($this->l('Added successfully'));
                    $data = array(
                        '{customer_name}' => $customer->firstname.' '.$customer->lastname,
                        '{id_reward}' => $reward->id,
                        '{amount}' => Tools::displayPrice($reward->amount),
                        '{program}' => ($type_program=='loy' ? $this->l('Loyalty program') : ($type_program=='aff' ? $this->l('Affiliate program') :( $type_program =='ref' ? $this->l('Referral program'):'---'))),
                        '{reason}' => $reward->note,
                    );
                    if(Configuration::get('ETS_EMAIL_ADMIN_ADD_REWARD') || Configuration::get('ETS_EMAIL_ADMIN_ADD_REWARD')===false)
                    {
                        $subjects = array(
                            'translation' => $this->l('Admin has added a reward to you'),
                            'origin'=> 'Admin has added a reward to you',
                            'specific'=>false
                        );
                        Ets_aff_email::send($customer->id_lang,'admin_add_reward',$subjects,$data,array('customer'=>$customer->email));
                    }
                }
            }
        }
        if(($id_parent = (int)Db::getInstance()->getValue('SELECT id_parent FROM '._DB_PREFIX_.'ets_am_sponsor WHERE id_customer='.(int)$id_customer)) && ($customerParent = new Customer($id_parent)) && Validate::isLoadedObject($customerParent) )
        {
            $this->context->smarty->assign(
                array(
                    'customerParent' => $customerParent,
                    'linkParent' => $this->getLinkCustomerAdmin($id_parent),
                )
            );
        }
        $currency = Currency::getDefaultCurrency();
        $this->smarty->assign(array(
            'user' => EtsAmAdmin::getUserInfo($id_customer),
            'reward_history' => EtsAmAdmin::getRewardHistory($id_customer),
            'sponsors' => Ets_Sponsor::getDetailSponsors($id_customer),
            'customer_link' => $this->getLinkCustomerAdmin($id_customer),
            'order_link' => $this->context->link->getAdminLink('AdminOrders', true),
            'link_admin' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name,
            'currency' => $currency,
            'id_data' => $id_customer,
            'enable_email_approve_app' => (int)Configuration::get('ETS_AM_ENABLED_EMAIL_RES_REG'),
            'enable_email_decline_app' => (int)Configuration::get('ETS_AM_ENABLED_EMAIL_DECLINE_APP')
        ));

        $this->_html .= $this->display(__FILE__, 'user/view.tpl');
    }

    public function renderList($params)
    {
        if (!$params) return $this->_html;
        $params = $params['list'] + array('fields_list' => $params['fields'],'toolbar_btn'=> isset($params['toolbar_btn'])?$params['toolbar_btn']:false);
        $fields_list = isset($params['fields_list']) && $params['fields_list'] ? $params['fields_list'] : false;
        if (!$fields_list) return false;
        //$this->initToolbar($params);
        $helper = new HelperList();
        $helper->title = isset($params['title']) && $params['title'] ? $params['title'] : '';
        $helper->table = isset($params['list_id']) && $params['list_id'] ? $params['list_id'] : $this->list_id;
        $helper->identifier = $params['primary_key'];
        if (version_compare(_PS_VERSION_, '1.6.1', '>=')) {
            $helper->_pagination = array(25, 50, 100);
            $helper->_default_pagination = 25;
        }
        $helper->_defaultOrderBy = $params['orderBy'];

        $this->processFilter($params);
        //Sort order
        $table_orderBy = $helper->table . 'Orderby';
        $table_orderway = $helper->table . 'Orderway';
        $order_by = urldecode(Tools::getValue($table_orderBy));
        if (!$order_by || !Validate::isCleanHtml($order_by)) {
            if ($this->context->cookie->{$table_orderBy}) {
                $order_by = $this->context->cookie->{$table_orderBy};
            } elseif ($helper->orderBy) {
                $order_by = $helper->orderBy;
            } else {
                $order_by = $helper->_defaultOrderBy;
            }
        }
        $order_way = urldecode(Tools::getValue($table_orderway));
        if (!$order_way || !Validate::isCleanHtml($order_way)) {
            if ($this->context->cookie->{$table_orderway}) {
                $order_way = $this->context->cookie->{$table_orderway};
            } elseif ($helper->orderWay) {
                $order_way = $helper->orderWay;
            } else {
                $order_way = $params['orderWay'];
            }
        }

        if (isset($fields_list[$order_by]) && isset($fields_list[$order_by]['filter_key'])) {
            $order_by = $fields_list[$order_by]['filter_key'];
        }
        //Pagination.
        $key_pagination =$helper->table . '_pagination';
        $limit = (int)Tools::getValue($key_pagination);
        if (!$limit) {
            if (isset($this->context->cookie->{$key_pagination}) && $this->context->cookie->{$key_pagination})
                $limit = $this->context->cookie->{$key_pagination};
            else
                $limit = (version_compare(_PS_VERSION_, '1.6.1', '>=') ? $helper->_default_pagination : 20);
        }
        if ($limit) {
            $this->context->cookie->{$key_pagination} = $limit;
        } else {
            unset($this->context->cookie->{$key_pagination});
        }
        $start = 0;
        $key = $helper->table . '_start';
        $submit = (int)Tools::getValue('submitFilter' . $helper->table);
        if ($submit) {
            $start = ($submit - 1) * $limit;
        } elseif (empty($start) && isset($this->context->cookie->{$key}) && Tools::isSubmit('export' . $helper->table)) {
            $start = $this->context->cookie->{$key};
        }
        if ($start) {
            $this->context->cookie->{$key} = $start;
        } elseif (isset($this->context->cookie->{$key})) {
            unset($this->context->cookie->{$key});
        }
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)
            || !is_numeric($start) || !is_numeric($limit)) {
            $this->_errors = $this->l('Parameter list is not valid');
        }
        $helper->orderBy = $order_by;
        if (preg_match('/[.!]/', $order_by)) {
            $order_by_split = preg_split('/[.!]/', $order_by);
            $order_by = bqSQL($order_by_split[0]) . '.`' . bqSQL($order_by_split[1]) . '`';
        } elseif ($order_by) {
            $order_by = '`' . bqSQL($order_by) . '`';
        }
        $args = array(
            'filter' => $this->_filter,
            'having' => $this->_filterHaving,
        );
        if (isset($params['id_customer']) && $params['id_customer']) {
            $args += array('id_customer' => (int)$params['id_customer']);
        }
        if (isset($params['status'])) {
            $args['status'] = $params['status'];
        }
        $helper->listTotal = EtsAmAdmin::{$params['nb']}(true);
        $args += array(
            'start' => $start,
            'limit' => $limit,
            'sort' => $params['alias'] . '.' . $order_by . ' ' . Tools::strtoupper($order_way),
        );
        if (!Tools::getIsset('submitFilter' . $helper->table) && !Tools::getIsset('submitReset' . $helper->table)) {
           $this->context->cookie->__set($helper->table . 'Filter_user_status', 1);
           $this->context->cookie->__set($helper->table . 'Filter_has_reward', 1);
        }
        if (Tools::getIsset('submitReset' . $helper->table)) {
           $this->context->cookie->__set($helper->table . 'Filter_has_reward', '');
        }
        $list = EtsAmAdmin::{$params['nb']}();
        $helper->orderWay = Tools::strtoupper($order_way);
        //$helper->toolbar_btn = false;
        $helper->shopLinkType = '';
        $helper->row_hover = true;
        $helper->no_link = $params['no_link'];
        $helper->simple_header = false;
        $helper->actions = !(isset($params['id_customer'])) ? $params['actions'] : array();
        $this->_helperlist = $helper;
        $helper->show_toolbar = false;
        if($params['toolbar_btn'])
            $helper->toolbar_btn =  $params['toolbar_btn'];
        $helper->module = $this;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&tabActive=reward_users';
        $helper->bulk_actions = $params['bulk_actions'] ? $params['bulk_actions'] : false;
        $helper->actions = array('view', 'active');
        $this->context->smarty->assign(
            array(
                'aff_link_search_customer'=> $this->context->link->getAdminLink('AdminEtsAmAffiliate').'&ajax_search_customer=1',// $this->getBaseLink().'/modules/'.$this->name.'/ajax_search_customer.php?token='.md5($this->id),
            )
        );
        if (isset($params['id_customer']) && $params['id_customer']) {
            return $helper->generateList($list, $fields_list);
        }
        
        $this->_html .= (!empty($params['html']) ? $params['html'] : '') . $helper->generateList($list, $fields_list);
    }
    public function getBaseLink()
    {
        $link = (Configuration::get('PS_SSL_ENABLED_EVERYWHERE')?'https://':'http://').$this->context->shop->domain.$this->context->shop->getBaseURI();
        return trim($link,'/');
    }    
    public function displayActiveLink($token = null, $id)
    {
        $customer = new Customer($id);
        if($token){
            //
        }
        $user = array();
        if ($customer && $customer->id) {
            $user = Ets_User::getUserByCustomerId($id);
        }
        $this->smarty->assign(array(
            'customer' => $customer,
            'user' => $user,
            'item_id' => $id
        ));
        return $this->display(__FILE__, 'helper_active_link.tpl');
    }

    /**
     * @param $params
     * @return bool
     * @throws PrestaShopException
     */
    public function maxFilter($key, $value)
    {
        $search_max = array(
            'reward_balance',
            'loy_rewards',
            'ref_rewards',
            'ref_orders',
            'aff_rewards',
            'aff_orders',
            'total_withdraws',
        );
        foreach ($search_max as $max) {
            if (stripos($key, $max) !== -1) {
                $index = $key . '_max';
                $this->context->cookie->{$index} = !is_array($value) ? $value : serialize($value);
            }
        }
        $this->context->cookie->write();
    }

    public function processFilter($params)
    {
        if (empty($params) || empty($params['list_id']))
            return false;
        if (!empty($_POST) && isset($params['list_id'])) {
            foreach ($_POST as $key => $value) {
                if ($value === '') {
                    unset($this->context->cookie->{ $key});
                } elseif (stripos($key, $params['list_id'] . 'Filter_') === 0) {
                    $this->context->cookie->{$key} = !is_array($value) ? $value : serialize($value);
                } elseif (stripos($key, 'submitFilter') === 0) {
                    $this->context->cookie->$key = !is_array($value) ? $value : serialize($value);
                }
            }
        }
        if (!empty($_GET) && isset($params['list_id'])) {
            foreach ($_GET as $key => $value) {
                if (stripos($key, $params['list_id'] . 'Filter_') === 0) {
                    $this->maxFilter($key, $value);
                    $this->context->cookie->{$key} = !is_array($value) ? $value : serialize($value);
                } elseif (stripos($key, 'submitFilter') === 0) {
                    $this->context->cookie->$key = !is_array($value) ? $value : serialize($value);
                }
                if (stripos($key, $params['list_id'] . 'Orderby') === 0 && Validate::isOrderBy($value)) {
                    if ($value === '' || $value == $params['orderBy']) {
                        unset($this->context->cookie->{$key});
                    } else {
                        $this->context->cookie->{$key} = $value;
                    }
                } elseif (stripos($key, $params['list_id'] . 'Orderway') === 0 && Validate::isOrderWay($value)) {
                    
                    if ($value === '' || $value == $params['orderWay']) {
                        unset($this->context->cookie->{$key});
                    } else {
                        $this->context->cookie->{$key} = $value;
                    }
                }
            }
        }

        $filters = $this->context->cookie->getFamily($params['list_id'] . 'Filter_');

        foreach ($filters as $key => $value) {
            /* Extracting filters from $_POST on key filter_ */
            if ($value != null && !strncmp($key, $params['list_id'] . 'Filter_', 7 + Tools::strlen( $params['list_id']))) {
                $key = Tools::substr($key, 7 + Tools::strlen($params['list_id']));
                /* Table alias could be specified using a ! eg. alias!field */
                $tmp_tab = explode('!', $key);
                $filter = count($tmp_tab) > 1 ? $tmp_tab[1] : $tmp_tab[0];

                if ($field = $this->filterToField($key, $filter, $params['fields_list'])) {
                    $type = (array_key_exists('filter_type', $field) ? $field['filter_type'] : (array_key_exists('type', $field) ? $field['type'] : false));
                    if (($type == 'date' || $type == 'datetime') && is_string($value))
                        $value = Tools::unSerialize($value);
                    $key = isset($tmp_tab[1]) ? $tmp_tab[0] . '.`' . $tmp_tab[1] . '`' : '`' . $tmp_tab[0] . '`';
                    $sql_filter = '';
                    /* Only for date filtering (from, to) */
                    if (is_array($value)) {
                        if (isset($value[0]) && !empty($value[0])) {
                            if (!Validate::isDate($value[0])) {
                                $this->errors[] = Tools::displayError('The \'From\' date format is invalid (YYYY-MM-DD)');
                            } else {
                                $sql_filter .= ' AND ' . pSQL($key) . ' >= \'' . pSQL(Tools::dateFrom($value[0])) . '\'';
                            }
                        }

                        if (isset($value[1]) && !empty($value[1])) {
                            if (!Validate::isDate($value[1])) {
                                $this->errors[] = Tools::displayError('The \'To\' date format is invalid (YYYY-MM-DD)');
                            } else {
                                $sql_filter .= ' AND ' . pSQL($key) . ' <= \'' . pSQL(Tools::dateTo($value[1])) . '\'';
                            }
                        }
                    } else {
                        $sql_filter .= ' AND ';
                        $check_key = ($key == 'id_' . $params['list_id'] || $key == '`id_' . $params['list_id'] . '`');
                        $alias = $params['alias'];

                        if ($type == 'int' || $type == 'bool') {
                            $sql_filter .= (($check_key || $key == '`active`') ? $alias . '.' : '') . pSQL($key) . ' = ' . (int)($key == '`position`' ? $value - 1 : $value) . ' ';
                        } elseif ($type == 'decimal') {
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' = ' . (float)$value . ' ';
                        } elseif ($type == 'select') {
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' = \'' . pSQL($value) . '\' ';
                        } elseif ($type == 'price') {
                            $value = (float)str_replace(',', '.', $value);
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' = ' . pSQL(trim($value)) . ' ';
                        } else {
                            $sql_filter .= ($check_key ? $alias . '.' : '') . pSQL($key) . ' LIKE \'%' . pSQL(trim($value)) . '%\' ';
                        }
                    }
                    if (isset($field['havingFilter']) && $field['havingFilter'])
                        $this->_filterHaving .= $sql_filter;
                    else
                        $this->_filter .= $sql_filter;
                }
            }
        }
    }

    /**
     * @param $key
     * @param $filter
     * @param $fields_list
     * @return bool
     */
    protected function filterToField($key, $filter, $fields_list)
    {
        if (empty($fields_list))
            return false;

        foreach ($fields_list as $field)
            if (array_key_exists('filter_key', $field) && $field['filter_key'] == $key)
                return $field;
        if (array_key_exists($filter, $fields_list))
            return $fields_list[$filter];
        return false;
    }

    /**
     * @param $params
     */
    public function initToolbar($params)
    {
        $this->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex . '&configure=' . $this->name . $params['list_id'] . '&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add') . ' ' . Tools::strtolower($params['title']),
        );
    }

    protected function loadmoreSponsors($sponsors)
    {
        $this->smarty->assign(array(
            'sponsors' => $sponsors
        ));
        return $this->display(__FILE__, 'user/paginate_sponsors.tpl');
    }

    protected function loadmoreRewardHistory($histories)
    {
        $this->smarty->assign(array(
            'reward_history' => $histories
        ));
        return $this->display(__FILE__, 'user/paginate_history_reward.tpl');
    }

    public function renderImportExportForm()
    {
        if (Tools::isSubmit('exportAllData', false)) {
            $export = new Ets_ImportExport();
            $export->generateArchive();
        } elseif (Tools::isSubmit('importAllData', false)) {
           $import = new Ets_ImportExport();
            $this->context->smarty->assign(
                array(
                    'restore_reward' => (int)Tools::getValue('restore_reward', false),
                    'restore_config' => (int)Tools::getValue('restore_config', false),
                    'delete_reawrd' => (int)Tools::getValue('delete_reawrd', false),
                )
            );
            $errors = $import->processImport();
            if ($errors) {
                $this->_html .= $this->displayError($errors);
            } else {
                $this->_html .= $this->displayConfirmation($this->l('Import successfully'));
            } 
            
        }


        $this->_html .= $this->display(__FILE__, 'import_export.tpl');
    }

    public function getSearchSuggestions($query, $query_type)
    {
        $results = EtsAmAdmin::getSearchSuggestionsReward($query, $query_type);
        $this->smarty->assign(array(
            'results' => $results,
        ));

        return $this->display(__FILE__, 'search_suggestion.tpl');
    }

    public function hookDisplayCustomerAccountForm($params)
    {
        if((int)Configuration::get('ETS_AM_REF_ENABLED') && !$this->context->customer->logged){
            $ref = $this->context->cookie->__get(EAM_REFS);
            $email_sponsor = '';
            if ($ref) {
                $customer = new Customer((int)$ref);
                if ($customer) {
                    $email_sponsor = $customer->email;
                }
            }
            $this->smarty->assign(array(
                'query' => Tools::getAllValues(),
                'email_sponsor' => $email_sponsor,
                'is17' => $this->is17,
            ));
            //die(Tools::getValue('eam_code_ref'));
            return $this->display(__FILE__, 'reg_code_ref.tpl');
        }
        return '';
        
    }

    public function removeImages()
    {
        $dir = scandir(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER);
        if (!empty($dir)) {
            foreach ($dir as $file) {
                if ($file !== '.' && $file !== '..' && $file !== 'index.php') {
                    @unlink(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $file);
                }
            }
        }
        return true;
    }

    public function getAffiliateMessage($data){
        $this->smarty->assign($data);
        return $this->display(__FILE__, 'affiliate_message.tpl');
    }

    public function getHtmlColum($params = array()){
        $this->smarty->assign($params);
        return $this->display(__FILE__, 'html_col.tpl');
    }

    public function cronjobSettings(){
        $cronjob_last= '';
        $run_cronjob = false;
        if(file_exists(dirname(__FILE__).'/cronjob_time.log') && $cronjob_time = Tools::file_get_contents(dirname(__FILE__).'/cronjob_time.log'))
        {
            $last_time = strtotime($cronjob_time);
            $time = strtotime(date('Y-m-d H:i:s'))-$last_time;
            if($time <= 43200 && $time)
                $run_cronjob = true;
            else
                $run_cronjob = false;
            if($time > 86400)
                $cronjob_last = $cronjob_time;
            elseif($time)
            {
                if($hours =floor($time/3600))
                {
                    $cronjob_last .= $hours.' '.$this->l('hours').' ';
                    $time = $time%3600;
                }
                if($minutes = floor($time/60))
                {
                    $cronjob_last .= $minutes.' '.$this->l('minutes').' ';
                    $time = $time%60;
                }
                if($time)
                    $cronjob_last .= $time.' '.$this->l('seconds').' ';
                $cronjob_last .= $this->l('ago');
            }    
        }
        $this->smarty->assign(array(
            'cronjob_token' => Configuration::getGlobalValue('ETS_AM_CRONJOB_TOKEN'),
            'cronjob_link' => $this->context->link->getAdminLink('AdminEtsAmCronjob'),
            'cronjob_dir' => _PS_MODULE_DIR_.'ets_affiliatemarketing/cronjob.php',
            'cronjob_demo' => Ets_AM::getBaseUrl(true).'cronjob.php',
            'cronjob_last' => $cronjob_last,
            'run_cronjob' => $run_cronjob,
            'loyaltyPrograEnabled' =>Configuration::get('ETS_AM_LOYALTY_ENABLED'),
            'loyaltyRewardAvailability' =>Configuration::get('ETS_AM_LOYALTY_MAX_DAY'),
        ));
        $this->_html .= $this->display(__FILE__, 'cronjob_settings.tpl');
    }

    public function generateTokenCronjob(){
        $code = $this->generateRandomString();
        Configuration::updateGlobalValue('ETS_AM_CRONJOB_TOKEN', $code);
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = Tools::strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function cronjobHistory(){
        if(Tools::isSubmit('ETS_AM_SAVE_LOG'))
        {
            $ETS_AM_SAVE_LOG = (int)Tools::getValue('ETS_AM_SAVE_LOG');
            Configuration::updateGlobalValue('ETS_AM_SAVE_LOG',$ETS_AM_SAVE_LOG);
            die(
                Tools::jsonEncode(array(
                    'success' => $this->l('Updated successful')
                ))
            );
        }
        $log_path = _PS_MODULE_DIR_.'ets_affiliatemarketing/cronjob.log';
        $log = '';
        if(file_exists($log_path)){
           $log = Tools::file_get_contents($log_path);
        }
        $cronjob_last= '';
        if(file_exists(dirname(__FILE__).'/cronjob_time.log') && $cronjob_time = Tools::file_get_contents(dirname(__FILE__).'/cronjob_time.log'))
        {
            $last_time = strtotime($cronjob_time);
            $time = strtotime(date('Y-m-d H:i:s'))-$last_time;
            if($time <= 43200 && $time)
                $run_cronjob = true;
            else
                $run_cronjob = false;
            if($time > 86400)
                $cronjob_last = $cronjob_time;
            elseif($time)
            {
                if($hours =floor($time/3600))
                {
                    $cronjob_last .= $hours.' '.$this->l('hours').' ';
                    $time = $time%3600;
                }
                if($minutes = floor($time/60))
                {
                    $cronjob_last .= $minutes.' '.$this->l('minutes').' ';
                    $time = $time%60;
                }
                if($time)
                    $cronjob_last .= $time.' '.$this->l('seconds').' ';
                $cronjob_last .= $this->l('ago');
            }    
        }
        else
            $run_cronjob = false;
        $this->smarty->assign(array(
            'log' => $log,
            'ETS_AM_SAVE_LOG' => Configuration::getGlobalValue('ETS_AM_SAVE_LOG'), 
            'post_url' => $this->context->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name,
            'cronjob_last' => $cronjob_last,
            'run_cronjob' => $run_cronjob,
            'loyaltyPrograEnabled' =>Configuration::get('ETS_AM_LOYALTY_ENABLED'),
            'loyaltyRewardAvailability' =>Configuration::get('ETS_AM_LOYALTY_MAX_DAY'),
        ));
        $this->_html = $this->display(__FILE__, 'cronjob_history.tpl');
    }

    public function getPercentReward($params= array()){
        //$params = array();
        
        if (($filter_date_from = Tools::getValue('filter_date_from')) && Validate::isCleanHtml($filter_date_from)) {
            $params['date_from'] = $filter_date_from;
        }
        if (($filter_date_to = Tools::getValue('filter_date_to')) && Validate::isCleanHtml($filter_date_to)) {
            $params['date_to'] = $filter_date_to;
        }
        if (($filter_date_type = Tools::getValue('filter_date_type')) && Validate::isCleanHtml($filter_date_type) ) {
            $params['date_type'] = $filter_date_type;
        }
        die(Tools::jsonEncode(Ets_AM::getPercentReward($params)));
    }

    public function getBreadcrumb(){
        $controller = Tools::getValue('controller');
        $node = array();
        $node[] = array(
            'title' => $this->l('Home'),
            'url' => $this->context->link->getPageLink('index', true),
        );
        $node[] = array(
            'title' => $this->l('Your account'),
            'url' => $this->context->link->getPageLink('my-account', true),
        );
        if($controller == 'aff_products'){
            $node[] = array(
                'title' => $this->l('Affiliate program'),
                'url' => $this->context->link->getModuleLink($this->name,'aff_products'),
            );
            $node[] = array(
                'title' => $this->l('Affiliate Products'),
                'url' => $this->context->link->getModuleLink($this->name,'aff_products'),
            );
        }
        if($controller == 'my_sale'){
            $node[] = array(
                'title' => $this->l('Affiliate program'),
                'url' => $this->context->link->getModuleLink($this->name,'aff_products'),
            );
            $node[] = array(
                'title' => $this->l('My sales'),
                'url' => $this->getLinks('my_sale')
            );
            if(($id_product = (int)Tools::getValue('id_product', false))){
                $product = new Product($id_product, false, (int)$this->context->language->id);
                $product_link = Ets_Affiliate::generateAffiliateLinkForProduct($product, $this->context, false);

                $node[] = array(
                    'title' => $product->name,
                    'url' => $product_link
                );
            }
            elseif(($tab_active = Tools::getValue('tab_active', false)) && $tab_active == 'statistics'){
                $node[] = array(
                    'title' => $this->l('Statistics'),
                    'url' => $this->getLinks('my_sale', array('tab_active' => 'statistics'))
                );
            }

            
        }
        
        if($controller == 'sponsorship'){
            if(($tab = Tools::getValue('tab', false)) && $tab == 'how-to-refer-friends'){
                $node[] = array(
                    'title' => $this->l('Referral program'),
                    'url' => $this->getLinks('refer_friends')
                );
                $node[] = array(
                    'title' => $this->l('How to refer friends'),
                    'url' => $this->getLinks('sponsorship', array('tab' => 'how-to-refer-friends'))
                );
            }
            else{
                $node[] = array(
                    'title' => $this->l('Referral program'),
                    'url' => $this->getLinks('refer_friends')
                );
                $node[] = array(
                    'title' => $this->l('My friends'),
                    'url' => $this->getLinks('sponsorship')
                );
            }
        }
        if($controller == 'refer_friends'){
            $node[] = array(
                'title' => $this->l('Referral program'),
                'url' => $this->getLinks('refer_friends')
            );
            $node[] = array(
                'title' => $this->l('How to refer friends'),
                'url' => $this->getLinks('refer-friends')
            );
        }
        if($controller == 'loyalty'){
            $node[] = array(
                'title' => $this->l('Loyalty program'),
                'url' => $this->getLinks('loyalty')
            );
        }
        if($controller == 'register'){
            $node[] = array(
                'title' => $this->l('Register program'),
                'url' => $this->getLinks('register')
            );
        }
        if($controller == 'dashboard'){
            $node[] = array(
                'title' => $this->l('My rewards'),
                'url' => $this->getLinks('dashboard')
            );
            $node[] = array(
                'title' => $this->l('Dashboard'),
                'url' => $this->getLinks('dashboard')
            );
        }
        if($controller == 'history'){
            $node[] = array(
                'title' => $this->l('My rewards'),
                'url' => $this->getLinks('dashboard')
            );
            $node[] = array(
                'title' => $this->l('Reward history'),
                'url' => $this->getLinks('history')
            );
        }
        if($controller == 'withdraw'){
            $node[] = array(
                'title' => $this->l('My rewards'),
                'url' => $this->getLinks('dashboard')
            );
            $node[] = array(
                'title' => $this->l('Withdrawals'),
                'url' => $this->getLinks('withdraw')
            );
        }
        if($controller == 'voucher'){
            $node[] = array(
                'title' => $this->l('My rewards'),
                'url' => $this->getLinks('dashboard')
            );
            $node[] = array(
                'title' => $this->l('Convert into vouchers'),
                'url' => $this->getLinks('voucher')
            );
        }

        if($this->is17)
            return array('links' => $node,'count' => count($node));
        return $this->displayBreadcrumb($node);
        
    }

    public function getLinks($controller, $params = array()){
        if($controller == 'aff_product'){
            return Ets_AM::getBaseUrlDefault('aff_product',$params);
        }
        elseif($controller == 'my_sale'){
            return Ets_AM::getBaseUrlDefault('my_sale',$params);
        }
        elseif($controller == 'sponsorship'){
            if(!$params){
                return Ets_AM::getBaseUrlDefault('sponsorship',$params);
            }
            elseif($params && isset($params['tab']) && $params['tab'] == 'how-to-refer-friends'){
                return Ets_AM::getBaseUrlDefault('sponsorship',array('tab'=>'tab=how-to-refer-friends'));
            }
        }elseif($controller=='refer_friends')
            return Ets_AM::getBaseUrlDefault('refer_friends',$params);
        elseif($controller == 'loyalty'){
            return Ets_AM::getBaseUrlDefault('loyalty',$params);
        }
        elseif($controller == 'dashboard'){
            return Ets_AM::getBaseUrlDefault('dashboard',$params);
        }
        elseif($controller == 'history'){
            return Ets_AM::getBaseUrlDefault('history',$params);
        }
        elseif($controller == 'withdraw'){
            return Ets_AM::getBaseUrlDefault('withdraw',$params);
        }
        elseif($controller == 'voucher'){
            return Ets_AM::getBaseUrlDefault('voucher',$params);
        }
        elseif($controller == 'register'){
            return Ets_AM::getBaseUrlDefault('register',$params);
        }

        return '/';
    }
    public function displayBreadcrumb($node = array()){
        if($node){
            $this->smarty->assign(array(
                'nodes' => $node,
            ));
            return $this->display(__FILE__,'breadcrumb.tpl');
        }
        return '';
    }
    public function saveCartRule($id_cart_rule=0)
    {
        $languages = Language::getLanguages(false);
        if($id_cart_rule)
        {
            $cartRuleObj = new CartRule($id_cart_rule);
            $cartRuleObj->active = Configuration::get('ETS_AM_SELL_OFFER_VOUCHER') ? 1: 0;
        }
        else
        {
            $quantity = (int)Tools::getValue('ETS_AM_SELL_QUANTITY') ? :999;
            $prefix = Configuration::get('ETS_AM_SELL_DISCOUNT_PREFIX');
            $code = Ets_AM::generatePromoCode($prefix); 
            $discount_in = Configuration::get('ETS_AM_SELL_APPLY_DISCOUNT_IN');
            $cartRuleObj = new CartRule();
            $cartRuleObj->quantity = $quantity;
            $cartRuleObj->code = $code;
            $cartRuleObj->date_from = date('Y-m-d H:i:s');
            $cartRuleObj->date_to = date('Y-m-d H:i:s', strtotime('+' . $discount_in . 'days', strtotime(date('Y-m-d H:i:s'))));
            foreach ($languages as $lang) {
               $cartRuleObj->name[(int)$lang['id_lang']] = Configuration::get('ETS_AM_SELL_DISCOUNT_DESC',$lang['id_lang']);
            }
            $cartRuleObj->active = 1;
            $cartRuleObj->id_customer = 0;
            $cartRuleObj ->reduction_exclude_special = (int)Configuration::get('ETS_AM_SELL_EXCLUDE_SPECIAL');
        }
        $discount_percent = Configuration::get('ETS_AM_SELL_APPLY_DISCOUNT') =='PERCENT' ? Configuration::get('ETS_AM_SELL_REDUCTION_PERCENT'):0;
        $discount_amount = Configuration::get('ETS_AM_SELL_APPLY_DISCOUNT') =='AMOUNT' ? Configuration::get('ETS_AM_SELL_REDUCTION_AMOUNT'):0;
        $id_currency = Configuration::get('ETS_AM_SELL_ID_CURRENCY');
        $reduction_tax = Configuration::get('ETS_AM_SELL_REDUCTION_TAX');
        $free_shipping = Configuration::get('ETS_AM_SELL_FREE_SHIPPING');
        $voucher_min_amount = Configuration::get('ETS_AM_SELL_DISCOUNT_MIN_AMOUNT');
        $voucher_min_amount_currency = Configuration::get('ETS_AM_SELL_DISCOUNT_MIN_AMOUNT_CURRENCY');
        $voucher_min_amount_tax = Configuration::get('ETS_AM_SELL_DISCOUNT_MIN_AMOUNT_TAX');
        $voucher_min_amount_shipping = Configuration::get('ETS_AM_SELL_DISCOUNT_MIN_AMOUNT_SHIPPING');
        $cartRuleObj->quantity_per_user = 1;
        $cartRuleObj->reduction_percent = $discount_percent;
        $cartRuleObj->reduction_amount = $discount_amount;
        $cartRuleObj->reduction_currency = $id_currency;
        $cartRuleObj->reduction_product = 0;
        $cartRuleObj->reduction_tax = $reduction_tax;
        $cartRuleObj->free_shipping = $free_shipping;
        $cartRuleObj->minimum_amount = $voucher_min_amount;
        if($voucher_min_amount){
            $cartRuleObj->minimum_amount_tax = $voucher_min_amount_tax;
            $cartRuleObj->minimum_amount_currency = $voucher_min_amount_currency;
            $cartRuleObj->minimum_amount_shipping = $voucher_min_amount_shipping;
        }
        if($id_cart_rule)
            $cartRuleObj->update();
        elseif(!$cartRuleObj->add())
            return false;
        return $cartRuleObj; 
    }
    public function ajaxSearchFriends()
    {
        if(($customer = Tools::getValue('customer')) && Validate::isCleanHtml($customer))
        {
            $id_customer = (int)Tools::getValue('id_customer');
            $customers= Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'customer` WHERE id_customer!="'.(int)$id_customer.'" AND id_shop = "'.(int)$this->context->shop->id.'" AND (id_customer="'.(int)$customer.'" OR email like "%'.pSQL($customer).'%" OR firstname like "%'.pSQL($customer).'%" OR lastname like "%'.pSQL($customer).'%" OR CONCAT(firstname," ",lastname) LIKE "%'.pSQL($customer).'%")');
            if($customers)
            {
                foreach($customers as &$item)
                {
                    $sponsor = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ets_am_sponsor` WHERE id_customer="'.(int)$item['id_customer'].'" AND level=1');
                    if($sponsor)
                    {
                        if($sponsor['id_parent'] == $id_customer)
                            $item['friend'] =1;
                        else
                            $item['friend'] = 2;
                    }
                    elseif(Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ets_am_sponsor` WHERE id_parent="'.(int)$item['id_customer'].'"'))
                        $item['friend']=3;
                    else
                        $item['friend'] =0;
                    
                }
            }
            $this->context->smarty->assign(
                array(
                    'customers' => $customers,
                )
            );
            die(
                Tools::jsonEncode(
                    array(
                        'list_customers' => $this->display(__FILE__,'user/list_customer.tpl'),
                    )
                )
            );
        }
    }
    public function ajaxAddFriend()
    {
        $id_sponsor = (int)Tools::getValue('id_customer');
        $id_customer= (int)Tools::getValue('id_friend');
        $id_shop = $this->context->shop->id;
        if(!Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ets_am_sponsor` WHERE id_customer="'.(int)$id_customer.'"'))
        {
            $eam_sponsor = new Ets_Sponsor();
            $eam_sponsor->id_customer = $id_customer;
            $eam_sponsor->id_parent = $id_sponsor;
            $eam_sponsor->level = 1;
            $eam_sponsor->id_shop = $id_shop;
            $eam_sponsor->datetime_added = date('Y-m-d H:i:s');
            if($eam_sponsor->add())
            {
                $sqlGetParent = "SELECT * FROM `"._DB_PREFIX_."ets_am_sponsor` WHERE id_customer = ".(int)$id_sponsor." AND id_shop = ".(int)$id_shop;
                $parents = Db::getInstance()->executeS($sqlGetParent);
                if(count($parents)){
                    $values_insert = '';
                    $datetime_added = date('Y-m-d H:i:s');
                    foreach ($parents as $parent) {
                        $values_insert .= "($id_customer, ".(int)$parent['id_parent'].", ".((int)$parent['level'] + 1).", ".(int)$id_shop.", '$datetime_added'),";
                      
                    }
                    $sql_insert = "INSERT INTO `"._DB_PREFIX_."ets_am_sponsor` (`id_customer`, `id_parent`, `level`, `id_shop`, `datetime_added`) VALUES ".trim($values_insert, ',');
                    Db::getInstance()->execute($sql_insert);
                }
                $sponsor  = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'customer` c 
                INNER JOIN `'._DB_PREFIX_.'ets_am_sponsor` eas ON (c.id_customer = eas.id_customer)
                WHERE c.id_customer="'.(int)$id_customer.'"');
                $sponsor['link'] = $this->context->link->getAdminLink('AdminCustomers').'&viewcustomer&id_customer='.$id_customer;
                $sponsor['reward'] = Ets_affiliatemarketing::displayPrice(0);
                $sponsor['total_order'] = Db::getInstance()->getValue('SELECT SUM(o2.total_paid_tax_incl) FROM `'._DB_PREFIX_.'orders` o2 WHERE id_order IN (SELECT id_order FROM `'._DB_PREFIX_.'ets_am_reward` er WHERE er.id_friend='.(int)$id_customer.')') ;
                $sponsor['total_order'] = $sponsor['total_order'] ? Ets_AM::displayRewardAdmin($sponsor['total_order']) : Ets_AM::displayRewardAdmin(0);
                die(
                    Tools::jsonEncode(
                        array(
                            'success' => $this->l('Added successful'),
                            'sponsor' => (array)$sponsor,
                        )
                    )  
                );
            }
            else
            {
                die(
                    Tools::jsonEncode(
                        array(
                            'errors' => $this->l('Add error'),
                        )
                    )
                );
            }
            
        }
        else
        {
            die(
                Tools::jsonEncode(
                    array(
                        'errors' => $this->l('This customer is already in friends list of another sponsor'),
                    )
                )
            );
        }
    }
    public function checkCreatedColumn($table,$column)
    {
        $fieldsCustomers = Db::getInstance()->ExecuteS('DESCRIBE '._DB_PREFIX_.pSQL($table));
        $check_add=false;
        foreach($fieldsCustomers as $field)
        {
            if($field['Field']==$column)
            {
                $check_add=true;
                break;
            }    
        }
        return $check_add;
    }
    public function hookDisplayOrderConfirmation()
    {
        $id_order = (int)Tools::getValue('id_order');
        if($reward = Db::getInstance()->getRow('SELECT id_order,status,SUM(amount) total_amount FROM `'._DB_PREFIX_.'ets_am_reward` WHERE id_order='.(int)$id_order.' AND program="'.pSQL(EAM_AM_LOYALTY_REWARD).'" GROUP BY id_order,status'))
        {
            $reward['status'] = trim($this->getStatus($reward['status']));
            $msg = Configuration::get('ETS_AM_LOYALTY_MSG_ORDER',$this->context->language->id);
            $this->context->smarty->assign(
                array(
                    'loyaty_msg' => str_replace(array('[amount]','[reward_status]'),array( Configuration::get('ETS_AM_REWARD_DISPLAY')=='point' ? Ets_AM::displayReward($reward['total_amount']) :   Ets_affiliatemarketing::displayPrice(Tools::convertPrice($reward['total_amount'])),$reward['status']),$msg),
                )
            );
            return $this->display(__FILE__,'order_confirmation.tpl');
        }
    }
    public function getStatus($status)
    {
        if($status==0){
            return Module::getInstanceByName('ets_affiliatemarketing')->displayHtml($this->l('Pending'),'span','loy_status pending');
        }
        if($status==1){
            return Module::getInstanceByName('ets_affiliatemarketing')->displayHtml($this->l('Approved'),'span','loy_status approved');
        }
        if($status==-1){
            return Module::getInstanceByName('ets_affiliatemarketing')->displayHtml($this->l('Canceled'),'span','loy_status canceled');
        }
        if ($status==-2){
            return Module::getInstanceByName('ets_affiliatemarketing')->displayHtml($this->l('Expired'),'span','loy_status expired');
        }
    }
    public function getPopupDefault()
    {
        return $this->display(__FILE__, 'ref_popup_default_content.tpl');
    }
    public function displaySuccessMessage($msg, $title = false, $link = false)
    {
         $this->smarty->assign(array(
            'msg' => $msg,
            'title' => $title,
            'link' => $link
         ));
         if($msg)
            return $this->display(__FILE__, 'success_message.tpl');
    }
    public static function displayPrice($price, $currency = null)
    {
        return Tools::displayPrice(floor(($price*100))/100, $currency);
    }
    public static function getContextLocale(Context $context)
    {
        $locale = $context->getCurrentLocale();
        if (null !== $locale) {
            return $locale;
        }
        $container = isset($context->controller) ? $context->controller->getContainer() : null;
        if (null === $container) {
            //$container = SymfonyContainer::getInstance();
            $container = call_user_func(array('SymfonyContainer', 'getInstance'));
        }

        /** @var LocaleRepository $localeRepository */
        $localeRepository = $container->get(self::SERVICE_LOCALE_REPOSITORY);
        $locale = $localeRepository->getLocale(
            $context->language->getLocale()
        );

        return $locale;
    }
    public function checkCartRuleValidity()
    {
        if((Tools::getValue('controller') == 'cart' || Tools::getValue('controller') == 'order') && Tools::isSubmit('addDiscount')&& (Tools::isSubmit('ajax') || Tools::isSubmit('ajax_request'))){             
            $error = '';
            $code = ($code = Tools::getValue('discount_name')) && Validate::isCleanHtml($code) ? $code : '';
            if($code && ($id_cart_rule = CartRule::getIdByCode($code))){
                $id_sponsor = Db::getInstance()->getValue('SELECT id_customer FROM `'._DB_PREFIX_.'ets_am_cart_rule_seller` WHERE id_cart_rule="'.(int)$id_cart_rule.'"');
                $sponsor_exists = (int)Db::getInstance()->getValue("SELECT COUNT(*) as total FROM `"._DB_PREFIX_."ets_am_sponsor` WHERE id_customer = ".(int)$this->context->customer->id." AND id_shop = ".(int)$this->context->shop->id.' AND id_parent!='.(int)$id_sponsor.' AND level=1');
                $sql = "SELECT COUNT(*) as total FROM `"._DB_PREFIX_."ets_am_sponsor` WHERE id_parent = ".(int)$this->context->customer->id." AND id_shop = ".(int)$this->context->shop->id;
                $parent_exists = (int)Db::getInstance()->getValue($sql);
                if($id_sponsor && ($sponsor_exists || $parent_exists ||  $id_sponsor==$this->context->customer->id))
                {
                    $error = $this->l('You cannot use this voucher');
                }
                else
                {
                    $voucherCode = null;                
                    if(!self::canUseCartRule($this->context->cart->id, $id_cart_rule, $voucherCode)){
                        $error = sprintf($this->l('Can not use voucher code %s with others voucher code'), $voucherCode);
                    }
                }
            }
            else{
                $error = $this->l('You voucher code is not exists');
            }
            if($error){
                if(isset($id_cart_rule) && $id_cart_rule)
                    Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'cart_cart_rule WHERE id_cart='.(int)$this->context->cart->id.' AND id_cart_rule='.(int)$id_cart_rule);
                die(Tools::jsonEncode(array(
                    'errors' => array($error),
                    'hasError' => true,
                    'quantity' => null,
                )));
            }
        }
    }
    public function hookActionFrontControllerAfterInit(){
        $this->checkCartRuleValidity();
    }
    public static function canUseCartRule($id_cart, $id_cart_rule, &$voucherCode)
    {
        $ETS_AM_CAN_USE_OTHER_VOUCHER = (int)Configuration::get('ETS_AM_CAN_USE_OTHER_VOUCHER');
        if($ETS_AM_CAN_USE_OTHER_VOUCHER)
            return true;
        
        if(!Context::getContext()->customer || !Context::getContext()->customer->isLogged()){
            return true;
        }
        $hasOtherCartRule = false;
        if((int)Db::getInstance()->getValue("SELECT r.id_voucher FROM `"._DB_PREFIX_."ets_am_reward_usage` r JOIN "._DB_PREFIX_."cart_rule c ON r.id_voucher=c.id_cart_rule WHERE r.id_customer=".(int)Context::getContext()->customer->id.' AND r.id_voucher='.(int)$id_cart_rule))
        {
            $id_other_cart_rule = (int)Db::getInstance()->getValue("SELECT ccr.id_cart_rule FROM `"._DB_PREFIX_."cart_cart_rule` ccr WHERE id_cart=".(int)$id_cart." AND id_cart_rule !=".(int)$id_cart_rule);
            if($id_other_cart_rule)
                $hasOtherCartRule = true;
        } 
        elseif($id_other_cart_rule = (int)Db::getInstance()->getValue("SELECT ccr.id_cart_rule FROM `"._DB_PREFIX_."cart_cart_rule` ccr WHERE id_cart=".(int)$id_cart." AND id_cart_rule IN (SELECT r.id_voucher FROM `"._DB_PREFIX_."ets_am_reward_usage` r JOIN "._DB_PREFIX_."cart_rule c ON r.id_voucher=c.id_cart_rule WHERE r.id_customer=".(int)Context::getContext()->customer->id.')'))
            $hasOtherCartRule = true;
        if($hasOtherCartRule){
            $cartRule = new CartRule($id_other_cart_rule);
            $voucherCode = $cartRule->code;
            return false;
        }  
        return true;
    }
    public function addOverride($classname)
    {
        if (Module::isInstalled('ets_abandonedcart') && $classname == 'CartRule')
            return true;
        return parent::addOverride($classname);
    }
    public function removeOverride($classname)
    {
        if (Module::isInstalled('ets_abandonedcart') && $classname == 'CartRule')
            return true;
        return parent::removeOverride($classname);
    }
    public static function loyRewardUsed($usageLOY,$id_reward_usage,$id_customer=0)
    {
        if(!$id_customer)
            $id_customer = Context::getContext()->customer->id;
        $id_shop = Context::getContext()->shop->id;
        $sql ='SELECT id_ets_am_reward,amount FROM `'._DB_PREFIX_.'ets_am_reward` WHERE used=0 AND id_shop="'.(int)$id_shop.'" AND id_customer="'.(int)$id_customer.'" AND status=1 AND deleted=0 AND program="'.pSQL(EAM_AM_LOYALTY_REWARD).'" ORDER BY datetime_added ASC';
        $rewards = Db::getInstance()->executeS($sql);
        if($rewards)
        {
            foreach($rewards as $reward)
            {
                if($usageLOY >0)
                {
                    Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ets_am_reward` SET used="'.(int)$id_reward_usage.'" WHERE id_ets_am_reward= "'.(int)$reward['id_ets_am_reward'].'"');
                    $usageLOY = $usageLOY - $reward['amount'] ;
                    if($usageLOY <0)
                    {
                        $usageLOY = -1*$usageLOY;
                        $new_reward = new Ets_AM($reward['id_ets_am_reward']);
                        $new_reward ->amount = $new_reward->amount-$usageLOY;
                        if($new_reward->update())
                        {
                            $new_reward->amount = $usageLOY;
                            $new_reward->used = 0;
                            unset($new_reward->id);
                            $new_reward->add();
                        }
                        break;
                    }
                }
                else
                    break;
            }
        }
    }
    public function renderCategoryTree($params)
    {
        $tree = new HelperTreeCategories($params['tree']['id'], isset($params['tree']['title']) ? $params['tree']['title'] : null);

        if (isset($params['name'])) {
            $tree->setInputName($params['name']);
        }

        if (isset($params['tree']['selected_categories'])) {
            $tree->setSelectedCategories($params['tree']['selected_categories']);
        }

        if (isset($params['tree']['disabled_categories'])) {
            $tree->setDisabledCategories($params['tree']['disabled_categories']);
        }

        if (isset($params['tree']['root_category'])) {
            $tree->setRootCategory($params['tree']['root_category']);
        }

        if (isset($params['tree']['use_search'])) {
            $tree->setUseSearch($params['tree']['use_search']);
        }

        if (isset($params['tree']['use_checkbox'])) {
            $tree->setUseCheckBox($params['tree']['use_checkbox']);
        }

        if (isset($params['tree']['set_data'])) {
            $tree->setData($params['tree']['set_data']);
        }
        return $tree->render();
    }
    public function getTextLang($text, $lang,$file_name='')
    {
        if(is_array($lang))
            $iso_code = $lang['iso_code'];
        elseif(is_object($lang))
            $iso_code = $lang->iso_code;
        else
        {
            $language = new Language($lang);
            $iso_code = $language->iso_code;
        }
		$modulePath = rtrim(_PS_MODULE_DIR_, '/').'/'.$this->name;
        $fileTransDir = $modulePath.'/translations/'.$iso_code.'.'.'php';
        if(!@file_exists($fileTransDir)){
            return $text;
        }
        $fileContent = Tools::file_get_contents($fileTransDir);
        $text_tras = preg_replace("/\\\*'/", "\'", $text);
        $strMd5 = md5($text_tras);
        $keyMd5 = '<{' . $this->name . '}prestashop>' . ($file_name ? : $this->name) . '_' . $strMd5;
        preg_match('/(\$_MODULE\[\'' . preg_quote($keyMd5) . '\'\]\s*=\s*\')(.*)(\';)/', $fileContent, $matches);
        if($matches && isset($matches[2])){
           return  $matches[2];
        }
        return $text;
    }
    public function copy_directory($src, $dst)
    {
        if(!file_exists($dst))
            return true;
        $dir = opendir($src);
        if(!file_exists($dst))
            @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $this->copy_directory($src . '/' . $file, $dst . '/' . $file);
                } elseif(!file_exists($dst . '/' . $file)) {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
    public function displayHtml($content=null,$tag,$class=null,$id=null,$href=null,$blank=false,$src = null,$name = null,$value = null,$type = null,$data_id_product = null,$rel = null,$attr_datas=null)
    {
        $this->smarty->assign(
            array(
                'content' =>$content,
                'tag' => $tag,
                'class'=> $class,
                'id' => $id,
                'href' => $href,
                'blank' => $blank,
                'src' => $src,
                'name' => $name,
                'value' => $value,
                'type' => $type,
                'data_id_product' => $data_id_product,
                'attr_datas' => $attr_datas,
                'rel' => $rel,
            )
        );
        return $this->display(__FILE__,'html.tpl');
    }
    public static function validateArray($array,$validate='isCleanHtml')
    {
        if(!is_array($array))
            return false;
        if(method_exists('Validate',$validate))
        {
            if($array && is_array($array))
            {
                $ok= true;
                foreach($array as $val)
                {
                    if(!is_array($val))
                    {
                        if($val && !Validate::$validate($val))
                        {
                            $ok= false;
                            break;
                        }
                    }
                    else
                        $ok = self::validateArray($val,$validate);
                }
                return $ok;
            }
        }
        return true;
    }
}
