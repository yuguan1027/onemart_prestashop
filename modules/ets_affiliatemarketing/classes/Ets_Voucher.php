<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <contact@etssoft.net>
 *  @copyright  2007-2021 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_')) {
    exit();
}

class Ets_Voucher extends ObjectModel
{
    /**
     * @var int
     */
    public $id_ets_am_voucher;
    /**
     * @var int
     */
    public $id_cart_rule;
    /**
     * @var int
     */
    public $id_customer;
    /**
     * @var int
     */
    public $id_product;
    /**
     * @var int
     */
    public $id_cart;
  
    public static $definition = array(
        'table' => 'ets_am_voucher',
        'primary' => 'id_ets_am_voucher',
        'fields' => array(
            'id_cart_rule' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_customer' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_cart' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            )
        )
    );

    /**
     * @param $id_customer
     * @param $id_product
     * @return bool
     */
    public static function canAddAffiliatePromoCode($id_product,$id_customer,$check_popup=false)
    {
        $context = Context::getContext();
        if(!isset($context->cart->id))
        {
            if($context->cookie->id_cart)
                $context->cart = new Cart($context->cookie->id_cart);
            else
                return $check_popup;
        }
        $added_cart= false;
        if(!isset($context->currency) && $context->cart->id_currency )
            $context->currency = new Currency($context->cart->id_currency);        
        if($products = $context->cart->getProducts())
        {
            foreach($products as $product)
                if($product['id_product']==$id_product)
                    $added_cart=true;
        }
        if((Configuration::get('ETS_AM_AFF_VOUCHER_SELLER') || $id_customer!= $context->customer->id) &&  ($added_cart || $check_popup))
        {
            if(!Ets_Affiliate::isActive($id_customer)){
                return false;
            }
            if(Configuration::get('ETS_AM_AFF_FIST_PRODUCT'))
            {
                $sql = "SELECT * 
                        FROM `" . _DB_PREFIX_ . "ets_am_voucher` 
                        WHERE id_customer = " . (int) $context->customer->id . " 
                        AND id_product= " . (int) $id_product;
                $result = Db::getInstance()->getRow($sql);
                if ($result) {
                    return false;
                }
            }
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'cart_rule` cr
            INNER JOIN `'._DB_PREFIX_.'cart_cart_rule` ccr ON (cr.id_cart_rule =ccr.id_cart_rule)
            WHERE ccr.id_cart="'.(int)$context->cookie->id_cart.'" AND cr.reduction_product="'.(int)$id_product.'"';
            if(Db::getInstance()->getRow($sql) && !$check_popup)
                return false;
            else
                return true;
        }
        return false;       
    }
    public static function canShowAffiliatePopupPromoCode($id_customer, $id_product, $context = null)
    {
        if (! $context) {
            $context = Context::getContext();
        }
        if ($context->customer->id == $id_customer) {
            return false;
        }
        if(!Ets_Affiliate::isActive($id_customer)){
            return false;
        }
        $sql = "SELECT COUNT(*) as total 
                FROM `" . _DB_PREFIX_ . "ets_am_voucher` 
                WHERE id_customer = " . (int) $context->customer->id . " 
                AND id_product= " . (int) $id_product;
        $result = DB::getInstance()->getValue($sql);
        if ($result) {
            return false;
        }
        return true;
    }
    public static function canConvertVoucher($context = null)
    {
        $total_balance = (float) Ets_Reward_Usage::getTotalBalance($context->customer->id, $context);
        if ($min = Configuration::get('ETS_AM_MIN_BALANCE_REQUIRED_FOR_VOUCHER')) {
            $min = (float) $min;
            if ($total_balance < $min) {
                return false;
            }
        }
        return true;
    }
    public function add($auto_date=true,$null_values=false)
    {
        parent::add($auto_date,$null_values);
    }
}
