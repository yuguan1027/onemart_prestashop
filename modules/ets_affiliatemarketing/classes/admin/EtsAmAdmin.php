<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <contact@etssoft.net>
 * @copyright  2007-2021 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */
 
if (!defined('_PS_VERSION_')) {
    exit;
}

class EtsAmAdmin extends ObjectModel
{
    /**
     * EtsAmAdmin constructor.
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function __construct()
    {
    }

    /**
     * @param $query
     * @param null $context
     * @throws PrestaShopDatabaseException
     */
    public static function searchPrdById($query, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $is17 = false;
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $is17 = true;
        }
        // search product.
        $imageType = $is17 ? ImageType::getFormattedName('cart') : ImageType::getFormatedName('cart');
        if ($pos = strpos($query, ' (ref:')) {
            $query = Tools::substr($query, 0, $pos);
        }

        $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, image_shop.`id_image` id_image, il.`legend`, p.`cache_default_attribute`
        		FROM `' . _DB_PREFIX_ . 'product` p
        		' . Shop::addSqlAssociation('product', 'p') . '
        		LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = ' . (int)$context->language->id . Shop::addSqlRestrictionOnLang('pl') . ')
        		LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop
        			ON (image_shop.`id_product` = p.`id_product` AND image_shop.id_shop=' . (int)$context->shop->id . ')
        		LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$context->language->id . ')
        		WHERE  (pl.id_product LIKE \'%' . pSQL($query) . '%\' OR pl.name LIKE \'%' . pSQL($query) . '%\' OR p.reference LIKE \'%' . pSQL($query) . '%\') GROUP BY p.id_product LIMIT 10';

        $items = Db::getInstance()->executeS($sql);
        if (is_array($items) && $items) {
            $results = array();
            foreach ($items as $item) {
                $results[] = array(
                    'id_product' => (int)($item['id_product']),
                    'id_product_attribute' => 0,
                    'name' => $item['name'],
                    'attribute' => '',
                    'ref' => (!empty($item['reference']) ? $item['reference'] : ''),
                    'image' => str_replace('http://', Tools::getShopProtocol(), $context->link->getImageLink($item['link_rewrite'], $item['id_image'], $imageType)),
                );

            }
            if ($results) {
                foreach ($results as &$item) {
                    echo trim($item['id_product'] . '|' . (int)($item['id_product_attribute']) . '|' . Tools::ucfirst($item['name']) . '|' . $item['attribute'] . '|' . $item['ref'] . '|' . $item['image']) . "\n";
                }
            }
        }
        die;
    }


    /**
     * @param $products
     * @param null $context
     * @return array|bool
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
   /* public static function getBlockProducts($products, $context = null)
    {
        $is17 = false;
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $is17 = true;
        }
        if (!$context) {
            $context = Context::getContext();
        }
        if (!$products)
            return false;
        if (!is_array($products)) {
            $IDs = explode(',', $products);
            $products = array();
            foreach ($IDs as $ID) {
                if ($ID && ($tmpIDs = explode('-', $ID))) {
                    $products[] = array(
                        'id_product' => $tmpIDs[0],
                        'id_product_attribute' => !empty($tmpIDs[1]) ? $tmpIDs[1] : 0,
                    );
                }
            }
        }
        if ($products) {
            $context = Context::getContext();
            $taxCalculationMethod = isset($context->customer->id) && $context->customer->id ? Product::getTaxCalculationMethod((int)$context->customer->id) : PS_TAX_EXC;
            foreach ($products as &$product) {
                $p = new Product($product['id_product'], true, $context->language->id, $context->shop->id);
                $product['link_rewrite'] = $p->link_rewrite;
                $product['price'] = Ets_affiliatemarketing::displayPrice($p->getPrice($taxCalculationMethod == PS_TAX_EXC ? false : true, $product['id_product_attribute'] ? $product['id_product_attribute'] : null));
                if (($oldPrice = $p->getPriceWithoutReduct($taxCalculationMethod == PS_TAX_EXC ? true : false, $product['id_product_attribute'] ? $product['id_product_attribute'] : null)) && $oldPrice != $product['price'])
                    $product['price_without_reduction'] = Tools::convertPrice($oldPrice, null, false);
                if (isset($product['price_without_reduction']) && $product['price_without_reduction'] != $product['price'])
                    $product['specific_prices'] = $p->specificPrice;

                if (isset($product['specific_prices']) && $product['specific_prices'] && $product['specific_prices']['to'] != '0000-00-00 00:00:00')
                    $product['specific_prices_to'] = $product['specific_prices']['to'];

                $product['name'] = $p->name;
                $image = ($product['id_product_attribute'] && ($image = Product::getCombinationImageById($product['id_product_attribute'], $context->language->id))) ? $image : Product::getCover($product['id_product']);

                if ($context->controller->controller_type == 'admin') {
                    $product['add_to_cart_url'] = isset($context->customer) && $is17 ? $context->link->getAddToCartURL((int)$product['id_product'], (int)$product['id_product_attribute']) : '';
                    $product['link'] = $context->link->getProductLink($p);


                    $imageType = $is17 ? ImageType::getFormattedName('home') : ImageType::getFormatedName('home');
                    $product['image'] = $context->link->getImageLink($p->link_rewrite, isset($image['id_image']) ? $image['id_image'] : 0, $imageType);

                    if (Pack::isPack((int)$product['id_product'])) {
                        $product['pack_quantity'] = Pack::getQuantity((int)$product['id_product'], (int)$product['id_product_attribute'], null, $context->cart);
                    }
                    $product['price_tax_exc'] = Product::getPriceStatic((int)$product['id_product'], false, (int)$product['id_product_attribute'], ($taxCalculationMethod == PS_TAX_EXC ? 2 : 6), null, false, true, $p->minimal_quantity);
                    $product['available_for_order'] = $p->available_for_order;
                    if ($product['id_product_attribute']) {
                        $p->id_product_attribute = $product['id_product_attribute'];
                        $product['attributes'] = $p->getAttributeCombinationsById($product['id_product_attribute'], $context->language->id);
                    }
                }
                $product['id_image'] = isset($image['id_image']) ? $image['id_image'] : 0;
                $product['is_available'] = $p->checkQty(1);
                $product['allow_oosp'] = Product::isAvailableWhenOutOfStock($p->out_of_stock);
                $product['show_price'] = $p->show_price;
            }
            unset($context);
        }
        if ($is17 && $products && $context->controller->controller_type != 'admin') {
            return EtsAmAdmin::productsForTemplate($products, $context);
        }

        return $products;
    }*/

    /**
     * @param $products
     * @param null $context
     * @return array
     */
    protected static function productsForTemplate($products, $context = null)
    {
        if (!$products || !is_array($products)) {
            return array();
        }
        if (!$context) {
            $context = Context::getContext();
        }
        $assembler = new ProductAssembler($context);
        $presenterFactory = new ProductPresenterFactory($context);
        $presentationSettings = $presenterFactory->getPresentationSettings();
        $presenter = new PrestaShop\PrestaShop\Core\Product\ProductListingPresenter(
            new PrestaShop\PrestaShop\Adapter\Image\ImageRetriever(
                $context->link
            ),
            $context->link,
            new PrestaShop\PrestaShop\Adapter\Product\PriceFormatter(),
            new PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever(),
            $context->getTranslator()
        );
        $products_for_template = array();
        foreach ($products as $item) {
            $products_for_template[] = $presenter->present(
                $presentationSettings,
                $assembler->assembleProduct($item),
                $context->language
            );
        }
        return $products_for_template;
    }

    /**
     * @param null $prd_id
     * @param null $shop_id
     * @return array|bool|null|object
     */
    public static function getLoyaltySettings($prd_id = null, $shop_id = null)
    {
        $where = ' WHERE 1';
        if ($prd_id) {
            $where .= " AND `id_product` = " . (int)$prd_id;
        }
        if ($shop_id) {
            $where .= " AND `id_shop` = " . (int)$shop_id;
        }
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "ets_am_loy_reward` ".(string)$where;
        return Db::getInstance()->getRow($sql);
    }

    /**
     * @param null $prd_id
     * @param null $shop_id
     * @return array|bool|null|object
     */
    public static function getAffiliateSettings($prd_id = null, $shop_id = null)
    {
        $where = ' WHERE 1';
        if ($prd_id) {
            $where .= " AND `id_product` = " . (int)$prd_id;
        }
        if ($shop_id) {
            $where .= " AND `id_shop` = " . (int)$shop_id;
        }
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "ets_am_aff_reward` ".(string)$where;
        return Db::getInstance()->getRow($sql);
    }

    public static function createOrUpdateSetting($type, $params = array())
    {
        if (!$params || !is_array($params)) {
            return false;
        }
        if (!isset($params['id_product']) || !$params['id_product'] || !isset($params['id_shop']) || !$params['id_shop']) {
            return false;
        }

        $id_product = (int)$params['id_product'];
        $id_shop = (int)$params['id_shop'];

        // unset($params['id_product']);
        // unset($params['id_shop']);

        $table = 'ets_am_aff_reward';
        if ($type == 'loyalty') {
            $table = 'ets_am_loy_reward';
        } elseif ($type == 'affiliate') {
            $table = 'ets_am_aff_reward';
        }
        //Check setting exists or not
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "$table` WHERE id_product = ".(int)$id_product." AND id_shop = ".(int)$id_shop;
        $setting = Db::getInstance()->getRow($sql);

        //Update if exists
        if ($setting) {
            $id_setting = 0;
            $id_col = '';
            if ($table == 'ets_am_aff_reward') {
                $id_setting = (int)$setting['id_ets_am_aff_reward'];
                $id_col = 'id_ets_am_aff_reward';
            } elseif ($table == 'ets_am_loy_reward') {
                $id_setting = (int)$setting['id_ets_am_loy_reward'];
                $id_col = 'id_ets_am_loy_reward';
            }
            $update_data = "";
            $count = 0;
            foreach ($params as $key => $value) {
                if ($count < count($params) - 1) {
                    $update_data .= "$key = '$value',";
                } else {
                    $update_data .= "$key = '$value'";
                }
                $count++;
            }
            if ($update_data) {
                $sql_update = "UPDATE `" . _DB_PREFIX_ . "$table` SET ".(string)$update_data." WHERE ".(string)$id_col." = ".(int)$id_setting;
                return Db::getInstance()->execute($sql_update);
            }
        } else { //Create if not exists

            $create_col = "";
            $create_val = "";
            $count = 0;
            foreach ($params as $key => $value) {
                if ($count < count($params) - 1) {
                    $create_col .= "$key,";
                    $create_val .= "'$value',";
                } else {
                    $create_col .= "$key";
                    $create_val .= "'$value'";
                }
                $count++;
            }
            if ($create_col && $create_val) {
                $sql_create = "INSERT INTO `" . _DB_PREFIX_ . "$table` ($create_col) VALUES($create_val)";
                return Db::getInstance()->execute($sql_create);
            }
        }
        return false;
    }

    public static function saveSponsorLevel()
    {
        $count = 2;
        $quit = false;
        while ($quit == false) {
            if (Configuration::hasKey('ETS_AM_REF_SPONSOR_COST_LEVEL_' . $count)) {
                $ETS_AM_REF_SPONSOR_COST_LEVEL = Tools::getValue('ETS_AM_REF_SPONSOR_COST_LEVEL_' . $count);
                if ($ETS_AM_REF_SPONSOR_COST_LEVEL === false) {
                    Configuration::deleteByName('ETS_AM_REF_SPONSOR_COST_LEVEL_' . $count);
                }
            } else {
                $quit = true;
                break;
            }
            $count++;
        }

        //Add new config
        $level_count = 2;
        $quit = false;
        while ($quit == false) {
            $level_data = Tools::getValue('ETS_AM_REF_SPONSOR_COST_LEVEL_' . $level_count);
            if ($level_data !== false && Validate::isCleanHtml($level_data)) {
                Configuration::updateValue('ETS_AM_REF_SPONSOR_COST_LEVEL_' . $level_count, $level_data);
            } else {
                $quit = true;
                break;
            }
            $level_count++;
        }

        return true;
    }

    public static function getDataApplications()
    {
        $context = Context::getContext();
   
        $type_date_filter = ($type_date_filter = Tools::getValue('type_date_filter', false)) && Validate::isCleanHtml($type_date_filter) ? $type_date_filter : '';
        $date_from_reward = ($date_from_reward = Tools::getValue('date_from_reward', false)) && Validate::isCleanHtml($date_from_reward) ? $date_from_reward : '';
        $date_to_reward = ($date_to_reward = Tools::getValue('date_to_reward', false)) && Validate::isCleanHtml($date_to_reward) ? $date_to_reward : '';
        $status = ($status = Tools::getValue('status', false)) && Validate::isCleanHtml($status) ? $status : '';

        $search = Tools::getValue('search');
        $limit = (int)Tools::getValue('limit', false) ? : 10;
        $page = (int)Tools::getValue('page', false) ? : 1;
        
        $filter_where = "p.id_shop = " . (int)$context->shop->id;
        if ($search && is_array($search) && !empty($search['value']) && Ets_affiliatemarketing::validateArray($search)) {
            $q = $search['value'];
            $filter_where .= " AND p.id_ets_am_participation = '" . pSQL($q) . "' 
                            OR p.status LIKE '%" . pSQL($q) . "%' 
                            OR c.email LIKE '%" . pSQL($q) . "%'";
        }

        if($status!==false && $status !== 'all'){
            $filter_where .= " AND p.status = ".(int)$status;
        }
        if($type_date_filter == 'this_month'){
            $filter_where .= " AND p.datetime_added >= '".date('Y-m-01 00:00:00')."' AND p.datetime_added <= '".date('Y-m-t 23:59:59')."'";
        }
        else if($type_date_filter == 'this_year'){
            $filter_where .= " AND p.datetime_added >= '".date('Y-01-01 00:00:00')."' AND p.datetime_added <= '".date('Y-12-31 23:59:59')."'";
        }
        else if($type_date_filter == 'time_ranger' && $date_from_reward && $date_to_reward){
            $filter_where .= " AND p.datetime_added >= '".date('Y-m-d 00:00:00', strtotime($date_from_reward))."' AND p.datetime_added <= '".date('Y-m-d 23:59:59', strtotime($date_to_reward))."'";
            $filter_where .= " AND p.datetime_added >= '".date('Y-m-d 00:00:00', strtotime($date_from_reward))."' AND p.datetime_added <= '".date('Y-m-d 23:59:59', strtotime($date_to_reward))."'";
        }
        /*else{
            $filter_where .= " AND p.datetime_added >= '".date('Y-m-01 00:00:00')."' AND p.datetime_added <= '".date('Y-m-t 23:59:59')."'";
        }*/

         $total_data = (int)Db::getInstance()->executeS("SELECT COUNT(*) as total 
                FROM `" . _DB_PREFIX_ . "ets_am_participation` p
                LEFT JOIN `" . _DB_PREFIX_ . "customer` c ON p.id_customer = c.id_customer
                WHERE $filter_where")[0]['total'];

        $total_page = ceil($total_data / $limit);
        $offset = ($page - 1) * $limit;

        $sql = "SELECT p.id_ets_am_participation as id, p.datetime_added as date_added, p.status as status, p.program as program,
                c.email as email_customer, c.firstname as firstname, c.lastname as lastname, p.id_customer as id_customer
                FROM `" . _DB_PREFIX_ . "ets_am_participation` p
                LEFT JOIN `" . _DB_PREFIX_ . "customer` c ON p.id_customer = c.id_customer
                WHERE $filter_where
                ORDER BY p.id_ets_am_participation DESC
                LIMIT ".(int)$offset.", ".(int)$limit."
            ";

        $results = DB::getInstance()->executeS($sql);

        foreach ($results as &$result) {
            $actions = array();
            $actions[] = array(
                'label' => Ets_affiliatemarketing::$trans['View'],
                'id' => $result['id'],
                'icon' => 'search',
                'class'=> '',
                'action' => '',
                'href' => $context->link->getAdminLink('AdminModules', true).'&configure=ets_affiliatemarketing&tabActive=applications&id_application='.$result['id'].'&viewapp=1'
            );
            if ($result['status'] == 0) {
                $actions[] = array(
                    'label' => Ets_affiliatemarketing::$trans['Approve'],
                    'id' => $result['id'],
                    'icon' => 'check',
                    'class'=> 'js-btn-action-app',
                    'action' => 'approve'
                );
                $actions[] = array(
                    'label' => Ets_affiliatemarketing::$trans['Decline'],
                    'id' => $result['id'],
                    'icon' => 'close',
                    'class'=> 'js-btn-action-app',
                    'action' => 'decline'
                );
               
            }
            $actions[] = array(
                'label' => Ets_affiliatemarketing::$trans['Delete'],
                'id' => $result['id'],
                'icon' => 'trash',
                'class'=> 'js-btn-action-app',
                'action' => 'delete'
            );
            $result['actions'] = $actions;


            $title_program = '';
            if ($result['program'] == 'loy') {
                $title_program = Ets_affiliatemarketing::$trans['loyalty_program'];
            } else if ($result['program'] == 'aff') {
                $title_program = Ets_affiliatemarketing::$trans['affiliate_program'];
            } else if ($result['program'] == 'ref') {
                $title_program = Ets_affiliatemarketing::$trans['referral_program'];
            }
            $result['program'] = $title_program;
            
        }
        return array(
            'results' => $results,
            'total_page' => $total_page,
            'total_data' => $total_data,
            'current_page' => $page,
            'per_page' => $limit
        );
    }

    public static function actionCustomer($id, $action, $reason = null)
    {
        $trans = Ets_affiliatemarketing::$trans;
        $context = Context::getContext();
        $module = Module::getInstanceByName('ets_affiliatemarketing');
        if ($id) {
            $p = new Ets_Participation($id);
            if(!$p){
                return array(
                    'success' => false,
                    'actions' => array()
                );
            }
            $actions = array();
            $actions[] = array(
                'label' => Ets_affiliatemarketing::$trans['View'],
                'id' => $id,
                'icon' => 'search',
                'class'=> '',
                'action' => '',
                'href' => $context->link->getAdminLink('AdminModules', true).'&configure=ets_affiliatemarketing&tabActive=applications&id_application='.(int)$id.'&viewapp=1'
            );
            $actions[] = array(
                'label' => Ets_affiliatemarketing::$trans['Delete'],
                'id' => $id,
                'icon' => 'trash',
                'class'=> 'js-btn-action-app',
                'action' => 'delete'
            );
            if($action == 'approve'){
                $p = new Ets_Participation($id);
                $p->status = 1;
                $p->update();

                $userExists = Ets_User::getUserByCustomerId($p->id_customer);
                 if ($userExists && $userExists['status'] != -1) {
                    $user = new Ets_User((int)$userExists['id_ets_am_user']);
                    $index = $p->program;
                    $user->{$index} = 1;
                    $user->status = 1;
                    $user->id_shop = $context->shop->id;
                    $user->update();
                } else if(!$userExists) {
                    $user = new Ets_User();
                    $user->id_customer = $p->id_customer;
                    $index = $p->program;
                    $user->{$index} = 1;
                    $user->id_shop = $context->shop->id;
                    $user->status = 1;
                    $user->add();
                }
                //Send mail
                self::sendMailApplicationApprovedWithoutConfig($id, $reason);
                return array(
                    'success' => true,
                    'actions' => $actions
                );
            }
            elseif($action == 'delete'){
                $paticipation = Db::getInstance()->getRow("SELECT * FROM `" . _DB_PREFIX_ . "ets_am_participation` WHERE id_ets_am_participation = ".(int)$id);
           
                if($paticipation){
                     $sql = "DELETE FROM `" . _DB_PREFIX_ . "ets_am_participation` WHERE id_ets_am_participation = ".(int)$id;
                    Db::getInstance()->execute($sql);
                    Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."ets_am_user` SET ".pSQL($paticipation['program'])." = 0 WHERE id_shop = ".(int)$context->shop->id." AND id_customer = ".(int)$paticipation['id_customer']);
                }

                return array(
                    'success' => true,
                    'actions' => $actions
                );
            }
            elseif($action == 'decline'){
                $p->status = -1;
                $p->update();
                $program = '';
                if($p->program == 'loy'){
                    $program = $trans['loyalty_program'];
                }
                elseif($p->program == 'ref'){
                    $program = $trans['referral_program'];
                }
                 elseif($p->program == 'aff'){
                    $program = $trans['affiliate_program'];
                }
                if($user = Ets_User::getUserByCustomerId($p->id_customer)){
                    Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."ets_am_user` SET ".pSQL((string)$p->program)." = -2 WHERE id_shop = ".(int)$context->shop->id." AND id_customer = ".(int)$p->id_customer);
                }
                else{
                    $user = new Ets_User();
                    $user->id_customer = $p->id_customer;
                    $index = $p->program;
                    $user->{$index} = -2;
                    $user->status = 1;
                    $user->id_shop = $context->shop->id;
                    $user->add();
                }
                $customer = new Customer($p->id_customer);
                $data = array(
                    '{title}' => 'Your application declined',
                    '{username}' => $customer->firstname.' '.$customer->lastname,
                    '{email}' => $customer->email,
                    '{reason}' => $reason,
                    '{date_declined}' => date('Y-m-d H:i:s'),
                    '{program}' => $program,
                );
                $subject = array(
                    'translation' => $module->l('Your application was declined','etsamadmin'),
                    'origin'=> 'Your application was declined',
                    'specific'=>'etsamadmin',
                );
                Ets_aff_email::send($customer->id_lang,'application_declined',$subject,$data,$customer->email);
                return array(
                    'success' => true,
                    'actions' => $actions
                );
            }
            
        }

        return array(
            'success' => false,
            'actions' => array()
        );
    }

    public static function sendMailApplicationApproved($id_app, $reason = ''){
        $trans = Ets_affiliatemarketing::$trans;
        $module = Module::getInstanceByName('ets_affiliatemarketing');
        if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_RES_REG')){ //Send mail if enable
            if($id_app){
                $sql = "SELECT pa.*, customer.firstname as firstname, customer.lastname as lastname, customer.email as email,customer.id_lang
                        FROM `" . _DB_PREFIX_ . "ets_am_participation` pa 
                        JOIN `"._DB_PREFIX_."customer` customer ON pa.id_customer = customer.id_customer
                        WHERE pa.id_ets_am_participation = ".(int)$id_app;
                $app = Db::getInstance()->getRow($sql);
                
                if($app){
                    $program = '';
                    if($app['program'] == 'loy'){
                        $program = $trans['loyalty_program'];
                    }
                    elseif($app['program'] == 'ref'){
                        $program = $trans['referral_program'];
                    }elseif($app['program'] == 'aff'){
                        $program = $trans['affiliate_program'];
                    }
                    $data = array(
                        '{title}' => 'Your account is approved to use services of affiliate marketing',
                        '{username}' => $app['firstname'].' '.$app['lastname'],
                        '{email}' => $app['email'],
                        '{status}' => 'Approved',
                        '{date_created}' => $app['datetime_added'],
                        '{reason}' => $reason,
                        '{program}' => $program
                    );
                    $email = $app['email'];
                    $subject = array(
                        'translation' => $module->l('Your application was approved','etsamadmin'),
                        'origin'=> 'Your application was approved',
                        'specific'=>'etsamadmin',
                    );
                    Ets_aff_email::send($app['id_lang'],'application_approved',$subject,$data,trim($email));
                }
            }
            
        }
    }
    public static function sendMailApplicationApprovedWithoutConfig($id_app, $reason = ''){
        
        $trans = Ets_affiliatemarketing::$trans;
        $module = Module::getInstanceByName('ets_affiliatemarketing');
        if($id_app){
            $sql = "SELECT pa.*, customer.firstname as firstname, customer.lastname as lastname, customer.email as email,customer.id_lang
                    FROM `" . _DB_PREFIX_ . "ets_am_participation` pa 
                    JOIN `"._DB_PREFIX_."customer` customer ON pa.id_customer = customer.id_customer
                    WHERE pa.id_ets_am_participation = ".(int)$id_app;
            $app = Db::getInstance()->getRow($sql);
            if($app){
                $program = '';
                if($app['program'] == 'loy'){
                    $program = $trans['loyalty_program'];
                }
                elseif($app['program'] == 'ref'){
                    $program = $trans['referral_program'];
                }elseif($app['program'] == 'aff'){
                    $program = $trans['affiliate_program'];
                }elseif($app['program'] == 'anr'){
                    $program = $trans['referral_and_affiliate_program'];
                }
                $data = array(
                    '{title}' => 'Your account is approved to use services of affiliate marketing',
                    '{username}' => $app['firstname'].' '.$app['lastname'],
                    '{email}' => $app['email'],
                    '{status}' => 'Approved',
                    '{date_created}' => $app['datetime_added'],
                    '{reason}' => $reason,
                    '{program}' => $program,
                );
                $email = $app['email'];
                $subject = array(
                    'translation' => $module->l('Your application was approved','etsamadmin'),
                    'origin'=> 'Your application was approved',
                    'specific'=>'etsamadmin',
                );
                Ets_aff_email::send($app['id_lang'],'application_approved',$subject,$data,trim($email));
            }
        }
    }

    /**
     * @param null $id_user
     * @param null $program
     * @param bool $frontend
     * @return array
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function getRewardHistory($id_user = null, $program = null, $frontend = false, $get_product_name = false)
    {
        $context = Context::getContext();
        $query = array();
        $type_date_filter = ($type_date_filter = Tools::getValue('type_date_filter', false)) && Validate::isCleanHtml($type_date_filter) ? $type_date_filter : '';
        $date_from_reward = ($date_from_reward = Tools::getValue('date_from_reward', false)) && Validate::isCleanHtml($date_from_reward) ? $date_from_reward : '';
        $date_to_reward = ($date_to_reward = Tools::getValue('date_to_reward', false)) && Validate::isCleanHtml($date_to_reward) ? $date_to_reward : '';
        $program = ($p = Tools::getValue('program', $program)) && Validate::isCleanHtml($p) ? $p : '';
        $status = ($status = Tools::getValue('status', false)) && Validate::isCleanHtml($status) ? $status : false;
        if ($status !== false) {
            $query['status'] = (int)$status;
        } else {
            $query['status'] = 'all';
        }
        $query['type_date_filter'] = $type_date_filter;
        $query['date_from_reward'] = $date_from_reward;
        $query['date_to_reward'] = $date_to_reward;
        $query['program'] = $date_to_reward;
        //$search = Tools::getValue('search', false);
        $id_customer = (int)Tools::getValue('id_customer', false);
        $limit = (int)Tools::getValue('limit', false) ? : 30;
        $page = (int)Tools::getValue('page', false) ? : 1;
        $offset = ($page - 1) * $limit;
        $filter_where = " AND reward.`deleted` = 0 AND reward.id_shop = ".(int)$context->shop->id;
        if((int)$id_user){
            $filter_where .= " AND reward.id_customer = ".(int)$id_user;
        }
        if($program && $program !== 'all'){
            if($program != 'reward_used'){
                $filter_where .= " AND reward.program = '" . (string)$program . "'";
            }
            else{
                $filter_where .= " AND reward.type = 'usage'";
            }
        }
        if((int)$id_customer){
            $filter_where .= " AND reward.id_customer = ".(int)$id_customer;
        }

        if($status !== false && $status !== 'all' && $program != 'reward_used'){
            $filter_where .= " AND reward.type = 'reward' AND reward.status = ".(int)$status;
        }
        if($type_date_filter == 'this_month'){
            $filter_where .= " AND reward.datetime_added >= '".date('Y-m-01 00:00:00')."' AND reward.datetime_added <= '".date('Y-m-t 23:59:59')."'";
        }
        else if($type_date_filter == 'this_year'){
            $filter_where .= " AND reward.datetime_added >= '".date('Y-01-01 00:00:00')."' AND reward.datetime_added <= '".date('Y-12-31 23:59:59')."'";
        }
        else if($type_date_filter == 'time_ranger' && $date_from_reward && $date_to_reward){
            $filter_where .= " AND reward.datetime_added >= '".date('Y-m-d 00:00:00', strtotime($date_from_reward))."' AND reward.datetime_added <= '".date('Y-m-d 23:59:59',strtotime($date_to_reward))."'";
        } else{
            /*$max_time = Db::getInstance()->getValue("SELECT MAX(datetime_added) FROM `" . _DB_PREFIX_ . "ets_am_reward` WHERE id_shop = " . (int)$context->shop->id);
            $min_time = Db::getInstance()->getValue("SELECT Min(datetime_added) FROM `" . _DB_PREFIX_ . "ets_am_reward` WHERE id_shop = " . (int)$context->shop->id);
            $start_date = $min_time;
            $end_date = $max_time;
            $filter_where .= " AND reward.datetime_added >= '".(string)$start_date."' AND reward.datetime_added <= '".(string)$end_date."'";*/
        }
        //GROUP BY id_order, id_withdraw, id_voucher        
        $resultTotal = DB::getInstance()->executeS("SELECT COUNT(*) as total
                FROM 
                (SELECT DISTINCT id_ets_am_reward as id, 'reward' as type, program, status,id_customer, amount, id_order,id_shop,deleted,note,datetime_added, expired_date FROM `"._DB_PREFIX_."ets_am_reward`                         UNION ALL
                    SELECT id_ets_am_reward_usage as id ,'usage' as type, type as program, status ,id_customer, SUM(amount) as amount, id_order,id_shop,deleted,note,datetime_added, NULL as expired_date FROM `"._DB_PREFIX_."ets_am_reward_usage` 
                ) reward 
                LEFT JOIN `" . _DB_PREFIX_ . "orders` orders ON reward.id_order = orders.id_order
                LEFT JOIN `" . _DB_PREFIX_ . "order_state` order_state ON orders.current_state = order_state.id_order_state
                LEFT JOIN `" . _DB_PREFIX_ . "order_state_lang` order_state_lang ON order_state.id_order_state = order_state_lang.id_order_state
                WHERE (CASE
                    WHEN order_state_lang.id_lang IS NULL
                    THEN 1
                    ELSE order_state_lang.id_lang = ". (int)Configuration::get('PS_LANG_DEFAULT') ."
                    END
                )" . (string)$filter_where);
        $total_data = (int)$resultTotal[0]['total'];
        $total_page = ceil($total_data / $limit);
        $sql = "SELECT reward.*, order_state_lang.name as order_state, orders.reference as order_reference,
                customer.firstname as firstname, customer.lastname as lastname
                FROM
                (
                    SELECT DISTINCT  r.id_ets_am_reward as id, 'reward' as type, r.program, r.status,id_customer,
                     ".($id_user ? "IF(rp.amount IS NOT NULL AND rp.amount > 0, rp.amount, r.amount)" : "r.amount")." as amount, r.id_order,r.id_shop,deleted,note,r.datetime_added, expired_date,pl.name as product_name,rp.id_product FROM `"._DB_PREFIX_."ets_am_reward` r
                    LEFT JOIN `"._DB_PREFIX_."ets_am_reward_product` rp ON (rp.id_ets_am_reward = r.id_ets_am_reward)
                    LEFT JOIN `"._DB_PREFIX_."product_lang` pl ON (pl.id_product= rp.id_product AND pl.id_lang =".(int)$context->language->id." AND pl.id_shop =".(int)$context->shop->id.")
                    UNION ALL
                    SELECT id_ets_am_reward_usage as id ,'usage' as type,type as program, status ,id_customer, amount as amount, id_order,id_shop,deleted,note,datetime_added, '' as expired_date,'' as product_name,'' as id_product  FROM `"._DB_PREFIX_."ets_am_reward_usage`                 ) reward 
                LEFT JOIN `" . _DB_PREFIX_ . "orders` orders ON reward.id_order = orders.id_order
                LEFT JOIN `" . _DB_PREFIX_ . "order_state` order_state ON orders.current_state = order_state.id_order_state
                LEFT JOIN `" . _DB_PREFIX_ . "order_state_lang` order_state_lang ON order_state.id_order_state = order_state_lang.id_order_state
                LEFT JOIN `"._DB_PREFIX_."customer` customer ON reward.id_customer = customer.id_customer
                WHERE (CASE
                    WHEN order_state_lang.id_lang IS NULL
                    THEN 1
                    ELSE order_state_lang.id_lang = ". (int)Configuration::get('PS_LANG_DEFAULT') ."
                    END
                )" . (string)$filter_where .
            " ORDER BY reward.datetime_added DESC
                 LIMIT ".(int)$offset.",".(int)$limit;
        $results = DB::getInstance()->executeS($sql);
        $trans = Ets_affiliatemarketing::$trans;
        foreach ($results as &$result) {
            $result['query'] = $query;
            $result['id_ets_am_reward'] = $result['type'] == 'reward' ? 'R-'.$result['id'] : 'S-'.$result['id'];
            $id_order = $result['id_order'];
            $actions = array();
            if ($get_product_name) {
                $products = self::getRewardProductByOrderId($id_order, $result['program'], $context);
                $result['products'] = $products;
            }
            $expired = false;
            if($result['expired_date'] && $result['expired_date'] !== '000-00-00 00:00:00'){
                $today = date("Y-m-d H:i:s");  
                $startdate = $result['expired_date'];   
                $offset = strtotime("+1 day");
                $enddate = date($startdate, $offset);    
                $today_date = new DateTime($today);
                $expiry_date = new DateTime($enddate);
                if ($expiry_date < $today_date) {
                    $expired = true;
                }
            }
            if ($result['status'] == -2 || $expired) {
                if($result['type'] == 'reward'){
                    if($result['firstname']){
                         $actions[] = array(
                            'label' => $trans['Approve'],
                            'class' => 'js-approve-reward-item',
                            'id' => $result['id'],
                            'icon' => 'check'
                        );
                    }
                    $actions[] = array(
                        'label' => $trans['Cancel'],
                        'class' => 'js-cancel-reward-item',
                        'id' => $result['id'],
                        'icon' => 'times'
                    );
                }
                
            } else if ($result['status'] == -1) {
                if($result['type'] == 'reward'){
                    if($result['firstname']){
                         $actions[] = array(
                            'label' => $trans['Approve'],
                            'class' => 'js-approve-reward-item',
                            'id' => $result['id'],
                            'icon' => 'check'
                        );
                    }
                }
                else{
                    if($result['firstname']){
                         $actions[] = array(
                            'label' => $trans['Approve'],
                            'class' => 'js-approve-reward-usage-item',
                            'id' => $result['id'],
                            'icon' => 'check'
                        );
                    }
                }
            } else if ($result['status'] == 0) {
                if($result['type'] == 'reward'){
                    if($result['firstname']){
                         $actions[] = array(
                            'label' => $trans['Approve'],
                            'class' => 'js-approve-reward-item',
                            'id' => $result['id'],
                            'icon' => 'check'
                        );
                    }
                    $actions[] = array(
                        'label' => $trans['Cancel'],
                        'class' => 'js-cancel-reward-item',
                        'id' => $result['id'],
                        'icon' => 'times'
                    );
                }
                else{
                    if($result['firstname']){
                         $actions[] = array(
                            'label' => $trans['Approve'],
                            'class' => 'js-approve-reward-usage-item',
                            'id' => $result['id'],
                            'icon' => 'check'
                        );
                    }
                    $actions[] = array(
                        'label' => $trans['Cancel'],
                        'class' => 'js-cancel-reward-usage-item',
                        'id' => $result['id'],
                        'icon' => 'times'
                    );
                }
               
            } else {
                if($result['type'] == 'reward'){
                    $actions[] = array(
                        'label' => $trans['Cancel'],
                        'class' => 'js-cancel-reward-item',
                        'id' => $result['id'],
                        'icon' => 'times'
                    );
                }
                else{
                    $actions[] = array(
                        'label' => $trans['Cancel'],
                        'class' => 'js-cancel-reward-usage-item',
                        'id' => $result['id'],
                        'icon' => 'times'
                    );
                }
            }
            if($result['type'] == 'reward'){
                 $actions[] = array(
                    'label' => $trans['Delete'],
                    'class' => 'js-delete-reward-item',
                    'id' => $result['id'],
                    'icon' => 'trash'
                );
             }
            else{
                 $actions[] = array(
                    'label' => $trans['Delete'],
                    'class' => 'js-delete-reward-usage-item',
                    'id' => $result['id'],
                    'icon' => 'trash'
                );
             }
            if($result['program'] == 'ref'){
                $result['program'] = $trans['referral_program'];
            }
            elseif($result['program'] == 'aff'){
                $result['program'] = $trans['affiliate_program'];
            }
            elseif($result['program'] == 'loy'){
                $result['program'] = $trans['loyalty_program'];
            }
            elseif($result['program'] == 'mnu'){
                $result['program'] = '--';
            }
            elseif($result['program'] == 'anr'){
                $result['program'] = $trans['referral_and_affiliate_program'];
            }
            $result['order_reference'] = $result['order_reference'] ? $result['order_reference'] : '-';
            $result['order_state'] = $result['order_state'] ? $result['order_state'] : '-';
            $result['actions'] = $actions;
            if($result['type'] == 'usage'){
                $result['amount'] =  '-'.($frontend ? Ets_AM::displayReward($result['amount'], true) : Ets_AM::displayRewardAdmin($result['amount']));
            }
            else{
                $result['amount'] =  $frontend ? Ets_AM::displayReward($result['amount'], true) : Ets_AM::displayRewardAdmin($result['amount']);
            } 
            if($result['product_name'] && $result['id_product']){
                $result['product_name'] = Module::getInstanceByName('ets_affiliatemarketing')->displayHtml($result['product_name'],'a','','',$context->link->getProductLink($result['id_product']));
            }
            else
                $result['product_name']='--';
        }
        return array(
            'results' => $results,
            'total_page' => (int)$total_page,
            'total_data' => $total_data,
            'current_page' => $page,
            'per_page' => $limit,
            'query' => $query
        );
    }

    public static function getRewardProductByOrderId($id_order, $program, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $sql = "SELECT  rp.id_product, pl.name FROM `" . _DB_PREFIX_ . "ets_am_reward_product` rp 
        INNER JOIN `" . _DB_PREFIX_ . "product_lang` pl ON rp.id_product = pl.id_product 
        WHERE pl.id_lang = ". (int)$context->language->id ." AND rp.id_order = " . (int) $id_order . " AND `program` = '" . pSQL($program) . "' AND pl.id_shop = " . (int)$context->shop->id;
        $results = Db::getInstance()->executeS($sql);
        $response = array();
        if ($results) {
            foreach ($results as $result) {
                $p_link = Ets_Affiliate::generateAffiliateLinkForProduct(new Product($result['id_product']), $context, false);
                $response[] = array(
                    'link' => $p_link,
                    'name' => $result['name']
                );
            }
        }
        return $response;
    }

    public static function getTotalPendingApplications(){
        $sql = "SELECT COUNT(*) as total FROM `"._DB_PREFIX_."ets_am_participation` WHERE status = 0";
        $total = (int)Db::getInstance()->getValue($sql);
        return $total;
    }

    public static function validateDataConfig($tab){
        $trans = Ets_affiliatemarketing::$trans;
        $errors = array();
        if($tab == 'general_settings'){
            $ETS_AM_REWARD_DISPLAY = Tools::getValue('ETS_AM_REWARD_DISPLAY');
            if($ETS_AM_REWARD_DISPLAY == 'point'){
                $rewardUnit = Tools::getValue('ETS_AM_REWARD_UNIT_LABEL_'.(int)Configuration::get('PS_LANG_DEFAULT'), false);
                if(!$rewardUnit || !Validate::isCleanHtml($rewardUnit)){
                    $errors[] = $trans['reward_unit_label_required'];
                }
                $conver = Tools::getValue('ETS_AM_CONVERSION', false);
                if(!$conver || !Validate::isCleanHtml($conver)){
                    $errors[] = $trans['coversion_rate_required'];
                }
            }
        }
        elseif($tab == 'general_email'){
            $ETS_AM_ENABLED_EMAIL_CONFIRM_REG = (int)Tools::getValue('ETS_AM_ENABLED_EMAIL_CONFIRM_REG', false);
            $ETS_AM_ENABLED_EMAIL_RES_REG = (int)Tools::getValue('ETS_AM_ENABLED_EMAIL_RES_REG', false);
            if($ETS_AM_ENABLED_EMAIL_CONFIRM_REG || $ETS_AM_ENABLED_EMAIL_RES_REG){
                $emailConfirm = Tools::getValue('ETS_AM_EMAILS_CONFIRM',false);
                if(!$emailConfirm || !Validate::isCleanHtml($emailConfirm)){
                    $errors[] =  $trans['email_receive_required'];
                }
            }

        }
        elseif($tab == 'loyalty_conditions'){
            $ETS_AM_LOYALTY_ENABLED = (int)Tools::getValue('ETS_AM_LOYALTY_ENABLED', false); 
            if($ETS_AM_LOYALTY_ENABLED){
                $ETS_AM_LOYALTY_TIME = Tools::getValue('ETS_AM_LOYALTY_TIME', false);
                if($ETS_AM_LOYALTY_TIME== 'specific'){
                    $loyTimeFrom = Tools::getValue('ETS_AM_LOYALTY_TIME_FROM', false);
                    $loyTimeTo = Tools::getValue('ETS_AM_LOYALTY_TIME_TO', false);
                    if(!$loyTimeFrom || !Validate::isCleanHtml($loyTimeFrom) || !$loyTimeTo || !Validate::isCleanHtml($loyTimeTo)){
                        $errors[] = $trans['specific_time_required'];
                    }
                }
                $ETS_AM_LOY_CAT_TYPE = Tools::getValue('ETS_AM_LOY_CAT_TYPE');
                if($ETS_AM_LOY_CAT_TYPE == 'SPECIFIC'){
                    $loyCate = Tools::getValue('ETS_AM_LOYALTY_CATEGORIES', false);
                    if(!$loyCate){
                        $errors[] = $trans['categories_required'];
                    }
                    elseif($loyCate && !Ets_affiliatemarketing::validateArray($loyCate))
                        $errors[] = $trans['categories_valid'];
                }
            }
        }
        elseif ($tab == 'rs_program_voucher'){
            $ETS_AM_REF_OFFER_VOUCHER = (int)Tools::getValue('ETS_AM_REF_OFFER_VOUCHER', false);
            if($ETS_AM_REF_OFFER_VOUCHER){
                $ETS_AM_REF_VOUCHER_TYPE = Tools::getValue('ETS_AM_REF_VOUCHER_TYPE');
                if($ETS_AM_REF_VOUCHER_TYPE == 'FIXED'){
                    /*if(!Tools::getValue('ETS_AM_REF_VOUCHER_CODE', false)){
                        $errors[] = 'Voucher code is required';
                    }*/
                }
                elseif($ETS_AM_REF_VOUCHER_TYPE == 'DYNAMIC'){
                    $ETS_AM_REF_APPLY_DISCOUNT = Tools::getValue('ETS_AM_REF_APPLY_DISCOUNT');
                    if($ETS_AM_REF_APPLY_DISCOUNT == 'PERCENT'){
                        $ETS_AM_REF_REDUCTION_PERCENT = (float)Tools::getValue('ETS_AM_REF_REDUCTION_PERCENT', false);
                        $ETS_AM_REF_APPLY_DISCOUNT_IN = (float)Tools::getValue('ETS_AM_REF_APPLY_DISCOUNT_IN', false);
                        if(!$ETS_AM_REF_REDUCTION_PERCENT){
                            $errors[] = $trans['discount_percent_required'];
                        }
                        elseif(!$ETS_AM_REF_APPLY_DISCOUNT_IN){
                            $errors[] = $trans['discount_availability_required'];
                        }
                    }
                    elseif($ETS_AM_REF_APPLY_DISCOUNT == 'AMOUNT'){
                        $ETS_AM_REF_REDUCTION_AMOUNT = (float)Tools::getValue('ETS_AM_REF_REDUCTION_AMOUNT', false);
                        $ETS_AM_REF_APPLY_DISCOUNT_IN = (float)Tools::getValue('ETS_AM_REF_APPLY_DISCOUNT_IN', false);
                        if(!$ETS_AM_REF_REDUCTION_AMOUNT){
                            $errors[] = $trans['amount_required'];
                        }
                        elseif(!$ETS_AM_REF_APPLY_DISCOUNT_IN){
                            $errors[] = $trans['discount_availability_required'];
                        }
                    }
                }
            }
            $ETS_AM_SELL_OFFER_VOUCHER = (int)Tools::getValue('ETS_AM_SELL_OFFER_VOUCHER', false);
            if($ETS_AM_SELL_OFFER_VOUCHER){
                $ETS_AM_SELL_APPLY_DISCOUNT = Tools::getValue('ETS_AM_SELL_APPLY_DISCOUNT');
                if($ETS_AM_SELL_APPLY_DISCOUNT == 'PERCENT'){
                    $ETS_AM_SELL_REDUCTION_PERCENT = (float)Tools::getValue('ETS_AM_SELL_REDUCTION_PERCENT', false);
                    $ETS_AM_SELL_APPLY_DISCOUNT_IN = (float)Tools::getValue('ETS_AM_SELL_APPLY_DISCOUNT_IN', false);
                    if(!$ETS_AM_SELL_REDUCTION_PERCENT){
                        $errors[] = $trans['discount_percent_required'];
                    }
                    elseif(!$ETS_AM_SELL_APPLY_DISCOUNT_IN){
                        $errors[] = $trans['discount_availability_required'];
                    }
                }
                elseif($ETS_AM_SELL_APPLY_DISCOUNT == 'AMOUNT'){
                    $ETS_AM_SELL_REDUCTION_AMOUNT = (float)Tools::getValue('ETS_AM_SELL_REDUCTION_AMOUNT', false);
                    $ETS_AM_SELL_APPLY_DISCOUNT_IN = (float)Tools::getValue('ETS_AM_SELL_APPLY_DISCOUNT_IN', false);
                    if(!$ETS_AM_SELL_REDUCTION_AMOUNT){
                        $errors[] = $trans['amount_required'];
                    }
                    elseif(!$ETS_AM_SELL_APPLY_DISCOUNT_IN){
                        $errors[] = $trans['discount_availability_required'];
                    }
                }
                $ETS_AM_SELL_QUANTITY = Tools::getValue('ETS_AM_SELL_QUANTITY');
                if(!$errors && $ETS_AM_SELL_QUANTITY=='')
                    $errors[] = $trans['voucher_sell_quantity_require'];
                elseif(!Validate::isInt($ETS_AM_SELL_QUANTITY) || $ETS_AM_SELL_QUANTITY <=0)
                    $errors[] = $trans['voucher_sell_quantity_vaild'];
            }
        }
        elseif($tab == 'affiliate_conditions'){
            $ETS_AM_AFF_ENABLED = (int)Tools::getValue('ETS_AM_AFF_ENABLED', false);
            if($ETS_AM_AFF_ENABLED){
                $ETS_AM_AFF_CAT_TYPE = Tools::getValue('ETS_AM_AFF_CAT_TYPE');
                if($ETS_AM_AFF_CAT_TYPE == 'SPECIFIC'){
                    $ETS_AM_AFF_CATEGORIES = Tools::getValue('ETS_AM_AFF_CATEGORIES');
                    if(!$ETS_AM_AFF_CATEGORIES){
                        $errors[] = $trans['categories_required'];
                    }elseif($ETS_AM_AFF_CATEGORIES && !Ets_affiliatemarketing::validateArray($ETS_AM_AFF_CATEGORIES))
                        $errors[] = $trans['categories_valid'];
                }
            }
        }
        elseif($tab == 'affiliate_reward_caculation'){
            $ETS_AM_AFF_HOW_TO_CALCULATE = Tools::getValue('ETS_AM_AFF_HOW_TO_CALCULATE');
            if($ETS_AM_AFF_HOW_TO_CALCULATE == 'PERCENT'){
                $ETS_AM_AFF_DEFAULT_PERCENTAGE = (float)Tools::getValue('ETS_AM_AFF_DEFAULT_PERCENTAGE', false);
                if(!$ETS_AM_AFF_DEFAULT_PERCENTAGE){
                    $errors[] = $trans['percentage_required'];
                }
            }
            elseif($ETS_AM_AFF_HOW_TO_CALCULATE == 'FIXED'){
                $ETS_AM_AFF_DEFAULT_FIXED_AMOUNT = (float)Tools::getValue('ETS_AM_AFF_DEFAULT_FIXED_AMOUNT', false);
                if(!$ETS_AM_AFF_DEFAULT_FIXED_AMOUNT){
                    $errors[] = $trans['amount_fixed_required'];
                }
            }
        }
        elseif($tab == 'affiliate_voucher'){
            $ETS_AM_AFF_OFFER_VOUCHER = (int)Tools::getValue('ETS_AM_AFF_OFFER_VOUCHER', false);
            if($ETS_AM_AFF_OFFER_VOUCHER){
                $ETS_AM_AFF_VOUCHER_TYPE = Tools::getValue('ETS_AM_AFF_VOUCHER_TYPE');
                if($ETS_AM_AFF_VOUCHER_TYPE == 'FIXED'){
                    /*if(!Tools::getValue('ETS_AM_AFF_VOUCHER_CODE', false)){
                        $errors[] = 'Voucher code is required';
                    }*/
                }
                elseif($ETS_AM_AFF_VOUCHER_TYPE == 'DYNAMIC'){
                    $ETS_AM_AFF_APPLY_DISCOUNT = Tools::getValue('ETS_AM_AFF_APPLY_DISCOUNT');
                    if($ETS_AM_AFF_APPLY_DISCOUNT == 'PERCENT'){    
                        $ETS_AM_AFF_REDUCTION_PERCENT = (float)Tools::getValue('ETS_AM_AFF_REDUCTION_PERCENT', false);
                        $ETS_AM_AFF_APPLY_DISCOUNT_IN = (float)Tools::getValue('ETS_AM_AFF_APPLY_DISCOUNT_IN', false);
                        if(!$ETS_AM_AFF_REDUCTION_PERCENT){
                            $errors[] = $trans['discount_percent_required'];
                        }
                        elseif(!$ETS_AM_AFF_APPLY_DISCOUNT_IN){
                            $errors[] = $trans['discount_availability_required'];
                        }
                    }
                    elseif($ETS_AM_AFF_APPLY_DISCOUNT == 'AMOUNT'){
                        $ETS_AM_AFF_REDUCTION_AMOUNT = (float)Tools::getValue('ETS_AM_AFF_REDUCTION_AMOUNT', false);
                        if(!$ETS_AM_AFF_REDUCTION_AMOUNT){
                            $errors[] = $trans['amount_required'];
                        }
                    }
                }
            }
        }
        return $errors;
    }

    public static function getListPaymentMethods(){
        $context = Context::getContext();
        $id_shop = (int)$context->shop->id;
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $sql = "SELECT pm.* FROM `"._DB_PREFIX_."ets_am_payment_method` pm WHERE pm.id_shop = $id_shop AND pm.`deleted` = 0
            ORDER BY pm.sort ASC";
        $results = Db::getInstance()->executeS($sql);
        $pml = Db::getInstance()->executeS("SELECT id_payment_method, title, id_lang FROM `"._DB_PREFIX_."ets_am_payment_method_lang` pml");
        foreach ($results as &$result) {
            $result['title'] = '';
            if($pml){
                foreach ($pml as $p) {
                    if($p['id_payment_method'] == $result['id_ets_am_payment_method']){
                        if($p['title']){
                            $result['title'] = $p['title'];
                            if($p['id_lang'] == $default_lang){
                                break;
                            }
                        }
                        
                    }
                }
            }
            $result['fee_fixed'] = Ets_affiliatemarketing::displayPrice($result['fee_fixed'], (int)Configuration::get('PS_CURRENCY_DEFAULT'));
            $result['fee_percent'] = (float)$result['fee_percent'];
        }
        return $results;
    }

    public static function getPaymentMethod($id){
        
        $sql = "SELECT * FROM `"._DB_PREFIX_."ets_am_payment_method` WHERE id_ets_am_payment_method = $id";
        $payment_method = Db::getInstance()->getRow($sql);
        $payment_method['fee_percent'] = (float)$payment_method['fee_percent'];
        $payment_method['fee_fixed'] = (float)$payment_method['fee_fixed'];
        if($payment_method){
            $sqlLang = "SELECT * FROM `"._DB_PREFIX_."ets_am_payment_method_lang` WHERE id_payment_method = $id";
            $payment_method_langs = Db::getInstance()->executeS($sqlLang);
            $payment_method['langs'] = array();
            foreach ($payment_method_langs as $pml) {
                $payment_method['langs'][$pml['id_lang']] = array(
                    'id' => $pml['id_ets_am_payment_method_lang'],
                    'title' => $pml['title'],
                    'description' => $pml['description'],
                    'note' => $pml['note'],
                    'id_lang' => $pml['id_lang'],
                );
            } 
        }
        return $payment_method;
    }

    public static function getListPaymentMethodField($id_pm, $id_lang = null){
       
        $filter_where = '';
        if($id_lang){
            $filter_where .= "AND pmfl.id_lang = $id_lang";
        }

        $sql = "SELECT pmf.*, pmfl.title, pmfl.description, pmfl.id_lang FROM (
            SELECT * FROM `"._DB_PREFIX_."ets_am_payment_method_field` WHERE id_payment_method = $id_pm AND `deleted` = 0
        ) pmf
        JOIN `"._DB_PREFIX_."ets_am_payment_method_field_lang` pmfl ON pmf.id_ets_am_payment_method_field = pmfl.id_payment_method_field
        WHERE 1 $filter_where
        ORDER BY pmf.sort ASC";

        $payment_method_fields = Db::getInstance()->executeS($sql);
        if(!$id_lang && $payment_method_fields){
            $results = array();
            foreach ($payment_method_fields as $field) {
                $results[$field['id_ets_am_payment_method_field']]['id'] = $field['id_ets_am_payment_method_field'];
                $results[$field['id_ets_am_payment_method_field']]['type'] = $field['type'];
                $results[$field['id_ets_am_payment_method_field']]['enable'] = $field['enable'];
                $results[$field['id_ets_am_payment_method_field']]['description'][$field['id_lang']] = $field['description'];
                $results[$field['id_ets_am_payment_method_field']]['required'] = $field['required'];
                $results[$field['id_ets_am_payment_method_field']]['title'][$field['id_lang']] = $field['title'];
            }
            return $results;
        }
        return $payment_method_fields;
    }

    public static function getPaymentMethodField($id_pmf){
        $sql = "SELECT * FROM `"._DB_PREFIX_."ets_am_payment_method_field` WHERE id_ets_am_payment_method_field = $id_pmf";
        $pmf = Db::getInstance()->getRow($sql);
        if($pmf){
             $sqlLang = "SELECT * FROM `"._DB_PREFIX_."ets_am_payment_method_field_lang` WHERE id_payment_method_field = $id_pmf";
            $pmfl = Db::getInstance()->executeS($sqlLang);
            foreach ($pmfl as  $item) {
                $pmf['langs'][$item['id_lang']] = array(
                    'id' => $item['id_ets_am_payment_method_field_lang'],
                    'title' => $item['title'],
                    'id_lang' => $item['id_lang'],
                );
            }
        }

        return $pmf;
    }

    public static function updatePaymentMethod($id_pm){

        if($id_pm){
            $pm_title = Tools::getValue('payment_method_name', array());
            if(!Ets_affiliatemarketing::validateArray($pm_title))
                $pm_title =array();
            $pm_fee_type = Tools::getValue('payment_method_fee_type', null);
            if($pm_fee_type!='FIXED' && $pm_fee_type!='PERCENT' && $pm_fee_type!='NO_FEE')
                $pm_fee_type='NO_FEE';
            $pm_fee_fixed = (float)Tools::getValue('payment_method_fee_fixed', null);
            $pm_fee_percent = (float)Tools::getValue('payment_method_fee_percent', null);
            $pm_enable = (int)Tools::getValue('payment_method_enabled', null);
            $pm_estimated = (int)Tools::getValue('payment_method_estimated', false) ? : null;
            $pm_desc = Tools::getValue('payment_method_desc', array());
            if(!Ets_affiliatemarketing::validateArray($pm_desc))
                $pm_desc =array();
            $pm_fields = Tools::getValue('payment_method_field', array());
            if(!Ets_affiliatemarketing::validateArray($pm_fields))
                $pm_fields =array();
            $pm_note = Tools::getValue('payment_method_note', array());
            if(!Ets_affiliatemarketing::validateArray($pm_note))
                $pm_note =array();
            $languages = Language::getLanguages(false);
            //update
            $pm = new Ets_PaymentMethod($id_pm);
            $pm->fee_type = $pm_fee_type;
            $pm->fee_fixed = $pm_fee_fixed;
            $pm->fee_percent = $pm_fee_percent;
            $pm->estimated_processing_time = $pm_estimated;
            $pm->enable = $pm_enable;
            $pm->update();

            $sql = "";

            foreach ($languages as $lang) {
                $id_lang = (int)$lang['id_lang'];
                $exists = Db::getInstance()->getRow("SELECT * FROM `"._DB_PREFIX_."ets_am_payment_method_lang` WHERE id_payment_method ='".(int)$id_pm."' AND id_lang = $id_lang");
                if($exists){
                    $sql .= "UPDATE `"._DB_PREFIX_."ets_am_payment_method_lang` SET title = '".pSQL(isset($pm_title[$id_lang]) ? $pm_title[$id_lang] : '')."', description = '".pSQL(isset($pm_desc[$id_lang]) ? $pm_desc[$id_lang] : '' )."', note = '".pSQL(isset($pm_note[$id_lang]) ? $pm_note[$id_lang] : '')."' WHERE id_payment_method = $id_pm AND id_lang = $id_lang;";
                }
                else{
                    $sql .= "INSERT INTO `"._DB_PREFIX_."ets_am_payment_method_lang` (id_payment_method, title, description, note,id_lang) VALUES ($id_pm, '".pSQL(isset($pm_title[$id_lang]) ? $pm_title[$id_lang] : '')."', '".pSQL(isset($pm_desc[$id_lang]) ? $pm_desc[$id_lang] : '')."', '".pSQL(isset($pm_note[$id_lang]) ? $pm_note[$id_lang] : '')."', $id_lang);";
                }  
            }
            

            if($sql){
                Db::getInstance()->execute($sql);
            }

            if($pm_fields){
                $sql_field = "";
                $ids_live = array(0);
                foreach ($pm_fields as $key => $field){
                    if(isset($field['id']) && $field['id']){
                        $ids_live[] = (int)$field['id'];
                    }
                }

                //Set deleted for item deleted
                Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."ets_am_payment_method_field` SET `deleted` = 1 WHERE id_ets_am_payment_method_field NOT IN (".implode(',', $ids_live).") AND id_payment_method = ".(int)$id_pm);

                //Update data
                
                foreach ($pm_fields as $key => $field) {
                    if(isset($field['id']) && $field['id']){
                        if($field['type']){
                            $sql_field .= " UPDATE `"._DB_PREFIX_."ets_am_payment_method_field` SET type = '".(string)$field['type']."', enable = ".(int)$field['enable'].", required = ".(int)$field['required']." WHERE id_ets_am_payment_method_field = ".(int)$field['id'].";";
                        }
                        $default_title = '';
                        foreach ($languages as $lang) {
                            $id_lang = $lang['id_lang'];
                            if(Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ets_am_payment_method_field_lang` WHERE id_payment_method_field ="'.(int)$field['id'].'" AND id_lang="'.(int)$id_lang.'"'))
                                $sql_field .= " UPDATE `"._DB_PREFIX_."ets_am_payment_method_field_lang` SET title = '".pSQL(isset($field['title'][$id_lang]) ? $field['title'][$id_lang] : '')."', description = '".pSQL(isset($field['description'][$id_lang]) ? $field['description'][$id_lang] : '')."' WHERE id_payment_method_field = ".(int)$field['id']." AND id_lang = ".(int)$id_lang.";";
                            else
                                $sql_field .= " INSERT INTO `"._DB_PREFIX_."ets_am_payment_method_field_lang` (id_payment_method_field, id_lang, title, description) VALUES(".(int)$field['id'].", ".(int)$id_lang.", '".pSQL(isset($field['title'][$id_lang]) && $field['title'][$id_lang] ? $field['title'][$id_lang] : $default_title)."', '".pSQL(isset($field['description'][$id_lang]) ? $field['description'][$id_lang] : '')."');";
                        }
                        
                    }
                    else{
                        $sql_field .= "INSERT INTO `"._DB_PREFIX_."ets_am_payment_method_field` (id_payment_method, type, enable, required) VALUES ($id_pm, '".(string)$field['type']."', ".(int)$field['enable'].", ".(int)$field['required']."); SET @id_pmf_".(string)$key." = LAST_INSERT_ID();";
                        $default_title = '';
                        foreach ($field['title'] as $ft) {
                            if($ft){
                                $default_title = $ft;
                                break;
                            }
                        }
                        foreach ($languages as $lang) {
                            $id_lang = $lang['id_lang'];
                            $sql_field .= " INSERT INTO `"._DB_PREFIX_."ets_am_payment_method_field_lang` (id_payment_method_field, id_lang, title, description) VALUES(@id_pmf_".(string)$key.", ".(int)$id_lang.", '".pSQL(isset($field['title'][$id_lang]) && $field['title'][$id_lang] ? $field['title'][$id_lang] : $default_title)."', '".pSQL(isset($field['description'][$id_lang]) ? $field['description'][$id_lang] : '')."');";
                        }
                    }
                }

                if($sql_field){
                    Db::getInstance()->execute($sql_field);
                }
            }
        }

        return true;
    }

    public static function createPaymentMethod(){
        $pm_title = Tools::getValue('payment_method_name', array());
        if(!Ets_affiliatemarketing::validateArray($pm_title))
            $pm_title = array();
        $pm_fee_type = Tools::getValue('payment_method_fee_type', null);
        if($pm_fee_type!='FIXED' && $pm_fee_type!='PERCENT' && $pm_fee_type!='NO_FEE')
            $pm_fee_type='NO_FEE';
        $pm_fee_fixed = (float)Tools::getValue('payment_method_fee_fixed', null) ? : null;
        $pm_fee_percent = (float)Tools::getValue('payment_method_fee_percent', null) ? : null;
        $pm_enable = (int)Tools::getValue('payment_method_enabled', null) ? : null;
        $pm_estimated = (int)Tools::getValue('payment_method_estimated', null) ? : null;
        $pm_desc = Tools::getValue('payment_method_desc', array());
        if(!Ets_affiliatemarketing::validateArray($pm_desc))
            $pm_desc = array();
        $pm_note = Tools::getValue('payment_method_note', array());
        if(!Ets_affiliatemarketing::validateArray($pm_note))
            $pm_note = array();
        //update
        $max_sort = (int)Db::getInstance()->getValue("SELECT MAX(sort) as max_sort FROM "._DB_PREFIX_."ets_am_payment_method");
        $pm = new Ets_PaymentMethod();
        $pm->fee_type = $pm_fee_type;
        $pm->fee_fixed = $pm_fee_fixed;
        $pm->fee_percent = $pm_fee_percent;
        $pm->estimated_processing_time = $pm_estimated;
        $pm->enable = $pm_enable;
        $pm->sort = $max_sort+1;
        $pm->id_shop = Context::getContext()->shop->id;
        $pm->add();

        $id_pm = $pm->id;
        $sql = "";
        $default_title = null;
        foreach ($pm_title as $pt) {
            if($pt){
                $default_title = $pt;
                break;
            }
        }
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $desc = isset($pm_desc[$lang['id_lang']]) && $pm_desc[$lang['id_lang']] ? $pm_desc[$lang['id_lang']] : null;
            $title = isset($pm_title[$lang['id_lang']]) && $pm_title[$lang['id_lang']] ? $pm_title[$lang['id_lang']] : $default_title;
            $note = isset($pm_note[$lang['id_lang']]) && $pm_note[$lang['id_lang']] ? $pm_note[$lang['id_lang']] : null;
            $sql .= "INSERT INTO `"._DB_PREFIX_."ets_am_payment_method_lang` (id_payment_method, title, description, note, id_lang) VALUES ($id_pm, '".pSQL($title)."', '".pSQL($desc)."', '".pSQL($note)."',".(int)$lang['id_lang'].");";
        }

        if($sql){
            Db::getInstance()->execute($sql);
        }

        return $id_pm;
    }

    public static function deletePaymentMethod($id_pm){
        if($id_pm){
        
            $pm = new Ets_PaymentMethod($id_pm);
            $pm->deleted = 1;
            $pm->update();
        }

        return true;
    }

    public static function updateSortPaymentMethod($data){
        if($data){
            $sql = "";
            foreach ($data as $key => $item) {
                $index = $key + 1;
                $item = (int)$item;
                $sql .= " UPDATE `"._DB_PREFIX_."ets_am_payment_method` SET sort = $index WHERE id_ets_am_payment_method = $item;";
            }

            if($sql){
                Db::getInstance()->execute($sql);
                return true;
            }
        }
        return false;
    }

    public static function updateSortPaymentMethodfield($data){
        if($data){
            $sql = "";
            foreach ($data as $key => $item) {
                $index = $key + 1;
                $item = (int)$item;
                $sql .= " UPDATE `"._DB_PREFIX_."ets_am_payment_method_field` SET sort = $index WHERE id_ets_am_payment_method_field = $item;";
            }

            if($sql){
                Db::getInstance()->execute($sql);
                return true;
            }
        }
        return false;
    }

    public static function actionReward($id, $type = null){
        if((int)$id && $type){
            $trans = Ets_affiliatemarketing::$trans;
            $reward = new Ets_AM($id);
            $actions = array();
            if(!$reward){
                return array(
                    'success' => false,
                    'actions' => array()
                );
            }
            $customer = new Customer($reward->id_customer);
            if($type == 'approve'){
                $reward->status = 1;
                $reward->last_modified = date('Y-m-d H:i:s');
                $reward->datetime_validated = date('Y-m-d H:i:s');
                $reward->datetime_canceled = null;
                $reward->expired_date = null;
                $reward->update();

                $actions[] = array(
                    'label' => $trans['Cancel'],
                    'class' => 'js-cancel-reward-item',
                    'id' => $id,
                    'icon' => 'times'
                );
                $actions[] = array(
                    'label' => $trans['Delete'],
                    'class' => 'js-delete-reward-item',
                    'id' => $id,
                    'icon' => 'trash'
                );

                if($reward->program == 'loy'){
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RVOC')){
                        Ets_Loyalty::sendEmailToCustomerWhenRewardValidated(Mail::l('Your reward was approved',$customer->id_lang), $reward);
                    }
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RVOC')){
                        Ets_Loyalty::sendEmailToAdminWhenRewardValidated(Mail::l('A reward was approved'), $reward);
                    }
                }
                if($reward->program == 'ref'){
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RVOC')){
                        Ets_Sponsor::sendMailRewardValidated(null, $id, 'reward_validated', EAM_AM_REF_REWARD);
                    }
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RVOC')){
                        Ets_Sponsor::sendMailAdminRewardValidated(null, $id, 'reward_validated_admin', EAM_AM_REF_REWARD);
                    }
                }
                if($reward->program == 'aff') {
                    if ((int)Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RVOC')) {
                        Ets_Affiliate::senEmailWhenAffiliateRewardValidated($trans['reward_validated'], $reward);
                    }
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RVOC')){
                        Ets_Affiliate::senEmailWhenAffiliateRewardValidated($trans['a_reward_validated'], $reward, true);
                    }
                }
                return array(
                    'success' => true,
                    'actions' => $actions,
                    'user' => EtsAmAdmin::getUserInfo($reward->id_customer),
                );
            }
            elseif($type == 'delete'){
                $reward->deleted = 1;
                $reward->last_modified = date('Y-m-d H:i:s');
                $reward->update();
                return array(
                    'success' => true,
                    'actions' => $actions ,
                    'user' => EtsAmAdmin::getUserInfo($reward->id_customer),
                );
            }
            elseif($type == 'cancel'){
                $reward->status = -1;
                $reward->last_modified = date('Y-m-d H:i:s');
                $reward->datetime_canceled = date('Y-m-d H:i:s');
                $reward->expired_date = null;
                $reward->update();
                if($customer){
                    $actions[] = array(
                        'label' => $trans['Approve'],
                        'class' => 'js-approve-reward-item',
                        'id' => $id,
                        'icon' => 'check'
                    );
                }
                $actions[] = array(
                    'label' => $trans['Delete'],
                    'class' => 'js-delete-reward-item',
                    'id' => $id,
                    'icon' => 'trash'
                );

                if($reward->program == 'loy'){
                    if(Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RVOC')){
                        Ets_Loyalty::sendEmailToCustomerWhenRewardCanceled(Mail::l('Your reward was canceled',$customer->id_lang), $reward);
                    }
                    if(Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RVOC')){
                        Ets_Loyalty::sendEmailToAdminWhenRewardCanceled(Mail::l('A reward was canceled'), $reward);
                    }
                }
                if($reward->program == 'ref'){
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RVOC')){
                        Ets_Sponsor::sendMailRewardCanceled(null, $id, 'reward_canceled', EAM_AM_REF_REWARD);
                    }
                    if((int)Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RVOC')){
                        Ets_Sponsor::sendMailAdminRewardCanceled(null, $id, 'reward_canceled_admin', EAM_AM_REF_REWARD);
                    }
                }
                if($reward->program == 'aff') {
                    if ((int)Configuration::get('ETS_AM_ENABLED_EMAIL_CUSTOMER_RVOC')) {
                        Ets_Affiliate::sendEmailWhenAffiliateCanceled(Mail::l('Your reward was canceled',$customer->id_lang), $reward);
                    }
                    if ((int)Configuration::get('ETS_AM_ENABLED_EMAIL_ADMIN_RVOC')) {
                        Ets_Affiliate::sendEmailWhenAffiliateCanceled(Mail::l('A reward was canceled'), $reward, true);
                    }
                }
                return array(
                    'success' => true,
                    'actions' => $actions,
                    'user' => EtsAmAdmin::getUserInfo($reward->id_customer),
                );
            }
        }
        return array(
            'success' => false,
            'actions' => $actions
        );
    }
    public static function actionRewardUsage($id, $type = null){
        if((int)$id && $type){
            $trans = Ets_affiliatemarketing::$trans;
            $reward = new Ets_Reward_Usage($id);
            $actions = array();
            if(!$reward){
                return array(
                    'success' => true,
                    'actions' => array()
                );
            }
            //$rewards = Db::getInstance()->executeS("SELECT id_ets_am_reward_usage FROM `"._DB_PREFIX_."ets_am_reward_usage`
//                WHERE id_order = ".(int)$reward->id_order."
//                    AND id_withdraw = ".(int)$reward->id_withdraw."
//                    AND id_voucher = ".(int)$reward->id_voucher."
//                    AND id_customer = ".(int)$reward->id_customer."
//                    AND id_shop = ".(int)$context->shop->id
//            );

            $customer = new Customer($reward->id_customer);

            $status = 0;
            $deleted = 0;
            if($type == 'approve'){
                $status = 1;
                $reward->status=$status;                
                $reward->update();
                $actions[] = array(
                    'label' => $trans['Refund'],
                    'class' => 'js-cancel-reward-usage-item',
                    'id' => $id,
                    'icon' => 'rotate-right'
                );
                $actions[] = array(
                    'label' => $trans['Delete'],
                    'class' => 'js-delete-reward-usage-item',
                    'id' => $id,
                    'icon' => 'trash'
                );
            }
            elseif($type == 'delete'){

                $deleted = 1;
                $reward->deleted=$deleted;
                $reward->update();                
            }
            elseif($type == 'cancel'){
                $status = 0;
                $reward->status=$status;
                $reward->update();                                
                if($customer){
                    $actions[] = array(
                        'label' => $trans['Deduct'],
                        'class' => 'js-approve-reward-usage-item',
                        'id' => $id,
                        'icon' => 'minus'
                    );
                }
                $actions[] = array(
                    'label' => $trans['Delete'],
                    'class' => 'js-delete-reward-usage-item',
                    'id' => $id,
                    'icon' => 'trash'
                );
            }
            //if($rewards){
//                foreach ($rewards as $r) {
//                    $usage = new Ets_Reward_Usage($r['id_ets_am_reward_usage']);
//                    if($deleted){
//                        $usage->deleted = 1;
//                    }else{
//                        $usage->status = $status;
//                    }
//                    $usage->update();
//                }
//                return array(
//                    'actions' => $actions,
//                    'success' => true,
//                );
//            }
            return array(
                'actions' => $actions,
                'success' => true,
                'user' => EtsAmAdmin::getUserInfo($reward->id_customer),
            );
        }
        return array(
            'actions' => isset($actions) ? $actions : array(),
            'success' => false,
        );
    }
    
    public static function getRewardUsers($nb = false){
        $context = Context::getContext();
        if(!empty($_POST)){
            $prefix = 'reward_users';
            $having = "";
            $filter_where = "";
            $point = Ets_AM::usingCustomUnit();
            $cols = array('reward_balance', 'loy_rewards', 'ref_rewards', 'ref_orders', 'aff_rewards', 'aff_orders', 'total_withdraws');
            if(Tools::isSubmit('submitFilter'.$prefix, false)){
                if(($_reward_balance_min = Tools::getValue($prefix.'Filter_reward_balance', false)) && Validate::isCleanHtml($_reward_balance_min)){
                    $filter_where .= " AND reward_balance >= ".($point == false ? (float)$_reward_balance_min : ((float)$_reward_balance_min /$point));
                }
                if(($_loy_rewards_min = Tools::getValue($prefix.'Filter_loy_rewards', false)) && $_loy_rewards_min !== '' && Validate::isCleanHtml($_loy_rewards_min)){
                    $filter_where .= " AND loy_rewards >= ".($point == false ? (float)$_loy_rewards_min : ((float)$_loy_rewards_min / $point));
                }
                if(($_ref_rewards_min = Tools::getValue($prefix.'Filter_ref_rewards', false)) && $_ref_rewards_min !== '' && Validate::isCleanHtml($_ref_rewards_min)){
                    $filter_where .= " AND ref_rewards >= ".($point == false ? (float)$_ref_rewards_min : ((float)$_ref_rewards_min / $point));
                }
                if(($_ref_orders_min = Tools::getValue($prefix.'Filter_ref_orders', false)) && $_ref_orders_min !== '' && Validate::isCleanHtml($_ref_orders_min)){
                    $filter_where .= " AND ref_orders >= ".(float)$_ref_orders_min;
                }
                if(($_aff_rewards_min = Tools::getValue($prefix.'Filter_aff_rewards', false)) && $_aff_rewards_min !== '' && Validate::isCleanHtml($_aff_rewards_min)){
                    $filter_where .= " AND aff_rewards >= ".($point == false ? (float)$_aff_rewards_min : ((float)$_aff_rewards_min / $point));
                }
                if(($_mnu_rewards_min = Tools::getValue($prefix.'Filter_mnu_rewards', false)) && $_mnu_rewards_min !== '' && Validate::isCleanHtml($_mnu_rewards_min)){
                    $filter_where .= " AND mnu_rewards >= ".($point == false ? (float)$_mnu_rewards_min : ((float)$_mnu_rewards_min / $point));
                }
                if(($_aff_orders_min = Tools::getValue($prefix.'Filter_aff_orders', false)) && $_aff_orders_min !== '' && Validate::isCleanHtml($_aff_orders_min)){
                    $filter_where .= " AND aff_orders >= ".(float)$_aff_orders_min;
                }
                if(($_total_withdraw_min = Tools::getValue($prefix.'Filter_total_withdraws', false)) && $_total_withdraw_min !== '' && Validate::isCleanHtml($_total_withdraw_min)){
                    $filter_where .= " AND total_withdraws >= ".($point == false ? (float)$_total_withdraw_min : ((float)$_total_withdraw_min / $point));
                }

                if(($_reward_balance_max = Tools::getValue($prefix.'Filter_reward_balance_max', false)) && $_reward_balance_max !== '' && Validate::isCleanHtml($_reward_balance_max)){
                    $filter_where .= " AND reward_balance <= ".($point == false ? (float)$_reward_balance_max : ((float)$_reward_balance_max /$point));
                }
                if(($_loy_rewards_max = Tools::getValue($prefix.'Filter_loy_rewards_max', false)) && $_loy_rewards_max !== '' && Validate::isCleanHtml($_loy_rewards_max)){
                    $filter_where .= " AND loy_rewards <= ".($point == false ? (float)$_loy_rewards_max : ((float)$_loy_rewards_max / $point));
                }
                if(($_ref_rewards_max = Tools::getValue($prefix.'Filter_ref_rewards_max', false)) && $_ref_rewards_max !== '' && Validate::isCleanHtml($_ref_rewards_max)){
                    $filter_where .= " AND ref_rewards <= ".($point == false ? (float)$_ref_rewards_max : ((float)$_ref_rewards_max / $point));
                }
                if(($_ref_orders_max = Tools::getValue($prefix.'Filter_ref_orders_max', false)) && $_ref_orders_max !== '' && Validate::isCleanHtml($_ref_orders_max)){
                    $filter_where .= " AND ref_orders <= ".(float)$_ref_orders_max;
                }
                if(($_aff_rewards_max = Tools::getValue($prefix.'Filter_aff_rewards_max', false)) && $_aff_rewards_max !== '' && Validate::isCleanHtml($_aff_rewards_max)){
                    $filter_where .= " AND aff_rewards <= ".($point == false ? (float)$_aff_rewards_max : ((float)$_aff_rewards_max / $point));
                }
                if(($_mnu_rewards_max = Tools::getValue($prefix.'Filter_mnu_rewards_max', false)) && $_mnu_rewards_max !== '' && Validate::isCleanHtml($_mnu_rewards_max)){
                    $filter_where .= " AND mnu_rewards <= ".($point == false ? (float)$_mnu_rewards_max : ((float)$_mnu_rewards_max / $point));
                }
                if(($_aff_orders_max = Tools::getValue($prefix.'Filter_aff_orders_max', false)) && $_aff_orders_max !== '' && Validate::isCleanHtml($_aff_orders_max)){
                    $filter_where .= " AND aff_orders <= ".(float)$_aff_orders_max;
                }
                if(($_total_withdraw_max = Tools::getValue($prefix.'Filter_total_withdraws_max', false)) && $_total_withdraw_max !== '' && Validate::isCleanHtml($_total_withdraw_max)){
                    $filter_where .= " AND total_withdraws <= ".($point == false ? (float)$_total_withdraw_max : ((float)$_total_withdraw_max / $point));
                }
                $_has_reward = Tools::getValue($prefix.'Filter_has_reward', false);
                if($_has_reward !== '' && $_has_reward !== false){
                    if((int)$_has_reward == 1){
                        $filter_where .= " AND has_reward =1";
                    }
                    else{
                        $filter_where .= " AND has_reward = 0";
                    }

                }

                if(($_id_customer = Tools::getValue($prefix.'Filter_id_customer', false)) && Validate::isCleanHtml($_id_customer) ){
                    $filter_where .= " AND app.id_customer = ".(int)$_id_customer;
                }
                if(($_username = Tools::getValue($prefix.'Filter_username', false)) && Validate::isCleanHtml($_username) ){
                    $filter_where .= " AND username LIKE '%".pSQL($_username)."%'";
                }
                $_status = Tools::getValue($prefix.'Filter_user_status', false);
                if($_status !== false && $_status !== '' && Validate::isCleanHtml($_status)){
                    $filter_where .= " AND user_status = ".(int)$_status;
                }
            }
            if(!Tools::getIsset('submitFilter'.$prefix) && !Tools::getIsset('submitReset'.$prefix)){
                 $filter_where .= " AND has_reward  = 1";
                 $filter_where .= " AND user_status =1";
            }
        }
        if($nb){
            if(!$filter_where)
            {
                $total = 0;
                $total += Db::getInstance()->getValue('SELECT COUNT(DISTINCT id_customer) FROM `'._DB_PREFIX_.'ets_am_participation` WHERE id_shop='.(int)$context->shop->id);
                $total += Db::getInstance()->getValue('SELECT COUNT(DISTINCT r.id_customer) FROM `'._DB_PREFIX_.'ets_am_reward` r
                LEFT JOIN `'._DB_PREFIX_.'ets_am_participation` ap ON (ap.id_customer=r.id_customer)
                WHERE ap.id_customer is null AND ap.id_shop='.(int)$context->shop->id);
                $total += Db::getInstance()->getValue('SELECT COUNT(DISTINCT id_parent) FROM `'._DB_PREFIX_.'ets_am_sponsor` s
                LEFT JOIN `'._DB_PREFIX_.'ets_am_reward` r ON (r.id_customer = s.id_parent)
                LEFT JOIN `'._DB_PREFIX_.'ets_am_participation` ap ON (ap.id_customer = s.id_parent)
                WHERE r.id_customer is null AND ap.id_customer is null AND s.`level`=1 AND s.id_shop='.$context->shop->id);
                $total += Db::getInstance()->getValue('SELECT COUNT(DISTINCT u.id_customer) FROM `'._DB_PREFIX_.'ets_am_user` u
                LEFT JOIN `'._DB_PREFIX_.'ets_am_reward` r ON (r.id_customer = u.id_customer)
                LEFT JOIN `'._DB_PREFIX_.'ets_am_participation` ap ON (ap.id_customer = u.id_customer)
                LEFT JOIN `'._DB_PREFIX_.'ets_am_sponsor` s ON (s.id_parent = u.id_customer AND s.`level`=1)
                WHERE r.id_customer is null AND ap.id_customer is null AND s.id_parent is null AND u.id_shop='.$context->shop->id);
                return $total; 
            }
            $sql = "SELECT COUNT(DISTINCT app.id_customer)
                FROM (
                    SELECT id_customer FROM "._DB_PREFIX_."ets_am_participation WHERE id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_customer FROM "._DB_PREFIX_."ets_am_reward r WHERE id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_parent as id_customer FROM "._DB_PREFIX_."ets_am_sponsor s WHERE s.`level` = 1 AND id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_customer FROM "._DB_PREFIX_."ets_am_user WHERE id_shop = ".(int)$context->shop->id."
                    )  app
                LEFT JOIN (
                    SELECT CONCAT(customer.firstname, ' ', customer.lastname) as username,
                    IFNULL(user.status, 1) as user_status,reward.*, rru2.reward_balance,rru2.loy_rewards, rru2.ref_rewards,rru2.aff_rewards,rru2.mnu_rewards,rru2.total_withdraws
                    
                    FROM "._DB_PREFIX_."customer customer 
                    LEFT JOIN  (SELECT  reward2.id_customer,
                         Count(DISTINCT IF(reward2.program = 'ref'
                                           AND reward2.deleted = 0
                                           AND reward2.id_order > 0,
                                        reward2.id_order,
                                        NULL)) AS
                                                            ref_orders,
                         Count(DISTINCT IF(reward2.program = 'aff'
                                           AND reward2.deleted = 0
                                           AND reward2.id_order >
                                        0, reward2.id_order,
                                        NULL)) AS
                                                            aff_orders,
                         CASE
                           WHEN Sum(reward2.amount) > 0 THEN 1
                           ELSE 0
                         end
                                                  AS
                                                            has_reward
                  FROM   `"._DB_PREFIX_."ets_am_reward` reward2
           GROUP BY reward2.id_customer) reward ON (reward.id_customer=customer.id_customer)
                    LEFT JOIN (
                            SELECT r2.id_customer, 
                                (SUM(IF ( r2.deleted = 0 AND r2.`status` = 1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.`status` != 0 AND ru2.deleted=0, ru2.amount, 0)),0)) AS reward_balance,
                                (SUM(IF(r2.program='loy' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='loy' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS loy_rewards,
                                (SUM(IF(r2.program='ref' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='ref' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS ref_rewards,
                                (SUM(IF(r2.program='aff' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='aff' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS aff_rewards,
                                (SUM(IF(r2.program='mnu' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='mnu' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS mnu_rewards,
                                SUM(IF(ru2.id_withdraw>0 AND ru2.`status` IN(1,0) AND ru2.deleted=0,ru2.amount,0)) AS total_withdraws
                            FROM `"._DB_PREFIX_."ets_am_reward` r2 
                            LEFT JOIN `"._DB_PREFIX_."ets_am_reward_usage` ru2 ON r2.id_customer = ru2.id_customer
                            GROUP BY r2.id_customer
						) rru2 ON rru2.id_customer=customer.id_customer
                    LEFT JOIN "._DB_PREFIX_."ets_am_user `user` ON (customer.id_customer = `user`.id_customer AND user.id_shop = ".(int)$context->shop->id.")
                    GROUP BY customer.id_customer
                ) reward_customer ON (reward_customer.id_customer = app.id_customer)
                WHERE app.id_customer!=0 $filter_where 
                HAVING 1 $having";
            return Db::getInstance()->getValue($sql);
        }

        $order_col = 'app.id_customer';
        $order_dir = 'DESC';
        $offset = 0;
        $limit = 25;
        $page = 1;
        if($_orderBy = Tools::getValue($prefix.'Orderby', false)){
            if(in_array($_orderBy, $cols)){
                $order_col = $_orderBy;
            }
            else{
                if($_orderBy =='id_customer'){
                    $order_col = 'app.id_customer';
                }
                elseif($_orderBy =='username'){
                    $order_col = 'customer.firstname';
                }
                elseif($_orderBy =='user_status'){
                    $order_col = '`user`.status';
                }
            }
        }
        if($_orderWay = Tools::getValue($prefix.'Orderway', false)){
            $_orderWay = Tools::strtoupper($_orderWay);
            if($_orderWay == 'DESC' || $_orderWay == 'ASC'){
                $order_dir = $_orderWay;
            }
        }
       if($_page = (int)Tools::getValue('page', false)){
            $page = (int)$_page > 0 ? (int)$_page : $page;
       }
       if($_limit = (int)Tools::getValue('selected_pagination', false)){
            $limit = (int)$_limit > 0 ? (int)$_limit : 25;
       }
        $sql = "SELECT reward_customer.*, IF(app.id_customer IS NULL, reward_customer.id_customer, app.id_customer) AS id_customer
                FROM (
                    SELECT id_customer FROM "._DB_PREFIX_."ets_am_participation WHERE id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_customer FROM "._DB_PREFIX_."ets_am_reward r WHERE id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_parent as id_customer FROM "._DB_PREFIX_."ets_am_sponsor s WHERE s.`level` = 1 AND id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_customer FROM "._DB_PREFIX_."ets_am_user WHERE id_shop = ".(int)$context->shop->id."
                    )  app
                LEFT JOIN (
                    SELECT CONCAT(customer.firstname, ' ', customer.lastname) as username,
                    IFNULL(user.status, 1) as user_status,reward.*,rru2.reward_balance,rru2.loy_rewards, rru2.ref_rewards,rru2.aff_rewards,rru2.mnu_rewards,rru2.total_withdraws
                    
                    FROM "._DB_PREFIX_."customer customer 
                    LEFT JOIN  (SELECT  reward2.id_customer,
                         
                         Count(DISTINCT IF(reward2.program = 'ref'
                                           AND reward2.deleted = 0
                                           AND reward2.id_order > 0,
                                        reward2.id_order,
                                        NULL)) AS
                                                            ref_orders,
                         Count(DISTINCT IF(reward2.program = 'aff'
                                           AND reward2.deleted = 0
                                           AND reward2.id_order >
                                        0, reward2.id_order,
                                        NULL)) AS
                                                            aff_orders,
                         CASE
                           WHEN Sum(reward2.amount) > 0 THEN 1
                           ELSE 0
                         end
                                                  AS
                                                            has_reward
                  FROM   "._DB_PREFIX_."ets_am_reward reward2
           GROUP BY reward2.id_customer) reward ON (reward.id_customer=customer.id_customer)
                    LEFT JOIN (
                            SELECT r2.id_customer, 
                                (SUM(IF ( r2.deleted = 0 AND r2.`status` = 1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.`status` != 0 AND ru2.deleted=0, ru2.amount, 0)),0)) AS reward_balance,
                                (SUM(IF(r2.program='loy' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='loy' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS loy_rewards,
                                (SUM(IF(r2.program='ref' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='ref' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS ref_rewards,
                                (SUM(IF(r2.program='aff' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='aff' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS aff_rewards,
                                (SUM(IF(r2.program='mnu' AND r2.deleted=0 AND r2.`status`=1, r2.amount, 0)) - COALESCE(SUM(IF(ru2.type='mnu' AND ru2.`status`!=0 AND ru2.deleted=0, ru2.amount,0)),0)) AS mnu_rewards,
                                SUM(IF(ru2.id_withdraw>0 AND ru2.`status` IN(1,0) AND ru2.deleted=0,ru2.amount,0)) AS total_withdraws
                            FROM `"._DB_PREFIX_."ets_am_reward` r2 
                            LEFT JOIN `"._DB_PREFIX_."ets_am_reward_usage` ru2 ON r2.id_customer = ru2.id_customer
                            GROUP BY r2.id_customer
						) rru2 ON rru2.id_customer=customer.id_customer
                    LEFT JOIN "._DB_PREFIX_."ets_am_user `user` ON (customer.id_customer = `user`.id_customer AND user.id_shop = ".(int)$context->shop->id.")
                    GROUP BY customer.id_customer
                ) reward_customer ON (reward_customer.id_customer = app.id_customer)
                WHERE app.id_customer!=0 $filter_where 
                GROUP BY app.id_customer HAVING 1 $having
                ORDER BY $order_col $order_dir
                LIMIT $offset, $limit";

        $results = Db::getInstance()->executeS($sql);
        if($results)
        {
            foreach($results as &$result)
            {
                $result['id_reward_users'] = $result['id_customer'];
            }
        }
        $ids = array_column($results, 'id_customer');
        foreach ($ids as $key => $id_item) {
            if(!$id_item){
                unset($ids[$key]);
            }
        }
        $ids[] = 0;
        $trans = Ets_affiliatemarketing::$trans;
        $e = new Ets_affiliatemarketing();
        if($results){
            $sponsors = Db::getInstance()->executeS("SELECT id_parent, level, COUNT(*) as total_sponsor FROM `"._DB_PREFIX_."ets_am_sponsor` WHERE id_parent IN (".implode(',', $ids).") GROUP BY id_parent, level");
            foreach ($results as $key => &$result) {
                $result['sponsors'] = '--';
                $list_sponsors = '';
                foreach ($sponsors as $k => $sponsor) {
                    if($sponsor['id_parent'] == $result['id_customer']){
                        $list_sponsors .=  $trans['level'].' '.$sponsor['level'].': '.$sponsor['total_sponsor'].$e->getHtmlColum(array(
                                        'type' => 'br'
                                    ));
                        unset($sponsors[$k]);
                    }
                }
                $result['has_reward'] = (int)$result['has_reward'] == 1 ? $trans['yes'] : $trans['no'];
                $result['sponsors'] = $list_sponsors ?  $list_sponsors : '--';
                $result['reward_balance'] = Ets_AM::displayRewardAdmin($result['reward_balance'],true,true);
                $result['loy_rewards'] = Ets_AM::displayRewardAdmin($result['loy_rewards'],true,true);
                $result['ref_rewards'] = Ets_AM::displayRewardAdmin($result['ref_rewards'],true,true);
                $result['aff_rewards'] = Ets_AM::displayRewardAdmin($result['aff_rewards'],true,true);
                $result['mnu_rewards'] = Ets_AM::displayRewardAdmin($result['mnu_rewards'],true,true);
                $result['total_withdraws'] = Ets_AM::displayRewardAdmin((float)$result['total_withdraws'],true,true);
                if($result['id_customer'] && !trim($result['username'])){
                    $c = new Customer($result['id_customer']);
                    if($c && $c->id){
                        $result['username'] = $c->firstname.' '.$c->lastname;
                    }
                    $result['user_status']=1;
                }
                if(!$result['username']){
                    $result['user_status'] = $e->getHtmlColum(array(
                                        'type' => 'label',
                                        'class' => 'label warning-deleted',
                                        'text' => $trans['user_deleted']
                                    ));
                }
                elseif($result['user_status'] == -1){
                    $result['user_status'] = $e->getHtmlColum(array(
                                        'type' => 'label',
                                        'class' => 'label label-default',
                                        'text' => $trans['Suspended']
                                    ));
                }
                else if($result['user_status'] == 1){
                    $result['user_status'] = $e->getHtmlColum(array(
                                        'type' => 'label',
                                        'class' => 'label-success',
                                        'text' => $trans['Active']
                                    ));
                }
                else if($result['user_status'] == 0){
                    $result['user_status'] = $e->getHtmlColum(array(
                                        'type' => 'label',
                                        'class' => 'label label-warning',
                                        'text' => $trans['Pending']
                                    ));
                }
                $result['username'] =  $e->getHtmlColum(array(
                                        'type' => 'link',
                                        'id' => $result['id_customer'],
                                        'link' => $context->link->getAdminLink('AdminModules', true).'&configure=ets_affiliatemarketing&tabActive=reward_users&id_reward_users='.$result['id_customer'].'&viewreward_users',
                                        'class' => '',
                                        'user_deleted' => $result['username'] ? false : true,
                                        'text' => $result['username']
                                    ));
                
                
            }
        }

        return $results;
    }

    public static function getUserInfo($id_user){
        $id_user = (int)$id_user;
        $context = Context::getContext();
        $loy_required_register = (int)Configuration::get('ETS_AM_LOYALTY_REGISTER');
        $ref_required_register = (int)Configuration::get('ETS_AM_REF_REGISTER_REQUIRED');
        $aff_required_register = (int)Configuration::get('ETS_AM_AFF_REGISTER_REQUIRED');
        $totalReward = (float)Db::getInstance()->getValue("SELECT SUM(amount) as total_amount FROM `"._DB_PREFIX_."ets_am_reward` WHERE id_customer=".(int)$id_user);
        $sql = "SELECT app.id_customer as id_customer, customer.firstname as firstname, customer.lastname as lastname, customer.email as email,
                customer.date_add as date_add, customer.birthday as birthday, 
                CASE WHEN $ref_required_register > 0 AND `user`.ref != 0 THEN `user`.ref
                    WHEN $ref_required_register > 0 AND (`user`.ref = 0 OR `user`.ref IS NULL) THEN 0
                    WHEN $ref_required_register = 0 AND `user`.ref < 0 THEN `user`.ref
                    ELSE 1 END as ref_program,
                CASE 
                    WHEN $loy_required_register > 0 AND `user`.loy != 0 THEN `user`.loy
                    WHEN $loy_required_register > 0 AND (`user`.loy = 0 OR `user`.loy IS NULL) THEN 0
                    WHEN $loy_required_register = 0 AND `user`.loy < 0 THEN `user`.loy
                    ELSE 1 END as loy_program,
                CASE 
                    WHEN $aff_required_register > 0 AND `user`.aff != 0 THEN `user`.aff
                    WHEN $aff_required_register > 0 AND (`user`.aff = 0 OR `user`.aff IS NULL) THEN 0
                    WHEN $aff_required_register = 0 AND `user`.aff < 0 THEN `user`.aff
                    ELSE 1 END as aff_program, 
                COALESCE(`user`.`status`, 1) as active,
                SUM(IF(reward.status = 1 , reward.amount, 0)) as total_point, 
                SUM(CASE WHEN reward.program = 'loy' AND reward.status = 1 THEN reward.amount ELSE 0 END) as loy_rewards, 
                SUM(CASE WHEN reward.program = 'ref' AND reward.status = 1 THEN reward.amount ELSE 0 END) as ref_rewards, 
                SUM(CASE WHEN reward.program = 'aff' AND reward.status = 1 THEN reward.amount ELSE 0 END) as aff_rewards,
                SUM(CASE WHEN reward.program = 'mnu' AND reward.status = 1 THEN reward.amount ELSE 0 END) as mnu_rewards,
                (SELECT SUM(amount) FROM `"._DB_PREFIX_."ets_am_reward_usage` ug WHERE id_withdraw > 0 AND deleted = 0 AND status = 1 AND id_shop=".(int)$context->shop->id." AND  id_customer = ".(int)$id_user.") as withdrawn,
                (SELECT SUM(amount) FROM `"._DB_PREFIX_."ets_am_reward_usage` ug WHERE id_order > 0  AND deleted = 0 AND status = 1 AND id_shop=".(int)$context->shop->id." AND id_customer = ".(int)$id_user.") as pay_for_order,
                (SELECT SUM(amount) FROM `"._DB_PREFIX_."ets_am_reward_usage` ug WHERE id_voucher > 0  AND deleted = 0 AND status = 1 AND id_shop=".(int)$context->shop->id." AND id_customer = ".(int)$id_user.") as convert_to_voucher,
                (SELECT SUM(amount) FROM `"._DB_PREFIX_."ets_am_reward_usage` ug WHERE id_customer = $id_user AND deleted = 0 AND status = 1 AND id_shop=".(int)$context->shop->id.") as total_usage
                FROM (
                    SELECT id_customer FROM `"._DB_PREFIX_."ets_am_participation`  WHERE id_customer = $id_user AND id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_customer FROM `"._DB_PREFIX_."ets_am_reward` r WHERE r.id_customer = $id_user AND id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_parent FROM `"._DB_PREFIX_."ets_am_sponsor` s WHERE s.`level` = 1 AND s.id_parent = $id_user AND id_shop = ".(int)$context->shop->id."
                    UNION
                    SELECT id_customer FROM `"._DB_PREFIX_."ets_am_user` WHERE id_customer = $id_user AND id_shop = ".(int)$context->shop->id."
                    )  app
                LEFT JOIN `"._DB_PREFIX_."ets_am_reward` reward ON app.id_customer = reward.id_customer
                LEFT JOIN `"._DB_PREFIX_."customer` customer ON customer.id_customer = app.id_customer
                LEFT JOIN `"._DB_PREFIX_."orders` ord ON reward.id_order = ord.id_order
                LEFT JOIN `"._DB_PREFIX_."ets_am_user` user ON (app.id_customer = user.id_customer AND user.id_shop = ".(int)$context->shop->id.")
                LEFT JOIN `"._DB_PREFIX_."currency` currency ON ord.id_currency = currency.id_currency
                WHERE app.id_customer = $id_user ".($totalReward > 0 ? " AND reward.deleted=0" : "")."
                GROUP BY app.id_customer"; // AND IFNULL(reward.deleted,0) = 0
        $result =  Db::getInstance()->getRow($sql);
        if(!$result){
            Tools::redirectAdmin(404);
        }
        
        $result['total_balance'] =  Ets_Am::displayRewardAdmin((float)$result['total_point'] - (float)$result['total_usage'],true,true);
        $result['total_point'] = Ets_Am::displayRewardAdmin((float)$result['total_point'],true,true);
        $result['loy_rewards'] = Ets_Am::displayRewardAdmin((float)$result['loy_rewards']  - Ets_Reward_Usage::getTotalSpent($result['id_customer'],false,null,$context,'loy') ,true,true );
        $result['ref_rewards'] = Ets_Am::displayRewardAdmin((float)$result['ref_rewards']  - Ets_Reward_Usage::getTotalSpent($result['id_customer'],false,null,$context,'ref'),true,true);
        $result['aff_rewards'] = Ets_Am::displayRewardAdmin((float)$result['aff_rewards']  - Ets_Reward_Usage::getTotalSpent($result['id_customer'],false,null,$context,'aff'),true,true);
        $result['mnu_rewards'] = Ets_Am::displayRewardAdmin((float)$result['mnu_rewards'] - Ets_Reward_Usage::getTotalSpent($result['id_customer'],false,null,$context,'mnu'),true,true);
        $result['withdrawn'] =   Ets_Am::displayRewardAdmin((float)$result['withdrawn'], true,true);
        $result['pay_for_order'] = Ets_Am::displayRewardAdmin((float)$result['pay_for_order'],true,true);
        $result['convert_to_voucher'] = Ets_Am::displayRewardAdmin((float)$result['convert_to_voucher'], true,true);
        $result['total_usage'] = Ets_Am::displayRewardAdmin((float)$result['total_usage'], true,true);
        if((int)Configuration::get('ETS_AM_LOYALTY_ENABLED')){
            if((int)Configuration::get('ETS_AM_LOYALTY_REGISTER')){
                $pa = Ets_Participation::getProgramRegistered($id_user,'loy');
                if($pa && $pa['status'] == 0){
                    $result['loy_status'] = 'pending';
                }
                else{
                    $result['loy_status'] = (int)$result['loy_program'];
                }
            }
            else{
                $result['loy_status'] = $result['loy_program'] === null || $result['loy_program'] === '' ? 1 : (int)$result['loy_program'] ;
            }
        }
        if((int)Configuration::get('ETS_AM_REF_ENABLED')){
            if((int)Configuration::get('ETS_AM_REF_REGISTER_REQUIRED')){
                $pa = Ets_Participation::getProgramRegistered($id_user,'ref');
                if($pa && $pa['status'] == 0){
                    $result['ref_status'] = 'pending';
                }
                else{
                    $result['ref_status'] = (int)$result['ref_program'];
                }
            }
            else{
                $result['ref_status'] = $result['ref_program'] === null || $result['ref_program'] === '' ? 1 : (int)$result['ref_program'] ;
            }
        }
        if((int)Configuration::get('ETS_AM_AFF_ENABLED')){
            if((int)Configuration::get('ETS_AM_AFF_REGISTER_REQUIRED')){
                $pa = Ets_Participation::getProgramRegistered($id_user,'aff');
                if($pa && $pa['status'] == 0){
                    $result['aff_status'] = 'pending';
                }
                else{
                    $result['aff_status'] = (int)$result['aff_program'];
                }
            }
            else{
                $result['aff_status'] = $result['aff_program'] === null || $result['aff_program'] === '' ? 1 : (int)$result['aff_program'] ;
            }
        }
        return $result;
    }


    public static function getSearchSuggestionsReward($query, $type){
        if($query){
            $query = trim($query);
            $query = str_replace(' ', '%', $query);

            if($type == 'withdraw'){
                $sql = "SELECT customer.id_customer as id, customer.firstname as firstname, customer.lastname as lastname 
                    FROM `"._DB_PREFIX_."ets_am_reward_usage` ug
                    LEFT JOIN `"._DB_PREFIX_."customer` customer ON ug.id_customer = customer.id_customer
                    WHERE
                    ug.id_withdraw > 0 AND ug.deleted = 0 AND (customer.id_customer = ".(int)$query." 
                        OR CONCAT(customer.firstname, ' ',customer.lastname) LIKE '%".pSQL($query)."%'
                        OR CONCAT(customer.lastname, ' ',customer.firstname) LIKE '%".pSQL($query)."%')
                    GROUP BY ug.id_customer
                    LIMIT 5"; 
            }
            else{
                $sql = "SELECT customer.id_customer as id, customer.firstname as firstname, customer.lastname as lastname 
                    FROM `"._DB_PREFIX_."ets_am_reward` reward
                    LEFT JOIN `"._DB_PREFIX_."customer` customer ON reward.id_customer = customer.id_customer
                    WHERE reward.deleted = 0 AND (customer.id_customer = ".(int)$query." 
                        OR CONCAT(customer.firstname, ' ',customer.lastname) LIKE '%".pSQL($query)."%'
                        OR CONCAT(customer.lastname, ' ',customer.firstname) LIKE '%".pSQL($query)."%')
                    GROUP BY reward.id_customer
                    LIMIT 5"; 
            }
             
            return Db::getInstance()->executeS($sql);
        }
        return array();
    }
}
