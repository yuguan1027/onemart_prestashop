<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <contact@etssoft.net>
 *  @copyright  2007-2021 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_')) {
    exit();
}

class Ets_Invitation extends ObjectModel
{
	public $email;
	public $name;
	public $datetime_sent;
	public $id_friend;
	public $id_sponsor;

	public static $definition = array(
        'table' => 'ets_am_invitation',
        'primary' => 'id_ets_am_invitation',
        'fields' => array(
            'email' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString'
            ),
            'name' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString'
            ),
            'datetime_sent' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'allow_null' => true
            ),
            'id_friend' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_sponsor' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
        )
    );

    public static function numberEmailCanInvitation(){
    	$context = Context::getContext();
    	if($context->customer && (int)Configuration::get('ETS_AM_REF_EMAIL_INVITE_FRIEND')){
    		$max_email_invatation = Configuration::get('ETS_AM_REF_MAX_INVITATION');
    		$id_customer = (int)$context->customer->id;
    		$sql = "SELECT COUNT(*) as total FROM `"._DB_PREFIX_."ets_am_invitation` WHERE id_sponsor = $id_customer";
    		$total_email = (int)Db::getInstance()->getValue($sql);
    		if(!$max_email_invatation && $max_email_invatation != '0'){
    			return 'unlimted';
    		}

    		$total = (int)$max_email_invatation - $total_email;
    		return $total >= 0 ? $total : 0 ;
    	}

    	return 0;
    }

    public static function totalEmailInvited($id_customer = null){
        $filter_where = "";
        if((int)$id_customer){
            $filter_where .= " AND id_sponsor = ".(int)$id_customer;
        }
        $sql = "SELECT COUNT(*) as total FROM `"._DB_PREFIX_."ets_am_invitation` WHERE 1 ".pSQL($filter_where);
        $total_email = (int)Db::getInstance()->getValue($sql);
        return $total_email;
    }

    public static function emailsInvited($id_customer = null){
        if(!$id_customer){
            $context = Context::getContext();
            $id_customer = $context->customer->id;
        }
        $sql = "SELECT * FROM `"._DB_PREFIX_."ets_am_invitation` WHERE id_sponsor = ".(int)$id_customer;
        return Db::getInstance()->executeS($sql);
    }
    public static function emailIsInvited($email){
    	
    	$sql = "SELECT COUNT(*) as total 
            FROM (SELECT email FROM `"._DB_PREFIX_."customer`                 UNION
                SELECT email FROM "._DB_PREFIX_."ets_am_invitation) c
            WHERE c.email = '".pSQL($email)."'";
    	return (int)Db::getInstance()->getValue($sql);
    }

    public static function getIdCustomerByEmail($email){
    	$customers = Customer::getCustomersByEmail($email);
    	if($customers){
    		return (int)$customers[0]['id_customer'];
    	}

    	return 0;
    }

    public static function updateIdFriend($id_customer, $email){
    	$sql = "SELECT id_ets_am_invitation, email FROM `"._DB_PREFIX_."ets_am_invitation` WHERE email = '".pSQL($email)."'";
    	$invitation = Db::getInstance()->getRow($sql);
    	if($invitation){
    		$inv = new Ets_Invitation($invitation['id_ets_am_invitation']);
    		$inv->id_friend = $id_customer;
    		$inv->update();
    		return true;
    	}

    	return false;
    }

    public static function getInvitations($params = array()){
        $page = 1;
        $limit = 10;
        $offset = 0;

        $filter_where = "";
        if(isset($params['id_customer']) && $params['id_customer']){
            $filter_where .= " AND inv.id_sponsor = ".(int)$params['id_customer'];
        }
        $sql_total = "SELECT COUNT(*) AS total 
                    FROM `"._DB_PREFIX_."ets_am_invitation` inv
                    LEFT JOIN `"._DB_PREFIX_."customer` customer ON inv.email = customer.email
                    WHERE 1 ".pSQL($filter_where);
        $total_result = (int)Db::getInstance()->getValue($sql_total);
        if($total_result > 0){
            if(isset($params['limit']) && $params['limit'] && (int)$params['limit'] > 0){
                $limit = (int)$params['limit'];

            }
            $total_page = ceil($total_result / $limit);
            if(isset($params['page']) && $params['page'] && (int)$params['page'] > 0){
                $page = (int)$params['page'];
                if($page > $total_page){
                    $page = $total_page;
                }
            }
            $offset = ($page - 1) * $limit;
             $sql = "SELECT inv.*, customer.firstname as firstname, customer.lastname as lastname, IF(inv.id_friend > 0, 1, 0) as status 
                FROM `"._DB_PREFIX_."ets_am_invitation` inv
                LEFT JOIN `"._DB_PREFIX_."customer` customer ON inv.email = customer.email
                WHERE 1 ".pSQL($filter_where)."
                ORDER BY inv.datetime_sent DESC
                LIMIT $offset, $limit";
            $results = Db::getInstance()->executeS($sql);
            foreach ($results as &$result) {
                $result['username'] = $result['firstname'] ? $result['firstname'].$result['lastname'] : $result['name'];
            }

            return array(
                'total_result' => $total_result,
                'total_page' => $total_page,
                'current_page' => $page,
                'per_page' => $limit,
                'result' => $results
            );
        }
        return array(
            'total_result' => 0,
            'total_page' => 1,
            'current_page' => 1,
            'per_page' => $limit,
            'result' => array()
        );
       
    }
}