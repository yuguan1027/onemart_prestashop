<?php
/**
* 2007-2021 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <contact@etssoft.net>
*  @copyright  2007-2021 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*/

class Ets_Reward_Product extends ObjectModel
{
    /**
     * @var int
     */
    public $id_ets_am_reward_product;
    /**
     * @var int
     */
    public $id_product;
    /**
     * @var int
     */
    public $id_ets_am_reward;
    /**
     * @var string
     */
    public $program;
    /**
     * @var float
     */
    public $amount;
    /**
     * @var int
     */
    public $id_order;
    /**
     * @var int
     */
    public $id_seller;
    /**
     * @var int
     */
    public $status;
    /**
     * @var int
     */
    public $quantity;
    /**
     * @var datetime
     */
    public $datetime_added;

    public static $definition = array(
        'table' => 'ets_am_reward_product',
        'primary' => 'id_ets_am_reward_product',
        'multilang_shop' => true,
        'fields' => array(
            'id_ets_am_reward_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_ets_am_reward' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'program' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString'
            ),
            'amount' => array(
                'type' => self::TYPE_FLOAT,
                'validate' => 'isFloat'
            ),
            'quantity' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_order' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_seller' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'status' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'datetime_added' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDate',
                'allow_null' => true
            )
        )
    );

    /**
     * @param null $context
     * @return array
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
     public static function getAffiliateProducts($context = null)

    {

        $query = array();

        if (!$context) {

            $context = Context::getContext();

        }

        $limit = 20;

        $order_by = 'name';

        $order_way = 'ASC';

        $page = 1;

        if ($p = (int)Tools::getValue('page')) {

            $page = (int)$p;

            $query['page'] = $page;

        }



        $offset = ($page - 1) * $limit;

        if (($order = Tools::getValue('orderBy')) && Validate::isCleanHtml($order)) {

            $order_by = $order;

            $query['orderBy'] = $order_by;

        }

        if (($o_direct = Tools::getValue('orderWay')) && Validate::isCleanHtml($o_direct)) {
            $order_way = $o_direct;
            $query['orderWay'] = $order_way;

        }

        $order_prefix = 'pl';

        if ($order_by && $order_by != 'name') {

            $order_prefix = 'p';

        }

        if ($order && $order_by == 'commission_rate') {

            $order_prefix = '';

        }

        $c_type = Configuration::get('ETS_AM_AFF_CAT_TYPE');

        if (!$c_type || $c_type == 'ALL') {

            $aff_cats = array();

        } else {

            $aff_cats = explode(',', Configuration::get('ETS_AM_AFF_CATEGORIES'));

        }

        $categories = array();

        if (($filter = Tools::getValue('category')) && Validate::isCleanHtml($filter) ) {
            if ($filter !== 'all') {
                $categories[] = (int)$filter;

            }
            $query['category'] = $filter;

        }
        if(!$categories){
            $categories = $aff_cats;
        }
        $c_includes = Configuration::get('ETS_AM_AFF_SPECIFIC_PRODUCTS');

        $c_excludes = Configuration::get('ETS_AM_AFF_PRODUCTS_EXCLUDED');

        $includes = array();

        $excludes = array();

        if ($c_includes) {

            $includes = explode(',', $c_includes);

        }

        if ($c_excludes) {

            $excludes = explode(',', $c_excludes);

        }
        if(Configuration::get('ETS_AM_AFF_PRODUCTS_EXCLUDED_DISCOUNT'))
        {
            $sql = 'SELECT id_product FROM `'._DB_PREFIX_.'specific_price` WHERE (`from` = "0000-00-00 00:00:00" OR `from` <="'.pSQL(date('Y-m-d H:i:s')).'" ) AND (`to` = "0000-00-00 00:00:00" OR `to` >="'.pSQL(date('Y-m-d H:i:s')).'" )';
            $products = Db::getInstance()->executeS($sql);
            if($products)
            {
                foreach($products as $product)
                    $excludes[] = $product['id_product'];
            }
        }
        $default_reward_config = self::getAffProductRewardDefaultConfig();

        $select_part = str_replace(',','.',$default_reward_config['amount']);
        
        $select = 'SELECT p.`id_product`, pl.`link_rewrite`, pr.`how_to_calculate`, pr.`use_default`, IF(pa.`id_product_attribute` is NOT NULL, pa.`id_product_attribute`, 0) as id_product_attribute, p.`reference`, pl.`name`, p.price, image_shop.`id_image` id_image, il.`legend`,pr.use_default,pr.how_to_calculate,pr.default_percentage,pr.default_fixed_amount, (IF(pr.use_default IS NOT NULL, IF(pr.how_to_calculate = \'PERCENT\', pr.default_percentage / 100, pr.default_fixed_amount) , ' . (string)$select_part . ')) as commission_rate';

        $cat_sql_part = self::buildCategorySqlPart($aff_cats, $excludes, $categories, $context);

        $product_sql_part = self::buildProductSqlPart($excludes, $includes, $categories, $context);

        $s_total = 'SELECT COUNT(*) as total FROM ((SELECT p.id_product ' . (string)$cat_sql_part . ') UNION (SELECT p.id_product ' . (string)$product_sql_part . ')) r';

        $sql_order = '';

        if ($order_prefix != '') {

            $sql_order .= $order_prefix . '.' . (string)$order_by;

        } else {

            $sql_order .= (string)$order_by;

        }

        $cat_sql_part .= 'GROUP BY p.id_product ORDER BY ' . (string)$sql_order . ' ' . (string)$order_way;

        $product_sql_part .= 'GROUP BY p.id_product ORDER BY ' . (string)$sql_order . ' ' . (string)$order_way;

        $sql = '(' . (string)$select . ' ' . (string)$cat_sql_part . ') UNION (' . (string)$select . ' ' . (string)$product_sql_part . ') ORDER BY ' . (string)$order_by . ' ' . (string)$order_way . ' LIMIT ' . (int)$limit . ' OFFSET ' . (int)$offset;
        $total = Db::getInstance()->getValue($s_total);
        $results = Db::getInstance()->executeS($sql);

        $total_page = (int)ceil($total / $limit);

        $imageType = version_compare(_PS_VERSION_, '1.7.0', '>=')? ImageType::getFormattedName('small') : ImageType::getFormatedName('small');
        foreach ($results as &$result) {

            $image = ($result['id_product_attribute'] && ($image = self::getCombinationImageById($result['id_product_attribute'],$context->language->id))) ? $image : Product::getCover($result['id_product']);

          
            $result['image'] = $context->link->getImageLink($result['link_rewrite'], isset($image['id_image']) ? $image['id_image'] : 0, $imageType);
            $product = new Product((int)$result['id_product']);
            $productPrice = 0;
            $productPriceWithoutReduction = 0;
            $no_tax = Configuration::get('ETS_AM_AFF_TAX_EXCLUDED');
            $productPrice = $product->getPrice(!$no_tax, null, 6);
            $productPriceWithoutReduction = $product->getPriceWithoutReduct($no_tax, null);
 //           $priceDisplay = Product::getTaxCalculationMethod((int)$context->cookie->id_customer);
//            if (!$priceDisplay || $priceDisplay == 2) {
//
//                $productPrice = $product->getPrice(true, null, 6);
//
//                $productPriceWithoutReduction = $product->getPriceWithoutReduct(false, null);
//
//            } elseif ($priceDisplay == 1) {
//
//                $productPrice = $product->getPrice(false, null, 6);
//
//                $productPriceWithoutReduction = $product->getPriceWithoutReduct(true, null);
//
//            }
            $tax = $product->getTaxesRate();

            $result['reduction'] = $productPriceWithoutReduction - $productPrice;

            $result['price'] = $productPrice;

            if(!$result['use_default'] || $result['use_default'] == 0){
                if($default_reward_config['type'] == 'PERCENT'){
                    $result['commission_rate']  = (float)$result['price'] * (float)$result['commission_rate'];
                }
            }
            else{
                if($result['how_to_calculate'] == 'PERCENT'){
                    $result['commission_rate']  = (float)$result['price'] * (float)$result['commission_rate'];
                }
            }

            $percentage = $result['price'] && (float)$result['price'] > 0 ? ((float) $result['commission_rate'] / (float) $result['price']) * 100 : 0;
            
            $result['commission_rate_percentage'] = Tools::ps_round($percentage, 2) . '%';

            $result['display_price'] =Ets_affiliatemarketing::displayPrice($productPrice);

            $result['price_without_reduction'] = $productPriceWithoutReduction;

            $result['price_tax_exc'] = $result['price_without_reduction'] - $tax;

            $result['price_tax_inc'] = $result['price_without_reduction'];

            $result['display_price_without_reduction'] = Ets_affiliatemarketing::displayPrice($productPriceWithoutReduction);
            if($result['use_default'] || trim($result['use_default'])==='')
            {
                if ($default_reward_config['type'] == 'PERCENT') {
                    $result['commission_rate'] = $productPrice * $default_reward_config['amount'];
                } else {
                    $result['commission_rate'] = $default_reward_config['amount'];
                }
            }
            else
            {
                
                if($result['how_to_calculate']=='PERCENT')
                    $result['commission_rate'] = $productPrice * $result['default_percentage']/100;
                else
                    $result['commission_rate'] = $result['default_fixed_amount'];
            }
            $percentage = $result['price'] && (float)$result['price'] > 0 ? ((float) $result['commission_rate'] / (float) $result['price']) * 100 : 0;
            $result['commission_rate_percentage'] = Tools::ps_round($percentage, 2) . ' %';
            $result['commission_rate'] = Ets_AM::displayPriceOnly($result['commission_rate'], $context);

            $product = new Product($result['id_product']);

            $aff_link = Ets_Affiliate::generateAffiliateLinkForProduct($product, $context);

            $p_link = Ets_Affiliate::generateAffiliateLinkForProduct($product, $context, false);

            $result['link'] = $p_link;

            $result['aff_link'] = $aff_link;
            

        }

        return array(

            'current_page' => $page,

            'per_page' => $limit,

            'results' => $results,

            'total_page' => $total_page,

            'query' => $query

        );

    }
    public static function getCombinationImageById($id_product_attribute, $id_lang)
    {
        if (version_compare(_PS_VERSION_, '1.6.1.0', '<'))
        {
            if (!Combination::isFeatureActive() || !$id_product_attribute)
                return false;
            $result = Db::getInstance()->executeS('
				SELECT pai.`id_image`, pai.`id_product_attribute`, il.`legend`
				FROM `'._DB_PREFIX_.'product_attribute_image` pai
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (il.`id_image` = pai.`id_image`)
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_image` = pai.`id_image`)
				WHERE pai.`id_product_attribute` = '.(int)$id_product_attribute.' AND il.`id_lang` = '.(int)$id_lang.' ORDER by i.`position` LIMIT 1'
            );
            if (!$result)
                return false;
            return $result[0];
        }
        else
            return Product::getCombinationImageById($id_product_attribute, $id_lang);
    }
    protected static function getAffProductRewardDefaultConfig()
    {
        $cal_by = Configuration::get('ETS_AM_AFF_HOW_TO_CALCULATE');
        if ($cal_by == 'PERCENT') {
            return array(
                'type' => 'PERCENT',
                'amount' => (float) Configuration::get('ETS_AM_AFF_DEFAULT_PERCENTAGE') / 100
            );
        } elseif ($cal_by == 'FIXED') {
            return array(
                'type' => $cal_by,
                'amount' => Configuration::get('ETS_AM_AFF_DEFAULT_FIXED_AMOUNT')
            );
        } else {
            return array(
                'type' => $cal_by,
                'amount' => 0
            );
        }
    }

    /**
     * @param array $cat
     * @param null $context
     * @return string
     */
    protected static function buildCategorySqlPart($cat = array(), $excludes = array(), $filter_cats = array(),  $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $sql_part = 'FROM `' . _DB_PREFIX_ . 'product` p ' . Shop::addSqlAssociation('product', 'p') . '
           LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = ' . (int)$context->language->id . ' AND pl.id_shop = ' . (int)$context->shop->id . ')
           LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop ON (image_shop.`id_product` = p.`id_product` AND image_shop.id_shop = ' . (int)$context->shop->id . ')
           LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$context->shop->id . ')
           INNER JOIN `' . _DB_PREFIX_ . 'category_product`  c ON (c.`id_product` = p.`id_product`) 
           LEFT JOIN `' . _DB_PREFIX_ . 'ets_am_aff_reward` pr ON pr.id_product = p.id_product
           LEFT JOIN  `'._DB_PREFIX_.'product_attribute` pa ON pa.id_product = p.id_product
           WHERE product_shop.active = 1 ';
        if (count($filter_cats)) {
            $sql_part .= ' AND c.id_category IN (' . implode(',', $filter_cats) . ') ';
        }
        if (count($cat)) {
            $sql_part .= ' AND c.id_category IN (' . implode(',', $cat) . ') ';
        }
        if (count($excludes)) {
            $sql_part .= ' AND c.id_product NOT IN (' . implode(',', $excludes) . ') ';
        }
        if(($product_name = trim(Tools::getValue('product_name'))) && Validate::isCleanHtml($product_name) )
            $sql_part .= ' AND pl.name like "%'.pSQL($product_name).'%" ';
        return $sql_part;
    }

    /**
     * @param array $excludes
     * @param array $includes
     * @param null $context
     * @return string
     */
    protected static function buildProductSqlPart($excludes = array(), $includes = array(), $cat = array(), $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $sql_part = 'FROM `' . _DB_PREFIX_ . 'product` p ' . Shop::addSqlAssociation('product', 'p') . '
           LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = ' . (int)$context->language->id . ' AND pl.id_shop = ' . (int)$context->shop->id . ')
           LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop ON (image_shop.`id_product` = p.`id_product` AND image_shop.id_shop = ' . (int)$context->shop->id . ')
           LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$context->shop->id . ')
           INNER JOIN `' . _DB_PREFIX_ . 'category_product`  c ON (c.`id_product` = p.`id_product`) 
           LEFT JOIN `' . _DB_PREFIX_ . 'ets_am_aff_reward` pr ON pr.id_product = p.id_product
           LEFT JOIN  `'._DB_PREFIX_.'product_attribute` pa ON pa.id_product = p.id_product
           WHERE product_shop.active = 1 ';
        $intersect = array_intersect($includes, $excludes);
        if (count($intersect)) {
            foreach ($intersect as $item) {
                foreach ($excludes as $key => $value) {
                    if ($value === $item) {
                        unset($excludes[$key]);
                    }
                }
            }
        }

        if (count($cat)) {
            $sql_part .= ' AND c.id_category IN (' . implode(',', $cat) . ') ';
        }
        if(($product_name = trim(Tools::getValue('product_name'))) && Validate::isCleanHtml($product_name) )
            $sql_part .= ' AND pl.name like "%'.pSQL($product_name).'%" ';
        if (!Configuration::get('ETS_AM_AFF_CAT_TYPE') || Configuration::get('ETS_AM_AFF_CAT_TYPE') == 'ALL') {
            if (count($excludes)) {
                $sql_part .= ' AND c.id_product NOT IN (' . implode(',', $excludes) . ') ';
            }
            return $sql_part;
        }
        if (count($excludes)) {
            $sql_part .= ' AND c.id_product NOT IN (' . implode(',', $excludes) . ') ';
        }
        if (count($includes)) {
            $sql_part .= ' AND c.id_product IN (' . implode(',', $includes) . ')';
        }
        return $sql_part;
    }

    /**
     * @param array $categories
     * @param null $context
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAffiliateProductCat($context = null)
    {
        if (! $context) {
            $context = Context::getContext();
        }
        $response = array();
        $type = Configuration::get('ETS_AM_AFF_CAT_TYPE');
        if ($type && $type == 'ALL') {
            $cat = Category::getSimpleCategories($context->language->id);
            if (count($cat)) {
                $response = $cat;
            }
        } else {
            $configCat = Configuration::get('ETS_AM_AFF_CATEGORIES');
            if ($configCat) {
                $configCat = explode(',', $configCat);
                foreach ($configCat as $config) {
                    $cat = new Category($config);
                    if ($cat->id) {
                        $response[] = array(
                            'id_category' => $config,
                            'name' => $cat->name[$context->language->id]
                        );
                    }
                }
            }
        }
        return $response;
    }

    protected static function getChildCat($id_category, $response = array(), $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $cats = Category::getNestedCategories($id_category, $context->language->id, true);
        if (count($cats)) {
            foreach ($cats as $cat) {
                $response[] = array('id_category' => $cat['id_category'], 'name' => $cat['name']);
                if (isset($cat['children']) && count($cat['children'])) {
                    foreach ($cat['children'] as $c) {
                        $response[] = array('id_category' => $c['id_category'], 'name' => $c['name']);
                        if (isset($c['children']) && count($c['children'])) {
                            $res = self::getChildCat($c['id_category'], $response, $context);
                            foreach ($res as $r) {
                                $response[] = $r;
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }

    public static function unique_multidim_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}
