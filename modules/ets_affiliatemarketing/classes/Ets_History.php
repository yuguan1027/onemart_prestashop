<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <contact@etssoft.net>
 *  @copyright  2007-2021 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

if (!defined('_PS_VERSION_')) {
    exit();
}

class Ets_History extends Ets_AM
{
    /**
     * Ets_History constructor.
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $program
     * @param null $context
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getHistory($program, $params = array(), $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $acceptable = array(
            EAM_AM_LOYALTY_REWARD,
            EAM_AM_AFFILIATE_REWARD
        );
        if (!in_array($program, $acceptable)) {
            return false;
        }
        $search = '';
        $limit = 10;
        $page = 0;
        $sort = 'id';
        $order = 'DESC';
        if (array_key_exists('limit', $params)) {
            $limit = $params['limit'];
        }
        if (array_key_exists('page', $params)) {
            $page = $params['page'];
        }
        if (array_key_exists('sort', $params)) {
            $sort = $params['sort'];
        }
        if (array_key_exists('order', $params)) {
            $order = $params['order'];
        }
        if (array_key_exists('search', $params)) {
            $search = $params['search'];
        }
        $count = "SELECT count(*) as `total` FROM `"._DB_PREFIX_."ets_am_reward` eam
                   INNER JOIN `"._DB_PREFIX_."customer` pc 
                      ON eam.`id_customer` = pc.`id_customer`
                   INNER JOIN `"._DB_PREFIX_."orders` po 
                      ON eam.`id_order` = po.`id_order` 
                      AND po.`id_shop` = " . (int)$context->shop->id . "
                   INNER JOIN `"._DB_PREFIX_."order_state` pos 
                      ON po.`current_state` = pos.`id_order_state`
                   INNER JOIN `"._DB_PREFIX_."order_state_lang` posl 
                      ON pos.`id_order_state` = posl.`id_order_state`
                      AND posl.`id_lang` = " . (int)$context->language->id . "
                WHERE eam.`program` = '".pSQL($program)."'";
        if ($search) {
            $count .= " AND (eam.`id_ets_am_reward` = '" .  pSQL($search) . "'
                          OR eam.`firstname` LIKE '%" .  pSQL($search) . "%' 
                          OR eam.`lastname` LIKE '%" .  pSQL($search) . "%'
                          OR eam.`email` LIKE '%" .  pSQL($search) . "%')";
        }
        $sql = "SELECT eam.`id_ets_am_reward`,
                       eam.`amount`,
                       eam.`datetime_added`,
                       eam.`datetime_validated`,
                       eam.`datetime_canceled`,
                       eam.`expired_date`,
                       eam.await_validate,
                       po.`reference` as order_ref,
                       pc.`id_customer`,
                       CONCAT(pc.`firstname`, ' ', pc.`lastname`) as customer_name,
                       pc.`email`,
                       posl.name as order_status
                FROM `"._DB_PREFIX_."ets_am_reward` eam
                   INNER JOIN `"._DB_PREFIX_."customer` pc 
                      ON eam.`id_customer` = pc.`id_customer`
                   INNER JOIN `"._DB_PREFIX_."orders` po 
                      ON eam.`id_order` = po.`id_order` 
                      AND po.`id_shop` = " . (int)$context->shop->id . "
                   INNER JOIN `"._DB_PREFIX_."order_state` pos 
                      ON po.`current_state` = pos.`id_order_state`
                   INNER JOIN `"._DB_PREFIX_."order_state_lang` posl 
                      ON pos.`id_order_state` = posl.`id_order_state`
                      AND posl.`id_lang` = " . (int)$context->language->id . "
                WHERE eam.`program` = '".pSQL($program)."'";
        if ($search) {
            $sql .= " AND (eam.`id_ets_am_reward` = '" . pSQL($search) . "' 
                          OR eam.`firstname` LIKE '%" .  pSQL($search) . "%' 
                          OR eam.`lastname` LIKE '%" .  pSQL($search) . "%'
                          OR eam.`email` LIKE '%" .  pSQL($search) . "%')";
        }
        if ($sort == 'id') {
            $sort = 'eam.`id_ets_am_reward`';
        }
        $current_page = $page;
        if ($page == 0) {
            $current_page = 1;
        }
        $sql .= " ORDER BY " . (string)$sort . " " . (string)$order . " LIMIT " . (int)$limit . " OFFSET " . (int)($limit * $page);
        $total = Db::getInstance()->getRow($count)['total'];
        $results = Db::getInstance()->executeS($sql);
        $pagination = array(
            'current_page' => $current_page,
            'per_page' => $limit,
            'data' => $results,
            'total' => $total
        );
        return $pagination;
    }

    /**
     * @param array $params
     * @param null $context
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getLoyaltyHistory($params = array(), $context = null)
    {
        return self::getHistory(EAM_AM_LOYALTY_REWARD, $params, $context);
    }

    /**
     * @param array $params
     * @param null $context
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAffiliateHistory($params = array(), $context = null)
    {
        return self::getHistory(EAM_AM_AFFILIATE_REWARD, $params, $context);
    }

    /**
     * @param $id_customer
     * @param array $params
     * @param null $context
     * @return array
     * @throws PrestaShopDatabaseException
     */
    public static function getCustomerHistoryRewards($id_customer = null, $params = array(), $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        if(!$id_customer){
          $id_customer = $context->customer->id;
        }
        $page = 1;
        $limit = 20;
        if (array_key_exists('page', $params)) {
            $page = $params['page'];
        }
        if (array_key_exists('limit', $params)) {
            $limit = $params['limit'];
        }
        
        $total = "SELECT COUNT(*) as total
                    FROM (SELECT  id_ets_am_reward as id,
                              amount,
                              status,
                              datetime_added,
                              program,
                              1 as `type`,
                              note,
                              datetime_canceled,
                              datetime_validated,
                              expired_date,
                              await_validate,
                              null as id_order,
                              null as id_withdraw,
                              null as id_voucher
                      FROM `"._DB_PREFIX_."ets_am_reward`
                      WHERE id_customer = " . (int)$id_customer . "
                        AND id_shop = " . (int)$context->shop->id . "
                      UNION ALL
                      SELECT  id_ets_am_reward_usage as id,
                              amount,
                              status,
                              datetime_added,
                              null as program,
                              0 as `type`,
                              note,
                              null as datetime_canceled,
                              null as datetime_validated,
                              null as expired_date,
                              null as await_validate,
                              id_order,
                              id_withdraw,
                              id_voucher
                      FROM `"._DB_PREFIX_."ets_am_reward_usage`
                     WHERE id_customer = " . (int)$id_customer . "
                        AND id_shop = " . (int)$context->shop->id . "
                     ) reward";
        $total = (int)Db::getInstance()->getValue($total);
        $total_page = ceil($total / $limit);
        if($page > $total_page){
          $page = $total_page;
        }
        $offset = ($page - 1) * $limit;
        $sql = "SELECT *
                FROM (SELECT  id_ets_am_reward as id,
                              amount,
                              status,
                              datetime_added,
                              program,
                              1 as `type`,
                              note,
                              datetime_canceled,
                              datetime_validated,
                              expired_date,
                              await_validate,
                              null as id_order,
                              null as id_withdraw,
                              null as id_voucher
                      FROM `"._DB_PREFIX_."ets_am_reward`
                      WHERE id_customer = " . (int)$id_customer . "
                        AND id_shop = " . (int)$context->shop->id . "
                      UNION ALL
                      SELECT  id_ets_am_reward_usage as id,
                              amount,
                              status,
                              datetime_added,
                              null as program,
                              0 as `type`,
                              note,
                              null as datetime_canceled,
                              null as datetime_validated,
                              null as expired_date,
                              null as await_validate,
                              id_order,
                              id_withdraw,
                              id_voucher
                      FROM `"._DB_PREFIX_."ets_am_reward_usage`
                     WHERE id_customer = " . (int)$id_customer . "
                        AND id_shop = " . (int)$context->shop->id . "
                     ) reward 
                ORDER BY reward.datetime_added DESC ";
        if ($limit && $offset) {
            $sql .= "LIMIT " . (int)$limit . "
                OFFSET " . (int)$offset;
        }
        $rewards = Db::getInstance()->executeS($sql);
        $results = array(
            'total' => $total,
            'data' => $rewards
        );
        return $results;
    }
}
