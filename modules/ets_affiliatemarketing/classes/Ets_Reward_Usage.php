<?php
/**
* 2007-2021 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <contact@etssoft.net>
*  @copyright  2007-2021 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*/

if (!defined('_PS_VERSION_')) {
    exit();
}

class Ets_Reward_Usage extends ObjectModel
{
    /**
     * @var int
     */
    public $id_ets_am_reward_usage;
    /**
     * @var float
     */
    public $amount;
    /**
     * @var datetime
     */
    public $datetime_added;
    /**
     * @var int
     */
    public $id_customer;
    /**
     * @var int
     */
    public $id_shop;
    /**
     * @var int
     */
    public $id_voucher;
    /**
     * @var int
     */
    public $id_order;
    /**
     * @var int
     */
    public $id_withdraw;
    /**
     * @var int
     */
    public $id_currency;
    /**
     * @var int
     */
    public $status;
    /**
     * @var string
     */
    public $note;
    /**
     * @var int
     */
    public $deleted;
    public $type;

    public static $definition = array(
        'table' => 'ets_am_reward_usage',
        'primary' => 'id_ets_am_reward_usage',
        'multilang_shop' => true,
        'fields' => array(
            'id_ets_am_reward_usage' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'amount' => array(
                'type' => self::TYPE_FLOAT,
                'validate' => 'isFloat'
            ),
            'type' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString'
            ),
            'id_customer' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_shop' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_voucher' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_withdraw' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_order' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'id_currency' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt'
            ),
            'status' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            ),
            'note' => array(
                'type' => self::TYPE_STRING,
                'validate' => 'isString'
            ),
            'datetime_added' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'allow_null' => true
            ),
            'deleted' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isInt'
            )
        )
    );


    /**
     * @param string $program
     * @param null $id_customer
     * @param null $context
     * @return float
     */
    public static function getTotalEarn($program = 'all', $id_customer = null, $context = null,$id_order=0)
    {
        if (!$context)
            $context = Context::getContext();
        if (!$id_customer) {
            $id_customer = $context->customer->id;
        }
        $sql = "SELECT SUM(`amount`) AS `total` FROM `" . _DB_PREFIX_ . "ets_am_reward` WHERE `id_customer` = " . (int)$id_customer . " AND `id_shop` = " . (int)$context->shop->id;
        
        if ($program != 'all' && $program) {
            $sql .= " AND program = '" . pSQL($program) . "'";
        }
        if($id_order)
            $sql .=' AND id_order!="'.(int)$id_order.'"';
        $sql .= " AND `deleted` = 0 AND `status` = 1";
        $result = Db::getInstance()->getRow($sql)['total'];
        return (float)$result;
    }
    public static function getTotalEarnWithoutLoyalty($id_customer = null, $context = null)
    {
        if (!$context)
            $context = Context::getContext();
        if (!$id_customer) {
            $id_customer = $context->customer->id;
        }
        $sql = "SELECT SUM(`amount`) AS `total` FROM `" . _DB_PREFIX_ . "ets_am_reward` WHERE `id_customer` = " . (int)$id_customer . " AND `id_shop` = " . (int)$context->shop->id . " AND program = '" . pSQL(EAM_AM_LOYALTY_REWARD) . "'";

        $sql .= " AND `deleted` = 0 AND `status` = 1";
        $result = Db::getInstance()->getRow($sql)['total'];
        return (float)$result;
    }


    /**
     * @param null $id_customer
     * @param bool $only_withdraw
     * @param null $status
     * @param null $context
     * @return float
     */
    public static function getTotalSpent($id_customer = null, $only_withdraw = false, $status = null, $context = null,$program='all')
    {
        if (!$context)
            $context = Context::getContext();
        if (!$id_customer && isset($context->customer))
            $id_customer = $context->customer->id;
        $sql = "SELECT SUM(`amount`) as total from `" . _DB_PREFIX_ . "ets_am_reward_usage` WHERE  `id_shop` = " . (int)$context->shop->id . "  AND `deleted` = 0".($id_customer ? " AND `id_customer` = " . (int)$id_customer:'');
        if ($only_withdraw) {
            $sql .= " AND id_withdraw IS NOT NULL ";
        }
        if($program!='all')
            $sql .= " AND type='".pSQL($program)."'";
        if (!is_null($status) && is_int($status) && ($status != -1 || $status != -2)) {
            $sql .= " AND status = " . (int)$status;
        }
        //$sql .= " AND `status` != -1 AND `status` != -2";
        $sql .= " AND `status` != 0";
        $result = (float)Db::getInstance()->getRow($sql)['total'];
        return $result;

    }

    public static function getTotalSpentLoy($id_customer = null, $only_withdraw = false, $status = null, $context = null)
    {
        if (!$context)
            $context = Context::getContext();
        if (!$id_customer)
            $id_customer = $context->customer->id;
        $sql = "SELECT SUM(`amount`) as total from `" . _DB_PREFIX_ . "ets_am_reward_usage` WHERE `type` = 'loy' AND `id_customer` = " . (int)$id_customer . " AND `id_shop` = " . (int)$context->shop->id . "  AND `deleted` = 0";

        if ($only_withdraw) {
            $sql .= " AND id_withdraw IS NOT NULL ";
        }
        if (!is_null($status) && is_int($status) && ($status != -1 || $status != -2)) {
            $sql .= " AND status = " . (int)$status;
        }
        $sql .= " AND `status` != 0";
        //$sql .= " AND `status` != -1 AND `status` != -2";
        $result = (float)Db::getInstance()->getRow($sql)['total'];
        return $result;

    }

    /**
     * @param null $id_customer
     * @param null $context
     * @return float
     */
    public static function getTotalBalance($id_customer = null, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        if (!$id_customer) {
            $id_customer = $context->customer->id;
        }
        $total_earn = self::getTotalEarn('all', $id_customer, $context);
        $total_spent = self::getTotalSpent(false, null, null, $context);

        $total_balance = $total_earn - $total_spent;
        return $total_balance;
    }

    public static function getAmountCanWithdrawRewards($id_customer = null, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        if (!$id_customer)
            $id_customer = $context->customer->id;
        $allow_withdraw_loyalty = Configuration::get('ETS_AM_ALLOW_WITHDRAW_LOYALTY_REWARDS');
        $total_earn_rewards = self::getTotalEarn('all', $id_customer, $context);
        $total_spent_rewards = self::getTotalSpent($id_customer, false, null, $context);
        $can_withdraw = $total_earn_rewards - $total_spent_rewards;
        if (!$allow_withdraw_loyalty) {
            $total_loyalty = self::getTotalEarn('loy', $id_customer, $context);
            if ($total_loyalty == $total_earn_rewards) {
                return 0;
            }
            $total_withdraw = Ets_Reward_Usage::getTotalSpent($id_customer, true, null, $context);
            $reward_without_loyalty = $total_earn_rewards - $total_loyalty;
            $temp = $reward_without_loyalty - $total_withdraw;
            if ($temp <= 0) {
                return 0;
            }
            $amount_left = $total_earn_rewards - $total_spent_rewards;
            if ($amount_left <= 0) {
                return 0;
            }
            if ($temp >= $amount_left) {
                $can_withdraw = $amount_left;
            } else {
                $can_withdraw = $temp;
            }
        }
        return $can_withdraw;
    }

    /**
     * @param null $id_customer
     * @param null $context
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function isCustomerHasPendingWithdrawal($id_customer = null, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if (!$id_customer) {
            $id_customer = $context->customer->id;
        }

        $sql = "SELECT COUNT(*) as total FROM  `" . _DB_PREFIX_ . "ets_am_reward_usage` eamru INNER JOIN `" . _DB_PREFIX_ . "ets_am_withdrawal` eaw ON eamru.id_withdraw = eaw.id_ets_am_withdrawal WHERE eaw.status=0 AND eamru.id_customer = " . (int)$id_customer . " AND eamru.id_shop = " . (int)$context->shop->id . " AND eamru.deleted = 0";
        $count = (int)Db::getInstance()->executeS($sql)[0]['total'];
        return $count > 0;
    }

    /**
     * @param $amount
     * @param $method_type
     * @param $id_method
     * @param null $context
     * @return bool|Ets_Reward_Usage|void
     */
    public static function useReward($amount, $method_type, $id_method, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $available = (float)self::calculateCustomerRewardCanUse($context->customer->id, $context);
        if ($available <= 0) {
            return;
        }
        $usage = new Ets_Reward_Usage();
        $usage->amount = $amount;
        $usage->id_customer = $context->customer->id;
        $usage->id_currency = $context->currency->id;
        $usage->id_shop = $context->shop->id;
        $usage->datetime_added = date('Y-m-d H:i:s');
        switch ($method_type) {
            case EAM_REWARD_USE_FOR_ORDER:
                $usage->id_order = $id_method;
                break;
            case EAM_REWARD_USE_FOR_VOUCHER:
                $usage->id_voucher = $id_method;
                break;
            case EAM_REWARD_USE_FOR_WITHDRAW:
                $usage->id_withdraw = $id_method;
                break;
        }
        return $usage->add() ? $usage : false;
    }


    /**
     * @param Cart $cart
     * @param $customer_id
     * @param null $context
     * @return bool
     * @throws Exception
     */
    public static function isCustomerCanUseReward(Cart $cart, $customer_id, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $minBalance = (float)self::getMinBalance();
        if ($minBalance > 0) {
            $minBalance = Ets_AM::convertPoint($minBalance, $context->currency->id, true);
        }
        $currentCustomerAvailable = (float)self::calculateCustomerRewardCanUse($customer_id, $context);
        $cartTotal = $cart->getOrderTotal();
        $cartTotal = Ets_AM::convertPoint($cartTotal);
        if (!$cartTotal) {
            return false;
        }
        if ($currentCustomerAvailable < $minBalance) {
            return false;
        }
        if ($currentCustomerAvailable < $cartTotal) {
            return false;
        }
        return true;
    }

    /**
     * Generate fixed promo code
     * @param $currencyAmount
     * @param array $data
     * @param null $context
     * @return bool|CartRule
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function createPromoCode($data = array())
    {
        if (!count($data)) {
            return false;
        }
        $promoCode = new CartRule();
        $promoCode->code = $data['code'];
        $languages = Language::getLanguages(false);
        if (!empty($languages)) {
            $cart_rule = array();
            foreach ($languages as $language) {
                $cart_rule[$language['id_lang']] = $data['name'];
            }
            $promoCode->name = $cart_rule;
        }
        $promoCode->id_customer = $data['id_customer'];
        $promoCode->date_from = $data['date_from'];
        $promoCode->date_to = $data['date_to'];
        $promoCode->quantity = $data['quantity'];
        $promoCode->reduction_amount = $data['reduction_amount'];
        $promoCode->reduction_currency = $data['reduction_currency'];
        $promoCode->active = $data['active'];
        return $promoCode->add() ? $promoCode : false;
    }

    /**
     *
     *  NOTE REMOVE AFTER CHECK {MVD NOTE}
     * @param $id_customer
     * @param bool $isAvailable
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getCustomerPromoCode($id_customer, $isAvailable = true, $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        $sql = "SELECT u.id_voucher,
                   cr.id_cart_rule,
                   crl.name,
                   cr.code,
                   cr.quantity,
                   cr.active,
                   cr.date_add
            FROM `" . _DB_PREFIX_ . "ets_am_reward_usage` u
            INNER JOIN `" . _DB_PREFIX_ . "cart_rule` cr ON u.id_voucher = cr.id_cart_rule
            INNER JOIN `" . _DB_PREFIX_ . "cart_rule_lang` crl ON cr.id_cart_rule = crl.id_cart_rule
            WHERE cr.id_customer = " . (int)$id_customer . "
            AND cr.active = 1 AND crl.id_lang = " . (int)$context->language->id;

        if ($isAvailable) {
            $sql .= " AND cr.quantity > 0";
        }
        $sql .= "  ORDER BY cr.id_cart_rule DESC";
        $results = Db::getInstance()->executeS($sql);
        return $results;
    }

    public static function getVouchers($page = null, $context = null, $id_customer = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        if (!$id_customer) {
            $id_customer = $context->customer->id;
        }
        $limit = 20;
        if ($p = Tools::getValue('page')) {
            if (Validate::isUnsignedInt($p))
                $page = $p;
            else
                $page = $page ? (int)$page : 1;
        } else {
            $page = $page ? (int)$page : 1;
        }
        $sql_where = "FROM `" . _DB_PREFIX_ . "cart_rule` cr
                LEFT JOIN `" . _DB_PREFIX_ . "cart_rule_lang` crl ON (cr.`id_cart_rule` = crl.`id_cart_rule` AND crl.`id_lang` = " . (int)$context->language->id . ")
                INNER JOIN `" . _DB_PREFIX_ . "ets_am_reward_usage` earu ON (cr.id_cart_rule = earu.id_voucher)
                WHERE cr.`id_customer` = " . (int)$id_customer . "
                AND cr.`active` = 1";
        $sql_total = "SELECT COUNT(*) FROM `" . _DB_PREFIX_ . "cart_rule` cr
                LEFT JOIN `" . _DB_PREFIX_ . "cart_rule_lang` crl ON (cr.`id_cart_rule` = crl.`id_cart_rule` AND crl.`id_lang` = " . (int)$context->language->id . ")
                INNER JOIN (SELECT MAX(id_ets_am_reward_usage) as id_ug FROM `"._DB_PREFIX_."ets_am_reward_usage` GROUP BY id_voucher) ug ON (cr.id_cart_rule = ug.id_ug)
                INNER JOIN `" . _DB_PREFIX_ . "ets_am_reward_usage` earu ON (ug.id_ug = earu.id_ets_am_reward_usage)
                WHERE cr.`id_customer` = " . (int)$id_customer . "
                AND cr.`active` = 1";
        $total = Db::getInstance()->getValue($sql_total);
        $total_page = ceil($total / $limit);
        $offset = ($page - 1) * $limit;
        $sql = "SELECT SQL_NO_CACHE * " . (string)$sql_where . " GROUP BY earu.`id_voucher` ORDER BY cr.id_cart_rule DESC LIMIT " . (int)$limit . " OFFSET " . (int)$offset;

        $results = Db::getInstance()->executeS($sql);
        $response = array();
        $response['current_page'] = $page;
        $response['total_page'] = $total_page;
        $response['results'] = $results;
        $response['total_data'] = $total;
        $response['per_page'] = $limit;
        return $response;
    }

    public static function updateUsageStatus($params)
    {
        $order_id = $params['id_order'];
        $newOrderStatus = $params['newOrderStatus'];
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "ets_am_reward_usage` WHERE `id_order` = " . (string)$order_id . " LIMIT 1";
        $result = Db::getInstance()->executeS($sql);
        if (count($result)) {
            $result = $result[0];
            $configStatus = Ets_AM::getConfigOrderStatus();
            foreach ($configStatus as $key => $status) {
                $date = date('Y-m-d H:i:s');
                $r = new Ets_Reward_Usage($result['id_ets_am_reward_usage']);
                if (in_array((int)$newOrderStatus->id, $status)) {
                    if ($key == 'pending') {
                        $s = 0;
                        $r->status = $s;
                        return $r->save();
                    } elseif ($key == 'validated') {
                        $s = 1;
                        $r->status = $s;
                        $r->datetime_validated = $date;
                        return $r->save();
                    } elseif ($key == 'canceled') {
                        $s = -1;
                        $r->status = $s;
                        $r->datetime_canceled = $date;
                        return $r->save();
                    }
                }
            }

        }
        return false;
    }

    public static function getTotalRemaining($id_customer = null,$program='all')
    {
        if ((int)$id_customer) {
            $total_point = self::getTotalEarn($program, $id_customer);
            $total_usage = self::getTotalSpent($id_customer,false,null,null,$program);
        } else {
            $total_point = self::getTotalEarn();
            $total_usage = self::getTotalSpent();
        }
        return $total_point - $total_usage;
    }

}
