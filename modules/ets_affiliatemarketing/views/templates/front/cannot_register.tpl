{*
* 2007-2021 ETS-Soft
*
* NOTICE OF LICENSE
*
* This file is not open source! Each license that you purchased is only available for 1 wesite only.
* If you want to use this file on more websites (or projects), you need to purchase additional licenses.
* You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please, contact us for extra customization service at an affordable price
*
*  @author ETS-Soft <contact@etssoft.net>
*  @copyright  2007-2021 ETS-Soft
*  @license    Valid for 1 website (or project) for each purchase of license
*  International Registered Trademark & Property of ETS-Soft
*}

{* {extends file='page.tpl'}
{block name='breadcrumb'}
    {$smarty.block.parent}
{/block}
{block name="page_content"}
    <div class="ets-am-register-program" style="min-height: 500px;">
    	<h1 style="text-align: center; margin-bottom: 20px;">{l s='Register affiliate programs' mod='ets_affiliatemarketing'}</h1>
    	<div class="alert alert-info">
    		<strong>{l s='Hi.' mod='ets_affiliatemarketing'}</strong> {if $flash_message}{$flash_message nofilter}{else}{l s='Welcome to Affiliate Marketing' mod='ets_affiliatemarketing'}{/if}
    	</div>
		{if isset($ets_am_flash_message)}
			<div class="alert alert-danger alert-dismissible est-am-alert" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
				</button>
				{$ets_am_flash_message nofilter}
			</div>
		{/if}
    </div>
{/block}
{block name="right_column"}{/block} *}