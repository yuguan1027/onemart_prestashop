<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please, contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <contact@etssoft.net>
 * @copyright  2007-2021 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

class Ets_affiliatemarketingSponsorshipModuleFrontController extends Ets_affiliatemarketingAllModuleFrontController
{

    public $auth = true;
    public $guestAllowed = false;
    public $authRedirection = URL_REF_PROGRAM;

    public function init()
    {
        parent::init();

        if (!$this->module->is17) {
            $this->display_column_left = false;
            $this->display_column_right = false;
        }
    }

    /**
     * @throws PrestaShopException
     */
    public function initContent()
    {
        parent::initContent();
        $friendly_url = (int)Configuration::get('PS_REWRITING_SETTINGS');
        $link_tab = array(
            'my_friends' =>  Ets_AM::getBaseUrlDefault('sponsorship'),
            'ref_friends' => Ets_AM::getBaseUrlDefault('refer_friends'),
        );
        //Set meta
        $page= 'module-'.$this->module->name.'-sponsorship';
        $meta = Meta::getMetaByPage($page,$this->context->language->id);
        $this->setMetas(array(
            'title' => isset($meta['title']) && $meta['title'] ? $meta['title'] : $this->module->l('My friends','sponsorship'),
            'keywords' => isset($meta['keywords']) && $meta['keywords'] ? $meta['keywords'] : $this->module->l('My friends','sponsorship'),
            'description' => isset($meta['description']) && $meta['description'] ? $meta['description'] : $this->module->l('My friends','sponsorship'),
        ));

        $this->context->smarty->assign(array(
            'link_tab' => $link_tab,
        ));
        if(Ets_Sponsor::isRefferalProgramReady()){
            $template ='sponsorship_reward_history.tpl';
            $customer = $this->context->customer;

            /* == Check state program ======*/
            $alert_type = '';
            $userExists = Ets_User::getUserByCustomerId($this->context->customer->id);

            if($userExists){
                if( $userExists['status'] == -1){
                    $alert_type = 'account_banned';
                }
                elseif($userExists['status'] > 0 && $userExists['ref'] == 1 ){
                    $alert_type = 'registered';
                }
                elseif($userExists['status'] > 0 && $userExists['ref'] == -1 ){
                    $alert_type = 'program_suspened';
                }
                elseif($userExists['status'] > 0 && $userExists['ref'] == -2 ){
                    $alert_type = 'program_declined';
                } 
                else {
                    $p = Ets_Participation::getProgramRegistered($this->context->customer->id, 'ref');
                    if($p){
                        if($p['status'] == 0){
                            $alert_type = 'register_success';
                        }
                        elseif($p['status'] == 1){
                            $alert_type = 'registered';
                        }
                        elseif($p['status'] < 0){
                            $alert_type = 'program_declined';
                        }
                    }
                    else{

                        if(Configuration::get('ETS_AM_REF_REGISTER_REQUIRED')){
                            $url_register = Ets_AM::getBaseUrlDefault('register',array('p'=>'ref'));
                            Tools::redirect($url_register);
                        }
                    }
                }
            } else{
                $p = Ets_Participation::getProgramRegistered($this->context->customer->id, 'ref');
                if($p){
                    if($p['status'] == 0){
                        $alert_type = 'register_success';
                    }
                    elseif($p['status'] == 1){
                        $alert_type = 'registered';
                    }
                    elseif($p['status'] < 0){
                        $alert_type = 'program_declined';
                    }
                }
                else{

                    if(Configuration::get('ETS_AM_REF_REGISTER_REQUIRED')){
                        $url_register = Ets_AM::getBaseUrlDefault('register',array('p'=>'ref'));
                        Tools::redirect($url_register);
                    }
                }
            }

            $message = '';
            if(!$alert_type){
                $res_data = Ets_Sponsor::canUseRefferalProgramReturn();
                if(!$res_data['success']){
                    $alert_type = 'need_condition';
                    $message = Configuration::get('ETS_AM_REF_MSG_CONDITION', $this->context->language->id) ? strip_tags(Configuration::get('ETS_AM_REF_MSG_CONDITION', $this->context->language->id)) : '';
                    if(isset($res_data['min_order']) && isset($res_data['total_order'])){
                        $message  = str_replace('[min_order_total]', Ets_affiliatemarketing::displayPrice(Tools::convertPrice($res_data['min_order'], $this->context->currency->id, true), $this->context->currency->id), $message);
                        $message  = str_replace('[total_past_order]', Ets_affiliatemarketing::displayPrice(Tools::convertPrice($res_data['total_order'], $this->context->currency->id, true), $this->context->currency->id), $message);
                        $message  = str_replace('[amount_left]', Ets_affiliatemarketing::displayPrice(Tools::convertPrice((float)$res_data['min_order'] - (float)$res_data['total_order'], $this->context->currency->id, true), $this->context->currency->id), $message);
                    }
                    elseif(isset($res_data['not_in_group'])){
                        $message = '';
                        //$message = $this->l('You are not in group customer to join this program', 'sponsorship');
                    }
                    if(!$message){
                        Tools::redirect($this->context->link->getPageLink('my-account', true));
                    }
                }
            }
            $this->context->smarty->assign(array(
                'alert_type' => $alert_type,
                'message' => $message
            ));
            /* == End Check state program ======*/
                
            if (Ets_Sponsor::isJoinedRef($customer->id)  && (!$alert_type || $alert_type == 'registered')) {
                $tab_active = '';
                if ($tab_active = Tools::getValue('tab', false)) {
                    if ($tab_active == 'how-to-refer-friends') {
                        //Set meta
                        $this->setMetas(array(
                            'title' => $this->module->l('How to refer friend','sponsorship'),
                            'keywords' => $this->module->l('How to refer friend','sponsorship'),
                            'description' => $this->module->l('How to refer friend','sponsorship'),
                        ));
                        $template = 'sponsorship_refer_friend.tpl';
                        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
                        $total_sent = Ets_Invitation::totalEmailInvited($customer->id);
                        $banner = Ets_Banner::getBanerByIdCustomer($customer->id);
                        $banner_img = '';
                        if ($banner && is_array($banner)) {
                            $banner_img = Ets_AM::getBaseUrl(true) . EAM_PATH_IMAGE_BANER . $banner['img'];
                        } else {
                            $banner_default = Configuration::get('ETS_AM_REF_DEFAULT_BANNER');
                            if ($banner_default) {
                                $banner_img = Ets_AM::getBaseUrl(true) . EAM_PATH_IMAGE_BANER . $banner_default;
                            }
                        }
                        $params = array(
                            'id_customer' => $customer->id
                        );
                        if ($page = (int)Tools::getValue('page')) {
                            $params['page'] = $page;
                        }
                        $max_email_invatation = Configuration::get('ETS_AM_REF_MAX_INVITATION');
                        if ($max_email_invatation) {
                            $max_email_invatation = (int)$max_email_invatation;
                        } else {
                            if (!$max_email_invatation && $max_email_invatation != '0') {
                                $max_email_invatation = 'unlimited';
                            }
                        }

                        $this->context->smarty->assign(array(
                            'ets_customer' => $customer,
                            'base_url' => $friendly_url ? Ets_AM::getBaseUrl().'?' : (strpos(Ets_AM::getBaseUrl(), '?') !== false ? Ets_AM::getBaseUrl().'&' : Ets_AM::getBaseUrl().'?'),
                            'total_email_sent' => $total_sent,
                            'max_invitation' => $max_email_invatation,
                            'invitation_left' => $max_email_invatation !== 'unlimited' ? ($max_email_invatation - $total_sent) : '',
                            'banner' => $banner_img,
                            'embed_code' => $this->getImgBanner(Ets_Banner::renderBannerCode($customer->id, $banner_img, 'refs')),
                            'explaination' => Configuration::get('ETS_AM_REF_TEXT_EXPLANATION', $default_lang),
                            'enable_invitation' => (int)Configuration::get('ETS_AM_REF_EMAIL_INVITE_FRIEND'),
                            'invitations' => Ets_Invitation::getInvitations($params),
                            'allow_upload_banner' => (int)Configuration::get('ETS_AM_REF_ALLOW_CUSTOM_BANNER'),
                            'resize_width' => Configuration::get('ETS_AM_RESIZE_BANNER_WITH'),
                            'resize_height' => Configuration::get('ETS_AM_RESIZE_BANNER_HEIGHT'),
                        ));
                    }
                }
                else {
                    $tab_active = 'my-friends';
                    if($id_customer = (int)Tools::getValue('id_customer'))
                    {
                        $template = 'sponsorship_customer.tpl';
                        $customer_info = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'customer` WHERE id_customer='.(int)$id_customer);
                        $customer_info['orders'] = Db::getInstance()->getValue('SELECT COUNT(DISTINCT o.id_order) FROM `'._DB_PREFIX_.'orders` o,'._DB_PREFIX_.'ets_am_reward r WHERE o.id_order=r.id_order AND o.id_customer='.(int)$id_customer.' AND program="ref" AND sub_program!="REG"');
                        $customer_info['level'] = Db::getInstance()->getValue('SELECT level FROM `'._DB_PREFIX_.'ets_am_sponsor` WHERE id_customer='.(int)$id_customer.' AND id_parent='.(int)$this->context->customer->id);
                        $customer_info['approved'] = Db::getInstance()->getValue('SELECT COUNT(DISTINCT o.id_order) FROM `'._DB_PREFIX_.'orders` o,'._DB_PREFIX_.'ets_am_reward r WHERE o.id_order=r.id_order AND o.id_customer='.(int)$id_customer.' AND r.status=1');
                        $customer_info['friends'] = Db::getInstance()->getValue('SELECT COUNT(DISTINCT id_customer) FROM `'._DB_PREFIX_.'ets_am_sponsor` WHERE id_parent='.(int)$id_customer);
                        $sql = 'SELECT *,o.id_currency as currency FROM `'._DB_PREFIX_.'orders` o,'._DB_PREFIX_.'ets_am_reward r WHERE o.id_order=r.id_order AND o.id_customer='.(int)$id_customer.' AND r.id_friend='.(int)$id_customer.' AND r.id_customer='.(int)$this->context->customer->id.' AND program="ref" AND sub_program!="REG"';
                        if(($order_sale_status = Tools::getValue('order_sale_status')) || $order_sale_status!=='')
                            $sql .= ' AND r.status='.(int)$order_sale_status;
                        if($type_date_filter = Tools::getValue('order_sale_filter'))
                        {
                            if ($type_date_filter == 'this_month') {
                                $sql .= " AND r.datetime_added >= '" . date('Y-m-01 00:00:00') . "' AND r.datetime_added <= '" . date('Y-m-t 23:59:59') . "'";
                            } else if ($type_date_filter == 'this_year') {
                                $sql .= " AND r.datetime_added >= '" . date('Y-01-01 00:00:00') . "' AND r.datetime_added <= '" . date('Y-12-31 23:59:59') . "'";
                            }
                        }
                        $sql .=' ORDER BY o.id_order DESC';
                        if($customer_info['level']==1)
                        {
                            $customer_info['price_register'] = (float)Db::getInstance()->getValue('SELECT amount FROM `'._DB_PREFIX_.'ets_am_reward` WHERE id_friend="'.(int)$id_customer.'" AND id_customer="'.(int)$this->context->customer->id.'" AND sub_program="REG" AND status=1');
                            if($customer_info['price_register'])
                                $customer_info['price_register'] = Ets_AM::displayReward($customer_info['price_register'],true);
                        } 
                        $customer_info['list_orders'] = Db::getInstance()->executeS($sql);
                        if($customer_info['list_orders'])
                        {
                            foreach($customer_info['list_orders'] as &$order)
                            {
                                $order['total_paid_tax_incl'] = Ets_affiliatemarketing::displayPrice($order['total_paid_tax_incl'],(int)$order['currency']);
                                $order['amount'] = Ets_AM::displayReward($order['amount']);
                            }
                        }
                        $this->context->smarty->assign(
                            array(
                                'customer_info' => $customer_info,
                                'ETS_AM_DISPLAY_ID_ORDER' => Configuration::get('ETS_AM_DISPLAY_ID_ORDER'),
                                'link_back' => Ets_AM::getBaseUrlDefault('sponsorship')
                            )
                        );
                    }
                    else
                    {
                        $template ='sponsorship_myfriend.tpl';
                        $params = array();
                        if ($page = (int)Tools::getValue('page', false)) {
                            if ((int)$page > 0) {
                                $params['page'] = (int)$page;
                            }
                        }
                        if (($order_by = Tools::getValue('orderBy', false)) && Validate::isCleanHtml($order_by) ) {
                            $params['order_by'] = $order_by;
                        }
                        if (($order_way = Tools::getValue('orderWay', false)) && Validate::isCleanHtml($order_way) ) {
                            $params['order_way'] = $order_way;
                        }
                        //Set meta
                        $this->setMetas(array(
                            'title' => $this->module->l('My friends','sponsorship'),
                            'keywords' => $this->module->l('My friends','sponsorship'),
                            'description' => $this->module->l('My friends','sponsorship'),
                        ));
                        $friends = Ets_Sponsor::getDetailSponsors($customer->id, $params, true);
                        $this->context->smarty->assign(array(
                            'friends' => $friends,
                            'query' => Tools::getAllValues(),
                            'display_email' => (int)Configuration::get('ETS_AM_REF_DISPLAY_EMAIL_SPONSOR'),
                            'display_name' => (int)Configuration::get('ETS_AM_REF_DISPLAY_NAME_SPONSOR'),
                        ));
                    }
                }
                $this->context->smarty->assign(array(
                    'template' => $template,
                    'tab_active' => $tab_active,
                    'query' => Tools::getAllValues()
                ));
            }
            else{
                if(!$alert_type){
                    $alert_type = 'register_success';
                }
                
                $template = 'my_friends.tpl';
                $this->context->smarty->assign(array(
                    'alert_type' => $alert_type,
                    'template' => $template,
                ));
            }
        }
        else{
            $this->context->smarty->assign(array(
                'alert_type' => 'disabled',
            ));
        };
        $voucher_code =Db::getInstance()->getRow('SELECT cr.code FROM `'._DB_PREFIX_.'cart_rule` cr 
        INNER JOIN `'._DB_PREFIX_.'ets_am_cart_rule_seller` crs ON (cr.id_cart_rule=crs.id_cart_rule)
        WHERE crs.id_customer ='.(int)$this->context->customer->id.' AND cr.quantity>0 AND cr.active=1 AND (cr.date_from="0000-00-00 00:00:00" OR cr.date_from <= "'.pSQL(date('Y-m-d H:i:s')).'") AND (cr.date_to="0000-00-00 00:00:00" OR cr.date_to >= "'.pSQL(date('Y-m-d H:i:s')).'")');
        if(Configuration::get('ETS_AM_SELL_APPLY_DISCOUNT'))
        {
            $discount_value = Configuration::get('ETS_AM_SELL_REDUCTION_PERCENT').'%';
        }
        elseif(Configuration::get('ETS_AM_SELL_APPLY_DISCOUNT'))
        {
            if($id_currency = Configuration::get('ETS_AM_SELL_ID_CURRENCY'))
                $currency = new Currency($id_currency);
            else
                $currency = $this->context->currency;
            $discount_value = Ets_affiliatemarketing::displayPrice(Configuration::get('ETS_AM_SELL_REDUCTION_AMOUNT'),$currency);
        }
        else
            $discount_value ='';
        $this->context->smarty->assign(
            array(
                'voucher_code' => $voucher_code['code'],
                'ETS_AM_SELL_OFFER_VOUCHER' => Configuration::get('ETS_AM_SELL_OFFER_VOUCHER',$this->context->language->id),
                'ETS_AM_REF_VOUCHER_CODE_DESC' => strip_tags(str_replace('[discount_value]',$discount_value,Configuration::get('ETS_AM_REF_VOUCHER_CODE_DESC',$this->context->language->id) ? Configuration::get('ETS_AM_REF_VOUCHER_CODE_DESC',$this->context->language->id) : $this->module->l('Share this voucher code to your friends. They will get [discount_value] off for their order and you will also get commission on your friends` orders.','sponsorship'))) 
            )
        );
        if ($this->module->is17) {
            $this->setTemplate('module:ets_affiliatemarketing/views/templates/front/sponsorship.tpl');
        } else {
            $this->setTemplate('sponsorship16.tpl');
        }
    }

    protected function sendMailInviting($mail_datas)
    {
        if ($mail_datas && isset($mail_datas['email']) && Validate::isEmail($mail_datas['email'])) {
            $name = $mail_datas['name'];
            $email = $mail_datas['email'];
            $language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
            $id_shop = $this->context->shop->id;
            $id_customer = $this->context->customer->id;
            //$friendly_url = (int)Configuration::get('PS_REWRITING_SETTINGS');
            //$link_ref = strpos(Ets_AM::getBaseUrl(), '?') === false ? Ets_AM::getBaseUrl() . 'index.php?refs=' . $id_customer : Ets_AM::getBaseUrl() . '&refs=' . $id_customer;
//            $link_ref = $friendly_url ? Ets_AM::getBaseUrl().'?' : (strpos(Ets_AM::getBaseUrl(), '?') !== false ? Ets_AM::getBaseUrl().'&' : Ets_AM::getBaseUrl().'?');
//            $link_ref = $link_ref.'refs=' . $id_customer;
            $link_ref = $this->context->link->getPageLink('authentication',null,null,array('create_account'=>1,'refs'=>$id_customer));
            //Check remail number emails can be send
            $total_sent = Ets_Invitation::totalEmailInvited();
            $max_email_invatation = Configuration::get('ETS_AM_REF_MAX_INVITATION');
            if ($max_email_invatation) {
                $max_email_invatation = (int)$max_email_invatation;
            } else {
                if (!$max_email_invatation && $max_email_invatation != '0') {
                    $max_email_invatation = 'unlimited';
                }
            }
            $total_left = 'unlimited';
            if ($max_email_invatation != 'unlimited') {
                $total_left = $max_email_invatation - $total_sent;
            }
            //Send mail
            if ($max_email_invatation == 'unlimited' || $total_left == 'unlimited' || $total_left > 0) {
                //$tmp_sent = $total_sent;
                $limited = false;
                //$mails_sent = array();
                //$mails_invited = array();
                $email_invited = Ets_Invitation::emailIsInvited($email);
                if (!$email_invited) {
                    if ($this->sendMail(array('email' => $email, 'name' => $name), $link_ref, $language->id, $id_shop)) {
                        die(Tools::jsonEncode(array(
                            'success' => true,
                            'message' => $this->module->l('Email sent successfully.', 'sponsorship'),
                            'limited' => $limited
                        )));
                    }
                    die(Tools::jsonEncode(array(
                        'success' => false,
                        'message' => $this->module->l('Could not send email. Please check your mail configuration or network and try again.', 'sponsorship'),
                        'limited' => $limited
                    )));

                } else {
                    die(Tools::jsonEncode(array(
                        'success' => false,
                        'message' => $this->module->l('Email ' . $email . ' has been registered. Please invite another friend', 'sponsorship'),
                        'limited' => $limited
                    )));
                }

            } else {
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->module->l('Your email invitations are limited', 'sponsorship'),
                    'limited' => true
                )));
            }
        }
        die(Tools::jsonEncode(array(
            'success' => false,
            'message' => $this->module->l('There was an error occurred while sending email', 'sponsorship'),
            'limited' => true
        )));
    }

    protected function sendMail($mail, $link_ref)
    {
        $email = $mail['email'];
        $username = $mail['name'];
        $data = array(
            '{email}' => $email,
            '{username}' => $username,
            '{your_friend}' => $this->context->customer->firstname.' '. $this->context->customer->lastname,
            '{link_ref}' => $link_ref
        );
        $subjects = array(
            'translation' => $this->module->l('You are invited to join us','sponsorship'),
            'origin'=> 'You are invited to join us',
            'specific'=>'sponsorship'
        );
        $mail_sent = Ets_aff_email::send(0,'invite_referral',$subjects,$data,array('customer'=>$email));
        //Add to database if sent success
        if ($mail_sent) {

            $id_friend = Ets_Invitation::getIdCustomerByEmail($email);

            $invitation = new Ets_Invitation();
            $invitation->email = $email;
            $invitation->name = $username;
            $invitation->datetime_sent = date('Y-m-d H:i:s');
            $invitation->id_friend = $id_friend;
            $invitation->id_sponsor = $this->context->customer->id;
            $invitation->add();
            return true;
        }
        return false;
    }

    protected function uploadBanner($banner)
    {
        if($banner['error'] <= 0){
            $allowExtentions = array('png', 'jpg', 'jpeg', 'gif');
            $imagesize = @getimagesize($banner['tmp_name']);
            $ext = Tools::strtolower(Tools::substr(strrchr($banner['name'], '.'), 1));
            $ext2 = Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1));
            if (in_array($ext, $allowExtentions) && in_array($ext2, $allowExtentions)){

                $id_customer = $this->context->customer->id;
                //$name = $banner["name"];
                
                if (!in_array($ext, $allowExtentions)) {
                    return false;
                }

                //create path
                Ets_AM::createPath(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER);

                $img_name = $id_customer . '.' . $ext;
                $path_img = _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER . $img_name;
                // Upload file
                $banner_exists = Ets_Banner::getBanerByIdCustomer($id_customer);
                $img_exists = $banner_exists && is_array($banner_exists) ? $banner_exists['img'] : '';
                $tmp_name = sha1(microtime()).$img_exists;
                if($img_exists){
                    rename(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER.$img_exists, _PS_ROOT_DIR_ . '/modules/'.$this->module->name.'/cache/'.$tmp_name);
                    @unlink(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER.$img_exists);
                }
                $success = false;
                if(!ImageManager::validateUpload($banner, 2097152)){
                    $temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                    if(move_uploaded_file($banner['tmp_name'], $temp_name)){
                        Ets_AM::createPath(_PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER);

                        $resize_with = null;
                        $resize_height = null;
                        if((int)Configuration::get('ETS_AM_RESIZE_BANNER')){
                            $resize_with = (int)Configuration::get('ETS_AM_RESIZE_BANNER_WITH');
                            $resize_height = (int)Configuration::get('ETS_AM_RESIZE_BANNER_HEIGHT');
                        }

                        if(ImageManager::resize($temp_name, $path_img, $resize_with, $resize_height, $ext)){
                            $success = true;
                            if (isset($temp_name)) {
                                @unlink($temp_name);
                            }
                            @unlink( _PS_ROOT_DIR_ . '/modules/'.$this->module->name.'/cache/'.$tmp_name);
                            if ($banner_exists && is_array($banner_exists)) {
                                $b = new Ets_Banner($banner_exists['id_ets_am_banner']);
                                if ($b) {
                                    $b->img = $img_name;
                                    $b->datetime_added = date('Y-m-d H:i:s');
                                    $b->update();
                                }

                            } else {
                                //Addd to database
                                $b = new Ets_Banner();
                                $b->id_sponsor = $id_customer;
                                $b->datetime_added = date('Y-m-d H:i:s');
                                $b->img = $img_name;
                                $b->save();
                            }

                            $path_img = Ets_AM::getBaseUrl(true) . EAM_PATH_IMAGE_BANER . $img_name;
                            die(Tools::jsonEncode(array(
                                'success' => true,
                                'message' => Ets_affiliatemarketing::$trans['banner_uploaded'],
                                'img' => $path_img,
                                'embed_code' => $this->getImgBanner(Ets_Banner::renderBannerCode($id_customer, $path_img, 'refs'))
                            )));
                        }
                        if (isset($temp_name)) {
                            @unlink($temp_name);
                        }
                    }

                }
                if(!$success && $img_exists){
                    rename( _PS_ROOT_DIR_ . '/modules/'.$this->module->name.'/cache/'.$tmp_name, _PS_ROOT_DIR_ . '/' . EAM_PATH_IMAGE_BANER.$img_exists);
                    @unlink(_PS_ROOT_DIR_ . '/modules/'.$this->module->name.'/cache/'.$tmp_name);
                    die(Tools::jsonEncode(array(
                        'success' => true,
                        'message' => Ets_affiliatemarketing::$trans['banner_uploaded'],
                        'img' => $path_img,
                        'embed_code' => $this->getImgBanner(Ets_Banner::renderBannerCode($id_customer, $path_img, 'refs'))
                    )));;
                }
            }
             die(Tools::jsonEncode(array(
                'success' => false,
                'message' => 'Banner uploaded fail.'
            )));
        }
        
    }

    public function deleteBanner($id_customer)
    {
        $banner = Ets_Banner::deleteBanner($id_customer);
        if ($banner !== false) {
            die(Tools::jsonEncode(array(
                'success' => true,
                'message' => $this->module->l('Sponsor banner deleted','sponsorship'),
                'img' => $banner,
                'embed_code' => $this->getImgBanner(Ets_Banner::renderBannerCode($id_customer, $banner, 'refs'))
            )));
        }
        die(Tools::jsonEncode(array(
            'success' => false,
            'message' => $this->module->l('You can\'t delete default banner.', 'sponsorship')
        )));
    }

    protected function getStatDataReward()
    {
        $params = array();
        if (($filter_status = Tools::getValue('filter_status')) && Validate::isCleanHtml($filter_status) ) {
            $params['status'] = $filter_status;
        }
        if (($filter_date_from = Tools::getValue('filter_date_from')) && Validate::isCleanHtml($filter_date_from) ) {
            $params['date_from'] = $filter_date_from;
        }
        if (($filter_date_to = Tools::getValue('filter_date_to')) && Validate::isCleanHtml($filter_date_to)  ) {
            $params['date_to'] = $filter_date_to;
        }
        $params['program'] = EAM_AM_REF_REWARD;
        $params['id_customer'] = $this->context->customer->id;
        $results = Ets_AM::getStatsRewardDetail($params);
        die(Tools::jsonEncode($results));
    }

    public function postProcess()
    {
        $customer = $this->context->customer;
        //Send mail invitation
        if (Tools::isSubmit('send_mail_invite')) {
            $token = Tools::getValue('token', false);
            if(!$token || $token !== md5($this->module->name.'-'.$this->module->version)){
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->module->l($token, 'sponsorship')
                )));
            }
            if ($mails_inviting = Tools::getValue('mails')) {
                $this->sendMailInviting($mails_inviting);
            } else {
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->module->l('Error', 'sponsorship')
                )));
            }
        }

        //Upload banner
        if (Tools::isSubmit('upload_banner')) {
            $token = Tools::getValue('token', false);
            if(!$token || $token !== md5($this->module->name.'-'.$this->module->version)){
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->module->l('Token is invalid', 'sponsorship')
                )));
            }
            if (isset($_FILES['banner'])) {
                $fileName = str_replace(' ', '', $_FILES['banner']['name']);
                if(!Validate::isFileName($fileName)){
                    die(Tools::jsonEncode(array(
                        'success' => false,
                        'message' => $this->module->l('File name is invalid', 'refer_friends')
                    )));
                }
                $this->uploadBanner($_FILES['banner']);
            }
            die(Tools::jsonEncode(array(
                'success' => false,
                'message' => $this->module->l('No files selected', 'sponsorship')
            )));
        }

        //Delete Banner
        if (Tools::isSubmit('delete_banner')) {
            $token = Tools::getValue('token', false);
            if(!$token || $token !== md5($this->module->name.'-'.$this->module->version)){
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->module->l('Token is invalid', 'sponsorship')
                )));
            }
            $this->deleteBanner($customer->id);
        }

        if (Tools::isSubmit('getStatDataReward')) {
            $this->getStatDataReward();
        }
        if(Tools::isSubmit('create_voucher_code_sell'))
        {
            if(Configuration::get('ETS_AM_SELL_OFFER_VOUCHER'))
            {
                $voucher_code =Db::getInstance()->getValue('SELECT cr.code FROM `'._DB_PREFIX_.'cart_rule` cr 
                INNER JOIN `'._DB_PREFIX_.'ets_am_cart_rule_seller` crs ON (cr.id_cart_rule=crs.id_cart_rule)
                WHERE crs.id_customer ='.(int)$this->context->customer->id.' AND cr.quantity>0 AND cr.active=1 AND (cr.date_from="0000-00-00 00:00:00" OR cr.date_from <= "'.pSQL(date('Y-m-d H:i:s')).'") AND (cr.date_to="0000-00-00 00:00:00" OR cr.date_to >= "'.pSQL(date('Y-m-d H:i:s')).'")');
                if(!$voucher_code)
                {
                    if($cartRuleObj = $this->module->saveCartRule())
                    {
                        $sql = 'INSERT INTO '._DB_PREFIX_.'ets_am_cart_rule_seller(id_customer,id_cart_rule,code,date_added) VALUES("'.(int)$this->context->customer->id.'","'.(int)$cartRuleObj->id.'","'.pSQL($cartRuleObj->code).'","'.pSQL(date('Y-m-d')).'")';
                        Db::getInstance()->execute($sql);
                        die(
                            Tools::jsonEncode(
                                array(
                                    'success' => true,
                                    'code' => $cartRuleObj->code,
                                )
                            )
                        );
                    }
                    else
                    {
                        die(
                            Tools::jsonEncode(
                                array(
                                    'success' => false,
                                    'error' => $this->module->l('Add cart rule error','sponsorship'),
                                )
                            )  
                        );
                    }
                }
                else
                {
                    die(
                        Tools::jsonEncode(
                            array(
                                'success' => false,
                                'error' => $this->module->l('Voucher code is exists','sponsorship'),
                            )
                        )  
                    );
                }
                
            }
        }
    }

    public function setMedia(){
        parent::setMedia();
        $this->addJs(_PS_MODULE_DIR_ . 'ets_affiliatemarketing/views/js/share_social.js');
    }

    public function getImgBanner($data = array()){
        $this->context->smarty->assign($data);
        $html = $this->context->smarty->fetch(_PS_MODULE_DIR_.'ets_affiliatemarketing/views/templates/front/img_banner.tpl');
        return trim(preg_replace('/<!--(.*)-->/Uis', '', $html), "\r\n");
    }
}