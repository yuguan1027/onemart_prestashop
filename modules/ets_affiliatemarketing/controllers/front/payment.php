<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please, contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <contact@etssoft.net>
 * @copyright  2007-2021 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

class Ets_affiliatemarketingPaymentModuleFrontController extends Ets_affiliatemarketingAllModuleFrontController
{
    public function init()
    {
        parent::init();
         $this->setMetas(array(
            'title' => $this->module->l('Payment','payment'),
            'keywords' => $this->module->l('Payment','payment'),
            'description' => $this->module->l('Payment','payment'),
        ));
        if (!$this->module->is17)
        {
            $this->display_column_left = false;
            $this->display_column_right = false;
        }
    }
    public function initContent()
    {
        parent::initContent();
        $context = $this->context;
        $cart = $context->cart;
        if (! $this->module->checkCurrency($cart)) {
            Tools::redirect('index.php?controller=order');
        }
        $total = sprintf(
            $this->getTranslator()->trans('%1$s (tax incl.)', array(), 'Modules.Ets_affiliatemarketing.Shop'),
            Ets_affiliatemarketing::displayPrice($cart->getOrderTotal(true, Cart::BOTH))
        );
        $this->context->smarty->assign(array(
            'back_url' => $this->context->link->getPageLink('order', true, NULL, "step=3"),
            'confirm_url' => $this->context->link->getModuleLink('ets_affiliatemarketing', 'validation', array(), true),
            'cust_currency' => $cart->id_currency,
            'currencies' => $this->module->getCurrency((int)$cart->id_currency),
            'total' => $total,
            'this_path' => $this->module->getPathUri(),
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/',
        ));

        $this->setTemplate('payment_execution.tpl');
    }
}
