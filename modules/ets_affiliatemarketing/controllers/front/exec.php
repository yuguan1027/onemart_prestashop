<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses. 
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please contact us for extra customization service at an affordable price
 *
 *  @author ETS-Soft <contact@etssoft.net>
 *  @copyright  2007-2021 ETS-Soft
 *  @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

class Ets_affiliatemarketingExecModuleFrontController extends ModuleFrontController
{
	public function initContent(){
		parent::initContent();
		if(Tools::isSubmit('save_cookie_ref_popup')){
			$token = Tools::getValue('token', false);
            if(!$token || $token !== md5($this->module->name.'-'.$this->module->version)){
                die(Tools::jsonEncode(array(
                    'success' => false,
                    'message' => $this->module->l('Token is invalid', 'exec')
                )));
            }
			 $this->context->cookie->__set('eam_ref_delay_popup', date('Y-m-d'));
			 die(Tools::jsonEncode(array(
			 	'success' => true
			 )));
		}
		if($this->module->is17){
            $this->setTemplate('module:ets_affiliatemarketing/views/templates/front/empty.tpl');
        }
        else{
            $this->setTemplate('empty.tpl');
        }
	}
}