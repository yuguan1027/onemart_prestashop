<?php
/**
 * 2007-2021 ETS-Soft
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 wesite only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please, contact us for extra customization service at an affordable price
 *
 * @author ETS-Soft <contact@etssoft.net>
 * @copyright  2007-2021 ETS-Soft
 * @license    Valid for 1 website (or project) for each purchase of license
 *  International Registered Trademark & Property of ETS-Soft
 */

class Ets_affiliatemarketingAffiliateModuleFrontController extends Ets_affiliatemarketingAllModuleFrontController
{
    /**
     * @var bool
     */
    public $auth = true;
    public $guestAllowed = false;
    public $authRedirection;

    public function init()
    {
        parent::init();
        $this->authRedirection = Ets_AM::getBaseUrlDefault('affiliate');
        if (!$this->module->is17) {
            $this->display_column_left = false;
            $this->display_column_right = false;
        }
    }

    /**
     * @throws PrestaShopException
     */
    public function initContent()
    {
        parent::initContent();
        $this->setMetas(array(
            'title' => $this->module->l('Affiliate program','affiliate'),
            'keywords' => $this->module->l('Affiliate program','affiliate'),
            'description' => $this->module->l('Affiliate program','affiliate'),
        ));
        if (!$this->module->is17) {
            $this->context->smarty->assign(array('controller' => 'affiliate'));
        }
        $sales = Ets_Affiliate::getSales($this->context);
        $template = 'affiliate_program.tpl';
        $valid = false;
        if (!Configuration::get('ETS_AM_AFF_ENABLED')) {
            $this->context->smarty->assign(array(
                'alert_type' => 'DISABLED'
            ));
        } else {
            if (Ets_Affiliate::isCustomerCanJoinAffiliateProgram()) {
                $reg_status = Ets_AM::customerJoinProgramStatus(EAM_AM_AFFILIATE_REWARD, $this->context);
                if (Configuration::get('ETS_AM_AFF_REGISTER_REQUIRED')) {
                    if (count($reg_status)) {
                        if ($reg_status['key'] == 'REGISTER') {
                            $url_register = Ets_AM::getBaseUrlDefault('register',array('p'=>EAM_AM_AFFILIATE_REWARD));
                            Tools::redirect($url_register);
                        }
                        $valid = $reg_status['valid'];
                        $key = $reg_status['key'];
                    }
                    $this->context->smarty->assign(array(
                        'alert_type' => $key
                    ));
                } else {
                    if (!$reg_status['valid']) {
                        if ($reg_status['key'] == 'BANNED') {
                            $this->context->smarty->assign(array(
                                'alert_type' => 'BANNED'
                            ));
                        }
                    } else {
                        $valid = true;
                    }
                }

            } else {
                $this->context->smarty->assign(array(
                    'alert_type' => 'NEED_CONDITION'
                ));
            }
        }
        $this->context->smarty->assign(array(
            'valid' => $valid
        ));
        $this->context->smarty->assign(array(
            'aff_product_url' => Ets_AM::getBaseUrlDefault('affiliate'),
            'history_url' => Ets_AM::getBaseUrlDefault('affiliate'),
            'my_sale_url' => Ets_AM::getBaseUrlDefault('my_sale'),
            'ets_am_sales' => $sales,
            'template' => $template,
            'controller' => 'affiliate',
        ));

        if ($valid) {
            $reward_history = EtsAmAdmin::getRewardHistory($this->context->customer->id, 'aff', true, true);
            $query = $reward_history['query'];
            unset($reward_history['query']);
            $this->context->smarty->assign(array(
                'valid' => $valid,
                'query' => $query,
                'reward_history' => $reward_history,
            ));
        }

        if ($this->module->is17) {
            $this->setTemplate('module:ets_affiliatemarketing/views/templates/front/_partials/affiliate_layout.tpl');
        } else {
            $this->setTemplate('_partials/affiliate_layout16.tpl');
        }
    }


    /**
     * @param $key
     * @return string
     */
    public function flash($key)
    {
        $flash = '';
        if ($this->context->cookie->__get($key)) {
            $flash = $this->context->cookie->__get($key);
            $this->context->cookie->__set($key, null);
        }
        return $flash;
    }

    protected function getStatDataReward()
    {
        $params = array();
        if (($filter_status = Tools::getValue('filter_status')) && Validate::isCleanHtml($filter_status) ) {
            $params['status'] = $filter_status;
        }
        if (($filter_date_from = Tools::getValue('filter_date_from')) && Validate::isCleanHtml($filter_date_from) ) {
            $params['date_from'] = $filter_date_from;
        }
        if (($filter_date_to = Tools::getValue('filter_date_to')) && Validate::isCleanHtml($filter_date_to) ) {
            $params['date_to'] = $filter_date_to;
        }
        $params['program'] = EAM_AM_AFFILIATE_REWARD;
        $params['id_customer'] = $this->context->customer->id;
        $results = Ets_AM::getStatsRewardDetail($params);
        die(Tools::jsonEncode($results));
    }

}