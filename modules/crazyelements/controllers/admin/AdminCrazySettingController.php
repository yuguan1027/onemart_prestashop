<?php
defined( '_PS_VERSION_' ) or exit;
require_once _PS_MODULE_DIR_ . 'crazyelements/PrestaHelper.php';
require_once _PS_MODULE_DIR_ . 'crazyelements/includes/plugin.php';
use CrazyElements\PrestaHelper;
class AdminCrazySettingController extends AdminController {


	public $dirpaths                     = array();
	public $json_file_name               = '';
	public $svg_file_name                = '';
	public $new_json                     = array();
	public $custom_icon_upload_font_name = '';
	public $text_file_name               = 'fontarray.txt';
	public $new_json_file_name           = 'fontarray.json';
	public $folder_name                  = '';
	public $first_icon_name              = '';
	public function __construct() {
		$this->context   = Context::getContext();
		$this->bootstrap = true;
		$this->table     = 'configuration';
		parent::__construct();
	}

	public function renderList() {
		$check_yes           = '';
		$check_no            = '';
		$page_title_selector = PrestaHelper::get_option( 'page_title' );
		if ( PrestaHelper::get_option( 'presta_editor_enable' ) == 'yes' ) {
			$check_yes = 'checked = checked';
			$check_no  = '';
		}
		if ( PrestaHelper::get_option( 'presta_editor_enable', 'no' ) == 'no' ) {
			$check_no  = 'checked = checked';
			$check_yes = '';
		}
		$content_check_yes = '';
		$content_check_no  = '';
		if ( PrestaHelper::get_option( 'crazy_content_disable' ) == 'yes' ) {
			$content_check_yes = 'checked = checked';
			$content_check_no  = '';
		}
		if ( PrestaHelper::get_option( 'crazy_content_disable', 'no' ) == 'no' ) {
			$content_check_no  = 'checked = checked';
			$content_check_yes = '';
		}


		$fromhtml = '
        <form action="" id="configuration_form_1" method="post" enctype="multipart/form-data" class="form-horizontal"> 
        <div class="panel " id="configuration_fieldset_cache"> 
            <div class="panel-heading"> <i class="icon-cogs"></i> Clear Cache for Crazy</div>
            <div class="form-wrapper"> 
                <div class="form-group"> 
                    <div id="conf_id_crazy_clear_cache"> 
                        <label class="control-label col-lg-3"> Clear Cache </label> 
                        <div class="col-lg-9"> <span class="switch prestashop-switch fixed-width-lg"> <input type="radio" name="crazy_clear_cache" id="crazy_clear_cache_on" value="1"><label for="crazy_clear_cache_on" class="radioCheck">Yes</label><input type="radio" name="crazy_clear_cache" id="crazy_clear_cache_off" value="0" checked="checked"><label for="crazy_clear_cache_off" class="radioCheck">No</label> <a class="slide-button btn"></a> </span> </div>
                        <div class="col-lg-9 col-lg-offset-3"> <div class="help-block"> If your css is not working clearing cache might help. </div></div>
                    </div>
                 </div>
            </div>
            <div class="panel-footer"> <button type="submit" class="btn btn-default pull-right" name="crazy_clear_cache_submit"><i class="process-icon-save"></i> Clear Cache</button> 
            </div>
        </div>
        </form>
        <form action="" id="configuration_form_4" method="post" enctype="multipart/form-data" class="form-horizontal"> 
        <div class="panel " id="configuration_fieldset_page_title_selector">
         <div class="panel-heading"> <i class="icon-cogs"></i> General Settings</div>
         <div class="form-wrapper"> 
         <div class="form-group">
          <div id="conf_id_page_title"> <label class="control-label col-lg-3"> Page Title Selector </label> 
          <div class="col-lg-9"><input class="form-control " type="text" size="5" name="page_title" value="' . $page_title_selector . '"> </div>
          <div class="col-lg-9 col-lg-offset-3"> 
          </div>
          </div>
          </div>
        <div class="form-group"> 
            <div id="conf_presta_editor_enable"> 
                <label class="control-label col-lg-3"> Enable Presta Editor </label> 
                <div class="col-lg-9"> <span class="switch prestashop-switch fixed-width-lg"> 
                <input type="radio" name="presta_editor_enable" id="presta_editor_enable_on" value="1" ' . $check_yes . '>
                <label for="presta_editor_enable_on" class="radioCheck">Yes</label>
                <input type="radio" name="presta_editor_enable" id="presta_editor_enable_off" value="0"  ' . $check_no . '>
                <label for="presta_editor_enable_off" class="radioCheck">No</label> <a class="slide-button btn"></a> </span> 
                </div>
                <div class="col-lg-9 col-lg-offset-3"> <div class="help-block"> Enable or disable Prestashop default editor </div></div>
            </div>
        </div>
        <div class="form-group"> 
            <div id="conf_crazy_content_disable"> 
                <label class="control-label col-lg-3"> Disable Crazyelements Content </label> 
                <div class="col-lg-9"> <span class="switch prestashop-switch fixed-width-lg"> 
                <input type="radio" name="crazy_content_disable" id="crazy_content_disable_on" value="1" ' . $content_check_yes . '>
                <label for="crazy_content_disable_on" class="radioCheck">Yes</label>
                <input type="radio" name="crazy_content_disable" id="crazy_content_disable_off" value="0"  ' . $content_check_no . '>
                <label for="crazy_content_disable_off" class="radioCheck">No</label> <a class="slide-button btn"></a> </span> 
                </div>
                <div class="col-lg-9 col-lg-offset-3"> <div class="help-block"> Enable or disable Crazyelements content in front </div></div>
            </div>
        </div>
        <div class="panel-footer"> <button type="submit" class="btn btn-default pull-right" name="page_title_submit"><i class="process-icon-save"></i> Add Title Selector</button> </div>
          </div> 
          </div> 
        </form>
        
        <form action="" id="configuration_form_2" method="post" enctype="multipart/form-data" class="form-horizontal"> 
        <div class="panel " id="configuration_fieldset_replace"> <div class="panel-heading"> <i class="icon-cogs"></i> Update Site Address</div><div class="form-wrapper"> <div class="form-group"> <div id="conf_id_crazy_old_url"> <label class="control-label col-lg-3"> Old Url </label> <div class="col-lg-9"><input class="form-control " type="text" size="5" name="crazy_old_url" value=""> </div><div class="col-lg-9 col-lg-offset-3"> <div class="help-block"> Enter your old URL. </div></div></div></div><div class="form-group"> <div id="conf_id_crazy_new_url"> <label class="control-label col-lg-3"> New Url </label> <div class="col-lg-9"><input class="form-control " type="text" size="5" name="crazy_new_url" value=""> </div><div class="col-lg-9 col-lg-offset-3"> <div class="help-block"> Enter your new URL. </div></div></div></div></div><div class="panel-footer"> <button type="submit" class="btn btn-default pull-right" name="crazy_url_submit"><i class="process-icon-save"></i> Replace URL</button> </div></div>
        </form>';
		$html     = parent::renderList() . $fromhtml;
		return $html;
	}

	public function initContent() {
		
		PrestaHelper::get_lience_expired_date();
		if ( Tools::isSubmit( 'page_title' ) ) {
			$page_title = Tools::getValue( 'page_title' );
			PrestaHelper::update_option( 'page_title', $page_title );
			$presta_editor_enable = Tools::getValue( 'presta_editor_enable' );
			if ( $presta_editor_enable == '1' ) {
				$presta_editor_enable = 'yes';
			} else {
				$presta_editor_enable = 'no';
			}
			PrestaHelper::update_option( 'presta_editor_enable', $presta_editor_enable );
			$crazy_content_disable = Tools::getValue( 'crazy_content_disable' );
			if ( $crazy_content_disable == '1' ) {
				$crazy_content_disable = 'yes';
			} else {
				$crazy_content_disable = 'no';
			}
			PrestaHelper::update_option( 'crazy_content_disable', $crazy_content_disable );
			Tools::redirectAdmin( $this->context->link->getAdminLink( 'AdminCrazySetting' ) );
		}
		if ( Tools::isSubmit( 'mailchimp_data' ) ) {
			$mailchimp_data = Tools::getValue( 'mailchimp_data' );
			PrestaHelper::update_option( 'mailchimp_data', $mailchimp_data );
			Tools::redirectAdmin( $this->context->link->getAdminLink( 'AdminCrazySetting' ) );
		}
		if ( Tools::isSubmit( 'crazy_clear_cache' ) ) {
			$crazy_clear_cache = Tools::getValue( 'crazy_clear_cache' );
			if ( $crazy_clear_cache ) {
				$this->clear_cache();
				Tools::redirectAdmin( $this->context->link->getAdminLink( 'AdminCrazySetting' ) );
			}
		}
		if ( Tools::isSubmit( 'crazy_old_url' ) && Tools::isSubmit( 'crazy_new_url' ) ) {
			$from = ! empty( Tools::getValue( 'crazy_old_url' ) ) ? Tools::getValue( 'crazy_old_url' ) : '';
			$to   = ! empty( Tools::getValue( 'crazy_new_url' ) ) ? Tools::getValue( 'crazy_new_url' ) : '';
			$this->replace_urls( $from, $to );
			$this->clear_cache();
		}
		parent::initContent();
	}

	public function initHeader() {
		parent::initHeader();
	}

	public function clear_cache() {
		$files = glob( _PS_MODULE_DIR_ . 'crazyelements/assets/css/frontend/css/*' ); // get all file names
		foreach ( $files as $file ) { // iterate files
			if ( is_file( $file ) ) {
				unlink( $file ); // delete file
			}
		}
		Db::getInstance()->delete( 'crazy_options', "option_name like '_elementor_css%'  OR option_name ='_elementor_global_css'" );
		Db::getInstance()->delete( 'crazy_options', "option_name ='elementor_remote_info_library'" );
		$admincontroller = Tools::getValue( 'controller' );
		$token           = Tools::getValue( 'token' );
		Configuration::updateValue( 'crazy_clear_cache', 0 );
	}

	public function replace_urls( $from, $to ) {
		if ( $from === $to ) {
			throw new \Exception( PrestaHelper::__( 'The `from` and `to` URL\'s must be different', 'elementor' ) );
		}
		$is_valid_urls = ( filter_var( $from, FILTER_VALIDATE_URL ) && filter_var( $to, FILTER_VALIDATE_URL ) );
		if ( ! $is_valid_urls ) {
			throw new \Exception( PrestaHelper::__( 'The `from` and `to` URL\'s must be valid URL\'s', 'elementor' ) );
		}
		Db::getInstance()->update(
			'crazy_content_lang',
			array(
				'resource' => array(
					'type'  => 'sql',
					'value' => "REPLACE(`resource`, '" . str_replace(
						'/',
						'\\\/',
						$from
					) . "','" . str_replace(
						'/',
						'\\\/',
						$to
					) . "')",
				),
			),
			"`resource` LIKE '[%' "
		);
	}
}
