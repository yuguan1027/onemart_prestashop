<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowHelper
{
    public static function createProduct()
    {
        $idProduct = null;
        $product = new Product();
        if ($idProduct = Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID')) {
            $product = new Product((int) $idProduct);
            if (!Validate::isLoadedObject($product)) {
                $product = new Product();
            }
        }
        $product->price = 0;
        $languages = Language::getLanguages();
        foreach ($languages as $language) {
            $product->name[$language['id_lang']] = 'Pay Later';
        }
        $product->reference = 'Pay_Later';
        $product->id_category_default = 2;
        $product->is_virtual = 1;
        $product->visibility = 'none';
        $product->id_tax_rules_group = 0;
        $product->quantity = 100000000;
        if ($product->save()) {
            $idProduct = $product->id;
            Configuration::updateValue(
                'PRESTA_BUY_NOW_PRODUCT_ID',
                $idProduct
            );
            $image_dir = _PS_MODULE_DIR_.'prestabuynowpaylater/views/img/default_logo.jpg';
            if (file_exists($image_dir)) {
                $image_obj = new Image();
                if (Configuration::get('PRESTA_BUY_NOW_COVER_IMG')) {
                    $image_obj = new Image(Configuration::get('PRESTA_BUY_NOW_COVER_IMG'));
                }
                $image_obj->id_product = $idProduct;
                $image_obj->position = 1;
                $image_obj->cover = 1;
                $image_obj->save();
                Configuration::updateValue('PRESTA_BUY_NOW_COVER_IMG', $image_obj->id);
                $imagesTypes = ImageType::getImagesTypes('products');
                if ($imagesTypes) {
                    foreach ($imagesTypes as $image_type) {
                        ImageManager::resize(
                            $image_dir,
                            $image_obj->getPathForCreation().'-'.$image_type['name'].'.jpg',
                            $image_type['width'],
                            $image_type['height']
                        );
                    }
                }
                ImageManager::resize(
                    $image_dir,
                    $image_obj->getPathForCreation().'.jpg'
                );
            }
        }
        return $idProduct;
    }

    public static function updateCartQty($idCart, $idProduct)
    {
        return Db::getInstance()->update(
            'cart_product',
            array(
                'quantity' => 1
            ),
            'id_cart = '.(int) $idCart.' AND `id_product` = '.(int)$idProduct
        );
    }

    public static function deleteCartProduct($idCart, $idProduct)
    {
        return Db::getInstance()->update(
            'cart_product',
            array(
                'quantity' => 1
            ),
            'id_cart = '.(int) $idCart.' AND `id_product` = '.(int)$idProduct
        );
    }

    public static function updateCustomerGroupRestriction()
    {
        $grp = array();
        $groups = Group::getGroups(Configuration::get('PS_LANG_DEFAULT'), true);
        if ($groups) {
            foreach ($groups as $group) {
                $grp[] = $group['id_group'];
            }
        }
        Configuration::updateValue('PRESTA_CUSTOMER_GROUP', serialize($grp));

        Configuration::updateValue('PRESTA_PAYLATER_ENABLE', 1);
        Configuration::updateValue('PRESTA_PAYLATER_ALL_CUSTOMER', 0);
        Configuration::updateValue('PRESTA_PAYLATER_GLOBAL_LIMIT', 1000);
        Configuration::updateValue('PRESTA_PAYLATER_ELIGIBLE', 1);
        Configuration::updateValue('PRESTA_PAYLATER_ORDER_RESTRICTION', 0);
        Configuration::updateValue('PRESTA_PAYLATER_ORDER_RESTRICTION_NO', 1);
        Configuration::updateValue('PRESTA_PAYLATER_LOAD_MORE_PRODUCT', 50);

        Configuration::updateValue('PRESTA_PAYLATER_DUE_DATE', 10);
        Configuration::updateValue('PRESTA_PAYLATER_LATE_FEE', 1);
        Configuration::updateValue('PRESTA_PAYLATER_LATE_FEE_TYPE', 1);
        Configuration::updateValue('PRESTA_PAYLATER_LATE_FEE_VALUE', 2);
        Configuration::updateValue('PRESTA_PAYLATER_STOP_AFTER_LATE_FEE', 0);

        $orderStatus = array(2);
        Configuration::updateValue('PRESTA_PAYLATER_PAYMENT_ORDER_STATUS', serialize($orderStatus));

        Configuration::updateValue('PRESTA_PAYLATER_ORDER_MAIL_TO_CUST', 1);
        Configuration::updateValue('PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN', 1);
        Configuration::updateValue('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_CUST', 1);
        Configuration::updateValue('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_ADMIN', 1);
        Configuration::updateValue('PRESTA_PAYLATER_WARNING_MAIL_TO_CUST', 1);
        Configuration::updateValue('PRESTA_PAYLATER_WARNING_MAIL_BEFORE_DUE_DATE', 5);
        Configuration::updateValue('PRESTA_PAYLATER_WARNING_MAIL_TIME', 1);

        $notAllowed = array(0);
        Configuration::updateValue('PRESTA_CUSTOMER_GROUP', serialize($notAllowed));
        Configuration::updateValue('PRESTA_CATEGORY_RESTRICTION', serialize($notAllowed));

        Configuration::updateValue('PRESTA_PAYLATER_FAQ', 0);
        Configuration::updateValue('PRESTA_PAYLATER_CMS_PAGE', 3);
        return true;
    }
}
