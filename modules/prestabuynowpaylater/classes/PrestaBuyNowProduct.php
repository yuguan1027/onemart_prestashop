<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowProduct extends ObjectModel
{
    public $id_product;
    public $active;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'presta_buynow_allowed_product',
        'primary' => 'id',
        'fields' => array(
            'id_product' => array('type' => self::TYPE_INT, 'required' => true),
            'active' => array('type' => self::TYPE_INT),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
        ),
    );

    public function checkProduct($idProduct, $onlyActive = false)
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'presta_buynow_allowed_product WHERE id_product = '.(int)$idProduct;
        if ($onlyActive) {
            $sql .= ' AND active = 1';
        }
        return Db::getInstance()->getRow($sql);
    }

    public function getRecords()
    {
        return Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'presta_buynow_allowed_product WHERE 1');
    }

    public function getProducts()
    {
        return Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'products WHERE 1');
    }
}
