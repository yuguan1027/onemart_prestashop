<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowCustomer extends ObjectModel
{
    public $id_customer;
    public $paylater_limit;
    public $active;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'presta_buynow_customer_configuration',
        'primary' => 'id',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'required' => true),
            'paylater_limit' => array('type' => self::TYPE_FLOAT, 'required' => true),
            'active' => array('type' => self::TYPE_INT, 'required' => true),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
        ),
    );

    public function getCustomerDetailByIdCustomer($idCustomer, $onlyActive = false)
    {
        $sql = 'SELECT * FROM '._DB_PREFIX_.'presta_buynow_customer_configuration
            WHERE `id_customer` = '.(int)$idCustomer;
        if ($onlyActive) {
            $sql .= ' AND active = 1';
        }
        return Db::getInstance()->getRow($sql);
    }
}
