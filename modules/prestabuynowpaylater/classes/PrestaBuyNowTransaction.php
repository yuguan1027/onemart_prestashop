<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowTransaction extends ObjectModel
{
    public $id_customer;
    public $id_cart;
    public $id_order;
    public $id_currency;
    public $dr_amount;
    public $cr_amount;
    public $transaction_for;
    public $status;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'presta_buynow_customer_transaction',
        'primary' => 'id',
        'fields' => array(
            'id_customer' => array('type' => self::TYPE_INT, 'required' => true),
            'id_cart' => array('type' => self::TYPE_INT, 'required' => true),
            'id_order' => array('type' => self::TYPE_INT),
            'id_currency' => array('type' => self::TYPE_INT, 'required' => true),
            'dr_amount' => array('type' => self::TYPE_FLOAT, 'required' => true),
            'cr_amount' => array('type' => self::TYPE_FLOAT, 'required' => true),
            'transaction_for' => array('type' => self::TYPE_INT),
            'status' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
        ),
    );

    public function isCartExist($idCart)
    {
        return Db::getInstance()->getRow(
            'SELECT * FROM '._DB_PREFIX_.'presta_buynow_customer_transaction WHERE `id_cart`= '.(int) $idCart
        );
    }

    public function getRecordByIdCustomer($idCustomer)
    {
        return Db::getInstance()->executeS(
            'SELECT reference as reference, pb.* FROM '._DB_PREFIX_.'presta_buynow_customer_transaction pb
            LEFT JOIN '._DB_PREFIX_.'orders o on (o.`id_cart` = pb.`id_cart`)
                WHERE pb.`id_customer`= '.(int) $idCustomer.' AND status = 1 ORDER BY date_add DESC'
        );
    }

    public function getUnbilledTotalByIdCustomer($idCustomer)
    {
        return Db::getInstance()->getValue(
            'SELECT SUM(`dr_amount`) - SUM(`cr_amount`) as unbilled
                FROM '._DB_PREFIX_.'presta_buynow_customer_transaction
                    WHERE `id_customer`= '.(int) $idCustomer.' AND status = 1'
        );
    }

    public function getAllRecords()
    {
        return Db::getInstance()->executeS(
            'SELECT * FROM '._DB_PREFIX_.'presta_buynow_customer_transaction'
        );
    }

    public function getAllUniqueRecords()
    {
        return Db::getInstance()->executeS(
            'SELECT DISTINCT id_customer FROM '._DB_PREFIX_.'presta_buynow_customer_transaction'
        );
    }

    public function getAllUniqueWithDetails()
    {
        return Db::getInstance()->executeS(
            'SELECT DISTINCT pt.id_customer, CONCAT(c.`firstname`, \' \', c.`lastname`) AS `customer`
                FROM '._DB_PREFIX_.'presta_buynow_customer_transaction pt
                INNER JOIN '._DB_PREFIX_.'customer c on (c.`id_customer` = pt.`id_customer`)'
        );
    }

    public function getProductsByIdCart($idCart)
    {
        return Db::getInstance()->executeS(
            'SELECT od.product_name as name, o.total_paid as total  FROM '._DB_PREFIX_.'order_detail od
                INNER JOIN '._DB_PREFIX_.'orders o on (o.`id_order` = od.`id_order`)
                WHERE o.id_cart = '.(int)$idCart
        );
    }

    public function getRecordsByIdCart($idCart)
    {
        return Db::getInstance()->getRow(
            'SELECT * FROM '._DB_PREFIX_.'presta_buynow_customer_transaction pb
                WHERE pb.`id_cart`= '.(int) $idCart.' AND status = 1'
        );
    }
}
