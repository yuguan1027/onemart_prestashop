<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

include_once 'classes/PrestaBuyNowClasses.php';

class PrestaBuyNowPayLater extends PaymentModule
{
    const INSTALL_SQL_FILE = 'db.sql';
    public function __construct()
    {
        $this->name = 'prestabuynowpaylater';
        $this->tab = 'payments_gateways';
        $this->version = '7.1.0';
        $this->author = 'presta_world';
        $this->bootstrap = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->module_key = 'ac0fe5e92332be3713a1bc16aaa0bce2';
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
        $this->confirmUninstall = $this->l('Do you want to uninstall this module?');
        parent::__construct();
        $this->displayName = $this->l('Presta Buy Now Pay Later');
        $this->description = $this->l('Allow your customers to pay later for the product which they can buy now');
    }

    public function hookActionObjectOrderUpdateAfter($params)
    {
        $controller = Tools::getValue('controller');
        if (isset($controller)) {
            if (Tools::getValue('controller') == 'AdminPrestaBuyNowCustomerTransaction') {
                return;
            }
        }
        $cartTotal = (float) $params['object']->total_paid;
        $buyNowTransaction = new PrestaBuyNowTransaction();
        if (isset($this->context->employee) && isset($this->context->employee->id)) {
            if ($params['object']->payment == 'Presta Buy Now Pay Later' && $this->context->employee->id) {
                $buyNowData = $buyNowTransaction->isCartExist($params['object']->id_cart);
                if ($buyNowData) {
                    $buyNowTransactionUpdate = new PrestaBuyNowTransaction($buyNowData['id']);
                    $buyNowTransactionUpdate->dr_amount = $cartTotal;
                    $buyNowTransactionUpdate->update();
                }
            }
        }
    }

    public function hookActionObjectOrderAddBefore($params)
    {
        $controller = Tools::getValue('controller');
        if (isset($controller)) {
            if (Tools::getValue('controller') == 'AdminPrestaBuyNowCustomerTransaction') {
                return;
            }
        }
        if ($params['object']->module == 'prestabuynowpaylater') {
            $buyNowTransaction = new PrestaBuyNowTransaction();
            $cartTotal = (float) $params['object']->total_paid;
            $unbilled = $buyNowTransaction->getUnbilledTotalByIdCustomer($params['object']->id_customer);
            $limit = $this->checkCustomerLimit($params['object']->id_customer);
            $balance = $limit - $unbilled;
            if ($cartTotal > $balance) {
                Tools::redirectAdmin(
                    $this->context->link->getAdminLink(
                        'AdminPrestaBuyNowCustomerTransaction',
                        true,
                        array(),
                        array(
                        'conf' => '119'
                        )
                    )
                );
            }
        }
    }

    public function hookActionObjectOrderDeleteAfter($params)
    {
    }

    public function hookActionObjectOrderAddAfter($params)
    {
        $controller = Tools::getValue('controller');
        if (isset($controller)) {
            if (Tools::getValue('controller') == 'AdminPrestaBuyNowCustomerTransaction') {
                return;
            }
        }
        $cartTotal = (float) $params['object']->total_paid;
        $buyNowTransaction = new PrestaBuyNowTransaction();
        if (isset($this->context->employee) && isset($this->context->employee->id)) {
            if ($params['object']->payment == 'Presta Buy Now Pay Later' && $this->context->employee->id) {
                $buyNowTransaction->id_customer = $params['object']->id_customer;
                $buyNowTransaction->id_cart = $params['object']->id_cart;
                $buyNowTransaction->id_currency = $params['object']->id_currency;
                $buyNowTransaction->dr_amount = $cartTotal;
                $buyNowTransaction->cr_amount = 0;
                $buyNowTransaction->transaction_for = 1; // Buying from PayLater
                $buyNowTransaction->status = 1;
                if ($buyNowTransaction->save()) {
                    if (Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_CUST')) {
                        $this->sendOrderConfirmationEmailToCustomer(
                            $params['object']->id_customer,
                            $params['object']->id_cart
                        );
                    }
                    if (Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN')) {
                        $this->sendOrderConfirmationEmailToAdmin(
                            $params['object']->id_customer,
                            $params['object']->id_cart
                        );
                    }
                }
            }
        }
    }

    public function hookDisplayAdminProductsExtra($params)
    {
        $idProduct = Tools::getValue('id_product');
        if (!$idProduct) {
            $idProduct = $params['id_product'];
        }
        $prestaBuyNowProduct = new PrestaBuyNowProduct();
        $isExist = $prestaBuyNowProduct->checkProduct($idProduct);
        if ($isExist) {
            $this->context->smarty->assign(
                array(
                    'prestaBuyNowProduct' => $isExist
                )
            );
        }
        if ($idProduct) {
            $this->context->smarty->assign(
                array(
                    'id_product' => $idProduct
                )
            );
        }
        return $this->display(__FILE__, 'presta_paylater_product.tpl');
    }

    public function hookActionProductSave($params)
    {
        $idProduct = Tools::getValue('id_product');
        if (!$idProduct) {
            $idProduct = $params['id_product'];
        }
        if ($idProduct) {
            $prestaBuyNowProduct = new PrestaBuyNowProduct();
            $isExist = $prestaBuyNowProduct->checkProduct($idProduct);
            if ($isExist) {
                $prestaBuyNowProduct = new PrestaBuyNowProduct($isExist['id']);
            }
            $prestaBuyNowProduct->id_product = $idProduct;
            $prestaBuyNowProduct->active = Tools::getValue('presta_paylater_enable');
            $prestaBuyNowProduct->save();
        }
    }

    public function hookActionFrontControllerSetMedia()
    {
        // var_dump($this->registerHook('actionObjectOrderAddBefore'));
        if (Tools::getValue('controller') == 'product' &&
            Configuration::get('PRESTA_PAYLATER_ELIGIBLE') &&
            Configuration::get('PRESTA_PAYLATER_ENABLE')
        ) {
            $this->context->controller->registerStyleSheet(
                'modules-paylater-css',
                'modules/'.$this->name.'/views/css/front/presta_paylater.css'
            );
        }
    }

    public function hookDisplayProductPriceBlock($params)
    {
        if ($params['type'] == 'after_price' &&
            Configuration::get('PRESTA_PAYLATER_ELIGIBLE') &&
            Configuration::get('PRESTA_PAYLATER_ENABLE')
        ) {
            $idProduct = Tools::getValue('id_product');
            if (!$this->checkProductAccess($idProduct)) {
                return;
            }

            $idCustomer = $this->context->customer->id;
            if (!Configuration::get('PRESTA_PAYLATER_ALL_CUSTOMER')) {
                if (!$this->checkCustomerAccess($idCustomer)) {
                    return;
                }
            }

            return $this->display(__FILE__, 'presta_eligible_display.tpl');
        }
    }

    public function hookDisplayCustomerAccount()
    {
        if (Configuration::get('PRESTA_PAYLATER_ENABLE')) {
            $idCustomer = $this->context->customer->id;
            if (!Configuration::get('PRESTA_PAYLATER_ALL_CUSTOMER')) {
                if (!$this->checkCustomerAccess($idCustomer)) {
                    return;
                }
            }
            return $this->display(__FILE__, 'presta_buynow_account.tpl');
        }
    }

    private function checkProductAccess($idProduct)
    {
        $allow = false;
        $allowedCategories = Configuration::get('PRESTA_CATEGORY_RESTRICTION');
        if ($allowedCategories) {
            $product = new Product($idProduct, false, $this->context->language->id);
            $productCategory = $product->getCategories();
            $allowedCategories = unserialize($allowedCategories);
            if ($allowedCategories) {
                foreach ($allowedCategories as $cat) {
                    if (in_array($cat, $productCategory)) {
                        $allow = true;
                        break;
                    }
                }
            }
        }
        if (!$allow) {
            $objProduct = new PrestaBuyNowProduct();
            if (!$objProduct->checkProduct($idProduct, true)) {
                return false;
            } else {
                $allow = true;
            }
        }
        return $allow;
    }

    public function checkCustomerAccess($idCustomer)
    {
        $objCustomer = new PrestaBuyNowCustomer();
        if ($idCustomer) {
            $isExist = $objCustomer->getCustomerDetailByIdCustomer($idCustomer, true);
            if ($isExist) {
                if ($isExist['active']) {
                    return true;
                } else {
                    return false;
                }
            }

            if ($this->checkCustomerGroup($idCustomer)) {
            // die('kjfdkjsf8778k');
                return false;
            }
        }

        if (Configuration::get('PRESTA_PAYLATER_ALL_CUSTOMER')) {
            return true;
        }
        return false;
    }

    private function checkCustomerGroup($idCustomer)
    {
        $customer = new Customer($idCustomer);
        $groups = $customer->getGroups();
        $allowedGroup = Configuration::get('PRESTA_CUSTOMER_GROUP');
        if ($allowedGroup) {
            $allowedGroup = unserialize($allowedGroup);
            if ($allowedGroup) {
                foreach ($allowedGroup as $value) {
                    if (in_array($value, $groups)) {
                        return true;
                    }
                }
            }
        }
    }

    private function checkCustomerLimit($idCustomer)
    {
        $objPrestaBuyNowCustomer = new PrestaBuyNowCustomer();
        $customerLimit = $objPrestaBuyNowCustomer->getCustomerDetailByIdCustomer($idCustomer);
        if ($customerLimit) {
            $limit = $customerLimit['paylater_limit'];
        } elseif ($this->checkCustomerGroup($idCustomer)) {
            $limit = Configuration::get('PRESTA_PAYLATER_GLOBAL_LIMIT');
        } elseif (Configuration::get('PRESTA_PAYLATER_ALL_CUSTOMER')) {
            $limit = Configuration::get('PRESTA_PAYLATER_GLOBAL_LIMIT');
        } else {
            $limit = 0;
        }
        return $limit;
    }

    public function hookDisplayOverrideTemplate($params)
    {
        if ($params['template_file'] == 'checkout/checkout' && Configuration::get('PRESTA_PAYLATER_ENABLE')) {
            $idCart = $this->context->cart->id;
            if ($idCart) {
                $cartProducts = $this->context->cart->getProducts();
                if (count($cartProducts) == 1) {
                    foreach ($cartProducts as $product) {
                        if (Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID') == $product['id_product']) {
                            $paymentOptionsFinder = new PaymentOptionsFinder();
                            $conditions = new ConditionsToApproveFinder(
                                $this->context,
                                $this->getTranslator()
                            );
                            $cartRules = $this->context->cart->getCartRules();
                            if ($cartRules) {
                                foreach ($cartRules as $rules) {
                                    $this->context->cart->removeCartRule($rules['id_cart_rule']);
                                }
                            }
                            CartRule::autoRemoveFromCart();
                            $this->context->smarty->assign(
                                array(
                                    'payment_options' => $paymentOptionsFinder->present(),
                                    'conditions_to_approve' => $conditions->getConditionsToApproveForTemplate(),
                                    'cart_totoal' => Tools::displayPrice($this->context->cart->getOrderTotal())
                                )
                            );
                            return 'module:'.$this->name.'/views/templates/front/presta_payment.tpl';
                        }
                    }
                }
            }
        }
    }

    public function hookActionCartSave($params)
    {
        if ($params['cart']) {
            $cartProducts = $params['cart']->getProducts();
            if (count($cartProducts) > 1) {
                foreach ($cartProducts as $product) {
                    if (Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID') == $product['id_product']) {
                        $this->context->cart->deleteProduct($product['id_product'], 0);
                    }
                }
            }
        }
    }

    public function hookActionValidateOrder($params)
    {
        $products = $params['order']->getProducts();
        if ($products) {
            foreach ($products as $product) {
                if ($product['product_id'] == Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID')) {
                    $buyNowTransaction = new PrestaBuyNowTransaction();
                    $buyNowTransaction->id_customer = $params['order']->id_customer;
                    $buyNowTransaction->id_cart = $params['cart']->id;
                    $buyNowTransaction->id_currency = $params['order']->id_currency;
                    $buyNowTransaction->dr_amount = 0;
                    $buyNowTransaction->cr_amount = $params['order']->total_paid;
                    $buyNowTransaction->transaction_for = 2; // Paid Unbilled Amount
                    $buyNowTransaction->status = 0; // Paid Unbilled Amount
                    if ($buyNowTransaction->save()) {
                        if (Configuration::get('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_CUST')) {
                            $this->sendPaymentConfirmationEmailToCustomer(
                                $params['order']->id_customer,
                                $params['cart']->id
                            );
                        }
                        if (Configuration::get('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_ADMIN')) {
                            $this->sendPaymentConfirmationEmailToAdmin(
                                $params['order']->id_customer,
                                $params['cart']->id
                            );
                        }
                    }
                    break;
                }
            }
        }
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
        // Check customer/customer group restriction
        $idCustomer = $this->context->customer->id;
        if (!$this->checkCustomerAccess($idCustomer)) {
            return;
        }
        // Check product/category restriction
        $cartProducts = $this->context->cart->getProducts();
        foreach ($cartProducts as $product) {
            if (Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID') == $product['id_product']) {
                return;
            }
            if (!$this->checkProductAccess($product['id_product'])) {
                return;
            }
        }

        $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
        $unbilled = $objPrestaBuyNowTransaction->getUnbilledTotalByIdCustomer($idCustomer);
        if ($unbilled) {
            $dueDate = Configuration::get('PRESTA_PAYLATER_NEXT_DUE_DATE');
            if (strtotime(date('Y-m-d') > strtotime($dueDate)) &&
                Configuration::get('PRESTA_PAYLATER_STOP_AFTER_LATE_FEE')
            ) {
                return;
            }
        }
        $cartTotal = $this->context->cart->getOrderTotal();
        $limit = $this->checkCustomerLimit($idCustomer);
        $balance = $limit - $unbilled;
        if ($cartTotal > $balance) {
            return;
        }
        $this->context->smarty->assign(
            array(
                'module_dir' => _MODULE_DIR_
            )
        );
        $newOption = new PaymentOption();
        $newOption->setCallToActionText($this->l('Buy Now Pay Later'))
            ->setAction($this->context->link->getModuleLink($this->name, 'process', array(), true))
            ->setModuleName($this->name);

        return array($newOption);
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $currentOrderStatus = $params['newOrderStatus']->id;
        $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
        $order = new Order($params['id_order']);
        $isExist = $objPrestaBuyNowTransaction->isCartExist($order->id_cart);
        if ($isExist) {
            // dump($isExist);die('5455');

            if (!$isExist['status'] && $isExist['transaction_for'] == 2) {
                // dump($params);die('fjjdlkjs');

                $validOrderStatus = Configuration::get('PRESTA_PAYLATER_PAYMENT_ORDER_STATUS');
                if ($validOrderStatus) {
                    $validOrderStatus = unserialize($validOrderStatus);
                    if ($validOrderStatus) {
                        if (in_array($currentOrderStatus, $validOrderStatus)) {
                            $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction($isExist['id']);
                            $objPrestaBuyNowTransaction->status = 1;
                            $objPrestaBuyNowTransaction->update();
                        }
                    }
                }
            }
        }
    }

    public function sendOrderConfirmationEmailToCustomer($idCustomer, $idCart)
    {
        $order = Order::getByCartId($idCart);
        $customer = new Customer((int) $idCustomer);
        $email = $customer->email;

        $limit = $this->checkCustomerLimit($idCustomer);
        $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
        $unbilled = $objPrestaBuyNowTransaction->getUnbilledTotalByIdCustomer($idCustomer);
        $balance = $limit - $unbilled;

        $customer_vars = array(
            '{name}' => $customer->firstname.' '.$customer->lastname,
            '{email}' => $email,
            '{order_reference}' => $order->reference,
            '{allowed_limit}' => Tools::displayPrice(
                $limit,
                new Currency($order->id_currency)
            ),
            '{used_amount}' => Tools::displayPrice(
                $unbilled,
                new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
            ),
            '{balance}' => Tools::displayPrice(
                $balance,
                new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
            ),
            '{my_account_link}' => $this->context->link->getModuleLink($this->name, 'transaction')
        );

        if ($email && Validate::isEmail($email)) {
            Mail::Send(
                (int) $this->context->language->id,
                'presta_order_conf_customer',
                Mail::l('Order Confirmed', (int) $this->context->language->id),
                $customer_vars,
                $email,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_.$this->name.'/mails/',
                false,
                null,
                null
            );
        }
    }

    public function sendOrderConfirmationEmailToAdmin($idCustomer, $idCart)
    {
        $employees = Employee::getEmployees();
        $order = Order::getByCartId($idCart);
        $customer = new Customer((int) $idCustomer);
        $email = $customer->email;

        $limit = $this->checkCustomerLimit($idCustomer);
        $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
        $unbilled = $objPrestaBuyNowTransaction->getUnbilledTotalByIdCustomer($idCustomer);
        $balance = $limit - $unbilled;

        $customer_vars = array(
            '{name}' => $customer->firstname.' '.$customer->lastname,
            '{email}' => $email,
            '{order_reference}' => $order->reference,
            '{allowed_limit}' => Tools::displayPrice(
                $limit,
                new Currency($order->id_currency)
            ),
            '{used_amount}' => Tools::displayPrice(
                $unbilled,
                new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
            ),
            '{balance}' => Tools::displayPrice(
                $balance,
                new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
            ),
        );
        if ($employees) {
            foreach ($employees as $employee) {
                $emp = new Employee($employee['id_employee']);
                if (Validate::isLoadedObject($emp)) {
                    $empEmail = $emp->email;
                    break;
                }
            }
        }
        if ($empEmail && Validate::isEmail($empEmail)) {
            Mail::Send(
                (int) $this->context->language->id,
                'presta_order_conf_admin',
                Mail::l('Order Placed', (int) $this->context->language->id),
                $customer_vars,
                $empEmail,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_.$this->name.'/mails/',
                false,
                null,
                null
            );
        }
    }

    public function sendPaymentConfirmationEmailToCustomer($idCustomer, $idCart)
    {
        $order = Order::getByCartId($idCart);
        $customer = new Customer((int) $idCustomer);
        $email = $customer->email;
        $status = new OrderState($order->current_state, $this->context->language->id);

        $customer_vars = array(
            '{name}' => $customer->firstname.' '.$customer->lastname,
            '{email}' => $email,
            '{order_reference}' => $order->reference,
            '{order_status}' => $status->name,
            '{payment_methpod}' => $order->payment,
            '{total_paid}' => Tools::displayPrice(
                $order->total_paid,
                new Currency($order->id_currency)
            ),
        );

        if ($email && Validate::isEmail($email)) {
            Mail::Send(
                (int) $this->context->language->id,
                'presta_payment_conf_customer',
                Mail::l('Payment Process', (int) $this->context->language->id),
                $customer_vars,
                $email,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_.$this->name.'/mails/',
                false,
                null,
                null
            );
        }
    }

    public function sendPaymentConfirmationEmailToAdmin($idCustomer, $idCart)
    {
        $employees = Employee::getEmployees();
        $order = Order::getByCartId($idCart);
        $customer = new Customer((int) $idCustomer);
        $status = new OrderState($order->current_state, $this->context->language->id);
        $customer_vars = array(
            '{name}' => $customer->firstname.' '.$customer->lastname,
            '{email}' => $customer->email,
            '{order_reference}' => $order->reference,
            '{order_status}' => $status->name,
            '{payment_methpod}' => $order->payment,
            '{total_paid}' => Tools::displayPrice(
                $order->total_paid,
                new Currency($order->id_currency)
            ),
        );
        if ($employees) {
            foreach ($employees as $employee) {
                $emp = new Employee($employee['id_employee']);
                if (Validate::isLoadedObject($emp)) {
                    $empEmail = $emp->email;
                    break;
                }
            }
        }
        if ($empEmail && Validate::isEmail($empEmail)) {
            Mail::Send(
                (int) $this->context->language->id,
                'presta_payment_conf_admin',
                Mail::l('Awaiting Payment Status', (int) $this->context->language->id),
                $customer_vars,
                $empEmail,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_.$this->name.'/mails/',
                false,
                null,
                null
            );
        }
    }

    public function sendWarningEmailToCustomer($idCustomer)
    {
        $customer = new Customer((int) $idCustomer);
        $email = $customer->email;

        $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
        $unbilled = $objPrestaBuyNowTransaction->getUnbilledTotalByIdCustomer($idCustomer);

        $dueDate = date('d-M-Y', strtotime(Configuration::get('PRESTA_PAYLATER_NEXT_DUE_DATE')));

        $firstDay =  date("d-M-Y", strtotime("first day of previous month", strtotime($dueDate)));
        $lastDay = date("d-M-Y", strtotime("last day of previous month", strtotime($dueDate)));

        $to = $this->l('To');
        $billDate = $firstDay.' '.$to.' '.$lastDay;
        $customer_vars = array(
            '{name}' => $customer->firstname.' '.$customer->lastname,
            '{email}' => $email,
            '{due_date}' => $dueDate,
            '{bill_date}' => $billDate,
            '{pending_amount}' => Tools::displayPrice(
                $unbilled,
                new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
            ),
            '{my_account_link}' => $this->context->link->getModuleLink($this->name, 'transaction')
        );

        if ($email && Validate::isEmail($email)) {
            Mail::Send(
                (int) $this->context->language->id,
                'presta_warning_customer',
                Mail::l('Payment Process', (int) $this->context->language->id),
                $customer_vars,
                $email,
                null,
                null,
                null,
                null,
                null,
                _PS_MODULE_DIR_.$this->name.'/mails/',
                false,
                null,
                null
            );
        }
    }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getContent()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminPrestaBuyNowConfiguration'));
    }

    public function callInstallTab()
    {
        $this->installTab('AdminPrestaBuyNowPayLater', 'Buy Now Pay Later');

        $this->installTab('AdminPrestaBuyNow', 'Buy Now Pay Later', 'AdminPrestaBuyNowPayLater');
        $this->installTab('AdminPrestaBuyNowConfiguration', 'Configuration', 'AdminPrestaBuyNow');

        $this->installTab('AdminPrestaBuyNowTransaction', 'Transactions', 'AdminPrestaBuyNow');
        $this->installTab('AdminPrestaBuyNowOrderTransaction', 'Order Transaction', 'AdminPrestaBuyNowTransaction');
        $this->installTab(
            'AdminPrestaBuyNowCustomerTransaction',
            'Customer Transaction',
            'AdminPrestaBuyNowTransaction'
        );

        $restriction = 'AdminPrestaBuyNowRestriction';
        $this->installTab($restriction, 'Manage Pay Later', 'AdminPrestaBuyNow');
        $this->installTab('AdminPrestaBuyNowProductRestriction', 'Allowed Product(s)', $restriction);
        $this->installTab('AdminPrestaBuyNowCategoryRestriction', 'Allowed Category(s)', $restriction);
        $this->installTab('AdminPrestaBuyNowCustomerGroupRestriction', 'Allowed Customer Group(s)', $restriction);
        $this->installTab('AdminPrestaBuyNowCustomerRestriction', 'Customer(s)', $restriction);

        return true;
    }

    public function installTab($class_name, $tab_name, $tab_parent_name = false)
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->icon = 'payment';
        $tab->class_name = $class_name;
        $tab->name = array();
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $tab_name;
        }

        if ($tab_parent_name) {
            $tab->id_parent = (int) Tab::getIdFromClassName($tab_parent_name);
        } else {
            $tab->id_parent = 0;
        }

        $tab->module = $this->name;
        return $tab->add();
    }

    private function installOrderStatus()
    {
        if (Configuration::get('PS_OS_PRESTA_BUYNOW') < 1) {
            $orderState = new OrderState();
            $orderState->name = array();
            foreach (Language::getLanguages() as $language) {
                $orderState->name[$language['id_lang']] = $this->l('Buy Now Pay Later');
            }
            $orderState->send_email = false;
            $orderState->color = "red";
            $orderState->hidden = false;
            $orderState->delivery = false;
            $orderState->logable = true;
            $orderState->invoice = false;
            $orderState->shipped = false;
            $orderState->paid = false;
            $orderState->module_name = $this->name;
            if ($orderState->add()) {
                $source = dirname(__FILE__).'/views/img/presta_status.gif';
                $destination = dirname(__FILE__).'/../../img/os/'.(int)$orderState->id.'.gif';
                copy($source, $destination);
                Configuration::updateValue('PS_OS_PRESTA_BUYNOW', (int)$orderState->id);
            } else {
                return false;
            }
        }
        return true;
    }

    private function prestaRegisterHooks()
    {
        return $this->registerHook(
            array(
                'displayHeader',
                'actionCartSave',
                'paymentOptions',
                'actionProductSave',
                'actionValidateOrder',
                'displayPaymentReturn',
                'displaycustomerAccount',
                'displayOverrideTemplate',
                'displayProductPriceBlock',
                'displayAdminProductsExtra',
                'actionOrderStatusPostUpdate',
                'actionFrontControllerSetMedia',
                'actionObjectAddAfter',
                'actionObjectOrderAddAfter',
                'actionObjectOrderUpdateAfter',
                'actionObjectOrderDeleteAfter',
                'actionObjectOrderAddBefore'
            )
        );
    }

    private function createProduct()
    {
        $idProduct = PrestaBuyNowHelper::createProduct();
        if ($idProduct) {
            StockAvailable::updateQuantity($idProduct, '', 10000000);
            return true;
        }

        return false;
    }

    private function updateConfiguration()
    {
        return PrestaBuyNowHelper::updateCustomerGroupRestriction();
    }

    public function install()
    {
        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            return (false);
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            return (false);
        }

        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(
            _DB_PREFIX_,
            _MYSQL_ENGINE_
        ), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);
        foreach ($sql as $query) {
            if ($query) {
                if (!Db::getInstance()->execute(trim($query))) {
                    return false;
                }
            }
        }
        if (!parent::install()
            || !$this->createProduct()
            || !$this->installOrderStatus()
            || !$this->updateConfiguration()
            || !$this->callInstallTab()
            || !$this->prestaRegisterHooks()
        ) {
            return false;
        }

        return true;
    }

    private function deleteConfigurationValue()
    {
        $config = array(
            'PRESTA_BUY_NOW_PRODUCT_ID', 'PRESTA_BUY_NOW_COVER_IMG',
            'PRESTA_CUSTOMER_GROUP', 'PRESTA_PAYLATER_ENABLE',
            'PRESTA_CUSTOMER_GROUP', 'PRESTA_PARTIAL_ENABLE',
            'PRESTA_PAYLATER_ALL_CUSTOMER', 'PRESTA_PAYLATER_GLOBAL_LIMIT',
            'PRESTA_PAYLATER_ELIGIBLE', 'PRESTA_PAYLATER_ORDER_RESTRICTION',
            'PRESTA_PAYLATER_ORDER_RESTRICTION_NO', 'PRESTA_PAYLATER_LOAD_MORE_PRODUCT',
            'PRESTA_PAYLATER_DUE_DATE', 'PRESTA_PAYLATER_LATE_FEE',
            'PRESTA_PAYLATER_LATE_FEE_TYPE', 'PRESTA_PAYLATER_LATE_FEE_VALUE',
            'PRESTA_PAYLATER_STOP_AFTER_LATE_FEE', 'PRESTA_PAYLATER_PAYMENT_ORDER_STATUS',
            'PRESTA_PAYLATER_ORDER_MAIL_TO_CUST', 'PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN',
            'PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_CUST', 'PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_ADMIN',
            'PRESTA_PAYLATER_WARNING_MAIL_TO_CUST', 'PRESTA_PAYLATER_WARNING_MAIL_BEFORE_DUE_DATE',
            'PRESTA_PAYLATER_NEXT_DUE_DATE', 'PRESTA_PAYLATER_FAQ', 'PRESTA_PAYLATER_CMS_PAGE'
        );

        foreach ($config as $key) {
            Configuration::deleteByName($key);
        }

        return true;
    }

    private function dropTable()
    {
        return Db::getInstance()->execute(
            'DROP TABLE IF EXISTS
                '._DB_PREFIX_.'presta_buynow_allowed_product,
                '._DB_PREFIX_.'presta_buynow_customer_transaction,
                '._DB_PREFIX_.'presta_buynow_customer_configuration'
        );
    }

    public function uninstallTab()
    {
        $moduleTabs = Tab::getCollectionFromModule($this->name);
        if (!empty($moduleTabs)) {
            foreach ($moduleTabs as $moduleTab) {
                $moduleTab->delete();
            }
        }

        return true;
    }

    private function deleteProduct()
    {
        $product = new Product((int) Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID'));
        if (Validate::isLoadedObject($product)) {
            if (!$product->delete()) {
                return false;
            }
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()
            || !$this->deleteProduct()
            || !$this->dropTable()
            || !$this->uninstallTab()
            || !$this->deleteConfigurationValue()
        ) {
            return false;
        }

        return true;
    }
}
