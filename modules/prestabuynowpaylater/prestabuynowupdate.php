<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include_once 'prestabuynowpaylater.php';

class PrestaBuyNowUpdate
{
    public function updateBuyNowPayLater()
    {
        $objPrestaBuyNowPayLater = new PrestaBuyNowPayLater();
        if (Tools::substr(Tools::encrypt($objPrestaBuyNowPayLater->name.'/cron'), 0, 10) != Tools::getValue('token') ||
            !Module::isInstalled($objPrestaBuyNowPayLater->name)
        ) {
            die('Bad Token');
        }
        $dueDate = Configuration::get('PRESTA_PAYLATER_NEXT_DUE_DATE');
        if (strtotime(date('Y-m-d')) > strtotime($dueDate) && Configuration::get('PRESTA_PAYLATER_LATE_FEE')) {
            $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
            $allCustomers = $objPrestaBuyNowTransaction->getAllUniqueRecords();
            if ($allCustomers) {
                $feeValue = Configuration::get('PRESTA_PAYLATER_LATE_FEE_VALUE');
                foreach ($allCustomers as $customer) {
                    $details = $objPrestaBuyNowTransaction->getUnbilledTotalByIdCustomer($customer['id_customer']);
                    if ($details > 0) {
                        // Update late fee into data base
                        if (strtotime(date('Y-m-d')) > strtotime($dueDate) &&
                            Configuration::get('PRESTA_PAYLATER_LATE_FEE')
                        ) {
                            if (Configuration::get('PRESTA_PAYLATER_LATE_FEE_TYPE') == 1) {
                                $lateFeeValue = Tools::ps_round(($details * $feeValue)/100, 2);
                            } else {
                                $lateFeeValue = $feeValue;
                            }
                            $buyNowTransaction = new PrestaBuyNowTransaction();
                            $buyNowTransaction->id_customer = $customer['id_customer'];
                            $buyNowTransaction->id_cart = 0;
                            $buyNowTransaction->id_currency = Configuration::get('PS_CURRENCY_DEFAULT');
                            $buyNowTransaction->dr_amount = $lateFeeValue;
                            $buyNowTransaction->cr_amount = 0;
                            $buyNowTransaction->transaction_for = 3; // Late Fee Of PayLater
                            $buyNowTransaction->save();
                            unset($buyNowTransaction);
                        }

                        // Send warning mail to customers
                        if (Configuration::get('PRESTA_PAYLATER_WARNING_MAIL_TO_CUST')) {
                            $daysBeforeWarningMail = Configuration::get('PRESTA_PAYLATER_WARNING_MAIL_BEFORE_DUE_DATE');
                            $dueDate = Configuration::get('PRESTA_PAYLATER_NEXT_DUE_DATE');
                            $currentDate = date('Y-m-d');
                            $diff = abs(strtotime($dueDate) - strtotime($currentDate));
                            $years = floor($diff / (365*60*60*24));
                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                            if ($daysBeforeWarningMail >= $days) {
                                $objPrestaBuyNowPayLater->sendWarningEmailToCustomer($customer['id_customer']);
                            }
                        }
                    }
                }
                $dueDate = strtotime('+1 month', strtotime($dueDate));
                $nextDueDate = date('Y-m-d', $dueDate);
                Configuration::updateValue('PRESTA_PAYLATER_NEXT_DUE_DATE', $nextDueDate);
            }
        }
        die("DONE"); //=============================================================================================
    }
}
$objPrestaBuyNowUpdate = new PrestaBuyNowUpdate();
$objPrestaBuyNowUpdate->updateBuyNowPayLater();
