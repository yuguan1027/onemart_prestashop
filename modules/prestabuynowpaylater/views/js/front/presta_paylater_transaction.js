/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

$(document).ready(function() {
    $(".presta_more").click(function() {
        var idValue = $(this).attr('id');
        $('#presta_order_detail_'+idValue).toggle();
    });
    $("#radio_other_amount").click(function() {
        $('#presta_total_pay_btn').hide();
        $('.presta_other_amount').show();
    });
    $("#radio_total_outstandings").click(function() {
        $('.presta_other_amount').hide();
        $('#presta_total_pay_btn').show();
    });
    $(".presta_other_amount").click(function() {
        var partial_amount = parseInt($("#presta_partial_amount").val());
        var unbilled = parseInt($("#presta_partial_amount").attr('data-id'));
        if (!partial_amount) {
            return false;
        }
    });
    $("#presta_partial_amount").keyup(function() {
        var partial_amount = parseInt($(this).val());
        var unbilled = parseInt($(this).attr('data-id'));
        if (partial_amount==0) {
            $('#presta_remaining_amount_msg').show();
            $('#presta_remaining_amount_msg').addClass('alert alert-danger').empty().append(empty_error);
        } else if (partial_amount>unbilled) {
            $('#presta_remaining_amount_msg').show();
            $('#presta_remaining_amount_msg').addClass('alert alert-danger').empty().append(larger_amount_error);
        } else {
            $('#presta_remaining_amount_msg').hide();
            $('#presta_remaining_amount_div').show(1000);
            $('#presta_remaining_amount').empty().append(unbilled-partial_amount);
        }

    });
});
