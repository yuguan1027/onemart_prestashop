/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

$(document).ready(function(){
    var xhr = '';
    if ($('#presta_product').length) {
        $('#presta_product').multiselect({
            columns: 1,
            placeholder: 'Select Products',
            search: true,
            selectAll: true
        });
    }
    // if($(".radio_payment_options").checked()){
    //     $('#presta_total_pay_btn').attr('disabled');
    // }

    $("#radio_other_amount").click(function() {
        $('#presta_total_pay_btn').hide();
        $('.presta_other_amount').show();
        $('#presta_remaining_amount_div').hide(500);
        $('#presta_partial_payment_options').show(1000);
    });
    $("#radio_total_outstandings").click(function() {
        $('.presta_other_amount').hide();
        $('#presta_total_pay_btn').show();
        $('#presta_partial_payment_options').hide(500);
        $('#presta_remaining_amount_div').show(1000);
    });
    $(".presta_other_amount").click(function() {
        var partial_amount = parseInt($("#presta_partial_amount").val());
        var unbilled = parseInt($("#presta_partial_amount").attr('data-id'));
        if (!partial_amount) {
            return false;
        }
    });
    $("#presta_partial_amount").keyup(function() {
        var partial_amount = parseInt($(this).val());
        var unbilled = parseInt($(this).attr('data-id'));
        if (!partial_amount || partial_amount==0) {
            $('#presta_remaining_amount_msg').show();
            $('#presta_remaining_amount_msg').addClass('alert alert-danger').empty().append(empty_error);
        } else if (partial_amount>unbilled) {
            $('#presta_remaining_amount_msg').show();
            $('#presta_remaining_amount_msg').addClass('alert alert-danger').empty().append(larger_amount_error);
        }

    });

    $(document).on('click', '.presta-load-more', function(){
        var lastCount = $('input[name="presta_product_last_count"]').val();
        $('.presta_img_container img').show();
        $('.ms-options').addClass('presta_hold');
        $.ajax({
            url: presta_admin_controller,
            cache: false,
            dataType: "json",
            data: {
                'ajax': true,
                'action': 'getMoreProducts',
                'lastCount': lastCount
            },
            success: function(data) {
                $('.presta_img_container img').hide();
                $('.ms-options').removeClass('presta_hold');
                if (data) {
                    if (data.products.length > 0) {
                        $('input[name="presta_product_last_count"]').val(data.currentCount);
                        $.each(data.products, function(index, value){
                            $('.ms-options ul').append('<li data-search-term="('+value.id_product+') '+value.name+'"><label for="ms-opt-20"><input type="checkbox" title="('+value.id_product+') '+value.name+'" id="ms-opt-20" value="'+value.id_product+'">('+value.id_product+') '+value.name+'</label></li>');
                            $('#presta_product').append('<option value="'+value.id_product+'">'+value.name+'</option>');
                        });
                    } else {
                        alert('No more products found');
                    }
                }
            }
        });
    });

    $(document).on('keyup', 'input[name="presta_customer_search"]', function(){
        var query = $(this).val();
        if (!query) {
            alert('Please type words to search customers');
            return false;
        } else if (query.length > 2) {
            $('.presta_img_container img').show();
            if(xhr && xhr.readyState != 200){
                xhr.abort();
            }
            xhr = $.ajax({
                url: presta_admin_controller,
                cache: false,
                dataType: "json",
                data: {
                    'ajax': true,
                    'action': 'searchCustomer',
                    'query': query
                },
                success: function(data) {
                    $('.presta_img_container img').hide();
                    $('.searched-result').empty();
                    if (data.status == 'ok') {
                        $.each(data.customers, function(i, value) {
                            var custName = value.firstname+' '+value.lastname;
                            var name = "'"+custName+"'";
                            var idCustomer = value.id_customer;
                            $('.searched-result').append('<p onclick="prestaCustomerSelect('+idCustomer+','+name+')">('+idCustomer+') '+custName+'</p>');
                        });
                    } else {
                        $('.searched-result').append('<p>No record found!</p>');
                    }
                }
            });
        }
    });

    $(document).on('keyup', 'input[name="presta_search_product"]', function(){
        var query = $(this).val();
        if (!query) {
            alert('Please type words to search products');
            return false;
        } else if (query.length > 2) {
            $('.presta_img_container img').show();
            if(xhr && xhr.readyState != 200){
                xhr.abort();
            }
            xhr = $.ajax({
                url: presta_admin_controller,
                cache: false,
                dataType: "json",
                data: {
                    'ajax': true,
                    'action': 'searchProduct',
                    'query': query
                },
                success: function(data) {
                    $('.presta_img_container img').hide();
                    $('.searched-result').empty();
                    if (data.status == 'ok') {
                        $.each(data.products, function(i, value) {
                            var pname = value.name;
                            var name = "'"+pname+"'";
                            var idProduct = value.id_product;
                            $('.searched-result').append('<p onclick="prestaProductSelect('+idProduct+','+name+')">('+idProduct+') '+pname+'</p>');
                        });
                    } else {
                        $('.searched-result').append('<p>No record found!</p>');
                    }
                }
            });
        }
    });

    $(document).on('change', 'input[name="presta_paylater_enable"]', function(){
        if ($(this).val() == 1) {
            $('.presta_limit').show('slow');
        } else {
            $('.presta_limit').hide('slow');
        }
    });

    if ($('input[name="presta_paylater_enable"]').length) {
        if ($('input[name="presta_paylater_enable"]:checked').val() == 1) {
            $('.presta_limit').show('slow');
        } else {
            $('.presta_limit').hide('slow');
        }
    }

    $(".presta_more").click(function() {
        var idValue = $(this).attr('id');
        $('#presta_order_detail_'+idValue).toggle();
    });

    $(document).on('change', '#idCustomer', function(){
        var idCustomer = $(this).val();
        if (idCustomer > 0) {
            window.location.href = prestaAdminLink+'&id_customer='+idCustomer;
        }
    });
});

function prestaCustomerSelect(idCustomer, custName)
{
    $('.searched-result, .selected-result').empty();
    $('.selected-result').append('<p>('+idCustomer+') '+custName+'</p>').show();
    $('.presta_customer').show('slow');
    $('input[name="presta_selected_customer"]').val(idCustomer);
}

function prestaProductSelect(idProduct, name)
{
    $('.searched-result, .selected-result').empty();
    $('.selected-result').append('<p>('+idProduct+') '+name+'</p>').show();
    $('.presta_customer').show('slow');
    $('input[name="presta_selected_product"]').val(idProduct);
}
