/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

$.fn.mColorPicker.defaults.imageFolder = '../img/admin/';
$(document).ready(function() {
    if ($('input[name="presta_paylater_order_restriction"]:checked').val() == 1) {
			$('.presta_order_restriction').show('slow');
		} else {
			$('.presta_order_restriction').hide('slow');
    }

    if ($('input[name="presta_paylater_late_fee_enable"]:checked').val() == 1) {
			$('.presta_late_fee').show('slow');
		} else {
			$('.presta_late_fee').hide('slow');
    }

    if ($('select[name="presta_late_fee_type"]:checked').val() == 1) {
		$('.presta_percentage').show();
				$('.presta_fixed').hide();
	} else {
		$('.presta_percentage').hide();
      	$('.presta_fixed').show();
	}

	if ($('input[name="presta_paylater_warning_to_cust"]:checked').val() == 1) {
		$('.presta_warning_mail').show('slow');
	} else {
		$('.presta_warning_mail').hide('slow');
	}

	if ($('input[name="presta_paylater_faq"]:checked').val() == 1) {
		$('.presta_cms').show('slow');
	} else {
		$('.presta_cms').hide('slow');
	}
});
$(document).on('change', 'input[name="presta_paylater_order_restriction"]', function(){
	if ($(this).val() == 1) {
		$('.presta_order_restriction').show('slow');
	} else {
		$('.presta_order_restriction').hide('slow');
	}
});
$(document).on('change', 'input[name="presta_paylater_late_fee_enable"]', function(){
	if ($(this).val() == 1) {
		$('.presta_late_fee').show('slow');
	} else {
		$('.presta_late_fee').hide('slow');
	}
});
$(document).on('change', 'select[name="presta_late_fee_type"]', function(){
	if ($(this).val() == 1) {
        $('.presta_percentage').show();
        $('.presta_fixed').hide();
	} else {
		$('.presta_percentage').hide();
        $('.presta_fixed').show();
	}
});
$(document).on('change', 'input[name="presta_paylater_warning_to_cust"]', function(){
	if ($(this).val() == 1) {
		$('.presta_warning_mail').show('slow');
	} else {
		$('.presta_warning_mail').hide('slow');
	}
});
$(document).on('change', 'input[name="presta_paylater_faq"]', function(){
	if ($(this).val() == 1) {
		$('.presta_cms').show('slow');
	} else {
		$('.presta_cms').hide('slow');
	}
});
