 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

{if empty($id_product)}
    <div class="product-tab-content">
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {l s='There is 1 warning.' mod='prestabuynowpaylater'}
            <ul style="display:block;" id="seeMore">
                <li>{l s='You have not save this product before applying payment restriction.' mod='prestabuynowpaylater'}</li>
            </ul>
        </div>
    </div>
{else}
<div class="product-tab-content">
    <div class="alert alert-info">
        {l s='Enable/Disable the product for Buy Now Pay Later' mod='prestabuynowpaylater'}
    </div>
    <div class="clearfix form-group">
        <label class="control-label col-lg-3 required">
            <span
                title=""
                data-html="true"
                data-toggle="tooltip"
                class="label-tooltip"
                data-original-title="{l s='If Enabled, Customer will able to use pay later payment method and its features' mod='prestabuynowpaylater'}">
                    {l s='Allow Buy Now Pay Later: ' mod='prestabuynowpaylater'}
            </span>
        </label>
        <div class="col-lg-9">
            <span class="switch prestashop-switch fixed-width-lg">
                <input
                    type="radio"
                    name="presta_paylater_enable"
                    id="presta_paylater_enable_on"
                    value="1"
                    {if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == 1}checked="checked"
                    {else if isset($prestaBuyNowProduct) && $prestaBuyNowProduct.active == 1}checked="checked"
                    {else if !isset($smarty.post.presta_paylater_enable)}checked="checked"{/if}>
                    <label for="presta_paylater_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                <input
                    type="radio"
                    name="presta_paylater_enable"
                    id="presta_paylater_enable_off"
                    value="0"
                    {if isset($prestaBuyNowProduct) && $prestaBuyNowProduct.active == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == '0'}checked="checked"{/if}>
                <label for="presta_paylater_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
                <a class="slide-button btn"></a>
            </span>
        </div>
    </div>
</div>
{/if}
