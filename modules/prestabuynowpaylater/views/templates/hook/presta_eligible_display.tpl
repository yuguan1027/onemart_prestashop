 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<div class="presta_eligible">
    <i class="material-icons">loyalty</i>
    <span>{l s='Eligible for Pay Later' mod='prestabuynowpaylater'}</span>
</div>
