 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<a
    class="col-lg-4 col-md-6 col-sm-6 col-xs-12"
    href="{url entity='module' name='prestabuynowpaylater' controller='transaction'}">
    <span class="link-item">
        <i class="material-icons">payment</i> {l s='Buy Now Pay Later' mod='prestabuynowpaylater'}
    </span>
</a>
