 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

{extends file='checkout/checkout.tpl'}
{block name='content'}
    <div class="row" style="margin-bottom:20px;">
        <div class="col-md-8">
            <section id="checkout-payment-step" class="checkout-step -reachable -current js-current-step">
            <h1 class="step-title h3"><i class="material-icons rtl-no-flip done"></i>
                <span class="step-number">4</span> {l s='Payment' mod='prestabuynowpaylater'}
            </h1>
            <div class="content">
                <div class="payment-options">
                    {if isset($payment_options) && $payment_options}
                        {foreach from=$payment_options item="module_options"}
                            {foreach from=$module_options item="option"}
                                <div>
                                    <div id="payment-option-{$option.id|escape:'html':'UTF-8'}-container" class="payment-option clearfix">
                                        <div class="payment-options">
                                            <span class="custom-radio float-xs-left">
                                                <input
                                                    class="ps-shown-by-js {if $option.binary} binary {/if}"
                                                    id="{$option.id|escape:'html':'UTF-8'}"
                                                    data-module-name="{$option.module_name|escape:'html':'UTF-8'}"
                                                    name="payment-option"
                                                    type="radio"
                                                    required>
                                                <span></span>
                                            </span>
                                            <form method="GET" class="ps-hidden-by-js">
                                                <button
                                                    class="ps-hidden-by-js"
                                                    type="submit"
                                                    name="select_payment_option"
                                                    value="{$option.id|escape:'html':'UTF-8'}">
                                                    {l s='Choose' mod='prestabuynowpaylater'}
                                                </button>
                                            </form>
                                            <label for="{$option.id|escape:'html':'UTF-8'}">
                                                <span>{$option.call_to_action_text|escape:'html':'UTF-8'}</span>
                                                {if $option.logo}
                                                    <img src="{$option.logo|escape:'html':'UTF-8'}">
                                                {/if}
                                            </label>
                                        </div>
                                    </div>
                                    {if $option.additionalInformation}
                                        <div
                                            id="{$option.id|escape:'html':'UTF-8'}-additional-information"
                                            class="js-additional-information definition-list additional-information">
                                            {$option.additionalInformation nofilter}
                                        </div>
                                    {/if}
                                    <div
                                        id="pay-with-{$option.id|escape:'html':'UTF-8'}-form"
                                        class="js-payment-option-form">
                                        {if $option.form}
                                            {$option.form nofilter}
                                        {else}
                                            <form id="payment-form" method="POST" action="{$option.action nofilter}">
                                                {foreach from=$option.inputs item=input}
                                                    <input
                                                        type="{$input.type|escape:'html':'UTF-8'}"
                                                        name="{$input.name|escape:'html':'UTF-8'}"
                                                        value="{$input.value|escape:'html':'UTF-8'}">
                                                {/foreach}
                                                <button
                                                    style="display:none"
                                                    id="pay-with-{$option.id|escape:'html':'UTF-8'}"
                                                    type="submit">
                                                    </button>
                                            </form>
                                        {/if}
                                    </div>
                                </div>
                            {/foreach}
                        {foreachelse}
                        <p class="alert alert-danger">
                            {l s='Unfortunately, there are no payment method available.'  mod='prestabuynowpaylater'}
                        </p>
                        {/foreach}
                    {/if}
                </div>
                {if count($conditions_to_approve)}
                    <p class="ps-hidden-by-js">
                        {l s='By confirming the order, you certify that you have read and agree with all of the conditions below:' mod='prestabuynowpaylater'}
                    </p>
                    <form id="conditions-to-approve" method="GET">
                        <ul>
                            {foreach from=$conditions_to_approve item="condition" key="condition_name"}
                            <li>
                                <div class="float-xs-left">
                                <span class="custom-checkbox">
                                    <input  id    = "conditions_to_approve[{$condition_name|escape:'html':'UTF-8'}]"
                                            name  = "conditions_to_approve[{$condition_name|escape:'html':'UTF-8'}]"
                                            required
                                            type  = "checkbox"
                                            value = "1"
                                            class = "ps-shown-by-js"
                                    >
                                    <span><i class="material-icons rtl-no-flip checkbox-checked">&#xE5CA;</i></span>
                                </span>
                                </div>
                                <div class="condition-label">
                                <label class="js-terms" for="conditions_to_approve[{$condition_name|escape:'html':'UTF-8'}]">
                                    {$condition nofilter}
                                </label>
                                </div>
                            </li>
                            {/foreach}
                        </ul>
                    </form>
                {/if}
                <div id="payment-confirmation">
                    <div class="ps-shown-by-js">
                        <button
                            disabled="disabled"
                            type="submit"
                            class="btn btn-primary center-block">
                            {l s='Order with an obligation to pay' mod='prestabuynowpaylater'}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <section class="card">
                <div class="card-block">
                    <div class="cart-summary-products">
                        <p>{l s='Pay Later Payment' mod='prestabuynowpaylater'}</p>
                        <p></p>
                        <div class="collapse"></div>
                    </div>
                    <div class="cart-summary-line cart-summary-subtotals">
                        <span class="label">{l s='Subtotal' mod='prestabuynowpaylater'}</span>
                        <span class="value">{$cart_totoal|escape:'html':'UTF-8'}</span>
                    </div>
                    <div class="cart-summary-line cart-summary-subtotals"></div>
                </div>
                <hr class="separator">
                <div class="card-block cart-summary-totals">
                    <div class="cart-summary-line cart-total">
                        <span class="label">{l s='Total (tax incl.)' mod='prestabuynowpaylater'}</span>
                        <span class="value">{$cart_totoal|escape:'html':'UTF-8'}</span>
                    </div>
                    <div class="cart-summary-line">
                        <span class="label sub"></span>
                        <span class="value sub"></span>
                    </div>
                </div>
            </section>
        </div>
    </div>
{/block}
