 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

{extends file=$layout}

{block name='content'}
	{* <div class="alert alert-warning">
		{l s='Pay before 21-01-2019 and avoid late fee charges' mod='prestabuynowpaylater'}
	</div> *}
    <section id="presta_main">
		<header class="presta-page-header">
			<h1>{l s='Pay Later' mod='prestabuynowpaylater'}</h1>
		</header>
		<hr>
		<p>
			<span>
				{l s='Current Usage' mod='prestabuynowpaylater'}
				{if isset($unbilled)}{$unbilled|escape:'html':'UTF-8'}{else}0.00{/if}
			</span>
			<span> | </span>
			<span>{l s='Balance' mod='prestabuynowpaylater'} </span>
			<span class="presta_balance">{$balance|escape:'html':'UTF-8'}</span>
			<span class="presta_right">{l s='Limit' mod='prestabuynowpaylater'} {$limit|escape:'html':'UTF-8'}</span>
		</p>
		<div class="presta_price_usage_container">
			<div class="presta_used"></div>
		</div>
		<hr>
		{if isset($unbilled)}
			<div class="clearfix">
				<p>{l s='Unbilled Amount' mod='prestabuynowpaylater'}</p>
				<div class="presta_unbilled">
					<span>{if isset($unbilled)}{$unbilled|escape:'html':'UTF-8'}{else}0.00{/if}
					{if isset($unbilled_amount) && $unbilled_amount|escape:'html':'UTF-8' > 0}

                    <div class="form-check">
                    {if Configuration::get('PRESTA_PARTIAL_ENABLE')}
                        <label class="form-check-label">
                            <input checked id="radio_total_outstandings" type="radio" class="form-check-input" name="optradio"> {l s='Pay Total Outstandings' mod='prestabuynowpaylater'}
                        </label>
                    {/if}
                    </div>
						<a
                            id="presta_total_pay_btn"
							href="{url entity='module' name='prestabuynowpaylater' controller='payment'}"
							class="btn btn-primary">
							{l s='Pay Now' mod='prestabuynowpaylater'}
						</a>
                    {if Configuration::get('PRESTA_PARTIAL_ENABLE')}
                    <div class="form-check">
                        <label class="form-check-label">
                            <input id="radio_other_amount" type="radio" class="form-check-input" name="optradio">  {l s='Pay Other Amount' mod='prestabuynowpaylater'}
                        </label>
                    </div>
                    {/if}
					{/if}</span>
					{if Configuration::get('PRESTA_PAYLATER_FAQ')}
						<a
							href="{$presta_cms|escape:'html':'UTF-8'}"
							class="presta-right">
							{l s='Read FAQs' mod='prestabuynowpaylater'}
						</a>
					{/if}
				</div>
			</div>
            <div class="clearfix presta_other_amount" style="display:none;">
                {if isset($unbilled_amount) && $unbilled_amount > 0}
                <form method="post" action="{url entity='module' name='prestabuynowpaylater' controller='payment'}">
                    <div class="clearfix col-md-2">
                        <input
                            style="width:100px;"
                            type="text"
                            data-id="{$unbilled_amount|escape:'html':'UTF-8'}"
                            name="presta_partial_amount"
                            id="presta_partial_amount"
                            class="form-control"
                            />
                    </div>
                    <div class="clearfix col-md-10">
                        <button
                            type="submit"
                            class="btn btn-primary">
                            {l s='Pay Now' mod='prestabuynowpaylater'}
                        </button>
                    </div>
                </form>
                {/if}
            </div><br>
            <p style="display:none;" id="presta_remaining_amount_msg"></p>
            {* <div id="presta_remaining_amount_div" class="clearfix" style="display:none;">
                <br>
                <p>{l s='Remaining Amount' mod='prestabuynowpaylater'}</p>
                    <span id="presta_remaining_amount">{if isset($unbilled)}{$unbilled}{else}0.00{/if}</span>
            </div> *}
		{/if}
		<div class="clearfix presta_transaction">
			<p class="presta_heading">{l s='Transaction History' mod='prestabuynowpaylater'}</p>
			<section class="clearfix presta-page-content">
				{if isset($alltransaction) && $alltransaction}
					{foreach from=$alltransaction key=key item=months}
						{foreach from=$months key=mkey item=month}
							<div class="clearfix presta_transaction_block">
								<div class="clearfix presta_transaction_head">
									<span>{$mkey|escape:'html':'UTF-8'} {$key|escape:'html':'UTF-8'}</span>
									<span class="presta_right">
										{if isset($unbilled_amount) && $unbilled_amount > 0}
											<span>{l s='Unbilled' mod='prestabuynowpaylater'}</span>
										{else}
											<span>{l s='Paid' mod='prestabuynowpaylater'}</span>
										{/if}
										<span>|</span>
										<span>{l s='Net Total:' mod='prestabuynowpaylater'} {$unbilled|escape:'html':'UTF-8'}</span>
									</span>
								</div>
								{foreach from=$month key=k item=value}
									<div class="clearfix presta_transaction_details">
										<div class="col-md-12 presta_inner">
											<div class="col-md-3 presta_date">
												{$value.date_add|date_format:"%A, %B %e"}
											</div>
											{foreach from=$value.products|@array_slice:0:1 key=p item=product}
												<div class="col-md-7 presta_item">
													{if $value.transaction_for == 1}
														{$product.name|truncate:40}
													{else if $value.transaction_for == 2}
														{l s='Pay Later Payment' mod='prestabuynowpaylater'}
													{else if $value.transaction_for == 3}
														<span class="late-fee">{l s='Late Payment Fee' mod='prestabuynowpaylater'}</span>
													{/if}
													{if $value.products|@count > 1}
														<a
															class="presta_more"
															id="{$value.id|escape:'html':'UTF-8'}"
															href="javascript:void(0);">
															+{$value.products|@count -1} Items
														</a>
													{/if}
												</div>
												<div class="col-md-2 presta_amount">
													{if $value.transaction_for == 1}
														<span class="presta_dr_transaction">{$value.dr_amount|escape:'html':'UTF-8'}</span>
													{else if $value.transaction_for == 2}
														<span class="presta_cr_transaction">- {$value.cr_amount|escape:'html':'UTF-8'}</span>
													{else if $value.transaction_for == 3}
														<span class="presta_late_fee_transaction">{$value.dr_amount|escape:'html':'UTF-8'}</span>
													{/if}
												</div>
											{/foreach}
										</div>
										<div class="clearfix presta_order_detail" id="presta_order_detail_{$value.id|escape:'html':'UTF-8'}">
											{foreach from=$value.products key=p item=product}
												<div class="col-md-12 presta_inner">
													<div class="col-md-4"></div>
													<div class="col-md-4">
														{if $value.transaction_for == 1}
															{$product.name|escape:'html':'UTF-8'}
														{else if $value.transaction_for == 2}
															{l s='Pay Later Payment' mod='prestabuynowpaylater'}
														{else if $value.transaction_for == 3}
															{l s='Late Payment Fee' mod='prestabuynowpaylater'}
														{/if}
													</div>
													<div class="col-md-2">{$product.total|escape:'html':'UTF-8'}</div>
												</div>
											{/foreach}
										</div>
										<div
											class="presta_order_reference">
											<div class="presta_order_ref presta_left">
												{l s='Order Reference:' mod='prestabuynowpaylater'}
											</div>
											<div class="presta_order_num presta_left">
												{if $value.reference}
													{$value.reference|escape:'html':'UTF-8'}
												{else}
													{l s='N/A' mod='prestabuynowpaylater'}
												{/if}
											</div>
										</div>
									</div>
								{/foreach}
							</div>
							<br/>
						{/foreach}
					{/foreach}
				{else}
					<p>{l s='No record found!' mod='prestabuynowpaylater'}</p>
				{/if}
			</section>
		</div>
	</section>
	<style>
		{if !$used > 0}
			.presta_used {
				background-color: inherit !important;
			}
		{/if}
		.presta_used {
			width: {$used|escape:'html':'UTF-8'}% !important;
		}
	</style>
{/block}
