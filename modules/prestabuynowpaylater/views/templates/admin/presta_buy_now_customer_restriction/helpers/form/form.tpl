 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<div class="row">
    <div class="col-lg-12">
        <form
            action="{$current|escape:'html':'UTF-8'}&{if !empty($submit_action)}{$submit_action|escape:'html':'UTF-8'}{/if}&token={$token|escape:'html':'UTF-8'}"
            method="post"
            novalidate=""
            id="configuration_form"
            enctype="multipart/form-data"
            class="defaultForm form-horizontal">
            <input type="hidden" name="submitProductRestriction" value="1">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    {if isset($presta_edit_customer)}
                        {l s='Edit Customer Information' mod='prestabuynowpaylater'}
                    {else}
                        {l s='Choose Customer To Allow For Buy Now Pay Later' mod='prestabuynowpaylater'}
                    {/if}
                </div>
                <div class="form-wrapper">
                    <div class="clearfix form-group">
                        <label class="control-label col-lg-3 required">
                            {if isset($presta_edit_customer)}
                                {l s='Customer Name' mod='prestabuynowpaylater'}
                            {else}
                                <span
                                    title=""
                                    data-html="true"
                                    data-toggle="tooltip"
                                    class="label-tooltip"
                                    data-original-title="{l s='Search customer to set limit' mod='prestabuynowpaylater'}">{l s='Search Customer' mod='prestabuynowpaylater'}
                                </span>
                            {/if}
                        </label>
                        <div class="col-lg-4 select-presta-product">
                        {if isset($presta_edit_customer)}
                            <input
                                type="hidden"
                                value="{$presta_edit_customer->id_customer|escape:'html':'UTF-8'}"
                                name="presta_selected_customer" />
                            <div class="clearfix selected-result" style="display:block;">
                                <p>{$customer->firstname|escape:'html':'UTF-8'} {$customer->lastname|escape:'html':'UTF-8'}</p>
                            </div>
                        {else}
                            <input autocomplete="off" class="form-control" type="text" name="presta_customer_search" />
                            <input type="hidden" value="" name="presta_selected_customer" />
                            <div class="help-block">{l s='Type three characters to search the customers' mod='prestabuynowpaylater'}</div>
                            <div class="clearfix searched-result"></div>
                            <div class="clearfix selected-result"></div>
                        {/if}
                        </div>
                        <div class="col-lg-2 presta_img_container">
                            {* <img width="32" src="{$modules_dir|escape:'html':'UTF-8'}prestabuynowpaylater/views/img/loader.gif"/> *}
                        </div>
                    </div>
                    <div class="clearfix {if !isset($presta_edit_customer)}presta_customer{/if}">
                        <div class="clearfix form-group">
                            <label class="control-label col-lg-3 required">
                                <span
                                    title=""
                                    data-html="true"
                                    data-toggle="tooltip"
                                    class="label-tooltip"
                                    data-original-title="{l s='If Enabled, Customer will able to use pay later payment method and its features' mod='prestabuynowpaylater'}">
                                        {l s='Allow Buy Now Pay Later: ' mod='prestabuynowpaylater'}
                                </span>
                            </label>
                            <div class="col-lg-9">
                                <span class="switch prestashop-switch fixed-width-lg">
                                    <input
                                        type="radio"
                                        name="presta_paylater_enable"
                                        id="presta_paylater_enable_on"
                                        value="1"
                                        {if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == 1}checked="checked"
                                        {else if isset($presta_edit_customer) && $presta_edit_customer->active == 1}checked="checked"
                                        {else if !isset($smarty.post.presta_paylater_enable)}checked="checked"{/if}>
                                        <label for="presta_paylater_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                                    <input
                                        type="radio"
                                        name="presta_paylater_enable"
                                        id="presta_paylater_enable_off"
                                        value="0"
                                        {if isset($presta_edit_customer) && $presta_edit_customer->active == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == '0'}checked="checked"{/if}>
                                    <label for="presta_paylater_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
                                    <a class="slide-button btn"></a>
                                </span>
                            </div>
                        </div>
                        <div class="clearfix form-group presta_limit">
                            <label class="control-label col-lg-3 required">
                                <span
                                    title=""
                                    data-html="true"
                                    data-toggle="tooltip"
                                    class="label-tooltip"
                                    data-original-title="Set credit limit for customer">{l s='Credit Limit' mod='prestabuynowpaylater'}
                                </span>
                            </label>
                            <div class="col-lg-2">
                                <div class="input-group">
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="presta_customer_limit"
                                        value="{if isset($presta_edit_customer)}{Tools::ps_round($presta_edit_customer->paylater_limit|escape:'html':'UTF-8')}{/if}">
                                    <span class="input-group-addon">{$presta_currency_symbol|escape:'html':'UTF-8'}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button
                        type="submit"
                        value="1"
                        name="submitCustomerRestriction"
                        class="btn btn-default pull-right">
                        <i class="process-icon-save"></i> {l s='Save' mod='prestabuynowpaylater'}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
