 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<div class="row" id="prestabuynowpaylater">
    <ul class="nav nav-pills nav-tabs">
        <li class="nav-item {if isset($prestatab) && $prestatab == 1}active{/if}">
            <a class="nav-link" data-toggle="tab" href="#prestageneral">
                <i class="icon-bell"></i> {l s='General Configuration' mod='prestabuynowpaylater'}
            </a>
        </li>
        <li class="nav-item {if isset($prestatab) && $prestatab == 2}active{/if}">
            <a class="nav-link" data-toggle="tab" href="#prestalatefee">
                <i class="icon-credit-card"></i> {l s='DUES/FEE CONFIGURATION' mod='prestabuynowpaylater'}
            </a>
        </li>
        <li class="nav-item {if isset($prestatab) && $prestatab == 3}active{/if}">
            <a class="nav-link" data-toggle="tab" href="#prestaorderstatus">
                <i class="icon-shopping-cart"></i> {l s='Order Status' mod='prestabuynowpaylater'}
            </a>
        </li>
        <li class="nav-item {if isset($prestatab) && $prestatab == 4}active{/if}">
            <a class="nav-link" data-toggle="tab" href="#prestamainconfig">
                <i class="icon-cogs"></i> {l s='Email Configuration' mod='prestabuynowpaylater'}
            </a>
        </li>
    </ul>
    <div class="tab-content panel collapse in">
        <div class="alert alert-info">
            <p>
                {l s='To update Buy Now Pay Later database, set cron job with provide URL :' mod='prestabuynowpaylater'}
                <br/><br/>
                0 0 * * * {$cronUrl|escape:'html':'UTF-8'}
            </p>
        </div>
        <div class="tab-pane {if isset($prestatab) && $prestatab == 1}active{/if}" id="prestageneral">
            {include file="{$prestamoduledir}prestabuynowpaylater/views/templates/admin/_partials/general_config.tpl"}
        </div>
        <div class="tab-pane {if isset($prestatab) && $prestatab == 2}active{/if}" id="prestalatefee">
            {include file="{$prestamoduledir}prestabuynowpaylater/views/templates/admin/_partials/late_fee.tpl"}
        </div>
        <div class="tab-pane {if isset($prestatab) && $prestatab == 3}active{/if}" id="prestaorderstatus">
            {include file="{$prestamoduledir}prestabuynowpaylater/views/templates/admin/_partials/order_status.tpl"}
        </div>
        <div class="tab-pane {if isset($prestatab) && $prestatab == 4}active{/if}" id="prestamainconfig">
            {include file="{$prestamoduledir}prestabuynowpaylater/views/templates/admin/_partials/mail_config.tpl"}
        </div>
    </div>
</div>
<style>
.bootstrap .nav-pills>li.active>a, .bootstrap .nav-pills>li.active>a:focus, .bootstrap .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #00aff0;
}
</style>
