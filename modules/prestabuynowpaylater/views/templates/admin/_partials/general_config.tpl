 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<form
    method="post"
    class="defaultForm form-horizontal"
    action="{$currentIndex|escape:'html':'UTF-8'}&prestatab=1"
    enctype="multipart/form-data">
    <div class="panel-body">
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Customer will able to use pay later payment method and its features' mod='prestabuynowpaylater'}">
                        {l s='Enable Pay Later : ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_enable"
                        id="presta_paylater_enable_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_ENABLE') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_enable)}checked="checked"{/if}>
                        <label for="presta_paylater_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_enable"
                        id="presta_paylater_enable_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_ENABLE') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Customer will able to use partial payment feature' mod='prestabuynowpaylater'}">
                        {l s='Enable Partial Payment : ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_partial_enable"
                        id="presta_partial_enable_on"
                        value="1"
                        {if isset($smarty.post.presta_partial_enable) && $smarty.post.presta_partial_enable == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PARTIAL_ENABLE') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_partial_enable)}checked="checked"{/if}>
                        <label for="presta_partial_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_partial_enable"
                        id="presta_partial_enable_off"
                        value="0"
                        {if Configuration::get('PRESTA_PARTIAL_ENABLE') == '0'}checked="checked"{else if isset($smarty.post.presta_partial_enable) && $smarty.post.presta_partial_enable == '0'}checked="checked"{/if}>
                    <label for="presta_partial_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, All customer will have this feature in their my account and global limit will be applicable if specific customer limit is not defined' mod='prestabuynowpaylater'}">
                        {l s='Allow All Customer: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_all_customer_enable"
                        id="presta_paylater_all_customer_enable_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_all_customer_enable) && $smarty.post.presta_paylater_all_customer_enable == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_ALL_CUSTOMER') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_all_customer_enable)}checked="checked"{/if}>
                        <label for="presta_paylater_all_customer_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_all_customer_enable"
                        id="presta_paylater_all_customer_enable_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_ALL_CUSTOMER') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_all_customer_enable) && $smarty.post.presta_paylater_all_customer_enable == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_all_customer_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
                <div class="help-block">
                    {l s='If Enabled, All customers can use pay later payment method without configuring individual customer and global limit will be applied if specific limit is not set.' mod='prestabuynowpaylater'}
                </div>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='This limit will be applicable for all customers' mod='prestabuynowpaylater'}">
                        {l s='Global Limit : ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-2">
                <div class="input-group">
                    <input
                        type="text"
                        name="presta_paylater_global_limit"
                        value="{Configuration::get('PRESTA_PAYLATER_GLOBAL_LIMIT')|escape:'html':'UTF-8'}">
                    <span class="input-group-addon">{$presta_currency_symbol|escape:'html':'UTF-8'}</span>
                </div>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, On product detail page, customer can see "Message Related Pay Later" if product is eligible for pay later' mod='prestabuynowpaylater'}">
                        {l s='Display Product Elibility On Product Page : ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_eligible"
                        id="presta_paylater_eligible_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_eligible) && $smarty.post.presta_paylater_eligible == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_ELIGIBLE') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_eligible)}checked="checked"{/if}>
                        <label for="presta_paylater_eligible_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_eligible"
                        id="presta_paylater_eligible_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_ELIGIBLE') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_eligible) && $smarty.post.presta_paylater_eligible == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_eligible_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Admin can set number of order restriction per customer' mod='prestabuynowpaylater'}">
                        {l s='Number Of Order Restriction : ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_order_restriction"
                        id="presta_paylater_order_restriction_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_order_restriction) && $smarty.post.presta_paylater_order_restriction == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_ORDER_RESTRICTION') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_order_restriction)}checked="checked"{/if}>
                        <label for="presta_paylater_order_restriction_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_order_restriction"
                        id="presta_paylater_order_restriction_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_ORDER_RESTRICTION') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_order_restriction) && $smarty.post.presta_paylater_order_restriction == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_order_restriction_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group presta_order_restriction">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='Set number of orders for each customer' mod='prestabuynowpaylater'}">
                        {l s='Number Of Allowed Orders: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-2">
                <input
                    type="text"
                    name="presta_paylater_order_restriction_no"
                    value="{Configuration::get('PRESTA_PAYLATER_ORDER_RESTRICTION_NO')|escape:'html':'UTF-8'}">
                <div class="help-block">
                {l s='Restriction will be month wise for each customer' mod='prestabuynowpaylater'}
                </div>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='Set the numbers to load more while working on bulk update' mod='prestabuynowpaylater'}">
                        {l s='Number of load more products : ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-2">
                <input
                    type="text"
                    name="presta_paylater_load_more_product"
                    value="{Configuration::get('PRESTA_PAYLATER_LOAD_MORE_PRODUCT')|escape:'html':'UTF-8'}">
                <div class="help-block">
                    {l s='When you work with bulk product update then you need to load more product.
                    Set according to your products in prestashop.' mod='prestabuynowpaylater'}
                </div>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Customer will see a link on their my buy now pay later account' mod='prestabuynowpaylater'}">
                        {l s='Show FAQ: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_faq"
                        id="presta_paylater_faq_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_faq) && $smarty.post.presta_paylater_faq == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_FAQ') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_faq)}checked="checked"{/if}>
                        <label for="presta_paylater_faq_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_faq"
                        id="presta_paylater_faq_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_FAQ') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_faq) && $smarty.post.presta_paylater_faq == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_faq_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
                <div class="help-block">
                    {l s='If you want to show terms and condition or FAQ for the customer(s). SO that they can read out before using buy now pay later.' mod='prestabuynowpaylater'}
                </div>
            </div>
        </div>
        <div class="clearfix form-group presta_cms">
			<label class="control-label col-lg-3 required">
				<span
					title=""
					data-html="true"
					data-toggle="tooltip"
					class="label-tooltip"
					data-original-title="{l s='Select CMS page so that customer can read all the terms and condition related to buy now pay later module.' mod='prestabuynowpaylater'}">
						{l s='CMS Page For FAQ: ' mod='prestabuynowpaylater'}
				</span>
			</label>
			<div class="col-lg-3">
				<select name="presta_late_cms_page">
                    {if isset($cmsArray)}
                        {foreach $cmsArray as $cms}
                            <option
						        {if Configuration::get('PRESTA_PAYLATER_CMS_PAGE') == $cms.id}selected="selected"{/if}
						        value="{$cms.id|escape:'html':'UTF-8'}">
                                {$cms.name|escape:'html':'UTF-8'}
                            </option>
                        {/foreach}
                    {/if}
				</select>
			</div>
		</div>
    </div>
    <div class="panel-footer">
        <button
            type="submit"
            name="presta_paylater_general"
            class="btn btn-default pull-right">
            <i class="process-icon-save"></i>{l s='Save' mod='prestabuynowpaylater'}
        </button>
    </div>
</form>
