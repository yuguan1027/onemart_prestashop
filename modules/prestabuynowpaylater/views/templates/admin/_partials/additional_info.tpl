 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<div class="alert alert-info">
    {if isset($prestacontroller) && $prestacontroller == 'AdminPrestaBuyNowCategoryRestriction'}
    {l s='If you allow any category then all the products associated with allowed category will be available for buy now pay later. No need to allow product individual.' mod='prestabuynowpaylater'}
    {else if isset($prestacontroller) && $prestacontroller == 'AdminPrestaBuyNowCustomerGroupRestriction'}
    {l s='If you allow any customer group then all the customers associated with allowed customer group will be able to use buy now pay later. No need to allow customer(s) individual.' mod='prestabuynowpaylater'}
    {/if}
</div>
