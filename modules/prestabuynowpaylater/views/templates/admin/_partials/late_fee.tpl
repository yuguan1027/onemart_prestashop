 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<form
	method="post"
	id="{$table|escape:'html':'UTF-8'}_form"
	class="defaultForm form-horizontal"
	action="{$currentIndex|escape:'html':'UTF-8'}&prestatab=2"
	enctype="multipart/form-data">
	<div class="panel-body">
		<div class="clearfix form-group">
			<label class="control-label col-lg-3 required">
				<span
					title=""
					data-html="true"
					data-toggle="tooltip"
					class="label-tooltip"
					data-original-title="{l s='Customer(s) have to pay due amount on or before due date set by admin' mod='prestabuynowpaylater'}">
						{l s='Set Due Date : ' mod='prestabuynowpaylater'}
				</span>
			</label>
			<div class="col-lg-2">
				<select name="presta_paylater_due_date">
					{for $date=1 to 28}
						<option
							{if Configuration::get('PRESTA_PAYLATER_DUE_DATE') == $date}selected="selected"{/if}
							value="{$date|escape:'html':'UTF-8'}">{$date|escape:'html':'UTF-8'}
						</option>
					{/for}
				</select>
			</div>
		</div>
		<div class="clearfix form-group">
			<label class="control-label col-lg-3 required">
				<span
					title=""
					data-html="true"
					data-toggle="tooltip"
					class="label-tooltip"
					data-original-title="{l s='If Enabled, Customer will be charged if they failed to pay their dues before or on due date' mod='prestabuynowpaylater'}">
						{l s='Enable Late Fee Charges : ' mod='prestabuynowpaylater'}
				</span>
			</label>
			<div class="col-lg-9">
				<span class="switch prestashop-switch fixed-width-lg">
					<input
						type="radio"
						name="presta_paylater_late_fee_enable"
						id="presta_paylater_late_fee_enable_on"
						value="1"
						{if isset($smarty.post.presta_paylater_late_fee_enable) && $smarty.post.presta_paylater_late_fee_enable == 1}checked="checked"
						{else if Configuration::get('PRESTA_PAYLATER_LATE_FEE') == 1}checked="checked"
						{else if !isset($smarty.post.presta_paylater_late_fee_enable)}checked="checked"{/if}>
						<label for="presta_paylater_late_fee_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
					<input
						type="radio"
						name="presta_paylater_late_fee_enable"
						id="presta_paylater_late_fee_enable_off"
						value="0"
						{if Configuration::get('PRESTA_PAYLATER_LATE_FEE') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_late_fee_enable) && $smarty.post.presta_paylater_late_fee_enable == '0'}checked="checked"{/if}>
					<label for="presta_paylater_late_fee_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>
		<div class="clearfix form-group presta_late_fee">
			<label class="control-label col-lg-3 required">
				<span
					title=""
					data-html="true"
					data-toggle="tooltip"
					class="label-tooltip"
					data-original-title="{l s='This limit will be applicable for all customers' mod='prestabuynowpaylater'}">
						{l s='Late Fee Charge: ' mod='prestabuynowpaylater'}
				</span>
			</label>
			<div class="col-lg-2">
				<select name="presta_late_fee_type">
					<option
						{if Configuration::get('PRESTA_PAYLATER_LATE_FEE_TYPE') == '1'}selected="selected"{/if}
						value="1">{l s='Percentage' mod='prestabuynowpaylater'}</option>
					<option
						{if Configuration::get('PRESTA_PAYLATER_LATE_FEE_TYPE') == '2'}selected="selected"{/if}
						value="2">{l s='Fixed Amount' mod='prestabuynowpaylater'}</option>
				</select>
			</div>
		</div>
		<div class="clearfix form-group presta_late_fee">
			<label class="control-label col-lg-3 required">
				<span
					title=""
					data-html="true"
					data-toggle="tooltip"
					class="label-tooltip"
					data-original-title="{l s='This limit will be applicable for all customers' mod='prestabuynowpaylater'}">
						{l s='Value: ' mod='prestabuynowpaylater'}
				</span>
			</label>
			<div class="col-lg-2">
				<div class="input-group">
					<input
						type="text"
						name="presta_paylater_late_fee_value"
						value="{Configuration::get('PRESTA_PAYLATER_LATE_FEE_VALUE')|escape:'html':'UTF-8'}">
					<span class="input-group-addon presta_percentage">%</span>
					<span class="input-group-addon presta_fixed" style="display:none;">{$presta_currency_symbol|escape:'html':'UTF-8'}</span>
				</div>
			</div>
		</div>
		<div class="clearfix form-group">
			<label class="control-label col-lg-3 required">
				<span
					title=""
					data-html="true"
					data-toggle="tooltip"
					class="label-tooltip"
					data-original-title="{l s='If Enabled, Customer will not able to use pay later payment method if they didn\'t pay their dues on or before due date' mod='prestabuynowpaylater'}">
						{l s='Stop Pay Later If Due Date Exceed : ' mod='prestabuynowpaylater'}
				</span>
			</label>
			<div class="col-lg-9">
				<span class="switch prestashop-switch fixed-width-lg">
					<input
						type="radio"
						name="presta_paylater_stop_after_due_date"
						id="presta_paylater_stop_after_due_date_on"
						value="1"
						{if isset($smarty.post.presta_paylater_stop_after_due_date) && $smarty.post.presta_paylater_stop_after_due_date == 1}checked="checked"
						{else if Configuration::get('PRESTA_PAYLATER_STOP_AFTER_LATE_FEE') == 1}checked="checked"
						{else if !isset($smarty.post.presta_paylater_stop_after_due_date)}checked="checked"{/if}>
						<label for="presta_paylater_stop_after_due_date_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
					<input
						type="radio"
						name="presta_paylater_stop_after_due_date"
						id="presta_paylater_stop_after_due_date_off"
						value="0"
						{if Configuration::get('PRESTA_PAYLATER_STOP_AFTER_LATE_FEE') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_stop_after_due_date) && $smarty.post.presta_paylater_stop_after_due_date == '0'}checked="checked"{/if}>
					<label for="presta_paylater_stop_after_due_date_off">{l s='No' mod='prestabuynowpaylater'}</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<button
			type="submit"
			name="presta_paylater_fee_config"
			class="btn btn-default pull-right">
			<i class="process-icon-save"></i>{l s='Save' mod='prestabuynowpaylater'}
		</button>
	</div>
</form>
