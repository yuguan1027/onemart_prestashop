 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<form
    method="post"
    class="defaultForm form-horizontal"
    action="{$currentIndex|escape:'html':'UTF-8'}&prestatab=3"
    enctype="multipart/form-data">
    <div class="panel-body">
        <div class="alert alert-info">
            {l s='Checked order status will be considered as payment accepted of dues.' mod='prestabuynowpaylater'}
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='This limit will be applicable for all customers' mod='prestabuynowpaylater'}">
                        {l s='Order Status: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-6">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="fixed-width-xs">
                                <span class="title_box">
                                    <input
                                        type="checkbox"
                                        name="checkme"
                                        id="checkme"
                                        onclick="checkDelBoxes(this.form, 'groupBox[]', this.checked)" />
                                </span>
                            </th>
                            <th class="fixed-width-xs">
                                <span class="title_box">{l s='ID' mod='prestabuynowpaylater'}</span>
                            </th>
                            <th>
                                <span class="title_box">
                                    {l s='Order Status' mod='prestabuynowpaylater'}
                                </span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    {if isset($groups)}
                        {foreach $groups as $key => $group}
                            <tr>
                                <td>
                                    {assign var=id_checkbox value=groupBox|cat:'_'|cat:$group['id_order_state']}
                                    <input
                                        type="checkbox"
                                        name="groupBox[]"
                                        class="groupBox"
                                        id="{$id_checkbox|escape:'html':'UTF-8'}"
                                        value="{$group.id_order_state|escape:'html':'UTF-8'}"
                                        {if isset($group.selected)}checked="checked"{/if} />
                                </td>
                                <td>{$group.id_order_state|escape:'html':'UTF-8'}</td>
                                <td>
                                    <label for="{$id_checkbox|escape:'html':'UTF-8'}">{$group.name|escape:'html':'UTF-8'}</label>
                                </td>
                            </tr>
                        {/foreach}
                    {/if}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button
            type="submit"
            name="presta_paylater_order_status"
            class="btn btn-default pull-right">
            <i class="process-icon-save"></i>{l s='Save' mod='prestabuynowpaylater'}
        </button>
    </div>
</form>
