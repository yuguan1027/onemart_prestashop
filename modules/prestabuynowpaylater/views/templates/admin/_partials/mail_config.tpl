 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<form
    method="post"
    class="defaultForm form-horizontal"
    action="{$currentIndex|escape:'html':'UTF-8'}&prestatab=4"
    enctype="multipart/form-data">
    <div class="panel-body">
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Customer will receive order confirmation mail along with pay later details' mod='prestabuynowpaylater'}">
                        {l s='Send Order Confimation Mail To Customer: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_order_to_cust"
                        id="presta_paylater_order_to_cust_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_order_to_cust) && $smarty.post.presta_paylater_order_to_cust == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_CUST') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_order_to_cust)}checked="checked"{/if}>
                        <label for="presta_paylater_order_to_cust_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_order_to_cust"
                        id="presta_paylater_order_to_cust_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_CUST') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_order_to_cust) && $smarty.post.presta_paylater_order_to_cust == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_order_to_cust_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Admin will receive order confirmation mail along with pay later details' mod='prestabuynowpaylater'}">
                        {l s='Send Order Confirmation Mail To Admin: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_order_to_admin"
                        id="presta_paylater_order_to_admin_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_order_to_admin) && $smarty.post.presta_paylater_order_to_admin == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_order_to_admin)}checked="checked"{/if}>
                        <label for="presta_paylater_order_to_admin_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_order_to_admin"
                        id="presta_paylater_order_to_admin_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_order_to_admin) && $smarty.post.presta_paylater_order_to_admin == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_order_to_admin_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Customer will receive payment process mail.' mod='prestabuynowpaylater'}">
                        {l s='Send Payment Success Mail To Customer: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_payment_to_cust"
                        id="presta_paylater_payment_to_cust_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_payment_to_cust) && $smarty.post.presta_paylater_payment_to_cust == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_CUST') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_payment_to_cust)}checked="checked"{/if}>
                        <label for="presta_paylater_payment_to_cust_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_payment_to_cust"
                        id="presta_paylater_payment_to_cust_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_CUST') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_payment_to_cust) && $smarty.post.presta_paylater_payment_to_cust == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_payment_to_cust_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Admin will receive payment process mail.' mod='prestabuynowpaylater'}">
                        {l s='Send Payment Success Mail To Admin: ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_payment_to_admin"
                        id="presta_paylater_payment_to_admin_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_payment_to_admin) && $smarty.post.presta_paylater_payment_to_admin == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_ADMIN') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_payment_to_admin)}checked="checked"{/if}>
                        <label for="presta_paylater_payment_to_admin_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_payment_to_admin"
                        id="presta_paylater_payment_to_admin_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_ADMIN') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_payment_to_admin) && $smarty.post.presta_paylater_payment_to_admin == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_payment_to_admin_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='If Enabled, Customer will warning mails for their dues/pending(s).' mod='prestabuynowpaylater'}">
                        {l s='Send Warning Mail To Customer On Pending(s): ' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-9">
                <span class="switch prestashop-switch fixed-width-lg">
                    <input
                        type="radio"
                        name="presta_paylater_warning_to_cust"
                        id="presta_paylater_warning_to_cust_on"
                        value="1"
                        {if isset($smarty.post.presta_paylater_warning_to_cust) && $smarty.post.presta_paylater_warning_to_cust == 1}checked="checked"
                        {else if Configuration::get('PRESTA_PAYLATER_WARNING_MAIL_TO_CUST') == 1}checked="checked"
                        {else if !isset($smarty.post.presta_paylater_warning_to_cust)}checked="checked"{/if}>
                        <label for="presta_paylater_warning_to_cust_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                    <input
                        type="radio"
                        name="presta_paylater_warning_to_cust"
                        id="presta_paylater_warning_to_cust_off"
                        value="0"
                        {if Configuration::get('PRESTA_PAYLATER_WARNING_MAIL_TO_CUST') == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_warning_to_cust) && $smarty.post.presta_paylater_warning_to_cust == '0'}checked="checked"{/if}>
                    <label for="presta_paylater_warning_to_cust_off">{l s='No' mod='prestabuynowpaylater'}</label>
                    <a class="slide-button btn"></a>
                </span>
            </div>
        </div>
        <div class="clearfix form-group presta_warning_mail">
            <label class="control-label col-lg-3 required">
                <span
                    title=""
                    data-html="true"
                    data-toggle="tooltip"
                    class="label-tooltip"
                    data-original-title="{l s='Select days before warning mail send to customers' mod='prestabuynowpaylater'}">
                        {l s='How Many Days Before Due Date Warning Mail Sent' mod='prestabuynowpaylater'}
                </span>
            </label>
            <div class="col-lg-2">
                <select name="presta_warning_before_due_date">
                    {for $date=1 to 7}
                        <option {if Configuration::get('PRESTA_PAYLATER_WARNING_MAIL_BEFORE_DUE_DATE') == $date} selected="selected"{/if} value="{$date|escape:'html':'UTF-8'}">{$date|escape:'html':'UTF-8'}</option>
                    {/for}
                </select>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <button
            type="submit"
            name="presta_paylater_mail"
            class="btn btn-default pull-right">
            <i class="process-icon-save"></i>{l s='Save' mod='prestabuynowpaylater'}
        </button>
    </div>
</form>
