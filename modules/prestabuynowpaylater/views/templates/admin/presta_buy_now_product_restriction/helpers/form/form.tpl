 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<div class="row">
    <div class="col-lg-12">
        <form
            action=""
            method="post"
            novalidate=""
            id="configuration_form"
            enctype="multipart/form-data"
            class="defaultForm form-horizontal">
            <input type="hidden" name="submitProductRestriction" value="1">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    {if isset($prestaBuyNowProduct)}
                        {l s='Edit Product Information' mod='prestabuynowpaylater'}
                    {else}
                        {l s='Choose Products To Allow For Buy Now Pay Later' mod='prestabuynowpaylater'}
                    {/if}
                </div>
                <div class="form-wrapper">
                    <div class="clearfix form-group">
                        <label class="control-label col-lg-3 required">
                            {if isset($prestaBuyNowProduct)}
                                {l s='Product Name' mod='prestabuynowpaylater'}
                            {else if isset($addbulk)}
                                <span
                                    title=""
                                    data-html="true"
                                    data-toggle="tooltip"
                                    class="label-tooltip"
                                    data-original-title="{l s='Choose products to allow products for pay later' mod='prestabuynowpaylater'}">{l s='Choose Products' mod='prestabuynowpaylater'}
                                </span>
                            {else}
                                <span
                                    title=""
                                    data-html="true"
                                    data-toggle="tooltip"
                                    class="label-tooltip"
                                    data-original-title="{l s='Search product to allow for pay later' mod='prestabuynowpaylater'}">{l s='Search Product' mod='prestabuynowpaylater'}
                                </span>
                            {/if}
                        </label>
                        <div class="col-lg-4 select-presta-product">
                            {if isset($prestaBuyNowProduct)}
                                <input
                                    type="hidden"
                                    name="presta_selected_product"
                                    value="{$prestaBuyNowProduct->id_product|escape:'html':'UTF-8'}" />
                                <p class="selected-product">{$product_name|escape:'html':'UTF-8'}</p>
                            {else if isset($addbulk)}
                                <input type="hidden" name="presta_product_last_count" value="{$products|@count}"/>
                                <select name="presta_product[]" multiple="multiple" id="presta_product">
                                {if isset($products)}
                                    {foreach $products as $product}
                                        <option value="{$product.id_product|escape:'html':'UTF-8'}">({$product.id_product|escape:'html':'UTF-8'}) {$product.name|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                {else}
                                    <option value="0">{l s='No Product Found' mod='prestabuynowpaylater'}</option>
                                {/if}
                                </select>
                            {else}
                                <input type="hidden" name="presta_selected_product" value=""/>
                                <input type="text" class="form-control" name="presta_search_product" />
                                <div class="help-block">{l s='Type three or more characters to search the product' mod='prestabuynowpaylater'}</div>
                                <div class="clearfix searched-result"></div>
                                <div class="clearfix selected-result"></div>
                            {/if}
                        </div>
                        <div class="col-lg-2 presta_img_container">
                            <img width="32" src="{$modules_dir|escape:'html':'UTF-8'}prestabuynowpaylater/views/img/loader.gif"/>
                        </div>
                    </div>
                    <div class="clearfix form-group">
                        <label class="control-label col-lg-3 required">
                            <span
                                title=""
                                data-html="true"
                                data-toggle="tooltip"
                                class="label-tooltip"
                                data-original-title="{l s='If Enabled, Customer will able to use pay later payment method and its features' mod='prestabuynowpaylater'}">
                                    {l s='Allow Buy Now Pay Later: ' mod='prestabuynowpaylater'}
                            </span>
                        </label>
                        <div class="col-lg-9">
                            <span class="switch prestashop-switch fixed-width-lg">
                                <input
                                    type="radio"
                                    name="presta_paylater_enable"
                                    id="presta_paylater_enable_on"
                                    value="1"
                                    {if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == 1}checked="checked"
                                    {else if isset($prestaBuyNowProduct) && $prestaBuyNowProduct->active == 1}checked="checked"
                                    {else if !isset($smarty.post.presta_paylater_enable)}checked="checked"{/if}>
                                    <label for="presta_paylater_enable_on">{l s='Yes' mod='prestabuynowpaylater'}</label>
                                <input
                                    type="radio"
                                    name="presta_paylater_enable"
                                    id="presta_paylater_enable_off"
                                    value="0"
                                    {if isset($prestaBuyNowProduct) && $prestaBuyNowProduct->active == '0'}checked="checked"{else if isset($smarty.post.presta_paylater_enable) && $smarty.post.presta_paylater_enable == '0'}checked="checked"{/if}>
                                <label for="presta_paylater_enable_off">{l s='No' mod='prestabuynowpaylater'}</label>
                                <a class="slide-button btn"></a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button
                        type="submit"
                        value="1"
                        name="submitProductRestriction"
                        class="btn btn-default pull-right">
                        <i class="process-icon-save"></i> {l s='Save' mod='prestabuynowpaylater'}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
