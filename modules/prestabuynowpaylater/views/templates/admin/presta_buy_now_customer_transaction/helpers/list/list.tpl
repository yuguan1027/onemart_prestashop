 {**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 *}

<div class="row">
    <div class="col-lg-12">
        <form
            action="{$current|escape:'html':'UTF-8'}&{if !empty($submit_action)}{$submit_action|escape:'html':'UTF-8'}{/if}&token={$token|escape:'html':'UTF-8'}"
            method="post"
            novalidate=""
            id="configuration_form"
            enctype="multipart/form-data"
            class="defaultForm form-horizontal">
            <input type="hidden" name="submitProductRestriction" value="1">
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    Customer Details
                </div>
                <div class="form-wrapper">
                    <section id="presta_main">
                        <p>
                            <span>
                                {l s='Current Usage' mod='prestabuynowpaylater'}
                                {if isset($unbilled)}{$unbilled|escape:'html':'UTF-8'}{else}0.00{/if}
                            </span>
                            <span> | </span>
                            <span>{l s='Balance' mod='prestabuynowpaylater'} </span>
                            <span class="presta_balance">{if isset($balance)}{$balance|escape:'html':'UTF-8'}{/if}</span>
                            <span class="presta_right">{l s='Limit' mod='prestabuynowpaylater'}
                                {if isset($limit)}{$limit|escape:'html':'UTF-8'}{/if}
                            </span>
                        </p>
                        <div class="presta_price_usage_container">
                            <div class="presta_used"></div>
                        </div>
                		<hr>
                        {if isset($unbilled)}
                            <div class="clearfix">
                                <p>{l s='Unbilled Amount' mod='prestabuynowpaylater'}</p>
                                <div>
                                    <span style="font-size:22px;font-weight:bolder;">{if isset($unbilled)}{$unbilled|escape:'html':'UTF-8'}{else}0.00{/if}</span>

                                    {if isset($unbilled_amount) && $unbilled_amount|escape:'html':'UTF-8' > 0}
                                    <div class="form-check">
                                        <label style="font-size:14px;margin:10px;" class="form-check-label">
                                            <input id="radio_total_outstandings" type="radio" class="form-check-input" name="optradio"> {l s='Pay Total Outstandings' mod='prestabuynowpaylater'}
                                        </label>
                                    </div>
                                        <form method="post" action="{$prestaAdminLink|escape:'html':'UTF-8'}">
                                            <div>
                                                <input
                                                type="hidden"
                                                value="AdminPrestaBuyNowCustomerTransaction"
                                                name="controller"
                                                />
                                                <input
                                                    type="hidden"
                                                    value="{$idCustomer|escape:'html':'UTF-8'}"
                                                    name="id_customer"
                                                    id="id_customer"
                                                    />
                                                <input
                                                    type="hidden"
                                                    value ="{$unbilled_amount|escape:'html':'UTF-8'}"
                                                    name="presta_partial_amount"
                                                    />
                                            </div>
                                            <p style="display:none; width:50%;" id="presta_remaining_amount_msg"></p>
                                            <div id="presta_remaining_amount_div" class="clearfix" style="display:none;width:50%;">
                                                <span id="presta_remaining_amount">
                                                    {if isset($paymentModules)}
                                                        <div class="form-group">
                                                            <label for="paymentOptions"> {l s='Select payment method' mod='prestabuynowpaylater'}</label>
                                                            <select name="presta_payment_options" class="form-control">
                                                            {foreach $paymentModules as $methods}
                                                                {if $methods.name != prestabuynowpaylater}
                                                                <option value="{$methods.id_module|escape:'html':'UTF-8'}">{$methods.name|escape:'html':'UTF-8'}</option>
                                                                {/if}
                                                            {/foreach}
                                                            </select>
                                                        </div>
                                                    {/if}
                                                    <div class="form-group prestaForm">
                                                        <label>
                                                        {l s='Order Status:' mod='prestabuynowpaylater'}
                                                        </label>
                                                        <select name="presta_order_status" id="" class="form-control">
                                                            {foreach $presta_status as $status}
                                                                <option value="{$status.id_order_state|escape:'html':'UTF-8'}">{$status.name|escape:'html':'UTF-8'}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </span>
                                            </div>
                                            <div>
                                                <button
                                                    id="presta_total_pay_btn"
                                                    style="margin:0 0 0 22px;"
                                                    type="submit"
                                                    name="presta_total_pay"
                                                    class="btn btn-primary">
                                                    {l s='Pay Now' mod='prestabuynowpaylater'}
                                                </button>
                                            </div>
                                        </form>
                                    <div class="form-check">
                                        <label style="font-size:14px;margin:10px;" class="form-check-label">
                                            <input id="radio_other_amount" type="radio" class="form-check-input" name="optradio">  {l s='Pay Other Amount' mod='prestabuynowpaylater'}
                                        </label>
                                    </div>
                                    {/if}
                                </div>
                            </div>
                            <div class="clearfix presta_other_amount" style="display:none;">
                                {if isset($unbilled_amount) && $unbilled_amount|escape:'html':'UTF-8' > 0}
                                <form method="post" action="{$prestaAdminLink|escape:'html':'UTF-8'}">
                                    <div class="clearfix col-md-2">
                                        <input
                                            type="hidden"
                                            value="AdminPrestaBuyNowCustomerTransaction"
                                            name="controller"
                                            />
                                        <input
                                            type="hidden"
                                            value="{$idCustomer|escape:'html':'UTF-8'}"
                                            name="id_customer"
                                            id="id_customer"
                                            />
                                        <input
                                            style="width:150px;margin:0 0 0 18px;"
                                            type="text"
                                            data-id="{$unbilled_amount|escape:'html':'UTF-8'}"
                                            name="presta_partial_amount"
                                            id="presta_partial_amount"
                                            class="form-control"
                                            />
                                    </div>
                                    <br><br>
                                    <div id="presta_partial_payment_options" class="clearfix" style="display:none;width:50%;">
                                        <br>
                                            <span id="presta_remaining_amount">
                                                {if isset($paymentModules)}
                                                <div class="form-group">
                                                    <label for="paymentOptions"> {l s='Select payment method' mod='prestabuynowpaylater'}</label>
                                                    <select name="presta_partial_payment_options" class="form-control">
                                                    {foreach $paymentModules as $methods}
                                                        {if $methods.name != prestabuynowpaylater}
                                                        <option value="{$methods.id_module|escape:'html':'UTF-8'}">{$methods.name|escape:'html':'UTF-8'}</option>
                                                        {/if}
                                                    {/foreach}
                                                    </select>
                                                </div>
                                                {/if}
                                                <div class="form-group prestaForm">
                                                    <label>
                                                    {l s='Order Status:' mod='prestabuynowpaylater'}
                                                    </label>
                                                    <select name="presta_order_status" id="" class="form-control">
                                                        {foreach $presta_status as $status}
                                                            <option value="{$status.id_order_state|escape:'html':'UTF-8'}">{$status.name|escape:'html':'UTF-8'}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </span>
                                    </div>
                                    <div class="clearfix col-md-10">
                                        <button
                                            type="submit"
                                            name="presta_partial_pay"
                                            class="btn btn-primary">
                                            {l s='Pay Now' mod='prestabuynowpaylater'}
                                        </button>
                                    </div>
                                </form>
                                {/if}
                            </div>

                        {/if}
                        <div class="clearfix presta_transaction">
                            <p class="presta_heading">Transaction History</p>
                            <section class="clearfix presta-page-content">
                                {if isset($alltransaction) && $alltransaction|escape:'html':'UTF-8'}
                                    {foreach from=$alltransaction key=key item=months}
                                        {foreach from=$months key=mkey item=month}
                                            <div class="clearfix presta_transaction_block">
                                                <div class="clearfix presta_transaction_head">
                                                    <span>{$mkey|escape:'html':'UTF-8'} {$key|escape:'html':'UTF-8'}</span>
                                                    <span class="presta_right">
                                                        {if isset($unbilled_amount) && $unbilled_amount|escape:'html':'UTF-8' > 0}
                                                            <span>{l s='Unbilled' mod='prestabuynowpaylater'}</span>
                                                        {else}
                                                            <span>{l s='Paid' mod='prestabuynowpaylater'}</span>
                                                        {/if}
                                                        <span>|</span>
                                                        <span>{l s='Net Total:' mod='prestabuynowpaylater'} {$unbilled|escape:'html':'UTF-8'}</span>
                                                    </span>
                                                </div>
                                                {foreach from=$month key=k item=value}
                                                    <div class="clearfix presta_transaction_details">
                                                        <div class="col-md-12 presta_inner">
                                                            <div class="col-md-3 presta_date">
                                                                {$value.date_add|date_format:"%A, %B %e"}
                                                            </div>
                                                            {foreach from=$value.products|@array_slice:0:1 key=p item=product}
                                                                <div class="col-md-7 presta_item">
                                                                    {if $value.transaction_for == 1}
                                                                        {$product.name|truncate:40}
                                                                    {else if $value.transaction_for == 2}
                                                                        {l s='Pay Later Payment' mod='prestabuynowpaylater'}
                                                                    {else if $value.transaction_for == 3}
                                                                        <span class="late-fee">{l s='Late Payment Fee' mod='prestabuynowpaylater'}</span>
                                                                    {/if}
                                                                    {if $value.products|@count > 1}
                                                                        <a
                                                                            class="presta_more"
                                                                            id="{$value.id|escape:'html':'UTF-8'}"
                                                                            href="javascript:void(0);">
                                                                            +{$value.products|@count -1} Items
                                                                        </a>
                                                                    {/if}
                                                                </div>
                                                                <div class="col-md-2 presta_amount">
                                                                    {if $value.transaction_for|escape:'html':'UTF-8' == 1}
                                                                        <span class="presta_dr_transaction">{$value.dr_amount|escape:'html':'UTF-8'}</span>
                                                                    {else if $value.transaction_for|escape:'html':'UTF-8' == 2}
                                                                        <span class="presta_cr_transaction">- {$value.cr_amount|escape:'html':'UTF-8'}</span>
                                                                    {else if $value.transaction_for|escape:'html':'UTF-8' == 3}
                                                                        <span class="presta_late_fee_transaction">{$value.dr_amount|escape:'html':'UTF-8'}</span>
                                                                    {/if}
                                                                </div>
                                                            {/foreach}
                                                        </div>
                                                        <div class="clearfix presta_order_detail" id="presta_order_detail_{$value.id|escape:'html':'UTF-8'}">
                                                            {foreach from=$value.products key=p item=product}
                                                                <div class="col-md-12 presta_inner">
                                                                    <div class="col-md-4"></div>
                                                                    <div class="col-md-4">
                                                                        {if $value.transaction_for|escape:'html':'UTF-8' == 1}
                                                                            {$product.name|escape:'html':'UTF-8'}
                                                                        {else if $value.transaction_for|escape:'html':'UTF-8' == 2}
                                                                            {l s='Pay Later Payment' mod='prestabuynowpaylater'}
                                                                        {else if $value.transaction_for|escape:'html':'UTF-8' == 3}
                                                                            {l s='Late Payment Fee' mod='prestabuynowpaylater'}
                                                                        {/if}
                                                                    </div>
                                                                    <div class="col-md-2">{$product.total|escape:'html':'UTF-8'}</div>
                                                                </div>
                                                            {/foreach}
                                                        </div>
                                                        <div
                                                            class="presta_order_reference">
                                                            <div class="presta_order_ref presta_left">
                                                                {l s='Order Reference:' mod='prestabuynowpaylater'}
                                                            </div>
                                                            <div class="presta_order_num presta_left">
                                                                {if $value.reference|escape:'html':'UTF-8'}
                                                                    {$value.reference|escape:'html':'UTF-8'}
                                                                {else}
                                                                    {l s='N/A' mod='prestabuynowpaylater'}
                                                                {/if}
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            </div>
                                            <br/>
                                        {/foreach}
                                    {/foreach}
                                {else}
                                    <p>{l s='No record found!' mod='prestabuynowpaylater'}</p>
                                {/if}
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
    {if !$used > 0}
        .presta_used {
            background-color: inherit !important;
        }
    {/if}
    .presta_used {
        width: {$used|escape:'html':'UTF-8'}% !important;
    }
</style>
