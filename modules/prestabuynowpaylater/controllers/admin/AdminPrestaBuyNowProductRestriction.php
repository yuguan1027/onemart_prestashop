<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowProductRestrictionController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
        $this->className = 'PrestaBuyNowProduct';
        $this->_select = '
            pl.`name` as product_name,
            cl.`name` as category_name';
        $default = Configuration::get('PS_LANG_DEFAULT');
        $this->list_no_link = true;

        $this->table = 'presta_buynow_allowed_product';
        $this->_join .= 'LEFT JOIN `'. _DB_PREFIX_.'product` p ON (a.`id_product` = p.`id_product`) ';
        $this->_join .= 'LEFT JOIN `'. _DB_PREFIX_.'product_lang` pl ON (pl.`id_product` = p.`id_product`) ';
        $this->_join .= 'LEFT JOIN `'. _DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category`) ';
        $this->_where = ' AND pl.id_lang = '.$default.' AND cl.id_lang = '.$default;

        $this->fields_list = array(
            'id' => array(
                'title' => $this->l('Id') ,
                'align' => 'center'
            ),
            'id_product' => array(
                'title' => $this->l('Id Product') ,
                'align' => 'center',
                'havingFilter' => true,
            ),
            'product_name' => array(
                'title' => $this->l('Product Name'),
                'align' => 'center',
                'havingFilter' => true,
            ),
            'category_name' => array(
                'title' => $this->l('Default Category'),
                'align' => 'center',
                'havingFilter' => true,
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'align' => 'center',
                'type' => 'bool',
                'active' => 'status',
                'havingFilter' => true,
            )
        );
        $this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );
    }

    public function initToolbar()
    {
        parent::initToolBar();
        $this->page_header_toolbar_btn['bulk_update'] = array(
            'href' => self::$currentIndex.'&add'.$this->table.'&addbulk=1&token='.$this->token,
            'icon' => 'process-icon-new',
            'desc' => $this->l('Bulk Update')
        );
        $this->page_header_toolbar_btn['new'] = array(
            'href' => self::$currentIndex.'&add'.$this->table.'&token='.$this->token,
            'desc' => $this->l('Search Product')
        );
    }

    public function renderForm()
    {
        Media::addJsDef(
            array(
                'presta_admin_controller' => $this->context->link->getAdminLink('AdminPrestaBuyNowProductRestriction')
            )
        );
        $products = Product::getProducts($this->context->language->id, 0, 10, 'id_product', 'DESC');
        if ($products) {
            $this->context->smarty->assign(
                array(
                    'products' => $products
                )
            );
        }
        $id = Tools::getValue('id');
        if ($id) {
            $prestaBuyNowProduct = new PrestaBuyNowProduct($id);
            $this->context->smarty->assign(
                array(
                    'product_name' => Product::getProductName($prestaBuyNowProduct->id_product),
                    'prestaBuyNowProduct' => $prestaBuyNowProduct
                )
            );
        }
        $addbulk = Tools::getValue('addbulk');
        if ($addbulk) {
            $this->context->smarty->assign(
                array(
                    'addbulk' => 1
                )
            );
        }
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Checked Customer Group Will Access Buy Now Pay Later '),
                'icon' => 'icon-user'
            ),
            'submit' => array(
                'name' => 'submitProductRestriction',
                'title' => $this->l('Save'),
            ),
        );
        $this->context->smarty->assign(
            array(
                'modules_dir' => _MODULE_DIR_
            )
        );
        return parent::renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitProductRestriction')) {
            $id = Tools::getValue('id');
            $products = Tools::getValue('presta_product');
            $idProduct = Tools::getValue('presta_selected_product');
            $status = Tools::getValue('presta_paylater_enable');
            if ($id) {
                $prestaBuyNowProduct = new PrestaBuyNowProduct($id);
                if ($prestaBuyNowProduct->id_product != $idProduct) {
                    $this->errors[] = $this->l('Product Id is not valid');
                } else {
                    $prestaBuyNowProduct->id_product = $idProduct;
                    $prestaBuyNowProduct->active = $status;
                    $prestaBuyNowProduct->save();
                    Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
                }
            } else {
                if ($products) {
                    foreach ($products as $idProduct) {
                        $prestaBuyNowProduct = new PrestaBuyNowProduct();
                        $isExist = $prestaBuyNowProduct->checkProduct($idProduct);
                        if ($isExist) {
                            $prestaBuyNowProduct = new PrestaBuyNowProduct($isExist['id']);
                        }
                        $prestaBuyNowProduct->id_product = $idProduct;
                        $prestaBuyNowProduct->active = $status;
                        $prestaBuyNowProduct->save();
                        unset($prestaBuyNowProduct);
                    }
                    Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
                } elseif ($idProduct) {
                    $prestaBuyNowProduct = new PrestaBuyNowProduct();
                    $isExist = $prestaBuyNowProduct->checkProduct($idProduct);
                    if ($isExist) {
                        $prestaBuyNowProduct = new PrestaBuyNowProduct($isExist['id']);
                    }
                    $prestaBuyNowProduct->id_product = $idProduct;
                    $prestaBuyNowProduct->active = $status;
                    $prestaBuyNowProduct->save();
                    Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
                }
            }
        }

        if ($this->display == 'view') {
            if (Tools::getValue('id')) {
                $prestaBuyNowProduct = new PrestaBuyNowProduct(Tools::getValue('id'));
                if (Validate::isLoadedObject($prestaBuyNowProduct)) {
                    Tools::redirectAdmin(
                        $this->context->link->getAdminLink(
                            'AdminProducts',
                            true,
                            array('id_product' => $prestaBuyNowProduct->id_product)
                        )
                    );
                }
            }
        }

        if (Tools::isSubmit('submitBulkdelete'.$this->table)) {
            $selectedValues = Tools::getValue($this->table.'Box');
            if ($selectedValues) {
                foreach ($selectedValues as $value) {
                    $prestaBuyNowProduct = new PrestaBuyNowProduct($value);
                    $prestaBuyNowProduct->delete();
                }
                Tools::redirectAdmin(self::$currentIndex.'&conf=2&token='.$this->token);
            }
        }

        if (Tools::isSubmit('submitBulkdisableSelection'.$this->table)) {
            $selectedValues = Tools::getValue($this->table.'Box');
            if ($selectedValues) {
                foreach ($selectedValues as $value) {
                    $prestaBuyNowProduct = new PrestaBuyNowProduct($value);
                    $prestaBuyNowProduct->active = 0;
                    $prestaBuyNowProduct->update();
                }
                Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
        }

        if (Tools::isSubmit('submitBulkenableSelection'.$this->table)) {
            $selectedValues = Tools::getValue($this->table.'Box');
            if ($selectedValues) {
                foreach ($selectedValues as $value) {
                    $prestaBuyNowProduct = new PrestaBuyNowProduct($value);
                    $prestaBuyNowProduct->active = 1;
                    $prestaBuyNowProduct->update();
                }
                Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
        }

        parent::postProcess();
    }

    public function setMedia($isNewTheme = false)
    {
        if ($isNewTheme || !$isNewTheme) {
            parent::setMedia(false);
            $this->context->controller->addJs(
                _MODULE_DIR_.'prestabuynowpaylater/views/js/admin/jquery.multiselect.js'
            );
            $this->context->controller->addJs(
                _MODULE_DIR_.'prestabuynowpaylater/views/js/admin/presta_paylater_admin.js'
            );
            $this->context->controller->addCSS(
                _MODULE_DIR_.'prestabuynowpaylater/views/css/admin/jquery.multiselect.css'
            );
            $this->context->controller->addCSS(
                _MODULE_DIR_.'prestabuynowpaylater/views/css/admin/presta_paylater_admin.css'
            );
        }
    }

    public function ajaxProcessGetMoreProducts()
    {
        $products = array();
        $load = Configuration::get('PRESTA_PAYLATER_LOAD_MORE_PRODUCT');
        if (!$load) {
            $load = 10;
        }
        $lastCount = Tools::getValue('lastCount');
        if (!$lastCount) {
            $lastCount = 0;
        }
        $result = Product::getProducts($this->context->language->id, $lastCount, 10, 'id_product', 'DESC');
        $products = array(
            'currentCount' => (int) $lastCount + count($result),
            'products' => $result
        );
        die(json_encode($products));
    }

    public function ajaxProcessSearchProduct()
    {
        $products = array();
        $query = Tools::getValue('query');
        if (!$query) {
            $products = array(
                'status' => 'ko',
                'msg' => $this->l('Please type key words to search products')
            );
        } else {
            $result = Product::searchByName($this->context->language->id, $query);
            if ($result) {
                $products = array(
                    'status' => 'ok',
                    'products' => $result
                );
            } else {
                $products = array(
                    'status' => 'ko',
                    'msg' => $this->l('No record found')
                );
            }
        }
        die(json_encode($products));
    }
}
