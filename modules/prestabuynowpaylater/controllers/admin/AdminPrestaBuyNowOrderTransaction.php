<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowOrderTransactionController extends ModuleAdminController
{
    protected $statuses_array = array();

    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
        $this->table = 'presta_buynow_customer_transaction';
        $this->className = 'PrestaBuyNowTransaction';
        $this->list_no_link = true;

        $this->_select = '
            a.`id_customer` as presta_id_customer,
            c.`email` as presta_email,
            osl.`name` AS `osname`,
            os.`color`,
            os.`id_order_state` as id_order_state,
            CONCAT(c.`firstname`, \' \', c.`lastname`) AS `customer`,
            o.`reference` as `reference`';

        $this->_join .= 'JOIN `'._DB_PREFIX_.'customer` c ON  (a.`id_customer` = c.`id_customer` )';
        $this->_join .= 'JOIN `'._DB_PREFIX_.'orders` o ON  (a.`id_cart`= o.`id_cart`)';
        $this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = o.`current_state`)';
        $this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON
            (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)$this->context->language->id.')';
        $this->_where .= ' AND a.`dr_amount` > 0';

        $statuses = OrderState::getOrderStates((int)$this->context->language->id);
        foreach ($statuses as $status) {
            $this->statuses_array[$status['id_order_state']] = $status['name'];
        }
        $this->fields_list = array(
            'presta_id_customer' => array(
                'title' => $this->l('Customer Id') ,
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'customer' => array(
                'title' => $this->l('Customer') ,
                'align' => 'center',
                'havingFilter' => true,
            ),
            'presta_email' => array(
                'title' => $this->l('Email') ,
                'align' => 'center',
                'havingFilter' => true,
            ),
            'reference' => array(
                'title' => $this->l('Order Reference') ,
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'dr_amount' => array(
                'title' => $this->l('Order Total') ,
                'align' => 'center',
                'type' => 'price',
                'currency' => true,
                'havingFilter' => true,
            ),
            'osname' => array(
                'title' => $this->l('Status'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->statuses_array,
                'filter_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'order_key' => 'osname',
                'havingFilter' => true,
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            ),
        );

        $this->addRowAction('view');
    }

    public function initToolbar()
    {
        parent::initToolBar();
        unset($this->toolbar_btn['new']);
    }

    public function postProcess()
    {
        if ($this->display == 'view') {
            if (Tools::getValue('id')) {
                $transaction = new PrestaBuyNowTransaction(Tools::getValue('id'));
                if (Validate::isLoadedObject($transaction)) {
                    $order = Order::getByCartId($transaction->id_cart);
                    Tools::redirectAdmin(
                        $this->context->link->getAdminLink('AdminOrders').'&vieworder&id_order='.(int)$order->id
                    );
                }
            }
        }
        parent::postProcess();
    }
}
