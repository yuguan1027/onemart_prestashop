<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowCategoryRestrictionController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
    }

    public function initToolbar()
    {
        parent::initToolBar();
        unset($this->toolbar_btn['new']);
        $this->toolbar_btn['export_theme'] = array(
            'href' => self::$currentIndex.'&action=exporttheme&token='.$this->token,
            'desc' => $this->l('Export theme', null, null, false),
            'icon' => 'process-icon-export'
        );
    }

    public function initContent()
    {
        $this->context->smarty->assign(
            array(
                'current' => self::$currentIndex,
                'token' => $this->token,
                'content' => $this->content,
                'prestacontroller' => $this->controller_name
            )
        );
        $this->content = $this->context->smarty->fetch(
            _PS_MODULE_DIR_.'prestabuynowpaylater/views/templates/admin/_partials/additional_info.tpl'
        );
        $this->content .= $this->renderForm();
        parent::initContent();
    }

    public function renderForm()
    {
        $shop = new Shop((int) $this->context->shop->id);
        $id_root = $shop->id_category;
        $selected_cat = Configuration::get('PRESTA_CATEGORY_RESTRICTION');
        if ($selected_cat) {
            $selected_cat = unserialize($selected_cat);
        }
        if (!$selected_cat) {
            $selected_cat = array();
        }

        if (Shop::getContext() == Shop::CONTEXT_SHOP && Tools::isSubmit('id_shop')) {
            $root_category = new Category($shop->id_category);
        } else {
            $root_category = new Category($id_root);
        }

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Checked Categories Will Access Buy Now Pay Later '),
                'icon' => 'icon-user'
            ),
            'input' => array(
                array(
                    'type' => 'categories',
                    'name' => 'categoryBox',
                    'label' => $this->trans('Associated categories', array(), 'Admin.Catalog.Feature'),
                    'tree' => array(
                        'id' => 'categories-tree',
                        'selected_categories' => $selected_cat,
                        'root_category' => $root_category->id,
                        'use_search' => true,
                        'use_checkbox' => true
                    ),
                    'desc' => $this->l(
                        'By selecting associated categories, you are allowing products to get elibile for pay later.'
                    )
                )
            ),
            'submit' => array(
                'name' => 'submitCategoryRestriction',
                'title' => $this->l('Save'),
            ),
        );
        return parent::renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitCategoryRestriction')) {
            $categoryBox = Tools::getValue('categoryBox');
            if (!$categoryBox) {
                $categoryBox = 0;
            }
            Configuration::updateValue('PRESTA_CATEGORY_RESTRICTION', serialize($categoryBox));
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
        }
    }
}
