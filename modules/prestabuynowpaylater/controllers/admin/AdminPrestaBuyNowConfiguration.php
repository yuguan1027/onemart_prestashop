<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowConfigurationController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
    }

    public function initToolbar()
    {
        parent::initToolBar();
        unset($this->toolbar_btn['new']);
    }

    public function initContent()
    {
        if (!$this->ajax) {
            $this->content .= $this->renderForm();
        }

        $this->context->smarty->assign(
            array(
                'current' => self::$currentIndex,
                'token' => $this->token,
                'content' => $this->content,
            )
        );
        parent::initContent();
    }

    public function renderForm()
    {
        $prestatab = Tools::getValue('prestatab');
        if (!$prestatab) {
            $prestatab = 1;
        }
        $groups = OrderState::getOrderStates($this->context->language->id);
        if ($groups) {
            $orderStatus = Configuration::get('PRESTA_PAYLATER_PAYMENT_ORDER_STATUS');
            if ($orderStatus) {
                $orderStatus = unserialize($orderStatus);
                if (!$orderStatus) {
                    $orderStatus = array();
                }
            }
            foreach ($groups as $key => $group) {
                if (in_array($group['id_order_state'], $orderStatus)) {
                    $groups[$key]['selected'] = 1;
                }
            }
        }
        $cronUrl = _PS_BASE_URL_.
            _MODULE_DIR_.
            $this->module->name.
            '/prestabuynowupdate.php?token='.
            Tools::substr(Tools::encrypt($this->module->name.'/cron'), 0, 10);
        $result = CMS::getCMSPages($this->context->language->id);
        $cmsArray = array();
        if ($result) {
            foreach ($result as $key => $data) {
                $cmsArray[$key] = array(
                    'id' => $data['id_cms'],
                    'name' => $data['meta_title']
                );
            }
        }
        $this->context->smarty->assign(
            array(
                'cmsArray' => $cmsArray,
                'cronUrl' => $cronUrl,
                'groups' => $groups,
                'prestatab' => $prestatab,
                'prestamoduledir' => _PS_MODULE_DIR_,
                'presta_currency_symbol' => $this->context->currency->sign,
                'currentIndex' => AdminController::$currentIndex.
                    '&token='.Tools::getValue('token')
            )
        );
        $this->fields_form = array(
            'legend' => array(
                'title' =>  $this->l('Buy Now Pay Later Configuration'),
            ),
        );
        return parent::renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('presta_paylater_general')) {
            $paylaterEnable = Tools::getValue('presta_paylater_enable');
            $partialEnable = Tools::getValue('presta_partial_enable');
            $paylaterGlobalLimit = trim(Tools::getValue('presta_paylater_global_limit'));
            $paylaterEligible = Tools::getValue('presta_paylater_eligible');
            $paylaterOrderRestriction = Tools::getValue('presta_paylater_order_restriction');
            $paylaterOrderRestrictionNo = trim(Tools::getValue('presta_paylater_order_restriction_no'));
            $paylaterLoadMore = trim(Tools::getValue('presta_paylater_load_more_product'));
            $paylaterAllCustomer = trim(Tools::getValue('presta_paylater_all_customer_enable'));
            $paylaterFAQ = Tools::getValue('presta_paylater_faq');
            $paylaterCMSPage = Tools::getValue('presta_late_cms_page');

            if ($paylaterGlobalLimit) {
                if (!Validate::isInt($paylaterGlobalLimit)) {
                    $this->errors[] = $this->l('Global limit must be valid positive integer value');
                } elseif ($paylaterGlobalLimit <= 0) {
                    $this->errors[] = $this->l('Global limit must be greater than zero');
                }
            } else {
                $this->errors[] = $this->l('Global limit can not be empty');
            }

            if ($paylaterOrderRestriction) {
                if ($paylaterOrderRestrictionNo) {
                    if (!Validate::isInt($paylaterOrderRestrictionNo)) {
                        $this->errors[] = $this->l('Number of orders must be valid interger value');
                    } elseif ($paylaterOrderRestrictionNo <= 0) {
                        $this->errors[] = $this->l('Number of orders must be greater than zero');
                    }
                } else {
                    $this->errors[] = $this->l('Number of orders can not be empty');
                }
            }

            if (empty($this->errors)) {
                Configuration::updateValue('PRESTA_PAYLATER_ENABLE', $paylaterEnable);
                Configuration::updateValue('PRESTA_PARTIAL_ENABLE', $partialEnable);
                Configuration::updateValue('PRESTA_PAYLATER_ALL_CUSTOMER', $paylaterAllCustomer);
                Configuration::updateValue('PRESTA_PAYLATER_GLOBAL_LIMIT', $paylaterGlobalLimit);
                Configuration::updateValue('PRESTA_PAYLATER_ELIGIBLE', $paylaterEligible);
                Configuration::updateValue('PRESTA_PAYLATER_ORDER_RESTRICTION', $paylaterOrderRestriction);
                if (!$paylaterOrderRestrictionNo) {
                    $paylaterOrderRestrictionNo = 0;
                }
                Configuration::updateValue('PRESTA_PAYLATER_ORDER_RESTRICTION_NO', $paylaterOrderRestrictionNo);
                Configuration::updateValue('PRESTA_PAYLATER_LOAD_MORE_PRODUCT', $paylaterLoadMore);
                Configuration::updateValue('PRESTA_PAYLATER_FAQ', $paylaterFAQ);
                Configuration::updateValue('PRESTA_PAYLATER_CMS_PAGE', $paylaterCMSPage);
                Tools::redirectAdmin(self::$currentIndex.'&prestatab=1&conf=4&token='.$this->token);
            }
        } elseif (Tools::isSubmit('presta_paylater_fee_config')) {
            $paylaterDueDate = Tools::getValue('presta_paylater_due_date');

            $nextDate = strtotime('+1 month', strtotime(date('Y-m-d')));
            $nextDate = date('Y-m-d', $nextDate);
            $nDate = date('d', strtotime($nextDate));
            $diffDate = $paylaterDueDate - $nDate;
            $nextDueDate = date('Y-m-d', strtotime($diffDate.' day', strtotime($nextDate)));

            $paylaterLateFeeEnable = Tools::getValue('presta_paylater_late_fee_enable');
            $lateFeeType = Tools::getValue('presta_late_fee_type');
            $paylaterLateFeeValue = trim(Tools::getValue('presta_paylater_late_fee_value'));
            $paylaterStopAfterDueDate = Tools::getValue('presta_paylater_stop_after_due_date');

            if ($paylaterLateFeeEnable) {
                if ($lateFeeType == 1) {
                    if (!Validate::isInt($paylaterLateFeeValue)) {
                        $this->errors[] = $this->l('Percentage value must be valid.');
                    } elseif ($paylaterLateFeeValue < 0) {
                        $this->errors[] = $this->l('Percentage value must be greater than zero.');
                    } elseif ($paylaterLateFeeValue > 100) {
                        $this->errors[] = $this->l('Percentage value must be less than 100.');
                    }
                } else {
                    if (!Validate::isInt($paylaterLateFeeValue)) {
                        $this->errors[] = $this->l('Fixed value value must be valid.');
                    } elseif ($paylaterLateFeeValue < 0) {
                        $this->errors[] = $this->l('Fixed value must be greater than zero.');
                    }
                }
            }
            if (empty($this->errors)) {
                Configuration::updateValue('PRESTA_PAYLATER_DUE_DATE', $paylaterDueDate);
                Configuration::updateValue('PRESTA_PAYLATER_LATE_FEE', $paylaterLateFeeEnable);
                Configuration::updateValue('PRESTA_PAYLATER_LATE_FEE_TYPE', $lateFeeType);
                Configuration::updateValue('PRESTA_PAYLATER_LATE_FEE_VALUE', $paylaterLateFeeValue);
                Configuration::updateValue('PRESTA_PAYLATER_STOP_AFTER_LATE_FEE', $paylaterStopAfterDueDate);
                Configuration::updateValue('PRESTA_PAYLATER_NEXT_DUE_DATE', $nextDueDate);
                Tools::redirectAdmin(self::$currentIndex.'&prestatab=2&conf=4&token='.$this->token);
            }
        } elseif (Tools::isSubmit('presta_paylater_order_status')) {
            $orderStatus = Tools::getValue('groupBox');
            if ($orderStatus) {
                Configuration::updateValue(
                    'PRESTA_PAYLATER_PAYMENT_ORDER_STATUS',
                    serialize($orderStatus)
                );
            } else {
                Configuration::updateValue('PRESTA_PAYLATER_PAYMENT_ORDER_STATUS', 0);
            }
            Tools::redirectAdmin(self::$currentIndex.'&prestatab=3&conf=4&token='.$this->token);
        } elseif (Tools::isSubmit('presta_paylater_mail')) {
            Configuration::updateValue(
                'PRESTA_PAYLATER_ORDER_MAIL_TO_CUST',
                Tools::getValue('presta_paylater_order_to_cust')
            );
            Configuration::updateValue(
                'PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN',
                Tools::getValue('presta_paylater_order_to_admin')
            );
            Configuration::updateValue(
                'PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_CUST',
                Tools::getValue('presta_paylater_payment_to_cust')
            );
            Configuration::updateValue(
                'PRESTA_PAYLATER_PAYMENT_SUCCESS_MAIL_TO_ADMIN',
                Tools::getValue('presta_paylater_payment_to_admin')
            );
            Configuration::updateValue(
                'PRESTA_PAYLATER_WARNING_MAIL_TO_CUST',
                Tools::getValue('presta_paylater_warning_to_cust')
            );
            Configuration::updateValue(
                'PRESTA_PAYLATER_WARNING_MAIL_BEFORE_DUE_DATE',
                Tools::getValue('presta_warning_before_due_date')
            );
            Tools::redirectAdmin(self::$currentIndex.'&prestatab=4&conf=4&token='.$this->token);
        }
        parent::postProcess();
    }

    public function setMedia($isNewTheme = false)
    {
        if ($isNewTheme || !$isNewTheme) {
            parent::setMedia(false);
            $this->context->controller->addJs(_PS_JS_DIR_.'jquery/plugins/jquery.colorpicker.js');
            $this->context->controller->addJs(
                _MODULE_DIR_.'prestabuynowpaylater/views/js/admin/presta_paylater_configuration.js'
            );
        }
    }
}
