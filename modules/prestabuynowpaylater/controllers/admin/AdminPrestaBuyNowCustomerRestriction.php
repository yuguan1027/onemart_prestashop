<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowCustomerRestrictionController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
        $this->className = 'PrestaBuyNowCustomer';
        $this->_select = '
            a.`id_customer` as presta_id_customer,
            CONCAT(c.`firstname`, \' \', c.`lastname`)  as customer_name';
        $this->list_no_link = true;
        $this->table = 'presta_buynow_customer_configuration';
        $this->_join .= 'LEFT JOIN `'. _DB_PREFIX_.'customer` c ON (a.`id_customer` = c.`id_customer`)';

        $this->fields_list = array(
            'id' => array(
                'title' => $this->l('Id') ,
                'align' => 'center'
            ),
            'presta_id_customer' => array(
                'title' => $this->l('Id Customer'),
                'align' => 'center',
                'havingFilter' => true
            ),
            'customer_name' => array(
                'title' => $this->l('Customer Name'),
                'align' => 'center',
                'havingFilter' => true,
            ),
            'paylater_limit' => array(
                'title' => $this->l('Customer Limit'),
                'align' => 'center',
                'type' => 'price',
                'havingFilter' => true,
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'align' => 'center',
                'type' => 'bool',
                'active' => 'status',
                'havingFilter' => true,
            )
        );
        $this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );
    }

    public function processSave()
    {
        if (Tools::isSubmit('submitCustomerRestriction')) {
            $id = Tools::getValue('id');
            $idCustomer = Tools::getValue('presta_selected_customer');

            if (!$idCustomer) {
                $this->errors[] = $this->l('Customer ID is missing');
            } elseif (!Validate::isInt($idCustomer)) {
                $this->errors[] = $this->l('Customer ID is not valid');
            }

            $status = Tools::getValue('presta_paylater_enable');
            $limit = Tools::getValue('presta_customer_limit');

            if ($status) {
                if (!$limit) {
                    $this->errors[] = $this->l('Limit value can not be empty');
                } elseif (!Validate::isFloat($limit)) {
                    $this->errors[] = $this->l('Limit value must be valid');
                } elseif ($limit < 0) {
                    $this->errors[] = $this->l('Limit value must be greater than zero');
                }
            }

            if (!$limit && !$status) {
                $limit = 0;
            }

            if ($id) {
                $objPrestaBuyNowCustomer = new PrestaBuyNowCustomer($id);
                if ($idCustomer != $objPrestaBuyNowCustomer->id_customer) {
                    $this->errors[] = $this->l('Customer detail is not valid');
                }
            }

            if (empty($this->errors)) {
                $objPrestaBuyNowCustomer = new PrestaBuyNowCustomer();
                $isExist = $objPrestaBuyNowCustomer->getCustomerDetailByIdCustomer($idCustomer);
                if ($isExist) {
                    $objPrestaBuyNowCustomer = new PrestaBuyNowCustomer($isExist['id']);
                }
                $objPrestaBuyNowCustomer->id_customer = $idCustomer;
                $objPrestaBuyNowCustomer->paylater_limit = $limit;
                $objPrestaBuyNowCustomer->active = $status;
                $objPrestaBuyNowCustomer->save();
                Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
        }
    }

    public function renderForm()
    {
        Media::addJsDef(
            array(
                'presta_admin_controller' => $this->context->link->getAdminLink('AdminPrestaBuyNowCustomerRestriction')
            )
        );
        $this->context->smarty->assign(
            array(
                'presta_currency_symbol' => $this->context->currency->sign
            )
        );
        $id = Tools::getValue('id');
        $objCustomer = new PrestaBuyNowCustomer($id);
        if ($id) {
            $this->context->smarty->assign(
                array(
                    'presta_edit_customer' => $objCustomer,
                    'customer' => new Customer($objCustomer->id_customer)
                )
            );
        }
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Checked Customer Group Will Access Buy Now Pay Later '),
                'icon' => 'icon-user'
            ),
            'submit' => array(
                'name' => 'submitProductRestriction',
                'title' => $this->l('Save'),
            ),
        );
        return parent::renderForm();
    }

    public function postProcess()
    {
        if ($this->display == 'view') {
            if (Tools::getValue('id')) {
                $prestaBuyNowCustomer = new PrestaBuyNowCustomer(Tools::getValue('id'));
                if (Validate::isLoadedObject($prestaBuyNowCustomer)) {
                    Tools::redirectAdmin(
                        $this->context->link->getAdminLink('AdminCustomers').
                        '&viewcustomer&id_customer='.
                        (int)$prestaBuyNowCustomer->id_customer
                    );
                }
            }
        }
        if (Tools::isSubmit('submitBulkdelete'.$this->table)) {
            $selectedValues = Tools::getValue($this->table.'Box');
            if ($selectedValues) {
                foreach ($selectedValues as $value) {
                    $prestaBuyNowCustomer = new PrestaBuyNowCustomer($value);
                    $prestaBuyNowCustomer->delete();
                }
                Tools::redirectAdmin(self::$currentIndex.'&conf=2&token='.$this->token);
            }
        }
        if (Tools::isSubmit('submitBulkdisableSelection'.$this->table)) {
            $selectedValues = Tools::getValue($this->table.'Box');
            if ($selectedValues) {
                foreach ($selectedValues as $value) {
                    $prestaBuyNowCustomer = new PrestaBuyNowCustomer($value);
                    $prestaBuyNowCustomer->active = 0;
                    $prestaBuyNowCustomer->update();
                }
                Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
        }
        if (Tools::isSubmit('submitBulkenableSelection'.$this->table)) {
            $selectedValues = Tools::getValue($this->table.'Box');
            if ($selectedValues) {
                foreach ($selectedValues as $value) {
                    $prestaBuyNowCustomer = new PrestaBuyNowCustomer($value);
                    $prestaBuyNowCustomer->active = 1;
                    $prestaBuyNowCustomer->update();
                }
                Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
            }
        }
        parent::postProcess();
    }

    public function setMedia($isNewTheme = false)
    {
        if ($isNewTheme || !$isNewTheme) {
            parent::setMedia(false);
            $this->context->controller->addJs(
                _MODULE_DIR_.'prestabuynowpaylater/views/js/admin/presta_paylater_admin.js'
            );
            $this->context->controller->addCSS(
                _MODULE_DIR_.'prestabuynowpaylater/views/css/admin/presta_paylater_admin.css'
            );
        }
    }

    public function ajaxProcessSearchCustomer()
    {
        $customers = array();
        $query = Tools::getValue('query');
        if (!$query) {
            $customers = array(
                'status' => 'ko',
                'msg' => $this->l('Please type key words to search customers')
            );
        } else {
            $result = Customer::searchByName($query);
            if ($result) {
                $customers = array(
                    'status' => 'ok',
                    'customers' => $result
                );
            } else {
                $customers = array(
                    'status' => 'ko',
                    'msg' => $this->l('No record found')
                );
            }
        }
        die(json_encode($customers));
    }
}
