<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowCustomerGroupRestrictionController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
    }

    public function initToolbar()
    {
        parent::initToolBar();
        unset($this->toolbar_btn['new']);
        $this->toolbar_btn['export_theme'] = array(
            'href' => self::$currentIndex.'&action=exporttheme&token='.$this->token,
            'desc' => $this->l('Export theme', null, null, false),
            'icon' => 'process-icon-export'
        );
    }

    public function initContent()
    {
        $this->context->smarty->assign(
            array(
                'current' => self::$currentIndex,
                'token' => $this->token,
                'content' => $this->content,
                'prestacontroller' => $this->controller_name
            )
        );
        $this->content = $this->context->smarty->fetch(
            _PS_MODULE_DIR_.'prestabuynowpaylater/views/templates/admin/_partials/additional_info.tpl'
        );
        $this->content .= $this->renderForm();
        parent::initContent();
    }

    public function renderForm()
    {
        $groups = Group::getGroups($this->context->language->id, true);
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Checked Customer Group Will Access Buy Now Pay Later '),
                'icon' => 'icon-user'
            ),
            'input' => array(
                array(
                    'type' => 'group',
                    'label' => $this->l('Allowed Group access'),
                    'name' => 'groupBox',
                    'values' => $groups,
                    'required' => true,
                    'col' => '6',
                    'hint' => $this->l(
                        'Uncheck the customer group to prevent using buy now pay later features'
                    ),
                    'desc' => $this->l(
                        'If you want to restrict any customer group not to use buy now pay later feature
                        then uncheck that customer group'
                    )
                ),
            ),
            'submit' => array(
                'name' => 'submitCustomerGroup',
                'title' => $this->l('Save'),
            ),
        );
        $customerGroupsIds = Configuration::get('PRESTA_CUSTOMER_GROUP');
        if ($customerGroupsIds) {
            $customerGroupsIds = unserialize($customerGroupsIds);
            if (!$customerGroupsIds) {
                $customerGroupsIds = array();
            }
        } else {
            $customerGroupsIds = array();
        }
        foreach ($groups as $group) {
            $this->fields_value['groupBox_'.$group['id_group']] =
                Tools::getValue('groupBox_'.$group['id_group'], in_array($group['id_group'], $customerGroupsIds));
        }
        return parent::renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitCustomerGroup')) {
            $group = Tools::getValue('groupBox');
            if (!$group) {
                $group = 0;
            }
            Configuration::updateValue('PRESTA_CUSTOMER_GROUP', serialize($group));
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
        }
    }
}
