<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class AdminPrestaBuyNowCustomerTransactionController extends ModuleAdminController
{
    protected $statuses_array = array();

    public function __construct()
    {
        $this->bootstrap = true;
        $this->identifier = 'id';
        parent::__construct();
        $this->table = 'presta_buynow_customer_transaction';
        $this->className = 'PrestaBuyNowTransaction';
        $this->list_no_link = true;
        $this->_conf['119'] = $this->l(
            'Order can not be created because your order has exceeded the limit of buynowpaylater'
        );
        $this->_select = '
            a.`id_customer` as presta_id_customer,
            osl.`name` AS `osname`,
            os.`color`,
            os.`id_order_state` as id_order_state,
            CONCAT(c.`firstname`, \' \', c.`lastname`) AS `customer`,
            o.`reference` as `reference`';

        $this->_join .= 'JOIN `'._DB_PREFIX_.'customer` c ON  (a.`id_customer` = c.`id_customer` )';
        $this->_join .= 'JOIN `'._DB_PREFIX_.'orders` o ON  (a.`id_cart`= o.`id_cart`)';
        $this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = o.`current_state`)';
        $this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state`
            AND osl.`id_lang` = '.(int)$this->context->language->id.')';
        $this->_where .= ' AND a.`dr_amount` > 0';

        $statuses = OrderState::getOrderStates((int)$this->context->language->id);
        foreach ($statuses as $status) {
            $this->statuses_array[$status['id_order_state']] = $status['name'];
        }
        $this->fields_list = array(
            'presta_id_customer' => array(
                'title' => $this->l('Customer Id') ,
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'customer' => array(
                'title' => $this->l('Customer') ,
                'align' => 'center',
                'havingFilter' => true,
            ),
            'reference' => array(
                'title' => $this->l('Order Reference') ,
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'dr_amount' => array(
                'title' => $this->l('Order Total') ,
                'align' => 'center',
                'type' => 'price',
                'currency' => true,
                'havingFilter' => true,
            ),
            'osname' => array(
                'title' => $this->l('Status'),
                'type' => 'select',
                'color' => 'color',
                'list' => $this->statuses_array,
                'filter_key' => 'os!id_order_state',
                'filter_type' => 'int',
                'order_key' => 'osname',
                'havingFilter' => true,
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            ),
        );

        $this->addRowAction('view');
    }

    public function initToolbar()
    {
        parent::initToolBar();
        unset($this->toolbar_btn['new']);
    }

    public function initContent()
    {
        if (!$this->ajax) {
            $this->content .= $this->renderForm();
        }

        $this->context->smarty->assign(
            array(
                'current' => self::$currentIndex,
                'token' => $this->token,
                'content' => $this->content,
            )
        );

        parent::initContent();
    }

    public function postProcess()
    {
        $idCustomer = Tools::getValue('id_customer');
        $amount = Tools::getValue('presta_partial_amount');
        $payment_method = Tools::getValue('presta_payment_options');
        $status = Tools::getValue('presta_order_status');
        $partial_payment_method = Tools::getValue('presta_partial_payment_options');
        // dump($idCustomer);dump($amount);dump($partial_payment_method);
        if ($payment_method) {
            $methodName = Module::getInstanceById($payment_method);
        } elseif ($partial_payment_method) {
            $methodName = Module::getInstanceById($partial_payment_method);
        }
        if (Tools::isSubmit('presta_partial_pay')
            && $status
            && $amount
            && $idCustomer
            && Configuration::get('PRESTA_PAYLATER_ENABLE')
            && $partial_payment_method) {
            $this->processSpecificProduct($amount);
            $customer = new Customer($idCustomer);
            $this->context->cookie->id_customer = (int)($customer->id);
            $this->context->customer = $customer;
            $this->context->cart = new Cart();
            $address = $customer->getAddresses($this->context->language->id);
            $carrier = Carrier::getCarriers($idCustomer);
            // dump($address);die;
            if (!isset($address[0]['id_address'])) {
                echo 'please add an address for this customer';
                die;
            } else {
                $id_address = $address[0]['id_address'];
            }
            if (!isset($address[0]['id_address'])) {
                echo 'please add carrier for this customer';
                die;
            } else {
                $idCarrier = $carrier[0]['id_carrier'];
            }

            $this->context->cart->id_carrier = $idCarrier;
            $this->context->cart->id_address_invoice = $id_address;
            $this->context->cart->id_currency = $this->context->currency->id;
            $this->context->cart->add();
            $this->context->cookie->id_cart = (int)$this->context->cart->id;
            $this->context->cookie->write();
            $this->context->cart->id_customer = (int)$customer->id;
            $this->context->cart->secure_key = $customer->secure_key;
            $cartProducts = $this->context->cart->getProducts();
            // dump($this->context->cart);die;
            if ($cartProducts) {
                foreach ($cartProducts as $product) {
                    $this->context->cart->deleteProduct(
                        $product['id_product'],
                        $product['id_product_attribute']
                    );
                }
            }
            $this->context->cart->updateQty(1, Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID'));
            $total = (float)$this->context->cart->getOrderTotal(true, Cart::BOTH);
            $this->module->validateOrder(
                $this->context->cart->id,
                $status,
                $total,
                $methodName->displayName,
                null,
                null,
                (int)$this->context->currency->id,
                false,
                $customer->secure_key
            );
            Tools::redirectAdmin(self::$currentIndex .'&token=' . $this->token .'&id_customer='. (int)$customer->id);
        }
        if (Tools::isSubmit('presta_total_pay')
            && $status
            && $amount
            && $idCustomer
            && Configuration::get('PRESTA_PAYLATER_ENABLE')
            && $payment_method) {
            $this->processSpecificProduct($amount);
            $customer = new Customer($idCustomer);
            $this->context->cookie->id_customer = (int)($customer->id);
            $this->context->customer = $customer;
            $this->context->cart = new Cart();
            $address = $customer->getAddresses($this->context->language->id);
            $carrier = Carrier::getCarriers($idCustomer);

            if (!isset($address[0]['id_address'])) {
                echo 'please add an address for this customer';
                die;
            } else {
                $id_address = $address[0]['id_address'];
            }
            if (!isset($address[0]['id_address'])) {
                echo 'please add carrier for this customer';
                die;
            } else {
                $idCarrier = $carrier[0]['id_carrier'];
            }

            $this->context->cart->id_carrier = $idCarrier;
            $this->context->cart->id_address_invoice = $id_address;
            $this->context->cart->id_currency = $this->context->currency->id;
            $this->context->cart->add();
            $this->context->cookie->id_cart = (int)$this->context->cart->id;
            $this->context->cookie->write();
            $this->context->cart->id_customer = (int)$customer->id;
            $this->context->cart->secure_key = $customer->secure_key;
            $cartProducts = $this->context->cart->getProducts();

            if ($cartProducts) {
                foreach ($cartProducts as $product) {
                    $this->context->cart->deleteProduct(
                        $product['id_product'],
                        $product['id_product_attribute']
                    );
                }
            }
            $this->context->cart->updateQty(1, Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID'));
            $total = (float)$this->context->cart->getOrderTotal(true, Cart::BOTH);
            $this->module->validateOrder(
                $this->context->cart->id,
                $status,
                $total,
                $methodName->displayName,
                null,
                null,
                (int)$this->context->currency->id,
                false,
                $customer->secure_key
            );
            Tools::redirectAdmin(self::$currentIndex .'&token=' . $this->token .'&id_customer='. (int)$customer->id);
        }

        if ($this->display == 'view') {
            if (Tools::getValue('id')) {
                $transaction = new PrestaBuyNowTransaction(Tools::getValue('id'));
                if (Validate::isLoadedObject($transaction)) {
                    $order = Order::getByCartId($transaction->id_cart);
                    Tools::redirectAdmin(
                        $this->context->link->getAdminLink('AdminOrders').'&vieworder&id_order='.(int)$order->id
                    );
                }
            }
        }
        parent::postProcess();
    }

    public function processSpecificProduct($amount)
    {
        $id_product = Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID');
        $id_customer = Tools::getValue('id_customer');
        $customer = new Customer($id_customer);
        // $id_customer = $this->context->customer->id;
        $id_currency = $this->context->currency->id;
        $obj_specific = new SpecificPrice();
        $isSpecificExist = SpecificPrice::getSpecificPrice(
            $id_product,
            $this->context->shop->id,
            $id_currency,
            $this->context->shop->id_shop_group,
            $customer->id_default_group,
            1,
            null,
            $id_customer
        );
        if (!empty($isSpecificExist)) {
            $obj_specific = new SpecificPrice($isSpecificExist['id_specific_price']);
        }
        $obj_specific->id_customer = $id_customer;
        $obj_specific->id_specific_price_rule = 0;
        $obj_specific->id_cart = 0;
        $obj_specific->id_product = $id_product;
        $obj_specific->id_shop = $this->context->shop->id;
        $obj_specific->id_shop_group = $this->context->shop->id_shop_group;
        $obj_specific->id_currency = $id_currency;
        $obj_specific->id_country = 0;
        $obj_specific->id_group = $customer->id_default_group;
        $obj_specific->id_product_attribute = 0;
        $obj_specific->price = $amount;
        $obj_specific->from_quantity = 1;
        $obj_specific->reduction = 0;
        $obj_specific->reduction_tax = 0;
        $obj_specific->reduction_type = 'amount';
        $obj_specific->from = date('Y-m-d', time() - 86400);
        $obj_specific->to = date('Y-m-d', time() + 86400);
        if ($obj_specific->save()) {
            //======================
        } else {
            Tools::redirect($this->context->link->getPageLink('order'));
        }
    }

    public function renderForm()
    {
        $idCustomer = Tools::getValue('id_customer');
        if (!$idCustomer) {
            $idCustomer = 0;
        }
        $objPrestaBuyNowTransaction = new PrestaBuyNowTransaction();
        $allCustomers = $objPrestaBuyNowTransaction->getAllUniqueWithDetails();

        $custInfoArray = array();
        $custInfoArray[] =  array(
            'id' => 0,
            'name' => $this->l('Choose Customer')
        );
        if ($allCustomers) {
            foreach ($allCustomers as $cust) {
                $custInfoArray[] =  array(
                    'id' => $cust['id_customer'],
                    'name' => $cust['customer']
                );
            }
        }
        $this->fields_form = array(
            'legend' => array(
                'title' =>  $this->l('Customer Selection'),
                ),
            'input' =>  array(
                array(
                    'label' => $this->l('Select Customer'),
                    'type' => 'select',
                    'name' => 'idCustomer',
                    'options' => array(
                        'query' => $custInfoArray,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
            )
        );
        Media::addJsDef(
            array(
                'prestaAdminLink' => $this->context->link->getAdminLink('AdminPrestaBuyNowCustomerTransaction'),
                'empty_error' => $this->module->l('Please enter some amount', 'transaction'),
                'larger_amount_error' => $this->module->l(
                    'Please enter less amount than total outstandings',
                    'transaction'
                ),
            )
        );
        $this->fields_value = array(
            'idCustomer' => $idCustomer
        );
        // $products = array();
        $result = array();
        $objCustomer = new PrestaBuyNowCustomer();
        $isExist = $objCustomer->getCustomerDetailByIdCustomer($idCustomer, true);

        $objTransaction = new PrestaBuyNowTransaction();
        $records = $objTransaction->getRecordByIdCustomer($idCustomer);
        $unbilled = $objTransaction->getUnbilledTotalByIdCustomer($idCustomer);

        if ($isExist) {
            $limit = $isExist['paylater_limit'];
        } else {
            $limit = Configuration::get('PRESTA_PAYLATER_GLOBAL_LIMIT');
        }
        if ($records) {
            foreach ($records as $key => $detail) {
                if ($detail['id_cart']) {
                    $products = $objTransaction->getProductsByIdCart($detail['id_cart']);
                    if ($products) {
                        foreach ($products as $k => $v) {
                            $products[$k]['total'] = Tools::displayPrice(
                                $v['total'],
                                new Currency($detail['id_currency'])
                            );
                        }
                    }
                }
                if ($detail['transaction_for'] == 3) {
                    $products = array(
                        '0' => array(
                            'name' => $this->l('Late Payment Fee'),
                            'total' => $detail['dr_amount']
                        )
                    );
                }
                $records[$key]['products'] = $products;
                $drAmount = $detail['dr_amount'];
                $records[$key]['dr_amount'] = Tools::displayPrice(
                    $drAmount,
                    new Currency($detail['id_currency'])
                );
                $crAmount = $detail['cr_amount'];
                $records[$key]['cr_amount'] = Tools::displayPrice(
                    $crAmount,
                    new Currency($detail['id_currency'])
                );
                $year = Tools::strtoupper(date("Y", strtotime($detail['date_add'])));
                $month = Tools::strtoupper(date("F", strtotime($detail['date_add'])));
                $result[$year][$month][] = $records[$key];
            }
        } else {
            $result = 0;
            $unbilled = 0;
        }
        $balance = $limit - $unbilled;
        if ($balance < 0) {
            $balance = 0;
        }
        $used = Tools::ps_round((($unbilled*100)/$limit), 2);
        if ($used > 100) {
            $used = 100;
        }
        $statuses = OrderState::getOrderStates((int)$this->context->language->id);
        $this->context->smarty->assign(
            array(
                'limit' => Tools::displayPrice($limit),
                'balance' => Tools::displayPrice($balance),
                'used' => $used,
                'paymentModules' => PaymentModule::getInstalledPaymentModules(),
                'presta_status' =>$statuses,
                'alltransaction' => $result,
                'idCustomer' => $idCustomer,
                'unbilled_amount' => $unbilled,
                'prestaAdminLink' => $this->context->link->getAdminLink('AdminPrestaBuyNowCustomerTransaction'),
                'unbilled' => Tools::displayPrice(
                    $unbilled,
                    new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
                ),
            )
        );
        return parent::renderForm();
    }

    public function setMedia($isNewTheme = false)
    {
        if ($isNewTheme || !$isNewTheme) {
            parent::setMedia(false);
            $this->context->controller->addCss(
                _MODULE_DIR_.'prestabuynowpaylater/views/css/front/presta_paylater_transaction.css'
            );
            $this->context->controller->addJS(
                _MODULE_DIR_.'prestabuynowpaylater/views/js/admin/presta_paylater_admin.js'
            );
        }
    }
}
