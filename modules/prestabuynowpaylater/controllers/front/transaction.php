<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowPayLaterTransactionModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $idCustomer = $this->context->customer->id;
        if ($this->module->checkCustomerAccess($idCustomer)) {
            $objCustomer = new PrestaBuyNowCustomer();
            $isExist = $objCustomer->getCustomerDetailByIdCustomer($idCustomer, true);

            $result = array();
            $objTransaction = new PrestaBuyNowTransaction();
            $records = $objTransaction->getRecordByIdCustomer($idCustomer);
            $unbilled = $objTransaction->getUnbilledTotalByIdCustomer($idCustomer);

            if ($isExist) {
                $limit = $isExist['paylater_limit'];
            } else {
                $limit = Configuration::get('PRESTA_PAYLATER_GLOBAL_LIMIT');
            }
            if ($records) {
                foreach ($records as $key => $detail) {
                    $cart = new Cart($detail['id_cart']);
                    $products = $cart->getProducts();
                    if ($products) {
                        foreach ($products as $k => $v) {
                            $products[$k]['total'] = Tools::displayPrice(
                                $v['total'],
                                new Currency($detail['id_currency'])
                            );
                        }
                    }
                    if ($detail['transaction_for'] == 3) {
                        $products = array(
                            '0' => array(
                                'name' => $this->module->l('Late Payment Fee'),
                                'total' => $detail['dr_amount']
                            )
                        );
                    }
                    $records[$key]['products'] = $products;
                    $drAmount = $detail['dr_amount'];
                    $records[$key]['dr_amount'] = Tools::displayPrice(
                        $drAmount,
                        new Currency($detail['id_currency'])
                    );
                    $crAmount = $detail['cr_amount'];
                    $records[$key]['cr_amount'] = Tools::displayPrice(
                        $crAmount,
                        new Currency($detail['id_currency'])
                    );
                    $year = Tools::strtoupper(date("Y", strtotime($detail['date_add'])));
                    $month = Tools::strtoupper(date("F", strtotime($detail['date_add'])));
                    $result[$year][$month][] = $records[$key];
                }
            } else {
                $result = 0;
                $unbilled = 0;
            }
            $balance = $limit - $unbilled;
            if ($balance < 0) {
                $balance = 0;
            }
            $used = Tools::ps_round((($unbilled*100)/$limit), 2);
            if ($used > 100) {
                $used = 100;
            }
            Media::addJsDef(
                array(
                    'empty_error' => $this->module->l('Please enter some amount', 'transaction'),
                    'larger_amount_error' => $this->module->l(
                        'Please enter less amount than total outstandings',
                        'transaction'
                    ),
                )
            );
            $this->context->smarty->assign(
                array(
                    'limit' => Tools::displayPrice($limit),
                    'balance' => Tools::displayPrice($balance),
                    'used' => $used,
                    'alltransaction' => $result,
                    'unbilled_amount' => $unbilled,
                    'unbilled' => Tools::displayPrice(
                        $unbilled,
                        new Currency($this->context->currency->id)
                    ),
                    'presta_cms' => $this->context->link->getCMSLink(Configuration::get('PRESTA_PAYLATER_CMS_PAGE'))
                )
            );
        } else {
            Tools::redirect($this->context->link->getPageLink('my-account'));
        }
        $this->setTemplate('module:prestabuynowpaylater/views/templates/front/transaction.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->context->controller->registerStyleSheet(
            'modules-paylater-css',
            'modules/'.$this->module->name.'/views/css/front/presta_paylater_transaction.css'
        );
        $this->context->controller->registerJavaScript(
            'modules-paylater-js',
            'modules/'.$this->module->name.'/views/js/front/presta_paylater_transaction.js'
        );
    }
}
