<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowPayLaterProcessModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        if ($_SERVER['REQUEST_METHOD'] == 'POST' &&
            $this->context->customer->id &&
            $this->context->cart->id &&
            Configuration::get('PRESTA_PAYLATER_ENABLE')
        ) {
            $buyNowTransaction = new PrestaBuyNowTransaction();
            if (!$buyNowTransaction->isCartExist($this->context->cart->id)) {
                $cartTotal = (float)$this->context->cart->getOrderTotal(true, Cart::BOTH);
                $buyNowTransaction->id_customer = $this->context->customer->id;
                $buyNowTransaction->id_cart = $this->context->cart->id;
                $buyNowTransaction->id_currency = $this->context->currency->id;
                $buyNowTransaction->dr_amount = $cartTotal;
                $buyNowTransaction->cr_amount = 0;
                $buyNowTransaction->transaction_for = 1; // Buying from PayLater
                $buyNowTransaction->status = 1;
                if ($buyNowTransaction->save()) {
                    if ($this->module->validateOrder(
                        $this->context->cart->id,
                        Configuration::get('PS_OS_PRESTA_BUYNOW'),
                        $cartTotal,
                        $this->module->displayName,
                        null,
                        null,
                        (int) $this->context->currency->id,
                        false,
                        $this->context->cart->secure_key
                    )) {
                        if (Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_CUST')) {
                            $this->module->sendOrderConfirmationEmailToCustomer(
                                $this->context->customer->id,
                                $this->context->cart->id
                            );
                        }
                        if (Configuration::get('PRESTA_PAYLATER_ORDER_MAIL_TO_ADMIN')) {
                            $this->module->sendOrderConfirmationEmailToAdmin(
                                $this->context->customer->id,
                                $this->context->cart->id
                            );
                        }
                        Tools::redirect(
                            $this->context->link->getPageLink('order-confirmation').
                            "?id_cart=".
                            $this->context->cart->id.
                            "&id_module=".
                            Module::getModuleIdByName($this->module->name).
                            "&id_order=".
                            Order::getOrderByCartId($this->context->cart->id).
                            "&success=1&key=".
                            $this->context->cart->secure_key
                        );
                    } else {
                        Tools::redirect($this->context->link->getPageLink('order'));
                    }
                } else {
                    Tools::redirect($this->context->link->getPageLink('order'));
                }
            } else {
                Tools::redirect($this->context->link->getPageLink('my-account'));
            }
        } else {
            Tools::redirect($this->context->link->getPageLink('my-account'));
        }
    }
}
