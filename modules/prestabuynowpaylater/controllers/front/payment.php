<?php
/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */

class PrestaBuyNowPayLaterPaymentModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $idCustomer = $this->context->customer->id;
        if ($idCustomer && Configuration::get('PRESTA_PAYLATER_ENABLE')) {
            $transaction = new PrestaBuyNowTransaction();
            $remainingAmount = $transaction->getUnbilledTotalByIdCustomer($idCustomer);

            $partial_amount = tools::getValue('presta_partial_amount');
            if (isset($partial_amount) && $partial_amount) {
                $remainingAmount=$partial_amount;
            }
            if ($remainingAmount > 0) {
                $this->processSpecificProduct($remainingAmount);
                if (!$this->context->cart->id) {
                    $this->context->cart->add();
                    $this->context->cookie->id_cart = $this->context->cart->id;
                    $this->context->cookie->write();
                }
            } else {
                Tools::redirect($this->context->link->getPageLink('my-account'));
            }
            $idCart = $this->context->cart->id;
            if ($idCart) {
                $cartProducts = $this->context->cart->getProducts();
                if ($cartProducts) {
                    foreach ($cartProducts as $product) {
                        $this->context->cart->deleteProduct(
                            $product['id_product'],
                            $product['id_product_attribute']
                        );
                    }
                }
                $this->context->cart->updateQty(1, Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID'));
                PrestaBuyNowHelper::updateCartQty($idCart, Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID'));
                Tools::redirect($this->context->link->getPageLink('order'));
            } else {
                Tools::redirect($this->context->link->getPageLink('my-account'));
            }
        } else {
            Tools::redirect($this->context->link->getPageLink('my-account'));
        }
    }

    public function processSpecificProduct($amount)
    {
        $id_product = Configuration::get('PRESTA_BUY_NOW_PRODUCT_ID');
        $id_customer = $this->context->customer->id;
        $id_currency = $this->context->currency->id;
        $obj_specific = new SpecificPrice();
        $isSpecificExist = SpecificPrice::getSpecificPrice(
            $id_product,
            $this->context->shop->id,
            $id_currency,
            $this->context->shop->id_shop_group,
            $this->context->customer->id_default_group,
            1,
            null,
            $id_customer
        );
        if (!empty($isSpecificExist)) {
            $obj_specific = new SpecificPrice($isSpecificExist['id_specific_price']);
        }
        $obj_specific->id_customer = $id_customer;
        $obj_specific->id_specific_price_rule = 0;
        $obj_specific->id_cart = 0;
        $obj_specific->id_product = $id_product;
        $obj_specific->id_shop = $this->context->shop->id;
        $obj_specific->id_shop_group = $this->context->shop->id_shop_group;
        $obj_specific->id_currency = $id_currency;
        $obj_specific->id_country = 0;
        $obj_specific->id_group = $this->context->customer->id_default_group;
        $obj_specific->id_product_attribute = 0;
        $obj_specific->price = $amount;
        $obj_specific->from_quantity = 1;
        $obj_specific->reduction = 0;
        $obj_specific->reduction_tax = 0;
        $obj_specific->reduction_type = 'amount';
        $obj_specific->from = date('Y-m-d', time() - 86400);
        $obj_specific->to = date('Y-m-d', time() + 86400);
        if ($obj_specific->save()) {
            //======================
        } else {
            Tools::redirect($this->context->link->getPageLink('order'));
        }
    }
}
