Hello {name},

Your Postpaid bill of {pending_amount} is due on {due_date} at {shop_name}.

Bill Details -

Bill Period	: {bill_date}
Bill Amount	: {pending_amount}
Due Date: {due_date}

Please pay back your due amount by {due_date} to continue enjoying Postpaid services.

Thank you.
{shop_name} - {shop_url}
{shop_url} powered by PrestaShop&trade;
