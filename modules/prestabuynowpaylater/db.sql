/**
 * 2008-2021 Prestaworld
 *
 * All right is reserved,
 *
 * @author    Prestaworld <prestaworld12@gmail.com>
 * @copyright 2008-2021 Prestaworld
 * @license   One Paid Licence By WebSite Using This Module. No Rent. No Sell. No Share.
 */
 
CREATE TABLE IF NOT EXISTS `PREFIX_presta_buynow_customer_transaction` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_cart` int(11) unsigned NOT NULL,
	`id_customer` int(11) unsigned NOT NULL,
	`id_currency` int(11) unsigned NOT NULL,
	`dr_amount` decimal(20,6) DEFAULT '0.000000',
	`cr_amount` decimal(20,6) DEFAULT '0.000000',
	`transaction_for` int(2) unsigned NOT NULL DEFAULT 1,
	`status` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`date_add` datetime NOT NULL,
	`date_upd` datetime NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_presta_buynow_allowed_product` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `id_product` int(11) unsigned NOT NULL,
	`active` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`date_add` datetime NOT NULL,
	`date_upd` datetime NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_presta_buynow_customer_configuration` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`id_customer` int(11) unsigned NOT NULL,
	`paylater_limit` decimal(20,6) DEFAULT '0.000000',
	`active` tinyint(1) unsigned NOT NULL DEFAULT '0',
	`date_add` datetime NOT NULL,
	`date_upd` datetime NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
