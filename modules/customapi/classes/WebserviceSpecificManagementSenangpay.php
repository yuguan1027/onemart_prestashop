<?php


class WebserviceSpecificManagementSenangpay implements WebserviceSpecificManagementInterface
{
	protected $objOutput;
    protected $output;    
    protected $wsObject;

	public function setObjectOutput(WebserviceOutputBuilderCore $obj)
	{
		$this->objOutput = $obj;
        return $this;
	}

    public function getObjectOutput()
    {
    	return $this->objOutput;
    }

    public function setWsObject(WebserviceRequestCore $obj)
    {
    	$this->wsObject = $obj;
        return $this;
    }

    public function getWsObject()
    {
    	return $this->wsObject;
    }

    public function manage()
    {    	
        $customer_ORDER = uniqid();
        $detail = "CART_NO_" . $customer_ORDER;
        $email = $this->wsObject->urlFragments['email'];
        $name = $this->wsObject->urlFragments['name'];
        $phone = $this->wsObject->urlFragments['phone'];
        $total = $this->wsObject->urlFragments['total'];        
        $total_to_PAY = number_format((float) $total, 2, '.', '');

        $senangpay_ID     = Configuration::get('SENANGPAY_API_ID');
        $senangpay_SECRET = Configuration::get('SENANGPAY_API_SECRET');
                
        $detail           = "CART_NO_" . $customer_ORDER;        
        
        $concat_prehash = $senangpay_SECRET . $detail . $total_to_PAY . $customer_ORDER;
        $senangpay_HASH = md5($concat_prehash);
        $post_args      = array(
            'detail' => $detail,
            'amount' => $total_to_PAY,
            'order_id' => $customer_ORDER,
            'hash' => $senangpay_HASH,
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        );
        $senangpay_args = '';
        foreach ($post_args as $key => $value) {
            $senangpay_args .= "&";
            $senangpay_args .= $key . "=" . urlencode($value);
        }
        $senangpay_SEND_PARAM = $senangpay_args;
        $api_url_general      = 'https://sandbox.senangpay.my/payment/' . $senangpay_ID;
        $api_url              = $api_url_general . '?' . $senangpay_SEND_PARAM;

    	$this->output = '<payment>'.PHP_EOL.
            '<url><![CDATA['.$api_url.']]></url>'.PHP_EOL.
        '</payment>';
    }
    
    public function getContent()
    {
    	return $this->objOutput->getObjectRender()->overrideContent($this->output);
    }
}
