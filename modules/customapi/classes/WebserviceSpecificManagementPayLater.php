<?php


class WebserviceSpecificManagementPayLater implements WebserviceSpecificManagementInterface
{
	protected $objOutput;
    protected $output;    
    protected $wsObject;

	public function setObjectOutput(WebserviceOutputBuilderCore $obj)
	{
		$this->objOutput = $obj;
        return $this;
	}

    public function getObjectOutput()
    {
    	return $this->objOutput;
    }

    public function setWsObject(WebserviceRequestCore $obj)
    {
    	$this->wsObject = $obj;
        return $this;
    }

    public function getWsObject()
    {
    	return $this->wsObject;
    }

    public function manage()
    {
        try {        
            $module = Module::getInstanceByName('prestabuynowpaylater');
            $id_customer = $this->wsObject->urlFragments['id_customer'];

            if (isset($this->wsObject->urlFragments['action'])) {
                
                $transaction = new PrestaBuyNowTransaction();

                if ($this->wsObject->urlFragments['action'] == 'get-balance') {
                    $remainingAmount = (string)Tools::ps_round($transaction->getUnbilledTotalByIdCustomer($id_customer), 2);

                    $this->output = '<paylater>'.PHP_EOL.
                        '<balance>'.$remainingAmount.'</balance>'.PHP_EOL.
                    '</paylater>';

                    $this->content = [
                        'paylater' => [
                            'balance' => $remainingAmount
                        ]
                    ];

                } elseif ($this->wsObject->urlFragments['action'] == 'get-usage') {
                    $objCustomer = new PrestaBuyNowCustomer();
                    $isExist = $objCustomer->getCustomerDetailByIdCustomer($id_customer, true);                    
                    $unbilled = Tools::ps_round($transaction->getUnbilledTotalByIdCustomer($id_customer), 2);

                    $limit = 0;
                    if ($isExist) {
                        $limit = Tools::ps_round($isExist['paylater_limit'], 2);
                    } else {
                        $limit = Tools::ps_round(Configuration::get('PRESTA_PAYLATER_GLOBAL_LIMIT'), 2);
                    }

                    $balance = $limit - $unbilled;
                    if ($balance < 0) {
                        $balance = 0;
                    }
                    $balance = Tools::ps_round($limit - $unbilled, 2);

                    $this->output = '<paylater>'.PHP_EOL.
                        '<limit>'.$limit.'</limit>'.PHP_EOL.
                        '<balance>'.$balance.'</balance>'.PHP_EOL.
                    '</paylater>';

                    $this->content = [
                        'paylater' => [
                            'limit' => (string)$limit,
                            'unbilled' => (string)$unbilled,
                            'balance' => (string)$balance,
                        ]
                    ];

                } elseif ($this->wsObject->urlFragments['action'] == 'pay-outstanding') {

                    $id_currency = Tools::getValue('id_currency');
                    $id_customer = Tools::getValue('id_customer');
                    $amount = Tools::getValue('amount');

                    $buyNowTransaction = new PrestaBuyNowTransaction();
                    $buyNowTransaction->id_customer = $id_customer;
                    $buyNowTransaction->id_cart = 0;
                    $buyNowTransaction->id_order = 0;
                    $buyNowTransaction->id_currency = $id_currency;
                    $buyNowTransaction->dr_amount = 0;
                    $buyNowTransaction->cr_amount = $amount;
                    $buyNowTransaction->transaction_for = 2; // Paid Unbilled Amount
                    $buyNowTransaction->status = 1; // Paid Unbilled Amount
                    $transaction_id = 0;

                    if ($this->wsObject->method == 'POST' && $buyNowTransaction->save()) {

                        //order creation
                        $context = Context::getContext();
                        $customer = new Customer($id_customer);
                        $product = new Product(15000, false, Context::getContext()->language->id);
                        $id_address = Address::getFirstCustomerAddressId($id_customer);
                        $order = new Order();
                        $order->id_shop = (int)$context->shop->id;
                        $order->id_shop_group = (int)$context->shop->id_shop_group;
                        $order->id_cart = 0;
                        $order->reference = Order::generateReference();
                        $order->id_customer = $id_customer;
                        $order->id_carrier = 0;
                        $order->id_currency = $id_currency;
                        $order->id_address_delivery = (int)$id_address;
                        $order->id_address_invoice = (int)$id_address;
                        $order->secure_key = pSQL($customer->secure_key);
                        $order->payment = 'senangPay';
                        $order->current_state = 2;
                        $order->module = 0;
                        $order->total_paid = $amount;
                        $order->total_paid_real = 0;
                        $order->total_products = 0;
                        $order->total_products_wt = $amount;
                        $order->total_paid_tax_incl = $amount;
                        $order->conversion_rate = $context->currency->conversion_rate;
                        $order->valid = 1;
                        
                        if ($order->add()) {
                       
                            Db::getInstance()->insert('order_detail', 
                                array(
                                    'id_order' => $order->id,
                                    'id_order_invoice' => 0,
                                    'id_warehouse' => 0,
                                    'id_shop' => $context->shop->id,
                                    'product_id' => 15000,
                                    'product_attribute_id' => 0,
                                    'id_customization' => 0,
                                    'product_name' => $product->name,
                                    'product_quantity' => 1,
                                    'product_quantity_in_stock' => 0,
                                    'product_price' => $amount,
                                    'reduction_percent' => 0,
                                    'reduction_amount_tax_incl' => 0,
                                    'product_reference' => 0,
                                    'product_reference' => $product->reference,
                                    'product_supplier_reference' => $product->supplier_reference,
                                    'total_price_tax_incl' => $amount,
                                    'total_price_tax_excl' => $amount,
                                    'unit_price_tax_incl' => $amount,
                                    'unit_price_tax_excl' => $amount,
                                    'original_product_price' => $amount
                                )
                            );
                        }
                        $transaction_id = $buyNowTransaction->id;
                    }

                    $this->output = '<payment>'.PHP_EOL.
                        '<paylater>'.$transaction_id.'</paylater>'.PHP_EOL.
                    '</payment>';

                    $this->content = [
                        'payment' => [
                            'paylater' => $transaction_id
                        ]
                    ];

                }

            } else {                
                $id_cart = 0;//$this->wsObject->urlFragments['id_cart'];
                $id_order = $this->wsObject->urlFragments['id_order'];
                $id_currency = $this->wsObject->urlFragments['id_currency'];
                $total = $this->wsObject->urlFragments['total'];

                $buyNowTransaction = new PrestaBuyNowTransaction();
                $buyNowTransaction->id_customer = $id_customer;
                $buyNowTransaction->id_cart = $id_cart;
                $buyNowTransaction->id_order = $id_order;
                $buyNowTransaction->id_currency = $id_currency;
                $buyNowTransaction->dr_amount = $total;
                $buyNowTransaction->cr_amount = 0;
                $buyNowTransaction->transaction_for = 1; // Buying from PayLater
                $buyNowTransaction->status = 1;
                $transaction_id = 0;

                if ($this->wsObject->method == 'POST' && $buyNowTransaction->save()) {
                    $transaction_id = $buyNowTransaction->id;
                }

                $this->output = '<payment>'.PHP_EOL.
                    '<paylater>'.$transaction_id.'</paylater>'.PHP_EOL.
                '</payment>';

                $this->content = [
                    'payment' => [
                        'paylater' => $transaction_id
                    ]
                ];
            }            
        } catch (Exception $e) {
            throw new WebserviceException('An error occured - '.$e->getMessage(), array(199, 500));
        }
    }
    
    public function getContent()
    {
        if (isset($this->wsObject->urlFragments['output_format']) && $this->wsObject->urlFragments['output_format'] == 'JSON') {
            return json_encode($this->content);
        }

    	return $this->objOutput->getObjectRender()->overrideContent($this->output);
    }
}
