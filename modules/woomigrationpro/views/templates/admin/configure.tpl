{*
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2021 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: WooCommerce To PrestaShop
*}

{*<div class="panel">*}
{*	<h3><i class="icon icon-tags"></i> {l s='Migration Guide: Documentation' mod='woomigrationpro'}</h3>*}
{*	<p>*}
{*		&raquo; {l s='PDF documentation to configure the module' mod='woomigrationpro'} :*}
{*			<a href="{$module_dir|escape:'javascript':'UTF-8'}documentation/readme_en.pdf" target="_blank">{l*}
{*				s='Documentation' mod='woomigrationpro'}</a>*}
{*	</p>*}
{*	<p>*}
{*		&raquo; {l s='WooCommerce Connector Plugin' mod='woomigrationpro'} :*}
{*			*}{*<a href="{$module_dir|escape:'javascript':'UTF-8'}assets/connector.zip" target="_blank">{l s='Download' mod='woomigrationpro'}</a>*}
{*			<a class="migration-button" data-module-path="{$module_dir|escape:'javascript':'UTF-8'}" target="_blank">{l s='the connector module' mod='woomigrationpro'}</a>*}
{*	</p>*}
{*</div>*}

<div class="alert alert-info">
	<ul>
		<div style="font-size:1.2rem; font-weight:500; margin-top:-5px; margin-bottom:5px">{l s='How to connect your WooCOmmerce?' mod='woomigrationpro'}</div>
		<!-- <p>1. {l s='Enter the WooCOmmerce cart URL' mod='woomigrationpro'}</p> -->
		<p>&raquo;
			{l s='Download and install ' mod='woomigrationpro'}
			<a class="migration-button" data-module-path="{$module_dir|escape:'javascript':'UTF-8'}" target="_blank">{l s='the connector module' mod='woomigrationpro'}</a>
			{l s=' to the source WooCommerce. ' mod='woomigrationpro'}
			<!-- <a href="https://youtube.com" target="_blank">{l s='Video tutorial' mod='woomigrationpro'}</a> -->
		</p>
		<p>&raquo;
			<a href="https://addons.prestashop.com/contact-form.php?id_product=26018" target="_blank">{l s='Help center' mod='woomigrationpro'}</a>
		</p>
		<!-- <p><a href="{$module_dir|escape:'javascript':'UTF-8'}documentation/readme_en.pdf" target="_blank">{l s='Documentation' mod='woomigrationpro'}</a> -->
		<!-- <p><a href="{$module_dir|escape:'javascript':'UTF-8'}documentation/readme_en.pdf" target="_blank">{l s='Contact with support' mod='woomigrationpro'}</a> -->
		<!-- {l s='and' mod='woomigrationpro'} -->
		<!-- <a href="https://www.youtube.com/watch?v=DZjdoUMO2zA" target="_blank">{l s='video tutorials' mod='woomigrationpro'}</a> -->
		</p>
	</ul>
</div>
