<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2021 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: WooCommerce To PrestaShop
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * This function updates your module from previous versions to the version 5.2.0,
 * usefull when you modify your database, or register a new hook ...
 * Don't forget to create one file per version.
 */
function upgrade_module_5_2_0($module)
{
    /**
     * Do everything you want right there,
     * You could add a column in one of your module's tables
     */
    return Db::getInstance()->execute(
        'DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'woomigrationpro_link_rewrite`;
                CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'woomigrationpro_link_rewrite` (
                `id_data` int(11) NOT NULL AUTO_INCREMENT,
                  `entity_type` varchar(100) NOT NULL,
                  `rewrite_url` TEXT NOT NULL,
                  `keyword` TEXT NOT NULL,
                  `source_id` int(11) NOT NULL,
                  `local_id` int(11) NOT NULL,
                  PRIMARY KEY (`id_data`),
                  UNIQUE KEY `entity_type_source_id` (`entity_type`,`source_id`)
                )'
    );

    return true;
}
