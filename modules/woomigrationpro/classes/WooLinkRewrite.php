<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2021 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: WooCommerce To PrestaShop
 */

class WooLinkRewrite extends ObjectModel
{
    const TYPE_CATEGORY = 'c';
    const TYPE_PRODUCT = 'p';

    public $id_data;
    public $entity_type;
    public $source_id;
    public $local_id;

    public static $definition = array(
        'table' => 'woomigrationpro_link_rewrite',
        'primary' => 'id_data',
        'fields' => array(
            'entity_type' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'rewrite_url' => array('type' => self::TYPE_STRING),
            'keyword' => array('type' => self::TYPE_STRING),
            'source_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'local_id' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true)
        ),
    );

    /**
     * @param $entity_type
     * @param $rewrite_url
     * @param $keyword
     * @param $source_id
     * @param $local_id
     * @return bool
     */
    public static function import($entity_type, $rewrite_url, $keyword, $source_id, $local_id)
    {
        $entity_type = constant("self::TYPE_" . Tools::strtoupper($entity_type));
        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'woomigrationpro_link_rewrite SET entity_type=\'' . pSQL($entity_type) . '\', rewrite_url=\'' . pSQL($rewrite_url) . '\', keyword=\'' . pSQL($keyword) . '\', source_id=' . (int)$source_id . ', local_id=' . (int)$local_id . ' ON DUPLICATE KEY UPDATE local_id=' . (int)$local_id;

        return Db::getInstance()->execute($sql);
    }

    /**
     * @param $entity_type
     * @param $sourceID
     * @return false|string|null
     */
    public static function getLocalID($entity_type, $sourceID)
    {
        $entity_type = constant("self::TYPE_" . Tools::strtoupper(pSQL($entity_type)));

        $sql = 'SELECT keyword FROM ' . _DB_PREFIX_ . 'woomigrationpro_link_rewrite WHERE entity_type=\'' . pSQL($entity_type) . '\' AND source_id=' . (int)$sourceID . ';';

        return Db::getInstance()->getValue($sql);
    }
}
