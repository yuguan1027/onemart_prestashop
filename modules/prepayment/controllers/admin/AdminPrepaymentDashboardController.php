<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class AdminPrepaymentDashboardController extends ModuleAdminController
{
    public $currency_wallet;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->display = 'view';

        parent::__construct();

        $this->currency_wallet = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        $this->page_header_toolbar_title = $this->l('Dashboard');
        $this->page_header_toolbar_btn['settings'] = array(
            'href' => $this->module->getAdminLink('AdminModules', array('configure' => $this->module->name, 'tab_module' => $this->module->tab, 'module_name' => $this->module->name)),
            'desc' => $this->l('Settings', null, null, false),
            'icon' => 'process-icon-configure'
        );
    }

    public function initToolbar()
    {
        $this->toolbar_btn['edit'] = array(
            'href' => $this->module->getAdminLink('AdminModules', array('configure' => $this->module->name, 'tab_module' => $this->module->tab, 'module_name' => $this->module->name)),
            'desc' => $this->l('Settings')
        );
        $this->toolbar_title = $this->breadcrumbs;
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitOpenWallets')) {
            $customers = Customer::getCustomers();
            foreach ($customers as $customer) {
                if (PrepaymentWallets::walletExists((int)$customer['id_customer'])) {
                    continue;
                }

                $wallet = new PrepaymentWallets();
                $wallet->id_customer = (int)$customer['id_customer'];
                $wallet->total_amount = 0;
                $wallet->active = 1;
                $wallet->add();
            }

            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
        }

        return parent::postProcess();
    }

    public function renderView()
    {
        $global_data = array();

        $balance = PrepaymentWallets::getBalance();
        $global_data['tendance'] = 0;
        $tendance = PrepaymentLastActivities::getVariation();
        if ($tendance > 0) {
            $global_data['tendance'] = (float)Tools::ps_round((($balance - $tendance) / $tendance) * 100);
        }
        $global_data['balance'] = Tools::displayPrice($balance, $this->currency_wallet);
        $global_data['movements'] = (int)PrepaymentLastActivities::getMovements();
        $global_data['amount_earned'] = Tools::displayPrice(PrepaymentLastActivities::getAmountEarned());
        $global_data['pending_deposits'] = PrepaymentLastActivities::getPendingMovements(PrepaymentLastActivities::DEPOSIT);
        $global_data['total_deposits'] = PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::DEPOSIT);
        $global_data['total_deposits'] += PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::CUSTOM_DEPOSIT);
        $global_data['total_orders'] = PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::ORDER);
        $global_data['total_refunds'] = PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::REFUND);
        $global_data['total_disbursements'] = PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::DISBURSEMENT);
        $global_data['total_disbursements'] += PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::CUSTOM_DISBURSEMENT);
        $global_data['total_gifts'] = PrepaymentLastActivities::getTotalAmount(PrepaymentLastActivities::GIFT);

        $this->context->smarty->assign(array(
            'global_data' => $global_data,
            'tpl_overview' => _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/overview.tpl',
            'href_vieworders' => $this->module->getAdminLink('AdminOrders'),
            'href_viewsettings' => $this->module->getAdminLink('AdminModules', array('configure' => $this->module->name, 'tab_module' => $this->module->tab, 'module_name' => $this->module->name)),
        ));

        $this->displayWallets();
        $this->displayLastActivities();
        $this->displayGifts();

        return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/dashboard.tpl', $this->context->smarty)->fetch();
    }

    public function displayWallets()
    {
        $wallets = PrepaymentWallets::getWallets(20);
        foreach ($wallets as &$wallet) {
            $customer = new Customer((int)$wallet['id_customer']);
            $wallet['customer'] = Validate::isLoadedObject($customer) ? $customer->email : '--';
            $wallet['total_amount'] = Tools::displayPrice($wallet['total_amount'], $this->currency_wallet);
            $wallet['href_view'] = $this->module->getAdminLink('AdminCustomers', array('id_customer' => (int)$customer->id, 'viewcustomer' => 1));
        }

        $this->context->smarty->assign(array(
            'href_viewwallets' => $this->module->getAdminLink('AdminPrepaymentWallets'),
            'href_addwallet' => $this->module->getAdminLink('AdminPrepaymentWallets', array('addprepayment_wallets' => 1)),
            'href_viewdashboard' => $this->module->getAdminLink('AdminPrepaymentDashboard'),
            'wallets' => $wallets,
            'wallet_stats' => PrepaymentWallets::getStats(),
            'tpl_wallets' => _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/wallets.tpl',

        ));
    }

    public function displayLastActivities()
    {
        $last_activities = PrepaymentLastActivities::getLastActivities(null, 20);
        foreach ($last_activities as &$last_activity) {
            $object = new PrepaymentLastActivities((int)$last_activity['id_prepayment_last_activities']);

            $last_activity['customer'] = '--';

            if (($wallet = new PrepaymentWallets($object->id_wallet))
                && Validate::isLoadedObject($wallet)
                && ($customer = new Customer($wallet->id_customer))
                && Validate::isLoadedObject($customer)
            ) {
                $last_activity['customer'] = $customer->email;
            }

            $last_activity['operation'] = $this->module->getOperationDisplayName($object->id_operation);

            if ($object->id_operation == PrepaymentLastActivities::CUSTOM_DEPOSIT || $object->id_operation == PrepaymentLastActivities::CUSTOM_DISBURSEMENT) {
                $last_activity['href_view'] = $this->module->getAdminLink('AdminPrepaymentLastActivities', array('id_prepayment_last_activities' => (int)$object->id, 'updateprepayment_last_activities' => 1));
            } elseif ($object->id_operation == PrepaymentLastActivities::ORDER && PrepaymentLastActivities::isPartial($object->id)) {
                $id_partial = $object->getPartialId();
                $partial = new PrepaymentPartials((int)$id_partial);
                $last_activity['href_view'] = $this->module->getAdminLink('AdminCarts', array('id_cart' => (int)$partial->id_cart, 'viewcart' => 1));
            } else {
                $last_activity['href_view'] = $this->module->getAdminLink('AdminOrders', array('id_order' => (int)$object->getOrder(), 'vieworder' => 1));
            }

            $last_activity['amount'] = Tools::displayPrice(
                Tools::convertPriceFull(
                    PrepaymentLastActivities::getDisplayAmount($object->id_operation, $object->amount),
                    Currency::getCurrencyInstance($object->id_currency),
                    $this->currency_wallet
                ),
                $this->currency_wallet->id
            );
        }

        $this->context->smarty->assign(array(
            'href_viewlast_activities' => $this->module->getAdminLink('AdminPrepaymentLastActivities'),
            'href_addmovement' => $this->module->getAdminLink('AdminPrepaymentLastActivities', array('addprepayment_last_activities' => 1)),
            'last_activities' => $last_activities,
            'last_activity_stats' => PrepaymentLastActivities::getStats(),
            'tpl_last_activities' => _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/last_activities.tpl',
        ));
    }

    public function displayGifts()
    {
        $gifts = PrepaymentGifts::getGifts(20);
        foreach ($gifts as &$gift) {
            $gift['href_updategift'] = $this->module->getAdminLink('AdminPrepaymentGifts', array('id_prepayment_gifts' => (int)$gift['id_prepayment_gifts'], 'updateprepayment_gifts' => 1));

            if (isset($gift['reduction_percent']) && $gift['reduction_percent'] > 0) {
                $gift['reduction'] = $gift['reduction_percent'].'%';
            } elseif (isset($gift['reduction_amount']) && $gift['reduction_amount'] > 0) {
                $gift['reduction'] = Tools::displayPrice($gift['reduction_amount'], Currency::getCurrencyInstance((int)$gift['reduction_currency']));
            } else {
                $gift['reduction'] = 0;
            }
        }

        $this->context->smarty->assign(array(
            'href_viewgifts' => $this->module->getAdminLink('AdminPrepaymentGifts'),
            'href_addgift' => $this->module->getAdminLink('AdminPrepaymentGifts', array('addprepayment_gifts' => 1)),
            'gifts' => $gifts,
            'gift_stats' => PrepaymentGifts::getStats(),
            'currency' => $this->currency_wallet,
            'tpl_gifts' => _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/gifts.tpl',
        ));
    }
}
