<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class AdminPrepaymentLastActivitiesController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'prepayment_last_activities';
        $this->className = 'PrepaymentLastActivities';
        $this->explicitSelect = true;
        $this->lang = false;
        $this->context = Context::getContext();

        parent::__construct();

        $this->fields_list = array(
            'id_prepayment_last_activities' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'operation' => array(
                'title' => $this->l('Operation'),
                'havingFilter' => true,
            ),
            'reference' => array(
                'title' => $this->l('Reference'),
                'havingFilter' => true,
            ),
            'customer' => array(
                'title' => $this->l('Customer'),
                'havingFilter' => true,
            ),
            'amount' => array(
                'title' => $this->l('Amount'),
                'align' => 'text-right',
                'callback' => 'setCurrency',
                'badge_success' => true
            ),
            'paid' => array(
                'title' => $this->l('Paid'),
                'align' => 'text-center',
                'active' => 'paid',
                'type' => 'bool',
                'ajax' => false,
                'orderby' => false,
                'filter_key' => 'a!paid',
                'class' => 'fixed-width-sm'
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            )
        );
    }

    public function setCurrency($value, $tr)
    {
        $last_activities = new PrepaymentLastActivities($tr['id_prepayment_last_activities']);
        if (!Validate::isLoadedObject($last_activities)) {
            throw new PrestaShopException('object Last Activities can\'t be loaded');
        }

        $to_currency = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));

        return Tools::displayPrice(
            Tools::convertPriceFull(
                PrepaymentLastActivities::getDisplayAmount($last_activities->id_operation, $last_activities->amount),
                Currency::getCurrencyInstance((int)$last_activities->id_currency),
                $to_currency
            ),
            $to_currency->id
        );
    }

    public function initToolbar()
    {
        parent::initToolbar();

        if ($this->display == 'edit' || $this->display == 'add') {
            $this->toolbar_btn['save-and-stay'] = array(
                'href' => '#',
                'desc' => $this->l('Save and Stay')
            );
        }
        $this->toolbar_btn['back'] = array(
            'href' =>  $this->module->getAdminLink('AdminPrepaymentDashboard'),
            'desc' => $this->l('Back to list')
        );
    }

    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_title = $this->l('Last Activities');
        $this->page_header_toolbar_btn['back_to_dashboard'] = array(
            'href' => $this->module->getAdminLink('AdminPrepaymentDashboard'),
            'desc' => $this->l('Back', null, null, false),
            'icon' => 'process-icon-back'
        );
        $this->page_header_toolbar_btn['new_operation'] = array(
            'href' => $this->module->getAdminLink('AdminPrepaymentLastActivities', array('addprepayment_last_activities' => 1)),
            'desc' => $this->l('Add new movement', null, null, false),
            'icon' => 'process-icon-new'
        );

        parent::initPageHeaderToolbar();
    }

    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, Context::getContext()->shop->id);
        $nb_items = count($this->_list);
        for ($i = 0; $i < $nb_items; ++$i) {
            $item = &$this->_list[$i];

            $last_activity = new PrepaymentLastActivities((int)$item['id_prepayment_last_activities']);
            if ($last_activity->id_operation == PrepaymentLastActivities::CUSTOM_DEPOSIT
                || $last_activity->id_operation == PrepaymentLastActivities::CUSTOM_DISBURSEMENT
            ) {
                $this->addRowActionSkipList('view', array($item['id_prepayment_last_activities']));
            }

            if ($last_activity->getOrder()) {
                $this->addRowActionSkipList('delete', array($item['id_prepayment_last_activities']));
            }
        }
    }

    public function renderList()
    {
        $this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        $this->_select = '
		c.`email` AS `customer`,
		IF(a.`paid`, 1, 0) badge_success,
		a.`id_operation` as operation,
		CASE id_operation
			WHEN \''.PrepaymentLastActivities::DEPOSIT.'\' THEN \''.$this->l('Deposit').'\'
			WHEN \''.PrepaymentLastActivities::ORDER.'\' THEN \''.$this->l('Order').'\'
			WHEN \''.PrepaymentLastActivities::REFUND.'\' THEN \''.$this->l('Refund').'\'
			WHEN \''.PrepaymentLastActivities::DISBURSEMENT.'\' THEN \''.$this->l('Disbursement').'\'
			WHEN \''.PrepaymentLastActivities::GIFT.'\' THEN \''.$this->l('Gift').'\'
			WHEN \''.PrepaymentLastActivities::CUSTOM_DEPOSIT.'\' THEN \''.$this->l('Manual deposit').'\'
			WHEN \''.PrepaymentLastActivities::CUSTOM_DISBURSEMENT.'\' THEN \''.$this->l('Manual disbursement').'\'
		END operation';

        $this->_join = '
        LEFT JOIN `'._DB_PREFIX_.'prepayment_wallets` pw ON (pw.`id_prepayment_wallets` = a.`id_wallet`)
		LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = pw.`id_customer`)';
        $this->_orderBy = 'id_prepayment_last_activities';
        $this->_orderWay = 'DESC';

        return parent::renderList();
    }

    public function getFieldsValue($object)
    {
        $fields_value = array();

        $definition = ObjectModel::getDefinition($object);
        $fields = array_keys($definition['fields']);
        array_unshift($fields, $definition['primary']);
        foreach ($fields as $field) {
            if ($field == 'label') {
                foreach ($this->_languages as $language) {
                    $fields_value[$field][$language['id_lang']] = $this->getFieldValue($object, $field, $language['id_lang']);
                }
            } elseif (!in_array($field, array('date_add', 'date_upd'))) {
                $fields_value[$field] = $this->getFieldValue($object, $field);
            }
        }

        $currency = new Currency($fields_value['id_currency']);
        $fields_value['currency'] = Validate::isLoadedObject($currency) ? $currency : $this->context->currency;

        $fields_value['operation'] = $this->module->getOperationDisplayName($object->id_operation);

        return $fields_value;
    }

    public function renderView()
    {
        if (Validate::isLoadedObject($this->loadObject(true))) {
            if ($this->object->id_operation == PrepaymentLastActivities::CUSTOM_DEPOSIT
                || $this->object->id_operation == PrepaymentLastActivities::CUSTOM_DISBURSEMENT
            ) {
                Tools::redirectAdmin($this->module->getAdminLink('AdminPrepaymentLastActivities', array('id_prepayment_last_activities' => (int)$this->object->id, 'updateprepayment_last_activities' => 1)));
            } elseif ($this->object->id_operation == PrepaymentLastActivities::ORDER
                && PrepaymentLastActivities::isPartial($this->object->id)
                && ($partial = new PrepaymentPartials($this->object->getPartialId()))
                && Validate::isLoadedObject($partial)
            ) {
                Tools::redirectAdmin($this->module->getAdminLink('AdminCarts', array('id_cart' => (int)$partial->id_cart, 'viewcart' => 1)));
            } elseif (($order = new Order($this->object->getOrder()))
                && Validate::isLoadedObject($order)
            ) {
                Tools::redirectAdmin($this->module->getAdminLink('AdminOrders', array('id_order' => (int)$this->object->getOrder(), 'vieworder' => 1)));
            }
        }

        return parent::renderView();
    }

    public function renderForm()
    {
        $this->loadObject(true);

        $customer_email = false;
        $customer_id = Tools::getValue('id_customer', $this->object->retrieveCustomerId());
        if (($customer = new Customer($customer_id))
            && Validate::isLoadedObject($customer)
        ) {
            $customer_email = $customer->email;
        }

        $is_partial = false;
        $order_url = false;
        if (Validate::isLoadedObject($this->object)) {
            if ($this->object->id_operation == PrepaymentLastActivities::ORDER
                && PrepaymentLastActivities::isPartial($this->object->id)
                && ($partial = new PrepaymentPartials($this->object->getPartialId()))
                && Validate::isLoadedObject($partial)
            ) {
                $is_partial = true;
                $order_url = $this->module->getAdminLink('AdminCarts', array('id_cart' => (int)$partial->id_cart, 'viewcart' => 1));
            } elseif (($order = new Order($this->object->getOrder()))
                && Validate::isLoadedObject($order)
            ) {
                $order_url = $this->module->getAdminLink('AdminOrders', array('id_order' => (int)$this->object->getOrder(), 'vieworder' => 1));
            }
        }

        $this->context->smarty->assign(array(
            'fields_value' => $this->getFieldsValue($this->object),
            'order_url' => $order_url,
            'is_partial' => $is_partial,
            'language' => Language::getLanguage(Configuration::get('PS_LANG_DEFAULT')),
            'href_addwallet' => $this->module->getAdminLink('AdminPrepaymentWallets', array('addprepayment_wallets' => 1)),
            'href_viewmovements' => $this->module->getAdminLink('AdminPrepaymentLastActivities'),
            'href_viewcustomers' => $this->module->getAdminLink('AdminCustomers'),
            'href_viewcustomer' => $this->module->getAdminLink('AdminCustomers', array('viewcustomer' => 1, 'liteDisplaying' => 1, 'id_customer' => 0))
        ));

        $this->addJqueryPlugin('typewatch');
        Media::addJsDef(array('walletCustomerEmail' => $customer_email));
        Media::addJsDef(array('walletMovementEditing' => Validate::isLoadedObject($this->object)));

        $this->content .= $this->context->smarty->createTemplate(_PS_MODULE_DIR_.'prepayment/views/templates/admin/prepayment_last_activities/form.tpl', $this->context->smarty)->fetch();
        return parent::renderForm();
    }

    protected function _childValidation()
    {
        if ((Validate::isLoadedObject($this->object)
            && in_array($this->object->id_operation, array(PrepaymentLastActivities::CUSTOM_DEPOSIT, PrepaymentLastActivities::CUSTOM_DISBURSEMENT))
            && !in_array(Tools::getValue('id_operation'), array(PrepaymentLastActivities::CUSTOM_DEPOSIT, PrepaymentLastActivities::CUSTOM_DISBURSEMENT)))
            || (!Validate::isLoadedObject($this->object)
            && !in_array(Tools::getValue('id_operation'), array(PrepaymentLastActivities::CUSTOM_DEPOSIT, PrepaymentLastActivities::CUSTOM_DISBURSEMENT)))
        ) {
            $this->errors[] = $this->l('This operation cannot be saved');
        }
    }

    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);

        if (Validate::isLoadedObject($object)) {
            $update_fields = array('label');

            if (in_array($object->id_operation, array(PrepaymentLastActivities::CUSTOM_DEPOSIT, PrepaymentLastActivities::CUSTOM_DISBURSEMENT))) {
                $update_fields = array_merge($update_fields, array(
                    'id_operation',
                    'amount',
                ));
            }

            $definition = ObjectModel::getDefinition($object);
            $fields = array_keys($definition['fields']);

            foreach ($fields as $field) {
                if (!in_array($field, $update_fields)) {
                    $object->{$field} = $this->object->{$field};
                }
            }
        } else {
            $id_customer = Tools::getValue('id_customer');
            if (($wallet = PrepaymentWallets::getWalletInstance($id_customer))) {
                $object->id_wallet = $wallet->id;
            }
        }
    }

    public function postProcess()
    {
        $result = parent::postProcess();

        if (!empty($this->errors)) {
            $this->display = 'edit';
            return false;
        }

        return $result;
    }

    public function ajaxProcessSearchLastActivities()
    {
        $id_customer = (int)Tools::getValue('id_customer');
        $found = false;
        $last_activities = array();

        if (($wallet = PrepaymentWallets::getWalletInstance($id_customer))) {
            $found = true;
            $last_activities = PrepaymentLastActivities::getLastActivities($wallet->id);
            foreach ($last_activities as &$last_activity) {
                $last_activity['operation'] = $this->module->getOperationDisplayName($last_activity['id_operation']);
                $last_activity['amount'] = Tools::displayPrice(
                    Tools::convertPriceFull(
                        PrepaymentLastActivities::getDisplayAmount($last_activity['id_operation'], $last_activity['amount']),
                        Currency::getCurrencyInstance($last_activity['id_currency']),
                        Context::getContext()->currency
                    ),
                    Context::getContext()->currency
                );
                $last_activity['status'] = (bool)$last_activity['paid'] ? '<i class="icon-check"></i>' : '<i class="icon-times"></i>';
            }
        }

        $to_return = array(
            'found' => true,
            'last_activities' => $last_activities,
        );

        die(Tools::jsonEncode($to_return));
    }
}
