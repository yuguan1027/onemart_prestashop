<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class PrepaymentDepositsModuleFrontController extends ModuleFrontController
{
    public function display()
    {
        $scope = $this->context->smarty->createData($this->context->smarty);
        $scope->assign(array(
            'errors' => $this->errors,
            'request_uri' => Tools::safeOutput(urldecode($_SERVER['REQUEST_URI']))
        ));
        $tpl_errors = version_compare(_PS_VERSION_, '1.7', '<') ? _PS_THEME_DIR_.'/errors.tpl' : '_partials/form-errors.tpl';
        $errors_rendered = $this->context->smarty->createTemplate($tpl_errors, $scope)->fetch();

        $this->context->smarty->assign(array(
            'errors_rendered' => $errors_rendered,
            'errors_nb' => (int)count($this->errors),
            'token' => Tools::getToken(false),
            'attribute_anchor_separator' => Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR'),
            'currentUrl' => $this->context->link->getModuleLink('prepayment', 'deposits'),
            'ajax_allowed' => (int)(Configuration::get('PS_BLOCK_CART_AJAX')) == 1 ? true : false,
            'currency_sign' => $this->context->currency->sign
        ));

        $template = version_compare(_PS_VERSION_, '1.7', '>=') ? 'module:prepayment/views/templates/front/layout/deposits.tpl' : 'deposits.tpl';
        $this->setTemplate($template);

        return parent::display();
    }

    public function initContent()
    {
        $this->product = new Product(
            (int)Configuration::get('WALLET_PRODUCT_'.$this->context->currency->id),
            false,
            $this->context->language->id,
            $this->context->shop->id
        );

        if (Validate::isLoadedObject($this->product)
            && $this->product->isAssociatedToShop()
        ) {
            if (Tools::getValue('ajax')) {
                if (Tools::getValue('action') == 'getCombination') {
                    $this->displayAjaxGetCombination();
                }
                if (Tools::getValue('action') == 'refresh') {
                    $this->displayAjaxRefresh();
                }
            }

            parent::initContent();

            // Assign attribute groups to the template
            $this->assignAttributesGroups();

            $this->context->smarty->assign(array(
                'product' => $this->product,
                'amount_pitch' => (float)Configuration::get('WALLET_PRODUCT_AMOUNT_PITCH_'.(int)$this->product->id),
                'amount_from' => (float)Configuration::get('WALLET_PRODUCT_AMOUNT_FROM_'.(int)$this->product->id),
                'amount_to' => (float)Configuration::get('WALLET_PRODUCT_AMOUNT_TO_'.(int)$this->product->id),
                'amount_default' => (float)Configuration::get('WALLET_PRODUCT_AMOUNT_DEFAULT_'.(int)$this->product->id),
            ));
        } else {
            Tools::redirect('index.php?controller=404');
        }
    }

    protected function assignAttributesGroups()
    {
        $amount_vars = array();
        $amount_group_object = new AttributeGroup((int)Configuration::get('WALLET_ATTRGROUP_AMOUNT'));
        if (Validate::isLoadedObject($amount_group_object)) {
            $amounts = array();
            $fixed_amount_list = explode(',', Configuration::get('WALLET_PRODUCT_AMOUNT_FIXED_'.(int)$this->product->id));
            $amount_attributes = AttributeGroup::getAttributes((int)$this->context->language->id, (int)$amount_group_object->id);
            foreach ($amount_attributes as $amount_attribute) {
                if (!in_array($amount_attribute['name'], $fixed_amount_list)) {
                    continue;
                }

                $amount = array();
                $amount['attribute_value'] = (float)$amount_attribute['name'];
                $amount['position'] = (int)$amount_attribute['position'];
                $amounts[] = $amount;
            }

            if (count($amounts)) {
                array_multisort(array_column($amounts, 'attribute_value'), SORT_ASC, $amounts);
                $amount_vars = array(
                    'id_attribute_group' => (int)$amount_group_object->id,
                    'public_group_name' => $amount_group_object->public_name[(int)$this->context->language->id],
                    'rewrite_group_name' => str_replace(Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR'), '_', Tools::link_rewrite(str_replace(array(',', '.'), '-', $amount_group_object->public_name[(int)$this->context->language->id]))),
                    'attributes' => $amounts
                );
            }
        }

        $this->context->smarty->assign(array(
            'amount_vars' => (count($amount_vars)) ? $amount_vars : null,
        ));
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->addJS(_MODULE_DIR_.'prepayment/views/js/front/deposits.js');
        $this->addCSS(_MODULE_DIR_.'prepayment/views/css/front/deposits.css', 'all');
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = $this->addMyAccountToBreadcrumb();

        return $breadcrumb;
    }

    public function processDeposit()
    {
        // need to create cart if not exists for customization
        if (!$this->context->cart->id) {
            if (Context::getContext()->cookie->id_guest) {
                $guest = new Guest(Context::getContext()->cookie->id_guest);
                $this->context->cart->mobile_theme = $guest->mobile_theme;
            }
            $this->context->cart->add();
            if ($this->context->cart->id) {
                $this->context->cookie->id_cart = (int) $this->context->cart->id;
            }
        }

        if (!Validate::isLoadedObject($this->context->cart)) {
            throw new PrestaShopException($this->l('Cart cannot be loaded.', 'deposits'));
        } elseif (!Validate::isLoadedObject($this->product) || !$this->product->isAssociatedToShop()) {
            throw new PrestaShopException($this->l('Deposit product cannot be loaded.', 'deposits'));
        } elseif (!$this->product->active) {
            throw new PrestaShopException($this->l('Deposits are not available for the moment', 'deposits'));
        }

        $attributes = Tools::getValue('attributes');
        if (!isset($attributes)
            || empty($attributes)
            || !is_array($attributes)
            || !count($attributes)) {
            throw new PrestaShopException($this->l('Please select an amount', 'deposits'));
        }

        $amount_from = Configuration::get('WALLET_PRODUCT_AMOUNT_FROM_'.(int)$this->product->id);
        $amount_to = Configuration::get('WALLET_PRODUCT_AMOUNT_TO_'.(int)$this->product->id);
        $amount_pitch = Configuration::get('WALLET_PRODUCT_AMOUNT_PITCH_'.(int)$this->product->id);
        $id_attribute = false;
        $id_amount_attribute_group = (int)Configuration::get('WALLET_ATTRGROUP_AMOUNT');
        $languages = Language::getLanguages();
        $shops = $this->module->getShopsByIdCurrency($this->context->currency->id);

        foreach ($attributes as $attribute) {
            if ($attribute['id_attribute_group'] != $id_amount_attribute_group) {
                continue;
            }

            $amount = (float)$attribute['value'];
            if (!Validate::isPrice($amount)
                || $amount < $amount_from
                || $amount > $amount_to
                || (int)($amount * 100) % (int)($amount_pitch * 100) != 0
            ) {
                throw new PrestaShopException($this->l('The amount field is not valid', 'deposits'));
            }

            if (($existing_attribute = $this->module->getAttributeByName($id_amount_attribute_group, $amount, $this->context->language->id))) {
                $id_attribute = $existing_attribute;
                break;
            }

            $attribute = new Attribute();
            $attribute->id_attribute_group = (int)$id_amount_attribute_group;
            foreach ($languages as $language) {
                $attribute->name[(int) $language['id_lang']] = (float) $amount;
            }
            $attribute->position = Attribute::getHigherPosition($attribute->id_attribute_group) + 1;
            $attribute->id_shop_list = $shops;
            $attribute->add();
            $attribute->cleanPositions($attribute->id_attribute_group, false);
            $id_attribute = $attribute->id;
        }

        if (!$id_attribute) {
            throw new PrestaShopException($this->l('The amount field is not valid', 'deposits'));
        }

        $combinations = array_values($this->module->createCombinations(array(array($id_attribute))));
        $values = array_values(array_map(array($this->module, 'getCombinationProperties'), array($this->product->id), array($this->context->currency->id), $combinations));
        if (!$this->module->generateMultipleCombinations($this->product->id, $values, $combinations)) {
            throw new PrestaShopException($this->l('An error occured while creating the deposit', 'deposits'));
        }

        $id_combination = (int)$this->product->productAttributeExists($combinations[0], false, null, true, true);
        $combination = new Combination($id_combination);
        if (!Validate::isLoadedObject($combination)) {
            throw new PrestaShopException($this->l('An error occured while loading the deposit', 'deposits'));
        }

        return array(
            'id_product' => $this->product->id,
            'id_combination' => $id_combination,
            'token' => Tools::getToken(false)
        );
    }

    public function displayAjaxGetCombination()
    {
        try {
            $deposit = $this->processDeposit();
        } catch (PrestaShopException $e) {
            $this->errors[] = $e->getMessage();
        }

        if (!empty($this->errors)) {
            http_response_code(400);
            die(Tools::jsonEncode($this->errors));
        }

        die(Tools::jsonEncode(array(
            'query' => $this->context->link->getPageLink(
                'cart',
                true,
                null,
                array(
                    'add' => 1,
                    'qty' => 1,
                    'action' => 'update',
                    'id_product' => $deposit['id_product'],
                    'id_product_attribute' => $deposit['id_combination'],
                    'token' => $deposit['token'],
                ),
                false
            )
        )));
    }

    public function displayAjaxRefresh()
    {
        die(1);
    }
}
