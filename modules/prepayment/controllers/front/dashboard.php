<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class PrepaymentDashboardModuleFrontController extends ModuleFrontController
{
    public $auth = true;

    public function display()
    {
        $scope = $this->context->smarty->createData($this->context->smarty);
        $scope->assign(array(
            'errors' => $this->errors,
            'request_uri' => Tools::safeOutput(urldecode($_SERVER['REQUEST_URI']))
        ));
        $tpl_errors = version_compare(_PS_VERSION_, '1.7', '<') ? _PS_THEME_DIR_.'/errors.tpl' : '_partials/form-errors.tpl';
        $errors_rendered = $this->context->smarty->createTemplate($tpl_errors, $scope)->fetch();

        $this->context->smarty->assign(array(
            'errors_rendered' => $errors_rendered,
            'errors_nb' => (int)count($this->errors),
            'token' => Tools::getToken(false),
            'currentUrl' => $this->context->link->getModuleLink('prepayment', 'dashboard')
        ));

        $template = version_compare(_PS_VERSION_, '1.7', '>=') ? 'module:prepayment/views/templates/front/layout/dashboard.tpl' : 'dashboard.tpl';
        $this->setTemplate($template);

        return parent::display();
    }

    public function initContent()
    {
        parent::initContent();

        $id_customer = (int)$this->context->customer->id;
        $from_currency = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));
        $to_currency = Context::getContext()->currency;

        $balance = false;
        $is_active = false;
        $last_activities = array();

        if (($wallet = PrepaymentWallets::getWalletInstance($id_customer))) {
            $balance = Tools::displayPrice(Tools::convertPriceFull($wallet->total_amount, $from_currency, $to_currency), $to_currency);
            $is_active = $wallet->active;
            $last_activities = PrepaymentLastActivities::getLastActivities($wallet->id);
            foreach ($last_activities as &$last_activity) {
                $last_activity['operation'] = $this->module->getOperationDisplayName($last_activity['id_operation']);

                $last_activity['amount'] = PrepaymentLastActivities::getDisplayAmount($last_activity['id_operation'], $last_activity['amount']);
                $total_amount = Tools::convertPriceFull(
                    $last_activity['amount'],
                    Currency::getCurrencyInstance($last_activity['id_currency']),
                    $to_currency
                );
                $last_activity['display_amount'] = Tools::displayPrice($total_amount, $to_currency);

                $last_activity['is_partial'] = false;
                if (PrepaymentLastActivities::isPartial($last_activity['id_prepayment_last_activities'])) {
                    $last_activity_obj = new PrepaymentLastActivities((int)$last_activity['id_prepayment_last_activities']);
                    $id_partial = $last_activity_obj->getPartialId();

                    if (($partial_obj = new PrepaymentPartials((int)$id_partial))
                        && Validate::isLoadedObject($partial_obj)
                        && !Order::getOrderByCartId((int)$partial_obj->id_cart)
                        && ($cart = new Cart($partial_obj->id_cart))
                        && Validate::isLoadedObject($cart)
                        && ($cart_rule = new CartRule((int)$partial_obj->id_cart_rule))
                        && Validate::isLoadedObject($cart_rule)
                    ) {
                        $last_activity['link_order'] = $this->context->link->getPageLink(
                            'order',
                            false,
                            (int)$cart->id_lang,
                            'step=3&recover_cart='.(int)$cart->id.'&token_cart='.md5(_COOKIE_KEY_.'recover_cart_'.(int)$cart->id)
                        );

                        $last_activity['is_partial'] = true;
                        $last_activity['partial'] = (float)$cart->getOrderTotal(true, Cart::BOTH);
                        $last_activity['display_partial'] = Tools::displayPrice($last_activity['partial'], $to_currency);
                    }
                }
            }
        }

        $this->context->smarty->assign(array(
            'is_active' => $is_active,
            'balance' => $balance,
            'history_list' => $last_activities
        ));
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitWallet')) {
            $id_customer = (int)$this->context->customer->id;
            if (PrepaymentWallets::walletExists($id_customer)) {
                $this->errors[] = $this->l('A wallet is already opened for this account.', 'dashboard');
            } else {
                $wallet = new PrepaymentWallets();
                $wallet->id_customer = $id_customer;
                $wallet->total_amount = 0;
                $wallet->active = 1;
                $wallet->add();

                Tools::redirect($this->context->link->getModuleLink('prepayment', 'dashboard'));
            }
        } elseif ($id_last_activity = (int)Tools::getValue('deletePartial')) {
            $id_customer = (int)$this->context->customer->id;
            $wallet = PrepaymentWallets::getWalletInstance($id_customer);
            if (!$wallet) {
                $this->errors[] = $this->l('Wallet cannot be loaded.', 'dashboard');
            } else {
                $last_activity = new PrepaymentLastActivities((int)$id_last_activity);
                if (!Validate::isLoadedObject($last_activity)) {
                    $this->errors[] = $this->l('This partial payment does not exist.', 'dashboard');
                } else {
                    if ((int)$last_activity->id_wallet !== (int)$wallet->id) {
                        $this->errors[] = $this->l('An error occured while loading your wallet.', 'dashboard');
                    } else {
                        $id_partial = (int)$last_activity->getPartialId();
                        $partial = new PrepaymentPartials((int)$id_partial);
                        if (!Validate::isLoadedObject($partial)) {
                            $this->errors[] = $this->l('Partial payment cannot be loaded', 'dashboard');
                        } else {
                            if (Order::getOrderByCartId($partial->id_cart)) {
                                $this->errors[] = $this->l('Partial payment cannot be deleted', 'dashboard');
                            } else {
                                $partial->delete();
                            }
                        }
                    }
                }
            }
        }
        return parent::postProcess();
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = $this->addMyAccountToBreadcrumb();

        return $breadcrumb;
    }
}
