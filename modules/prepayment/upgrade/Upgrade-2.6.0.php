<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_6_0($object)
{
    //Backup
    $tables = array(
        'prepayment_wallets',
        'prepayment_last_activities',
        'prepayment_last_activities_lang',
        'prepayment_last_activities_order',
        'prepayment_last_activities_gift',
        'prepayment_last_activities_refund',
        'prepayment_packs',
        'prepayment_packs_lang',
        'prepayment_gifts',
        'prepayment_gifts_carrier',
        'prepayment_gifts_country',
        'prepayment_gifts_group',
        'prepayment_gifts_payment',
        'prepayment_gifts_product_rule',
        'prepayment_gifts_product_rule_group',
        'prepayment_gifts_product_rule_value',
        'prepayment_gifts_shop',
        'prepayment_gifts_lang',
        'prepayment_partials',
    );

    foreach ($tables as $table) {
        if (!Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$table.'_bk`
            SELECT *
            FROM `'._DB_PREFIX_.$table.'`
        ')) {
            return false;
        }
    }

    // Register hooks
    if (!$object->registerHook('actionAdminProductsListingFieldsModifier')
        || !$object->registerHook('actionAdminAttributesGroupsListingFieldsModifier')
        || !$object->registerHook('displayBackOfficeTop')
    ) {
        return false;
    }

    // Update confs
    Configuration::updateGlobalValue('PS_COMBINATION_FEATURE_ACTIVE', true);
    Configuration::updateGlobalValue('WALLET_CURRENCY_DEFAULT', Configuration::getGlobalValue('WALLET_DEFAULT_CURRENCY_PACKS'));
    Configuration::updateGlobalValue('WALLET_ALLOW_PARTIAL_PAYMENT', Configuration::getGlobalValue('WALLET_PARTIAL_PAYMENT'));

    // Uninstall tabs
    if (($id_tab = Tab::getIdFromClassName('AdminPrepaymentPacks'))) {
        $tab = new Tab($id_tab);
        if (Validate::isLoadedObject($tab)) {
            $tab->delete();
        }
    }

    // Delete prepayment products
    $table = 'prepayment_packs';
    $alter_sql = '
        SELECT *
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_NAME = "'._DB_PREFIX_.$table.'"
        AND TABLE_SCHEMA = "'._DB_NAME_.'"
    ';

    $sum_deposit_amount = 0;
    $sum_cash_back_amount = 0;

    if (Db::getInstance()->executeS($alter_sql)) {
        $packs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
    		SELECT *
    		FROM `'._DB_PREFIX_.'prepayment_packs`
            ORDER BY `credits` ASC
        ');

        if (is_array($packs) && count($packs)) {
            $elligible_deposits = array();
            foreach ($packs as $pack) {
                $product = new Product((int)$pack['id_product']);
                if (Validate::isLoadedObject($product)) {
                    $sum_deposit_amount += (int)round($pack['credits']);
                    $sum_cash_back_amount += (int)round($pack['extra_credits']);
                    $elligible_deposits[] = (int)round($pack['credits']);
                    $product->delete();
                }
            }

            if (count($elligible_deposits)) {
                $min_deposit = current($elligible_deposits);
                $max_deposit = end($elligible_deposits);
                $pitch = max(1, ceil(($max_deposit - $min_deposit) / 200));
                $closure = function ($deposits) use (&$closure, &$pitch) {
                    if ($pitch > 25) {
                        return null;
                    }

                    $continue = false;
                    foreach ($deposits as $deposit) {
                        if ($deposit % $pitch != 0) {
                            $continue = true;
                            break;
                        }
                    }

                    if ($continue) {
                        ++$pitch;
                        return $closure($deposits);
                    }

                    return $pitch;
                };

                if ($closure($elligible_deposits) !== null) {
                    $object->product = array(
                        'default' => $min_deposit,
                        'from' => $min_deposit,
                        'to' => $max_deposit,
                        'pitch' => $pitch,
                        'fixed' => implode(',', $elligible_deposits),
                        'active' => '1',
                    );
                }
            }
        }
    }

    // Uninstall Category
    $category = new Category(Configuration::get('WALLET_PACKS_CAT'));
    if (Validate::isLoadedObject($category)) {
        $category->delete();
    }

    // Install product
    $languages = Language::getLanguages();

    if (!$object->installAttributeGroups($languages)
        || !$object->installProduct($languages)
    ) {
        return false;
    }

    // Update tables
    $table = 'prepayment_last_activities';
    $alter_sql = '
        SELECT *
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = "'._DB_PREFIX_.$table.'"
        AND TABLE_SCHEMA = "'._DB_NAME_.'"
        AND COLUMN_NAME = "amount"
    ';

    if (!Db::getInstance()->executeS($alter_sql)) {
        $res = true;

        $res &= Db::getInstance()->execute('
            ALTER TABLE `'._DB_PREFIX_.$table.'`
            ADD COLUMN `amount` decimal(20,6) NOT NULL AFTER `reference`
        ');

        $res &= Db::getInstance()->execute('
            UPDATE `' . _DB_PREFIX_ . $table.'` pla
            SET pla.`amount` = pla.`credits` + pla.`extra_credits`
        ');

        $res &= Db::getInstance()->execute('
            ALTER TABLE `'._DB_PREFIX_.$table.'`
            DROP COLUMN `credits`,
            DROP COLUMN `extra_credits`
        ');

        if (!$res) {
            return false;
        }
    }

    $table = 'prepayment_gifts';
    $alter_sql = '
        SELECT *
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = "'._DB_PREFIX_.$table.'"
        AND TABLE_SCHEMA = "'._DB_NAME_.'"
        AND COLUMN_NAME = "reduction_product"
    ';

    if (!Db::getInstance()->executeS($alter_sql)) {
        $res = true;

        $res &= Db::getInstance()->execute('
            ALTER TABLE `'._DB_PREFIX_.$table.'`
            CHANGE `gift_percent` `reduction_percent` decimal(5,2) NOT NULL DEFAULT "0",
            CHANGE `gift_amount` `reduction_amount` decimal(17,2) NOT NULL DEFAULT "0",
            CHANGE `gift_tax` `reduction_tax` tinyint(1) NOT NULL DEFAULT "0",
            CHANGE `gift_currency` `reduction_currency` int(10) unsigned NOT NULL DEFAULT "0",
            ADD COLUMN `description` text DEFAULT NULL AFTER `id_prepayment_gifts`,
            ADD COLUMN `reduction_product` int(10) NOT NULL DEFAULT "0" AFTER `reduction_currency`
        ');

        $res &= Db::getInstance()->execute('
            UPDATE `' . _DB_PREFIX_ . $table . '` pg
            INNER JOIN `' . _DB_PREFIX_ . $table . '_lang` pgl ON (
                pg.`id_prepayment_gifts` = pgl.`id_prepayment_gifts`
                AND pgl.`id_lang` = '.(int)Context::getContext()->language->id.'
            )
            SET pg.`description` = pgl.`description`
        ');

        if (!$res) {
            return false;
        }

        if ($sum_deposit_amount
            && $sum_cash_back_amount
            && ($reduction = (int)round($sum_cash_back_amount * 100 / $sum_deposit_amount))
        ) {
            $gift = new PrepaymentGifts();
            foreach ($languages as $language) {
                $gift->name[$language['id_lang']] = 'deposits';
            }
            $gift->description = 'cash back on deposit products';
            $gift->date_from = date('Y-m-d');
            $gift->date_to = date('Y-m-d', strtotime('+1 year', strtotime($gift->date_from)));
            $gift->quantity = 5000;
            $gift->quantity_per_user = 5000;
            $gift->priority = 1;
            $gift->product_restriction = 1;
            $gift->reduction_percent = $reduction;
            $gift->reduction_product = 1;
            $gift->active = 1;
            if ($gift->add()) {
                $restrictions = array();
                $products = $object->getProducts(Shop::getContextListShopID(), true);
                foreach ($products as $product) {
                    $restrictions[] = array(
                        'type' => 'products',
                        'quantity' => '1',
                        'id' => (int)$product
                    );
                }
                $gift->setWsProductRules($restrictions);
            }
        }
    }

    $table = 'prepayment_gifts_lang';
    $alter_sql = '
        SELECT *
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = "'._DB_PREFIX_.$table.'"
        AND TABLE_SCHEMA = "'._DB_NAME_.'"
        AND COLUMN_NAME = "description"
    ';

    if (Db::getInstance()->executeS($alter_sql)) {
        if (!Db::getInstance()->execute('
            ALTER TABLE `'._DB_PREFIX_.$table.'`
            DROP COLUMN `description`
        ')) {
            return false;
        }
    }

    // Unregister hooks
    if (!$object->unregisterHook('actionProductSave')
        || !$object->unregisterHook('actionProductDelete')
        || !$object->unregisterHook('actionObjectAddBefore')
        || !$object->unregisterHook('actionObjectUpdateBefore')
    ) {
        return false;
    }

    // Delete confs
    Configuration::deleteByName('WALLET_DEFAULT_CURRENCY_PACKS');
    Configuration::deleteByName('WALLET_PARTIAL_PAYMENT');
    Configuration::deleteByName('WALLET_DISPLAY_PACKS');
    Configuration::deleteByName('WALLET_PACKS_CAT');

    // Delete deprecated tables
    Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'prepayment_packs`');
    Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'prepayment_packs_lang`');

    // Delete backup
    foreach ($tables as $table) {
        if (!Db::getInstance()->execute('
            DROP TABLE IF EXISTS `'._DB_PREFIX_.$table.'_bk`
        ')) {
            return false;
        }
    }

    return true;
}
