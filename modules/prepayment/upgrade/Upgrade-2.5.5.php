<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_5_5($object)
{
    Configuration::deleteByName('WALLET_ALLOW_NEGATIVE_BALANCE');

    if (!$object->registerHook('actionOrderSlipAdd')
        || !$object->registerHook('actionObjectAddBefore')
        || !$object->registerHook('actionObjectUpdateBefore')
        || !$object->registerHook('actionObjectDeleteAfter')
        || !$object->registerHook('actionObjectDeleteBefore')
        || !$object->registerHook('addWebserviceResources')) {
        return false;
    }

    if (version_compare(_PS_VERSION_, '1.7', '>=')) {
        $tabs = Tab::getCollectionFromModule($object->name, Configuration::get('PS_LANG_DEFAULT'));
        foreach ($tabs as $tab) {
            if ($tab->class_name == 'AdminPrepaymentDashboard') {
                continue;
            }

            $tab->id_parent = 0;
            $tab->update();
        }
    }

    // Create new tables
    $sql = array();
    include(_PS_MODULE_DIR_.$object->name.'/sql/install.php');
    foreach ($sql as $s) {
        if (!Db::getInstance()->execute($s)) {
            return false;
        }
    }

    // Populate new tables
    $gifts = PrepaymentGifts::getGifts();
    $last_activities = PrepaymentLastActivities::getLastActivities();
    foreach ($last_activities as $last_activity) {
        $movement = new PrepaymentLastActivities((int)$last_activity['id_prepayment_last_activities']);
        if (!Validate::isLoadedObject($movement)) {
            continue;
        }

        if (isset($last_activity['id_order']) && $last_activity['id_order']) {
            $movement->addOrder($last_activity['id_order']);
        }

        if ($movement->id_operation == PrepaymentLastActivities::GIFT) {
            foreach ($gifts as $gift) {
                if ($gift['reference'] == $movement->reference) {
                    $movement->addGift($gift['id_prepayment_gifts']);
                }
            }
        }
    }

    $labels = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
    SELECT plal.*
    FROM `'._DB_PREFIX_.'prepayment_last_activities_lang` plal');

    if (is_array($labels) && count($labels)) {
        $labels_value = array();
        $languages = Language::getLanguages(false);

        foreach ($labels as $label) {
            if (isset($label['label']) && !empty($label['label'])) {
                foreach ($languages as $language) {
                    $labels_value[] = array(
                        'id_prepayment_last_activities' => (int)$label['id_prepayment_last_activities'],
                        'id_lang' => (int)$language['id_lang'],
                        'label' => pSQL($label['label'])
                    );
                }
            }
        }

        Db::getInstance()->insert('prepayment_last_activities_lang', $labels_value, false, true, DB::INSERT_IGNORE);
    }

    if (!Db::getInstance()->execute('
        INSERT `'._DB_PREFIX_.'prepayment_last_activities_refund`
        SELECT `id_prepayment_last_activities`, `id_order_slip`
        FROM `'._DB_PREFIX_.'prepayment_refunds`')
    ) {
        return false;
    }

    // Update tables
    if (!Db::getInstance()->execute('
        DROP TABLE IF EXISTS `'._DB_PREFIX_.'prepayment_refunds`')
    ) {
        return false;
    }

    if (!Db::getInstance()->execute('
        ALTER TABLE `'._DB_PREFIX_.'prepayment_packs`
        DROP `id_currency`')
    ) {
        return false;
    }

    if (!Db::getInstance()->execute('
        ALTER TABLE `'._DB_PREFIX_.'prepayment_gifts`
        DROP `reference`,
        DROP `highlight`')
    ) {
        return false;
    }

    if (!Db::getInstance()->execute('
        ALTER TABLE `'._DB_PREFIX_.'prepayment_last_activities`
        DROP `id_order`,
        DROP `id_customer`,
        DROP `price`')
    ) {
        return false;
    }

    if (!Db::getInstance()->execute('
        ALTER TABLE `'._DB_PREFIX_.'prepayment_last_activities_lang`
        CHANGE `id_prepayment_last_activities`
        `id_prepayment_last_activities` int(10) unsigned NOT NULL')
    ) {
        return false;
    }

    if (!Db::getInstance()->execute('
        DELETE FROM `' . _DB_PREFIX_ . 'prepayment_partials`
        WHERE `active` = 0')) {
        return false;
    }

    if (!Db::getInstance()->execute('
        ALTER TABLE `'._DB_PREFIX_.'prepayment_partials`
        DROP `active`')
    ) {
        return false;
    }

    return true;
}
