<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

/* Security */
if (!defined('_PS_VERSION_')) {
    exit;
}

/* Checking compatibility with older PrestaShop and fixing it */
if (!defined('_MYSQL_ENGINE_')) {
    define('_MYSQL_ENGINE_', 'MyISAM');
}

require_once(_PS_MODULE_DIR_.'prepayment/models/PrepaymentWallets.php');
require_once(_PS_MODULE_DIR_.'prepayment/models/PrepaymentLastActivities.php');
require_once(_PS_MODULE_DIR_.'prepayment/models/PrepaymentGifts.php');
require_once(_PS_MODULE_DIR_.'prepayment/models/PrepaymentPartials.php');

class Prepayment extends PaymentModule
{
    private $_html = '';

    private $_post_errors = array();

    private $_warnings = array();

    public $product = array(
        'default' => '10',
        'from' => '10',
        'to' => '200',
        'pitch' => '1',
        'fixed' => '10,20,50,100,200',
        'active' => '1',
    );

    public $attribute_goups = array(array(
        'name' => 'amount',
        'value' => array(
            'en' => 'Deposit amount',
            'fr' => 'Montant des dépôts'
        ),
    ));

    public $tabs = array(
        array(
            'name' => 'Wallet',
            'className' => 'AdminPrepaymentDashboard'
        ),
        array(
            'name' => 'Wallets',
            'className' => 'AdminPrepaymentWallets'
        ),
        array(
            'name' => 'Last Activities',
            'className' => 'AdminPrepaymentLastActivities'
        ),
        array(
            'name' => 'Gifts',
            'className' => 'AdminPrepaymentGifts'
        )
    );

    public $metas = array(
        array(
            'controller' => 'deposits',
            'title' => array('en' => 'deposits', 'fr' => 'deposits'),
            'description' => array('en' => 'Deposit funds', 'fr' => 'Déposer des fonds'),
        ),
        array(
            'controller' => 'dashboard',
            'title' => array('en' => 'wallet-dashboard', 'fr' => 'wallet-dashboard'),
            'description' => array('en' => 'Wallet details', 'fr' => 'Détails du porte-monnaie'),
        ),
        array(
            'controller' => 'payment'
        ),
        array(
            'controller' => 'validation'
        )
    );

    protected $force_delete = false;

    public function __construct()
    {
        $this->name                = 'prepayment';
        $this->tab                 = 'payments_gateways';
        $this->version             = '2.6.2';
        $this->author              = 'Keyrnel';
        $this->module_key          = '45433c1bca07dd2a8375cc1d3c081e61';
        $this->bootstrap           = true;
        $this->currencies          = true;
        $this->currencies_mode     = 'checkbox';

        parent::__construct();

        $this->displayName         = $this->l('Wallet');
        $this->description         = $this->l('Prepayment wallet is the simplest payment method which allows customers to deposit funds in their own wallet in order to purchase orders.');

        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->l('No currency has been set for this module.');
        }
    }

    public function install($delete_params = true)
    {
        if (!parent::install()
            || !$this->registerHook('header')
            || !$this->registerHook('displayBackOfficeTop')
            || !$this->registerHook('displayNav')
            || !$this->registerHook('displayNav2')
            || !$this->registerHook('displayTop')
            || !$this->registerHook('leftColumn')
            || !$this->registerHook('actionOrderSlipAdd')
            || !$this->registerHook('displayShoppingCartFooter')
            || !$this->registerHook('actionAdminControllerSetMedia')
            || !$this->registerHook('actionOrderStatusPostUpdate')
            || !$this->registerHook('actionCustomerAccountAdd')
            || !$this->registerHook('displayCustomerAccount')
            || !$this->registerHook('actionObjectDeleteBefore')
            || !$this->registerHook('actionObjectDeleteAfter')
            || !$this->registerHook('actionAdminProductsListingFieldsModifier')
            || !$this->registerHook('actionAdminAttributesGroupsListingFieldsModifier')
            || !$this->registerHook('actionValidateOrder')
            || !$this->registerHook('actionPaymentCCAdd')
            || !$this->registerHook('actionOrderStatusUpdate')
            || !$this->registerHook('displayShoppingCart')
            || !$this->registerHook('payment')
            || !$this->registerHook('paymentOptions')
            || !$this->registerHook('paymentReturn')
            || !$this->registerHook('displayPaymentTop')
            || !$this->registerHook('cart')
            || !$this->registerHook('addWebserviceResources')
            || !$this->registerHook('displayAdminCustomers')
            || !$this->registerHook('actionObjectPaypalOrderAddAfter')) {
            return false;
        }

        // Install database & conf
        if ($delete_params) {
            if (!$this->installDb() || !$this->installConf()) {
                return false;
            }
        }

        $languages = Language::getLanguages();

        if (!$this->installTabs($languages)
            || !$this->installAttributeGroups($languages)
            || !$this->installProduct($languages)
            || !$this->installMetas($languages)
            || !$this->initMails($languages)
        ) {
            return false;
        }

        return true;
    }

    public function installTabs($languages)
    {
        $id_parent = version_compare(_PS_VERSION_, '1.7', '>=') ? 0 : -1;
        foreach ($this->tabs as $tab) {
            $obj = new Tab();
            foreach ($languages as $lang) {
                $obj->name[$lang['id_lang']] = $this->l($tab['name']);
            }
            $obj->class_name = $tab['className'];
            $obj->id_parent = $tab['className'] == 'AdminPrepaymentDashboard' ? (int)Tab::getIdFromClassName('AdminParentCustomer') : $id_parent;
            $obj->module = $this->name;
            if (!$obj->add()) {
                return false;
            }
        }

        return true;
    }

    public function installAttributeGroups($languages)
    {
        foreach ($this->attribute_goups as $attribute_group) {
            $attribute_group_obj = new AttributeGroup();
            foreach ($languages as $language) {
                if (isset($attribute_group['value'][$language['iso_code']]) && !empty($attribute_group['value'][$language['iso_code']])) {
                    $attribute_group_obj->name[(int) $language['id_lang']] = $attribute_group['value'][$language['iso_code']];
                    $attribute_group_obj->public_name[(int) $language['id_lang']] = $attribute_group['value'][$language['iso_code']];
                } else {
                    $attribute_group_obj->name[(int) $language['id_lang']] = $attribute_group['value']['en'];
                    $attribute_group_obj->public_name[(int) $language['id_lang']] = $attribute_group['value']['en'];
                }
            }
            $attribute_group_obj->group_type = 'radio';
            $attribute_group_obj->id_shop_list = Shop::getShops(true, null, true);

            if (!$attribute_group_obj->add()) {
                return false;
            }

            Configuration::updateGlobalValue('WALLET_ATTRGROUP_' . Tools::strtoupper($attribute_group['name']), (int) $attribute_group_obj->id);
        }

        return true;
    }

    public function installProduct($languages)
    {
        $currencies = Currency::getCurrencies(false, true, true);
        $currency_installed = array();

        foreach ($currencies as $currency) {
            if (in_array($currency['id_currency'], $currency_installed) || $currency['deleted'] || !$currency['active']) {
                continue;
            }

            $shops = $this->getShopsByIdCurrency($currency['id_currency']);

            $product = new Product();
            foreach ($languages as $language) {
                if ($language['iso_code'] == 'fr') {
                    $product->name[(int) $language['id_lang']] = 'Dépôt porte-monnaie';
                    $product->link_rewrite[(int) $language['id_lang']] = 'depot-porte-monnaie';
                } else {
                    $product->name[(int) $language['id_lang']] = 'Wallet deposit';
                    $product->link_rewrite[(int) $language['id_lang']] = 'wallet-deposit';
                }
            }
            $product->id_category_default = Configuration::get('PS_HOME_CATEGORY');
            $product->category = array(Configuration::get('PS_HOME_CATEGORY'));
            $product->active = 1;
            $product->customizable = 0;
            $product->is_virtual = 1;
            $product->visibility = 'none';
            $product->id_tax_rules_group = 0;
            $product->id_shop_list = $shops;

            if (!$product->add()) {
                return false;
            }

            $product->updateCategories($product->category);

            Configuration::updateGlobalValue('WALLET_PRODUCT_' . (int) $currency['id_currency'], (int) $product->id);
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $product->id, $this->product['from']);
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_TO_' . (int) $product->id, $this->product['to']);
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $product->id, $this->product['fixed']);
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $product->id, $this->product['default']);
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $product->id, $this->product['pitch']);

            if (file_exists(_PS_MODULE_DIR_ . $this->name . '/views/img/amount.jpg')) {
                $image = new Image();
                $image->id_product = (int) $product->id;
                $image->position = Image::getHighestPosition($product->id) + 1;
                foreach ($languages as $language) {
                    $image->legend[(int) $language['id_lang']] = 'moneypot';
                }

                if (!Image::getCover($image->id_product)) {
                    $image->cover = 1;
                } else {
                    $image->cover = 0;
                }

                $image->id_shop_list = $shops;
                $image->add();
                $new_path = $image->getPathForCreation();
                ImageManager::resize(_PS_MODULE_DIR_ . $this->name . '/views/img/amount.jpg', $new_path . '.jpg', null, null, 'jpg');
                $images_types = ImageType::getImagesTypes('products');
                foreach ($images_types as $imageType) {
                    ImageManager::resize(
                        $new_path . '.jpg',
                        $new_path . '-' . Tools::stripslashes($imageType['name']) . '.jpg',
                        $imageType['width'],
                        $imageType['height']
                    );
                }

                $image->associateTo($shops);
            }

            // init wallet amounts
            $fixed_list = array_map('floatval', explode(',', $this->product['fixed']));
            foreach ($fixed_list as $amount) {
                if ($this->isAttribute((int) Configuration::get('WALLET_ATTRGROUP_AMOUNT'), $amount, $this->context->language->id)) {
                    continue;
                }

                $attribute = new Attribute();
                $attribute->id_attribute_group = (int) Configuration::get('WALLET_ATTRGROUP_AMOUNT');
                foreach ($languages as $language) {
                    $attribute->name[(int) $language['id_lang']] = (float) $amount;
                }
                $attribute->position = Attribute::getHigherPosition($attribute->id_attribute_group) + 1;
                $attribute->id_shop_list = $shops;
                $attribute->add();
                $attribute->cleanPositions($attribute->id_attribute_group, false);
            }

            // init combinations
            $ids = $this->getDefaultAttributes($product->id);
            $combinations = array_values($this->createCombinations($ids));
            $values = array_values(array_map(array($this, 'getCombinationProperties'), array($product->id), array($currency['id_currency']), $combinations));
            $this->generateMultipleCombinations($product->id, $values, $combinations, $shops);
            $id_combination = (int) $product->productAttributeExists($combinations[0], false, null, true, true);
            $this->setDefaultAttribute($product->id, $id_combination, $shops);

            $currency_installed[] = (int) $currency['id_currency'];
        }

        return true;
    }

    public function installMetas($languages)
    {
        foreach ($this->metas as $meta) {
            $obj = new Meta();
            $obj->page = 'module-'.$this->name.'-'.$meta['controller'];
            $obj->configurable = 1;
            foreach ($languages as $language) {
                if ($language['iso_code'] == 'fr') {
                    $obj->title[(int)$language['id_lang']] = isset($meta['title'][$language['iso_code']]) ? $meta['title'][$language['iso_code']] : '';
                    $obj->description[(int)$language['id_lang']] = isset($meta['description'][$language['iso_code']]) ? $meta['description'][$language['iso_code']] : '';
                    $obj->url_rewrite[(int)$language['id_lang']] = Tools::link_rewrite($obj->title[(int)$language['id_lang']]);
                } else {
                    $obj->title[(int)$language['id_lang']] = isset($meta['title'][$language['iso_code']]) ? $meta['title'][$language['iso_code']] : '';
                    $obj->description[(int)$language['id_lang']] = isset($meta['description'][$language['iso_code']]) ? $meta['description'][$language['iso_code']] : '';
                    $obj->url_rewrite[(int)$language['id_lang']] = Tools::link_rewrite($obj->title[(int)$language['id_lang']]);
                }
            }
            if (!$obj->add()) {
                return false;
            }

            if (version_compare(_PS_VERSION_, '1.6', '>=') && version_compare(_PS_VERSION_, '1.7', '<')) {
                $themes = Theme::getThemes();
                $theme_meta_value = array();
                foreach ($themes as $theme) {
                    $theme_meta_value[] = array(
                        'id_theme' => $theme->id,
                        'id_meta' => (int)$obj->id,
                        'left_column' => (int)$theme->default_left_column,
                        'right_column' => (int)$theme->default_right_column
                    );
                }
                if (count($theme_meta_value) > 0) {
                    Db::getInstance()->insert('theme_meta', (array)$theme_meta_value, false, true, DB::INSERT_IGNORE);
                }
            }
        }

        return true;
    }

    public function initMails($languages)
    {
        $path = _PS_MODULE_DIR_.$this->name.'/mails/en';

        foreach ($languages as $language) {
            $dest = _PS_MODULE_DIR_.$this->name.'/mails/'.$language['iso_code'];
            if (file_exists($dest)) {
                continue;
            }

            if (!mkdir($dest, 0777, true)) {
                continue;
            }

            $files = scandir($path);
            foreach ($files as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }

                copy($path.'/'.$file, $dest.'/'.$file);
            }
        }

        return true;
    }

    protected function installDb()
    {
        $sql = array();
        include(dirname(__FILE__).'/sql/install.php');
        foreach ($sql as $s) {
            if (!Db::getInstance()->execute($s)) {
                return false;
            }
        }

        return true;
    }

    protected function installConf()
    {
        Configuration::updateGlobalValue('PS_COMBINATION_FEATURE_ACTIVE', true);
        Configuration::updateValue('WALLET_CURRENCY_DEFAULT', Configuration::get('PS_CURRENCY_DEFAULT'));
        Configuration::updateValue('WALLET_NEGATIVE_BALANCE_MAX', 0);
        Configuration::updateValue('WALLET_ALLOW_DISBURSEMENT', 0);
        Configuration::updateValue('WALLET_AUTO_REFUND', 0);
        Configuration::updateValue('WALLET_DISPLAY_GIFTS', 1);
        Configuration::updateValue('WALLET_DISPLAY_TOPMENU', 0);
        Configuration::updateValue('WALLET_AUTO_OPEN', 1);
        Configuration::updateValue('WALLET_ALLOW_PARTIAL_PAYMENT', 0);
        Configuration::updateValue('WALLET_NOTIFICATION_DEPOSIT', 0);
        Configuration::updateValue('WALLET_NOTIFICATION_ORDER', 0);
        Configuration::updateValue('WALLET_NOTIFICATION_REFUND', 0);
        Configuration::updateValue('WALLET_NOTIFICATION_DISBURSEMENT', 0);
        Configuration::updateValue('WALLET_NOTIFICATION_GIFT', 0);
        Configuration::updateValue('WALLET_ALLOW_CART_STACK', 1);

        return true;
    }

    public function uninstall($delete_params = true)
    {
        $this->force_delete = true;
        // Uninstall Module
        if (!parent::uninstall()
            || !$this->uninstallProduct()
            || !$this->uninstallAttributeGroups()
            || !$this->uninstallMetas()
            || !$this->uninstallTabs()
            || !$this->removeTopMenuLink()
        ) {
            return false;
        }

        // Uninstall database & conf
        if ($delete_params) {
            if (!$this->uninstallDB() || !$this->uninstallConf()) {
                return false;
            }
        }

        return true;
    }

    public function uninstallProduct()
    {
        $currencies = Currency::getCurrencies(false, true, true);
        $currency_uninstalled = array();

        foreach ($currencies as $currency) {
            if (in_array($currency['id_currency'], $currency_uninstalled)) {
                continue;
            }

            $shops = $this->getShopsByIdCurrency((int) $currency['id_currency']);
            $product = new Product(Configuration::get('WALLET_PRODUCT_' . (int) $currency['id_currency']));
            if (Validate::isLoadedObject($product)) {
                $product->id_shop_list = $shops;
                $product->delete();

                Configuration::deleteByName('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $product->id);
                Configuration::deleteByName('WALLET_PRODUCT_AMOUNT_TO_' . (int) $product->id);
                Configuration::deleteByName('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $product->id);
                Configuration::deleteByName('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $product->id);
                Configuration::deleteByName('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $product->id);
                Configuration::deleteByName('WALLET_PRODUCT_' . (int) $currency['id_currency']);
            }

            $currency_uninstalled[] = (int) $currency['id_currency'];
        }

        return true;
    }

    public function uninstallAttributeGroups()
    {
        foreach ($this->attribute_goups as $attribute_group) {
            $id_attribute_group = Configuration::get('WALLET_ATTRGROUP_' . Tools::strtoupper($attribute_group['name']));
            $attribute_group_obj = new AttributeGroup((int) $id_attribute_group);
            if (Validate::isLoadedObject($attribute_group_obj)) {
                $attribute_group_obj->id_shop_list = Shop::getShops(true, null, true);
                $attribute_group_obj->delete();
                Configuration::deleteByName('WALLET_ATTRGROUP_' . Tools::strtoupper($attribute_group['name']));
            }
        }

        return true;
    }

    public function uninstallMetas()
    {
        foreach ($this->metas as $meta) {
            $metas = Meta::getMetaByPage('module-'.$this->name.'-'.$meta['controller'], (int)$this->context->language->id);
            $obj = new Meta((int)$metas['id_meta']);
            if ($obj->delete() && version_compare(_PS_VERSION_, '1.6', '>=') && version_compare(_PS_VERSION_, '1.7', '<')) {
                Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'theme_meta` WHERE id_meta='.(int)$obj->id);
            }
        }

        return true;
    }

    public function uninstallTabs()
    {
        $tabs = Tab::getCollectionFromModule($this->name);
        foreach ($tabs as $tab) {
            $tab->delete();
        }

        return true;
    }

    protected function uninstallDb()
    {
        $sql = array();
        include(dirname(__FILE__).'/sql/uninstall.php');
        foreach ($sql as $s) {
            if (!Db::getInstance()->execute($s)) {
                return false;
            }
        }

        return true;
    }

    protected function uninstallConf()
    {
        Configuration::deleteByName('WALLET_CURRENCY_DEFAULT');
        Configuration::deleteByName('WALLET_NEGATIVE_BALANCE_MAX');
        Configuration::deleteByName('WALLET_ALLOW_DISBURSEMENT');
        Configuration::deleteByName('WALLET_AUTO_REFUND');
        Configuration::deleteByName('WALLET_DISPLAY_GIFTS');
        Configuration::deleteByName('WALLET_DISPLAY_TOPMENU');
        Configuration::deleteByName('WALLET_AUTO_OPEN');
        Configuration::deleteByName('WALLET_ALLOW_PARTIAL_PAYMENT');
        Configuration::deleteByName('WALLET_NOTIFICATION_GIFT');
        Configuration::deleteByName('WALLET_NOTIFICATION_ORDER');
        Configuration::deleteByName('WALLET_NOTIFICATION_REFUND');
        Configuration::deleteByName('WALLET_NOTIFICATION_DISBURSEMENT');
        Configuration::deleteByName('WALLET_NOTIFICATION_DEPOSIT');
        Configuration::deleteByName('WALLET_ALLOW_CART_STACK');

        return true;
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }

        return true;
    }

    public function getShopsByIdCurrency($id_currency)
    {
        $results = array();

        $rows = Db::getInstance()->executeS('
		SELECT cs.id_shop
		FROM  `' . _DB_PREFIX_ . 'currency_shop` cs
		WHERE cs.`id_currency` = ' . (int) $id_currency);

        foreach ($rows as $row) {
            $results[] = $row['id_shop'];
        }

        return $results;
    }

    public function enable($force_all = false)
    {
        $products = $this->getProducts(Shop::getContextListShopID(), true);
        $this->updateProductsStatus(Shop::getContextListShopID(), $products, 1);

        if (!parent::enable($force_all)) {
            return false;
        }

        return true;
    }

    public function disable($force_all = false)
    {
        $products = $this->getProducts(Shop::getContextListShopID(), true);
        $this->updateProductsStatus(Shop::getContextListShopID(), $products, 0);

        if (!parent::disable($force_all)) {
            return false;
        }

        return true;
    }

    public function getProducts($shops, $list = false)
    {
        $products = array();

        $currencies = Currency::getCurrencies(false, true, true);
        foreach ($currencies as $currency) {
            $product = new Product((int) Configuration::get('WALLET_PRODUCT_' . (int) $currency['id_currency']));
            if (!Validate::isLoadedObject($product)) {
                continue;
            }

            foreach ($shops as $shop) {
                if ($product->isAssociatedToShop($shop)) {
                    if ($list) {
                        $products[] = $product->id;
                    } else {
                        $products[] = array(
                            'product' => $product,
                            'id_currency' => $currency['id_currency'],
                        );
                    }
                }
            }
        }

        return $products;
    }

    public function updateProductsStatus($shops, $products, $active)
    {
        if (!count($shops) || !count($products)) {
            return false;
        }

        return Db::getInstance()->update(
            'product_shop',
            array('active' => (int) $active),
            'id_shop IN (' . implode(',', array_map('intval', $shops)) . ') AND id_product IN (' . implode(',', array_map('intval', $products)) . ')'
        );
    }

    public function updateProductImage($product)
    {
        $key = 'WALLET_PRODUCT_IMAGE_' . (int) $product->id;
        if (isset($_FILES[$key]['tmp_name']) && !empty($_FILES[$key]['tmp_name'])) {
            $product->deleteImages();

            $max_size = isset($this->max_image_size) ? $this->max_image_size : 0;
            if ($error = ImageManager::validateUpload($_FILES[$key], Tools::getMaxUploadSize($max_size))) {
                $this->_post_errors[] = $error;
            }

            $tmp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
            if (!$tmp_name) {
                return false;
            }

            if (!move_uploaded_file($_FILES[$key]['tmp_name'], $tmp_name)) {
                return false;
            }

            if (!ImageManager::checkImageMemoryLimit($tmp_name)) {
                $this->_post_errors[] = $this->l('Due to memory limit restrictions, this image cannot be loaded. Please increase your memory_limit value on your server configuration.');
            }

            if (count($this->_post_errors)) {
                return false;
            }

            $image = new Image();
            $image->id_product = (int) $product->id;
            $image->position = Image::getHighestPosition($product->id) + 1;
            foreach ($languages as $language) {
                $image->legend[(int) $language['id_lang']] = 'moneypot';
            }

            if (!Image::getCover($image->id_product)) {
                $image->cover = 1;
            } else {
                $image->cover = 0;
            }

            $image->add();
            $new_path = $image->getPathForCreation();
            ImageManager::resize($tmp_name, $new_path . '.jpg', null, null, 'jpg');
            $images_types = ImageType::getImagesTypes('products');
            foreach ($images_types as $imageType) {
                ImageManager::resize(
                    $new_path . '.jpg',
                    $new_path . '-' . Tools::stripslashes($imageType['name']) . '.jpg',
                    $imageType['width'],
                    $imageType['height']
                );
            }

            unlink($tmp_name);

            $shops = Shop::getContextListShopID();
            $image->associateTo($shops);
        }

        return true;
    }

    public function updateProducts($postProducts = array())
    {
        $languages = Language::getLanguages();
        $shops = Shop::getContextListShopID();
        $products = $this->getProducts(Shop::getContextListShopID(), false);
        $id_attribute_group = (int) Configuration::get('WALLET_ATTRGROUP_AMOUNT');
        $attributes_list = array();
        $amounts_list = array();

        foreach ($products as $product) {
            // update product
            $objectProduct = new Product((int) $product['product']->id);
            $objectCurrency = new Currency((int) $product['id_currency']);

            if (!Validate::isLoadedObject($objectProduct)) {
                $this->_post_errors[] = $this->l(sprintf('Unable to load the product associated to the currency %s ', $objectCurrency->name));
                continue;
            }

            $objectProduct->active = $postProducts['WALLET_PRODUCT_ACTIVE_' . (int) $objectProduct->id];
            foreach ($languages as $language) {
                $objectProduct->name[(int) $language['id_lang']] = $postProducts['WALLET_PRODUCT_NAME_' . (int) $objectProduct->id][(int) $language['id_lang']];
            }

            $objectProduct->setFieldsToUpdate(array(
                'active' => $objectProduct->active,
                'name' => $objectProduct->name
            ));

            $objectProduct->update();
            $this->updateProductImage($objectProduct);

            // update amount configurations
            $custom_amount_from = (float) $postProducts['WALLET_PRODUCT_AMOUNT_FROM_' . (int) $objectProduct->id];
            $custom_amount_to = (float) $postProducts['WALLET_PRODUCT_AMOUNT_TO_' . (int) $objectProduct->id];
            $pitch = (float) $postProducts['WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $objectProduct->id];
            $fixed_amounts = $postProducts['WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $objectProduct->id];
            $fixed_list = array_map('floatval', explode(',', $fixed_amounts));
            $default_amount = (float) $postProducts['WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $objectProduct->id];

            if ($custom_amount_from > $custom_amount_to || $custom_amount_from == 0 || !(Validate::isPrice($custom_amount_from) && Validate::isPrice($custom_amount_to))) {
                $this->_post_errors[] = $this->l('Invalid amount limits for deposits.');
            }

            if (!in_array($default_amount, $fixed_list) || !Validate::isPrice($default_amount)) {
                $this->_post_errors[] = $this->l('Please select a default amount which belongs to the preset amounts');
            }

            if ($pitch == 0 || !Validate::isPrice($pitch)) {
                $this->_post_errors[] = $this->l('Invalid step amount for deposits');
            }

            foreach ($fixed_list as $key => $amount) {
                if (!Validate::isPrice($amount) || $amount == 0 || $amount < $custom_amount_from || $amount > $custom_amount_to) {
                    unset($fixed_list[$key]);
                }
            }

            if (count($this->_post_errors)) {
                continue;
            }

            Configuration::updateValue('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $objectProduct->id, (float) $custom_amount_from);
            Configuration::updateValue('WALLET_PRODUCT_AMOUNT_TO_' . (int) $objectProduct->id, (float) $custom_amount_to);
            Configuration::updateValue('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $objectProduct->id, (string) implode(',', $fixed_list));
            Configuration::updateValue('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $objectProduct->id, (float) $pitch);
            Configuration::updateValue('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $objectProduct->id, (float) $default_amount);

            $attributes = $this->getAttributes($this->context->language->id, (int) $id_attribute_group);
            foreach ($attributes as $attribute) {
                $attributes_list[$attribute['id_attribute']] = (float) $attribute['name'];
            }

            $amounts = array();
            for ($i = $custom_amount_from; $i <= $custom_amount_to; $i = $i + $pitch) {
                $amounts[] = $i;
            }

            $amounts = array_merge($amounts, $fixed_list);
            $amounts_list = array_merge($amounts_list, $amounts);
        }

        $list_add = array_diff($fixed_list, $attributes_list);
        $list_del = array_diff($attributes_list, $amounts_list);

        // Remove all deprecated amounts in attributes
        foreach ($list_del as $id_attribute => $amount) {
            if (!$this->isAttribute((int) $id_attribute_group, (float) $amount, $this->context->language->id)) {
                continue;
            }

            $attribute = new Attribute((int) $id_attribute);
            $attribute->delete();
        }

        // Add all new amounts in attributes
        foreach ($list_add as $amount) {
            if ($this->isAttribute((int) $id_attribute_group, (float) $amount, $this->context->language->id) || $amount == 0) {
                continue;
            }

            $attribute = new Attribute();
            $attribute->id_attribute_group = (int) $id_attribute_group;
            foreach ($languages as $language) {
                $attribute->name[(int) $language['id_lang']] = (float) $amount;
            }
            $attribute->position = Attribute::getHigherPosition((int) $id_attribute_group) + 1;
            $attribute->id_shop_list = $shops;
            $attribute->add();
            $attribute->cleanPositions((int) $id_attribute_group, false);
        }

        // update default product combination
        foreach ($products as $product) {
            $objectProduct = new Product((int) $product['product']->id);
            $ids = $this->getDefaultAttributes($objectProduct->id, Context::getContext()->language->id, $objectProduct->id_shop_default);
            $combinations = array_values($this->createCombinations($ids));
            $values = array_values(array_map(array($this, 'getCombinationProperties'), array($objectProduct->id), array((int) $product['id_currency']), $combinations));
            $this->generateMultipleCombinations($objectProduct->id, $values, $combinations, $shops);
            $id_combination = $objectProduct->productAttributeExists($combinations[0], false, null, true, true);
            $this->deleteDefaultAttributes($objectProduct->id, $shops);
            $this->setDefaultAttribute($objectProduct->id, $id_combination, $shops);
        }

        return true;
    }

    public function getContent()
    {
        $currencies_not_indexed = $this->getCurrenciesNotIndexed();

        if (count($currencies_not_indexed)) {
            $action_url = $this->getAdminLink('AdminModules', array('configure' => $this->name, 'tab_module' => $this->tab, 'module_name' => $this->name, 'indexCurrencies' => 1));
            $action_btn = '<div class="btn-group"><a class="btn btn-default" href="'.$action_url.'">'.$this->l('index currencies').'</a></div>';

            $this->_warnings[] = sprintf(
                $this->l('%s currencies must be indexed'),
                count($currencies_not_indexed)
            ).$action_btn;
        }

        if (Tools::isSubmit('submitWalletConfiguration')) {
            $products = array();
            $new_wallet_currency = null;

            foreach ($this->getConfigFieldsValues() as $key => $val) {
                if (preg_match('/WALLET_PRODUCT/', $key)) {
                    $products[$key] = $val;
                    continue;
                } elseif ($key == 'WALLET_CURRENCY_DEFAULT') {
                    $new_wallet_currency = Currency::getCurrencyInstance((int) $val);
                }

                Configuration::updateValue($key, $val);
            }

            if (count($products)) {
                $this->updateProducts($products);
            }

            if ($new_wallet_currency) {
                $this->updateWalletCurrency($new_wallet_currency);
            }

            $topmenu_module = version_compare(_PS_VERSION_, '1.7', '>=') ? 'Ps_MainMenu' : 'blocktopmenu';
            $topmenu = Module::getInstanceByName($topmenu_module);
            if ($topmenu && $topmenu->active) {
                $this->updateTopMenuDisplay();
            }

            if (!count($this->_post_errors)) {
                Tools::redirectAdmin($this->getAdminLink('AdminModules', array('configure' => $this->name, 'tab_module' => $this->tab, 'module_name' => $this->name, 'conf' => 4)));
            }
        } elseif (Tools::getValue('deleteProductImage')) {
            $product = new Product((int) Tools::getValue('idProduct'));
            if (Validate::isLoadedObject($product)) {
                $product->deleteImages();
                Tools::redirectAdmin($this->getAdminLink('AdminModules', array('configure' => $this->name, 'tab_module' => $this->tab, 'module_name' => $this->name, 'conf' => 7)));
            } else {
                $this->_post_errors[] = $this->l('An error occured while deleting the product image');
            }
        } elseif (Tools::getValue('indexCurrencies')) {
            foreach ($currencies_not_indexed as $id_currency) {
                if (!$this->duplicateProduct((int)$id_currency)) {
                    $currency = new Currency($id_currency);
                    $this->_post_errors[] = $this->l(sprintf('Unable to create the deposit product associated to the currency %s ', $currency->name[$this->context->language->id]));
                }
            }

            if (!count($this->_post_errors)) {
                Tools::redirectAdmin($this->getAdminLink('AdminModules', array('configure' => $this->name, 'tab_module' => $this->tab, 'module_name' => $this->name, 'conf' => 4)));
            }
        }

        foreach ($this->_post_errors as $error) {
            $this->_html .= $this->displayError($error);
        }

        foreach ($this->_warnings as $warning) {
            $this->_html .= $this->displayWarning($warning);
        }

        $this->_html .= $this->renderConfigForm();

        return $this->_html;
    }

    public function renderConfigForm()
    {
        $currentIndex = $this->getAdminLink('AdminModules', array('configure' => $this->name, 'tab_module' => $this->tab, 'module_name' => $this->name));
        $token = Tools::getAdminTokenLite('AdminModules');

        $fields_form[0] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('General'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Deposit product default currency'),
                        'desc' => $this->l('Select the default currency for deposit product.'),
                        'name' => 'WALLET_CURRENCY_DEFAULT',
                        'options' => array(
                            'query' => Currency::getCurrencies(),
                            'id' => 'id_currency',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable partial payment'),
                        'desc' => $this->l('Allow customers to pay orders both with wallet & other payment methods.'),
                        'name' => 'WALLET_ALLOW_PARTIAL_PAYMENT',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Maximum negative balance to purchase orders'),
                        'desc' => $this->l('Set to 0 to disable this feature.'),
                        'name' => 'WALLET_NEGATIVE_BALANCE_MAX',
                        'col' => '2',
                        'suffix' => Currency::getCurrencyInstance(Configuration::get('PS_CURRENCY_DEFAULT'))->sign,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Open wallet with customer account add'),
                        'desc' => $this->l('Enable this option to automatically open customer wallet on account creation.'),
                        'name' => 'WALLET_AUTO_OPEN',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Disbursement on deposits'),
                        'desc' => $this->l('Enable this option to allow refund of deposits. The deposit amount will be directly debited from the customer wallet.'),
                        'name' => 'WALLET_ALLOW_DISBURSEMENT',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Refund orders in wallet'),
                        'desc' => $this->l('Enable this option to automatically credit back customer wallet on refund of orders, even if the order has been paid with another payment method'),
                        'name' => 'WALLET_AUTO_REFUND',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Allow the customer to stack catalog products and credit packs in the cart'),
                        'desc' => $this->l('If this option is disabled, adding a catalog product to the cart will delete all the credit packs previously added and vice versa'),
                        'name' => 'WALLET_ALLOW_CART_STACK',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitWalletConfiguration',
                )
            ),
        );

        $fields_form[1] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Notifications'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Send email on deposit process'),
                        'desc' => $this->l('Enable this option to send an email informing about user wallet balance on deposit process.'),
                        'name' => 'WALLET_NOTIFICATION_DEPOSIT',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Send email on order process'),
                        'desc' => $this->l('Enable this option to send an email informing about user wallet balance on order process.'),
                        'name' => 'WALLET_NOTIFICATION_ORDER',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Send email on cash back process'),
                        'desc' => $this->l('Enable this option to send an email informing about user wallet balance on cash back process.'),
                        'name' => 'WALLET_NOTIFICATION_GIFT',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Send email on refund process'),
                        'desc' => $this->l('Enable this option to send an email informing about user wallet balance on refund process.'),
                        'name' => 'WALLET_NOTIFICATION_REFUND',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Send email on disbursement process'),
                        'desc' => $this->l('Enable this option to send an email informing about user wallet balance on disbursement process.'),
                        'name' => 'WALLET_NOTIFICATION_DISBURSEMENT',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitWalletConfiguration',
                ),
            ),
        );

        $fields_form[2] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Display'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display gift rules in column'),
                        'desc' => $this->l('Enable this option to display active gift rules in the left/right column.'),
                        'name' => 'WALLET_DISPLAY_GIFTS',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display a deposit funds link in top menu'),
                        'desc' => $this->l('Enable this option to add a quick link to the deposit funds page in the top menu.'),
                        'name' => 'WALLET_DISPLAY_TOPMENU',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitWalletConfiguration',
                ),
            ),
        );

        $fields_form[3] = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Deposit'),
                    'icon' => 'icon-cogs',
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitWalletConfiguration',
                )
            )
        );

        $currencies = Currency::getCurrencies();
        $inputs = array(
            array(
                'type' => 'select',
                'label' => $this->l('Currency'),
                'name' => 'currency_selector',
                'options' => array(
                    'query' => $currencies,
                    'id' => 'id_currency',
                    'name' => 'name'
                )
            )
        );

        foreach ($currencies as $currency) {
            $id_product = Configuration::get('WALLET_PRODUCT_' . (int) $currency['id_currency']);
            $product = new Product((int) $id_product);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }

            $cover = Product::getCover((int) $product->id);
            $id_image = $cover['id_image'];
            $image = _PS_PROD_IMG_DIR_ . Image::getImgFolderStatic($id_image) . $id_image . '.' . $this->context->controller->imageType;
            $image_url = ImageManager::thumbnail($image, 'product_' . (int) $product->id . '.' . $this->context->controller->imageType, 350, $this->context->controller->imageType, true, true);
            $image_size = file_exists($image) ? filesize($image) / 1000 : false;
            $images_types = ImageType::getImagesTypes('products');
            $format = array();
            $thumb = $thumb_url = '';
            $formated_product = is_callable('ImageType::getFormattedName') ? ImageType::getFormattedName('product') : ImageType::getFormatedName('product');
            $formated_medium = is_callable('ImageType::getFormattedName') ? ImageType::getFormattedName('medium') : ImageType::getFormatedName('medium');

            foreach ($images_types as $k => $image_type) {
                if ($formated_product == $image_type['name']) {
                    $format['product'] = $image_type;
                } elseif ($formated_medium == $image_type['name']) {
                    $format['medium'] = $image_type;
                    $thumb = _PS_PROD_IMG_DIR_ . $product->id . '-' . $image_type['name'] . '.' . $this->context->controller->imageType;
                    if (is_file($thumb)) {
                        $thumb_url = ImageManager::thumbnail($thumb, 'product_' . (int) $product->id . '-thumb.' . $this->context->controller->imageType, (int) $image_type['width'], $this->context->controller->imageType, true, true);
                    }
                }
            }

            $hide_class = $currency['id_currency'] != Configuration::get('WALLET_CURRENCY_DEFAULT') ? ' hide' : '';
            $form_group_class = 'currency-field currency-'.(int)$currency['id_currency'].$hide_class;

            $inputs = array_merge($inputs, array(

                array(
                    'type' => 'text',
                    'label' => $this->l('Product name'),
                    'form_group_class' => $form_group_class,
                    'desc' => $this->l('Enter the name which will be displayed for deposit product.'),
                    'name' => 'WALLET_PRODUCT_NAME_' . (int) $id_product,
                    'lang' => true,
                    'col' => '6',
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Cover image'),
                    'form_group_class' => $form_group_class,
                    'name' => 'WALLET_PRODUCT_IMAGE_' . (int) $id_product,
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'delete_url' => $currentIndex . '&deleteProductImage=1&idProduct=' . (int) $id_product,
                    'desc' => $this->l('This is the main image for deposit product.'),
                    'format' => isset($format['product']) && !empty($format['product']) ? $format['product'] : $format['medium'],
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Amount from'),
                    'form_group_class' => $form_group_class,
                    'desc' => $this->l('Enter the minimum limit for deposits.'),
                    'name' => 'WALLET_PRODUCT_AMOUNT_FROM_' . (int) $id_product,
                    'col' => '2',
                    'suffix' => $currency['sign'],
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Amount to'),
                    'form_group_class' => $form_group_class,
                    'desc' => $this->l('Enter the maximum limit for deposits.'),
                    'name' => 'WALLET_PRODUCT_AMOUNT_TO_' . (int) $id_product,
                    'col' => '2',
                    'suffix' => $currency['sign'],
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Step between each amount'),
                    'form_group_class' => $form_group_class,
                    'desc' => $this->l('Enter the step between each amount for deposits.'),
                    'name' => 'WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $id_product,
                    'col' => '2',
                    'suffix' => $currency['sign'],
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Preset amounts'),
                    'form_group_class' => $form_group_class,
                    'desc' => $this->l('Each amount must be followed by a comma.'),
                    'name' => 'WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $id_product,
                    'col' => '2',
                    'suffix' => $currency['sign'],
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Default amount'),
                    'form_group_class' => $form_group_class,
                    'desc' => $this->l('Choose the amount which will be selected by default for deposits'),
                    'name' => 'WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $id_product,
                    'col' => '2',
                    'suffix' => $currency['sign'],
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Active'),
                    'form_group_class' => $form_group_class,
                    'name' => 'WALLET_PRODUCT_ACTIVE_' . (int) $id_product,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                )
            ));
        }

        $fields_form[3]['form']['input'] = $inputs;

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->name;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitWalletConfiguration';
        $helper->currentIndex = $currentIndex;
        $helper->token = $token;
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm($fields_form);
    }

    public function getConfigFieldsValues()
    {
        $configs = array(
            'WALLET_CURRENCY_DEFAULT' => (int)Tools::getValue('WALLET_CURRENCY_DEFAULT', Configuration::get('WALLET_CURRENCY_DEFAULT')),
            'WALLET_NEGATIVE_BALANCE_MAX' => (int)Tools::getValue('WALLET_NEGATIVE_BALANCE_MAX', Configuration::get('WALLET_NEGATIVE_BALANCE_MAX')),
            'WALLET_ALLOW_DISBURSEMENT' => (bool)Tools::getValue('WALLET_ALLOW_DISBURSEMENT', Configuration::get('WALLET_ALLOW_DISBURSEMENT')),
            'WALLET_AUTO_REFUND' => (bool)Tools::getValue('WALLET_AUTO_REFUND', Configuration::get('WALLET_AUTO_REFUND')),
            'WALLET_DISPLAY_GIFTS' => (bool)Tools::getValue('WALLET_DISPLAY_GIFTS', Configuration::get('WALLET_DISPLAY_GIFTS')),
            'WALLET_AUTO_OPEN' => (bool)Tools::getValue('WALLET_AUTO_OPEN', Configuration::get('WALLET_AUTO_OPEN')),
            'WALLET_DISPLAY_TOPMENU' => (bool)Tools::getValue('WALLET_DISPLAY_TOPMENU', Configuration::get('WALLET_DISPLAY_TOPMENU')),
            'WALLET_ALLOW_PARTIAL_PAYMENT' => (bool)Tools::getValue('WALLET_ALLOW_PARTIAL_PAYMENT', Configuration::get('WALLET_ALLOW_PARTIAL_PAYMENT')),
            'WALLET_NOTIFICATION_DEPOSIT' => (bool)Tools::getValue('WALLET_NOTIFICATION_DEPOSIT', Configuration::get('WALLET_NOTIFICATION_DEPOSIT')),
            'WALLET_NOTIFICATION_ORDER' => (bool)Tools::getValue('WALLET_NOTIFICATION_ORDER', Configuration::get('WALLET_NOTIFICATION_ORDER')),
            'WALLET_NOTIFICATION_REFUND' => (bool)Tools::getValue('WALLET_NOTIFICATION_REFUND', Configuration::get('WALLET_NOTIFICATION_REFUND')),
            'WALLET_NOTIFICATION_DISBURSEMENT' => (bool)Tools::getValue('WALLET_NOTIFICATION_DISBURSEMENT', Configuration::get('WALLET_NOTIFICATION_DISBURSEMENT')),
            'WALLET_NOTIFICATION_GIFT' => (bool)Tools::getValue('WALLET_NOTIFICATION_GIFT', Configuration::get('WALLET_NOTIFICATION_GIFT')),
            'WALLET_ALLOW_CART_STACK' => (bool)Tools::getValue('WALLET_ALLOW_CART_STACK', Configuration::get('WALLET_ALLOW_CART_STACK')),
            'currency_selector' => (int)Configuration::get('WALLET_CURRENCY_DEFAULT')
        );

        $languages = Language::getLanguages();
        $currencies = Currency::getCurrencies();
        foreach ($currencies as $currency) {
            $id_product = Configuration::get('WALLET_PRODUCT_' . (int) $currency['id_currency']);
            $product = new Product((int) $id_product);

            $productName = array();
            foreach ($languages as $language) {
                $productName[$language['id_lang']] = Tools::getValue('WALLET_PRODUCT_NAME_' . (int) $id_product . '_' . (int) $language['id_lang'], $product->name[$language['id_lang']]);
            }

            $configs = array_merge($configs, array(
                'WALLET_PRODUCT_NAME_' . (int)$id_product => $productName,
                'WALLET_PRODUCT_AMOUNT_FROM_' . (int) $id_product => (float) Tools::getValue('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $id_product, Configuration::get('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $id_product)),
                'WALLET_PRODUCT_AMOUNT_TO_' . (int) $id_product => (float) Tools::getValue('WALLET_PRODUCT_AMOUNT_TO_' . (int) $id_product, Configuration::get('WALLET_PRODUCT_AMOUNT_TO_' . (int) $id_product)),
                'WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $id_product => Tools::getValue('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $id_product, Configuration::get('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $id_product)),
                'WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $id_product => (float) Tools::getValue('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $id_product, Configuration::get('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $id_product)),
                'WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $id_product => (float) Tools::getValue('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $id_product, Configuration::get('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $id_product)),
                'WALLET_PRODUCT_ACTIVE_' . (int) $id_product => Tools::getValue('WALLET_PRODUCT_ACTIVE_' . (int) $id_product, $product->active)
            ));
        }

        return $configs;
    }

    protected function updateWalletCurrency($currency)
    {
        //update Wallets
        $wallets = PrepaymentWallets::getWallets();
        foreach ($wallets as $wallet) {
            $wallet_object = new PrepaymentWallets((int)$wallet['id_prepayment_wallets']);
            PrepaymentLastActivities::updateWallet($wallet_object);
        }

        return true;
    }

    protected function addTopMenuLink($remove = false)
    {
        $id_linksmenutop = 0;
        $topmenu_class = version_compare(_PS_VERSION_, '1.7', '>=') ? 'Ps_MenuTopLinks' : 'MenuTopLinks';
        $labels = array();
        $page_link = array();
        $languages = Language::getLanguages();

        foreach ($languages as $language) {
            $label = ($language['iso_code'] == 'fr') ? 'Déposer des fonds' : 'Deposit money';
            $labels[(int)$language['id_lang']] = $label;
            $page_link[(int)$language['id_lang']] = $this->context->link->getModuleLink($this->name, 'deposits', array(), null, (int)$language['id_lang']);
            $links = $topmenu_class::gets((int)$language['id_lang'], null, (int)Shop::getContextShopID());
            foreach ($links as $link) {
                if ($link['link'] == $page_link[(int)$language['id_lang']]) {
                    $id_linksmenutop = (int)$link['id_linksmenutop'];
                    break 2;
                }
            }
        }

        if ($id_linksmenutop == 0 && !$remove) {
            $topmenu_class::add($page_link, $labels, 0, (int)Shop::getContextShopID());
            $id_linksmenutop = $this->addTopMenuLink();
        }

        return $id_linksmenutop;
    }

    protected function updateTopMenuDisplay()
    {
        $shop_id = (int)Shop::getContextShopID();
        $shop_group_id = Shop::getGroupFromShop($shop_id);
        $conf = Configuration::get('MOD_BLOCKTOPMENU_ITEMS', null, $shop_group_id, $shop_id);

        $display = Configuration::get('WALLET_DISPLAY_TOPMENU');

        $id_linksmenutop = $this->addTopMenuLink();

        if (!$display) {
            $topmenu_class = version_compare(_PS_VERSION_, '1.7', '>=') ? 'Ps_MenuTopLinks' : 'MenuTopLinks';
            $topmenu_class::remove($id_linksmenutop, $shop_id);

            Configuration::updateValue('MOD_BLOCKTOPMENU_ITEMS', (string)str_replace(array('LNK'.$id_linksmenutop.',', 'LNK'.$id_linksmenutop), '', $conf), false, $shop_group_id, $shop_id);
        } else {
            $menu_items = Tools::strlen($conf) ? explode(',', $conf) : array();
            if (!in_array('LNK'.$id_linksmenutop, $menu_items)) {
                $menu_items[] = 'LNK'.$id_linksmenutop;
            }

            Configuration::updateValue('MOD_BLOCKTOPMENU_ITEMS', (string)implode(',', $menu_items), false, (int)$shop_group_id, (int)$shop_id);
        }

        //clear top menu cache
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $dir = _PS_CACHE_DIR_.DIRECTORY_SEPARATOR.'ps_mainmenu';
            if (is_dir($dir)) {
                foreach (scandir($dir) as $entry) {
                    if (preg_match('/\.json$/', $entry)) {
                        unlink($dir.DIRECTORY_SEPARATOR.$entry);
                    }
                }
            }
        } else {
            $this->_clearCache('blocktopmenu.tpl');
        }

        return true;
    }

    public function removeTopMenuLink()
    {
        // Uninstall top-menu link
        $topmenu_class = version_compare(_PS_VERSION_, '1.7', '>=') ? 'Ps_MenuTopLinks' : 'MenuTopLinks';
        if (class_exists($topmenu_class)) {
            $shop_id = (int)Shop::getContextShopID();
            $shop_group_id = Shop::getGroupFromShop($shop_id);
            $conf = Configuration::get('MOD_BLOCKTOPMENU_ITEMS', null, $shop_group_id, $shop_id);

            $id_linksmenutop = $this->addTopMenuLink(true);
            $topmenu_class::remove($id_linksmenutop, $shop_id);

            Configuration::updateValue(
                'MOD_BLOCKTOPMENU_ITEMS',
                (string)str_replace(array('LNK'.$id_linksmenutop.',', 'LNK'.$id_linksmenutop), '', $conf),
                false,
                $shop_group_id,
                $shop_id
            );
        }

        return true;
    }

    public function isAttribute($id_attribute_group, $name, $id_lang)
    {
        if (is_callable(array('Attribute', 'isAttribute'))) {
            return Attribute::isAttribute($id_attribute_group, $name, $id_lang);
        }

        if (!Combination::isFeatureActive()) {
            return [];
        }

        $result = Db::getInstance()->getValue('
			SELECT COUNT(*)
			FROM `' . _DB_PREFIX_ . 'attribute_group` ag
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute_group_lang` agl
				ON (ag.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = ' . (int) $id_lang . ')
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute` a
				ON a.`id_attribute_group` = ag.`id_attribute_group`
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al
				ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) $id_lang . ')
			' . Shop::addSqlAssociation('attribute_group', 'ag') . '
			' . Shop::addSqlAssociation('attribute', 'a') . '
			WHERE al.`name` = \'' . pSQL($name) . '\' AND ag.`id_attribute_group` = ' . (int) $id_attribute_group . '
			ORDER BY agl.`name` ASC, a.`position` ASC
		');

        return (int) $result > 0;
    }

    public function getAttributeByName($id_attribute_group, $name, $id_lang)
    {
        return (int)Db::getInstance()->getValue('
			SELECT a.`id_attribute`
            FROM `' . _DB_PREFIX_ . 'attribute` a
			' . Shop::addSqlAssociation('attribute', 'a') . '
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al
				ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) $id_lang . ')
			WHERE al.`name` = \'' . pSQL($name) . '\' AND a.`id_attribute_group` = ' . (int) $id_attribute_group . '
			ORDER BY `position` ASC
		');
    }

    public function getDefaultAttributes($id_product, $id_lang = false, $id_shop = null)
    {
        $ids = array();

        if (!$id_lang) {
            $id_lang = (int) Context::getContext()->language->id;
        }

        $attributes = $this->getAttributes($id_lang, (int) Configuration::get('WALLET_ATTRGROUP_AMOUNT'), $id_shop);
        foreach ($attributes as $attribute) {
            if ($attribute['name'] == (float) Configuration::get('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $id_product)) {
                $ids[] = array((int) $attribute['id_attribute']);
                break;
            }
        }

        return $ids;
    }

    public function getAttributes($id_lang, $id_attribute_group, $id_shop = null)
    {
        if (!Combination::isFeatureActive()) {
            return array();
        }

        return Db::getInstance()->executeS('
			SELECT *
			FROM `' . _DB_PREFIX_ . 'attribute` a
			' . ($id_shop !== null ? ' INNER JOIN `' . _DB_PREFIX_ . 'attribute_shop` ash
				ON (a.`id_attribute` = ash.`id_attribute` AND ash.`id_shop` = ' . (int) $id_shop . ')' : '') . '
			LEFT JOIN `' . _DB_PREFIX_ . 'attribute_lang` al
				ON (a.`id_attribute` = al.`id_attribute` AND al.`id_lang` = ' . (int) $id_lang . ')
			WHERE a.`id_attribute_group` = ' . (int) $id_attribute_group . '
			ORDER BY `position` ASC
		');
    }

    public function createCombinations($list)
    {
        if (count($list) <= 1) {
            $callback = function ($value) {
                return (array($value));
            };

            return count($list) ? array_map($callback, $list[0]) : $list;
        }
        $res = array();
        $first = array_pop($list);
        foreach ($first as $attribute) {
            $tab = $this->createCombinations($list);
            foreach ($tab as $to_add) {
                $res[] = is_array($to_add) ? array_merge($to_add, array($attribute)) : array($to_add, $attribute);
            }
        }

        return $res;
    }

    public function getCombinationProperties($id_product, $id_currency, $attributes)
    {
        $price = 0;
        $product = new Product((int) $id_product);
        if (!Validate::isLoadedObject($product)) {
            return array();
        }

        $currency_from = new Currency((int) $id_currency);
        $currency_to = new Currency((int) Configuration::get('WALLET_CURRENCY_DEFAULT'));

        foreach ($attributes as $attribute) {
            $obj = new Attribute((int) $attribute);
            if (!Validate::isLoadedObject($obj)) {
                continue;
            }

            $price = Tools::ps_round(Tools::convertPriceFull((float) $obj->name[$this->context->language->id], $currency_from, $currency_to, false), 6);
        }

        return array(
            'id_product' => (int) $product->id,
            'price' => (float) $price,
            'weight' => 0,
            'ecotax' => 0,
            'reference' => 'wallet_deposit',
            'default_on' => 0,
            'minimal_quantity' => 1,
            'available_date' => date('Y-m-d H:i:s'),
        );
    }

    public function generateMultipleCombinations($id_product, $combinations, $attributes, $shops = array())
    {
        $product = new Product((int) $id_product);
        if (!Validate::isLoadedObject($product)) {
            return false;
        }

        $id_shop_list = $shops;
        if (!count($shops)) {
            $id_shop_list = Shop::getContextListShopID();
        }

        $res = true;

        foreach ($combinations as $key => $combination) {
            $id_combination = (int) $product->productAttributeExists($attributes[$key], false, null, true, true);
            $obj = new Combination((int) $id_combination);
            foreach ($combination as $field => $value) {
                $obj->$field = $value;
            }
            $obj->quantity = (int) StockAvailable::getQuantityAvailableByProduct($product->id, $obj->id) + 2;
            $obj->id_shop_list = $id_shop_list;
            if (version_compare(_PS_VERSION_, '1.7.3', '>=')) {
                $obj->low_stock_threshold = null;
                $obj->low_stock_alert = false;
            }

            $obj->setFieldsToUpdate($obj->getFieldsShop());
            $obj->save();

            foreach ($id_shop_list as $shop) {
                StockAvailable::updateQuantity($product->id, (int) $obj->id, 2, (int) $shop);
            }

            if (!$id_combination) {
                $attribute_list = array();
                foreach ($attributes[$key] as $id_attribute) {
                    $attribute = new Attribute((int) $id_attribute);

                    $attribute_list[] = array(
                        'id_product_attribute' => (int) $obj->id,
                        'id_attribute' => (int) $id_attribute,
                    );
                }

                $res &= Db::getInstance()->insert('product_attribute_combination', (array) $attribute_list);
            }
        }

        return $res;
    }

    public function setDefaultAttribute($id_product, $id_product_attribute, $shops = array())
    {
        $id_shop_list = $shops;
        if (!count($shops)) {
            $id_shop_list = Shop::getContextListShopID();
        }

        $result = Db::getInstance()->execute(
            'UPDATE `' . _DB_PREFIX_ . 'product_attribute_shop` pas
            LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON pas.id_product_attribute = pa.id_product_attribute
            SET pas.default_on = 1
            WHERE pa.id_product = ' . (int) $id_product . '
            AND pas.id_product_attribute = ' . (int) $id_product_attribute . '
            AND pas.id_shop IN (' . implode(',', array_map('intval', $id_shop_list)) . ')'
        );

        $result &= Db::getInstance()->execute(
            'UPDATE `' . _DB_PREFIX_ . 'product_shop` ps
            LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON ps.id_product = p.id_product
            SET ps.cache_default_attribute = ' . (int) $id_product_attribute . '
            WHERE p.id_product = ' . (int) $id_product . '
            AND ps.id_shop IN (' . implode(',', array_map('intval', $id_shop_list)) . ')'
        );

        return $result;
    }

    public function deleteDefaultAttributes($id_product, $shops = array())
    {
        $id_shop_list = $shops;
        if (!count($shops)) {
            $id_shop_list = Shop::getContextListShopID();
        }

        return Db::getInstance()->execute(
            'UPDATE `' . _DB_PREFIX_ . 'product_attribute_shop` pas
            LEFT JOIN `' . _DB_PREFIX_ . 'product_attribute` pa ON pas.id_product_attribute = pa.id_product_attribute
            SET pas.default_on = NULL
            WHERE pas.id_product = ' . (int) $id_product . '
            AND pas.default_on = 1
            AND pas.id_shop IN (' . implode(',', array_map('intval', $id_shop_list)) . ')'
        );
    }

    public function addComponentToShops($component, $id_shop_default, $shops)
    {
        if ($component['class_name'] == 'Product') {
            $object = new $component['class_name']((int) $component['id'], false, null, $id_shop_default);
        } else {
            $object = new $component['class_name']((int) $component['id'], null, $id_shop_default);
        }

        $object_shop_list = $shops;
        foreach ($object_shop_list as $key => $id_shop) {
            if ($object->isAssociatedToShop($id_shop)) {
                unset($object_shop_list[$key]);
            }
        }

        if (count($object_shop_list)) {
            $object->id_shop_list = $object_shop_list;
            $object->setFieldsToUpdate($object->getFieldsShop());
            $object->update();
        }

        unset($object);

        return true;
    }

    public function duplicateProduct($id_currency, $shops = array())
    {
        $id_shop_list = Shop::getContextListShopID();
        if (count($shops)) {
            $id_shop_list = $shops;
        }

        $id_default_currency = (int) Configuration::get('PS_CURRENCY_DEFAULT');
        $product = new Product((int) Configuration::get('WALLET_PRODUCT_' . (int) $id_default_currency));
        if (!Validate::isLoadedObject($product)) {
            return false;
        }

        $product = new Product((int) $product->id, false, null, $product->id_shop_default);
        foreach ($this->_attribute_goup as $attribute_group) {
            $this->addComponentToShops(
                array(
                    'class_name' => 'AttributeGroup',
                    'id' => (int) Configuration::get('WALLET_ATTRGROUP_' . Tools::strtoupper($attribute_group['name'])),
                ),
                $product->id_shop_default,
                $id_shop_list
            );
        }

        $id_product_old = $product->id;
        $id_shop_default = $product->id_shop_default;

        unset($product->id);
        unset($product->id_product);

        $product->id_shop_list = $id_shop_list;
        if ($product->add()
            && Category::duplicateProductCategories($id_product_old, $product->id)
            && Image::duplicateProductImages($id_product_old, $product->id, false)
        ) {
            Configuration::updateGlobalValue('WALLET_PRODUCT_' . (int) $id_currency, (int) $product->id);
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $product->id, Configuration::get('WALLET_PRODUCT_AMOUNT_FROM_' . (int) $id_product_old));
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_TO_' . (int) $product->id, Configuration::get('WALLET_PRODUCT_AMOUNT_TO_' . (int) $id_product_old));
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $product->id, Configuration::get('WALLET_PRODUCT_AMOUNT_FIXED_' . (int) $id_product_old));
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $product->id, Configuration::get('WALLET_PRODUCT_AMOUNT_DEFAULT_' . (int) $id_product_old));
            Configuration::updateGlobalValue('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $product->id, Configuration::get('WALLET_PRODUCT_AMOUNT_PITCH_' . (int) $id_product_old));
            Configuration::updateGlobalValue('WALLET_PRODUCT_ACTIVE_' . (int) $product->id, $product->active);

            $amount_group_object = new AttributeGroup((int) Configuration::get('WALLET_ATTRGROUP_AMOUNT'));
            $amount_attributes = $this->getAttributes((int) $this->context->language->id, (int) $amount_group_object->id, (int) $id_shop_default);
            foreach ($amount_attributes as $amount_attribute) {
                $this->addComponentToShops(
                    array(
                        'class_name' => 'Attribute',
                        'id' => (int) $amount_attribute['id_attribute'],
                    ),
                    $id_shop_default,
                    $id_shop_list
                );
            }

            $ids = $this->getDefaultAttributes($product->id, false, (int) $id_shop_default);
            $combinations = array_values($this->createCombinations($ids));
            $values = array_values(array_map(array($this, 'getCombinationProperties'), array($product->id), array($id_currency), $combinations));
            $this->generateMultipleCombinations($product->id, $values, $combinations, $id_shop_list);
            $id_combination = (int) $product->productAttributeExists($combinations[0], false, null, true, true);
            $this->deleteDefaultAttributes($product->id, $id_shop_list);
            $this->setDefaultAttribute($product->id, $id_combination, $id_shop_list);

            return true;
        }

        return false;
    }

    private function _makeRefund($order, $id_order_slip, $id_wallet, $deposit)
    {
        $last_activity = new PrepaymentLastActivities();
        $last_activity->id_wallet = (int)$id_wallet;
        $last_activity->id_currency = (int)$order->id_currency;
        $last_activity->paid = 1;

        $order_slip = new OrderSlip((int)$id_order_slip);
        $slip_amount = $order_slip->total_shipping_tax_incl;
        $message = '';
        $label = Validate::isCleanHtml(Tools::getValue('refund_to_wallet_label')) ? Tools::getValue('refund_to_wallet_label') : false;

        $products_ret = OrderSlip::getOrdersSlipDetail((int)$id_order_slip);
        foreach ($products_ret as $slip_detail) {
            $slip_amount += $slip_detail['amount_tax_incl'];
        }

        if ($deposit) {
            if (!Configuration::get('WALLET_ALLOW_DISBURSEMENT')) {
                return false;
            }

            $last_activity->id_operation = PrepaymentLastActivities::DISBURSEMENT;
            $message .= $this->l('Disburse to wallet')." : ".Tools::displayPrice($slip_amount, Currency::getCurrencyInstance((int)$last_activity->id_currency))."\r";
            if ($label) {
                $message .= $this->l('Label')." : ".$label."\r";
            }
        } else {
            if (!Configuration::get('WALLET_AUTO_REFUND')) {
                $total_amount_wallet = 0;
                $ids_last_activity = PrepaymentLastActivities::getIdsByIdOrder((int)$order->id);
                foreach ($ids_last_activity as $id_last_activity) {
                    $last_activity = new PrepaymentLastActivities((int)$id_last_activity['id_prepayment_last_activities']);
                    if ($last_activity->id_operation != PrepaymentLastActivities::ORDER) {
                        continue;
                    }

                    $total_amount_wallet += Tools::convertPriceFull(
                        $last_activity->amount,
                        Currency::getCurrencyInstance($last_activity->id_currency),
                        Currency::getCurrencyInstance($order->id_currency)
                    );
                }

                if ($slip_amount > $total_amount_wallet) {
                    $slip_amount = $total_amount_wallet;
                }
            }

            $last_activity->id_operation = PrepaymentLastActivities::REFUND;
            $message .= $this->l('Refund to wallet')." : ".Tools::displayPrice($slip_amount, Currency::getCurrencyInstance((int)$last_activity->id_currency))."\r";
            if ($label) {
                $message .= $this->l('Label')." : ".$label."\r";
            }
        }

        if ($slip_amount <= 0) {
            return false;
        }

        $last_activity->amount = $slip_amount;

        if ($label) {
            $languages = Language::getLanguages();
            foreach ($languages as $language) {
                $last_activity->label[$language['id_lang']] = $label;
            }
        }

        if ($last_activity->add()) {
            $last_activity->addOrder($order->id);
            $last_activity->addRefund($id_order_slip);
            $this->_addNewPrivateMessage($order, $message);
        }
    }

    public function _addNewPrivateMessage($order, $message)
    {
        $orderMessage = new CustomerMessage();
        $orderMessage->id_customer_thread = $this->_createOrderThread($order);
        $orderMessage->message = $message;
        $orderMessage->private = 1;
        return $orderMessage->add();
    }

    public function _createOrderThread($order)
    {
        $customer = new Customer($order->id_customer);
        $orderThread = new CustomerThread();
        $orderThread->id_contact = 0;
        $orderThread->id_customer = (int)$customer->id;
        $orderThread->id_shop = (int)$order->id_shop;
        $orderThread->id_order = (int)$order->id;
        $orderThread->id_lang = (int)$this->context->language->id;
        $orderThread->email = $customer->email;
        $orderThread->status = 'open';
        $orderThread->token = Tools::passwdGen(12);
        $orderThread->add();
        return (int)$orderThread->id;
    }

    public function checkCurrency($cart)
    {
        $currency_order = Currency::getCurrencyInstance((int)$cart->id_currency);
        $currencies_module = $this->getCurrency((int)$cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hookDisplayShoppingCartFooter($params)
    {
        $gift = PrepaymentGifts::getGift($this->context->cart);
        if (isset($gift) && !empty($gift)) {
            $cash_back_amount = Tools::displayPrice($gift->getContextualValue(true, $this->context->cart), Context::getContext()->currency);
            $this->context->smarty->assign('cash_back_amount', $cash_back_amount);
            return $this->display(__FILE__, 'shopping-cart.tpl');
        }
        return false;
    }

    public function hookRightColumn($params)
    {
        $gifts = Configuration::get('WALLET_DISPLAY_GIFTS')
            ? PrepaymentGifts::getCustomerGifts((int)$this->context->language->id, (int)$this->context->customer->id, true, true, true)
            : array();

        foreach ($gifts as $k => $gift) {
            $current_object = new PrepaymentGifts($gift['id_prepayment_gifts']);
            $gifts[$k]['countries'] = $current_object->getAssociatedRestrictions('country', true, true);
            $gifts[$k]['groups'] = $current_object->getAssociatedRestrictions('group', false, true);
            $gifts[$k]['shops'] = $current_object->getAssociatedRestrictions('shop', false, false);
            $gifts[$k]['payments'] = $current_object->getAssociatedRestrictions('payment', false, false);
            $gifts[$k]['carriers'] = $current_object->getAssociatedRestrictions('carrier', true, false);
            $gifts[$k]['product_rule_groups'] = $current_object->getProductRuleGroups();

            if (isset($gift['minimum_amount']) && $gift['minimum_amount'] > 0) {
                $gifts[$k]['minimum_amount'] = Tools::displayPrice(
                    Tools::convertPriceFull(
                        $gift['minimum_amount'],
                        Currency::getCurrencyInstance((int)$gift['minimum_amount_currency']),
                        $this->context->currency
                    )
                );
            } else {
                $gift['minimum_amount'] = null;
            }

            if (isset($gift['reduction_amount']) && $gift['reduction_amount'] > 0) {
                $gifts[$k]['reduction_amount'] = Tools::displayPrice(
                    Tools::convertPriceFull(
                        $gift['reduction_amount'],
                        Currency::getCurrencyInstance((int)$gift['reduction_currency']),
                        $this->context->currency
                    )
                );
            } else {
                $gift['reduction_amount'] = null;
            }

            foreach ($gifts[$k]['product_rule_groups'] as &$product_rule_group) {
                foreach ($product_rule_group['product_rules'] as &$product_rule) {
                    foreach ($product_rule['values'] as $key => $item) {
                        if ($product_rule['type'] == 'products') {
                            $product_rule['values'][$key] = Product::getProductName((int)$item);
                        } elseif ($product_rule['type'] == 'categories') {
                            $category = new Category((int)$item);
                            $product_rule['values'][$key] = $category->getName();
                        } elseif ($product_rule['type'] == 'manufacturers') {
                            $product_rule['values'][$key] = Manufacturer::getNameById((int)$item);
                        } elseif ($product_rule['type'] == 'suppliers') {
                            $product_rule['values'][$key] = Supplier::getNameById((int)$item);
                        } elseif ($product_rule['type'] == 'attributes') {
                            $attribute = new Attribute((int)$item);
                            $attribute_group = new AttributeGroup((int)$attribute->id_attribute_group);
                            $product_rule['values'][$key] = $attribute_group->name[$this->context->language->id].' : '.$attribute->name[$this->context->language->id];
                        }
                    }
                }
            }
        }

        $this->smarty->assign(array(
            'gifts' => count($gifts) > 0 ? $gifts : null,
            'currency_sign' => $this->context->currency->sign
        ));

        return $this->display(__FILE__, 'column.tpl');
    }

    public function hookLeftColumn($params)
    {
        return $this->hookRightColumn($params);
    }

    public function hookDisplayNav($params)
    {
        $id_customer = (int)$this->context->customer->id;
        if (!($wallet = PrepaymentWallets::getWalletInstance($id_customer))) {
            return;
        }

        $from_currency = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));
        $to_currency = Context::getContext()->currency;
        $balance = Tools::displayPrice(Tools::convertPriceFull($wallet->total_amount, $from_currency, $to_currency), Context::getContext()->currency);

        $this->smarty->assign(array(
            'is_logged' => $this->context->customer->isLogged(),
            'balance' => $balance
        ));

        return $this->display(__FILE__, 'nav.tpl');
    }

    public function hookDisplayNav2($params)
    {
        return $this->hookDisplayNav($params);
    }

    public function hookActionAdminControllerSetMedia()
    {
        $controller = Tools::getValue('controller');

        if (in_array($controller, array('AdminCustomers' , 'AdminPrepaymentDashboard'))) {
            $this->context->controller->addCSS($this->_path.'views/css/admin/layout.css', 'all');
        } elseif ($controller == 'AdminOrders') {
            $id_order = null;
            $path = null;

            if (version_compare(_PS_VERSION_, '1.7.7', '>=')) {
                if (($request = $this->getRequest())) {
                    $id_order = (int)$request->attributes->get('orderId');
                    $path = $this->_path . 'views/js/admin/bo_order_migrated.js';
                }
            } else {
                $id_order = Tools::getValue('id_order');
                $path = $this->_path . 'views/js/admin/bo_order.js';
            }

            if ($id_order && $path) {
                $deposit = false;
                $wallet_movement = false;
                $operations = PrepaymentLastActivities::getOperationsByIdOrder($id_order);
                foreach ($operations as $operation) {
                    if ($operation == PrepaymentLastActivities::DEPOSIT && Configuration::get('WALLET_ALLOW_DISBURSEMENT')) {
                        $deposit = true;
                        $wallet_movement = true;
                        break;
                    } elseif ($operation == PrepaymentLastActivities::ORDER) {
                        $wallet_movement = true;
                        break;
                    }
                }

                if ($wallet_movement || (!$deposit && Configuration::get('WALLET_AUTO_REFUND'))) {
                    Media::addJsDefL('wallet_refund', $deposit ? $this->l('Disburse to wallet') : $this->l('Refund to wallet'));
                    Media::addJsDefL('wallet_refund_label', $this->l('Label'));

                    $this->context->controller->addJS($path);
                }
            }
        }
    }

    public function hookHeader($params)
    {
        $this->context->controller->addCSS($this->_path.'views/css/front/layout.css', 'all');
        $this->context->controller->addjqueryPlugin('fancybox');

        if (method_exists($this->context->controller, 'getProduct')
            && ($product = $this->context->controller->getProduct())
        ) {
            if ($this->depositExists(array((int)$product->id))) {
                $hash = null;

                $combination = new Combination((int)Tools::getValue('id_product_attribute'));
                if (Validate::isLoadedObject($combination)) {
                    $attributes_name = $combination->getAttributesName($this->context->language->id);
                    foreach ($attributes_name as $attribute_name) {
                        $attribute = new Attribute($attribute_name['id_attribute']);
                        if (!Validate::isLoadedObject($attribute)
                            || $attribute->id_attribute_group != Configuration::get('WALLET_ATTRGROUP_AMOUNT')
                        ) {
                            continue;
                        }

                        $attribute_group = new AttributeGroup($attribute->id_attribute_group, $this->context->language->id);
                        if (!Validate::isLoadedObject($attribute_group)) {
                            continue;
                        }

                        $rewrite_group_name = str_replace(
                            Configuration::get('PS_ATTRIBUTE_ANCHOR_SEPARATOR'),
                            '_',
                            Tools::link_rewrite(
                                str_replace(
                                    array(',', '.'),
                                    '-',
                                    $attribute_group->public_name
                                )
                            )
                        );

                        $hash = '#/'.$rewrite_group_name.'-'.$attribute_name['name'];
                    }
                }

                $redirect_url = $this->context->link->getModuleLink($this->name, 'deposits');

                if ($hash) {
                    $redirect_url .= $hash;
                }

                Tools::redirect($redirect_url);
            }
        }

        $callable_controllers = array('cart', 'order-opc', 'order');
        if (isset($this->context->controller->php_self)
            && in_array($this->context->controller->php_self, $callable_controllers)
            && ($cart = $this->context->cart)
            && ($partial = PrepaymentPartials::getPartialInstance((int)$cart->id))
            && !$cart->getDiscountsCustomer((int)$partial->id_cart_rule)) {
            $partial->delete();
        }

        return;
    }

    public function hookDisplayBackOfficeTop($params)
    {
        $redirect = false;
        $controller = Tools::getValue('controller');

        if ($controller == 'AdminProducts') {
            if (Tools::getValue('ajax') && Tools::getValue('action') === 'updatePositions') {
                return;
            }

            $id_product = ($request = $this->getRequest())
                ? (int)$request->attributes->get('id')
                : Tools::getValue('id_product');

            if ($this->depositExists(array($id_product))) {
                $redirect = true;
            }
        } elseif ($controller == 'AdminAttributesGroups') {
            if (Tools::getValue('id_attribute_group') === Configuration::get('WALLET_ATTRGROUP_AMOUNT')) {
                $redirect = true;
            }
        }

        if ($redirect) {
            Tools::redirectAdmin($this->getAdminLink('AdminModules', array('configure' => $this->name, 'tab_module' => $this->tab, 'module_name' => $this->name)));
        }

        return;
    }

    public function hookActionOrderSlipAdd($params)
    {
        if (!Tools::isSubmit('refund_to_wallet')
            || !($order = new Order($params['order']->id))
            || !Validate::isLoadedObject($order)
            || !($cart = new Cart($order->id_cart))
            || !Validate::isLoadedObject($cart)
            || !($wallet = PrepaymentWallets::getWalletInstance((int)$order->id_customer))
        ) {
            return false;
        }

        $deposit = false;
        $wallet_movement = false;
        $operations = PrepaymentLastActivities::getOperationsByIdOrder($order->id);
        foreach ($operations as $operation) {
            if ($operation == PrepaymentLastActivities::DEPOSIT) {
                $deposit = true;
                $wallet_movement = true;
                break;
            } elseif ($operation == PrepaymentLastActivities::ORDER) {
                $wallet_movement = true;
                break;
            }
        }

        if (!$wallet_movement && !Configuration::get('WALLET_AUTO_REFUND')) {
            return false;
        }

        $order_slips = OrderSlip::getOrdersSlip((int)$order->id_customer, (int)$order->id);
        foreach ($order_slips as $order_slip) {
            $match = false;
            if (PrepaymentLastActivities::refundExists((int)$order_slip['id_order_slip'])) {
                $match = true;
            }
            if ($order_slip['partial'] == 1) {
                $cart_rules = CartRule::getCustomerCartRules((int)$this->context->language->id, (int)$order->id_customer, true, false);
                foreach ($cart_rules as $cart_rule) {
                    if ($cart_rule['code'] == sprintf('V%1$dC%2$dO%3$d', $cart_rule['id_cart_rule'], $order->id_customer, $order->id)) {
                        $match = true;
                    }
                }
            }
            if (!$match) {
                $this->_makeRefund($order, (int)$order_slip['id_order_slip'], (int)$wallet->id, $deposit);
            }
        }

        return true;
    }

    public function getOperationDisplayName($id_operation)
    {
        switch ($id_operation) {
            case PrepaymentLastActivities::DEPOSIT:
                $operation = $this->l('Deposit');
                break;
            case PrepaymentLastActivities::ORDER:
                $operation = $this->l('Order');
                break;
            case PrepaymentLastActivities::REFUND:
                $operation = $this->l('Refund');
                break;
            case PrepaymentLastActivities::DISBURSEMENT:
                $operation = $this->l('Disbursement');
                break;
            case PrepaymentLastActivities::GIFT:
                $operation = $this->l('Gift');
                break;
            case PrepaymentLastActivities::CUSTOM_DEPOSIT:
                $operation = $this->l('Manual deposit');
                break;
            case PrepaymentLastActivities::CUSTOM_DISBURSEMENT:
                $operation = $this->l('Manual disbursement');
                break;
            default:
                $operation = null;
                break;
        }

        return $operation;
    }

    public function hookDisplayAdminCustomers($params)
    {
        $id_customer = (int)$params['id_customer'];
        if (!($wallet = PrepaymentWallets::getWalletInstance($id_customer))) {
            return;
        }

        $from_currency = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));
        $to_currency = Context::getContext()->currency;

        $balance = $wallet ? Tools::displayPrice(Tools::convertPriceFull($wallet->total_amount, $from_currency, $to_currency), $to_currency) : false;
        $is_active = $wallet ? $wallet->active : false;

        $last_activities = PrepaymentLastActivities::getLastActivities($wallet->id);
        foreach ($last_activities as &$last_activity) {
            $last_activity['operation'] = $this->getOperationDisplayName($last_activity['id_operation']);

            $total_amount = Tools::convertPriceFull($last_activity['amount'], Currency::getCurrencyInstance($last_activity['id_currency']), $to_currency);
            $last_activity['display_amount'] = Tools::displayPrice($total_amount, $to_currency);
            $last_activity['is_partial'] = PrepaymentLastActivities::isPartial($last_activity['id_prepayment_last_activities']) ? true : false;

            $object = new PrepaymentLastActivities((int)$last_activity['id_prepayment_last_activities']);
            if ($object->id_operation == PrepaymentLastActivities::CUSTOM_DEPOSIT || $object->id_operation == PrepaymentLastActivities::CUSTOM_DISBURSEMENT) {
                $last_activity['href'] = $this->getAdminLink('AdminPrepaymentLastActivities', array('id_prepayment_last_activities' => (int)$object->id, 'updateprepayment_last_activities' => 1));
            } elseif ($object->id_operation == PrepaymentLastActivities::ORDER && PrepaymentLastActivities::isPartial($object->id)) {
                $id_partial = $object->getPartialId();
                $partial = new PrepaymentPartials((int)$id_partial);
                $last_activity['href'] = $this->getAdminLink('AdminCarts', array('id_cart' => (int)$partial->id_cart, 'viewcart' => 1));
            } else {
                $last_activity['href'] = $this->getAdminLink('AdminOrders', array('id_order' => (int)$object->getOrder(), 'vieworder' => 1));
            }
        }

        $this->context->smarty->assign(array(
            'wallet' => $wallet,
            'balance' => $balance,
            'history_list' => $last_activities
        ));

        return $this->display(__FILE__, 'admin_customers.tpl');
    }

    public function hookActionCustomerAccountAdd($params)
    {
        if (!Configuration::get('WALLET_AUTO_OPEN')) {
            return false;
        }
        if ($wallet = PrepaymentWallets::walletExists((int)$params['newCustomer']->id)) {
            return false;
        }

        $wallet = new PrepaymentWallets();
        $wallet->id_customer = (int)$params['newCustomer']->id;
        $wallet->total_amount = 0;
        $wallet->active = 1;
        $wallet->add();

        return true;
    }

    public function hookDisplayCustomerAccount($params)
    {
        return $this->display(__FILE__, 'customer_account.tpl');
    }

    public function hookPaymentReturn($params)
    {
        $order = version_compare(_PS_VERSION_, '1.7', '<') ? $params['objOrder'] : $params['order'];

        if (!$this->active || !(PrepaymentWallets::walletExists((int)$order->id_customer))) {
            return;
        }

        $state = $order->getCurrentState();
        if ($state == Configuration::get('PS_OS_PAYMENT') || $state == Configuration::get('PS_OS_OUTOFSTOCK')) {
            $this->smarty->assign(array(
                'total_to_pay' => Tools::displayPrice($order->getOrdersTotalPaid(), new Currency($order->id_currency), false),
                'shop_name' => $this->context->shop->name,
                'status' => 'ok',
                'id_order' => (int)$order->id
            ));
            if (isset($order->reference) && !empty($order->reference)) {
                $this->smarty->assign('reference', $order->reference);
            }
        } else {
            $this->smarty->assign('status', 'failed');
        }
        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function hookPaymentOptions($params)
    {
        $cart = $params['cart'];
        $wallet = PrepaymentWallets::getWalletInstance((int)$cart->id_customer);
        $products = $cart->getProducts();
        $product_ids = array();
        foreach ($products as $product) {
            $product_ids[] = $product['id_product'];
        }

        if (!$this->active
            || !$this->checkCurrency($cart)
            || $this->depositExists($product_ids)
            || !($wallet && $wallet->active)
            || PrepaymentPartials::partialExists((int)$cart->id)) {
            return;
        }

        $from_currency = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));
        $to_currency = Currency::getCurrencyInstance((int)$cart->id_currency);

        $order = $cart->getOrderTotal(true, Cart::BOTH);
        $balance = Tools::convertPriceFull($wallet->total_amount, $from_currency, $to_currency);
        $balance_net = Tools::convertPriceFull($wallet->total_amount + Configuration::get('WALLET_NEGATIVE_BALANCE_MAX'), $from_currency, $to_currency);
        if ($balance_net <= 0) {
            return;
        }

        $to_be_paid = 0;
        $is_partial = false;

        if (Configuration::get('WALLET_ALLOW_PARTIAL_PAYMENT') && ($order > $balance_net) && ($balance_net > 0)) {
            $total_products = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
            $debited_amount = $total_products < $balance_net ? $total_products : $balance_net;
            $to_be_paid = $order - $debited_amount;
            $is_partial = true;

            $order = $debited_amount;
        }

        $current_balance = Tools::displayPrice($balance);
        $total_order = Tools::displayPrice($order);
        $total_balance = Tools::displayPrice($balance - $order);
        $total_to_be_paid = Tools::displayPrice($to_be_paid);

        $this->smarty->assign(array(
            'current_balance' => $current_balance,
            'total_order' => $total_order,
            'total_balance' => $total_balance,
            'total_to_be_paid' => $total_to_be_paid,
            'is_partial' => $is_partial
        ));

        $newOption = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $newOption->setCallToActionText($this->l('Pay with my wallet'))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true))
            ->setAdditionalInformation($this->fetch('module:prepayment/views/templates/front/payment_infos.tpl'));

        return array($newOption);
    }

    public function hookPayment($params)
    {
        $cart = $params['cart'];
        $wallet = PrepaymentWallets::getWalletInstance((int)$cart->id_customer);
        $products = $cart->getProducts();
        $product_ids = array();
        foreach ($products as $product) {
            $product_ids[] = $product['id_product'];
        }

        if (!$this->active
            || !$this->checkCurrency($cart)
            || $this->depositExists($product_ids)
            || !($wallet && $wallet->active)
            || PrepaymentPartials::partialExists((int)$cart->id)) {
            return;
        }

        $this->smarty->assign(array(
            'this_path' => $this->_path,
            'this_path_wallet' => $this->_path,
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
        ));
        return $this->display(__FILE__, 'payment.tpl');
    }

    public function hookActionValidateOrder($params)
    {
        if (!$this->active || !($order = $params['order'])) {
            return false;
        }

        if (!($wallet = PrepaymentWallets::getWalletInstance((int)$order->id_customer))) {
            $wallet = new PrepaymentWallets();
            $wallet->id_customer = (int)$order->id_customer;
            $wallet->active = 1;
            $wallet->add();
        }

        if (($gift = PrepaymentGifts::getGift($order))) {
            $last_activity = new PrepaymentLastActivities();
            $last_activity->id_wallet = (int)$wallet->id;
            $last_activity->id_currency = (int)$order->id_currency;
            $last_activity->amount = $gift->getContextualValue(true, $order);
            $last_activity->id_operation = PrepaymentLastActivities::GIFT;
            $last_activity->paid = $order->module == 'prepayment' ? 1 : 0;
            $last_activity->add();
            $last_activity->addOrder($order->id);
            $last_activity->addGift($gift->id);

            $message = $this->l('Cash back to wallet')." : ".Tools::displayPrice($last_activity->amount, Currency::getCurrencyInstance((int)$last_activity->id_currency))."\r";
            $this->_addNewPrivateMessage($order, $message);

            $gift = new PrepaymentGifts((int)$gift->id);
            $gift->quantity = $gift->quantity - 1;
            $gift->update();
        }

        if (($partial = PrepaymentPartials::getPartialInstance((int)$order->id_cart))) {
            $cart_rule = new CartRule((int)$partial->id_cart_rule);
            if (Validate::isLoadedObject($cart_rule)) {
                $cart_rule->delete();
            }

            $last_activity = new PrepaymentLastActivities((int)$partial->id_prepayment_last_activities);
            if (Validate::isLoadedObject($last_activity)) {
                $last_activity->update();
                $last_activity->addOrder($order->id);
            }

            return true;
        }

        if (($deposit = $this->getDeposit($order))) {
            $last_activity = new PrepaymentLastActivities();
            $last_activity->id_wallet = (int)$wallet->id;
            $last_activity->id_currency = (int)$order->id_currency;
            $last_activity->amount = $deposit;
            $last_activity->id_operation = PrepaymentLastActivities::DEPOSIT;
            $last_activity->paid = 0;
            $last_activity->add();
            $last_activity->addOrder($order->id);

            $message = $this->l('Deposit to wallet')." : ".Tools::displayPrice($last_activity->amount, Currency::getCurrencyInstance((int)$last_activity->id_currency))."\r";
            $this->_addNewPrivateMessage($order, $message);
        }

        return true;
    }

    public function hookActionPaymentCCAdd($params)
    {
        if (!isset($params['paymentCC'])
            || empty($params['paymentCC'])) {
            return false;
        }

        $id_order = false;
        $reference = ltrim($params['paymentCC']->order_reference, '#');
        $orders = Order::getByReference($reference);
        if ($orders) {
            foreach ($orders as $order) {
                $id_order = $order->id;
                break;
            }
        }

        if (!$id_order
            || !($order = new Order((int)$id_order))
            || !Validate::isLoadedObject($order)
            || !($wallet = PrepaymentWallets::getWalletInstance($order->id_customer))
            || $params['paymentCC']->payment_method != $this->displayName
            || (($partial = PrepaymentPartials::getPartialInstance((int)$order->id_cart)))
        ) {
            return false;
        }

        $amount = $params['paymentCC']->amount;
        if ($amount < 0) {
            $operation = PrepaymentLastActivities::REFUND;
            $amount = abs($amount);
        } else {
            $operation = PrepaymentLastActivities::ORDER;
        }

        $last_activity = new PrepaymentLastActivities();
        $last_activity->id_wallet = (int)$wallet->id;
        $last_activity->id_currency = $params['paymentCC']->id_currency;
        $last_activity->amount = $amount;
        $last_activity->id_operation = $operation;
        $last_activity->paid = 1;
        $last_activity->add();
        $last_activity->addOrder($order->id);

        return true;
    }

    public function hookActionOrderStatusUpdate($params)
    {
        if (!($order = new Order($params['id_order']))
            || !Validate::isLoadedObject($order)
            || !($params['newOrderStatus'] instanceof OrderState)
            || $params['newOrderStatus']->paid != 1
            || !PrepaymentWallets::walletExists((int)$order->id_customer)
        ) {
            return false;
        }

        $ids_last_activity = PrepaymentLastActivities::getIdsByIdOrder((int)$order->id);
        foreach ($ids_last_activity as $id_last_activity) {
            $last_activity = new PrepaymentLastActivities((int)$id_last_activity['id_prepayment_last_activities']);
            if (!Validate::isLoadedObject($last_activity)
                || $last_activity->paid
            ) {
                continue;
            }

            $last_activity->paid = 1;
            $last_activity->update();
        }

        return true;
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        if (!$this->active
            || !($order = new Order($params['id_order']))
            || !Validate::isLoadedObject($order)
            || $order->module == 'paypal'
            || !PrepaymentWallets::walletExists((int)$order->id_customer)) {
            return false;
        }

        $order_status = $params['newOrderStatus'];
        if ($order_status->paid == 1
            && ($partial = PrepaymentPartials::getPartialInstance((int)$order->id_cart))
            && ($last_activity = new PrepaymentLastActivities((int)$partial->id_prepayment_last_activities))
            && Validate::isLoadedObject($last_activity)
        ) {
            $id_order_cart_rule = false;
            $order_cart_rules = $order->getCartRules();
            foreach ($order_cart_rules as $order_cart_rule) {
                if ($order_cart_rule['id_cart_rule'] == $partial->id_cart_rule) {
                    $id_order_cart_rule = $order_cart_rule['id_order_cart_rule'];
                    break;
                }
            }

            if (!$id_order_cart_rule
                || !($order_cart_rule = new OrderCartRule($id_order_cart_rule))
                || !Validate::isLoadedObject($order_cart_rule)
            ) {
                return false;
            }

            $order_invoice = null;
            if ($order->hasInvoice()
                && ($invoice = OrderInvoice::getInvoiceByNumber($order->invoice_number))
                && Validate::isLoadedObject($invoice)) {
                $order_invoice = $invoice;
            }

            if ($order->addOrderPayment($last_activity->amount, $this->displayName, null, Currency::getCurrencyInstance((int)$last_activity->id_currency), null, $order_invoice)) {
                //Update amounts of order
                $order->total_discounts = max(0, Tools::ps_round($order->total_discounts, 2) - $order_cart_rule->value);
                $order->total_discounts_tax_incl = max(0, Tools::ps_round($order->total_discounts_tax_incl, 2) - $order_cart_rule->value);
                $order->total_discounts_tax_excl = max(0, Tools::ps_round($order->total_discounts_tax_excl, 2) - $order_cart_rule->value_tax_excl);

                $order->total_paid = $order->total_paid + $order_cart_rule->value;
                $order->total_paid_tax_incl = $order->total_paid_tax_incl + $order_cart_rule->value;
                $order->total_paid_tax_excl = $order->total_paid_tax_excl + $order_cart_rule->value_tax_excl;
                $order->update();

                PrepaymentPartials::deleteDiscountOnInvoice($order_invoice, $order_cart_rule->value, $order_cart_rule->value_tax_excl);

                // Delete Order Cart Rule & partial payment
                $order_cart_rule->delete();
            }
        }

        return true;
    }

    public function hookDisplayPaymentTop()
    {
        $cart = $this->context->cart;

        if (($partial = PrepaymentPartials::getPartialInstance((int)$cart->id))
            && $cart->getDiscountsCustomer((int)$partial->id_cart_rule)) {
            $last_activity = new PrepaymentLastActivities((int)$partial->id_prepayment_last_activities);
            if (Validate::isLoadedObject($last_activity)) {
                $total_paid = Tools::displayPrice((float)$last_activity->amount, Currency::getCurrencyInstance((int)$last_activity->id_currency), false);
                $this->smarty->assign(array(
                    'total_paid' => $total_paid
                ));
                return $this->display(__FILE__, 'payment-top.tpl');
            }
        }

        return;
    }

    public function hookCart($params)
    {
        if (!($cart = $this->context->cart)) {
            return false;
        }

        if (!Configuration::get('WALLET_ALLOW_CART_STACK')) {
            $products = $cart->getProducts();
            $product_ids = array();
            foreach ($products as $product) {
                $product_ids[] = $product['id_product'];
            }

            if ($this->depositExists($product_ids)) {
                $_products = array();
                $last_product = $cart->getLastProduct();
                $deposit_ids = $this->getProducts(Shop::getContextListShopID(), true);

                if (in_array((int)$last_product['id_product'], $deposit_ids)) {
                    foreach ($products as $product) {
                        if (in_array((int)$product['id_product'], $deposit_ids)) {
                            continue;
                        }

                        $_products[] = array(
                            'id_product' => $product['id_product'],
                            'id_product_attribute' => $product['id_product_attribute'] ? $product['id_product_attribute'] : 0,
                            'id_customization' => $product['id_customization'] ? $product['id_customization'] : 0,
                            'id_address_delivery' => $product['id_address_delivery'] ? $product['id_address_delivery'] : 0
                        );
                    }
                } else {
                    foreach ($products as $product) {
                        if ($last_product['id_product'] === $product['id_product']) {
                            continue;
                        }

                        $_products[] = array(
                            'id_product' => $product['id_product'],
                            'id_product_attribute' => $product['id_product_attribute'] ? $product['id_product_attribute'] : 0,
                            'id_customization' => $product['id_customization'] ? $product['id_customization'] : 0,
                            'id_address_delivery' => $product['id_address_delivery'] ? $product['id_address_delivery'] : 0
                        );
                    }
                }

                if (count($_products)) {
                    foreach ($cart->getCartRules() as $cart_rule) {
                        $cart->removeCartRule($cart_rule['obj']->id);
                    }

                    foreach ($_products as $_product) {
                        $cart->deleteProduct($_product['id_product'], $_product['id_product_attribute'], $_product['id_customization'], $_product['id_address_delivery']);
                    }
                }
            }
        }

        if (($partial = PrepaymentPartials::getPartialInstance((int)$cart->id))
            && $cart->getDiscountsCustomer((int)$partial->id_cart_rule)) {
            $last_activity = new PrepaymentLastActivities((int)$partial->id_prepayment_last_activities);
            if (!Validate::isLoadedObject($last_activity)) {
                return false;
            }

            $cart_rule = new CartRule((int)$partial->id_cart_rule);
            if (!Validate::isLoadedObject($cart_rule)) {
                return false;
            }

            $cart_rule_contextual = $this->getCartRuleContextualValue($cart_rule);
            if ($cart_rule_contextual != $cart_rule->reduction_amount) {
                $partial->delete();
            } else {
                $last_activity->amount = $cart_rule_contextual;
                $last_activity->id_currency = $cart->id_currency;
                $last_activity->update();
            }
        }

        return true;
    }

    public function getRequest()
    {
        $request = null;

        global $kernel;
        if ($kernel instanceof Symfony\Component\HttpKernel\HttpKernelInterface) {
            $request = $kernel->getContainer()->get('request_stack')->getCurrentRequest();
        }

        return $request;
    }

    public function getCartRuleContextualValue($cart_rule)
    {
        $use_tax = true;
        $context = Context::getContext();
        $filter = CartRule::FILTER_ACTION_ALL;
        $all_products = $context->cart->getProducts();

        $cache_id = 'getContextualValue_'.(int)$cart_rule->id.'_'.(int)$use_tax.'_'.(int)$context->cart->id.'_'.(int)$filter;
        foreach ($all_products as $product) {
            $cache_id .= '_'.(int)$product['id_product'].'_'.(int)$product['id_product_attribute'].(isset($product['in_stock']) ? '_'.(int)$product['in_stock'] : '');
        }

        if (Cache::isStored($cache_id)) {
            Cache::clean($cache_id);
        }

        return $cart_rule->getContextualValue($use_tax, $context, $filter);
    }

    public function depositExists($product_ids = array())
    {
        $result = false;

        $deposit_ids = $this->getProducts(Shop::getContextListShopID(), true);
        foreach ($product_ids as $product_id) {
            if (in_array($product_id, $deposit_ids)) {
                $result = true;
                break;
            }
        }

        return $result;
    }

    public function getDeposit($order)
    {
        if (!Validate::isLoadedObject($order)) {
            return false;
        }

        $deposit = 0;
        $products = $order->getProductsDetail();
        $deposit_ids = $this->getProducts(Shop::getContextListShopID(), true);

        foreach ($products as $product) {
            if (!in_array($product['id_product'], $deposit_ids)) {
                continue;
            }

            $deposit += $product['product_price'] * $product['product_quantity'];
        }

        return $deposit;
    }

    public function processNotification($id_customer, $id_operation, $old_wallet_balance, $new_wallet_balance, $id_currency)
    {
        if ((($id_operation == PrepaymentLastActivities::DEPOSIT || $id_operation == PrepaymentLastActivities::CUSTOM_DEPOSIT) && !Configuration::get('WALLET_NOTIFICATION_DEPOSIT'))
            || (($id_operation == PrepaymentLastActivities::DISBURSEMENT || $id_operation == PrepaymentLastActivities::CUSTOM_DISBURSEMENT) && !Configuration::get('WALLET_NOTIFICATION_DISBURSEMENT'))
            || ($id_operation == PrepaymentLastActivities::ORDER && !Configuration::get('WALLET_NOTIFICATION_ORDER'))
            || ($id_operation == PrepaymentLastActivities::REFUND && !Configuration::get('WALLET_NOTIFICATION_REFUND'))
            || ($id_operation == PrepaymentLastActivities::GIFT && !Configuration::get('WALLET_NOTIFICATION_GIFT'))) {
            return false;
        }

        $customer = new Customer((int)$id_customer);
        if (!Validate::isLoadedObject($customer)) {
            return false;
        }

        if (!Validate::isFloat($old_wallet_balance)
            || !Validate::isFloat($new_wallet_balance)) {
            return false;
        }

        $from_currency = Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT'));
        $to_currency = Currency::getCurrencyInstance((int)$id_currency);

        $old_wallet_balance = Tools::convertPriceFull($old_wallet_balance, $from_currency, $to_currency);
        $new_wallet_balance = Tools::convertPriceFull($new_wallet_balance, $from_currency, $to_currency);
        $round  = _PS_PRICE_COMPUTE_PRECISION_;
        $amount = Tools::ps_round($new_wallet_balance - $old_wallet_balance, $round);

        if ($amount > 0) {
            $movement_operation = $this->l('credited', false, $customer->id_lang);
        } elseif ($amount < 0) {
            $movement_operation = $this->l('debited', false, $customer->id_lang);
        } else {
            return false;
        }

        if ($id_operation == PrepaymentLastActivities::DEPOSIT) {
            $movement_type = $this->l('Deposit', false, $customer->id_lang);
        } elseif ($id_operation == PrepaymentLastActivities::CUSTOM_DEPOSIT) {
            $movement_type = $this->l('Manual deposit', false, $customer->id_lang);
        } elseif ($id_operation == PrepaymentLastActivities::DISBURSEMENT) {
            $movement_type = $this->l('Disbursement', false, $customer->id_lang);
        } elseif ($id_operation == PrepaymentLastActivities::CUSTOM_DISBURSEMENT) {
            $movement_type = $this->l('Manual disbursement', false, $customer->id_lang);
        } elseif ($id_operation == PrepaymentLastActivities::ORDER) {
            $movement_type = $this->l('Order', false, $customer->id_lang);
        } elseif ($id_operation == PrepaymentLastActivities::REFUND) {
            $movement_type = $this->l('Refund', false, $customer->id_lang);
        } elseif ($id_operation == PrepaymentLastActivities::GIFT) {
            $movement_type = $this->l('Gift', false, $customer->id_lang);
        } else {
            return false;
        }

        $template_vars = array(
            '{customer}' => $customer->firstname.' '.$customer->lastname,
            '{movement_amount}' => Tools::displayPrice(abs($amount), $to_currency),
            '{movement_operation}' => $movement_operation,
            '{movement_type}' => $movement_type,
            '{wallet_balance_old}' => Tools::displayPrice($old_wallet_balance, $to_currency),
            '{wallet_balance_new}' => Tools::displayPrice($new_wallet_balance, $to_currency)
        );

        if (!Mail::Send(
            $customer->id_lang,
            'wallet_notification',
            $this->l('Wallet notification', false, $customer->id_lang),
            $template_vars,
            $customer->email,
            $customer->firstname.' '.$customer->lastname,
            null,
            null,
            null,
            null,
            dirname(__FILE__).'/mails/'
        )) {
            return false;
        }

        return true;
    }

    public function l($string, $specific = false, $id_lang = 0)
    {
        if (!$id_lang || $id_lang == Context::getContext()->language->id) {
            return parent::l($string, $specific);
        }

        global $_MODULE;

        $lang_cache = array();
        $translations_merged = array();

        $language = new Language((int)$id_lang);
        if (!Validate::isLoadedObject($language)) {
            $language = Context::getContext()->language;
        }

        if (!isset($translations_merged[$this->name]) && isset($language)) {
            $files_by_priority = array(
                // Translations in theme
                _PS_THEME_DIR_.'modules/'.$this->name.'/translations/'.$language->iso_code.'.php',
                _PS_THEME_DIR_.'modules/'.$this->name.'/'.$language->iso_code.'.php',
                // PrestaShop 1.5 translations
                _PS_MODULE_DIR_.$this->name.'/translations/'.$language->iso_code.'.php',
                // PrestaShop 1.4 translations
                _PS_MODULE_DIR_.$this->name.'/'.$language->iso_code.'.php'
            );
            foreach ($files_by_priority as $file) {
                if (file_exists($file)) {
                    include_once($file);
                    $translations_merged[$this->name] = true;
                }
            }
        }
        $string = preg_replace("/\\\*'/", "\'", $string);
        $key = md5($string);
        $cache_key = $this->name.'|'.$string.'|'.(int)$language->id;

        if (!isset($lang_cache[$cache_key])) {
            $current_key = Tools::strtolower('<{'.$this->name.'}'._THEME_NAME_.'>'.$this->name).'_'.$key;
            $default_key = Tools::strtolower('<{'.$this->name.'}prestashop>'.$this->name).'_'.$key;
            if (!empty($_MODULE[$current_key])) {
                $ret = Tools::stripslashes($_MODULE[$current_key]);
            } elseif (!empty($_MODULE[$default_key])) {
                $ret = Tools::stripslashes($_MODULE[$default_key]);
            }

            $ret = htmlspecialchars($ret, ENT_COMPAT, 'UTF-8');
            $lang_cache[$cache_key] = $ret;
        }

        return $lang_cache[$cache_key];
    }

    public function hookActionAdminProductsListingFieldsModifier($params)
    {
        if (($deposit_ids = $this->getProducts(Shop::getContextListShopID(), true))
            && count($deposit_ids)
        ) {
            $sql_where = array('AND', 'p.id_product NOT IN ('.implode(',', array_map('intval', $deposit_ids)).')');
            $params['sql_where'] = array_merge($params['sql_where'], $sql_where);
        }
    }

    public function hookActionAdminAttributesGroupsListingFieldsModifier($params)
    {
        // $amount_attribute_id = (int)Configuration::get('WALLET_ATTRGROUP_AMOUNT');
        // $params['where'] = $params['where'] . ' AND a.id_attribute_group != '.(int)$amount_attribute_id;
    }

    public function hookActionObjectDeleteBefore($params)
    {
        $object = $params['object'];

        if (get_class($object) == 'Product') {
            if ($this->depositExists(array((int)$object->id)) && !$this->force_delete) {
                if ($this->getRequest()) {
                    throw new PrestaShopBundle\Exception\UpdateProductException($this->l('Deposit product cannot be deleted.'));
                }

                throw new PrestaShopException($this->l('Deposit product cannot be deleted.'));
            }
        } elseif (get_class($object) == 'AttributeGroup') {
            if ($object->id == Configuration::get('WALLET_ATTRGROUP_AMOUNT') && !$this->force_delete) {
                throw new PrestaShopException($this->l('Deposit attribute group cannot be deleted.'));
            }
        }

        return true;
    }

    public function hookActionObjectDeleteAfter($params)
    {
        $object = $params['object'];

        if (get_class($object) == 'CartRule'
            && ($partial = PrepaymentPartials::getPartialInstanceByIdCartRule($object->id))
            && !Order::getOrderByCartId($partial->id_cart)
        ) {
            $partial->delete();
        } elseif (get_class($object) == 'Cart'
            && ($partial = PrepaymentPartials::getPartialInstance($object->id))
        ) {
            $partial->delete();
        } elseif (get_class($object) == 'OrderCartRule'
            && ($partial = PrepaymentPartials::getPartialInstanceByIdCartRule($object->id_cart_rule))
        ) {
            $clean = Tools::isSubmit('submitDeleteVoucher') ? true : false;
            if ($clean) {
                $last_activity = new PrepaymentLastActivities($partial->id_prepayment_last_activities);
                $last_activity->deleteOrder();
            }

            $partial->delete($clean);
        } elseif (get_class($object) == 'Attribute') {
            PrepaymentGifts::cleanProductRuleIntegrity('attributes', $object->id);
        } elseif (get_class($object) == 'Category') {
            PrepaymentGifts::cleanProductRuleIntegrity('categories', $object->id);
        } elseif (get_class($object) == 'Manufacturer') {
            PrepaymentGifts::cleanProductRuleIntegrity('manufacturers', $object->id);
        } elseif (get_class($object) == 'Product') {
            PrepaymentGifts::cleanProductRuleIntegrity('products', $object->id);
        } elseif (get_class($object) == 'Supplier') {
            PrepaymentGifts::cleanProductRuleIntegrity('suppliers', $object->id);
        }

        return true;
    }

    public function hookAddWebserviceResources($params)
    {
        return [
           'prepayment_wallets' => [
               'description' => 'Prepayment Wallets',
               'class' => 'PrepaymentWallets'
           ],
           'prepayment_gifts' => [
               'description' => 'Prepayment Gifts',
               'class' => 'PrepaymentGifts'
           ],
           'prepayment_last_activities' => [
               'description' => 'Prepayment Last Activities',
               'class' => 'PrepaymentLastActivities'
           ],
       ];
    }

    public function hookActionObjectPaypalOrderAddAfter($params)
    {
        $paypalOrder = $params['object'];

        if (!Validate::isLoadedObject($paypalOrder)
            || !($order = new Order($paypalOrder->id_order))
            || !Validate::isLoadedObject($order)
            || !$order->hasBeenPaid()
            || !PrepaymentWallets::walletExists((int)$order->id_customer)
            || !($partial = PrepaymentPartials::getPartialInstance((int)$order->id_cart))
            || !($last_activity = new PrepaymentLastActivities((int)$partial->id_prepayment_last_activities))
            || !Validate::isLoadedObject($last_activity)
        ) {
            return false;
        }

        $id_order_cart_rule = false;
        $order_cart_rules = $order->getCartRules();
        foreach ($order_cart_rules as $order_cart_rule) {
            if ($order_cart_rule['id_cart_rule'] == $partial->id_cart_rule) {
                $id_order_cart_rule = $order_cart_rule['id_order_cart_rule'];
                break;
            }
        }

        if (!$id_order_cart_rule
            || !($order_cart_rule = new OrderCartRule($id_order_cart_rule))
            || !Validate::isLoadedObject($order_cart_rule)
        ) {
            return false;
        }

        $order_invoice = null;
        if ($order->hasInvoice()
            && ($invoice = OrderInvoice::getInvoiceByNumber($order->invoice_number))
            && Validate::isLoadedObject($invoice)) {
            $order_invoice = $invoice;
        }

        if ($order->addOrderPayment($last_activity->amount, $this->displayName, null, Currency::getCurrencyInstance((int)$last_activity->id_currency), null, $order_invoice)) {
            //Update amounts of order
            $order->total_discounts = max(0, Tools::ps_round($order->total_discounts, 2) - $order_cart_rule->value);
            $order->total_discounts_tax_incl = max(0, Tools::ps_round($order->total_discounts_tax_incl, 2) - $order_cart_rule->value);
            $order->total_discounts_tax_excl = max(0, Tools::ps_round($order->total_discounts_tax_excl, 2) - $order_cart_rule->value_tax_excl);

            $order->total_paid = $order->total_paid + $order_cart_rule->value;
            $order->total_paid_tax_incl = $order->total_paid_tax_incl + $order_cart_rule->value;
            $order->total_paid_tax_excl = $order->total_paid_tax_excl + $order_cart_rule->value_tax_excl;
            $order->update();

            PrepaymentPartials::deleteDiscountOnInvoice($order_invoice, $order_cart_rule->value, $order_cart_rule->value_tax_excl);

            // Delete Order Cart Rule & partial payment
            $order_cart_rule->delete();
        }

        return true;
    }

    public static function getCurrenciesNotIndexed()
    {
        $currencies = Currency::getCurrencies(false, true, true);
        $not_idexed = array();

        foreach ($currencies as $currency) {
            $deposit = new Product((int)Configuration::get('WALLET_PRODUCT_'.(int)$currency['id_currency']));
            if (Validate::isLoadedObject($deposit) && ($deposit->isAssociatedToShop() || Shop::getContextShopID(true) === null)) {
                continue;
            }

            $not_idexed[] = $currency['id_currency'];
        }

        return $not_idexed;
    }

    public function getAdminLink($controller, $params = array())
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '>=')) {
            return Context::getContext()->link->getAdminLink($controller, true, array(), $params);
        }

        return Context::getContext()->link->getAdminLink($controller).'&'.http_build_query($params);
    }
}
