<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class PrepaymentGifts extends ObjectModel
{
    /* Filters used when retrieving the cart rules applied to a cart of when calculating the value of a reduction */
    const FILTER_ACTION_ALL = 1;
    const FILTER_ACTION_SHIPPING = 2;
    const FILTER_ACTION_REDUCTION = 3;
    const FILTER_ACTION_GIFT = 4;
    const FILTER_ACTION_ALL_NOCAP = 5;
    const BO_ORDER_CODE_PREFIX = 'BO_ORDER_';

    public $id;
    public $name;
    public $description;
    public $id_customer;
    public $date_from;
    public $date_to;
    public $quantity = 1;
    public $quantity_per_user = 1;
    public $priority = 1;
    public $minimum_amount;
    public $minimum_amount_tax;
    public $minimum_amount_currency;
    public $minimum_amount_shipping;
    public $country_restriction;
    public $carrier_restriction;
    public $group_restriction;
    public $product_restriction;
    public $shop_restriction;
    public $payment_restriction;
    public $reduction_percent;
    public $reduction_amount;
    public $reduction_tax;
    public $reduction_currency;
    public $reduction_product;
    public $active = 1;
    public $date_add;
    public $date_upd;

    protected static $cartAmountCache = [];

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'prepayment_gifts',
        'primary' => 'id_prepayment_gifts',
        'multilang' => true,
        'fields' => array(
            'id_customer' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'date_from' =>                  array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'date_to' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
            'description' =>                array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'size' => 65534),
            'quantity' =>                   array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'quantity_per_user' =>          array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'priority' =>                   array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'minimum_amount' =>             array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'minimum_amount_tax' =>         array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'minimum_amount_currency' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'minimum_amount_shipping' =>    array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'country_restriction' =>        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'carrier_restriction' =>        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'group_restriction' =>          array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'product_restriction' =>        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'shop_restriction' =>           array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'payment_restriction' =>        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'reduction_percent' =>          array('type' => self::TYPE_FLOAT, 'validate' => 'isPercentage'),
            'reduction_amount' =>           array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'reduction_tax' =>              array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'reduction_currency' =>         array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'reduction_product' =>          array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'active' =>                     array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' =>                   array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>                   array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'name' =>                       array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 254),

        ),
    );

    public static function resetStaticCache()
    {
        static::$cartAmountCache = [];
    }

    protected $webserviceParameters = array(
        'objectsNodeName' => 'prepayment_gifts',
        'fields' => array(),
        'associations' => array(
            'countries' => array(
                'getter' => 'getWsCountries',
                'setter' => 'setWsCountries',
                'resource' => 'country',
                'api' => 'gifts',
            ),
            'groups' => array(
                'getter' => 'getWsGroups',
                'setter' => 'setWsGroups',
                'resource' => 'group',
                'api' => 'gifts',
            ),
            'shops' => array(
                'getter' => 'getWsShops',
                'setter' => 'setWsShops',
                'resource' => 'shop',
                'api' => 'gifts',
            ),
            'payments' => array(
                'getter' => 'getWsPayments',
                'setter' => 'setWsPayments',
                'resource' => 'module',
                'api' => 'gifts',
            ),
            'carriers' => array(
                'getter' => 'getWsCarriers',
                'setter' => 'setWsCarriers',
                'resource' => 'carrier',
                'api' => 'gifts',
            ),
            'product_rules' => array(
                'getter' => 'getWsProductRules',
                'setter' => 'setWsProductRules',
                'resource' => 'product_rule',
                'api' => 'gifts',
                'fields' => array(
                    'quantity' => array('required' => true),
                    'type' => array('required' => true),
                    'id' => array('required' => true),
                )
            )
        ),
    );

    public function add($autodate = true, $null_values = false)
    {
        $this->beforeSave();

        if (!parent::add($autodate, $null_values)) {
            return false;
        }

        return true;
    }

    public function update($null_values = false)
    {
        Cache::clean('getContextualValue_' . $this->id . '_*');

        $this->beforeSave();

        if (!parent::update($null_values)) {
            return false;
        }

        return true;
    }

    public function delete()
    {
        if (!parent::delete()) {
            return false;
        }

        $r = Db::getInstance()->delete('prepayment_gifts_carrier', '`id_prepayment_gifts` = ' . (int) $this->id);
        $r &= Db::getInstance()->delete('prepayment_gifts_shop', '`id_prepayment_gifts` = ' . (int) $this->id);
        $r &= Db::getInstance()->delete('prepayment_gifts_group', '`id_prepayment_gifts` = ' . (int) $this->id);
        $r &= Db::getInstance()->delete('prepayment_gifts_country', '`id_prepayment_gifts` = ' . (int) $this->id);
        $r &= Db::getInstance()->delete('prepayment_gifts_payment', '`id_prepayment_gifts` = ' . (int) $this->id);
        $r &= Db::getInstance()->delete('prepayment_gifts_product_rule_group', '`id_prepayment_gifts` = ' . (int) $this->id);
        $r &= Db::getInstance()->delete('prepayment_gifts_product_rule', 'NOT EXISTS (SELECT 1 FROM `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_group`
			WHERE `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule`.`id_product_rule_group` = `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_group`.`id_product_rule_group`)');
        $r &= Db::getInstance()->delete('prepayment_gifts_product_rule_value', 'NOT EXISTS (SELECT 1 FROM `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule`
			WHERE `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_value`.`id_product_rule` = `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule`.`id_product_rule`)');

        return $r;
    }

    public function beforeSave()
    {
        if (strtotime($this->date_from) > strtotime($this->date_to)) {
            throw new PrestaShopException('The cashback rule cannot end before it begins.');
        } elseif (!$this->reduction_amount && !$this->reduction_percent) {
            throw new PrestaShopException('An action is required for this cart rule.');
        } elseif ($this->minimum_amount < 0) {
            throw new PrestaShopException('The minimum cash back amount cannot be lower than zero.');
        } elseif ($this->reduction_percent < 0 || $this->reduction_percent > 100) {
            throw new PrestaShopException('Reduction percentage must be between 0% and 100%');
        } elseif ($this->reduction_amount < 0) {
            throw new PrestaShopException('Reduction amount cannot be lower than zero.');
        }

        if (!$this->minimum_amount_currency) {
            $this->minimum_amount_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
        }

        if (!$this->reduction_currency) {
            $this->reduction_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
        }

        $currency_ids = array();
        $currencies = Currency::getCurrencies();
        foreach ($currencies as $currency) {
            $currency_ids[] = $currency['id_currency'];
        }

        if (!in_array($this->reduction_currency, $currency_ids)) {
            throw new PrestaShopException('The currency does not exist');
        }
    }

    public static function getGifts($limit = null)
    {
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pg.*, pgl.`name`
		FROM `'._DB_PREFIX_.'prepayment_gifts` pg
		LEFT JOIN `'._DB_PREFIX_.'prepayment_gifts_lang` pgl on pg.`id_prepayment_gifts` = pgl.`id_prepayment_gifts`
		WHERE pgl.`id_lang` = '.(int)Context::getContext()->language->id.'
		ORDER BY pg.`priority` ASC, pg.`date_to`, pg.`id_prepayment_gifts`'
        .(isset($limit) ? ' LIMIT '.(int)$limit : ''));

        if (!$res) {
            return array();
        }

        return $res;
    }

    public static function getCustomerGifts(
        $id_lang,
        $id_customer,
        $active = false,
        $include_generic = true,
        $in_stock = false,
        Cart $cart = null
    ) {
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'prepayment_gifts` pg
		LEFT JOIN `'._DB_PREFIX_.'prepayment_gifts_lang` pgl ON (pg.`id_prepayment_gifts` = pgl.`id_prepayment_gifts` AND pgl.`id_lang` = '.(int)$id_lang.')
		WHERE (
			pg.`id_customer` = '.(int)$id_customer.' OR pg.group_restriction = 1
			'.($include_generic ? 'OR pg.`id_customer` = 0' : '').'
		)
		AND pg.date_from < "'.date('Y-m-d H:i:s').'"
		AND pg.date_to > "'.date('Y-m-d H:i:s').'"
		'.($active ? 'AND pg.`active` = 1' : '').'
		'.($in_stock ? 'AND pg.`quantity` > 0' : ''));

        // Remove cart rule that does not match the customer groups
        $customer_groups = Customer::getGroupsStatic($id_customer);

        foreach ($result as $key => $gift) {
            if ($gift['group_restriction']) {
                $gift_groups = Db::getInstance()->executeS('SELECT id_group FROM '._DB_PREFIX_.'prepayment_gifts_group WHERE id_prepayment_gifts = '.(int)$gift['id_prepayment_gifts']);
                foreach ($gift_groups as $gift_group) {
                    if (in_array($gift_group['id_group'], $customer_groups)) {
                        continue 2;
                    }
                }

                unset($result[$key]);
            }
        }

        foreach ($result as $key => $gift) {
            if ($gift['quantity_per_user']) {
                $quantity_used = self::usedByCustomer((int)$id_customer, $gift['id_prepayment_gifts']);
                $gift['quantity_per_user'] = $gift['quantity_per_user'] - $quantity_used;
            } else {
                $gift['quantity_per_user'] = 0;
            }

            if ($gift['quantity_per_user'] == 0) {
                unset($result[$key]);
            }
        }

        foreach ($result as $key => $gift) {
            if ($gift['shop_restriction']) {
                $gift_shops = Db::getInstance()->executeS('SELECT id_shop FROM '._DB_PREFIX_.'prepayment_gifts_shop WHERE id_prepayment_gifts = '.(int)$gift['id_prepayment_gifts']);
                foreach ($gift_shops as $gift_shop) {
                    if (Shop::isFeatureActive() && ($gift_shop['id_shop'] == Context::getContext()->shop->id)) {
                        continue 2;
                    }
                }
                unset($result[$key]);
            }
        }

        $result_bak = $result;
        $result = [];
        $country_restriction = false;
        foreach ($result_bak as $key => $gift) {
            if ($gift['country_restriction']) {
                $country_restriction = true;
                $countries = Db::getInstance()->executeS(
                    '
                    SELECT `id_country`
                    FROM `' . _DB_PREFIX_ . 'address`
                    WHERE `id_customer` = ' . (int) $id_customer . '
                    AND `deleted` = 0'
                );

                if (is_array($countries) && count($countries)) {
                    foreach ($countries as $country) {
                        $id_prepayment_gifts = (bool) Db::getInstance()->getValue('
                            SELECT pgc.id_prepayment_gifts
                            FROM ' . _DB_PREFIX_ . 'prepayment_gifts_country pgc
                            WHERE pgc.id_prepayment_gifts = ' . (int) $gift['id_prepayment_gifts'] . '
                            AND pgc.id_country = ' . (int) $country['id_country']);
                        if ($id_prepayment_gifts) {
                            $result[] = $result_bak[$key];

                            break;
                        }
                    }
                }
            } else {
                $result[] = $result_bak[$key];
            }
        }

        if (!$country_restriction) {
            $result = $result_bak;
        }

        return $result;
    }

    public static function usedByCustomer($id_customer, $id_gift)
    {
        return (int)Db::getInstance()->getValue('
		SELECT COUNT(plag.`id_prepayment_last_activities`)
		FROM `'._DB_PREFIX_.'prepayment_last_activities_gift` plag
        LEFT JOIN `'._DB_PREFIX_.'prepayment_last_activities` pla ON plag.`id_prepayment_last_activities` = pla.`id_prepayment_last_activities`
		LEFT JOIN `'._DB_PREFIX_.'prepayment_last_activities_order` plao ON plao.`id_prepayment_last_activities` = pla.`id_prepayment_last_activities`
		LEFT JOIN `'._DB_PREFIX_.'orders` o ON plao.`id_order` = o.`id_order`
		WHERE plag.`id_prepayment_gifts` = '.(int)$id_gift.'
		AND o.`id_customer` = '.(int)$id_customer.'
        AND '.(int)Configuration::get('PS_OS_ERROR').' != o.current_state');
    }

    public static function deleteByIdCustomer($id_customer)
    {
        $return = true;
        $gifts = new PrestaShopCollection('PrepaymentGifts');
        $gifts->where('id_customer', '=', (int)$id_customer);
        foreach ($gifts as $gift) {
            $return &= $gift->delete();
        }
        return $return;
    }

    public function getProductRuleGroups()
    {
        if (!Validate::isLoadedObject($this) || $this->product_restriction == 0) {
            return array();
        }

        $product_rule_groups = array();
        $result = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'prepayment_gifts_product_rule_group WHERE id_prepayment_gifts = '.(int)$this->id);
        foreach ($result as $row) {
            if (!isset($product_rule_groups[$row['id_product_rule_group']])) {
                $product_rule_groups[$row['id_product_rule_group']] = array('id_product_rule_group' => $row['id_product_rule_group'], 'quantity' => $row['quantity']);
            }
            $product_rule_groups[$row['id_product_rule_group']]['product_rules'] = $this->getProductRules($row['id_product_rule_group']);
        }
        return $product_rule_groups;
    }

    public function getProductRules($id_product_rule_group)
    {
        if (!Validate::isLoadedObject($this) || $this->product_restriction == 0) {
            return array();
        }

        $product_rules = array();
        $results = Db::getInstance()->executeS('
		SELECT *
		FROM '._DB_PREFIX_.'prepayment_gifts_product_rule pr
		LEFT JOIN '._DB_PREFIX_.'prepayment_gifts_product_rule_value prv ON pr.id_product_rule = prv.id_product_rule
		WHERE pr.id_product_rule_group = '.(int)$id_product_rule_group);
        foreach ($results as $row) {
            if (!isset($product_rules[$row['id_product_rule']])) {
                $product_rules[$row['id_product_rule']] = array('type' => $row['type'], 'values' => array());
            }
            $product_rules[$row['id_product_rule']]['values'][] = $row['id_item'];
        }
        return $product_rules;
    }

    public function checkValidity($object)
    {
        if (!$this->active
            || !$this->quantity
            || strtotime($this->date_from) > time()
            || strtotime($this->date_to) < time()
        ) {
            return false;
        }

        $cart = get_class($object) == 'Order' ? new Cart($object->id_cart) : $object;

        if ($object->id_customer) {
            $quantity_used = self::usedByCustomer((int)$object->id_customer, $this->id);
            if ($quantity_used + 1 > $this->quantity_per_user) {
                return false;
            }
        }

        // Get an intersection of the customer groups and the cart rule groups (if the customer is not logged in, the default group is 1)
        if ($this->group_restriction) {
            $id_gift = (int)Db::getInstance()->getValue('
			SELECT pgg.id_prepayment_gifts
			FROM '._DB_PREFIX_.'prepayment_gifts_group pgg
			WHERE pgg.id_prepayment_gifts = '.(int)$this->id.'
			AND pgg.id_group '.($object->id_customer ? 'IN (SELECT cg.id_group FROM '._DB_PREFIX_.'customer_group cg WHERE cg.id_customer = '.(int)$object->id_customer.')' : '= ' . (int) Configuration::get('PS_UNIDENTIFIED_GROUP')));
            if (!$id_gift) {
                return false;
            }
        }

        // Check if the customer delivery address is usable with the cart rule
        if ($this->country_restriction) {
            if (!$object->id_address_delivery) {
                return false;
            }
            $id_gift = (int)Db::getInstance()->getValue('
			SELECT pgc.id_prepayment_gifts
			FROM '._DB_PREFIX_.'prepayment_gifts_country pgc
			WHERE pgc.prepayment_gifts = '.(int)$this->id.'
			AND pgc.id_country = (SELECT a.id_country FROM '._DB_PREFIX_.'address a WHERE a.id_address = '.(int)$object->id_address_delivery.' LIMIT 1)');
            if (!$id_gift) {
                return false;
            }
        }

        // Check if the carrier chosen by the customer is usable with the cart rule
        if ($this->carrier_restriction) {
            if (!$object->id_carrier) {
                return false;
            }
            $id_gift = (int)Db::getInstance()->getValue('
			SELECT pgc.id_prepayment_gifts
			FROM '._DB_PREFIX_.'prepayment_gifts_carrier pgc
			INNER JOIN '._DB_PREFIX_.'carrier c ON (c.id_reference = pgc.id_carrier AND c.deleted = 0)
			WHERE pgc.id_prepayment_gifts = '.(int)$this->id.'
			AND c.id_carrier = '.(int)$object->id_carrier);
            if (!$id_gift) {
                return false;
            }
        }

        // Check if the cart rules appliy to the shop browsed by the customer
        if ($this->shop_restriction && $object->id_shop && Shop::isFeatureActive()) {
            $id_gift = (int)Db::getInstance()->getValue('
			SELECT pgs.id_prepayment_gifts
			FROM '._DB_PREFIX_.'prepayment_gifts_shop pgs
			WHERE pgs.id_prepayment_gifts = '.(int)$this->id.'
			AND pgs.id_shop = '.(int)$object->id_shop);
            if (!$id_gift) {
                return false;
            }
        }

        // Check if the payment chosen by the customer is usuable with the gift rule
        if ($this->payment_restriction && (get_class($object) == 'Order')) {
            $id_gift = (int)Db::getInstance()->getValue('
			SELECT pgp.id_prepayment_gifts
			FROM '._DB_PREFIX_.'prepayment_gifts_payment pgp
			WHERE pgp.id_prepayment_gifts = '.(int)$this->id.'
			AND pgp.id_module = '.(int)Module::getModuleIdByName($object->module));
            if (!$id_gift) {
                return false;
            }
        }

        // Check if the products chosen by the customer are usable with the cart rule
        if ($this->product_restriction
            && !$this->checkProductRestrictions($cart)
        ) {
            return false;
        }

        // Check if the cart rule is only usable by a specific customer, and if the current customer is the right one
        if ($this->id_customer && $object->id_customer != $this->id_customer) {
            return false;
        }

        if ($this->minimum_amount) {
            // Minimum amount is converted to the contextual currency
            $minimum_amount = $this->minimum_amount;
            if ($this->minimum_amount_currency != $object->id_currency) {
                $minimum_amount = Tools::convertPriceFull($minimum_amount, Currency::getCurrencyInstance($this->minimum_amount_currency), Currency::getCurrencyInstance($object->id_currency));
            }

            if (get_class($object) == 'Order') {
                $cart_total = $this->minimum_amount_tax == true ? $object->total_paid_tax_incl : $object->total_paid_tax_excl;
            } else {
                $cart_total = $object->getOrderTotal($this->minimum_amount_tax, Cart::ONLY_PRODUCTS);
                if ($this->minimum_amount_shipping) {
                    $cart_total += $object->getOrderTotal($this->minimum_amount_tax, Cart::ONLY_SHIPPING);
                }
            }

            if ($cart_total < $minimum_amount) {
                return false;
            }
        }

        $nb_products = Cart::getNbProducts($cart->id);
        if (!$nb_products) {
            return false;
        }

        return true;
    }

    protected function checkProductRestrictions($cart, $returnProducts = false)
    {
        $selected_products = array();

        // Check if the products chosen by the customer are usable with the cart rule
        if ($this->product_restriction) {
            $product_rule_groups = $this->getProductRuleGroups();
            foreach ($product_rule_groups as $id_product_rule_group => $product_rule_group) {
                $eligible_products_list = array();
                if (isset($cart) && is_object($cart) && is_array($products = $cart->getProducts())) {
                    foreach ($products as $product) {
                        $eligible_products_list[] = (int) $product['id_product'] . '-' . (int) $product['id_product_attribute'];
                    }
                }

                if (!count($eligible_products_list)) {
                    return false;
                }

                $product_rules = $this->getProductRules($id_product_rule_group);
                foreach ($product_rules as $product_rule) {
                    switch ($product_rule['type']) {
                        case 'attributes':
                            $cart_attributes = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, pac.`id_attribute`, cp.`id_product_attribute`
							FROM `'._DB_PREFIX_.'cart_product` cp
							LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON cp.id_product_attribute = pac.id_product_attribute
							WHERE cp.`id_cart` = '.(int)$cart->id.'
							AND cp.`id_product` IN ('.implode(',', array_map('intval', $eligible_products_list)).')
							AND cp.id_product_attribute > 0');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_attributes as $cart_attribute) {
                                if (in_array($cart_attribute['id_attribute'], $product_rule['values'])) {
                                    $count_matching_products += $cart_attribute['quantity'];
                                    $matching_products_list[] = $cart_attribute['id_product'].'-'.$cart_attribute['id_product_attribute'];
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                return false;
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                        case 'products':
                            $cart_products = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`
							FROM `'._DB_PREFIX_.'cart_product` cp
							WHERE cp.`id_cart` = '.(int)$cart->id.'
							AND cp.`id_product` IN ('.implode(',', array_map('intval', $eligible_products_list)).')');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_products as $cart_product) {
                                if (in_array($cart_product['id_product'], $product_rule['values'])) {
                                    $count_matching_products += $cart_product['quantity'];
                                    $matching_products_list[] = $cart_product['id_product'].'-0';
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                return false;
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                        case 'categories':
                            $cart_categories = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, cp.`id_product_attribute`, catp.`id_category`
							FROM `'._DB_PREFIX_.'cart_product` cp
							LEFT JOIN `'._DB_PREFIX_.'category_product` catp ON cp.id_product = catp.id_product
							WHERE cp.`id_cart` = '.(int)$cart->id.'
							AND cp.`id_product` IN ('.implode(',', array_map('intval', $eligible_products_list)).')');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_categories as $cart_category) {
                                if (in_array($cart_category['id_category'], $product_rule['values'])
                                    // We also check that the product is not already in the matching product list, because there are doubles in the query results (when the product is in multiple categories)
                                    && !in_array($cart_category['id_product'].'-'.$cart_category['id_product_attribute'], $matching_products_list)) {
                                    $count_matching_products += $cart_category['quantity'];
                                    $matching_products_list[] = $cart_category['id_product'].'-'.$cart_category['id_product_attribute'];
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                return false;
                            }
                            // Attribute id is not important for this filter in the global list, so the ids are replaced by 0
                            foreach ($matching_products_list as &$matching_product) {
                                $matching_product = preg_replace('/^([0-9]+)-[0-9]+$/', '$1-0', $matching_product);
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                        case 'manufacturers':
                            $cart_manufacturers = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, p.`id_manufacturer`
							FROM `'._DB_PREFIX_.'cart_product` cp
							LEFT JOIN `'._DB_PREFIX_.'product` p ON cp.id_product = p.id_product
							WHERE cp.`id_cart` = '.(int)$cart->id.'
							AND cp.`id_product` IN ('.implode(',', array_map('intval', $eligible_products_list)).')');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_manufacturers as $cart_manufacturer) {
                                if (in_array($cart_manufacturer['id_manufacturer'], $product_rule['values'])) {
                                    $count_matching_products += $cart_manufacturer['quantity'];
                                    $matching_products_list[] = $cart_manufacturer['id_product'].'-0';
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                return false;
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                        case 'suppliers':
                            $cart_suppliers = Db::getInstance()->executeS('
							SELECT cp.quantity, cp.`id_product`, p.`id_supplier`
							FROM `'._DB_PREFIX_.'cart_product` cp
							LEFT JOIN `'._DB_PREFIX_.'product` p ON cp.id_product = p.id_product
							WHERE cp.`id_cart` = '.(int)$cart->id.'
							AND cp.`id_product` IN ('.implode(',', array_map('intval', $eligible_products_list)).')');
                            $count_matching_products = 0;
                            $matching_products_list = array();
                            foreach ($cart_suppliers as $cart_supplier) {
                                if (in_array($cart_supplier['id_supplier'], $product_rule['values'])) {
                                    $count_matching_products += $cart_supplier['quantity'];
                                    $matching_products_list[] = $cart_supplier['id_product'].'-0';
                                }
                            }
                            if ($count_matching_products < $product_rule_group['quantity']) {
                                return false;
                            }
                            $eligible_products_list = $this->filterProducts($eligible_products_list, $matching_products_list, $product_rule['type']);

                            break;
                    }

                    if (!count($eligible_products_list)) {
                        return false;
                    }
                }
                $selected_products = array_merge($selected_products, $eligible_products_list);
            }
        }

        if ($returnProducts) {
            return $selected_products;
        }

        return true;
    }

    public function getContextualValue($use_tax, $object, $filter = null, $package = null)
    {
        if (!Validate::isLoadedObject($object)) {
            return 0;
        }

        $cart = get_class($object) == 'Order' ? new Cart($object->id_cart) : $object;

        $partial = PrepaymentPartials::getPartialInstance($cart->id);
        $id_partial = Validate::isLoadedObject($partial) ? $partial->id : 0;

        if (!$filter) {
            $filter = PrepaymentGifts::FILTER_ACTION_ALL;
        }

        $all_products = $cart->getProducts();
        $package_products = (is_null($package) ? $all_products : $package['products']);

        $cash_back_value = 0;

        $cache_id = 'getContextualValue_'.(int)$this->id.'_'.(int)$use_tax.'_'.(int)$cart->id.'_'.(int)$filter.'_'.(int)$id_partial;
        foreach ($package_products as $product) {
            $cache_id .= '_'.(int)$product['id_product'].'_'.(int)$product['id_product_attribute'];
        }

        if (Cache::isStored($cache_id)) {
            return Cache::retrieve($cache_id);
        }

        if (in_array($filter, array(PrepaymentGifts::FILTER_ACTION_ALL, PrepaymentGifts::FILTER_ACTION_ALL_NOCAP, PrepaymentGifts::FILTER_ACTION_REDUCTION))) {
            // Discount (%) on the whole order
            if ($this->reduction_percent > 0 && $this->reduction_product == 0) {
                if (get_class($object) == 'Order') {
                    $order_total = $use_tax == true ? $object->total_paid_tax_incl : $object->total_paid_tax_excl;
                } else {
                    $order_total = $object->getOrderTotal($use_tax, Cart::BOTH);
                }

                // add partial cart rule
                if (Validate::isLoadedObject($partial)
                    && ($cart_rule = new CartRule((int)$partial->id_cart_rule))
                    && Validate::isLoadedObject($cart_rule)
                ) {
                    $order_total += $cart_rule->getContextualValue($use_tax, Context::getContext(), CartRule::FILTER_ACTION_ALL);
                }

                $cash_back_value += $order_total * $this->reduction_percent / 100;
            }

            // Discount (%) on the selection of products
            if ($this->reduction_percent && $this->reduction_product == 1) {
                $selected_products_reduction = 0;
                $selected_products = $this->checkProductRestrictions($cart, true);

                if (is_array($selected_products)) {
                    foreach ($package_products as $product) {
                        if (in_array($product['id_product'] . '-' . $product['id_product_attribute'], $selected_products)
                            || in_array($product['id_product'] . '-0', $selected_products)
                        ) {
                            $price = $product['price'];
                            if ($use_tax) {
                                $price *= (1 + $product['rate']);
                            }

                            $selected_products_reduction += $price * $product['cart_quantity'];
                        }
                    }
                }

                $cash_back_value += $selected_products_reduction * $this->reduction_percent / 100;
            }

            // Discount (¤)
            if ($this->reduction_amount > 0) {
                $reduction_amount = $this->reduction_amount;
                $currency = Currency::getCurrencyInstance($object->id_currency);

                // If we need to convert the voucher value to the cart currency
                if ($this->reduction_currency != $currency->id) {
                    $voucher_currency = Currency::getCurrencyInstance($this->reduction_currency);
                    // First we convert the voucher value to the default currency
                    if ($reduction_amount == 0 || $voucher_currency->conversion_rate == 0) {
                        $reduction_amount = 0;
                    } else {
                        $reduction_amount /= $voucher_currency->conversion_rate;
                    }

                    // Then we convert the voucher value in the default currency into the cart currency
                    $reduction_amount *= $currency->conversion_rate;
                    $reduction_amount = Tools::ps_round($reduction_amount, 2);
                }

                // If it has the same tax application that you need, then it's the right value, whatever the product!
                if ($this->reduction_tax == $use_tax) {
                    $cash_back_value = $reduction_amount;
                } else {
                    if (get_class($object) == 'Order') {
                        $cart_amount_ti = $object->total_paid_tax_incl;
                        $cart_amount_te = $object->total_paid_tax_excl;
                    } else {
                        $cart_amount_ti = $object->getOrderTotal(true, Cart::BOTH);
                        $cart_amount_te = $object->getOrderTotal(false, Cart::BOTH);
                    }

                    // Discount (¤) on the whole order
                    $cart_average_vat_rate = $cart->getAverageProductsTaxRate($cart_amount_te, $cart_amount_ti);

                    if ($this->reduction_tax && !$use_tax) {
                        $cash_back_value += $reduction_amount / (1 + $cart_average_vat_rate);
                    } elseif (!$this->reduction_tax && $use_tax) {
                        $cash_back_value += $reduction_amount * (1 + $cart_average_vat_rate);
                    }
                }
            }
        }

        Cache::store($cache_id, $cash_back_value);
        return $cash_back_value;
    }

    public function getAssociatedRestrictions($type, $active_only, $i18n)
    {
        $array = array('selected' => array(), 'unselected' => array());

        if (!in_array($type, array('country', 'carrier', 'group', 'shop', 'payment'))) {
            return false;
        }

        $shop_list = '';
        if ($type == 'shop') {
            $shops = Shop::getShops(true, null, true);
            if (count($shops)) {
                $shop_list = ' AND t.id_shop IN ('.implode(',', array_map('intval', $shops)).')';
            }
        }

        $payment_list = '';
        if ($type == 'payment') {
            $payments = PaymentModule::getInstalledPaymentModules();
            if (count($payments)) {
                $payment_list = ' AND t.id_module IN ('.implode(',', array_map(array($this, 'getIdModule'), $payments)).')';
            }
        }

        if (!Validate::isLoadedObject($this) || $this->{$type.'_restriction'} == 0) {
            $array['selected'] = Db::getInstance()->executeS('
			SELECT t.*'.($i18n ? ', tl.*' : '').', 1 as selected
			FROM `'._DB_PREFIX_.($type == 'payment' ? 'module' : $type).'` t
			'.($i18n ? 'LEFT JOIN `'._DB_PREFIX_.$type.'_lang` tl ON (t.id_'.$type.' = tl.id_'.$type.' AND tl.id_lang = '.(int)Context::getContext()->language->id.')' : '').'
			WHERE 1
			'.($active_only ? 'AND t.active = 1' : '').'
			'.(in_array($type, array('carrier', 'shop')) ? ' AND t.deleted = 0' : '').
            ($type == 'payment' ? $payment_list : $shop_list).
            (in_array($type, ['carrier', 'shop', 'payment']) ? ' ORDER BY t.name ASC ' : '') .
            (in_array($type, ['country', 'group']) && $i18n ? ' ORDER BY tl.name ASC ' : ''));
        } else {
            $resource = Db::getInstance()->executeS(
                'SELECT t.*'.($i18n ? ', tl.*' : '').', IF(crt.id_'.($type == 'payment' ? 'module' : $type).' IS NULL, 0, 1) as selected
    			FROM `'._DB_PREFIX_.($type == 'payment' ? 'module' : $type).'` t
    			'.($i18n ? 'LEFT JOIN `'._DB_PREFIX_.$type.'_lang` tl ON (t.id_'.$type.' = tl.id_'.$type.' AND tl.id_lang = '.(int)Context::getContext()->language->id.')' : '').'
    			LEFT JOIN (SELECT id_'.($type == 'payment' ? 'module' : $type).' FROM `'._DB_PREFIX_.'prepayment_gifts_'.$type.'` WHERE id_prepayment_gifts = '.(int)$this->id.') crt
    				ON t.id_'.($type == 'carrier' ? 'reference' : ($type == 'payment' ? 'module' : $type)).' = crt.id_'.($type == 'payment' ? 'module' : $type).'
    			WHERE 1 '.($active_only ? ' AND t.active = 1' : '').
                ($type == 'payment' ? $payment_list : $shop_list).
                (in_array(
                    $type,
                    array('carrier', 'shop')
                ) ? ' AND t.deleted = 0' : '').
                (in_array($type, ['carrier', 'shop', 'payment']) ? ' ORDER BY t.name ASC ' : '') .
                (in_array($type, ['country', 'group']) && $i18n ? ' ORDER BY tl.name ASC ' : ''),
                false
            );
            while ($row = Db::getInstance()->nextRow($resource)) {
                $array[($row['selected'] || $this->{$type.'_restriction'} == 0) ? 'selected' : 'unselected'][] = $row;
            }
        }

        return $array;
    }

    public static function cleanProductRuleIntegrity($type, $list)
    {
        // Type must be available in the 'type' enum of the table cart_rule_product_rule
        if (!in_array($type, ['products', 'categories', 'attributes', 'manufacturers', 'suppliers'])) {
            return false;
        }

        // This check must not be removed because this var is used a few lines below
        $list = (is_array($list) ? implode(',', array_map('intval', $list)) : (int) $list);
        if (!preg_match('/^[0-9,]+$/', $list)) {
            return false;
        }

        // Delete associated restrictions on cart rules
        Db::getInstance()->execute('
		DELETE crprv
		FROM `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule` crpr
		LEFT JOIN `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_value` crprv ON crpr.`id_product_rule` = crprv.`id_product_rule`
		WHERE crpr.`type` = "' . pSQL($type) . '"
		AND crprv.`id_item` IN (' . $list . ')');
        // $list is checked a few lines above
        // Delete the product rules that does not have any values
        if (Db::getInstance()->Affected_Rows() > 0) {
            Db::getInstance()->delete('prepayment_gifts_product_rule', 'NOT EXISTS (SELECT 1 FROM `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_value`
			WHERE `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule`.`id_product_rule` = `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_value`.`id_product_rule`)');
        }
        // If the product rules were the only conditions of a product rule group, delete the product rule group
        if (Db::getInstance()->Affected_Rows() > 0) {
            Db::getInstance()->delete('prepayment_gifts_product_rule_group', 'NOT EXISTS (SELECT 1 FROM `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule`
			WHERE `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule`.`id_product_rule_group` = `' . _DB_PREFIX_ . 'prepayment_gifts_product_rule_group`.`id_product_rule_group`)');
        }

        return true;
    }

    protected function filterProducts($products, $eligibleProducts, $ruleType)
    {
        //If the two same array, no verification todo.
        if ($products === $eligibleProducts) {
            return $products;
        }
        $return = [];
        // Attribute id is not important for this filter in the global list
        // so the ids are replaced by 0
        if (in_array($ruleType, ['products', 'categories', 'manufacturers', 'suppliers'])) {
            $productsList = explode(':', preg_replace("#\-[0-9]+#", '-0', implode(':', $products)));
        } else {
            $productsList = $products;
        }

        foreach ($productsList as $k => $product) {
            if (in_array($product, $eligibleProducts)) {
                $return[] = $products[$k];
            }
        }

        return $return;
    }

    protected function getIdModule($module)
    {
        return (int)$module['id_module'];
    }

    public static function getGift($object)
    {
        if (!Validate::isLoadedObject($object)
            || !($wallet = PrepaymentWallets::getWalletInstance((int)$object->id_customer))) {
            return;
        }

        $sql = '
		SELECT pg.*
		FROM '._DB_PREFIX_.'prepayment_gifts pg
		LEFT JOIN '._DB_PREFIX_.'prepayment_gifts_shop pgs ON pg.id_prepayment_gifts = pgs.id_prepayment_gifts
		LEFT JOIN '._DB_PREFIX_.'prepayment_gifts_payment pgp ON pg.id_prepayment_gifts = pgp.id_prepayment_gifts
		LEFT JOIN '._DB_PREFIX_.'prepayment_gifts_carrier pgca ON pg.id_prepayment_gifts = pgca.id_prepayment_gifts
		'.($object->id_carrier ? 'LEFT JOIN '._DB_PREFIX_.'carrier c ON (c.id_reference = pgca.id_carrier AND c.deleted = 0)' : '').'
		LEFT JOIN '._DB_PREFIX_.'prepayment_gifts_country pgco ON pg.id_prepayment_gifts = pgco.id_prepayment_gifts
		WHERE pg.active = 1
		AND pg.quantity > 0
		AND pg.date_from < "'.date('Y-m-d H:i:s').'"
		AND pg.date_to > "'.date('Y-m-d H:i:s').'"
		AND (
			pg.id_customer = 0
			'.($object->id_customer ? 'OR pg.id_customer = '.(int)$object->id_customer : '').'
		)
		AND (
			pg.`carrier_restriction` = 0
			'.($object->id_carrier ? 'OR c.id_carrier = '.(int)$object->id_carrier : '').'
		)
		AND (
			pg.`shop_restriction` = 0
			'.((Shop::isFeatureActive() && $object->id_shop) ? 'OR pgs.id_shop = '.(int)$object->id_shop : '').'
		)
		AND (
			pg.`payment_restriction` = 0
			'.(get_class($object) == 'Order' ? 'OR pgp.id_module = '.(int)Module::getModuleIdByName($object->module) : '').'
		)
		AND (
			pg.`group_restriction` = 0
			'.($object->id_customer ? 'OR 0 < (
				SELECT cg.`id_group`
				FROM `'._DB_PREFIX_.'customer_group` cg
				INNER JOIN `'._DB_PREFIX_.'prepayment_gifts_group` pgg ON cg.id_group = pgg.id_group
				WHERE pg.`id_prepayment_gifts` = pgg.`id_prepayment_gifts`
				AND cg.`id_customer` = '.(int)$object->id_customer.'
				LIMIT 1
			)' : '').'
		)
		ORDER BY priority';
        $result = Db::getInstance()->executeS($sql);

        if ($result) {
            $gifts = ObjectModel::hydrateCollection('PrepaymentGifts', $result);
            if ($gifts) {
                foreach ($gifts as $gift) {
                    if (Validate::isLoadedObject($gift) && $gift->checkValidity($object)) {
                        return $gift;
                    }
                }
            }
        }

        return false;
    }

    public static function getStats()
    {
        $result = array();

        /* Get most used gift rule */
        $most_used = Db::getInstance()->getRow('
		SELECT COUNT(pg.`id_prepayment_gifts`) as nb, pgl.`name`
		FROM `'._DB_PREFIX_.'prepayment_gifts` pg
		LEFT JOIN `'._DB_PREFIX_.'prepayment_gifts_lang` pgl on pg.`id_prepayment_gifts` = pgl.`id_prepayment_gifts`
		WHERE pgl.`id_lang` = '.(int)Context::getContext()->language->id.'
		ORDER BY `nb` DESC');
        if (!$most_used) {
            $most_used = array();
        }

        /* Get average amount per gift rule and number of gifts used*/
        $gifts = Db::getInstance()->getRow('
		SELECT SUM(pla.`amount`) AS nb_credit, COUNT(pla.`id_prepayment_last_activities`) AS nb_operation
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`id_operation` = '.(int)PrepaymentLastActivities::GIFT.'
		HAVING COUNT(pla.`id_prepayment_last_activities`) > 0');
        if (!$gifts) {
            $amount_per_gift = 0;
            $gifts_used = 0;
        } else {
            $amount_per_gift = $gifts['nb_credit'] / $gifts['nb_operation'];
            $gifts_used = $gifts['nb_operation'];
        }

        /* Get expired gift rules*/
        $now = date('Y-m-d H:i:s');
        $expired_gifts = Db::getInstance()->getValue('
		SELECT COUNT(`id_prepayment_gifts`)
		FROM `'._DB_PREFIX_.'prepayment_gifts`
		WHERE `date_to` < \''.pSQL($now).'\'');

        /* Add stats into an array */
        $result['most_used'] = $most_used;
        $result['amount_per_gift'] = Tools::ps_round($amount_per_gift, 2);
        $result['gifts_used'] = $gifts_used;
        $result['expired_gifts'] = $expired_gifts;

        return $result;
    }

    public function getRestrictions($type)
    {
        if (!in_array($type, array('country', 'carrier', 'group', 'shop', 'payment'))) {
            return array();
        }

        $result = array();
        $property = $type.'_restriction';

        if ($this->{$property}) {
            $active_only = in_array($type, array('carrier', 'country')) ? true : false;
            $i18n = in_array($type, array('group', 'country')) ? true : false;

            $rows = $this->getAssociatedRestrictions($type, $active_only, $i18n);
            if (count($rows['selected'])) {
                foreach ($rows['selected'] as $row) {
                    $result[] = array(
                        'id' => $type == 'payment' ? $row['id_module'] : $row['id_'.$type]
                    );
                }
            }
        }


        return $result;
    }

    public function setRestrictions($type, $rows)
    {
        if (!in_array($type, array('country', 'carrier', 'group', 'shop', 'payment'))) {
            return false;
        }

        $shop_list = '';
        if ($type == 'shop') {
            $shops = Shop::getShops(true, null, true);
            if (count($shops)) {
                $shop_list = ' AND t.id_shop IN ('.implode(',', array_map('intval', $shops)).')';
            }
        }

        $payment_list = '';
        if ($type == 'payment') {
            $payments = PaymentModule::getInstalledPaymentModules();
            if (count($payments)) {
                $payment_list = ' AND t.id_module IN ('.implode(',', array_map(array($this, 'getIdModule'), $payments)).')';
            }
        }

        Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'prepayment_gifts_'.$type.'` WHERE `id_prepayment_gifts` = '.(int)$this->id);

        $nb_type = Db::getInstance()->getValue('
            SELECT COUNT(*)
            FROM `'._DB_PREFIX_.($type == 'payment' ? 'module' : $type).'` t
            WHERE 1
            '.(in_array($type, array('carrier', 'country')) ? ' AND t.active = 1' : '').
            (in_array($type, array('carrier', 'shop')) ? ' AND t.deleted = 0' : '').
            ($type == 'payment' ? $payment_list : $shop_list).'
        ');

        $rows = array_unique($rows, SORT_REGULAR);
        $property = $type.'_restriction';

        if ($nb_type == count($rows)) {
            $this->{$property} = 0;
            Db::getInstance()->execute('
                UPDATE ' . _DB_PREFIX_ . 'prepayment_gifts pg
                SET pg.`'.$type.'_restriction` = 0
            ');
        }

        if ($this->{$property}) {
            $values = array();
            foreach ($rows as $row) {
                $values[] = '('.(int)$this->id.','.(int)$row['id'].')';
            }

            if (count($values)) {
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'prepayment_gifts_'.$type.'` (`id_prepayment_gifts`, `id_'.($type == 'payment' ? 'module' : $type).'`) VALUES '.implode(',', $values)
                );
            }
        }

        return true;
    }

    public function getWsProductRules()
    {
        $result = array();

        if ($this->product_restriction) {
            $product_rule_groups = $this->getProductRuleGroups();
            foreach ($product_rule_groups as $product_rule_group) {
                foreach ($product_rule_group['product_rules'] as $rule) {
                    foreach ($rule['values'] as $id) {
                        $result[] = array(
                            'quantity' => $product_rule_group['quantity'],
                            'type' => $rule['type'],
                            'id' => $id
                        );
                    }
                }
            }
        }

        return $result;
    }

    public function setWsProductRules($product_rules)
    {
        Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'prepayment_gifts_product_rule_group` WHERE `id_prepayment_gifts` = '.(int)$this->id);
        Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'prepayment_gifts_product_rule` WHERE `id_product_rule_group`
            NOT IN (SELECT `id_product_rule_group` FROM `'._DB_PREFIX_.'prepayment_gifts_product_rule_group`)');
        Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'prepayment_gifts_product_rule_value` WHERE `id_product_rule`
            NOT IN (SELECT `id_product_rule` FROM `'._DB_PREFIX_.'prepayment_gifts_product_rule`)');

        if (($product_restriction = Db::getInstance()->getValue('
            SELECT `product_restriction`
            FROM `'._DB_PREFIX_.'prepayment_gifts`
            WHERE `id_prepayment_gifts` = '.(int)$this->id))
        ) {
            if (is_array($product_rules) && count($product_rules)) {
                $types = array('attributes', 'products', 'suppliers', 'manufacturers', 'categories');

                $rules = array();
                foreach ($product_rules as $product_rule) {
                    if (!isset($rules[$product_rule['quantity']][$product_rule['type']])) {
                        $rules[$product_rule['quantity']][$product_rule['type']] = array();
                    }

                    array_push($rules[$product_rule['quantity']][$product_rule['type']], $product_rule['id']);
                }

                foreach ($rules as $quantity => $rule) {
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'prepayment_gifts_product_rule_group` (`id_prepayment_gifts`, `quantity`)
                    VALUES ('.(int)$this->id.', '.(int)$quantity.')');
                    $id_product_rule_group = Db::getInstance()->Insert_ID();

                    foreach ($rule as $type => $ids) {
                        Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'prepayment_gifts_product_rule` (`id_product_rule_group`, `type`)
                        VALUES ('.(int)$id_product_rule_group.', "'.pSQL($type).'")');
                        $id_product_rule = Db::getInstance()->Insert_ID();

                        $values = array();
                        foreach ($ids as $id) {
                            $values[] = '('.(int)$id_product_rule.','.(int)$id.')';
                        }
                        $values = array_unique($values);

                        if (count($values)) {
                            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'prepayment_gifts_product_rule_value` (`id_product_rule`, `id_item`) VALUES '.implode(',', $values));
                        }
                    }
                }
            }
        }

        return true;
    }

    public function getWsCountries()
    {
        return $this->getRestrictions('country');
    }

    public function setWsCountries($countries)
    {
        return $this->setRestrictions('country', $countries);
    }

    public function getWsGroups()
    {
        return $this->getRestrictions('group');
    }

    public function setWsGroups($groups)
    {
        return $this->setRestrictions('group', $groups);
    }

    public function getWsShops()
    {
        return $this->getRestrictions('shop');
    }

    public function setWsShops($shops)
    {
        return $this->setRestrictions('shop', $shops);
    }

    public function getWsPayments()
    {
        return $this->getRestrictions('payment');
    }

    public function setWsPayments($payments)
    {
        return $this->setRestrictions('payment', $payments);
    }

    public function getWsCarriers()
    {
        return $this->getRestrictions('carrier');
    }

    public function setWsCarriers($carriers)
    {
        return $this->setRestrictions('carrier', $carriers);
    }
}
