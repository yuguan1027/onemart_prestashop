<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class PrepaymentWallets extends ObjectModel
{
    public $id_customer;

    public $total_amount = 0;

    public $total_orders_amount = 0;

    public $total_deposits_amount = 0;

    public $total_gifts_amount = 0;

    public $total_refunds_amount = 0;

    public $total_disbursements_amount = 0;

    public $active;

    public $date_add;

    public $date_upd;


    public static $definition = array(
        'table' => 'prepayment_wallets',
        'primary' => 'id_prepayment_wallets',
        'fields' => array(
            'id_customer' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'total_amount' =>                array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice'),
            'total_orders_amount' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice'),
            'total_deposits_amount' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice'),
            'total_gifts_amount' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice'),
            'total_refunds_amount' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice'),
            'total_disbursements_amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice'),
            'active' =>                        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

    protected $webserviceParameters = array(
        'objectsNodeName' => 'prepayment_wallets',
        'fields' => array(
            'id_customer' => array(
                'setter' => 'setWsCustomerId'
            ),
            'total_amount' => array(
                'setter' => 'setWsTotalAmount'
            ),
            'total_orders_amount' => array(
                'setter' => 'setWsTotalOrdersAmount'
            ),
            'total_deposits_amount' => array(
                'setter' => 'setWsTotalDepositsAmount'
            ),
            'total_gifts_amount' => array(
                'setter' => 'setWsTotalGiftsAmount'
            ),
            'total_refunds_amount' => array(
                'setter' => 'setWsTotalRefundsAmount'
            ),
            'total_disbursements_amount' => array(
                'setter' => 'setWsTotalDisbursementsAmount'
            ),
        ),
        'associations' => array(
            'last_activities' => array(
                'getter' => 'getWsLastActivities',
                'setter' => false,
                'resource' => 'last_activity',
                'api' => 'last_activities',
            ),
        ),
    );

    public function add($autodate = true, $null_values = false)
    {
        $customer = new Customer((int)$this->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            throw new PrestaShopException('The customer does not exist');
        } elseif (PrepaymentWallets::walletExists($customer->id)) {
            throw new PrestaShopException('A wallet is already set for this customer');
        }

        if (!parent::add($autodate, $null_values)) {
            return false;
        }

        return true;
    }

    public function update($null_values = false)
    {
        $this->total_amount = Tools::ps_round($this->total_amount, _PS_PRICE_COMPUTE_PRECISION_);
        $this->total_orders_amount = Tools::ps_round($this->total_orders_amount, _PS_PRICE_COMPUTE_PRECISION_);
        $this->total_deposits_amount = Tools::ps_round($this->total_deposits_amount, _PS_PRICE_COMPUTE_PRECISION_);
        $this->total_gifts_amount = Tools::ps_round($this->total_gifts_amount, _PS_PRICE_COMPUTE_PRECISION_);
        $this->total_refunds_amount = Tools::ps_round($this->total_refunds_amount, _PS_PRICE_COMPUTE_PRECISION_);
        $this->total_disbursements_amount = Tools::ps_round($this->total_disbursements_amount, _PS_PRICE_COMPUTE_PRECISION_);

        return parent::update($null_values);
    }

    public function delete()
    {
        return false;
    }

    public function getWsLastActivities()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pla.id_prepayment_last_activities as id
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.id_wallet = '.(int)$this->id);
    }

    public static function getWallets($limit = null)
    {
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pw.*
		FROM `'._DB_PREFIX_.'prepayment_wallets` pw
		ORDER BY pw.`total_amount` DESC'
        .(isset($limit) ? ' LIMIT '.(int)$limit : ''));

        if (!$res) {
            return array();
        }

        return $res;
    }

    public static function getWalletInstance($id_customer)
    {
        $id_wallet = Db::getInstance()->getValue('
		SELECT `id_prepayment_wallets`
		FROM `'._DB_PREFIX_.'prepayment_wallets`
		WHERE `id_customer` = '.(int)$id_customer);

        $wallet = new PrepaymentWallets((int)$id_wallet);
        if (Validate::isLoadedObject($wallet)) {
            return $wallet;
        }

        return false;
    }

    public static function walletExists($id_customer)
    {
        return (bool)Db::getInstance()->getValue('
		SELECT `id_prepayment_wallets`
		FROM `'._DB_PREFIX_.'prepayment_wallets`
		WHERE `id_customer` = '.(int)$id_customer);
    }

    public static function getBalance()
    {
        return (float)Db::getInstance()->getValue('
		SELECT SUM(pw.`total_amount`)
		FROM `'._DB_PREFIX_.'prepayment_wallets` pw');
    }

    public static function getStats()
    {
        $result = array();
        $nb_wallets = count(self::getWallets());
        $nb_customers = count(Customer::getCustomers());
        $wallet_per_customer = $nb_wallets / $nb_customers * 100;

        $active_wallets = Db::getInstance()->getValue('
		SELECT COUNT(`id_prepayment_wallets`)
		FROM `'._DB_PREFIX_.'prepayment_wallets`
		WHERE `total_amount` > 0');

        $unactive_wallets = Db::getInstance()->getValue('
		SELECT COUNT(`id_prepayment_wallets`)
		FROM `'._DB_PREFIX_.'prepayment_wallets`
		WHERE `total_amount` = 0');

        $result['nb_wallets'] = $nb_wallets;
        $result['wallet_per_customer'] = $wallet_per_customer;
        $result['active_wallets'] = $active_wallets;
        $result['unactive_wallets'] = $unactive_wallets;

        return $result;
    }

    public function setWsCustomerId($id_customer)
    {
        if (!Validate::isLoadedObject($this)) {
            $this->id_customer = $id_customer;
        }

        return true;
    }

    public function setWsTotalAmount($total_amount)
    {
        return true;
    }

    public function setWsTotalOrdersAmount($total_amount)
    {
        return true;
    }

    public function setWsTotalDepositsAmount($total_amount)
    {
        return true;
    }

    public function setWsTotalGiftsAmount($total_amount)
    {
        return true;
    }

    public function setWsTotalRefundsAmount($total_amount)
    {
        return true;
    }

    public function setWsTotalDisbursementsAmount($total_amount)
    {
        return true;
    }
}
