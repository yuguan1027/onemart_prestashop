<?php
/**
* 2017 - Keyrnel SARL
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author    Keyrnel
* @copyright 2017 - Keyrnel SARL
* @license   commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

class PrepaymentLastActivities extends ObjectModel
{
    public $id_operation;

    public $id_wallet;

    public $id_currency;

    public $reference;

    public $amount;

    public $paid;

    public $date_add;

    public $date_upd;

    public $label;

    public static $definition = array(
        'table' => 'prepayment_last_activities',
        'primary' => 'id_prepayment_last_activities',
        'multilang' => true,
        'fields' => array(
            'id_operation' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_wallet' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'reference' =>        array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
            'amount' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isNegativePrice', 'required' => true),
            'id_currency' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'paid' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' =>        array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' =>        array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'label' =>          array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 64),
        ),
    );

    protected $webserviceParameters = array(
        'objectMethods' => array(
            'add' => 'addWs',
            'update' => 'updateWs',
        ),
        'objectsNodeName' => 'prepayment_last_activities',
        'hidden_fields' => array('id_operation'),
        'fields' => array(
            'id_operation' => array(
                'setter' => 'setWsOperationId'
            ),
            'operation' => array(
                'getter' => 'getWsOperation',
                'setter' => 'setWsOperation'
            ),
            'reference' => array(
                'setter' => 'setWsReference'
            ),
            'paid' => array(
                'setter' => 'setWsPaid'
            )
        ),
        'associations' => array(
            'order' => array(
                'getter' => 'getWsOrder',
                'setter' => false,
                'resource' => 'order',
                'api' => 'last_activities',
                'fields' => array(
                    'id' => array()
                )
            ),
            'refund' => array(
                'getter' => 'getWsRefunds',
                'setter' => false,
                'resource' => 'order_slip',
                'api' => 'last_activities',
                'fields' => array(
                    'id' => array()
                )
            ),
            'partial' => array(
                'getter' => 'getWsPartials',
                'setter' => false,
                'resource' => 'partial',
                'api' => 'last_activities',
                'fields' => array(
                    'id_cart' => array(),
                    'id_cart_rule' => array(),
                )
            )
        )
    );

    const DEPOSIT = 1;
    const ORDER = 2;
    const REFUND = 3;
    const DISBURSEMENT = 4;
    const GIFT = 5;
    const CUSTOM_DEPOSIT = 6;
    const CUSTOM_DISBURSEMENT = 7;

    public function add($autodate = true, $null_values = false)
    {
        if (in_array($this->id_operation, array(self::CUSTOM_DEPOSIT, self::CUSTOM_DISBURSEMENT))) {
            $this->paid = 1;
            if (!$this->id_currency) {
                $this->id_currency = Configuration::get('PS_CURRENCY_DEFAULT');
            }
        }

        $this->reference = PrepaymentLastActivities::generateReference();

        $this->beforeSave();

        if (!parent::add($autodate, $null_values)) {
            return false;
        }

        $this->afterSave();

        return true;
    }

    public function update($null_values = false)
    {
        $this->beforeSave();

        if (!parent::update($null_values)) {
            return false;
        }

        $this->afterSave();

        return true;
    }

    public function delete()
    {
        if ($this->getOrder() || !parent::delete()) {
            return false;
        }

        $this->deletePartial();
        $this->afterSave();

        return true;
    }

    public function beforeSave()
    {
        $wallet = new PrepaymentWallets($this->id_wallet);
        if (!Validate::isLoadedObject($wallet)) {
            throw new PrestaShopException('The wallet does not exist');
        }

        $operation_ids = array(
            PrepaymentLastActivities::DEPOSIT,
            PrepaymentLastActivities::ORDER,
            PrepaymentLastActivities::REFUND,
            PrepaymentLastActivities::DISBURSEMENT,
            PrepaymentLastActivities::GIFT,
            PrepaymentLastActivities::CUSTOM_DEPOSIT,
            PrepaymentLastActivities::CUSTOM_DISBURSEMENT,
        );

        if (!in_array($this->id_operation, $operation_ids)) {
            throw new PrestaShopException('The operation is not valid');
        }

        $currency_ids = array();
        $currencies = Currency::getCurrencies();
        foreach ($currencies as $currency) {
            $currency_ids[] = $currency['id_currency'];
        }

        if (!in_array($this->id_currency, $currency_ids)) {
            throw new PrestaShopException('The currency does not exist');
        }

        $label_exists = true;
        $languages = Language::getLanguages(false);
        foreach ($languages as $language) {
            if (empty($this->label[Configuration::get('PS_LANG_DEFAULT')])) {
                $label_exists = false;
                break;
            }

            $this->label[$language['id_lang']] = $this->label[Configuration::get('PS_LANG_DEFAULT')];
        }

        if (!$label_exists) {
            $this->label = null;
        }

        $this->amount = Tools::ps_round($this->amount, _PS_PRICE_COMPUTE_PRECISION_);
    }

    public function afterSave()
    {
        $wallet = new PrepaymentWallets($this->id_wallet);
        $old_wallet_balance = $wallet->total_amount;
        self::updateWallet($wallet);
        $new_wallet_balance = $wallet->total_amount;

        $prepayment = new Prepayment();
        $prepayment->processNotification(
            $wallet->id_customer,
            $this->id_operation,
            $old_wallet_balance,
            $new_wallet_balance,
            $this->id_currency
        );
    }

    public function getFieldsLang()
    {
        if (!is_array($this->label)) {
            $this->deleteLabel();
            return true;
        }

        return parent::getFieldsLang();
    }

    public static function updateWallet(&$wallet)
    {
        if (!is_object($wallet) || !Validate::isLoadedObject($wallet)) {
            return false;
        }

        $wallet->total_deposits_amount = self::getTotalAmount(PrepaymentLastActivities::DEPOSIT, $wallet->id);
        $wallet->total_deposits_amount += self::getTotalAmount(PrepaymentLastActivities::CUSTOM_DEPOSIT, $wallet->id);
        $wallet->total_orders_amount = self::getTotalAmount(PrepaymentLastActivities::ORDER, $wallet->id);
        $wallet->total_refunds_amount = self::getTotalAmount(PrepaymentLastActivities::REFUND, $wallet->id);
        $wallet->total_disbursements_amount = self::getTotalAmount(PrepaymentLastActivities::DISBURSEMENT, $wallet->id);
        $wallet->total_disbursements_amount += self::getTotalAmount(PrepaymentLastActivities::CUSTOM_DISBURSEMENT, $wallet->id);
        $wallet->total_gifts_amount = self::getTotalAmount(PrepaymentLastActivities::GIFT, $wallet->id);
        $wallet->total_amount = $wallet->total_deposits_amount + $wallet->total_gifts_amount + $wallet->total_refunds_amount - $wallet->total_orders_amount - $wallet->total_disbursements_amount;
        $wallet->update();
        return true;
    }

    public static function getLastActivities($id_wallet = null, $limit = null)
    {
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pla.*, plal.`label`
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		LEFT JOIN `'._DB_PREFIX_.'prepayment_last_activities_lang` plal ON (pla.`id_prepayment_last_activities` = plal.`id_prepayment_last_activities` AND plal.`id_lang` = '.(int)Context::getContext()->language->id.')'
        .(isset($id_wallet) ? ' WHERE pla.`id_wallet` = '.(int)$id_wallet : '').'
        GROUP BY pla.`id_prepayment_last_activities`
        ORDER BY pla.`date_add` DESC'
        .(isset($limit) ? ' LIMIT '.(int)$limit : ''));

        if (!$res) {
            return array();
        }

        return $res;
    }

    public function retrieveCustomerId()
    {
        $id_customer = 0;

        if (Validate::isLoadedObject($this)
            && ($wallet = new PrepaymentWallets($this->id_wallet))
            && Validate::isLoadedObject($wallet)
        ) {
            $id_customer = $wallet->id_customer;
        }

        return $id_customer;
    }

    public static function getIdsByIdOrder($id_order)
    {
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT id_prepayment_last_activities
		FROM `'._DB_PREFIX_.'prepayment_last_activities_order`
		WHERE `id_order` = '.(int)$id_order.'');

        if (!$res) {
            return array();
        }

        return $res;
    }

    public static function getOperationsByIdOrder($id_order)
    {
        $ids = array();

        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pla.id_operation
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		LEFT JOIN `'._DB_PREFIX_.'prepayment_last_activities_order` plao ON pla.`id_prepayment_last_activities` = plao.`id_prepayment_last_activities`
		WHERE plao.`id_order` = '.(int)$id_order.'');

        if (is_array($res) && count($res)) {
            foreach ($res as $id_operation) {
                $ids[] = $id_operation['id_operation'];
            }
        }

        return $ids;
    }

    public static function refundExists($id_order_slip)
    {
        return (bool)Db::getInstance()->getValue('
		SELECT plar.`id_prepayment_last_activities`
		FROM `'._DB_PREFIX_.'prepayment_last_activities_refund` plar
		WHERE plar.`id_order_slip` = '.(int)$id_order_slip.'');
    }

    public function deletePartial()
    {
        if (($partial = PrepaymentPartials::getPartialInstanceByIdLastActivity($this->id))) {
            $partial->delete();
        }

        return true;
    }

    public function deleteLabel()
    {
        return Db::getInstance()->execute('
        DELETE FROM `'._DB_PREFIX_.'prepayment_last_activities_lang` WHERE `id_prepayment_last_activities` = '.(int)$this->id);
    }

    public function addGift($id_gift)
    {
        $this->deleteGift();

        return Db::getInstance()->execute('
        INSERT INTO `'._DB_PREFIX_.'prepayment_last_activities_gift` (`id_prepayment_last_activities`, `id_prepayment_gifts`)
        VALUES ('.(int)$this->id.', '.(int)$id_gift.')');
    }

    public function deleteGift()
    {
        return Db::getInstance()->execute('
        DELETE FROM `'._DB_PREFIX_.'prepayment_last_activities_gift` WHERE `id_prepayment_last_activities` = '.(int)$this->id);
    }

    public function getGift()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
		SELECT plag.`id_prepayment_gifts`
		FROM `'._DB_PREFIX_.'prepayment_last_activities_gift` plag
		WHERE plag.`id_prepayment_last_activities` = '.(int)$this->id);
    }

    public function addRefund($id_order_slip)
    {
        $this->deleteRefund();

        return Db::getInstance()->execute('
        INSERT INTO `'._DB_PREFIX_.'prepayment_last_activities_refund` (`id_prepayment_last_activities`, `id_order_slip`)
        VALUES ('.(int)$this->id.', '.(int)$id_order_slip.')');
    }

    public function deleteRefund()
    {
        return Db::getInstance()->execute('
        DELETE FROM `'._DB_PREFIX_.'prepayment_last_activities_refund` WHERE `id_prepayment_last_activities` = '.(int)$this->id);
    }

    public function getRefund()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
		SELECT plar.`id_order_slip`
		FROM `'._DB_PREFIX_.'prepayment_last_activities_refund` plar
		WHERE plar.`id_prepayment_last_activities` = '.(int)$this->id);
    }

    public function addOrder($id_order)
    {
        $this->deleteOrder();

        return Db::getInstance()->execute('
        INSERT INTO `'._DB_PREFIX_.'prepayment_last_activities_order` (`id_prepayment_last_activities`, `id_order`)
        VALUES ('.(int)$this->id.', '.(int)$id_order.')');
    }

    public function deleteOrder()
    {
        return Db::getInstance()->execute('
        DELETE FROM `'._DB_PREFIX_.'prepayment_last_activities_order` WHERE `id_prepayment_last_activities` = '.(int)$this->id);
    }

    public function getOrder()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
		SELECT plao.`id_order`
		FROM `'._DB_PREFIX_.'prepayment_last_activities_order` plao
		WHERE plao.`id_prepayment_last_activities` = '.(int)$this->id);
    }

    public static function getPendingMovements($id_operation)
    {
        return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
		SELECT COUNT(*) AS nb_pending
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`id_operation` = '.(int)$id_operation.'
	 	AND pla.`paid` = 0');
    }

    public static function getTotalAmount($id_operation, $id_wallet = null, $paid = true)
    {
        $amount = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT SUM(pla.`amount`) as nb_amount, pla.`id_currency`
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`id_operation` = '.(int)$id_operation.
        (isset($id_wallet) ? ' AND pla.`id_wallet` = '.(int)$id_wallet : '')
        .($paid ? ' AND pla.`paid` = "1"' : '').
        'GROUP BY pla.`id_currency`');

        $total_amount = 0;
        foreach ($amount as $credit) {
            $total_amount += Tools::convertPriceFull($credit['nb_amount'], Currency::getCurrencyInstance((int)$credit['id_currency']), Currency::getCurrencyInstance((int)Configuration::get('WALLET_CURRENCY_DEFAULT')));
        }

        return $total_amount;
    }

    public static function generateReference()
    {
        return Tools::strtoupper(Tools::passwdGen(9, 'ALPHANUMERIC'));
    }

    public static function getMovements()
    {
        $now = date('Y-m-d H:i:s');
        $time = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($now)));

        return (int)Db::getInstance()->getValue('
		SELECT COUNT(pla.`id_prepayment_last_activities`)
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`date_upd` > \''.pSQL($time).'\'');
    }

    public static function getVariation()
    {
        $now = date('Y-m-d H:i:s');
        $time = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($now)));

        $deposits = 0;
        $orders = 0;
        $refunds = 0;
        $disbursements = 0;
        $gifts = 0;

        $movements = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pla.`id_operation`, SUM(pla.`amount`) as nb_amount
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`date_upd` < \''.pSQL($time).'\'
		AND pla.`paid` = 1
		GROUP BY pla.`id_operation`');

        if (!$movements) {
            return 0;
        }

        foreach ($movements as $movement) {
            if ($movement['id_operation'] == PrepaymentLastActivities::DEPOSIT || $movement['id_operation'] == PrepaymentLastActivities::CUSTOM_DEPOSIT) {
                $deposits += $movement['nb_amount'];
            } elseif ($movement['id_operation'] == PrepaymentLastActivities::ORDER) {
                $orders += $movement['nb_amount'];
            } elseif ($movement['id_operation'] == PrepaymentLastActivities::REFUND) {
                $refunds += $movement['nb_amount'];
            } elseif ($movement['id_operation'] == PrepaymentLastActivities::DISBURSEMENT || $movement['id_operation'] == PrepaymentLastActivities::CUSTOM_DISBURSEMENT) {
                $disbursements += $movement['nb_amount'];
            } elseif ($movement['id_operation'] == PrepaymentLastActivities::GIFT) {
                $gifts += $movement['nb_amount'];
            }
        }

        $tendance = $deposits + $gifts + $refunds - $orders - $disbursements;
        return Tools::ps_round($tendance);
    }

    public static function getAmountEarned()
    {
        $default_currency = Currency::getCurrencyInstance((int)Configuration::get('PS_CURRENCY_DEFAULT'));
        $amounts = 0;

        $rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT pla.`id_currency`, SUM(pla.`amount`) as amount
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`paid`= 1
		AND pla.`id_operation` = '.(int)PrepaymentLastActivities::DEPOSIT.'
		GROUP BY pla.`id_currency`');

        if (!$rows) {
            return $amounts;
        }

        foreach ($rows as $row) {
            $amounts += Tools::convertPriceFull($row['amount'], Currency::getCurrencyInstance((int)$row['id_currency']), $default_currency);
        }

        return $amounts;
    }

    public static function getStats()
    {
        $result = array();

        $nb_activities = count(self::getLastActivities());

        $deposits = Db::getInstance()->getRow('
		SELECT SUM(pla.`amount`) AS nb_credit, COUNT(pla.`id_prepayment_last_activities`) AS nb_operation
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`id_operation` = '.(int)PrepaymentLastActivities::DEPOSIT.'
		HAVING COUNT(pla.`id_prepayment_last_activities`) > 0');
        if (!$deposits) {
            $amount_per_deposit = 0;
        } else {
            $amount_per_deposit = $deposits['nb_credit'] / $deposits['nb_operation'];
        }

        $orders = Db::getInstance()->getRow('
		SELECT SUM(pla.`amount`) AS nb_credit, COUNT(pla.`id_prepayment_last_activities`) AS nb_operation
		FROM `'._DB_PREFIX_.'prepayment_last_activities` pla
		WHERE pla.`id_operation` = '.PrepaymentLastActivities::ORDER.'
		HAVING COUNT(pla.`id_prepayment_last_activities`) > 0');
        if (!$orders) {
            $amount_per_order = 0;
        } else {
            $amount_per_order = $orders['nb_credit'] / $orders['nb_operation'];
        }

        $deposit_per_order = 0;
        if (isset($deposits['nb_operation']) && isset($orders['nb_operation'])) {
            $deposit_per_order = $orders['nb_operation'] / $deposits['nb_operation'];
        }

        $currency = Currency::getCurrencyInstance(Configuration::get('WALLET_CURRENCY_DEFAULT'));

        $result['nb_activities'] = $nb_activities;
        $result['amount_per_deposit'] = Tools::displayPrice($amount_per_deposit, $currency);
        $result['amount_per_order'] = Tools::displayPrice($amount_per_order, $currency);
        $result['deposit_per_order'] = Tools::ps_round($deposit_per_order, 2);

        return $result;
    }

    public static function isPartial($id_last_activity, $active = true)
    {
        return (bool)Db::getInstance()->getValue('
		SELECT `id_prepayment_partials`
		FROM `'._DB_PREFIX_.'prepayment_partials`
		WHERE `id_prepayment_last_activities` = '.(int)$id_last_activity);
    }

    public function getPartialId()
    {
        return (int)Db::getInstance()->getValue('
		SELECT `id_prepayment_partials`
		FROM `'._DB_PREFIX_.'prepayment_partials`
		WHERE `id_prepayment_last_activities` = '.(int)$this->id);
    }

    public static function getDisplayAmount($operation, $amount = 0)
    {
        if (!$operation) {
            return 0;
        }

        $positive = array(PrepaymentLastActivities::DEPOSIT, PrepaymentLastActivities::REFUND, PrepaymentLastActivities::GIFT, PrepaymentLastActivities::CUSTOM_DEPOSIT);
        $negative = array(PrepaymentLastActivities::ORDER, PrepaymentLastActivities::DISBURSEMENT, PrepaymentLastActivities::CUSTOM_DISBURSEMENT);

        if (in_array($operation, $positive)) {
            return $amount;
        } elseif (in_array($operation, $negative)) {
            return -$amount;
        }

        return 0;
    }

    public function setWsOperationId($id_operation)
    {
        return true;
    }

    public function getWsOperation()
    {
        switch ($this->id_operation) {
            case self::DEPOSIT:
                $operation = 'DEPOSIT';
                break;
            case self::ORDER:
                $operation = 'ORDER';
                break;
            case self::REFUND:
                $operation = 'REFUND';
                break;
            case self::DISBURSEMENT:
                $operation = 'DISBURSEMENT';
                break;
            case self::GIFT:
                $operation = 'GIFT';
                break;
            case self::CUSTOM_DEPOSIT:
                $operation = 'CREDIT';
                break;
            case self::CUSTOM_DISBURSEMENT:
                $operation = 'DEBIT';
                break;
            default:
                $operation = null;
                break;
        }

        return $operation;
    }

    public function setWsOperation($operation)
    {
        $object = new self($this->id);

        if (Validate::isLoadedObject($object)
            && !in_array($object->id_operation, array(self::CUSTOM_DEPOSIT, self::CUSTOM_DISBURSEMENT))
        ) {
            return true;
        }

        switch ($operation) {
            case 'CREDIT':
                $this->id_operation = self::CUSTOM_DEPOSIT;
                break;
            case 'DEBIT':
                $this->id_operation = self::CUSTOM_DISBURSEMENT;
                break;
            default:
                $this->id_operation = null;
        }

        return true;
    }

    public function setWsReference($reference)
    {
        return true;
    }

    public function setWsPaid($paid)
    {
        return true;
    }

    public function getWsOrder()
    {
        $result = array();

        if (($id_order = $this->getOrder())) {
            $result = array(
                'id' => $id_order
            );
        }

        return $result;
    }

    public function getWsRefunds()
    {
        $result = array();

        if (($id_order_slip = $this->getRefund())) {
            $result = array(
                'id' => $id_order_slip
            );
        }

        return $result;
    }

    public function getWsPartials()
    {
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
        SELECT pp.id_cart, pp.id_cart_rule
        FROM `'._DB_PREFIX_.'prepayment_partials` pp
        WHERE pp.id_prepayment_last_activities = '.(int)$this->id);
    }

    public function addWs($autodate = true, $null_values = false)
    {
        if (!in_array($this->id_operation, array(self::CUSTOM_DEPOSIT, self::CUSTOM_DISBURSEMENT))) {
            return false;
        }

        return $this->add($autodate, $null_values);
    }

    public function updateWs($null_values = false)
    {
        $update_fields = array('label');

        if (in_array($this->id_operation, array(self::CUSTOM_DEPOSIT, self::CUSTOM_DISBURSEMENT))) {
            $update_fields = array_merge($update_fields, array(
                'id_operation',
                'amount',
            ));
        }

        $object = new self($this->id);
        $fields = array_keys(self::$definition['fields']);

        foreach ($fields as $field) {
            if (!in_array($field, $update_fields)) {
                $this->{$field} = $object->{$field};
            }
        }

        return $this->update($null_values);
    }
}
