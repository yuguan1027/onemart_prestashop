/*
* 2014 Keyrnel
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author Keyrnel
* @copyright  2017 - Keyrnel SARL
* @license commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

$(function() {
	$(document).on('click', '#desc-order-partial_refund', function() {
		var destination = $('button[name=partialRefund]').closest('.partial_refund_fields');
		insertFormFields(destination);
    });

	$(document).on('click', '#desc-order-standard_refund', function() {
		var destination = $('input[name=cancelProduct]').closest('.standard_refund_fields').find('.form-group')
		insertFormFields(destination);
    });

	$(document).on('change', 'input[name="refund_to_wallet"]', function() {
		$(this).is(':checked')
			? $('.refund_to_wallet_field').show()
			: $('.refund_to_wallet_field').hide();
	});

	function insertFormFields(destination) {
		var source = $('#prepayment_refund');

		source.length == 0
			? destination.prepend(getFormFields())
			: source.prependTo(destination)
	}

	function getFormFields() {
		var newCheckBox = '<p class="checkbox"><label>\n' +
			'<input type="checkbox" name="refund_to_wallet">\n' +
			wallet_refund + '</label></p>';

		var newInput = '<div class="refund_to_wallet_field form-group" style="display:none; max-width:400px; margin-top:15px"><div class="input-group">\n' +
				'<span class="input-group-addon">'+ wallet_refund_label +'</span>\n' +
				'<input type="text" name="refund_to_wallet_label" value=""></div></div>';

		return '<div id="prepayment_refund">' + newCheckBox + newInput + '</div>';
	}
});
