/*
* 2014 Keyrnel
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author Keyrnel
* @copyright  2017 - Keyrnel SARL
* @license commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

$(function() {
	$(document).on('click', 'button.partial-refund-display, button.standard-refund-display', function() {
        if ($('input[name="refund_to_wallet"]').length == 0) {
            $('form[name="cancel_product"] .refund-checkboxes-container').append(getFormFields());
        }
    });

	$(document).on('change', 'input[name="refund_to_wallet"]', function() {
		$(this).is(':checked')
			? $('.refund_to_wallet_field').show()
			: $('.refund_to_wallet_field').hide();
	});

	function getFormFields() {
		var newCheckBox = '<div class="form-group" style="display:block;">\n' +
			'<div class="checkbox"><div class="md-checkbox md-checkbox-inline"><label>\n' +
			'<input type="checkbox" name="refund_to_wallet" material_design="material_design">\n' +
			'<i class="md-checkbox-control"></i>' + wallet_refund + '</label></div></div></div>';

		var newInput = '<div class="refund_to_wallet_field form-group" style="display:none; max-width:400px;">\n' +
			'<div class="input-group"><div class="input-group-prepend"><div class="input-group-text">'+ wallet_refund_label +'</div></div>\n' +
			'<input type="text" class="form-control" name="refund_to_wallet_label" value=""></div></div>';

		return newCheckBox + newInput;
	}
});
