/*
* 2014 Keyrnel
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author Keyrnel
* @copyright  2017 - Keyrnel SARL
* @license commercial
* International Registered Trademark & Property of Keyrnel SARL
*/

$(function() {
	prepayment_front_deposits.init();
});

var prepayment_front_deposits = (function() {
	var original_url = window.location + '';
	var first_url_check = true;
	var selected_attributes = [];

	function initUrl() {
		if (original_url != window.location || first_url_check) {
			first_url_check = false;
			var url = window.location + '';

			// if we need to load a specific combination
			if (url.indexOf('#/') != -1){
				// get the params to fill from a "normal" url
				params = url.substring(url.indexOf('#') + 1, url.length);
				tabParams = params.split('/');
				tabValues = [];
				if (tabParams[0] == '')
					tabParams.shift();

				var len = tabParams.length;
				for (var i=0; i<len; i++)
					tabValues.push(tabParams[i].split('-'));

				$('#block_amounts').each(function() {
					var group_name = $(this).attr('data-rewrite-group-name');
					for (var a in tabValues) {
						if (group_name === decodeURIComponent(tabValues[a][0])) {
							var blockId = $(this).attr('id');
							if (blockId === "block_amounts") {
								var amount_fixed = false;
								$('#block_amounts input[name="amount_select"]').each(function() {
									var amount = $(this).val();
									if (amount == tabValues[a][1]) {
										amount_fixed = true;
										return;
									}
								});

								if (amount_fixed) {
									$('#block_amounts input[name="amount_input"]').val('').closest('.input-group').removeClass('selected');
									$('#block_amounts input[name="amount_select"][value="'+tabValues[a][1]+'"]').attr('checked', 'checked');
									$('#block_amounts input[name="amount_select"]').closest('label').find('.card').removeClass('selected');
									$('#block_amounts input[name="amount_select"][value="'+tabValues[a][1]+'"]').closest('label').find('.card').addClass('selected');
								} else {
									$('#block_amounts input[name="amount_select"]').removeAttr('checked');
									$('#block_amounts input[name="amount_select"]').closest('label').find('.card').removeClass('selected');
									$('#block_amounts input[name="amount_input"]').val(tabValues[a][1].replace('.', ',')).closest('.input-group').addClass('selected');
								}

								$('#block_amounts input[name="amount"]').val(parseAmount(tabValues[a][1]));
							}
						}
					}
				});
			}
		}

		getProductAttribute();
	}

	function initBinds() {
		$('#block_amounts input[name="amount_select"]').on('change', function() {
			hideErrorMessage();
			$('#proceed_deposit_btn').attr('disabled', true);
			$('#block_amounts input[name="amount_input"]').closest('.input-group').removeClass('selected');
			$('#block_amounts input[name="amount"]').val(parseAmount($(this).val()));
			$('#block_amounts input[name="amount_select"]').closest('label').find('.card').removeClass('selected');
			$(this).closest('label').find('.card').addClass('selected');
			getProductAttribute();
		});

		$('#block_amounts input[name="amount_input"]').focusin(function() {
			hideErrorMessage();
			$('#proceed_deposit_btn').attr('disabled', true);
			$('#block_amounts input[name="amount_select"]').removeAttr('checked');
			$('#block_amounts input[name="amount_select"]').closest('label').find('.card').removeClass('selected');
			$(this).closest('.input-group').addClass('selected');
		});

		$('#block_amounts input[name="amount_input"]').focusout(function() {
			var amount = parseAmount($(this).val());

			if (amount !== false
				&& amount >= amount_from
				&& amount <= amount_to
				&& Number.parseInt(amount * 100) % Number.parseInt(amount_pitch * 100) == 0
			) {
				$('#block_amounts input[name="amount"]').val(amount);
				getProductAttribute();
			} else {
				showErrorMessage(amount_invalid_message);
			}

		});

		$('#block_button button[type="button"]').on('click', function() {
			addToCart();
		});
	}

	function getProductAttribute() {
		var request = '';
		var tab_attributes = [];

		$('#block_amounts input[name="amount"]').each(function() {
			var attribute = new Object();
			attribute['id_attribute_group'] = $(this).closest('.attributes').attr('data-id-attribute-group');
			attribute['group_name'] = $(this).closest('.attributes').attr('data-rewrite-group-name');
			attribute['value'] = $(this).val();
			tab_attributes.push(attribute);
		});

		selected_attributes = tab_attributes;

		// build new request
		for (var a in tab_attributes)
			request += '/'+tab_attributes[a]['group_name'] + attribute_anchor_separator + tab_attributes[a]['value'];

		request = request.replace(request.substring(0, 1), '#/');
		url = window.location + '';

		// redirection
		if (url.indexOf('#') != -1)
			url = url.substring(0, url.indexOf('#'));

		window.location = url + request;

		$('#proceed_deposit_btn').removeAttr('disabled');
	}

	function addToCart() {
		var params = {
			attributes: selected_attributes,
			ajax: true,
			action: 'getCombination'
		};

		$('#proceed_deposit_btn').attr('disabled', true);

		$.ajax({
			type: 'POST',
			url: $('#proceed_deposit_btn').attr('data-action'),
			data: params,
			success: function(data) {
				data = JSON.parse(data);
				window.location.assign(data.query);
			},
			error: function(data) {
				data = JSON.parse(data.responseText);

				if (Array.isArray(data)) {
					$.each(data, function(key, error) {
						showErrorMessage(error);
					});
				}

				$('#proceed_deposit_btn').removeAttr('disabled');
			}
		});
	}

	function showErrorMessage(msg) {
		$('#block_error_message').find('span').html(msg);
		$('#block_error_message').show();
	}

	function hideErrorMessage() {
		$('#block_error_message').hide();
		$('#block_error_message').find('span').html();
	}

	function parseAmount(value) {
		var match = value.match(/\d+[.,]?\d{0,2}/);

		if (match === null || match[0] !== match.input) {
			return false;
		}

		var amount = value.toString().replace(',', '.').replace(/\s+/g, '');
		return Number.parseFloat(amount).toFixed(2);
	}

	return {
		init: function() {
			initUrl();
			initBinds();
		}
	}
})();
