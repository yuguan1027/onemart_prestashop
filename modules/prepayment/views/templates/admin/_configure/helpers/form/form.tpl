{*
* 2017 Keyrnel
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author Keyrnel
* @copyright  2017 - Keyrnel SARL
* @license commercial
* International Registered Trademark & Property of Keyrnel SARL
*}

{extends file="helpers/form/form.tpl"}

{block name="script"}
    {$smarty.block.parent}

     $(function() {
         $(document).on('change', 'select[name="currency_selector"]', function() {
             hideOtherCurrency(this.value);
         });
     });

     function hideOtherCurrency(id) {
        $('.currency-field').addClass('hide');
        $('.currency-'+id).removeClass('hide');
    }
{/block}
