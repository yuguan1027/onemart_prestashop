{*
* 2014 Keyrnel
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author Keyrnel
* @copyright  2017 - Keyrnel SARL
* @license commercial
* International Registered Trademark & Property of Keyrnel SARL
*}

{if version_compare($smarty.const._PS_VERSION_, '1.7', '<')}
{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		{l s='My account' mod='prepayment'}
	</a>
	<span class="navigation-pipe">{$navigationPipe|escape:'html':'UTF-8'}</span>
	<span class="navigation_page">{l s='Deposits funds' mod='prepayment'}</span>
{/capture}
{/if}

{if $errors_nb > 0}
	{$errors_rendered nofilter}{* HTML, cannot escape *}
{/if}

<script type="text/javascript">
var ajax_allowed = {$ajax_allowed|intval};
var attribute_anchor_separator = "{$attribute_anchor_separator|escape:'html':'UTF-8'}";
var amount_from = {$amount_from|floatval};
var amount_to = {$amount_to|floatval};
var amount_pitch = {$amount_pitch|floatval};
var amount_invalid_message = "{l s='Please select a valid amount' mod='prepayment'}";
</script>

<div class="prepayment-main-content">
	<h1>{l s='Deposit funds into your wallet' mod='prepayment'}</h1>

	{if isset($product) && $product->id && $product->active}
		<div class="max-width-lg mt-12 mb-6">
			<div id="block_amounts" class="attributes" data-id-attribute-group="{$amount_vars.id_attribute_group|intval}" data-rewrite-group-name="{$amount_vars.rewrite_group_name|escape:'quotes':'UTF-8'}">
				<div class="row">
					<div class="col-lg-12">
						<div class="content">
							<div id="block_error_message" class="alert alert-danger" role="alert">
								<span class="alert-text"></span>
							</div>
							<div class="row is-flex">
								<div class="col-lg-5 col-xs-12">
									<div class="form-group">
										<h3 class="ta-center">
											{l s='Select an amount' mod='prepayment'}
										</h3>
										{foreach from=$amount_vars.attributes item=amount}
											<label>
												<input
													type="radio"
													class="card-input-element d-none"
													name="amount_select"
													value="{number_format($amount.attribute_value|floatval, 2, '.', '')}"
													{if $amount_default==$amount.attribute_value}checked="checked"{/if}
												>
													<div class="card card-body bg-light d-flex flex-row justify-content-between align-items-center {if $amount_default==$amount.attribute_value}selected"{/if}">
														{Tools::displayPrice($amount.attribute_value|floatval)}
													</div>
											</label>
										{/foreach}
									</div>
								</div>
								<div class="col-lg-2 col-xs-12 justify-content-center">
									<span class="separator">
										{l s='or' mod='prepayment'}
									</span>
								</div>
								<div class="col-lg-5 col-xs-12">
									<div class="form-group">
										<h3 class="ta-center">
											{l s='Enter an amount' mod='prepayment'}
										</h3>
										<label class="form-control-label">
											{l s='Amount between' mod='prepayment'} {Tools::displayPrice($amount_from|floatval)} {l s='and' mod='prepayment'} {Tools::displayPrice($amount_to|floatval)}
										</label>
										<div class="input-group">
											<input type="text" class="form-control" name="amount_input" placeholder="{l s='step' mod='prepayment'}: {Tools::displayPrice($amount_pitch|floatval)}" value=""/>
											<input type="hidden" name="amount" value="{$amount_default|floatval}"/>
											<div class="input-group-append">
												<span class="input-group-text">{$currency_sign|escape:'html':'UTF-8'}</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="block_button" class="mt-9">
				<div class="row">
					<div class="col-lg-12">
						<div class="content ta-center">
							<button type="button" disabled id="proceed_deposit_btn" data-action="{$link->getModuleLink('prepayment', 'deposits')|escape:'html':'UTF-8'}" class="btn btn-primary">
								{l s='Proceed' mod='prepayment'}</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	{else}
		{if !$product->active}
			{l s='Deposits are not available for this currency yet' mod='prepayment'}
		{else}
			{l s='No deposits available for the moment..' mod='prepayment'}
		{/if}
	{/if}
</div>
