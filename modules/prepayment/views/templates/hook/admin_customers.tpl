{*
* 2014 Keyrnel
*
* NOTICE OF LICENSE
*
* The source code of this module is under a commercial license.
* Each license is unique and can be installed and used on only one shop.
* Any reproduction or representation total or partial of the module, one or more of its components,
* by any means whatsoever, without express permission from us is prohibited.
* If you have not received this module from us, thank you for contacting us.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future.
*
* @author Keyrnel
* @copyright  2017 - Keyrnel SARL
* @license commercial
* International Registered Trademark & Property of Keyrnel SARL
*}

<div id="wallet_admin_customers" class="{if version_compare($smarty.const._PS_VERSION_, '1.7.6', '<')}col-lg-6{else}col{/if}">
	<div class="{if version_compare($smarty.const._PS_VERSION_, '1.7.6', '<')}panel{else}card{/if}">
		<h3 class="card-header">
			<i class="icon-group"></i>
			{l s='Wallet' mod='prepayment'}
				<span class="badge badge-primary rounded">{($history_list)|@count|intval}</span>
		</h3>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-5">
					<div class="{if version_compare($smarty.const._PS_VERSION_, '1.7.6', '<')}panel{else}card{/if}">
						<div class="card-body">
							{l s='Account balance' mod='prepayment'}<br/>
							<span class="bold" style="color:#5C9939;">{$balance|escape:'quotes':'UTF-8'}</span>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					 <div class="col-lg-12">
						<span class="label-tooltip" data-toggle="tooltip" title="{l s='Enabled or disabled customer wallet' mod='prepayment'}">{l s='Status' mod='prepayment'}</span>
					</div>
					<div class="col-lg-12">
						<span class="switch prestashop-switch fixed-width-lg">
							<input id="status_on" type="radio" value="1" name="status" {if $wallet->active == 1}checked="checked"{/if}>
							<label class="radioCheck" for="status_on">{l s='Enabled' mod='prepayment'}</label>
							<input id="status_off" type="radio" value="0" name="status" {if $wallet->active == 0}checked="checked"{/if}>
							<label class="radioCheck" for="status_off">{l s='Disabled' mod='prepayment'}</label>
							<a class="slide-button btn"></a>
						</span>
					</div>
				</div>
				<div class="col-lg-3">
					 <div class="col-lg-12">
						<button style="margin-top: 16px" class="btn btn-primary js-btn-save" type="button" onclick="update_balance();">
							<span>{l s='Update Balance' mod='prepayment'}</span>
						</button>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<table class="table">
						<thead>
							<tr>
								<th class="first_item" data-sort-ignore="true">{l s='Operation' mod='prepayment'}</th>
								<th class="item" data-sort-ignore="true">{l s='Date' mod='prepayment'}</th>
								<th data-sort-ignore="true" class="item">{l s='Amount' mod='prepayment'}</th>
								<th data-sort-ignore="true" class="item">{l s='Status' mod='prepayment'}</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						{if $history_list AND count($history_list)}
							{foreach from=$history_list item=history name=myLoop}
								<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
								<td>
									<span class="operation">
										{$history.operation|escape:'html':'UTF-8'}
									</span>
								</td>
								<td data-value="{$history.date_add|escape:'quotes':'UTF-8'}" class="history_date bold">
									{dateFormat date=$history.date_add full=0}
								</td>
								<td class="history_price" data-value="{$history.amount|floatval}">
									<span class="amount">
										{if $history.amount > 0}{$history.display_amount|escape:'html':'UTF-8'}{else} -- {/if}
									</span>
								</td>
								<td class="history_state">
									{if $history.paid == 0}{l s='Pending' mod='prepayment'}{else}{l s='Done' mod='prepayment'}{/if}
								</td>
								<td class="text-right">
									<div class="btn-group">
										<a class="btn btn-default" href="{$history.href|escape:'html':'UTF-8'}">
											<i class="icon-search"></i> {l s='View' mod='prepayment'}
										</a>
									</div>
								</td>
								</tr>
							{/foreach}
						{else}
						<tr><td>{l s='No records found' mod='prepayment'}</tr></td>
						{/if}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var update_success_msg = "{l s='Update successful' mod='prepayment' js=1}";

function update_balance()
{
	var params = {
		tab : 'AdminPrepaymentWallets',
		action : 'updateBalanceWallet',
		ajax: 1,
		id_wallet : {$wallet->id|intval},
		token : '{getAdminToken tab='AdminPrepaymentWallets'}'
		};

	$.ajax({
		type: "POST",
		url: "index.php",
		data: params,
		async : true,
		success: function(data) {
			data = $.parseJSON(data);
			if (data.success == 'ok'){
				showSuccessMessage(update_success_msg);
			}
		}
	});
}

$("input[name=status]:radio").change(function(){

	var checked = $('input:radio[name=status]:checked').val();
	var params = {
		tab : 'AdminPrepaymentWallets',
		action : 'updateStatusWallet',
		ajax: 1,
		id_wallet : {$wallet->id|intval},
		status : checked,
		token : '{getAdminToken tab='AdminPrepaymentWallets'}'
		};

	$.ajax({
		type: "POST",
		url: "index.php",
		data: params,
		async : true,
		success: function(data) {
			data = $.parseJSON(data);
			if (data.success == 'ok'){
				showSuccessMessage(update_success_msg);
			}
		}
	});
	return false;
});

</script>
